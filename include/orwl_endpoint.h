/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the authors and copyright holders for this work are as follows:            */
/* all rights reserved,  2011 Emmanuel Jeanvoine, INRIA, France               */
/* all rights reserved,  2010-2013 Jens Gustedt, INRIA, France                */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#ifndef     ORWL_ENDPOINT_H_
# define    ORWL_ENDPOINT_H_

#include "orwl_buffer.h"
#include "orwl_timing.h"

#ifdef ORWL_NO_IPV6
# undef POSIX_IPV6
# define POSIX_IPV6 -1
#endif


P99_DECLARE_STRUCT(orwl_server);
P99_DECLARE_STRUCT(orwl_proc);
P99_DECLARE_STRUCT(orwl_mirror);
P99_DECLARE_STRUCT(orwl_wq);
P99_DECLARE_STRUCT(orwl_endpoint);
P99_DECLARE_UNION(orwl_addr);
P99_DECLARE_STRUCT(orwl_wh);
P99_DECLARE_STRUCT(orwl_handle2);
P99_DECLARE_STRUCT(orwl_handle);
P99_DECLARE_STRUCT(orwl_sockaddr_raw);
P99_DECLARE_STRUCT(orwl_vertex);
P99_DECLARE_STRUCT(orwl_graph);
P99_DECLARE_STRUCT(orwl_address_book);

/**
 ** @addtogroup library_support
 ** @{
 **/

typedef struct o_rwl_sockaddr_test {
#if SA_FAMILY_OFFSET > 0
  char prefix[SA_FAMILY_OFFSET];
#endif
  sa_family_t soff_family;
  uint8_t offset[1];
} o_rwl_sockaddr_test;

P99_CONST_FUNCTION char const* sa_family_getname(sa_family_t);


static_assert(
  offsetof(struct sockaddr, sa_family) == offsetof(o_rwl_sockaddr_test, soff_family),
  "please adjust SA_FAMILY_OFFSET if this assertion fails"
);

union o_rwl_sockaddr_max {
  struct sockaddr_in sin;
  struct sockaddr_un sun;
#if POSIX_IPV6 > 0
  struct sockaddr_in6 sin6;
#endif
};

/**
 ** @brief A structure for the manipulation of the raw socket data
 ** structures.
 **
 ** Don't use this structure directly, but the corresponding macros.
 ** @see ORWL_IN4
 ** @see ORWL_IN4_ANY
 **/
struct orwl_sockaddr_raw {
#if SA_FAMILY_OFFSET > 0
  char prefix[SA_FAMILY_OFFSET];
#endif
  sa_family_t raw_family;
  char raw_data[sizeof(union o_rwl_sockaddr_max) - offsetof(o_rwl_sockaddr_test, offset)];
};

#ifdef DOXYGEN
/**
 ** @brief Store IPv4 and IPv6 addresses in the same structure.
 **/
P99_DEFINE_UNION(orwl_addr,
                 struct sockaddr sa;
                 struct sockaddr_in sin;
                 struct sockaddr_un sun;
                 struct sockaddr_in6 sin6;
                 orwl_sockaddr_raw raw;
                );
#else
/**
 ** @brief Store IPv4 and IPv6 addresses in the same structure.
 **/
#if POSIX_IPV6 > 0
P99_DEFINE_UNION(orwl_addr,
                 struct sockaddr sa;
                 struct sockaddr_in sin;
                 struct sockaddr_un sun;
                 struct sockaddr_in6 sin6;
                 orwl_sockaddr_raw raw;
                );
#else
P99_DEFINE_UNION(orwl_addr,
                 struct sockaddr sa;
                 struct sockaddr_in sin;
                 struct sockaddr_un sun;
                 orwl_sockaddr_raw raw;
                );
#endif
#endif

/**
 ** @brief Represent a remote ORWL location.
 **/
struct orwl_endpoint {
  int sock_type;
  uint64_t index;
  orwl_addr addr;
};

/**
 ** @see orwl_addr
 **/
#define ORWL_ADDR_INITIALIZER                                  \
{                                                              \
  /* try to ensure that the whole is zeroed out. */            \
  .raw = { .raw_data = { 0 } }                                 \
}

#define ORWL_ADDR_EMPTY ((orwl_addr const)ORWL_ADDR_INITIALIZER)

/**
 ** @see orwl_endpoint
 **/
#define ORWL_ENDPOINT_INITIALIZER(ADDR, INDEX) { .addr = ADDR, .index = INDEX, }


DOCUMENT_INIT(orwl_addr)
inline
orwl_addr* orwl_addr_init(orwl_addr *A) {
  if (A) {
    *A = (orwl_addr const)ORWL_ADDR_INITIALIZER;
  }
  return A;
}

/**
 ** @brief Return the IPv4 address stored in @a A.
 **
 ** If this is not an IPv4 address return all bit ones, that is
 ** @c 255.255.255.255 which isn't a valid address.
 **
 ** Also, if @a A is the all zero address, @c 0.0.0.0 is returned.
 **
 ** @related orwl_addr
 **/
inline
struct in_addr addr2net(orwl_addr const*A) {
  struct in_addr ret = { .s_addr = ~P99_0(in_addr_t) };
  if (A->sa.sa_family == AF_INET) {
    ret = A->sin.sin_addr;
  }
  return ret;
}

/**
 ** @related orwl_addr
 **/
inline
in_port_t
orwl_addr_get_port(orwl_addr const* addr) {
  in_port_t ret = 0;
  switch (addr->sa.sa_family) {
  case AF_INET: return addr->sin.sin_port;
#if POSIX_IPV6 > 0
  case AF_INET6: return addr->sin6.sin6_port;
#endif
  }
  return ret;
}

/**
 ** @related orwl_addr
 **/
inline
orwl_addr*
orwl_addr_set_port(orwl_addr * addr, in_port_t port) {
  switch (addr->sa.sa_family) {
  case AF_INET: addr->sin.sin_port = port; break;
#if POSIX_IPV6 > 0
  case AF_INET6: addr->sin6.sin6_port = port; break;
#endif
  }
  return addr;
}

/**
 ** @related orwl_addr
 **/
inline
int orwl_getsockname( int fd, orwl_addr* addr) {
  return getsockname(fd, &addr->sa, (socklen_t[1]) { sizeof *addr });
}

/**
 ** @related orwl_addr
 **/
inline
int orwl_getpeername( int fd, orwl_addr* addr) {
  return getpeername(fd, &addr->sa, (socklen_t[1]) { sizeof *addr });
}


/**
 ** @brief set a Boolean of a socket @a fd to value @a optval
 ** @see setsockopt
 **/
int orwl_setsockflag(int fd, int lev, int optname, bool optval);


/**
 ** @brief get a Boolean of a socket @a fd in @a optval
 ** @see getsockopt
 **/
inline
int orwl_getsockflag(int fd, int lev, int optname, bool *optval) {
  int retval = optval ? *optval : 0;
  int ret = getsockopt(fd, lev, optname, &retval);
  if (ret == -1) P99_THROW(errno);
  if (optval) *optval = retval;
  return ret;
}

void orwl_socket_close(int fd);


/**
 ** @brief switch the "no delay" property of a tcp socket @a fd on or
 ** off.
 **
 ** For a "no delay" socket all data is communicated to the other
 ** endpoint as soon it is available. This is appropriate for control
 ** information that by itself is "small" but should be delivered as
 ** fast as possible. For ORWL all sockets initially transmit control
 ** information, so by default we switch sockets into this mode.
 **
 ** "Delay" sockets possibly accumulate data before it is
 ** transmitted. This is appropriate for all application data that is
 ** transfered asynchronously. It helps to reduce the impact of the
 ** connection to the network (by reducing control messages) and to
 ** augment the perceived the bandwidth. ORWL only transfers such data
 ** with the push operation, so this mode is only used there.
 **
 ** @return 0 on success and @c -1 on failure. In case of failure
 ** @a errno is set to an error indication. This is in particular the
 ** case if the socket is not a tcp socket.
 **
 ** @warning Using this might render the application volatile
 ** concerning transient network errors. Set the macro ORWL_TCP_DELAY
 ** to avoid the use of this function as initial default on all
 ** sockets.
 **
 ** @see orwl_tcp_cork
 **/
inline
int orwl_tcp_nodelay(int fd, bool val) {
#ifdef TCP_NODELAY
  int sock_type = 0;
  if (P99_LIKELY(!getsockopt(fd, SOL_SOCKET, SO_TYPE, &sock_type)
                 && sock_type == SOCK_STREAM))
    return orwl_setsockflag(fd, IPPROTO_TCP, TCP_NODELAY, val);
#endif
  errno = ENOPROTOOPT;
  return -1;
}

/*
  Enable TCP_CORK if available, otherwise use TCP_NODELAY.

  TCP_CORK, if set, means don't send out partial frames.  All queued
  partial frames are sent when the option is cleared again.  This is
  useful for prepending headers before calling sendfile(2), or for
  throughput optimization.  As currently implemented, there is a 200
  millisecond ceiling on the time for which output is corked by
  TCP_CORK.  If this ceiling is reached, then queued data is
  automatically transmitted.  This option can be combined with
  TCP_NODELAY only since Linux 2.5.71.  This option should not be used
  in code intended to be portable.
*/
inline
void orwl_tcp_cork(int fd, bool val) {
#if defined(TCP_CORK) || defined(TCP_NODELAY)
  int sock_type = 0;
  int ret = getsockopt(fd, SOL_SOCKET, SO_TYPE, &sock_type);
  if (P99_LIKELY(!ret && sock_type == SOCK_STREAM)) {
#endif
#if defined(TCP_CORK)
    orwl_setsockflag(fd, IPPROTO_TCP, TCP_CORK, val);
#elif defined(TCP_NODELAY)
    orwl_setsockflag(fd, IPPROTO_TCP, TCP_NODELAY, !val);
#endif
#if defined(TCP_CORK) || defined(TCP_NODELAY)
  }
#endif
}

/**
 ** @brief Tune a socket @a fd to the needs of ORWL.
 **
 ** This is used to set certain properties of sockets that either
 ** ensure the consistency of the data or improve the performance.
 ** Currently these are:
 **
 ** - sockets are in "linger" state, that is a @c close on the socket
 **   blocks until all data is delivered
 ** - port numbers can be reused as soon as the corresponding socket
 **   is closed
 ** - sockets are in "no wait" state, that is data is transfered as
 **   soon as it is available.
 **
 ** @see orwl_tcp_nodelay
 **/
int orwl_socket_tune(int fd);

/**
 ** @brief Signal the end of the conversation to the other endpoint of
 ** the socket
 **
 ** Use that if you know that you received all data and you'd just
 ** have to signal that fact to the other end.
 **
 ** @see orwl_socket_wait_close must be called at the other end to
 ** take that notice into account. Otherwise, if the other end closes
 ** its fd too early data could be lost.
 **/
void orwl_socket_sig_close(int fd);

/**
 ** @brief Wait for confirmation from the other end of the
 ** conversation that it has received all data
 **
 ** @see orwl_socket_sig_close must be called at the other end to
 ** confirm that the conversation is really over.
 **/
void orwl_socket_wait_close(int fd);

int orwl_setsockopt(int fd, int lev, int optname, int optval);
int orwl_getsockopt(int fd, int lev, int optname);

int orwl_socket_error(int fd);

#ifndef DOXYGEN
inline
P99_PROTOTYPE(int, orwl_accept, int, orwl_addr*);
#define orwl_accept(...) P99_CALL_DEFARG(orwl_accept, 2, __VA_ARGS__)
#define orwl_accept_defarg_1() P99_0(orwl_addr*)
#endif


/**
 ** @brief a wrapper around the @c accept system call
 ** @param fd must correspond to a socket that is in listening state
 ** @param addr (optional) can be used to return the address of the remote peer.
 **/
P99_DEFARG_DOCU(orwl_accept)
/**
 ** @see orwl_socket_tune
 ** @see orwl_tcp_nodelay
 **
 ** @todo Replace a call to @c accept by a new feature of linux, TCP
 ** fast open. Fast open is in the procedure of being normalized and
 ** is a possibility to already send a first payload along with the
 ** TCP SYN request. Since we do some short message exchanges at the
 ** beginning of the communication, we could avoid at least two ways
 ** of the handshake. TFO is implemented fully from linux 3.7. But it
 ** might still need some time until the user space integration is
 ** widely available.
 **/
inline
int orwl_accept(int fd, orwl_addr* addr) {
  int ret
    = addr
  ? accept(fd, &addr->sa, (socklen_t[1]) { sizeof *addr })
    : accept(fd, 0, 0);
  if (ret == -1) P99_THROW(errno);
  return orwl_socket_tune(ret);
}

/**
 ** @brief a wrapper around the @c connect system call
 ** @see orwl_socket_tune
 ** @see orwl_tcp_nodelay
 **/
inline
int orwl_connect(int fd, orwl_addr const* addr) {
  int ret = connect(fd, &addr->sa, sizeof *addr);
  if (ret == -1) P99_THROW(errno);
  orwl_socket_tune(fd);
  return ret;
}


/* The top of the pops of bogus portability code for the network
   layer. This computes the position of the start of the IPv4 address
   inside the "raw" structure's @c raw_data field. This all uses
   macros, so this is a compile time integral constant. */
enum {
  o_rwl_sockaddr_in_addr =  offsetof(struct sockaddr_in, sin_addr),
  o_rwl_in_addr_addr = offsetof(struct in_addr, s_addr),
  o_rwl_addr_raw = offsetof(orwl_addr, raw),
  o_rwl_addr_raw_raw_data = offsetof(orwl_sockaddr_raw, raw_data),

  /* offset to skip in the sockaddr structure that corresponds to
     the @c sa_family_t field plus possibly padding bytes. */
  o_rwl_addr_raw_data = (o_rwl_addr_raw + o_rwl_addr_raw_raw_data),

  o_rwl_in4_addr =
    /* Start of the address inside a IPv4 structure */
    o_rwl_sockaddr_in_addr + o_rwl_in_addr_addr
    /* offset to skip in the sockaddr structure that corresponds to
       the @c sa_family_t field plus possibly padding bytes. */
    - o_rwl_addr_raw_data,

  o_rwl_in4_port =
    /* Start of the port inside a IPv4 structure */
    offsetof(struct sockaddr_in, sin_port)
    /* offset to skip in the sockaddr structure that corresponds to
       the @c sa_family_t field plus possibly padding bytes. */
    - o_rwl_addr_raw_data,

#if POSIX_IPV6 > 0
  o_rwl_in6_addr_sin6_addr = offsetof(struct sockaddr_in6, sin6_addr),
  o_rwl_in6_addr_sin6_addr_addr
    =
      /* s6_addr on linux is a macro that expands into a chain of two
         designators. The use of such a thing is non standard. On the
         other hand there is not much that we can do about that, other
         platforms might or might not have such a thing. */
      p99_extension
      offsetof(struct in6_addr, s6_addr),

  o_rwl_in6_addr =
    /* Start of the address inside a IPv6 structure */
    o_rwl_in6_addr_sin6_addr + o_rwl_in6_addr_sin6_addr_addr
    /* offset to skip in the sockaddr structure that corresponds to
       the @c sa_family_t field plus possibly padding bytes. */
    - o_rwl_addr_raw_data,

  o_rwl_in6_port =
    /* Start of the port inside a IPv6 structure */
    offsetof(struct sockaddr_in6, sin6_port)
    /* offset to skip in the sockaddr structure that corresponds to
       the @c sa_family_t field plus possibly padding bytes. */
    - o_rwl_addr_raw_data,
#endif
};


/**
 ** @see orwl_addr
 **/
#define ORWL_IN4_ADDR(_0, _1, _2, _3) [o_rwl_in4_addr] = (_0), (_1), (_2), (_3)

/**
 ** @see orwl_addr
 **/
#define ORWL_IN4_PORT(PORT) [o_rwl_in4_port] = ((PORT)>>8), (PORT)&0xFF

/**
 ** @brief an initializer expression for type <code>orwl_addr
 ** const</code> for IPv4
 **
 ** E.g <code>ORWL_IN4(ORWL_IN4_ADDR(127, 0, 0, 1))</code> would
 ** correspond to the loopback address <code>127.0.0.1</code> or
 ** <code>ORWL_IN4(ORWL_IN4_ADDR(208, 67, 222, 222),
 ** ORWL_IN4_PORT(53))</code> could correspond to the initialization
 ** of a connection to a DNS server.
 **
 ** @see ORWL_IN4_ADDR
 ** @see ORWL_IN4_PORT
 **
 ** To implement this macro we have to go to the raw
 ** representation. IPv4 network addresses are traditionally given in
 ** "network" order but internally represented by integers. So we have
 ** to find the position of that integer and address the bytes
 ** individually. Such we can handle this stuff without knowing
 ** anything about the endianess of the target platform.
 ** @see orwl_addr
 **/
#define ORWL_IN4(...)                                          \
{                                                              \
  .raw = {                                                     \
    .raw_family = AF_INET,                                     \
    .raw_data = {                                              \
      __VA_ARGS__                                              \
    },                                                         \
  }                                                            \
}

/**
 ** @brief an initializer expression to use with ::ORWL_IN4 that
 ** represents an unspecific IPv4 address
 ** @see orwl_addr
 **/
#define ORWL_IN4_ANY ORWL_IN4_ADDR(0, 0, 0, 0)


/**
 ** @brief an initializer expression to use with ::ORWL_IN4 that
 ** represents the standard IPv4 loopback address
 ** @see orwl_addr
 **/
#define ORWL_IN4_LOOPBACK ORWL_IN4_ADDR(127, 0, 0, 1)


#if POSIX_IPV6 > 0
/**
 ** @brief an initializer expression for type <code>orwl_addr
 ** const</code> for IPv6
 **
 ** It represents the IPv6 address that is given by the arguments. E.g
 ** <code>ORWL_IN6(ORWL_IN6_ADDR(0, 0, 0, 0, 0, 0, 0, 0))</code> would be a valid
 ** initializer to represent the IPv6 any address. Other possible use
 ** could be
 ** @code
 ** orwl_addr addr
 **    =  ORWL_IN6(ORWL_IN6_ADDR(0x2620, 0x000c, 0xcc00, 0, 0, 0, 0, 0x0002),
 **                ORWL_IN6_PORT(53));
 ** @endcode
 ** to initialize a socket endpoint to connect to a dns server.
 **
 ** @remark This is only defined on platforms that support IPv6.
 ** @see ORWL_IN6_ADDR
 ** @see ORWL_IN6_PORT
 ** @see ORWL_IN4
 ** @see ORWL_IN6_IN4
 ** @see ORWL_IN6_ANY
 ** @see ORWL_IN6_LOOPBACK
 ** @see orwl_addr
 **/
#define ORWL_IN6(...)                                          \
{                                                              \
  .raw = {                                                     \
    .raw_family = AF_INET6,                                    \
    .raw_data = {                                              \
      __VA_ARGS__,                                             \
    },                                                         \
  }                                                            \
}

/**
 ** @brief an initializer expression to use with ::ORWL_IN6 that
 ** places the eight 16 bit address segments of an IPv6 address in the
 ** data structure.
 **
 ** @remark This is only defined on platforms that support IPv6.
 ** @see orwl_addr
 **/
# define ORWL_IN6_ADDR(_0, _1, _2, _3, _4, _5, _6, _7)                            \
[o_rwl_in6_addr] =                                                                \
  (_0)>>8, (_0)&0xFF, (_1)>>8, (_1)&0xFF, (_2)>>8, (_2)&0xFF, (_3)>>8, (_3)&0xFF, \
  (_4)>>8, (_4)&0xFF, (_5)>>8, (_5)&0xFF, (_6)>>8, (_6)&0xFF, (_7)>>8, (_7)&0xFF
/**
 ** @see orwl_addr
 **/
# define ORWL_IN6_PORT(PORT) [o_rwl_in6_port] = (((unsigned)(PORT))>>8), ((unsigned)PORT)&0xFFU

/**
 ** @brief an initializer expression to use with ::ORWL_IN6 that
 ** places the four bytes of an IPv4 address in the data structure to
 ** represent that address as encapsulated in IPv6.
 **
 ** @remark This is only defined on platforms that support IPv6.
 ** @see orwl_addr
 **/
# define ORWL_IN6_IN4(_0, _1, _2, _3)                          \
[o_rwl_in6_addr + 10] = 0xFF, 0xFF, (_0), (_1), (_2), (_3)

/**
 ** @brief an initializer expression to use with ::ORWL_IN6 that
 ** represent the unspecific IPv6 address.
 **
 ** @remark This is only defined on platforms that support IPv6.
 ** @see ORWL_IN4_ANY
 ** @see orwl_addr
 **/
# define ORWL_IN6_ANY ORWL_IN6_ADDR(0, 0, 0, 0, 0, 0, 0, 0)

/**
 ** @brief an initializer expression to use with ::ORWL_IN6 that
 ** represent the IPv6 loopback address.
 **
 ** @remark This is only defined on platforms that support IPv6.
 ** @see ORWL_IN4_LOOPBACK
 ** @see orwl_addr
 **/
# define ORWL_IN6_LOOPBACK ORWL_IN6_ADDR(0, 0, 0, 0, 0, 0, 0, 1)

#ifndef ORWL_ADDR_ANY
# define ORWL_ADDR_ANY ORWL_IN6(ORWL_IN6_ANY)
#endif
#ifndef ORWL_ADDR_LOOPBACK
# define ORWL_ADDR_LOOPBACK ORWL_IN6(ORWL_IN6_LOOPBACK)
#endif
#ifndef ORWL_LOCALHOST
# define ORWL_LOCALHOST "orwl://[::1]"
#endif
#endif

/**
 ** @def ORWL_ADDR_ANY
 ** @brief an initializer expression for type ::orwl_addr that represents an
 ** unspecific address.
 **
 ** @remark On platforms that support IPv6 this is an address of that
 ** type, otherwise it is IPv4.
 ** @see ORWL_IN4_ANY
 ** @see ORWL_IN6_ANY
 ** @see orwl_addr
 **/
#ifndef ORWL_ADDR_ANY
# define ORWL_ADDR_ANY ORWL_IN4(ORWL_IN4_ANY)
#endif

#ifndef ORWL_LOCALHOST
# define ORWL_LOCALHOST "orwl://127.0.0.1"
#endif

/**
 ** @def ORWL_ADDR_LOOPBACK
 ** @brief an initializer expression for type ::orwl_addr that
 ** represents a loopback address.
 **
 ** @remark On platforms that support IPv6 this is an address of that
 ** type, otherwise it is IPv4.
 ** @see ORWL_IN_LOOPBACK
 ** @see ORWL_IN6_LOOPBACK
 ** @see orwl_addr
 **/
#ifndef ORWL_ADDR_LOOPBACK
# define ORWL_ADDR_LOOPBACK ORWL_IN_LOOPBACK
#endif

#if POSIX_IPV6 > 0
/**
 ** @brief Return the IPv6 address stored in @a A.
 **
 ** This is only present if the platform supports IPv6.
 ** @related orwl_addr
 **/
struct in6_addr addr2net6(orwl_addr const*A);

orwl_addr orwl_addr6(orwl_addr const*A);
#endif

/**
 ** @brief Sort an array of IP addresses according to their scope.
 **
 ** Addresses with largest scope will come first. The order of
 ** priority is
 ** - global addresses
 ** - private addresses
 ** - link local addresses
 ** - loopback (host) addresses
 ** - the "any" address
 ** @related orwl_addr
 */
orwl_addr* orwl_addr_sort(size_t n, orwl_addr ali[n]);

/**
 ** @brief return all the IP addresses of host @a name in an array.
 **
 ** The last element of the vector is the all 0 address to mark its
 ** end.
 ** @related orwl_addr
 **/
orwl_addr * orwl_addr_aliases(char const *name);

/**
 ** @related orwl_addr
 **/
inline
bool orwl_addr_eq(orwl_addr const* A, orwl_addr const* B) {
  if (!A) return !B;
  if (!B) return false;
  if (A->sa.sa_family != B->sa.sa_family) return false;
  switch (A->sa.sa_family) {
  case AF_INET: {
    return A->sin.sin_addr.s_addr == B->sin.sin_addr.s_addr;
  }
#if POSIX_IPV6 > 0
  case AF_INET6: {
    return !memcmp(A->sin6.sin6_addr.s6_addr, B->sin6.sin6_addr.s6_addr, sizeof A->sin6.sin6_addr.s6_addr);
  }
#endif
  }
  return false;
}

/**
 ** @}
 **/

/**
 ** @addtogroup library_init
 ** @{
 **/

/**
 ** @related orwl_endpoint
 **/
inline
bool orwl_endpoint_similar(orwl_endpoint const* A, orwl_endpoint const* B) {
  if (!A) return !B;
  if (!B) return false;
  orwl_addr const* a = &A->addr;
  orwl_addr const* b = &B->addr;
  if (a->sa.sa_family != b->sa.sa_family) return false;
  switch (a->sa.sa_family) {
  case AF_INET: {
    return (a->sin.sin_addr.s_addr == b->sin.sin_addr.s_addr)
           && (a->sin.sin_port == b->sin.sin_port);
  }
#if POSIX_IPV6 > 0
  case AF_INET6: {
    return !memcmp(a->sin6.sin6_addr.s6_addr, b->sin6.sin6_addr.s6_addr, sizeof a->sin6.sin6_addr.s6_addr)
           && (a->sin6.sin6_port == b->sin6.sin6_port);
  }
#endif
  }
  return false;
}


/**
 ** @related orwl_endpoint
 **/
inline
bool orwl_endpoint_eq(orwl_endpoint const* A, orwl_endpoint const* B) {
  return
    orwl_endpoint_similar(A, B)
    && &A->index == &B->index;
}

#ifndef DOXYGEN
inline
P99_PROTOTYPE(orwl_endpoint*, orwl_endpoint_init, orwl_endpoint*, orwl_addr, uint64_t);
#define orwl_endpoint_init(...) P99_CALL_DEFARG(orwl_endpoint_init, 3, __VA_ARGS__)
#define orwl_endpoint_init_defarg_1() (orwl_addr const)ORWL_ADDR_INITIALIZER
#define orwl_endpoint_init_defarg_2() P99_0(uint64_t)
#endif

DOCUMENT_INIT(orwl_endpoint)
P99_DEFARG_DOCU(orwl_endpoint_init)
inline
orwl_endpoint* orwl_endpoint_init
(orwl_endpoint *endpoint, /*!< the object to initialize */
 orwl_addr addr,          /*!< defaults to the IPv4 null address */
 uint64_t index           /*!< defaults to 0 */
) {
  if (endpoint) {
    *endpoint = P99_LVAL(orwl_endpoint, .addr = addr, .index = index);
  }
  return endpoint;
}

DOCUMENT_DESTROY(orwl_endpoint)
inline
void orwl_endpoint_destroy(orwl_endpoint *endpoint) {
  P99_TZERO(*endpoint);
  endpoint->index = P99_TMAX(uint64_t);
}


DECLARE_NEW_DELETE(orwl_endpoint);

#ifndef DOXYGEN
P99_PROTOTYPE(orwl_endpoint*, orwl_endpoint_local, int, orwl_endpoint *);
#define orwl_endpoint_local(...) P99_CALL_DEFARG(orwl_endpoint_local, 2, __VA_ARGS__)
#define orwl_endpoint_local_defarg_1() &P99_LVAL(orwl_endpoint)
#endif

/**
 ** @brief return the connection information corresponding to the
 ** local endpoint of a socket connection
 ** @related orwl_endpoint
 ** @param fd file descriptor corresponding to a socket
 ** @param A memory location to which the information will be written
 ** @return @a A if the information could be provided, @c 0 in case of an error
 **/
P99_DEFARG_DOCU(orwl_endpoint_local)
orwl_endpoint* orwl_endpoint_local(int fd, orwl_endpoint * A);


#ifndef DOXYGEN
P99_PROTOTYPE(orwl_endpoint*, orwl_endpoint_remote, int, orwl_endpoint *);
#define orwl_endpoint_remote(...) P99_CALL_DEFARG(orwl_endpoint_remote, 2, __VA_ARGS__)
#define orwl_endpoint_remote_defarg_1() &P99_LVAL(orwl_endpoint)
#endif

/**
 ** @brief return the connection information corresponding to the
 ** remote endpoint of a socket connection
 ** @related orwl_endpoint
 ** @param fd file descriptor corresponding to a socket
 ** @param A memory location to which the information will be written
 ** @return @a A if the information could be provided, @c 0 in case of an error
 **/
P99_DEFARG_DOCU(orwl_endpoint_remote)
orwl_endpoint* orwl_endpoint_remote(int fd, orwl_endpoint * A);


#ifndef DOXYGEN
P99_PROTOTYPE(orwl_endpoint*, orwl_endpoint_parse, char const*, orwl_endpoint *);
#define orwl_endpoint_parse(...) P99_CALL_DEFARG(orwl_endpoint_parse, 2, __VA_ARGS__)
#define orwl_endpoint_parse_defarg_1() &P99_LVAL(orwl_endpoint)
#endif

orwl_endpoint* orwl_endpoint_parse(
  char const* name,   /*!< [in] the string to parse */
  orwl_endpoint* ep   /*!< [out] the object to initialize */
);

#ifndef DOXYGEN
P99_PROTOTYPE(char const*, orwl_endpoint_print, orwl_endpoint const*, char*restrict);
#define orwl_endpoint_print(...) P99_CALL_DEFARG(orwl_endpoint_print, 2, __VA_ARGS__)
P99_DECLARE_DEFARG(orwl_endpoint_print, , );
#define orwl_endpoint_print_defarg_1() P99_LVAL(char[128])
#endif


/**
 ** @related orwl_endpoint
 **/
P99_DEFARG_DOCU(orwl_endpoint_print)
char const* orwl_endpoint_print(orwl_endpoint const* ep, /*!< [in] the object to interpret */
                                char name[static 128]    /*!< [out] the string to initialize */
                               );

/**
 ** @}
 **/

/**
 ** @addtogroup library_low
 ** @{
 **/

void orwl_send_(int fd, uint64_t remo, size_t n, orwl_buffer volatile mess[n]);
void orwl_recv_(int fd, orwl_buffer volatile const* mess, uint64_t remo);

/**
 ** @brief Error value returned by ::orwl_send.
 **/
#define ORWL_SEND_ERROR UINT64_MAX

uint64_t o_rwl_rpc(orwl_server* srv, orwl_endpoint const* there, p99_seed *seed, size_t n, orwl_buffer mess[n]);

uint64_t orwl_send_local(orwl_server* srv, orwl_endpoint const* there, p99_seed *seed, size_t n, orwl_buffer mess[n]);
uint64_t orwl_send_remote(orwl_server* srv, orwl_endpoint const* there, p99_seed *seed, size_t n, orwl_buffer mess[n], bool async);

#define o_rwl_rpc5(SRV, THERE, SEED, F, ...)                     \
o_rwl_rpc(SRV, THERE, SEED, 1,                                   \
          &((orwl_buffer)ORWL_BUFFER_INITIALIZER64(              \
                  ((size_t)P99_NARG(__VA_ARGS__)) + 1,           \
                  ((uint64_t[]){ ORWL_OBJID(F), __VA_ARGS__ }) ) \
            )                                                    \
          )

#define o_rwl_rpc4(SRV, THERE, SEED, F)                        \
o_rwl_rpc(SRV, THERE, SEED, 1,                                 \
          &((orwl_buffer)ORWL_BUFFER_INITIALIZER64(            \
              SIZE_C(1),                                       \
              ((uint64_t[]){ ORWL_OBJID(F), }))                \
            )                                                  \
          )

/**
 ** @brief Lauch a remote procedure call with function @a F.
 **
 ** @msc
 **   caller,main,server,thread,procedure;
 **   main -> server [label="orwl_server_create()", URL="\ref orwl_server_create()"];
 **   caller -> server [label="orwl_send(F, ...)", URL="\ref orwl_send()"];
 **   server->thread [label="orwl_proc_create(F, ...)", URL="\ref orwl_proc_create()"];
 **   thread->procedure [label="F(...)"];
 **   procedure->caller [label="orwl_proc_untie_caller()", URL="\ref orwl_proc_untie_caller()"];
 **   procedure->thread [label="\c return"];
 **   thread->main [label="thrd_exit()"];
 ** @endmsc
 **/
#define orwl_rpc(SRV, THERE, SEED, ...)                        \
P99_IF_ELSE(P99_HAS_COMMA(__VA_ARGS__))                        \
(o_rwl_rpc5(SRV, THERE, SEED, __VA_ARGS__))                    \
(o_rwl_rpc4(SRV, THERE, SEED, __VA_ARGS__))

/**
 ** @}
 **/


#endif      /* !ORWL_ENDPOINT_H_ */
