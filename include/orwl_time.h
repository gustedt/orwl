/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the author and copyright holder for this work is                           */
/* all rights reserved,  2011-2012, 2014 Jens Gustedt, INRIA, France          */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#ifndef     ORWL_TIME_H_
# define    ORWL_TIME_H_

#include "orwl_posix.h"
#include "p99_atomic.h"
#include "p99_threads.h"
#include "p99_clib.h"

/**
 ** @file "orwl_time.h"
 ** @brief Handle different representations of time.
 **
 ** We have 4 different representations of time values
 ** - @b seconds represented by @c double
 ** - @b useconds, micro-seconds represented by @c uint64_t
 ** - <code>struct timeval</code>, breaking time down to micro-seconds
 ** - <code>struct timespec</code>, breaking time down to nano-seconds
 **
 ** We implement all 12 functions that convert between these different
 ** types.
 **/

/**
 ** @addtogroup library_support Compiler and system support features
 ** @{
 **/

#define O_RWL_DOCUMENT_TIME(S, T)                                                                                      \
/*! @brief Convert a time value from S to T. @see orwl_time.h for more information about time values and functions. */

#define ORWL_KILO UINT64_C(1000)
#define ORWL_MEGA UINT64_C(1000000)
#define ORWL_GIGA UINT64_C(1000000000)
#define ORWL_MILLI (+1.0E-3)
#define ORWL_MICRO (+1.0E-6)
#define ORWL_NANO (+1.0E-9)

O_RWL_DOCUMENT_TIME(seconds, timespec)
inline
struct timespec seconds2timespec(double t) {
  double const sec = trunc(t);
  const struct timespec ret = {
    .tv_sec = (time_t)sec,
    .tv_nsec = (time_t)((t - sec) * ORWL_GIGA)
  };
  return ret;
}

O_RWL_DOCUMENT_TIME(timespec, seconds)
inline
double timespec2seconds(struct timespec t) {
  double const ret = t.tv_sec + ORWL_NANO * t.tv_nsec;
  return ret;
}

O_RWL_DOCUMENT_TIME(seconds, timeval)
inline
struct timeval seconds2timeval(double t) {
  double const sec = trunc(t);
  const struct timeval ret = {
    .tv_sec = (time_t)sec,
    .tv_usec = (time_t)((t - sec) * ORWL_MEGA)
  };
  return ret;
}

O_RWL_DOCUMENT_TIME(timeval, seconds)
inline
double timeval2seconds(struct timeval t) {
  double const ret = t.tv_sec + ORWL_MICRO * t.tv_usec;
  return ret;
}

O_RWL_DOCUMENT_TIME(useconds, timespec)
inline
struct timespec useconds2timespec(uint64_t t) {
  uint64_t const sec = t / ORWL_MEGA;
  uint64_t const rem = t % ORWL_MEGA;
  const struct timespec ret = {
    .tv_sec = sec,
    .tv_nsec = rem * ORWL_KILO
  };
  return ret;
}

O_RWL_DOCUMENT_TIME(timespec, useconds)
inline
uint64_t timespec2useconds(struct timespec t) {
  uint64_t const ret = t.tv_sec * ORWL_MEGA + (t.tv_nsec / ORWL_KILO);
  return ret;
}

O_RWL_DOCUMENT_TIME(useconds, seconds)
inline
double useconds2seconds(uint64_t t) {
  return t * ORWL_MICRO;
}

O_RWL_DOCUMENT_TIME(seconds, useconds)
inline
uint64_t seconds2useconds(double t) {
  return (uint64_t)(t * ORWL_MEGA);
}

O_RWL_DOCUMENT_TIME(useconds, timeval)
inline
struct timeval useconds2timeval(uint64_t t) {
  uint64_t const sec = t / ORWL_MEGA;
  uint64_t const rem = t % ORWL_MEGA;
  const struct timeval ret = {
    .tv_sec = sec,
    .tv_usec = rem
  };
  return ret;
}

O_RWL_DOCUMENT_TIME(timeval, useconds)
inline
uint64_t timeval2useconds(struct timeval t) {
  uint64_t const ret = t.tv_sec * ORWL_MEGA + t.tv_usec;
  return ret;
}

O_RWL_DOCUMENT_TIME(timeval, timespec)
inline
struct timespec timeval2timespec(struct timeval t) {
  const struct timespec ret = {
    .tv_sec = t.tv_sec,
    .tv_nsec = t.tv_usec * ORWL_KILO
  };
  return ret;
}

O_RWL_DOCUMENT_TIME(timespec, timeval)
inline
struct timeval timespec2timeval(struct timespec t) {
  long usec = t.tv_nsec / ORWL_KILO;
  long rem = t.tv_nsec % ORWL_KILO;
  const struct timeval ret = {
    .tv_sec = t.tv_sec,
    .tv_usec = (rem < 500 ? usec : usec + 1)
  };
  return ret;
}

/**
 ** @brief Substract time @a b from @a a
 **/
inline
struct timespec* timespec_minus(struct timespec *a, struct timespec const *b) {
  a->tv_sec -= b->tv_sec;
  a->tv_nsec -= b->tv_nsec;
  if (a->tv_nsec < 0L) {
    a->tv_nsec += ORWL_GIGA;
    a->tv_sec--;
  }
  return a;
}

/**
 ** @brief Return the difference of times @a earlier and @a later.
 **/
inline
struct timespec timespec_diff(struct timespec earlier, struct timespec later) {
  timespec_minus(&later, &earlier);
  return later;
}

/**
 ** @brief Add time @a b to @a a
 **/
inline
struct timespec* timespec_add(struct timespec *a, struct timespec const *b) {
  a->tv_sec += b->tv_sec;
  a->tv_nsec += b->tv_nsec;
  if (a->tv_nsec >= ORWL_GIGA) {
    a->tv_nsec -= ORWL_GIGA;
    a->tv_sec++;
  }
  return a;
}

/**
 ** @brief Return the sum of times @a a and @a b.
 **/
inline
struct timespec timespec_sum(struct timespec a, struct timespec b) {
  timespec_add(&a, &b);
  return a;
}

/* implement the BSD timer arithmetic */
#if !defined(timercmp) || defined(DOXYGEN)
# define O_RWL_BSD_TIMER_INTERFACES
inline
struct timeval const* timeradd(struct timeval const *a,
                               struct timeval const *b,
                               struct timeval *res) {
  if (!a || !b || !res) return 0;
  res->tv_sec = a->tv_sec + b->tv_sec;
  res->tv_usec = a->tv_usec + b->tv_usec;
  if (res->tv_usec >= ORWL_MEGA) {
    res->tv_usec -= ORWL_MEGA;
    res->tv_sec++;
  }
  return res;
}

inline
struct timeval const* timersub(struct timeval const *a,
                               struct timeval const *b,
                               struct timeval *res) {
  if (!a || !b || !res) return 0;
  res->tv_sec = a->tv_sec - b->tv_sec;
  if (a->tv_usec >= b->tv_usec) {
    res->tv_usec = a->tv_usec - b->tv_usec;
  } else {
    res->tv_usec = (ORWL_MEGA - b->tv_usec) + a->tv_usec;
    res->tv_sec--;
  }
  return res;
}

inline
void timerclear(struct timeval *tvp) {
  if (tvp) *tvp = P99_LVAL(struct timeval);
}

inline
int timerisset(struct timeval const*tvp) {
  return tvp && (tvp->tv_sec || tvp->tv_usec);
}

inline
int timercmp0(struct timeval const*a) {
  if (!a) return INT_MAX;
  if (a->tv_sec) return a->tv_sec < 0 ? -1 : +1;
  else return a->tv_usec;
}

inline
int timercmp(struct timeval const*a,
             struct timeval const*b) {
  return timercmp0(timersub(a, b, &P99_LVAL(struct timeval)));
}

# define timercmp(A, B, CMP) (timercmp2((A), (B)) CMP 0)

#endif

char const* orwl_seconds2str(double sec, char tmp[static 32]);

#define orwl_seconds2str(...) P99_CALL_DEFARG(orwl_seconds2str, 2, __VA_ARGS__)
#define orwl_seconds2str_defarg_1() (char[32])P99_INIT

char const* orwl_bytes2str(uint64_t number, char tmp[static 32]);

#define orwl_bytes2str(...) P99_CALL_DEFARG(orwl_bytes2str, 2, __VA_ARGS__)
#define orwl_bytes2str_defarg_1() (char[32])P99_INIT


#if defined(TIME_MONOTONIC) || defined(DOXYGEN)
/**
 ** @brief the clock that ORWL uses for timings
 **
 ** It uses ::TIME_MONOTONIC if available and falls back to
 ** ::TIME_UTC.
 **/
# define ORWL_TIME_BASE TIME_MONOTONIC
#else
# define ORWL_TIME_BASE TIME_UTC
#endif

/**
 ** @brief get the current time in nanosecond precision.
 ** @see ORWL_TIME_BASE
 **/
inline
struct timespec orwl_gettime(void) {
  struct timespec t;
  timespec_get(&t, ORWL_TIME_BASE);
  return t;
}

P99_DECLARE_ONCE_CHAIN(orwl_gettime);


/**
 ** @brief Get the micro-seconds since epoch.
 ** @see orwl_time.h for more information about time values and functions.
 **/
inline
uint64_t useconds(void) {
  struct timespec t = orwl_gettime();
  return timespec2useconds(t);
}

/**
 ** @brief Get the seconds since epoch.
 ** @see orwl_time.h for more information about time values and functions.
 **/
inline
double seconds(void) {
  struct timespec t = orwl_gettime();
  return timespec2seconds(t);
}

/**
 ** @brief Let the calling thread rest for @a t seconds
 **
 ** The sleep time for this function includes the time that the thread
 ** is stopped by a signal.
 ** @see orwl_time.h for more information about time values and functions.
 **/
extern void sleepfor(double t);

/**
 ** @}
 **/

#endif      /* !ORWL_TIME_H_ */
