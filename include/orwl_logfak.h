// This may look like C code, but it really is -*- mode: c++; coding: utf-8 -*-
#ifndef ORWL_LOGFAK_H
#define ORWL_LOGFAK_H

/// @file

#include <math.h>
#include <stddef.h>

enum { o_rwl_logfak_size = 30, };

extern double const o_rwl_logfak[o_rwl_logfak_size];

inline
double orwl_flogfak (size_t k) {
  if (k < o_rwl_logfak_size) return (o_rwl_logfak[k]);

  double const C0 = 9.18938533204672742e-01;
  double const C1 = 8.33333333333333333e-02;
  double const C3 = -2.77777777777777778e-03;
  double const C5 = 7.93650793650793651e-04;
  double const C7 = -5.95238095238095238e-04;

  register double dk = k;
  register double r = 1.0/dk;
  register double rr = r*r;
  dk = (dk + 0.5) * log(dk) - dk + C0;
  return ( dk +
           r * (C1 + rr * (C3 + rr * (C5 + rr * C7))));
}



#endif
