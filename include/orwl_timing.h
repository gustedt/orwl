/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the authors and copyright holders for this work are as follows:            */
/* all rights reserved,  2011 Emmanuel Jeanvoine, INRIA, France               */
/* all rights reserved,  2011-2014 Jens Gustedt, INRIA, France                */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#ifndef     ORWL_TIMING_H_
#define     ORWL_TIMING_H_

#if !defined(ATOMIC_VAR_INIT) && defined(__GNUC__)
# include "p99_atomic.h"
#endif
#include "orwl_rand.h"
#include P99_ADVANCE_ID

/**
 ** @brief Determine if and how the timing statistics should be
 ** printed at the end of an ORWL run.
 **
 ** If this environment variable is defined, the timing information
 ** will be collected and printed at the end of the run.
 **
 ** If the value of this variable is anything different from the empty
 ** string, the print out is done in "human readable" form. That is
 ** all times and other resources are scaled with the appropriate
 ** factors, e.g <code>MiB</code> for mega byte or <code>M s</code>
 ** for million seconds.
 **/
ORWL_DECLARE_ENV(ORWL_TIMING_TYPE);

ORWL_DECLARE_ENV(ORWL_TIMING_CALIBRATE);

/**
 ** @addtogroup library_support
 ** @{
 **/

P99_DECLARE_STRUCT(orwl_stat);
struct orwl_stat {
  atomic_flag p00_cat;
  size_t p00_cnt;
  size_t p00_nb;
  double p00_time;
  double p00_time2;
  double p00_min;
  double p00_max;
};

#define ORWL_STAT_INTIALIZER { .p00_min = HUGE_VAL, .p00_max = -HUGE_VAL, }

P99_DECLARE_ATOMIC(orwl_stat);

P99_DECLARE_ONCE_CHAIN(orwl_timing);

inline
void orwl_stat_add(orwl_stat * p00_el, size_t p00_cnt, double p00_time) {
  P99_INIT_CHAIN(orwl_timing);
  P99_SPIN_EXCLUDE(&p00_el->p00_cat) {
    p00_el->p00_cnt += p00_cnt;
    ++p00_el->p00_nb;
    p00_el->p00_time += p00_time;
    p00_el->p00_time2 += p00_time * p00_time;
    if (p00_time < p00_el->p00_min) p00_el->p00_min = p00_time;
    if (p00_time > p00_el->p00_max) p00_el->p00_max = p00_time;
  }
}

P99_DECLARE_STRUCT(orwl_timing_element);
P99_POINTER_TYPE(orwl_timing_element);
P99_LIFO_DECLARE(orwl_timing_element_ptr);

enum { p00_t_prev = 0, p00_t_elem = 0, };

struct orwl_timing_element {
  char const*const p00_name;
  p99_once_flag p00_flag;
  orwl_timing_element* p00_up;
  orwl_timing_element_ptr p99_lifo;
  orwl_stat p00_stats;
};

#define ORWL_TIMING_ELEMENT_INITIALIZER(NAME)                  \
{                                                              \
  .p00_name = #NAME,                                           \
  .p00_stats = ORWL_STAT_INTIALIZER,                           \
    }

void orwl_timing_element_insert(void* );

P99_DECLARE_STRUCT(orwl_timing);

/** @brief These are the names of timing elements that can be used in
 ** inline functions.
 **
 ** The naming convention is just to have them the same as the
 ** function to which they correspond, with the "orwl_" prefix
 ** omitted.
 ** @see ORWL_TIMING
 **/
#define ORWL_TIMING_LIST                                       \
  /* overall stats */                                          \
  _orwl,                                                       \
    recv,                                                      \
    callback,                                                  \
    thread,                                                    \
    fd,                                                        \
  /* library functions */                                      \
  acquire,                                                     \
    handle_attach,                                             \
    handle_replace,                                            \
    handle_hold,                                               \
    test,                                                      \
    write_map,                                                 \
    read_map,                                                  \
    truncate

#define O_RWL_FIELD(NAME, X, I) NAME* X

#define O_RWL_FIELDS(NAME, ...) P99_FOR(NAME, P99_NARG(__VA_ARGS__), P00_SEP, O_RWL_FIELD, __VA_ARGS__)

/* This struct is used to declare timing elements that are supposed to
*  be used in inline functions */
struct orwl_timing {
  O_RWL_FIELDS(orwl_timing_element, ORWL_TIMING_LIST);
};


orwl_timing * orwl_timing_info(void);

/** @brief Print the statistics that are collected during an ORWL
 ** execution.
 ** @see ORWL_TIMING */
void orwl_timing_print_stats(void);

enum { orwl_timing_var = 0 };

extern _Atomic(size_t) o_rwl_timing_recv;
extern _Atomic(int) o_rwl_timing_fd;

/**
 ** @def ORWL_TIMING(NAME, COUNT)
 ** @brief Timing of a statement or block
 **
 ** @remark Use this in inline functions that don't allow to declare
 ** @c static variables. For other places use ::ORWL_TIMER.
 **
 ** This macro is used as a prefix of a particular statement or block
 ** of code.
 **
 ** @param NAME must correspond to one of the resources that are
 ** listed in the ::orwl_timing.
 ** @param COUNT (default = 1) is a resource count that can be associated to this timer.
 ** It is evaluated @em after the depending statement or block.
 **
 ** Care is taken that the function calls are optimized
 ** - ::orwl_timing_info is only called once per block. When timing is
 **   used again inside a block that is itself timed no new call to
 **   that function is issued
 ** - the two calls to ::orwl_gettime are as close as possible to the
 **   application code. I.e we have something like a sequence
 **   (0) ::orwl_timing_info()
 **   (1) ::orwl_gettime()
 **   (2) application code
 **   (3) ::orwl_gettime()
 **   (4) update of statistics
 **
 ** The update of the statistics are done with atomic operations such
 ** that all of this can be done without danger in a threaded
 ** environment.
 **
 ** @pre for this mechanism to be activated ::ORWL_USE_TIMING must be
 ** defined on the compiler command line, e.g by passing the option
 ** <code>-DORWL_USE_TIMING</code> to the compiler.
 **
 ** If an environment variable with the same name
 ** <code>ORWL_TIMING</code> is present, ::orwl_timing_print_stats
 ** will be called automatically at the end of the ORWL execution.  If
 ** this variable also has a content a "human" output format is used
 ** for the collected timings.
 **/



#ifdef ORWL_USE_TIMING
# define O_RWL_TIMING(NAME, COUNT)                                                          \
P00_BLK_START                                                                               \
P00_BLK_DECL(register orwl_timing*const, _timing,                                           \
             (orwl_timing_var ? orwl_timing_var : orwl_timing_info()))                      \
  P00_BLK_DECL(register orwl_timing*const, orwl_timing_var, _timing)                        \
  P00_BLK_DECL(register orwl_timing_element*const, o_rwl_timing_var, orwl_timing_var->NAME) \
  P00_BLK_DECL(struct timespec, p00_start, orwl_gettime())                                  \
  P00_BLK_AFTER(orwl_stat_add(&(o_rwl_timing_var->p00_stats),                               \
                              (COUNT),                                                      \
                              timespec2seconds(timespec_diff(p00_start, orwl_gettime()))))  \
  P00_BLK_END
# define ORWL_TIMING(...) P99_IF_LT(P99_NARG(__VA_ARGS__), 2)(O_RWL_TIMING(__VA_ARGS__, 0))(O_RWL_TIMING(__VA_ARGS__))
#else
# define ORWL_TIMING(...)
#endif

/**
 ** @def ORWL_TIMER(NAME, COUNT)
 ** @brief Timing of a statement or block
 **
 ** @param NAME should be token but no identifier is declared or
 ** supposed to exist with that name.
 ** @param COUNT (default = 1) is a resource count that can be associated to this timer.
 ** It is evaluated @em after the depending statement or block.
 **
 ** This macro is used as a prefix of a particular statement or block
 ** of code.
 **
 ** Care is taken that the function calls are optimized
 ** - A static initialization is only run the first time that this
 **   place is encountered.
 ** - the two calls to ::orwl_gettime are as close as possible to the
 **   application code. I.e we have something like a sequence
 **   (1) ::orwl_gettime()
 **   (2) application code
 **   (3) ::orwl_gettime()
 **   (4) update of statistics
 **
 ** The update of the statistics are done with atomic operations such
 ** that all of this can be done without danger in a threaded
 ** environment.
 **
 ** @see ORWL_TIMING when the context doesn't allow to declare @c
 ** static variables, in particular @c inline functions.
 **/

#ifdef ORWL_USE_TIMING
# define O_RWL_TIMER_(ID, NAME, COUNT)                                                                    \
P00_BLK_START                                                                                             \
P00_BLK_BEFORE(register orwl_timing_element*const p00_t_prev = p00_t_elem)                                \
  P00_BLK_BEFORE(register orwl_timing_element* p00_t_elem = 0)                                            \
  P99_PREFER(                                                                                             \
             static orwl_timing_element ID[2]                                                             \
             = { ORWL_TIMING_ELEMENT_INITIALIZER(NAME), { .p00_name = __func__ } };                       \
             P99_INIT_CHAIN(orwl_timing);                                                                 \
             p99_call_once(&ID[0].p00_flag,                                                               \
                           orwl_timing_element_insert,                                                    \
                           &(orwl_timing_element*[3]){                                                    \
                             &ID[0],                                                                      \
                               p00_t_prev,                                                                \
                               &ID[1],                                                                    \
                               });                                                                        \
             p00_t_elem = &ID[0];                                                                         \
             goto ID;                                                                                     \
             ) ID:                                                                                        \
                 P00_BLK_DECL(struct timespec, p00_start, orwl_gettime())                                 \
                 P00_BLK_AFTER(orwl_stat_add(&(p00_t_elem->p00_stats),                                    \
                                             (COUNT),                                                     \
                                             timespec2seconds(timespec_diff(p00_start, orwl_gettime())))) \
                 P99_BLK_MARK(ID)                                                                         \
     P00_BLK_END
# define O_RWL_TIMER(NAME, COUNT) O_RWL_TIMER_(P99_UNIQ(timer, NAME), NAME, COUNT)
# define ORWL_TIMER(...) P99_IF_LT(P99_NARG(__VA_ARGS__), 2)(O_RWL_TIMER(__VA_ARGS__, 0))(O_RWL_TIMER(__VA_ARGS__))
#elif defined(DOXYGEN)
# define ORWL_TIMER(NAME, COUNT)
# define ORWL_TIMING(NAME, COUNT)
#else
# define ORWL_TIMER(...)
#endif

/**
 ** @}
 **/

#endif      /* !ORWL_TIMING_H_ */
