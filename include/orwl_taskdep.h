/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the authors and copyright holders for this work are as follows:            */
/* all rights reserved,  2018 Jens Gustedt, INRIA, France                     */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#ifndef     ORWL_TASKDEP_H_
#define     ORWL_TASKDEP_H_

/**
 ** @file
 **
 ** @brief Auxiliary declarations that are needed for the automatic
 ** taskgraph generation.
 **/

#ifdef NORWL
#include <stdint.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#else
# include "orwl.h"
# include "orwl_alloc_queue.h"
# include "p99_getopt.h"
#endif

// Interface with Fortran

// Mangling that is done for symbols visible to Fortran
#define ORWL_TASKDEP_FORT(X) X ## _

// Define a Fortran alias of  variable N.
#define ORWL_TASKDEP_ALIAS(N) extern __typeof__(N) ORWL_TASKDEP_FORT(N) __attribute__((__alias__(#N)))

#ifndef NORWL
P99_GETOPT_DECLARE(v, bool, orwl_taskdep_verbose, false, "verbose", "verbose output");
#endif

extern int64_t const orwl_taskdep_nodes;
extern int64_t const orwl_taskdep_max_inp;
extern int64_t const orwl_taskdep_max_out;

extern char const*const orwl_taskdep_nnames[];
extern char const*const orwl_taskdep_snames[];
extern char const*const orwl_taskdep_fnames[];
extern char const*const orwl_taskdep_dnames[];
extern int64_t const orwl_taskdep_deg_out[];
extern int64_t const orwl_taskdep_deg_inp[];
extern int64_t const*const orwl_taskdep_onum[];
extern int64_t const*const orwl_taskdep_inum[];

extern int64_t orwl_taskdep_status_;
extern int64_t orwl_taskdep_id_out_[];
extern int64_t orwl_taskdep_id_inp_[];

#ifndef NORWL
// An internal type to transfer startup data.
P99_DECLARE_STRUCT(orwl_taskdep_type);

P99_DEFINE_STRUCT(orwl_taskdep_type,
   int argc,
   char** argv
);
#endif

typedef union orwl_taskdep_blob orwl_taskdep_blob;
union orwl_taskdep_blob {
  // Just provide Fortran functions with a large enough memory location
  // such that they can store a fat pointer or whatever.
  _Alignas(16) unsigned char data[64];
  // C should just use this as generic pointer array
  void* pdata[64/sizeof(void*)];
};

#ifndef NORWL
extern orwl_taskdep_type* orwl_taskdep_type_init(orwl_taskdep_type* task, int argc, char* argv[argc+1]);
extern void orwl_taskdep_type_destroy(orwl_taskdep_type *task);


P99_DECLARE_DELETE(orwl_taskdep_type);
#endif

extern void orwl_taskdep_start(orwl_taskdep_blob state[static restrict 1], int64_t const tid[static 1]);

extern void orwl_taskdep_shutdown(orwl_taskdep_blob state[static restrict 1], int64_t const tid[static 1]);

extern void orwl_taskdep_usage(void);


#endif
