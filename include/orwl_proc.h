/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the author and copyright holder for this work is                           */
/* all rights reserved,  2010-2013 Jens Gustedt, INRIA, France                */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#ifndef     ORWL_PROC_H_
# define    ORWL_PROC_H_

#include "orwl_register.h"
#include "orwl_header.h"

/**
 ** @addtogroup library_low
 ** @{
 **/

/**
 ** @brief Negotiate a send request with remote endpoint @a ep.
 **/
uint64_t orwl_send(orwl_server *srv, orwl_endpoint const* there, p99_seed *seed, size_t n, orwl_buffer mess[n]);

/**
 ** @brief Data for a procedure call
 **
 ** These can either be remote, with data that is authenticated
 ** through the ::orwl_server mechanism. Or they are called locally
 ** when during the processing of the call request that possibility is
 ** detected.
 **
 ** @see orwl_server
 ** @see orwl_rpc
 ** @see orwl_proc_init
 ** @see orwl_proc_destroy
 ** @see orwl_proc_untie_caller
 ** @see orwl_proc_create
 ** @see orwl_proc_join
 ** @see orwl_proc_insert_peer
 ** @see orwl_proc_insert_host
 **/
struct orwl_proc {
  uint64_t ret;            /*!< a place to store the return value of
                             the call, poisoned if not set */
  /* the messages */
  size_t n;
  orwl_buffer *mes;           /*!< the messages itself */
  /* internal control fields */
  struct orwl_server* srv; /*!< the server through which we received this socket */
  bool is_untied;          /*!< orwl_proc_untie_caller has been called once */
  orwl_thread_cntrl* det;  /*!< non-null if we are local */
  int fd;                  /*!< the open file descriptor */
  bool async;              /*!< represents an asynchronous communication */
  uint64_t remoteorder;    /*!< the byte order on the remote host */
};

P99_DECLARE_ONCE_CHAIN(orwl_proc);

#ifndef DOXYGEN
inline
P99_PROTOTYPE(orwl_proc*, orwl_proc_init, orwl_proc *, int, struct orwl_server*, bool, uint64_t, size_t, orwl_buffer*, orwl_thread_cntrl*);
#define orwl_proc_init(...) P99_CALL_DEFARG(orwl_proc_init, 8, __VA_ARGS__)
#define orwl_proc_init_defarg_1() -1
#define orwl_proc_init_defarg_2() P99_0(struct orwl_server*)
#define orwl_proc_init_defarg_3() false
#define orwl_proc_init_defarg_4() P99_0(uint64_t)
#define orwl_proc_init_defarg_5() 0
#define orwl_proc_init_defarg_6() 0
#define orwl_proc_init_defarg_7() P99_0(orwl_thread_cntrl*)
#endif

DOCUMENT_INIT(orwl_proc)
P99_DEFARG_DOCU(orwl_proc_init)
inline
orwl_proc*
orwl_proc_init(orwl_proc *proc,         /*!< [out] */
               int fd,                  /*!< [in] file descriptor, defaults to -1 */
               struct orwl_server* srv, /*!< [in,out] defaults to a null pointer */
               bool async,              /*!< [in] the synchronization type */
               uint64_t remo,           /*!< [in] the byte order on remote */
               size_t n,                /*!< [in] the amount of splices of the message */
               orwl_buffer m[n],        /*!< [in] the messages or 0 */
               orwl_thread_cntrl *det   /*!< [in] non 0 if a local connection */
              ) {
  if (proc) {
    orwl_buffer * mes = orwl_buffer_vnew(n);
    for (size_t i = 0; i < n; ++i) {
      orwl_alloc* nalloc =
        (!m[i].aio.aio_buf && m[i].aio.aio_nbytes) ? P99_NEW(orwl_alloc, 0) : 0;
      mes[i].aio = (aiocb)ORWL_AIOCB_INITIALIZER(
                     m[i].aio.aio_nbytes,
                     nalloc
                     ? orwl_alloc_realloc(nalloc, sizeof(uint64_t[m[i].aio.aio_nbytes]))
                     : m[i].aio.aio_buf);
      if (nalloc) {
        orwl_alloc_ref_replace(&mes[i].allocr, nalloc);
      }
      orwl_alloc* oalloc = orwl_alloc_ref_get(&m[i].allocr);
      if (oalloc) {
        orwl_alloc_ref_assign(&mes[i].allocr, &m[i].allocr);
      }
      if (nalloc && oalloc) P99_THROW(EINVAL);
    }
    *proc = P99_LVAL(orwl_proc const,
                     .ret = -1, // poison the return value
                     .fd = fd,
                     .srv = srv,
                     .async = async,
                     .remoteorder = remo,
                     .n = n,
                     .mes = mes,
                     .det = det,
                    );
  }
  return proc;
}

DOCUMENT_DESTROY(orwl_proc)
void orwl_proc_destroy(orwl_proc *proc);
P99_DECLARE_DELETE(orwl_proc);
DECLARE_THREAD(orwl_proc);

#ifndef DOXYGEN
#define orwl_proc_create(...) P99_CALL_DEFARG(orwl_proc_create, 2, __VA_ARGS__)
#define orwl_proc_create_defarg_1() P99_0(thrd_t*)
#endif

/**
 ** @related orwl_proc
 **/
void orwl_proc_untie_caller(orwl_proc *proc);

#ifdef DOXYGEN
#define DECLARE_ORWL_PROC_FUNC(F, ...)                                                                      \
/*! @brief the number of @c uint64_t that F uses for control @see F */                                      \
enum { P99_PASTID2(F, header) = P99_NARG(__VA_ARGS__) + 1 };                                                \
/*! An ::orwl_proc function interpreting a message received on a socket. */                                 \
/*! It interprets the message it receives as if it where declared @code uint64_t F(__VA_ARGS__) @endcode */ \
/*! @see ORWL_PROC_READ is used to interpret the message as specified */                                    \
/*! @see  P99_PASTID2(F, header)*/                                                                          \
/*! @related orwl_proc */                                                                                   \
void F(orwl_proc *Arg)
#define DEFINE_ORWL_PROC_FUNC(F, ...)                          \
void F(orwl_proc *Arg)
#else
#define DEFINE_ORWL_PROC_FUNC(F, ...)                          \
void P99_PASTID2(F, proto)(__VA_ARGS__) { }                    \
DEFINE_ORWL_REGISTER_ALIAS(F, orwl_proc);                      \
void F(orwl_proc *Arg)

#define DECLARE_ORWL_PROC_FUNC(F, ...)                         \
enum { P99_PASTID2(F, header) = P99_NARG(__VA_ARGS__) };       \
void P99_PASTID2(F, proto)(__VA_ARGS__);                       \
void F(orwl_proc *Arg);                                        \
DECLARE_ORWL_REGISTER_ALIAS(F, orwl_proc)
#endif

DECLARE_ORWL_TYPE_DYNAMIC(orwl_proc);


#define ORWL_PROC_READ(A, F, ...)                                                                \
/* Assert that F has the correct type. */                                                        \
(void)((void (*)(orwl_proc*)){ F });                                                             \
/* Assert that the argument list has the correct types. */                                       \
(void)((void (*)(__VA_ARGS__)){ P99_PASTID2(F, proto) });                                        \
/* Assert that mes[0] has enough elements */                                                     \
P99_THROW_ASSERT(ENOBUFS, (A)->mes[0].aio.aio_nbytes >= P99_NARG(__VA_ARGS__)*sizeof(uint64_t)); \
P99_VASSIGNS(((uint64_t*)(A)->mes[0].aio.aio_buf), __VA_ARGS__);                                 \
orwl_buffer_advance((A)->mes, P99_NARG(__VA_ARGS__))

/*! @brief an accessor function */
/*! @related orwl_proc */
orwl_addr orwl_proc_getpeer(orwl_proc const*Arg);

/**
 ** @}
 **/


#endif      /* !ORWL_PROC_H_ */
