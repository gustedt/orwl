/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the author and copyright holder for this work is                           */
/* all rights reserved,  2010-2013, 2016 Jens Gustedt, INRIA, France          */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#ifndef     ORWL_HANDLE2_H_
# define    ORWL_HANDLE2_H_


#include "orwl_remote_queue.h"
#include P99_ADVANCE_ID

/**
 ** @addtogroup library_iterative
 ** @{
 **/

/**
 ** @brief A handle for periodic tasks
 **
 ** This knows basically two states, orwl_requested and orwl_acquired. If
 ** several handles are inserted into the same location they will
 ** always be acquired in the same cyclic order.
 ** @see orwl_handle2_acquire blocks until the state is orwl_acquired
 ** @see orwl_test can be used to know if an orwl_handle2 has been
 ** acquired asynchronously
 ** @see orwl_handle2_next inserts the handle at the end of the queue and
 ** frees the front of it.
 **/
struct orwl_handle2 {
  /** @privatesection
   ** @{
   **/

  /** @brief is this an inclusive (read) request? */
  bool inclusive:1;
  /**
   ** @brief a time stamp to alternate according to its parity
   **
   ** This may just be an @c unsigned (and therefore a bit field)
   ** since we are usually only interested by the parity. Having it on
   ** some bits more might come handy when debugging, though.
   **/
unsigned clock:(sizeof(unsigned)*CHAR_BIT - 1);
  /** the last known state of pair[i] */
  orwl_state state[2];
  /** the two handles that are used in alternation */
  orwl_handle pair[2];

  /**
   ** @}
   **/

};

#define ORWL_HANDLE2_INITIALIZER                               \
{                                                              \
  .inclusive = false,                                          \
    .clock = 1,                                                \
    .state = { [0] = orwl_invalid, [1] = orwl_invalid },       \
    .pair = {                                                  \
    [0] = ORWL_HANDLE_INITIALIZER,                             \
    [1] = ORWL_HANDLE_INITIALIZER,                             \
  }                                                            \
}

/**
 ** @brief Dynamically initialize a ::orwl_handle2
 **
 ** @param rh2 is the handle to be initialized
 **/
DOCUMENT_INIT(orwl_handle2)
inline
orwl_handle2 *orwl_handle2_init(orwl_handle2 *rh2) {
  if (!rh2) return 0;
  rh2->inclusive = false;
  rh2->clock = 1;
  rh2->state[0] = orwl_invalid;
  rh2->state[1] = orwl_invalid;
  orwl_handle_init(&rh2->pair[0]);
  orwl_handle_init(&rh2->pair[1]);
  return rh2;
}

DOCUMENT_DESTROY(orwl_handle2)
inline
void orwl_handle2_destroy(orwl_handle2 *rh2) {
  orwl_handle_destroy(&rh2->pair[0]);
  orwl_handle_destroy(&rh2->pair[1]);
  rh2->inclusive = false;
  rh2->clock = 0;
}


DECLARE_NEW_DELETE(orwl_handle2);

/**
 ** @related orwl_mirror
 **
 ** @brief Link a set of handles @a rh2 that will be used in an iterative
 ** procedure to a set of locations @a location and request exclusive access.
 ** @see orwl_write_request
 **/
O_RWL_DOCUMENT_VECTOR
O_RWL_DOCUMENT_SEED
P99_DEFARG_DOCU(orwl_handle2_write_request)
orwl_state orwl_handle2_write_request(orwl_mirror location[],/*!< [in,out] the location(s) for the request */
                                      orwl_handle2 rh2[],    /*!< [in,out] the handle(s) for the request */
                                      size_t size,           /*!< [in] vector size, defaults to 1 */
                                      p99_seed* seed         /*!< [in,out] defaults to a thread local seed */
                                     );

/**
 ** @related orwl_mirror
 **
 ** @brief Link a set of handles @a rh2 that will be used in an iterative
 ** procedure to a set of locations @a location and request shared access.
 ** @see orwl_read_request
 **/
O_RWL_DOCUMENT_VECTOR
O_RWL_DOCUMENT_SEED
P99_DEFARG_DOCU(orwl_handle2_read_request)
orwl_state orwl_handle2_read_request(orwl_mirror location[],/*!< [in,out] the location(s) for the request */
                                     orwl_handle2 rh2[],    /*!< [in,out] the handle(s) for the request */
                                     size_t size,           /*!< [in] vector size, defaults to 1 */
                                     p99_seed* seed         /*!< [in,out] defaults to a thread local seed */
                                    );
/**
 ** @related orwl_mirror
 ** @see orwl_write_insert
 **/
O_RWL_DOCUMENT_SEED
P99_DEFARG_DOCU(orwl_handle2_write_request)
void orwl_handle2_write_insert(orwl_handle2 * rh2,    /*!< [in,out] the handle(s) for the request */
                               size_t locid,          /*!< defaults to the id of the current tasks's principal location */
                               size_t priority,       /*!< defaults to the id of the current tasks */
                               p99_seed* seed,        /*!< [in,out] defaults to a thread local seed */
                               orwl_server * srv,
                               orwl_mirror * location /*!< [in,out] the location(s) for the request */
                              );

/**
 ** @related orwl_mirror
 ** @see orwl_read_insert
 **/
O_RWL_DOCUMENT_SEED
P99_DEFARG_DOCU(orwl_handle2_read_request)
void orwl_handle2_read_insert(orwl_handle2 * rh2,      /*!< [in,out] the handle(s) for the request */
                              size_t locid,
                              size_t priority,        /*!< defaults to the id of the current tasks */
                              p99_seed* seed,         /*!< [in,out] defaults to a thread local seed */
                              orwl_server * srv,
                              orwl_mirror * location  /*!< [in,out] the location(s) for the request */
                             );

/**
 ** @related orwl_handle2
 **
 ** @brief Release the access to the location(s) for the current iteration.
 **
 ** @see orwl_handle2_acquire
 **/
O_RWL_DOCUMENT_VECTOR
O_RWL_DOCUMENT_SEED
P99_DEFARG_DOCU(orwl_handle2_next)
orwl_state orwl_handle2_next(orwl_handle2 rh2[],    /*!< [in,out] the handle(s) to be released */
                             size_t size,           /*!< [in] vector size, defaults to 1 */
                             p99_seed* seed         /*!< [in,out] defaults to a thread local seed */
                            );


/**
 ** @related orwl_handle2
 **/
O_RWL_DOCUMENT_SEED
P99_DEFARG_DOCU(orwl_forced_cancel2)
orwl_state orwl_forced_cancel2(orwl_handle2* rh2,   /*!< [in,out] the handle to be canceled */
                               p99_seed* seed         /*!< [in,out] defaults to a thread local seed */
                              );

/**
 ** @related orwl_handle2
 **
 ** @brief Gain access to the location(s) for the current iteration.
 **
 ** @see orwl_handle2_next
 **/
O_RWL_DOCUMENT_VECTOR
O_RWL_DOCUMENT_SEED
P99_DEFARG_DOCU(orwl_handle2_acquire)
orwl_state orwl_handle2_acquire(orwl_handle2 rh2[],    /*!< [in,out] the handle(s) to be acquired */
                                size_t size,           /*!< [in] vector size, defaults to 1 */
                                p99_seed* seed         /*!< [in,out] defaults to a thread local seed */
                               );


/**
 ** @related orwl_handle2
 **
 ** @brief Dissolve the handle @a rh2 from its location.
 **
 ** @remark This introduces a final synchronization of the calling
 ** thread. The effect is that the thread waits until the lock is
 ** acquired and then releases it without placing a new item in the
 ** queue of the location.
 **/
O_RWL_DOCUMENT_VECTOR
O_RWL_DOCUMENT_SEED
P99_DEFARG_DOCU(orwl_handle2_disconnect)
orwl_state orwl_handle2_disconnect(orwl_handle2 rh2[],    /*!< [in,out] the handle(s) to be disconnected */
                                   size_t size,           /*!< [in] vector size, defaults to 1 */
                                   p99_seed* seed         /*!< [in,out] defaults to a thread local seed */
                                  );


/**
 ** @related orwl_handle2
 **
 ** @brief Gain access to the location(s) for the current iteration if
 ** this can be obtained without blocking.
 **
 ** @see orwl_test
 **/
O_RWL_DOCUMENT_VECTOR
O_RWL_DOCUMENT_SEED
P99_DEFARG_DOCU(orwl_handle2_test)
orwl_state orwl_handle2_test(orwl_handle2 rh2[],    /*!< [in,out] the handle(s) to be tested */
                             size_t size,           /*!< [in] vector size, defaults to 1 */
                             p99_seed* seed         /*!< [in,out] defaults to a thread local seed */
                            );

/**
 ** @related orwl_handle2
 **
 ** @brief Iteratively obtain a pointer to modify the data that is
 ** associated to a handle.
 **
 ** @pre This supposes that @a rh2 has required an exclusive access.
 **
 ** @return An address to access the data that is associated with the
 ** handle.
 **
 ** @throw EPERM If the handle @a rh only had been requested for a
 ** shared lock.
 **
 ** @throw EINVAL If the handle @a rh only had not been
 ** requested for any kind of lock.
 **
 ** @remark If @a rh2 has not yet acquired the access through
 ** ::orwl_handle2_acquire (or detected through ::orwl_test) this will be
 ** blocking until the access is granted.
 ** @see orwl_write_map
 **/
O_RWL_DOCUMENT_SEED
P99_DEFARG_DOCU(orwl_handle2_write_map)
void* orwl_handle2_write_map(orwl_handle2* rh2,   /*!< [in,out] the handle to be queried */
                             size_t* data_len,    /*!< [out] length of @a data in
                                       number of bytes, may be omitted */
                             p99_seed* seed       /*!< [in,out] defaults to a thread local seed */
                            );

/**
 ** @related orwl_handle2
 **
 ** @brief Iteratively obtain a pointer to consult the data that is
 ** associated to a handle.
 **
 ** @pre This supposes that @a rh2 has required an access to some
 ** location, be it exclusive or inclusive access.
 **
 ** @return An address to access the data that is associated with the
 ** handle.
 **
 ** @throw EINVAL If the handle @a rh only had not been
 ** requested for any kind of lock.
 **
 ** @remark If @a rh2 has not yet acquired the access through
 ** ::orwl_handle2_acquire (or detected through ::orwl_test) this will be
 ** blocking until the access is granted.
 ** @see orwl_read_map
 **/
O_RWL_DOCUMENT_SEED
P99_DEFARG_DOCU(orwl_handle2_read_map)
void* orwl_handle2_read_map(orwl_handle2* rh2,   /*!< [in,out] the handle to be queried */
                            size_t* data_len,    /*!< [out] length of @a data in
                                            number of bytes, may be omitted */
                            p99_seed* seed       /*!< [in,out] defaults to a thread local seed */
                           );


/**
 ** @related orwl_handle2
 **
 ** @brief Get the offset in bytes of data resource to which @a rh2
 ** links.
 **/
O_RWL_DOCUMENT_SEED
P99_DEFARG_DOCU(orwl_handle2_read_map)
size_t orwl_handle2_offset(orwl_handle2* rh2,   /*!< [in,out] the handle to be queried */
                           p99_seed* seed     /*!< [in,out] defaults to a thread local seed */
                          );


void orwl_handle2_replace(orwl_handle2* rh2, orwl_alloc_ref* alloc, p99_seed* seed);

/**
 ** @related orwl_handle2
 **
 ** @brief Obtain the data resource to which @a rh2 links.
 **/
O_RWL_DOCUMENT_SEED
P99_DEFARG_DOCU(orwl_handle2_read_map)
void orwl_handle2_hold(orwl_handle2* rh2, orwl_alloc_ref* ar, p99_seed* seed, bool mark);


/**
 ** @related orwl_handle2
 **
 ** @brief Change the size of the data that is associated to a handle.
 **
 ** @pre This supposes that @a rh2 has required an exclusive access.
 **
 ** @pre If @a rh2 has not yet acquired the access through
 ** ::orwl_handle2_acquire (or detected through ::orwl_test) this will fail.
 **
 ** @see orwl_truncate
 **/
O_RWL_DOCUMENT_SEED
P99_DEFARG_DOCU(orwl_handle2_truncate)
void orwl_handle2_truncate(orwl_handle2* rh2,   /*!< [in,out] the handle whos
                                          data to resize */
                           size_t data_len,     /*!< [in] future length of @a data in
                                         number of bytes */
                           p99_seed* seed       /*!< [in,out] defaults to a thread local seed */
                          );

#ifndef DOXYGEN
P99_PROTOTYPE(orwl_state, orwl_handle2_write_request, orwl_mirror*, orwl_handle2*, size_t, p99_seed*);
#define orwl_write_request2(...)  P99_CALL_DEFARG(orwl_handle2_write_request, 4, __VA_ARGS__)
#define orwl_handle2_write_request(...)  P99_CALL_DEFARG(orwl_handle2_write_request, 4, __VA_ARGS__)
#define orwl_handle2_write_request_defarg_2() 1u
#define orwl_handle2_write_request_defarg_3() p99_seed_get()

P99_PROTOTYPE(orwl_state, orwl_handle2_read_request, orwl_mirror*, orwl_handle2*, size_t, p99_seed*);
#define orwl_read_request2(...)  P99_CALL_DEFARG(orwl_handle2_read_request, 4, __VA_ARGS__)
#define orwl_handle2_read_request(...)  P99_CALL_DEFARG(orwl_handle2_read_request, 4, __VA_ARGS__)
#define orwl_handle2_read_request_defarg_2() 1u
#define orwl_handle2_read_request_defarg_3() p99_seed_get()

P99_PROTOTYPE(void, orwl_handle2_write_insert, orwl_handle2*, size_t, size_t, p99_seed*, orwl_server*, orwl_mirror*);
#define orwl_handle2_write_insert(...)  P99_CALL_DEFARG(orwl_handle2_write_insert, 6, __VA_ARGS__)
#define orwl_handle2_write_insert_defarg_1() SIZE_MAX
#define orwl_handle2_write_insert_defarg_2() SIZE_MAX
#define orwl_handle2_write_insert_defarg_3() p99_seed_get()
#define orwl_handle2_write_insert_defarg_4() orwl_server_get()
#define orwl_handle2_write_insert_defarg_5() P99_RVAL(orwl_mirror*)

P99_PROTOTYPE(void, orwl_handle2_read_insert, orwl_handle2*, size_t, size_t, p99_seed*, orwl_server*, orwl_mirror*);
#define orwl_handle2_read_insert(...)  P99_CALL_DEFARG(orwl_handle2_read_insert, 6, __VA_ARGS__)
#define orwl_handle2_read_insert_defarg_2() SIZE_MAX
#define orwl_handle2_read_insert_defarg_3() p99_seed_get()
#define orwl_handle2_read_insert_defarg_4() orwl_server_get()
#define orwl_handle2_read_insert_defarg_5() P99_RVAL(orwl_mirror*)

P99_PROTOTYPE(orwl_state, orwl_handle2_next, orwl_handle2*, size_t, p99_seed*);
#define orwl_handle2_next(...)  P99_CALL_DEFARG(orwl_handle2_next, 3, __VA_ARGS__)
#define orwl_handle2_next_defarg_1() 1u
#define orwl_handle2_next_defarg_2() p99_seed_get()

P99_PROTOTYPE(orwl_state, orwl_handle2_release, orwl_handle2*, size_t, p99_seed*);
#define orwl_release2(...)  P99_CALL_DEFARG(orwl_handle2_release, 3, __VA_ARGS__)
#define orwl_handle2_release(...)  P99_CALL_DEFARG(orwl_handle2_release, 3, __VA_ARGS__)
#define orwl_handle2_release_defarg_1() 1u
#define orwl_handle2_release_defarg_2() p99_seed_get()

P99_PROTOTYPE(orwl_state, orwl_cancel2, orwl_handle2*, p99_seed*);
#define orwl_cancel2(...)  P99_CALL_DEFARG(orwl_cancel2, 2, __VA_ARGS__)
#define orwl_cancel2_defarg_1() p99_seed_get()

P99_PROTOTYPE(orwl_state, orwl_forced_cancel2, orwl_handle2*, p99_seed*);
#define orwl_forced_cancel2(...)  P99_CALL_DEFARG(orwl_forced_cancel2, 2, __VA_ARGS__)
#define orwl_forced_cancel2_defarg_1() p99_seed_get()

P99_PROTOTYPE(orwl_state, orwl_handle2_acquire, orwl_handle2*, size_t, p99_seed*);
#define orwl_handle2_acquire(...)  P99_CALL_DEFARG(orwl_handle2_acquire, 3, __VA_ARGS__)
#define orwl_handle2_acquire_defarg_1() 1u
#define orwl_handle2_acquire_defarg_2() p99_seed_get()

P99_PROTOTYPE(orwl_state, orwl_handle2_disconnect, orwl_handle2*, size_t, p99_seed*);
#define orwl_handle2_disconnect(...)  P99_CALL_DEFARG(orwl_handle2_disconnect, 3, __VA_ARGS__)
#define orwl_handle2_disconnect_defarg_1() 1u
#define orwl_handle2_disconnect_defarg_2() p99_seed_get()

P99_PROTOTYPE(orwl_state, orwl_handle2_test, orwl_handle2*, size_t, p99_seed*);
#define orwl_test2(...)  P99_CALL_DEFARG(orwl_handle2_test, 3, __VA_ARGS__)
#define orwl_handle2_test(...)  P99_CALL_DEFARG(orwl_handle2_test, 3, __VA_ARGS__)
#define orwl_handle2_test_defarg_1() 1u
#define orwl_handle2_test_defarg_2() p99_seed_get()

P99_PROTOTYPE(uint64_t*, orwl_handle2_map, orwl_handle2*, size_t*, p99_seed*);
#define orwl_map2(...)  P99_CALL_DEFARG(orwl_handle2_map, 3, __VA_ARGS__)
#define orwl_handle2_map(...)  P99_CALL_DEFARG(orwl_handle2_map, 3, __VA_ARGS__)
#define orwl_handle2_map_defarg_1() 0
#define orwl_handle2_map_defarg_2() p99_seed_get()

P99_PROTOTYPE(uint64_t const*, orwl_handle2_mapro, orwl_handle2*, size_t*, p99_seed*);
#define orwl_mapro2(...)  P99_CALL_DEFARG(orwl_handle2_mapro, 3, __VA_ARGS__)
#define orwl_handle2_mapro(...)  P99_CALL_DEFARG(orwl_handle2_mapro, 3, __VA_ARGS__)
#define orwl_handle2_mapro_defarg_1() 0
#define orwl_handle2_mapro_defarg_2() p99_seed_get()

P99_PROTOTYPE(void, orwl_handle2_resize, orwl_handle2*, size_t, p99_seed*);
#define orwl_resize2(...)  P99_CALL_DEFARG(orwl_handle2_resize, 3, __VA_ARGS__)
#define orwl_handle2_resize(...)  P99_CALL_DEFARG(orwl_handle2_resize, 3, __VA_ARGS__)
#define orwl_handle2_resize_defarg_2() p99_seed_get()

P99_PROTOTYPE(void*, orwl_handle2_write_map, orwl_handle2*, size_t*, p99_seed*);
#define orwl_write_map2(...)  P99_CALL_DEFARG(orwl_handle2_write_map, 3, __VA_ARGS__)
#define orwl_handle2_write_map(...)  P99_CALL_DEFARG(orwl_handle2_write_map, 3, __VA_ARGS__)
#define orwl_handle2_write_map_defarg_1() 0
#define orwl_handle2_write_map_defarg_2() p99_seed_get()

P99_PROTOTYPE(void*, orwl_handle2_read_map, orwl_handle2*, size_t*, p99_seed*);
#define orwl_read_map2(...)  P99_CALL_DEFARG(orwl_handle2_read_map, 3, __VA_ARGS__)
#define orwl_handle2_read_map(...)  P99_CALL_DEFARG(orwl_handle2_read_map, 3, __VA_ARGS__)
#define orwl_handle2_read_map_defarg_1() 0
#define orwl_handle2_read_map_defarg_2() p99_seed_get()

P99_PROTOTYPE(size_t, orwl_handle2_offset, orwl_handle2*, p99_seed*);
#define orwl_handle2_offset(...)  P99_CALL_DEFARG(orwl_handle2_offset, 2, __VA_ARGS__)
#define orwl_handle2_offset_defarg_1() p99_seed_get()

P99_PROTOTYPE(void, orwl_handle2_hold, orwl_handle2*, orwl_alloc_ref*, p99_seed*, bool);
#define orwl_handle2_hold(...)  P99_CALL_DEFARG(orwl_handle2_hold, 4, __VA_ARGS__)
#define orwl_handle2_hold_defarg_2() p99_seed_get()
#define orwl_handle2_hold_defarg_3() true

P99_PROTOTYPE(void, orwl_handle2_truncate, orwl_handle2*, size_t, p99_seed*);
#define orwl_truncate2(...)  P99_CALL_DEFARG(orwl_handle2_truncate, 3, __VA_ARGS__)
#define orwl_handle2_truncate(...)  P99_CALL_DEFARG(orwl_handle2_truncate, 3, __VA_ARGS__)
#define orwl_handle2_truncate_defarg_2() p99_seed_get()

#endif

/**
 ** @}
 **/


#endif      /* !ORWL_HANDLE2_H_ */
