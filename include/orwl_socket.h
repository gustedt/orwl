/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the author and copyright holder for this work is                           */
/* all rights reserved,  2010-2013 Jens Gustedt, INRIA, France                */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#ifndef     ORWL_SOCKET_H_
# define    ORWL_SOCKET_H_

#include "orwl_register.h"
#include "orwl_endpoint.h"
#include P99_ADVANCE_ID
#include "orwl_header.h"

/**
 ** @addtogroup library_support
 ** @{
 **/

/**
 ** @brief Classify the scope of IP addresses.
 **
 ** IP addresses (IPv4 or IPv6) come in different flavors, that
 ** regulate the scope in which they are visible (= routable).
 **/
P99_DECLARE_ENUM(orwl_ip_scope,
                 orwl_ip_any,
                 orwl_ip_loopback,
                 orwl_ip_link,
                 orwl_ip_privat,
                 orwl_ip_global
                );

/**
 ** @var orwl_ip_scope orwl_ip_any
 **
 ** @brief The "unitialized address".
 **/

/**
 ** @var orwl_ip_scope orwl_ip_loopback
 **
 ** @brief Loopback addresses are only able to communicate internally
 ** to the host.
 **/

/**
 ** @var orwl_ip_scope orwl_ip_link
 **
 ** @brief Link addresses can be used to access a host over a specific
 ** wire (e.g) but will never be routed.
 **/

/**
 ** @var orwl_ip_scope orwl_ip_privat
 **
 ** @brief Privat addresses can be used inside a local domain but will
 ** never be routed to the open.
 **/

/**
 ** @var orwl_ip_scope orwl_ip_global
 **
 ** @brief Addresses that anybody can speak to.
 **/

/**
 ** @brief Return the scope of an ::orwl_addr.
 **/
P99_PURE_FUNCTION
orwl_ip_scope orwl_addr2scope(orwl_addr const* A);



/**
 ** @brief A list of protocols that we know of.
 **
 ** We use such a list to prescribe an ordering on the
 ** protocols. Otherwise an ordering would be too much platform
 ** dependent.
 **/
#define ORWL_AF_LIST  AF_UNSPEC, AF_UNIX, AF_INET, AF_INET6

#define ORWL_SOCK_TYPE  0,  SOCK_DGRAM, SOCK_STREAM
#define ORWL_SOCK_NAME  "any",  "udp",    "tcp"

P99_CONST_FUNCTION char const* orwl_sock_type_name(unsigned st);
P99_PURE_FUNCTION signed orwl_sock_type_get(char const* name);

/**
 ** @brief Return the inet <code>/N</code> network for address @a X,
 ** which is supposed to be in host order.
 **
 ** @pre This will only work for network representations of @a X in a
 ** 32 bit word.
 **/
#define ORWL_IN_MASK(X, N) ((X) & P99_HMASK(32, N))


#ifndef DOXYGEN
inline
P99_PROTOTYPE(void, orwl_hton, uint64_t *, uint64_t const *, size_t);
#define orwl_hton(...) P99_CALL_DEFARG(orwl_hton, 3, __VA_ARGS__)
#define orwl_hton_defarg_2() 1
#endif

#ifndef HOST_NAME_MAX
# ifdef POSIX_HOST_NAME_MAX
#  define HOST_NAME_MAX POSIX_HOST_NAME_MAX
# endif
#endif
P99_DEFARG_DOCU(orwl_hton)
inline
void
orwl_hton(uint64_t *n,        /*!< [out] array of length @a l */
          uint64_t const *h,  /*!< [in] array of length @a l */
          size_t l            /*!< [in] defaults to 1 */
         ) {
  uint64_t const* hv = h ? h : n;
  for (size_t i = 0; i < l; ++i) {
    n[i] = orwl_hton64(hv[i]);
  }
}

#ifndef DOXYGEN
inline
P99_PROTOTYPE(void, orwl_ntoh, uint64_t*, uint64_t const *, size_t);
#define orwl_ntoh(...) P99_CALL_DEFARG(orwl_ntoh, 3, __VA_ARGS__)
#define orwl_ntoh_defarg_2() 1
#endif

P99_DEFARG_DOCU(orwl_ntoh)
inline
void
orwl_ntoh(uint64_t* h,       /*!< [out] array of length @a l */
          uint64_t const *n, /*!< [in] array of length @a l */
          size_t l           /*!< [in] defaults to 1 */
         ) {
  uint64_t const *nv = n ? n : h;
  for (size_t i = 0; i < l; ++i) {
    h[i] = orwl_ntoh64(nv[i]);
  }
}

extern orwl_addr orwl_inet_addr(char const *name);

extern void orwl_ntoa(orwl_addr const* addr, char *restrict name);

#define diagnose(fd, form, ...)                                    \
do {                                                               \
  orwl_addr addr = ORWL_ADDR_ANY;                                  \
  if (orwl_getpeername(fd, &addr) != -1) {                         \
    char name[256] = "";                                           \
    orwl_ntoa(&addr, name);                                        \
    report(stderr, "connection from %s " form, name, __VA_ARGS__); \
  }                                                                \
 } while (0)


#ifndef DOXYGEN
inline
P99_PROTOTYPE(char const*, orwl_inet_ntop, orwl_addr const*, char*restrict, size_t);
P99_DECLARE_DEFARG(orwl_inet_ntop, , , );
#define orwl_inet_ntop(...) P99_CALL_DEFARG(orwl_inet_ntop, 3, __VA_ARGS__)
#define orwl_inet_ntop_defarg_1() ((char[INET6_ADDRSTRLEN]){ 0 })
#define orwl_inet_ntop_defarg_2() (INET6_ADDRSTRLEN)
#endif


P99_DEFARG_DOCU(orwl_inet_ntop)
inline
char const* orwl_inet_ntop(orwl_addr const* addr,
                           char*restrict buf, size_t size) {
  void const* src =
    ((addr->sa.sa_family) == AF_INET
     ? (void const*)&(((struct sockaddr_in const*)(addr))->sin_addr)
     :  ((addr->sa.sa_family) == AF_INET6
         ? (void const*)&(((struct sockaddr_in6 const*)(addr))->sin6_addr)
         : (void const*)0)
    );
  if (src) inet_ntop(addr->sa.sa_family, src, buf, size);
  else strncpy(buf, "<invalid addr>", size);
  return buf;
}

#ifndef DOXYGEN
#define hostname(...) P99_CALL_DEFARG(hostname, 2, __VA_ARGS__)
#define hostname_defarg_0() ((char[HOST_NAME_MAX]){ 0 })
#define hostname_defarg_1() HOST_NAME_MAX
#endif

/**
 ** @brief Obtain the %hostname of the execution host
 **
 ** This is a wrapper to the POSIX function @c gethostname,
 ** the difference is the signature.
 ** @return @a buffer if the call to @c gethostname was successful, a
 ** null pointer otherwise.
 **/
P99_DEFARG_DOCU(hostname)
inline
char const*
hostname(char buffer[],   /*!< [out] defaults to a temporary */
         size_t len       /*!< [in] maximum length of the name (HOST_NAME_MAX) */
        ) {
  return
    gethostname(buffer, len)
    ? P99_0(char*)
    : buffer;
}

/**
 ** @}
 **/


#endif      /* !ORWL_SOCKET_H_ */
