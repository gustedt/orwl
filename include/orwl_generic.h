/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the author and copyright holder for this work is                           */
/* all rights reserved,  2012-2013, 2016 Jens Gustedt, INRIA, France          */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#ifndef     ORWL_GENERIC_H_
# define    ORWL_GENERIC_H_

#include "orwl_remote_queue.h"
#include "p99_defarg.h"


/**
 ** @addtogroup library_generic Type generic interfaces for single or iterative access
 **
 ** The interfaces in this group are probably those of ORWL that
 ** should be used with preference. They are type generic and accept
 ** pointers to ::orwl_handle as well as ::orwl_handle2 as
 ** arguments. The most important of these interfaces are
 **
 ** - ::orwl_read_insert and ::orwl_write_insert to insert a handle
 **   into the FIFO of a location
 **
 ** - ::ORWL_SECTION to protect a critical section of your code by
 **     means of an ::orwl_handle or ::orwl_handle2
 **
 ** - ::orwl_read_map and ::orwl_write_map to obtain a pointer to the
 **    object in the associated location
 **
 **
 ** @{
 **
 **/

#define O_RWL_CHOSE_FUNC(FUNC, FUNC2, X)                       \
P99_GENERIC(X,                                                 \
            P99_GENERIC(X[0],                                  \
                        ,                                      \
                        (orwl_handle, FUNC),                   \
                        (orwl_handle2, FUNC2)                  \
                        ),                                     \
            (orwl_handle*, FUNC),                              \
            (orwl_handle2*, FUNC2)                             \
            )

#define O_RWL_CHOSE_SIZE(X) P99_OBJLEN(X, orwl_handle, orwl_handle2)

#define O_RWL_CALL_DEFARG0_(FUNC, FUNC2, M, X, ...)                             \
O_RWL_CHOSE_FUNC(FUNC, FUNC2, X)(P99_CALL_DEFARG_LIST(FUNC, M, X, __VA_ARGS__))

#define O_RWL_CALL_DEFARG0(FUNC, M, X, ...)                                                               \
O_RWL_CALL_DEFARG0_(P99_PASTID2(orwl_handle, FUNC), P99_PASTID2(orwl_handle2, FUNC), M, (X), __VA_ARGS__)

#define O_RWL_CALL_DEFARG3(FUNC, M, X, N, E, ...)                                   \
O_RWL_CALL_DEFARG0_(P99_PASTID(orwl_handle, FUNC), P99_PASTID2(orwl_handle2, FUNC), \
                    M, (X),                                                         \
                    P99_IF_EMPTY(N)(O_RWL_CHOSE_SIZE(X))(N),                        \
                    E)

#define O_RWL_CALL_DEFARG1_(FUNC, FUNC2, M, X, Y, ...)                             \
O_RWL_CHOSE_FUNC(FUNC, FUNC2, Y)(P99_CALL_DEFARG_LIST(FUNC, M, X, Y, __VA_ARGS__))

#define O_RWL_CALL_DEFARG1(FUNC, M, X, Y, ...)                                                                 \
O_RWL_CALL_DEFARG1_(P99_PASTID2(orwl_handle, FUNC), P99_PASTID2(orwl_handle2, FUNC), M, (X), (Y), __VA_ARGS__)

#define O_RWL_DEFARG_DOCUMENT(NAME)                                                                                                    \
/** @brief A type generic macro that selects the correct function according to the type of @a HANDLE **/                               \
/** @remark @c orwl_ ## NAME is actually implemented as a macro that helps to provide default arguments to the underlying function. */ \
/** @see orwl_handle_ ## NAME for the ::orwl_handle interface **/                                                                      \
/** @see orwl_handle2_ ## NAME for the ::orwl_handle2 interface **/

#ifdef DOXYGEN
O_RWL_DEFARG_DOCUMENT(write_request)
#define orwl_write_request(MIRROR, HANDLE, SIZE, SEED) orwl_handle{2}_write_request(MIRROR, HANDLE, SIZE, SEED)

O_RWL_DEFARG_DOCUMENT(read_request)
#define orwl_read_request(MIRROR, HANDLE, SIZE, SEED) orwl_handle{2}_read_request(MIRROR, HANDLE, SIZE, SEED)

O_RWL_DEFARG_DOCUMENT(write_insert)
#define orwl_write_insert(HANDLE, LOCID, PRIO, SEED, SRV, RQ) orwl_handle{2}_write_insert(HANDLE, LOCID, PRIO, SEED, SRV, RQ)

O_RWL_DEFARG_DOCUMENT(read_insert)
#define orwl_read_insert(HANDLE, LOCID, PRIO, SEED, SRV, RQ) orwl_handle{2}_read_insert(HANDLE, LOCID, PRIO, SEED, SRV, RQ)

/**
 ** @remark This is a type generic macro that selects the correct
 ** function according to the base type of @a HANDLE.
 **
 ** @remark If the optional argument @a SIZE is omitted and if @a
 ** HANDLE is a pointer type this supposes that the pointed-to object
 ** contains just one handle. If it is an array type, the length of
 ** that array is used for @a SIZE.
 **
 **/
O_RWL_DEFARG_DOCUMENT(release)
#define orwl_release(HANDLE, SIZE, SEED) orwl_handle{2}_release(HANDLE, SIZE, SEED)

/**
 ** @remark This is a type generic macro that selects the correct
 ** function according to the base type of @a HANDLE.
 **
 ** @remark If the optional argument @a SIZE is omitted and if @a
 ** HANDLE is a pointer type this supposes that the pointed-to object
 ** contains just one handle. If it is an array type, the length of
 ** that array is used for @a SIZE.
 **
 **/
O_RWL_DEFARG_DOCUMENT(acquire)
#define orwl_acquire(HANDLE, SIZE, SEED) orwl_handle{2}_acquire(HANDLE, SIZE, SEED)

/**
 ** @remark This is a type generic macro that selects the correct
 ** function according to the base type of @a HANDLE.
 **
 ** @remark If the optional argument @a SIZE is omitted and if @a
 ** HANDLE is a pointer type this supposes that the pointed-to object
 ** contains just one handle. If it is an array type, the length of
 ** that array is used for @a SIZE.
 **
 **/
O_RWL_DEFARG_DOCUMENT(test)
#define orwl_test(HANDLE, SIZE, SEED) orwl_handle{2}_test(HANDLE, SIZE, SEED)

/**
 ** @brief Disconnect @a HANDLE from its location(s).
 **
 ** @remark This is a type generic macro that selects the correct
 ** function according to the base type of @a HANDLE.
 **
 ** @remark If the optional argument @a SIZE is omitted and if @a
 ** HANDLE is a pointer type this supposes that the pointed-to object
 ** contains just one handle. If it is an array type, the length of
 ** that array is used for @a SIZE.
 **
 **/
O_RWL_DEFARG_DOCUMENT(disconnect)
#define orwl_disconnect(HANDLE, SIZE, SEED) orwl_handle2_disconnect(HANDLE, SIZE, SEED)

O_RWL_DEFARG_DOCUMENT(write_map)
#define orwl_write_map(HANDLE, LEN, SEED) orwl_handle{2}_write_map(HANDLE, LEN, SEED)

O_RWL_DEFARG_DOCUMENT(read_map)
#define orwl_read_map(HANDLE, LEN, SEED) orwl_handle{2}_read_map(HANDLE, LEN, SEED)

O_RWL_DEFARG_DOCUMENT(offset)
#define orwl_offset(HANDLE, SEED) orwl_handle{2}_offset(HANDLE, SEED)

O_RWL_DEFARG_DOCUMENT(truncate)
#define orwl_truncate(HANDLE, LEN, SEED) orwl_handle{2}_truncate(HANDLE, LEN, SEED)

/**
 ** @brief Replace the data of the location corresponding to @a HANDLE.by @a BUF
 **
 ** @remark This is a type generic macro that selects the correct
 ** function according to the base type of @a HANDLE.
 **
 **/
O_RWL_DEFARG_DOCUMENT(replace)
#define orwl_replace(HANDLE, BUF, SEED) orwl_handle{2}_replace(HANDLE, BUF, SEED)


/**
 ** @brief Put the data of the location o @a HANDLE on hold in the
 ** current task and return it.
 **
 ** @remark This is a type generic macro that selects the correct
 ** function according to the base type of @a HANDLE.
 **
 **/
O_RWL_DEFARG_DOCUMENT(hold)
#define orwl_hold(HANDLE, REF, SEED, MARK) orwl_handle{2}_hold(HANDLE, REF, SEED, MARK)


/**
 ** @brief Release a handle @a HANDLE
 **
 ** If the base type is ::orwl_handle2 an iterative access is supposed and
 ** a new lock request is placed in the FIFO of the resource to ensure
 ** the proper lock ordering for the next iteration. There is probably
 ** not much interest in using this interface directly. Use
 ** ::ORWL_SECTION instead.
 **
 ** @remark This is a type generic macro that selects the correct
 ** function according to the base type of @a HANDLE.
 **
 ** @remark If the optional argument @a SIZE is omitted and if @a
 ** HANDLE is a pointer type this supposes that the pointed-to object
 ** contains just one handle. If it is an array type, the length of
 ** that array is used for @a SIZE.
 **
 ** @see ORWL_SECTION for a macro that protects a critical code
 ** section
 **
 ** @see orwl_handle_release for the ::orwl_handle interface
 **
 ** @see orwl_handle2_next for the ::orwl_handle2 interface
 **
 ** **/
#define orwl_release_or_next(HANDLE, SIZE, SEED)
#else
#define orwl_write_request(...)  O_RWL_CALL_DEFARG1(write_request, 4, __VA_ARGS__)
#define orwl_handle_write_request_defarg_2() 1u
#define orwl_handle_write_request_defarg_3() p99_seed_get()

#define orwl_read_request(...)  O_RWL_CALL_DEFARG1(read_request, 4, __VA_ARGS__)
#define orwl_handle_read_request_defarg_2() 1u
#define orwl_handle_read_request_defarg_3() p99_seed_get()

#define orwl_write_insert(...)  O_RWL_CALL_DEFARG0(write_insert, 6, __VA_ARGS__)
#define orwl_handle_write_insert_defarg_1() SIZE_MAX
#define orwl_handle_write_insert_defarg_2() SIZE_MAX
#define orwl_handle_write_insert_defarg_3() p99_seed_get()
#define orwl_handle_write_insert_defarg_4() orwl_server_get()
#define orwl_handle_write_insert_defarg_5() P99_RVAL(orwl_mirror*)

#define orwl_read_insert(...)  O_RWL_CALL_DEFARG0(read_insert, 6, __VA_ARGS__)
#define orwl_handle_read_insert_defarg_2() SIZE_MAX
#define orwl_handle_read_insert_defarg_3() p99_seed_get()
#define orwl_handle_read_insert_defarg_4() orwl_server_get()
#define orwl_handle_read_insert_defarg_5() P99_RVAL(orwl_mirror*)

#define orwl_release(...)  O_RWL_CALL_DEFARG3(release, 3, __VA_ARGS__,,,)
#define orwl_handle_release_defarg_2() p99_seed_get()

#define orwl_acquire(...)  O_RWL_CALL_DEFARG3(acquire, 3, __VA_ARGS__,,,)
#define orwl_handle_acquire_defarg_2() p99_seed_get()

#define orwl_test(...)  O_RWL_CALL_DEFARG3(test, 3, __VA_ARGS__,,,)
#define orwl_handle_test_defarg_2() p99_seed_get()

#define orwl_write_map(...)  O_RWL_CALL_DEFARG0(write_map, 3, __VA_ARGS__)
#define orwl_handle_write_map_defarg_1() 0
#define orwl_handle_write_map_defarg_2() p99_seed_get()

#define orwl_read_map(...)  O_RWL_CALL_DEFARG0(read_map, 3, __VA_ARGS__)
#define orwl_handle_read_map_defarg_1() 0
#define orwl_handle_read_map_defarg_2() p99_seed_get()

#define orwl_offset(...)  O_RWL_CALL_DEFARG0(offset, 2, __VA_ARGS__)
#define orwl_handle_offset_defarg_1() p99_seed_get()

#define orwl_truncate(...) O_RWL_CALL_DEFARG0(truncate, 3, __VA_ARGS__)
#define orwl_handle_truncate_defarg_2() p99_seed_get()

#define orwl_replace(...) O_RWL_CALL_DEFARG0(replace, 3, __VA_ARGS__)
#define orwl_handle_replace_defarg_2() p99_seed_get()

#define orwl_hold(...) O_RWL_CALL_DEFARG0(hold, 4, __VA_ARGS__)
#define orwl_handle_hold_defarg_2() p99_seed_get()
#define orwl_handle_hold_defarg_3() true

#define orwl_release_or_next(X, ...)                                                              \
P99_GENERIC(&((X)[0]), , (orwl_handle*, orwl_handle_release), (orwl_handle2*, orwl_handle2_next)) \
O_RWL_RELEASE_OR_NEXT((X), __VA_ARGS__,p99_seed_get())

#define O_RWL_RELEASE_OR_NEXT(X, N, E, ...)                    \
  ((X),                                                        \
   P99_IF_EMPTY(N)(O_RWL_CHOSE_SIZE(X))(N),                    \
   E)

#define orwl_disconnect(X, ...)                                                                         \
P99_GENERIC(&((X)[0]), , (orwl_handle*, orwl_handle_release), (orwl_handle2*, orwl_handle2_disconnect)) \
O_RWL_DISCONNECT((X), __VA_ARGS__,p99_seed_get())

#define O_RWL_DISCONNECT(X, N, E, ...)                         \
  (X,                                                          \
   P99_IF_EMPTY(N)(O_RWL_CHOSE_SIZE(X))(N),                    \
   E)

#endif

#ifdef DOXYGEN
/**
 ** @brief Protect the following block or statement with
 ** @a HANDLE.
 **
 ** This is a type generic interface that accepts either pointers to
 ** ::orwl_handle or ::orwl_handle2 or as value for @a HANDLE. Its
 ** effect is to place a call to ::orwl_acquire before the execution
 ** of the depending block and a call to ::orwl_release_or_next after
 ** it. That is, the resource to which @a HANDLE refers is locked on
 ** entry and then released on exit. If the type is ::orwl_handle2 an
 ** iterative access is supposed and a new lock request is placed in
 ** the FIFO of the resource to ensure the proper lock ordering for
 ** the next iteration.
 **
 ** This macro performs some rudimentary error checking for the result
 ** of the locking. If an error occurs the whole block and any other
 ** enclosing blocks that protected with P99_UNWIND_PROTECT are
 ** aborted.
 **
 ** @param HANDLE either a pointer to ::orwl_handle or ::orwl_handle2 (that should not
 ** be @c 0) or an array of one of these types.
 ** @param SIZE (optional) the length of the array that is pointed to.
 ** If @a HANDLE is a pointer type this supposes that the pointed-to
 ** object contains just one handle. If it is an array type, the
 ** length of that array is used for @a SIZE.
 ** @param SEED (optional) is the seed for the pseudo random generator
 ** to use, default if omitted is to call ::p99_seed_get.
 **
 ** @warning This macro evaluates @a HANDLE twice, so the expression
 ** should not contain side effects.
 **
 **/
P99_BLOCK_DOCUMENT
#define ORWL_SECTION(HANDLE, SIZE, SEED) orwl_handle_acquire()
#else
#define ORWL_SECTION(...)                                      \
P99_IF_LT(P99_NARG(__VA_ARGS__),2)                             \
  (O_RWL_SECTION(__VA_ARGS__,,))                               \
  (P99_IF_LT(P99_NARG(__VA_ARGS__), 3)                         \
   (O_RWL_SECTION(__VA_ARGS__,))                               \
   (O_RWL_SECTION(__VA_ARGS__)))
#define O_RWL_SECTION(HANDLE, SIZE, SEED)                                                                \
P00_BLK_START                                                                                            \
P00_BLK_DECL(orwl_state, P99_FILEID(state), orwl_invalid)                                                \
P99_IF_EMPTY(SIZE)                                                                                       \
     (P00_BLK_DECL(size_t const, P99_FILEID(size), O_RWL_CHOSE_SIZE(HANDLE)))                            \
     (P00_BLK_DECL(size_t const, P99_FILEID(size), SIZE))                                                \
P99_IF_EMPTY(SEED)                                                                                       \
     (P00_BLK_DECL(p99_seed *const, P99_FILEID(seed), p99_seed_get()))                                   \
     (P00_BLK_DECL(p99_seed *const, P99_FILEID(seed), SEED))                                             \
P99_GUARDED_BLOCK                                                                                        \
  (void*,                                                                                                \
   P99_FILEID(handle),                                                                                   \
   (HANDLE),                                                                                             \
   (void)(P99_UNLIKELY                                                                                   \
          (((P99_FILEID(state) = orwl_acquire(&(HANDLE)[0], P99_FILEID(size), P99_FILEID(seed))))        \
           != orwl_acquired)                                                                             \
          && ((errno = EINVAL))                                                                          \
          && (perror(__FILE__ ":" P99_STRINGIFY(__LINE__) ": lock error for " P99_STRINGIFY(HANDLE)), 1) \
          && ((errno = 0))                                                                               \
          && ((P99_FILEID(handle) = 0), 1)                                                               \
          && (P99_UNWIND(-1), 1)                                                                         \
          ),                                                                                             \
   (void)(P99_FILEID(handle)                                                                             \
          && orwl_release_or_next(&(HANDLE)[0], P99_FILEID(size), P99_FILEID(seed))))

/* The Intel compiler produces this useless warning with ORWL_SECTION,
and there seems to be no way convince it by () not to issue it at a
particular point. Disable it. */

P99_IF_COMPILER(INTEL, warning(disable: 187))  /* use of "=" where "==" may have been intended */

#endif

#ifdef DOXYGEN
/**
 ** @brief obsolete, use the type generic interface ::ORWL_SECTION, instead
 **/
P99_BLOCK_DOCUMENT
#define ORWL_SECTION2(HANDLE, SIZE, SEED)
#else
#define ORWL_SECTION2 ORWL_SECTION
#endif

/**
 ** @}
 **
 **/


#endif
