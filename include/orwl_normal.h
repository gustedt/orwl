// This may look like C code, but it really is -*- mode: c; coding: utf-8 -*-
//
#ifndef ORWL_NORMAL_H
#define ORWL_NORMAL_H

/// @file

#include <math.h>
#include "p99_rand.h"


/// @brief Sample a normalized normal distribution.
double orwl_normal0(p99_seed seed[static 1]);

/// @brief Sample a normal distribution with given mean and standard
/// deviation.
inline
double orwl_normal(p99_seed seed[static 1], double mean, double sdev) {
  return orwl_normal0(seed) * sdev + mean;
}

#ifndef DOXYGEN
#define orwl_normal(...) P99_CALL_DEFARG(orwl_normal, 3, __VA_ARGS__)
#define orwl_normal_defarg_1() 0.0
#define orwl_normal_defarg_2() 1.0
#endif



#endif
