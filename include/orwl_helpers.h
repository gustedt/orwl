/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the authors and copyright holders for this work are as follows:            */
/* all rights reserved,  2011 Emmanuel Jeanvoine, INRIA, France               */
/* all rights reserved,  2011-2014, 2016 Jens Gustedt, INRIA, France          */
/* all rights reserved,  2013 Rodrigo Campos, INRIA, France                   */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#ifndef     ORWL_HELPERS_H_
# define    ORWL_HELPERS_H_

#include "orwl_handle2.h"
#include "orwl_barrier.h"
#include "orwl_deep_instr.h"

/**
 ** @addtogroup process_model
 ** @{
 **/

/**
 ** @brief stop the current ORWL task
 **
 ** This function ends the current ORWL task as we know it. This
 ** contains a barrier, so either no tasks or all should perform this
 ** operation.
 **
 ** Essentially this runs some callbacks that would be run anyhow when
 ** the thread of the task exits. Use ::orwl_stop_task if you leak
 ** stack variables to other parts of ORWL.
 **/
void orwl_stop_task(void);

/**
 ** @brief ensure that the callback @a func is run with the given @a
 ** data.
 **
 ** This pair will be run when the thread of the task exits, or when
 ** ::orwl_stop_task is called.
 **/
int orwl_at_task_exit(p99_callback_voidptr_func func, void* data);

P99_PROTOTYPE(void, o_rwl_task_dtor, void*);
P99_PROTOTYPE(void, o_rwl_run_at_task_exit, void*);

P99_TSS_DECLARE_LOCAL(p99_callback_stack, o_rwl_at_task_exit, o_rwl_run_at_task_exit);

/**
 ** @brief Declare the identifiers for the set of locations that are
 ** associated to each task.
 **
 ** The arguments to this macro should be a list of identifiers that
 ** name a set of locations (resources) for each task. E.g
 ** @code
 ** ORWL_LOCATIONS_PER_TASK(main_task, gather_task, boundary_task);
 ** @endcode
 **
 ** would declare three locations per task. The whole program run with
 ** such a declaration then would realize 3 * ::orwl_nt
 ** locations, over all.
 **
 ** The number of locations per task is a compile time integer
 ** constant and is available to the program through the identifier
 ** @c orwl_locations_amount.
 **
 ** In some cases, the first identifier that is listed (here @c
 ** main_task) is supposed to refer to a principal location of the
 ** task. If your application has such a notion you should list its
 ** identifier first.
 **
 ** @see P99_DECLARE_ENUM to see how this is done technically, in
 ** particular on how to print the identifiers.
 **
 ** @see ORWL_LOCATIONS_PER_TASK_INSTANTIATION to instantiate some
 ** useful functions.
 **/
#define ORWL_LOCATIONS_PER_TASK(...)                                       \
P99_DECLARE_ENUM(orwl_locations, __VA_ARGS__);                             \
typedef orwl_comstate o_rwl_comstate[orwl_locations_amount];               \
P99_TSS_DECLARE_LOCAL(o_rwl_comstate, o_rwl_comstate_loc, o_rwl_task_dtor)

#define ORWL_COMSTATE P99_TSS_LOCAL(o_rwl_comstate_loc)

#ifdef DOXYGEN
/**
 ** @brief A user supplied type that lists the ORWL locations that are
 ** associated to each task.
 **/
ORWL_LOCATIONS_PER_TASK(some_location, another_location);
#endif

/**
 ** @brief Instantiate some symbols that may be needed by
 ** ::ORWL_LOCATIONS_PER_TASK
 **
 ** @see ORWL_LOCATIONS_PER_TASK
 **/
#define ORWL_LOCATIONS_PER_TASK_INSTANTIATION(...)               \
void o_rwl_task_dtor(void* p) {                                  \
  o_rwl_comstate * coms = p;                                     \
  for (orwl_locations i = 0; i < orwl_locations_amount; ++i) {   \
    if ((*coms)[i].is_init) {                                    \
      orwl_comstate_disconnect(&(*coms)[i]);                     \
    }                                                            \
  }                                                              \
  free(p);                                                       \
}                                                                \
void orwl_stop_task(void) {                                      \
  orwl_deep_instr_inline_start();                                \
  /* The following ensures that the comstate callbacks are */    \
  /* called, now. Otherwise, with no orwl_stop_task, they are */ \
  /* called when the thread of the task exits. */                \
  if (p99_tss_get(&o_rwl_comstate_loc))                          \
    P99_THROW_CALL_THRD(p99_tss_set, &o_rwl_comstate_loc, 0);    \
  /* Similar for the task_exit callbacks. Otherwise, with no */  \
  /* orwl_stop_task, they are called when the task exits. */     \
  if (p99_tss_get(&o_rwl_at_task_exit))                          \
    P99_THROW_CALL_THRD(p99_tss_set, &o_rwl_at_task_exit, 0);    \
  orwl_global_barrier_wait();                                    \
  orwl_deep_instr_inline_stop();                                 \
}                                                                \
P99_DEFINE_ENUM(orwl_locations)

#define O_RWL_LOCATION(TASK, LOCAL) (P99_RVAL(size_t, (TASK)) * orwl_locations_amount + (LOCAL))

#ifdef DOXYGEN
/**
 ** @brief Given task ID @a TASK and local location ID @a LOCAL,
 ** compute a global ID for a location.
 **
 ** @param TASK must be a valid ID of a task.
 **
 ** @param LOCAL must be one of the identifiers that was given with
 ** ::ORWL_LOCATIONS_PER_TASK.
 **/
#define ORWL_LOCATION(TASK, LOCAL)
#else
#define ORWL_LOCATION(...) P99_IF_LT(P99_NARG(__VA_ARGS__), 2)(O_RWL_LOCATION(__VA_ARGS__, 0))(O_RWL_LOCATION(__VA_ARGS__))
#endif

/**
 ** @brief Given a global location ID @a LOCATION, return the task ID
 ** to which it is associated.
 **/
#define ORWL_TASK(LOCATION) (P99_RVAL(size_t, (LOCATION)) / orwl_locations_amount)

/**
 ** @brief Given a global location ID @a LOCATION, return the local
 ** location ID to which it is associated.
 **
 ** That return value is one of the values that were specified with
 ** ::ORWL_LOCATIONS_PER_TASK.
 **/
#define ORWL_LOCAL(LOCATION) (P99_RVAL(size_t, (LOCATION)) % orwl_locations_amount)

#define ORWL_DATA_LOCATION(LOCATION)                           \
P99_MACRO_END(ORWL_DATA_LOCATION, LOCATION)

#define ORWL_FILE_LOCATION(LOCATION)                           \
P99_MACRO_END(ORWL_DATA_LOCATION, LOCATION)

#define ORWL_DEVICE_LOCATION(LOCATION)                         \
P99_MACRO_END(ORWL_DATA_LOCATION, LOCATION)

/**
 ** @}
 **/


inline
size_t
orwl_tasks2locations(size_t task_nb,
                     size_t const task_list[task_nb],
                     size_t locations_nb,
                     size_t location_list[locations_nb]) {
  assert(!(locations_nb % task_nb));
  size_t const orwl_locations_amount = locations_nb / task_nb;
  size_t count = 0;
  for (size_t task = 0; task < task_nb; ++task) {
    for (size_t loc = 0; loc < orwl_locations_amount; ++loc, ++count) {
      location_list[count] = ORWL_LOCATION(task_list[task], loc);
    }
  }
  return count;
}



/**
 ** @brief A vertex in a directed colored graph
 **/
struct orwl_vertex {
  size_t color;
  char label[16];
  size_t nb_neighbors;
  size_t * neighbors;
};

/**
 ** @brief Dynamically initialize a ::orwl_vertex
 **
 ** @param vertex is the vertex to be initialized
 ** @param nb_neighbors is the number of neighbors on which vertex
 **        is connected.
 **/
DOCUMENT_INIT(orwl_vertex)
inline
orwl_vertex* orwl_vertex_init(orwl_vertex *vertex, size_t nb_neighbors) {
  if (vertex) {
    *vertex = P99_LVAL(orwl_vertex,
                       .color = 0,
                       .nb_neighbors = nb_neighbors,
                       .neighbors = (nb_neighbors
                                     ? vertex->neighbors = size_t_vnew(nb_neighbors)
                                         : 0),
                       .label = P99_INIT,
                      );
  }

  return vertex;
}

#ifndef DOXYGEN
inline
P99_PROTOTYPE(orwl_vertex*, orwl_vertex_init, orwl_vertex*, size_t);
#define orwl_vertex_init(...) P99_CALL_DEFARG(orwl_vertex_init, 2, __VA_ARGS__)
P99_DECLARE_DEFARG(orwl_vertex_init, , P99_0(size_t));
#endif

DOCUMENT_DESTROY(orwl_vertex)
inline
void orwl_vertex_destroy(orwl_vertex *vertex) {
  size_t_vdelete(vertex->neighbors);
}

DECLARE_NEW_DELETE(orwl_vertex);

/**
 ** @brief A directed and colored graph
 **/
struct orwl_graph {
  size_t nb_vertices;
  orwl_vertex *vertices;
};

/**
 ** @brief Dynamically initialize a ::orwl_graph
 **
 ** @param graph is the graph to be initialized
 ** @param nb_vertices is the number of vertices in the graph
 **/
DOCUMENT_INIT(orwl_graph)
inline
orwl_graph * orwl_graph_init(orwl_graph * graph, size_t nb_vertices) {
  if (graph) {
    *graph = P99_LVAL(orwl_graph,
                      .nb_vertices = nb_vertices,
                      .vertices = orwl_vertex_vnew(nb_vertices)
                     );
  }
  return graph;
}

#ifndef DOXYGEN
inline
P99_PROTOTYPE(orwl_graph*, orwl_graph_init, orwl_graph *, size_t);
#define orwl_graph_init(...) P99_CALL_DEFARG(orwl_graph_init, 2, __VA_ARGS__)
P99_DECLARE_DEFARG(orwl_graph_init, , P99_0(size_t));
#endif

DOCUMENT_DESTROY(orwl_graph)
inline
void orwl_graph_destroy(orwl_graph *graph) {
  orwl_vertex_vdelete(graph->vertices);
}

DECLARE_NEW_DELETE(orwl_graph);

/**
 ** @brief Read a directed and colored graph from a file in dot format
 **
 ** @param graph is a pointer on a ::orwl_graph*. Warning, memory
 **        is allocated inside this function.
 ** @param file is the path to a dot file
 ** @param nb_vertices is the number of vertices that are expected to be
 **        read in the graph
 ** @return true if the read has been correctly performed, false otherwise
 ** @related orwl_graph
 **/
void orwl_graph_read(orwl_graph ** graph, char const* file, size_t nb_vertices);

P99_DECLARE_STRUCT(orwl_thread_local);

struct orwl_thread_local {
  size_t myloc;
};

inline
orwl_thread_local* orwl_thread_local_init(orwl_thread_local* p) {
  if (p) {
    *p = (orwl_thread_local) { .myloc = SIZE_MAX, };
  }
  return p;
}

inline
void orwl_thread_local_destroy(orwl_thread_local* p) {
  /* empty */
}

DECLARE_NEW_DELETE(orwl_thread_local);

inline
void orwl_thread_local_free(void* p) {
  orwl_thread_local_delete(p);
}

/**
 ** @brief An address book that contains the endpoint and the location
           associated to all the tasks
 **/
struct orwl_address_book {
  size_t nl;              /*!< the global number of locations */
  orwl_endpoint *eps;     /*!< the endpoints of the remote servers */
  orwl_mirror * mirrors;  /*!< local mirrors that are connected to the distant locations */
  p99_tss local_key;        /*!< a key to manage thread local data inside this server */
  int error;              /*!< an error condition that was encountered with one of the remotes */
  size_t lost;            /*!< the remote task that produced error, if any */
  size_t next;            /*!< index of the next server in the ring */
};

/**
 ** @brief Dynamically initialize an ::orwl_address_book
 **
 ** @param ab is the address book to be initialized
 **
 ** @param nl is the total number of locations
 **/
DOCUMENT_INIT(orwl_address_book)
inline
orwl_address_book* orwl_address_book_init(orwl_address_book *ab,
    size_t nl) {
  if (ab) {
    *ab = P99_LVAL(orwl_address_book,
                   .nl = nl,
    .local_key = {
      .p99_dtor = orwl_thread_local_free,
    },
                  );
    if (nl > 0) {
      ab->eps = orwl_endpoint_vnew(nl);
      ab->mirrors = orwl_mirror_vnew(nl);
    }
  }
  return ab;
}

#ifndef DOXYGEN
inline
P99_PROTOTYPE(orwl_address_book*, orwl_address_book_init, orwl_address_book *, size_t);
#define orwl_address_book_init(...) P99_CALL_DEFARG(orwl_address_book_init, 2, __VA_ARGS__)
P99_DECLARE_DEFARG(orwl_address_book_init, , P99_0(size_t));
#endif

DOCUMENT_DESTROY(orwl_address_book)
inline
void orwl_address_book_destroy(orwl_address_book *ab) {
  orwl_endpoint_vdelete(ab->eps); ab->eps = 0;
  orwl_mirror_vdelete(ab->mirrors); ab->mirrors = 0;
  p99_tss_set(&ab->local_key, 0);
  p99_tss_delete(&ab->local_key);
}

/**
 ** @brief return the thread local information for the address book
 **
 ** @warning this should only be used through the macro
 ** ::ORWL_GET_LOCAL that feeds this function with the appropriate
 ** default value for the second parameter
 **/
inline
orwl_thread_local* orwl_address_book_get_local(orwl_address_book * ab, orwl_thread_local* def) {
  if (ab) {
    void* p = p99_tss_get(&ab->local_key);
    if (!p) {
      p = P99_NEW(orwl_thread_local);
      P99_THROW_CALL_THRD(p99_tss_set, &ab->local_key, p);
    }
    return p;
  } else {
    return def;
  }
}

inline
size_t * o_rwl_ab_nl(orwl_address_book * ab, size_t* def) {
  return ab ? &ab->nl : def;
}

size_t o_rwl_srv_sl(orwl_server* srv, size_t def);

#define ORWL_GET_LOCAL(AB) (*orwl_address_book_get_local((AB), &P99_LVAL(orwl_thread_local)))
#define ORWL_MYLOC(AB) (orwl_address_book_get_local((AB), &P99_LVAL(orwl_thread_local))->myloc)
#define ORWL_NL(AB) (*o_rwl_ab_nl(AB, &P99_LVAL(size_t)))
#define ORWL_NT(AB) (ORWL_NL(AB) / orwl_locations_amount)
#define ORWL_MYTID(AB) (ORWL_MYLOC(AB) / orwl_locations_amount)
#define ORWL_SL(SRV) (o_rwl_srv_sl(SRV, 0))

/**
 ** @addtogroup process_model
 ** @{
 **/

/**
 ** @brief The first location that is served by the same server
 **
 ** This is used by #report, #progress and #trace.
 **/
#define orwl_sl ORWL_SL(orwl_server_get())

/**
 ** @brief A default (global!) version of something like the
 ** total number of locations.
 **
 ** This is used by #report, #progress and #trace.
 **
 ** This value is set when reading the address book.
 **/
#define orwl_nl ORWL_NL(orwl_server_get()->ab)

/**
 ** @brief A default (global!) version of something like the
 ** total number of tasks.
 **
 ** This value is set when reading the address book.
 **/
#define orwl_nt ORWL_NT(orwl_server_get()->ab)


/**
 ** @brief A default version of something like a location id when
 ** there are more than one thread (operation) per task.
 **
 ** This is a thread local variable. This should be a value between 0
 ** and ::orwl_nl. Assign it at the beginning of a task.
 **
 ** @see DEFINE_THREAD, this value is automatically set when a thread
 ** is started as an "operation", where the location ID is passed as
 ** an argument to the thread start function.
 **
 ** @see orwl_auxiliary to know if a thread is auxiliary or the lead
 ** operation of a task.
 **/
#define orwl_myloc ORWL_MYLOC(orwl_server_get()->ab)

/**
 ** @brief Determine if a thread/operation is auxiliary, that is, it
 ** is launched with a location that is not the principal location of
 ** the task.
 **
 ** This makes only sense in a context that distinguishes several
 ** operations inside the same task, and in particular ::orwl_myloc
 ** must have been properly set to reflect that fact.
 **/
#define orwl_auxiliary (!!(orwl_myloc % orwl_locations_amount))

extern size_t o_rwl_operations;

/**
 ** @brief The number of operations per task.
 **
 ** @see orwl_operations_set to set this value from a central place,
 ** usually the @c main function of the application.
 **/
#define orwl_operations (o_rwl_operations+0)

/**
 ** @brief set the number of operation per task
 **
 ** This can be called in the main function for the special case of an
 ** orwl application with several operations per task.
 **
 ** @param op_amount defaults to ::orwl_locations_amount, that is one
 ** operation per location.
 **
 ** @see orwl_affinity_init is the only systematic user of this from
 ** within orwl.
 **/
void orwl_operations_set(size_t op_amount);


P99_PROTOTYPE(void, orwl_operations_set, size_t);

#define orwl_operations_set(...) P99_CALL_DEFARG(orwl_operations_set, 1, __VA_ARGS__)
#define orwl_operations_set_defarg_0() orwl_locations_amount

/**
 ** @brief A default version of something like a task id.
 **
 ** This is a value between 0 and ::orwl_nl. This value is deduced
 ** form ::orwl_myloc.
 **/
#define orwl_mytid ORWL_MYTID(orwl_server_get()->ab)

/**
 ** @}
 **/

/**
 ** @addtogroup thread_model
 ** @{
 **/


#define orwl_tids (orwl_server_get()->tids)
#define orwl_locids (orwl_server_get()->locids)
#define orwl_ll (orwl_server_get()->ll)
#define orwl_lt (orwl_server_get()->lt)

/**
 ** @brief Get a task specific "environment variable" @a name.
 **
 ** These are passed through environment variable ::ORWL_TARGS and
 ** should be set through the launch script.
 **
 ** This mechanism has some restrictions:
 **
 ** - values can only be set through the launch script and can not be
 **   modified during execution.
 **
 ** @param name should be a string that follows the naming rules as
 ** for identifiers. The value that is stored in the environment
 ** should not contain ";" or "|" characters.
 **
 ** @param mytid defaults to orwl_tid, the ID of the current task. But
 ** it may be used to asked for the values for valid task IDs.
 **
 ** @param serv defaults to something reasonable and should usually
 ** not be touched by user code.
 **
 ** @return is a string that can not be modified if @a name exists in
 ** the "environment" of the task. If it doesn't exist, @c 0 is
 ** returned.
 **/
char const* orwl_tgetenv(char const* name,
                         size_t mytid,
                         orwl_server* serv);

P99_PROTOTYPE(char const*, orwl_tgetenv,
              char const*,
              size_t,
              orwl_server*);

#define orwl_tgetenv(...) P99_CALL_DEFARG(orwl_tgetenv, 3, __VA_ARGS__)
#define orwl_tgetenv_defarg_1() orwl_mytid
#define orwl_tgetenv_defarg_2() orwl_server_get()


/**
 ** @}
 **/


/**
 ** @brief backwards compatibility
 **
 ** Just some old names that may still occur in some applications
 ** somewhere.
 **
 ** @{
 ** */

#define ORWL_MYNUM ORWL_MYLOC
#define ORWL_ID ORWL_MYTID
#define ORWL_NP ORWL_NL
#define orwl_mynum orwl_myloc
#define orwl_id orwl_mytid
#define orwl_np orwl_nl

/**
 ** @}
 **/

DECLARE_NEW_DELETE(orwl_address_book);

/**
 ** @brief Read an address book
 **
 ** @param ab is a pointer on a ::orwl_address_book*. Warning, memory
 **        is allocated inside this function.
 ** @param file is an open @c FILE buffer to the address book file to be read
 ** @return true if the read has been correctly performed, false otherwise
 ** @related orwl_address_book
 **/
void orwl_address_book_read(orwl_address_book **ab, FILE *file);


void orwl_write_address_book(orwl_server *serv,
                             FILE *filename,
                             size_t nb_id,
                             size_t const list_id[nb_id]);

/**
 ** @brief Block the application until a kick-off is given and load
 **        initialization files
 **
 ** Once called, this function produces a part of the address book
 ** with local information and writes it to @a id_filename. The idea
 ** is to collect the @a id_filename files on all the nodes with an
 ** external application that concatenates it to a global address
 ** book. Then the external application has to send the global address
 ** book on the nodes (the expected file name is specified in @a
 ** ab_filename).  Finally, once the global address book is copied
 ** everywhere, the kick-off can be performed by deleting id_filename
 ** on the nodes.
 **
 ** @param nb_id is the number of vertices running on the local server
 ** @param list_id is an array containing the id of the local vertices
 ** @param id_filename is the path to the partial address book produced
 ** @param ab_filename is the expected path to the global address book
 ** @param graph_filename is the path to the dot file containing the graph description
 ** @param serv is a pointer on the local ::orwl_server
 ** @return true if everything has been correctly performed, false otherwise
 **/
void orwl_wait_and_load_init_files(size_t nb_id,
                                   size_t const list_id[nb_id],
                                   const char *id_filename,
                                   const char *ab_filename,
                                   const char *graph_filename,
                                   orwl_server *serv);


/**
 ** @brief Connect a distant location to the local server
 **
 ** @param dest_id is the id of the distant vertex
 ** @param location is a pointer on a local ::orwl_mirror
 **        where the distant location must be mirrored
 ** @param server is a pointer on the local ::orwl_server
 **/
void orwl_make_distant_connection(size_t dest_id,
                                  orwl_mirror *location,
                                  orwl_server *server);

P99_PROTOTYPE(void, orwl_make_distant_connection, size_t, orwl_mirror *,  orwl_server *);
#define orwl_make_distant_connection(...) P99_CALL_DEFARG(orwl_make_distant_connection, 3, __VA_ARGS__)
#define orwl_make_distant_connection_defarg_2() orwl_server_get()

/**
 ** @brief Connect a local location to the local server
 **
 ** @param dest_id is the id of the local vertex
 ** @param location is a pointer on a local ::orwl_mirror
 **        where the local vertex location must be mirrored
 ** @param server is a pointer on the local ::orwl_server
 **/
void orwl_make_local_connection(size_t dest_id,
                                orwl_mirror *location,
                                orwl_server *server);

P99_PROTOTYPE(void, orwl_make_local_connection, size_t, orwl_mirror*,  orwl_server *);
#define orwl_make_local_connection(...) P99_CALL_DEFARG(orwl_make_local_connection, 3, __VA_ARGS__)
#define orwl_make_local_connection_defarg_2() orwl_server_get()

/**
 ** @brief Block an application thread until the corresponding
 **        vertex is able to initialize its locks. This is based
 **        on graph coloring and the policy is to block a vertex
 **        until all its neighbors in the undirected graph with
 **        a lower color are initialized.
 **
 ** @param id is the id of the vertex corresponding to the
 **        application thread
 ** @param serv is a pointer on a ::orwl_server
 ** @param seed is a pointer on a ::p99_seed (required because
 **        RPC can be launched)
 ** @return true if everything has been correctly performed, false otherwise
 **/
bool orwl_wait_to_initialize_locks(size_t id,
                                   orwl_server *serv,
                                   p99_seed *seed);

P99_PROTOTYPE(bool, orwl_wait_to_initialize_locks, size_t, orwl_server *, p99_seed *);
#define orwl_wait_to_initialize_locks(...) P99_CALL_DEFARG(orwl_wait_to_initialize_locks, 3, __VA_ARGS__)
#define orwl_wait_to_initialize_locks_defarg_1() orwl_server_get()
#define orwl_wait_to_initialize_locks_defarg_2() p99_seed_get()

/**
 ** @brief Block an application thread until the neighbors (in
 **        the undirected graph) of the corresponding vertex
 **        have initialized their locks.
 **
 ** @param id is the id of the vertex corresponding to the
 **        application thread
 ** @param nb_ll is the number of local vertices
 ** @param server is a pointer on the local ::orwl_server
 ** @param seed is a pointer on a ::p99_seed (required because
 **        RPC can be launched)
 ** @return true if everything has been correctly performed, false otherwise
 **/
bool orwl_wait_to_start(size_t id,
                        size_t nb_ll,
                        orwl_server *server,
                        p99_seed *seed);

P99_PROTOTYPE(bool, orwl_wait_to_start, size_t, size_t, orwl_server *, p99_seed *);
#define orwl_wait_to_start(...) P99_CALL_DEFARG(orwl_wait_to_start, 4, __VA_ARGS__)
#define orwl_wait_to_start_defarg_2() orwl_server_get()
#define orwl_wait_to_start_defarg_3() p99_seed_get()

/**
 ** @brief Initialize a global barrier. This must be performed on all the tasks
 **
 ** @param id is the is the id of the vertex corresponding to the
 **        application thread
 ** @param server is a pointer on the local ::orwl_server
 **/
void orwl_global_barrier_init(size_t id, orwl_server *server);

P99_PROTOTYPE(void, orwl_global_barrier_init, size_t, orwl_server *);
#define orwl_global_barrier_init(...) P99_CALL_DEFARG(orwl_global_barrier_init, 2, __VA_ARGS__)
#define orwl_global_barrier_init_defarg_1() orwl_server_get()

/**
 ** @brief Wait until all the application tasks have entered in the barrier.
 **
 ** @param myloc is the is the id of the principal location corresponding to the
 **        application thread, it defaults to the ::ORWL_MYLOC for @a server
 ** @param locations_amount is the number of locations that this task represents
 ** @param server is a pointer on the local ::orwl_server, defaults to the standard server thread
 ** @param seed is a pointer on a ::p99_seed, defaults to a predefined per thread seed. This is necessary because
 **        an RPC may be launched by the barrier.
 **
 ** @return For all but one chosen participants in the barrier the
 ** value @c 0 is returned. The chosen one returns a non-zero value
 ** such that an application may perform special action that has to be
 ** done by just one of the participants.
 **/
int orwl_global_barrier_wait(size_t myloc, size_t locations_amount, orwl_server *server, p99_seed *seed);

P99_PROTOTYPE(int, orwl_global_barrier_wait, size_t, size_t, orwl_server *, p99_seed *);
#define orwl_global_barrier_wait(...) P99_CALL_DEFARG(orwl_global_barrier_wait, 4, __VA_ARGS__)
#define orwl_global_barrier_wait_defarg_0() SIZE_MAX
#define orwl_global_barrier_wait_defarg_1() orwl_locations_amount
#define orwl_global_barrier_wait_defarg_2() orwl_server_get()
#define orwl_global_barrier_wait_defarg_3() p99_seed_get()

/**
 ** @brief The file name of the local address book
 **
 ** You probably don't have to manipulate this by your launch script
 ** should have set that to the correct value.
 **/
ORWL_DECLARE_ENV(ORWL_LOCAL_AB);

/**
 ** @brief The file name of the global address book
 **
 ** You probably don't have to manipulate this by your launch script
 ** should have set that to the correct value.
 **/
ORWL_DECLARE_ENV(ORWL_GLOBAL_AB);

/**
 ** @brief The file name to write the of a dependency graph between locations.
 **/
ORWL_DECLARE_ENV(ORWL_OUTGRAPH);

/**
 ** @brief The file name to be used as @c stdout, once ORWL has been
 ** successfully initialized.
 **
 ** If this environment variable is set, @c stdin and @c stdout can be
 ** used at the beginning of an ORWL to set up the communication
 ** between processes.
 **/
ORWL_DECLARE_ENV(ORWL_STDOUT);

/**
 ** @}
 **/


#endif
