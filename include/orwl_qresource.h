/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the author and copyright holder for this work is                           */
/* all rights reserved,  2013 Jens Gustedt, INRIA, France                     */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#ifndef ORWL_QRESOURCE_H_
#define ORWL_QRESOURCE_H_
#include "p99_futex.h"
#include "orwl_wait_queue.h"

P99_DECLARE_STRUCT(orwl_qresource);

/**
 ** @brief Handle a quantified resource
 **
 ** This is an internal data type that handles quantified
 ** resources. Usually you wouldn't use this type directly but the
 ** macros that are listed below.
 **
 ** @see ORWL_QRESOURCE_DECLARE to declare your own quantified
 ** resource
 **
 ** @see ORWL_QSECTION for a macro that marks a critical
 ** section for the use with such a resource
 **/
struct orwl_qresource {
  p99_futex futex;  /**< counter that symbolizes the resource */
  orwl_wq wq;       /**< the FIFO to regulate the access */
  size_t minval;
  size_t number;
};


#define ORWL_QRESOURCE_INITIALIZER(VAL)                        \
{                                                              \
    .futex = P99_FUTEX_INITIALIZER(VAL),                       \
    .wq = ORWL_WQ_INITIALIZER,                                 \
    }

/**
 ** @memberof orwl_qresource
 **/
inline
orwl_qresource* orwl_qresource_init(orwl_qresource* q, unsigned val) {
  if (q) {
    p99_futex_init(&q->futex, val);
    orwl_wq_init(&q->wq);
  }
  return q;
}

/**
 ** @memberof orwl_qresource
 **/
inline
void orwl_qresource_destroy(orwl_qresource* q) {
  if (q) {
    p99_futex_destroy(&q->futex);
    orwl_wq_destroy(&q->wq);
  }
}

/**
 ** @memberof orwl_qresource
 **/
inline
unsigned orwl_qresource_load(orwl_qresource* q) {
  return q ? p99_futex_load(&q->futex) : 0;
}

/**
 ** @memberof orwl_qresource
 **/
inline
void orwl_qresource_grab(orwl_qresource* q, size_t* accu, unsigned howmuch) {
  *accu += howmuch;
  orwl_wh* wh = P99_NEW(orwl_wh);
  orwl_wh_ref whr = ORWL_WH_REF_INITIALIZER(wh);
  orwl_state state = orwl_wq_request(&q->wq, wh);
  state = orwl_wh_acquire(wh);
  if (state == orwl_acquired) {
    P99_FUTEX_COMPARE_EXCHANGE(&q->futex,
                               /* name of the local variable */
                               val,
                               /* block if val is too small and retry */
                               val >= howmuch,
                               /* once there is val, decrement it, if possible */
                               val - howmuch,
                               /* never wake up anybody */
                               0u, 0u);
  }
  orwl_wh_ref_trigger(&whr);
}

/**
 ** @memberof orwl_qresource
 **/
inline
void orwl_qresource_release(orwl_qresource* q, size_t* accu, unsigned howmuch) {
  P99_FUTEX_COMPARE_EXCHANGE(&q->futex,
                             /* name of the local variable */
                             val,
                             /* never wait  */
                             true,
                             /* increment the value */
                             val + howmuch,
                             /* wake up waiters, but no more than
                                there are slots */
                             0u, howmuch);
}


#define O_RWL_QRESOURCE_SECTION(QR, VAL)                                  \
P00_BLK_DECL(unsigned, o_rwl_hm, (VAL))                                   \
P00_BLK_DECL(size_t*, o_rwl_accu, &P99_THREAD_LOCAL(QR ## _tss))          \
P99_GUARDED_BLOCK(orwl_qresource*const, o_rwl_qr, &(QR),                  \
                  orwl_qresource_grab(o_rwl_qr, o_rwl_accu, o_rwl_hm),    \
                  orwl_qresource_release(o_rwl_qr, o_rwl_accu, o_rwl_hm))

#ifdef DOXYGEN
/**
 ** @brief Declare a quantified resource named @a NAME and initial value @a VAL
 **
 ** @a VAL defaults to UINT_MAX if omitted.
 **
 ** Quantified resources live in their own namespace, sort of. An
 ** identifier declared as such will not conflict with other
 ** identifiers of the program that have the same name.
 **/
#define ORWL_QRESOURCE_DECLARE(...)                            \
/** \brief  a quantified resource, see ::ORWL_QSECTION */      \
extern orwl_qresource __VA_ARGS__
/**
 ** @brief Declare a quantified resource named @a NAME and initial value @a VAL
 **
 ** @a VAL defaults to UINT_MAX if omitted.
 **/
#define ORWL_QRESOURCE_DEFINE(NAME, VAL) orwl_qresource NAME = ORWL_QRESOURCE_INITIALIZER(VAL)
/**
 ** @brief Initialize a quantified resource named @a NAME to value @a VAL
 **
 ** @a VAL defaults to UINT_MAX if omitted.
 **/
#define ORWL_QRESOURCE_INIT(NAME, VAL) orwl_qresource_init(&o_rwl_qresource_ ## NAME, (VAL))
/**
 ** @brief Provide the value of the quantified resource named @a NAME
 **/
#define ORWL_QRESOURCE(NAME) orwl_qresource_load(&o_rwl_qresource_ ## NAME)


/**
 ** @brief Use @a HOWMUCH units of the quantified resource named @a
 ** NAME in the dependent statement or block.
 **
 **
 ** A typical usage would be as follows:
 ** @code
 ** ORWL_QRESOURCE_INIT(L2, 2);    // Assume that the platform has 2 distinct L2 caches
 **
 ** ORWL_QSECTION(L2) {
 **    // reserve one "L2" resource in FIFO order
 **    cblas_gemm(CblasRowMajor, CblasNoTrans, CblasNoTrans,
 **               n0, n2, nt*n1,
 **               1.0, &(*curRowA)[0][0], nt*n1,
 **               &(*curRowB)[0][0], n2,
 **               0.0, &(*curRowC)[0][iB*n2], nt*n2
 **               );
 **    // release the "L2" resource
 ** }
 ** @endcode
 **
 ** Here we suppose that @c cblas_gemm is a function that is optimized
 ** for a use of using one "L2" data cache, and that the platform has
 ** two banks of processors, each equipped with a proper L1 cache, The
 ** above section would then guaranteed that the inner part is never
 ** executed more than twice, simultaneously, regardless how many ORWL
 ** tasks will execute this code.
 **
 ** As for identified sections (see ::ORWL_SECTION) the access is
 ** regulated through a FIFO, but that FIFO is only local to the
 ** current process and does not arbitrate between tasks that don't
 ** share the same process context.
 **
 ** Once a task has acquired the FIFO, it waits for the availability
 ** of the number of resources it needs, and after that releases the
 ** FIFO (but not the resource!). The resource is release when the
 ** task leaves the critical section.
 **
 ** The resources that are controlled through that are "symbolic" and
 ** not strictly related to any hardware or software resource. It is
 ** up to the programmer to mark e.g all use of L2 critical sections
 ** my means of this macro here.
 **
 ** @a HOWMUCH defaults to @c 1u if omitted.
 **
 ** @see ORWL_QRESOURCE_DECLARE to declare additional quantified resources
 ** @see ORWL_QRESOURCE_DEFINE to define additional quantified resources
 **/
#define ORWL_QSECTION(NAME, HOWMUCH)
#else
#define O_RWL_QRESOURCE_DECLARE(NAME, ...)                     \
P99_DECLARE_THREAD_LOCAL_EXTERN(size_t, NAME ## _tss);         \
extern orwl_qresource NAME

#define O_RWL_QRESOURCE_DEFINE(NAME, VAL)                      \
P99_DEFINE_THREAD_LOCAL(size_t, NAME ## _tss);                 \
orwl_qresource NAME = ORWL_QRESOURCE_INITIALIZER(VAL)

#define O_RWL_QRESOURCE_INIT(NAME, VAL) orwl_qresource_init(&NAME, (VAL))
#define O_RWL_QRESOURCE(NAME) orwl_qresource_load(&NAME)

#define ORWL_QRESOURCE_DECLARE(...) O_RWL_QRESOURCE_DECLARE(o_rwl_qresource_ ## __VA_ARGS__, UINT_MAX)
#define ORWL_QRESOURCE_DEFINE(...)                                  \
P99_IF_LT(P99_NARG(o_rwl_qresource_ ## __VA_ARGS__), 2)             \
(O_RWL_QRESOURCE_DEFINE(o_rwl_qresource_ ## __VA_ARGS__, UINT_MAX)) \
(O_RWL_QRESOURCE_DEFINE(o_rwl_qresource_ ## __VA_ARGS__))

#define ORWL_QRESOURCE_INIT(...)                                  \
P99_IF_LT(P99_NARG(o_rwl_qresource_ ## __VA_ARGS__), 2)           \
(O_RWL_QRESOURCE_INIT(o_rwl_qresource_ ## __VA_ARGS__, UINT_MAX)) \
(O_RWL_QRESOURCE_INIT(o_rwl_qresource_ ## __VA_ARGS__))

#define ORWL_QRESOURCE(NAME) orwl_qresource_load(&o_rwl_qresource_ ## NAME)

#define ORWL_QSECTION(...)                                     \
P99_IF_LT(P99_NARG(__VA_ARGS__), 2)                            \
(O_RWL_QRESOURCE_SECTION(o_rwl_qresource_ ## __VA_ARGS__, 1u)) \
(O_RWL_QRESOURCE_SECTION(o_rwl_qresource_ ## __VA_ARGS__))
#endif

ORWL_QRESOURCE_DECLARE(CPU);
ORWL_QRESOURCE_DECLARE(L1);
ORWL_QRESOURCE_DECLARE(L2);
ORWL_QRESOURCE_DECLARE(L3);


#endif
