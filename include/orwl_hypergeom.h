// This may look like C code, but it really is -*- mode: c; coding: utf-8 -*-
//
#ifndef ORWL_HYPERGEOM_H
#define ORWL_HYPERGEOM_H

/// @file

#include <math.h>
#include "p99_rand.h"

/// @brief Sample a hypergeometric distribution.
///
/// This is distribution of an urn experiment with @a total balls
/// (black or white) and amoung them @a whites white balls. If we draw
/// @a draw balls from such an urn, how many white balls will we have
/// in this draw?

size_t
orwl_randHypergeom(p99_seed seed[static 1],
              size_t total,
              size_t whites,
              size_t draw);

/// @brief Sample a variate hypergeometric distribution.
///
/// This generalizes the simple hypergeometric distribution to
/// multiple succesive draws that depend on each other.
void
orwl_varRandHypergeom (p99_seed seed[static 1],
                  size_t total,
                  size_t whites,
                  size_t draws,
                  size_t draw[static draws]);

/// @brief Similar to par::sys::randHypergeom.
///
/// The difference is that @a N1 is the number of blacks and @a N2
/// the number of whites.
inline
size_t
orwl_hyperprox(p99_seed seed[static 1],
               size_t N1,
               size_t N2,
               size_t T) {
  size_t ret = 0;
  ret = orwl_randHypergeom(seed, N1 + N2, N1, T);
  return(ret);
}

#endif
