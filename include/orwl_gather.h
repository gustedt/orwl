/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the author and copyright holder for this work is                           */
/* all rights reserved,  2012 Jens Gustedt, INRIA, France                     */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#ifndef     ORWL_GATHER_H_
# define    ORWL_GATHER_H_

#include "orwl_comstate.h"

P99_PROTOTYPE(void, orwl_gather_action, orwl_comstate*, void*, size_t);

/**
 ** @brief Reserve local location ID @a LOC for use with a gather
 ** collective operation.
 **/
#define ORWL_GATHER_LOCATION(LOC) ORWL_COMSTATE_LOCATION(LOC, false, true)

/**
 ** @brief Reserve local location ID @a LOC for use with a all-gather
 ** collective operation.
 **/
#define ORWL_ALLGATHER_LOCATION(LOC) ORWL_COMSTATE_LOCATION(LOC, true, true)

#define O_RWL_GATHER_DECLARE_(T, LOC, ROOT, SRV)  ORWL_COMSTATE_DECLARE_ARRAY(T, LOC, ROOT, SRV)

#define O_RWL_GATHER_DECLARE(...) O_RWL_GATHER_DECLARE_(__VA_ARGS__)

/**
 ** @brief declare a variable that can be used with ::ORWL_GATHER
 **
 ** @param T is the type of the reduction variable. It must be such
 ** that <code>typedef T NAME;</code> is a valid declaration. If you
 ** want to use an array, you should @c typedef it beforehand and use
 ** that type name for @a T.
 **
 ** @param LOC is the name of the new variable. It must be the same as
 ** the name of a location that has been declared with
 ** ::ORWL_LOCATIONS_PER_TASK in global scope.
 **
 ** @param ROOT is the id of the location that will hold the final
 ** result. It defaults to the corresponding id at task 0.
 **
 ** @param SRV is the server thread that handles this request. In most
 ** cases this may be omitted. It defaults to the usually unique
 ** server thread for the process.
 **
 ** @see ORWL_GATHER
 ** @see ORWL_LOCATIONS_PER_TASK
 ** @see ORWL_GATHER_LOCATION
 **/
P99_DEFARG_DOCU(ORWL_GATHER_DECLARE)
P00_DOCUMENT_TYPE_ARGUMENT(ORWL_GATHER_DECLARE, 0)
P00_DOCUMENT_IDENTIFIER_ARGUMENT(ORWL_GATHER_DECLARE, 1)
#ifdef DOXYGEN
#define ORWL_GATHER_DECLARE(T, LOC, ROOT, SRV)
#else
#define ORWL_GATHER_DECLARE(...) O_RWL_GATHER_DECLARE(P99_CALL_DEFARG_LIST(ORWL_GATHER_DECLARE, 4, __VA_ARGS__))
#define ORWL_GATHER_DECLARE_defarg_2() SIZE_MAX
#define ORWL_GATHER_DECLARE_defarg_3() orwl_server_get()
#endif

#ifdef DOXYGEN
#define ORWL_ALLGATHER_DECLARE(T, LOC, ROOT, SRV)
#else
#define ORWL_ALLGATHER_DECLARE(...) O_RWL_GATHER_DECLARE(P99_CALL_DEFARG_LIST(ORWL_ALLGATHER_DECLARE, 4, __VA_ARGS__))
#define ORWL_ALLGATHER_DECLARE_defarg_2() SIZE_MAX
#define ORWL_ALLGATHER_DECLARE_defarg_3() orwl_server_get()
#endif

/**
 ** @brief perform a gather operation on @a LOC
 **
 ** @param LOC must be a "variable" that is declared through
 ** ::ORWL_GATHER_DECLARE. The type of that variable and array of the
 ** type @c T that was given there. The array size is
 ** the number of tasks in the execution, ::orwl_nt. So @a LOC is then
 ** a variable array of type @c T[orwl_nt].
 **
 ** Here is an almost complete example that performs the global sum of
 ** @c double values, one for each task.
 **
 ** @code
 ** // in file scope, possibly in a header file:
 ** // declare a location that will be used to propagate the information
 ** ORWL_LOCATIONS_PER_TASK(..., gather_loc, ...);
 ** ORWL_GATHER_LOCATION(gather_loc);
 **
 ** // in file scope, in a .c file:
 ** ORWL_LOCATIONS_PER_TASK_INSTANTIATION();
 **
 ** // inside your function:
 ** // declare the reduction. root is taken as 0 if omitted.
 ** ORWL_GATHER_DECLARE(double, gather_loc, root);
 ** // From here there is now VLA that behaves the same as if declared as
 ** // double gather_loc[orwl_nt];
 **
 ** // As VLA cannot be initialized, the contents is uninitialized for now.
 ** // Write the value for this task in the item that corresponds to this task.
 ** gather_loc[orwl_mytid] = init_val;
 **
 ** ... place your other requests ...
 **
 ** //
 ** orwl_schedule_grouped();
 **
 ** for (size_t orwl_phase = 0; orwl_phase < 10; ++orwl_phase) {
 **   // assign a value as you would to any other variable
 **   gather_loc[orwl_mytid] = my_precious_result;
 **
 **   ORWL_GATHER(gather_loc, other);
 **
 **   // only the root can use the collected value, for other tasks
 **   // this only holds an intermediate value, now.
 **   if (orwl_myloc == root) {
 **      report(1, "the values of the others are");
 **      for (size_t i = 0; i < orwl_nt; ++i) report(1, " %f", gather_loc[i]);
 **   }
 ** }
 ** @endcode
 **
 ** @remark the type @c T of the variable that is declared with
 ** ::ORWL_GATHER_DECLARE must be such that <code>typedef T
 ** NAME;</code> is a valid declaration. In particular to be of an
 ** array type you have to @c typedef the type beforehand.
 **
 ** @remark @a LOC acts as a local (@c auto) variable in its
 ** scope. You can take the address, pass it to functions etc as you
 ** would do for any other local variable.
 **
 ** @see ORWL_GATHER_DECLARE
 ** @see ORWL_LOCATIONS_PER_TASK
 **/
#define ORWL_GATHER(LOC)                                       \
(                                                              \
 (ORWL_COMSTATE[O_RWL_COMNAME(LOC)].data = &LOC),              \
 (ORWL_COMSTATE[O_RWL_COMNAME(LOC)].el_size = sizeof(LOC[0])), \
 (orwl_comstate_gather(&ORWL_COMSTATE[O_RWL_COMNAME(LOC)])),   \
 (ORWL_COMSTATE[O_RWL_COMNAME(LOC)].data = 0),                 \
 (ORWL_COMSTATE[O_RWL_COMNAME(LOC)].el_size = 0))

/**
 ** @brief perform a gather operation on @a LOC that additionally
 ** broadcasts the result vector of the operation to all participating
 ** tasks
 **
 ** The semantics are the same as for ::ORWL_GATHER, only that the
 ** result at the end is available for all tasks.
 **
 ** @warning This is less efficient than ::ORWL_GATHER since the
 ** communication has to go in both directions. So if you only need
 ** the result vector in one place just do that operation instead.
 **
 ** @see ORWL_GATHER
 ** @see ORWL_ALLGATHER_DECLARE
 ** @see ORWL_LOCATIONS_PER_TASK
 **/
#define ORWL_ALLGATHER(LOC)                                     \
(                                                               \
 (ORWL_COMSTATE[O_RWL_COMNAME(LOC)].data = &LOC),               \
 (ORWL_COMSTATE[O_RWL_COMNAME(LOC)].el_size = sizeof(LOC[0])),  \
 (orwl_comstate_gather(&ORWL_COMSTATE[O_RWL_COMNAME(LOC)])),    \
 (orwl_comstate_broadcast(&ORWL_COMSTATE[O_RWL_COMNAME(LOC)])), \
 (ORWL_COMSTATE[O_RWL_COMNAME(LOC)].data = 0),                  \
 (ORWL_COMSTATE[O_RWL_COMNAME(LOC)].el_size = 0))

#define ORWL_ALLGATHER_DISCONNECT ORWL_COMSTATE_DISCONNECT
#define ORWL_GATHER_DISCONNECT ORWL_COMSTATE_DISCONNECT

#endif
