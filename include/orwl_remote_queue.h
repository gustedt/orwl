/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the authors and copyright holders for this work are as follows:            */
/* all rights reserved,  2011 Emmanuel Jeanvoine, INRIA, France               */
/* all rights reserved,  2010-2014, 2016 Jens Gustedt, INRIA, France          */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#ifndef     ORWL_REMOTE_QUEUE_H_
# define    ORWL_REMOTE_QUEUE_H_

#include "orwl_wait_queue.h"
#include P99_ADVANCE_ID
#include "orwl_proc_symbols.h"
#include "p99_try.h"

/**
 ** @addtogroup library_critical
 ** @{
 **/

/**
 ** @brief A structure to regulate queues between different servers.
 **
 ** An orwl_mirror is a local data structure that represents an
 ** abstract @em %location in a ORWL (ordered read-write lock)
 ** system. It is initialized with two ::orwl_endpoint
 ** specifications. One corresponds to the address of the location
 ** that is to be mirrored, it may be local in the same process or
 ** remote on another host. The other ::orwl_endpoint corresponds to
 ** the address of the current process and will be used by others to
 ** connect to this host here to reply to requests.
 **
 ** @msc
 **   orwl_server,orwl_mirror,cli_wh,orwl_handle;
 **   cli_wh<-orwl_handle [label="orwl_request()", URL="\ref orwl_request()", ID="1"];
 **   orwl_mirror<-cli_wh [label="orwl_request()", URL="\ref orwl_request()", ID="1"];
 **   orwl_server<-orwl_mirror [label="orwl_rpc()"];
 **   orwl_proc<-orwl_server [label="orwl_proc_request()"];
 **   orwl_proc;
 **   orwl_proc->orwl_server [label="Ack"];
 **   orwl_server->orwl_mirror [label="orwl_rpc()"];
 **   orwl_mirror->orwl_handle [label="Ack "];
 **   cli_wh<-orwl_handle [label="orwl_acquire"];
 **   orwl_proc->cli_wh [label="acquired"];
 **   cli_wh->orwl_handle [label="orwl_acquired"];
 **   cli_wh<-orwl_handle [label="orwl_release"];
 **   cli_wh<-orwl_handle [label="orwl_release"];
 ** @endmsc
 ** @see orwl_handle
 **/
struct orwl_mirror {

  /** @privatesection
   ** @{
   **/

  orwl_server* srv;   /**< the local endpoint to which we report */
  orwl_endpoint there;  /**< the remote that centralizes the order */
  orwl_wq local;        /**< the local queue that interfaces the
                           remote */

  /**
   ** @}
   **/

};

P99_DECLARE_ONCE_CHAIN(orwl_mirror);

/**
 ** @see orwl_mirror
 **/
#define ORWL_MIRROR_INITIALIZER(SRV, THERE) {                  \
  .local = ORWL_WQ_INITIALIZER,                                \
  .srv = SRV,                                                  \
  .there = THERE,                                              \
  }

#ifndef DOXYGEN
inline
P99_PROTOTYPE(orwl_mirror *, orwl_mirror_init, orwl_mirror *, orwl_server*, orwl_endpoint);

#define orwl_mirror_init(...) P99_CALL_DEFARG(orwl_mirror_init, 3, __VA_ARGS__)
#endif

DOCUMENT_INIT(orwl_mirror)
P99_DEFARG_DOCU(orwl_mirror_init)
inline
orwl_mirror *orwl_mirror_init(orwl_mirror *rq, /*!< [out] the object to iniialize */
                              orwl_server *srv, /*!< [in] local, defaults to a temp variable */
                              orwl_endpoint t  /*!< [in] remote, defaults to a temp variable */
                             ) {
  if (rq) {
    rq->srv = srv;
    rq->there = t;
    orwl_wq_init(&rq->local);
  }
  return rq;
}

P99_DECLARE_DEFARG(orwl_mirror_init, , , );
#define orwl_mirror_init_defarg_1() P99_0(orwl_server*)
#define orwl_mirror_init_defarg_2() P99_LVAL(orwl_endpoint, .index = 0 )

DOCUMENT_DESTROY(orwl_mirror)
inline
void orwl_mirror_destroy(orwl_mirror *rq) {
  orwl_wq_destroy(&rq->local);
  orwl_endpoint_destroy(&rq->there);
}

DECLARE_NEW_DELETE(orwl_mirror);

#ifndef DOXYGEN
P99_PROTOTYPE(void, orwl_mirror_connect, orwl_mirror *, orwl_endpoint, int64_t, orwl_server*);
#define orwl_mirror_connect(...) P99_CALL_DEFARG(orwl_mirror_connect, 4, __VA_ARGS__)
#define orwl_mirror_connect_defarg_2() -INT64_C(1)
#define orwl_mirror_connect_defarg_3() orwl_server_get()
#endif

/**
 ** @brief Initialize and connect a ::orwl_mirror @a rq with local
 ** server @a srv to the endpoint @a endp
 ** @related orwl_mirror
 **/
void orwl_mirror_connect(orwl_mirror *rq, orwl_endpoint endp, int64_t index, orwl_server* srv);


/**
 ** @brief An ORWL lock handle for remote locations.
 **
 ** In an ORWL (ordered read-write lock) system an ::orwl_handle is
 ** used to control locks that an application process issues on a
 ** local or remote lock location. As the rw in ORWL indicates, locks
 ** can be shared-read or exclusive-write. With ORWL a typical locking
 ** sequence is to
 ** -# announce a future locking by issuing a @em request, via
 **   ::orwl_handle_read_request or ::orwl_handle_write_request
 ** -# do some other work
 ** -# ensure that the lock is obtained by a blocking call to ::orwl_handle_acquire
 ** -# do the work that is critical and that now is protected by the lock
 ** -# release the lock by means of ::orwl_handle_release.
 **
 ** Obtaining a read (=shared) lock on a remote location means that
 ** other handles may have read locks simultaneously on the same
 ** location, but that a write lock is attributed to at most one
 ** handle at a time.
 **
 ** ORWL imposes a strict first come first serve (FIFO) policy for
 ** obtaining the locks:
 **
 ** - The earlier a @em request arrives at a location the earlier the
 **   corresponding acquire will succeed.
 ** - An acquire will block as long as there is an earlier
 **   incompatible lock on the same location.
 **
 ** All calls to the ORWL functions (but ::orwl_handle_acquire) in such a
 ** sequence should be immediate and consume only few resources: the
 ** idea that most of the lock administration is done asynchronously
 ** behind the scenes and that the application only has to wait (in
 ** ::orwl_handle_acquire) if this is unavoidable since the lock is hold by
 ** another handle.
 **
 ** Deviations from the above sequence are possible. ::orwl_handle_test can
 ** be used to know if the lock can be obtained without
 ** blocking.
 **
 ** The application may associate data to each location of which it
 ** also may control the size, ::orwl_handle_map and ::orwl_handle_resize.
 **
 ** @see orwl_mirror
 ** @see orwl_read_request
 ** @see orwl_write_request
 ** @see orwl_acquire
 ** @see orwl_test
 ** @see orwl_release
 ** @see orwl_map
 ** @see orwl_resize
 **/
struct orwl_handle {
  /** @privatesection
   ** @{
   **/

  /**
   ** @brief The queue that regulates the local accesses.
   **/
  orwl_mirror *rq;
  /**
   ** @brief The handle in the local queue.
   **/
  orwl_wh_ref whr;
  /**
   ** @brief An ID of a local or remote handle that holds the global lock.
   **
   ** This will be notified whence here locally we have released the lock.
   **/
  uint64_t svrID;

  /**
   ** @}
   **/

};

P99_DECLARE_ONCE_CHAIN(orwl_handle);

/**
 ** @see orwl_handle
 **/
#define ORWL_HANDLE_INITIALIZER { .rq = 0, .whr = ORWL_WH_REF_INITIALIZER(0), .svrID = P99_0(uint64_t) }

DOCUMENT_INIT(orwl_handle)
inline
orwl_handle *orwl_handle_init(orwl_handle *rh) {
  if (!rh) return 0;
  rh->rq = 0;
  orwl_wh_ref_init(&rh->whr, 0);
  rh->svrID = 0;
  return rh;
}

DOCUMENT_DESTROY(orwl_handle)
inline
void orwl_handle_destroy(orwl_handle *rh) {
  orwl_wh_ref_trigger(&rh->whr);
  orwl_handle_init(rh);
}

DECLARE_NEW_DELETE(orwl_handle);

#define O_RWL_DOCUMENT_VECTOR                                                                                \
/*! \note This operates atomically on a vector of handles. The length of this vector is given by @a size. */

#define O_RWL_DOCUMENT_SEED                                                                                                                                                                                                \
/*! @remark Because this function might open a socket it needs an additional parameter @a seed for a random generator. This argument is provided by default, so usually you don't have to worry and may simply omit it. */

/**
 ** @brief Insert a set of write requests in the FIFO at the set of locations @a rq
 ** @related orwl_mirror
 **
 ** Once such a write request will be achieved
 ** this will be the only lock (read or write) that can be hold by any
 ** handle simultaneously.
 **/
O_RWL_DOCUMENT_VECTOR
O_RWL_DOCUMENT_SEED
P99_DEFARG_DOCU(orwl_handle_write_request)
orwl_state orwl_handle_write_request(orwl_mirror rq[],/*!< [in,out] the location for the request */
                                     orwl_handle rh[],/*!< [in,out] the handle for the request */
                                     size_t size,     /*!< [in] vector size, defaults to 1 */
                                     p99_seed* seed   /*!< [in] defaults to a thread local seed */
                                    );

/**
 ** @brief Insert a set of read requests @a rh in the FIFO at the set of locations @a rq
 ** @related orwl_mirror
 **
 ** Once such a read request will be achieved other @em read request
 ** can be granted to other handles simultaneously.
 **/
O_RWL_DOCUMENT_VECTOR
O_RWL_DOCUMENT_SEED
P99_DEFARG_DOCU(orwl_handle_read_request)
orwl_state orwl_handle_read_request(orwl_mirror rq[],/*!< [in,out] the location for the request */
                                    orwl_handle rh[],/*!< [in,out] the handle for the request */
                                    size_t size,     /*!< [in] vector size, defaults to 1 */
                                    p99_seed* seed   /*!< [in] defaults to a thread local seed */
                                   );

/**
 ** @brief Schedule the insertion of a write requests in the FIFO at the set of locations @a rq
 ** @related orwl_mirror
 **
 ** Once such a write request will be achieved this will be the only
 ** lock (read or write) that can be hold by any handle
 ** simultaneously.
 **
 ** This version delays the insertion until later. All such requests
 ** will then be inserted according to the priority @a priority (lower
 ** value first).
 **
 ** @see orwl_schedule_grouped
 **/
O_RWL_DOCUMENT_SEED
P99_DEFARG_DOCU(orwl_handle_write_insert)
void orwl_handle_write_insert(orwl_handle * rh,/*!< [in,out] the handle for the request */
                              size_t locid,   /*!< defaults to the id of the current task's main location */
                              size_t priority, /*!< defaults to the id of the current task's main location */
                              p99_seed* seed,   /*!< [in] defaults to a thread local seed */
                              orwl_server* srv,
                              orwl_mirror * rq/*!< [in,out] the location for the request */

                             );

/**
 ** @brief Schedule the insertion of a read requests in the FIFO at the set of locations @a rq
 ** @related orwl_mirror
 **
 **
 ** Once such a read request will be achieved other @em read request
 ** can be granted to other handles simultaneously.
 **
 ** This version delays the insertion until later. All such requests
 ** will then be inserted according to the priority @a priority (lower
 ** value first).
 **/
O_RWL_DOCUMENT_SEED
P99_DEFARG_DOCU(orwl_handle_read_insert)
void orwl_handle_read_insert(orwl_handle * rh,/*!< [in,out] the handle for the request */
                             size_t locid,
                             size_t priority, /*!< defaults to the id of the current task's main location */
                             p99_seed* seed,   /*!< [in] defaults to a thread local seed */
                             orwl_server* srv,
                             orwl_mirror * rq/*!< [in,out] the location for the request */
                            );

/**
 ** Release the lock(s) that a set @a rh of handles has obtained on its location(s)
 ** @related orwl_handle
 **
 ** This also invalidates any data address that might have been
 ** obtained through a call to ::orwl_handle_map and sends the modified data
 ** back to the local or remote server, if necessary.
 ** @todo Keep track if the data has been mapped and only send it back
 ** in that case.
 **/
O_RWL_DOCUMENT_VECTOR
O_RWL_DOCUMENT_SEED
P99_DEFARG_DOCU(orwl_handle_release)
orwl_state orwl_handle_release(orwl_handle rh[],  /*!< [in,out] the handle to be released */
                               size_t size,       /*!< [in] the size of the vector */
                               p99_seed* seed     /*!< [in] defaults to a thread local seed */
                              );

#ifndef DOXYGEN
P99_PROTOTYPE(orwl_state, orwl_handle_write_request, orwl_mirror*, orwl_handle*, size_t, p99_seed*);
P99_PROTOTYPE(orwl_state, orwl_handle_read_request, orwl_mirror*, orwl_handle*, size_t, p99_seed*);
P99_PROTOTYPE(void, orwl_handle_write_insert, orwl_handle*, size_t, size_t, p99_seed*, orwl_server*, orwl_mirror*);
P99_PROTOTYPE(void, orwl_handle_read_insert, orwl_handle*, size_t, size_t, p99_seed*, orwl_server*, orwl_mirror*);
P99_PROTOTYPE(orwl_state, orwl_handle_release, orwl_handle*, size_t, p99_seed*);
#define orwl_handle_release(...)  P99_CALL_DEFARG(orwl_handle_release, 3, __VA_ARGS__)
#define orwl_handle_release_defarg_1() 1u
#define orwl_handle_release_defarg_2() p99_seed_get()

inline P99_PROTOTYPE(orwl_state, orwl_handle_test, orwl_handle*, size_t, p99_seed*);
#endif

/**
 ** @brief Check if handle @a rh has been requested for an inclusive lock.
 **
 ** @a rh may be in a state of being requested or having already
 ** acquired.
 ** @related orwl_handle
 **/
inline
bool orwl_inclusive(orwl_handle* rh) {
  if (!rh) return false;
  orwl_wh* wh = orwl_wh_ref_get(&rh->whr);
  return (wh && wh->svrID);
}

/**
 ** @brief Block until a set of previously issued read or write request can
 ** be fulfilled
 ** @related orwl_handle
 ** @todo By means of a version counter avoid to copy data over the
 ** wire that we know already.
 **/
O_RWL_DOCUMENT_VECTOR
inline
orwl_state orwl_handle_acquire(orwl_handle rh[], size_t size, p99_seed* seed) {
  orwl_state ret =  orwl_state_amount;
  for (size_t i = 0; i < size; ++i) {
    if (rh) {
      ORWL_TIMING(acquire) {
        orwl_state state = orwl_wh_acquire(orwl_wh_ref_get(&rh[i].whr));
        if (state < ret) ret = state;
      }
    }
  }
  return ret;
}

#ifndef DOXYGEN
inline
P99_PROTOTYPE(orwl_state, orwl_handle_acquire, orwl_handle*, size_t, p99_seed*);
#define orwl_handle_acquire(...)  P99_CALL_DEFARG(orwl_handle_acquire, 3, __VA_ARGS__)
#define orwl_handle_acquire_defarg_1() 1u
#define orwl_handle_acquire_defarg_2() p99_seed_get()
#endif

/**
 ** @brief Test if a set of previously issued read or write request can
 ** be fulfilled
 ** @related orwl_handle
 **
 ** Returns the "minimum" value among the return values for all @a
 ** size elements of the vector @a rh, i.e ::orwl_invalid if at least
 ** one of the handles is invalid, ::orwl_requested if at least one is
 ** not yet acquired and ::orwl_acquired if all have been acquired.
 **/
O_RWL_DOCUMENT_VECTOR
inline
orwl_state orwl_handle_test(orwl_handle rh[], size_t size, p99_seed * seed) {
  orwl_state ret =  orwl_state_amount;
  for (size_t i = 0; i < size; ++i) {
    if (rh)
      ORWL_TIMING(test) {
      orwl_state res = orwl_wh_test(orwl_wh_ref_get(&rh[i].whr));
      if (res < ret) ret = res;
    } else {
      ret = orwl_invalid;
    }
  }
  return ret;
}

/**
 ** @brief Obtain address and size of the data that is associated to a
 ** location for reading and writing
 ** @related orwl_handle
 ** The application may associate data to each location of which it
 ** also may control the size. Once the lock is acquired for a given
 ** handle, this data is available through ::orwl_handle_map, returning a
 ** pointer to the data and to its size. The pointer to the data will
 ** be invalid, as soon as the lock is again released.
 **
 ** @param rh the handle in question
 ** @param [out] data_len the length of the data in bytes. This
 ** argument can be omitted or set to @c 0 if the size of the data is
 ** not needed in return from the function.
 **
 ** @pre The handle @a rh must hold a write lock on the location to
 ** which it is linked.
 **
 ** @return An address to access the data that is associated with the
 ** handle.
 **
 ** @throw EPERM If the handle @a rh only had been requested for a
 ** shared lock.
 **
 ** @throw EINVAL If the handle @a rh only had not been
 ** requested for any kind of lock.
 **
 ** @warning The return address is only valid until @a rh is
 ** released.
 ** @warning The return address may (and will) be different between
 ** different calls to that function.
 ** @warning The new content of the data will only be visible for
 ** other lock handles once they obtain a lock after this handle
 ** releases its write lock.
 **
 ** @see orwl_mapro for the case that the lock that is hold is a read
 ** lock and / or the data should only be read.
 **
 ** @todo Keep track if we have mapped this data for writing via a
 ** "dirty" flag.
 **
 ** @todo Make sure that this interface (and ::orwl_handle_read_map) don't do
 ** the endian transformation. They are supposed to work on
 ** uninterpreted bytes.
 **/
inline
void* orwl_handle_write_map(orwl_handle* rh, size_t* data_len, p99_seed* seed) {
  uint64_t* ret = 0;
  ORWL_TIMING(write_map) {
    if (data_len) *data_len = 0;
    if (rh)
      switch (orwl_handle_test(rh, 1, seed)) {
      case orwl_acquired: ;
        if (orwl_inclusive(rh)) P99_THROW(EPERM);
        orwl_wh* wh = orwl_wh_ref_get(&rh->whr);
        P99_THROW_ASSERT(EINVAL, wh);
        ret = orwl_wh_map(wh, data_len);
        if (ret && data_len)
          *data_len *= sizeof(uint64_t);
      default:;
      }
  }
  return ret;
}

/**
 ** @brief Obtain address and size of the data that is associated to a
 ** location for reading
 ** @related orwl_handle
 ** @pre The handle @a rh must hold a read lock on the location to
 ** which it is linked.
 ** @param rh the handle in question
 ** @param [out] data_len the length of the data in bytes. This
 ** argument can be omitted or set to @c 0 if the size of the data is
 ** not needed in return from the function.
 **
 ** @return An address to access the data that is associated with the
 ** location. The return address is @b not @c const qualified,
 ** otherwise it would be too tedious to interpret the value as
 ** pointer to multi-dimensional @c const qualified matrix.
 **
 ** @throw EINVAL If the handle @a rh only had not been
 ** requested for any kind of lock.
 **
 ** @warning The return address is only valid until @a rh is
 ** released.
 **
 ** @warning The return address may (and will) be different between
 ** different calls to that function.
 **
 ** @remark Though the object to which the return value points to is
 ** modifiable, its contents will be lost as soon as the handle @a rh
 ** is released. This can be a convenient way to initialize a
 ** temporary variable with a desired content.
 **
 ** @see orwl_handle_write_map for the case that the lock that is hold is a write
 ** lock the data and should also be written to persistently.
 **/
inline
void* orwl_handle_read_map(orwl_handle* rh, size_t* data_len, p99_seed* seed) {
  uint64_t* ret = 0;
  ORWL_TIMING(read_map) {
    if (data_len) *data_len = 0;
    if (rh)
      switch (orwl_handle_test(rh, 1, seed)) {
      case orwl_acquired: ;
        {
          orwl_wh* wh = orwl_wh_ref_get(&rh->whr);
          P99_THROW_ASSERT(EINVAL, wh);
          ret = orwl_wh_map(wh, data_len);
          if (ret &&  data_len)
            *data_len *= sizeof(uint64_t);
        }
      default:;
      }
  }
  return ret;
}

/**
 ** @brief Get the offset in bytes of data resource to which @a rh
 ** links.
 ** @related orwl_handle
 **/
inline
size_t orwl_handle_offset(orwl_handle* rh, p99_seed* seed) {
  size_t ret = 0;
  ORWL_TIMING(read_map) {
    if (rh)
      switch (orwl_handle_test(rh, 1, seed)) {
      case orwl_acquired: ;
        {
          orwl_wh* wh = orwl_wh_ref_get(&rh->whr);
          P99_THROW_ASSERT(EINVAL, wh);
          orwl_wq *wq = wh->location;
          P99_THROW_ASSERT(EINVAL, wq);
          orwl_alloc*alloc = orwl_alloc_ref_get(&wq->allocr);
          P99_THROW_ASSERT(EINVAL, alloc);
          ret = alloc->offset;
        }
      default:;
      }
  }
  return ret;
}

/**
 ** @brief Shrink or extend the data that is associated to a location.
 ** @related orwl_handle
 ** Initially, the data of a location is empty, i.e of 0 size. If the
 ** lock that a handle holds is exclusive ::orwl_handle_resize can be used to
 ** resize the data to a new length. If such a resize operation
 ** is an extension of existing data that data is preserved and the
 ** newly appended area is filled with zero bytes.
 ** @warning @a data_len is accounted in elements of a width of 64 bits.
 **
 ** @pre The handle @a rh must hold a write (exclusive) lock on the
 ** location to which it is linked.
 ** @todo Update a "dirty" flag.
 ** @todo Rename this to something like @c orwl_resize64 to make it
 ** clear that this acts on 64 bit elements.
 ** @see orwl_truncate for a variant that only accounts for bytes
 **/
inline
void orwl_handle_resize(orwl_handle* rh, size_t data_len) {
  if (orwl_handle_test(rh, 1, 0) > orwl_valid) {
    orwl_wh* wh = orwl_wh_ref_get(&rh->whr);
    P99_THROW_ASSERT(EINVAL, wh);
    orwl_wh_resize(wh, data_len);
  }
}

#define orwl_resize orwl_handle_resize

/**
 ** @brief Shrink or extend the data that is associated to a location.
 ** @related orwl_handle
 ** Initially, the data of a location is empty, i.e of 0 size. If the
 ** lock that a handle holds is exclusive ::orwl_handle_truncate can be used to
 ** resize the data to a new length. If such a resize operation
 ** is an extension of existing data that data is preserved and the
 ** newly appended area is filled with zero bytes.
 ** @warning @a data_len is accounted in bytes but the real length
 ** that is associated is the next multiple that can hold elements of
 ** width 64 bit.
 **
 ** @pre The handle @a rh must hold a write (exclusive) lock on the
 ** location to which it is linked.
 ** @todo Update a "dirty" flag.
 ** @see orwl_resize for a variant that accounts for 64 bit elements
 **/
inline
void orwl_handle_truncate(orwl_handle* rh, size_t data_len, p99_seed* seed) {
  ORWL_TIMING(truncate) {
    if (orwl_handle_test(rh, 1, seed) > orwl_valid) {
      size_t len = data_len / sizeof(uint64_t);
      len += (data_len % sizeof(uint64_t)) ? 1 : 0;
      orwl_wh* wh = orwl_wh_ref_get(&rh->whr);
      P99_THROW_ASSERT(EINVAL, wh);
      orwl_wh_resize(wh, len);
    }
  }
}

inline
void orwl_handle_attach(orwl_handle* rh, orwl_alloc_ref* ar, p99_seed* seed) {
  ORWL_TIMING(handle_attach) {
    if (orwl_handle_test(rh, 1, seed) > orwl_valid) {
      orwl_wh* wh = orwl_wh_ref_get(&rh->whr);
      P99_THROW_ASSERT(EINVAL, wh);
      orwl_wq *wq = wh->location;
      orwl_alloc*alloc = orwl_alloc_ref_get(ar);
      orwl_wh_resize(wh, alloc->size/sizeof(uint64_t));
      orwl_alloc_ref_replace(&wq->allocr, alloc);
    }
  }
}

/**
 ** @brief Associate an existing resource allocation to a location
 **
 ** Since this changes the data that is associated, the location must
 ** have been acquired for exclusive access before such an operation.
 **/
inline
void orwl_handle_replace(orwl_handle* rh, orwl_alloc_ref* ar, p99_seed* seed) {
  ORWL_TIMING(handle_replace) {
    if (orwl_handle_test(rh, 1, seed) == orwl_acquired) {
      if (orwl_inclusive(rh))  P99_THROW(EPERM);
      orwl_wh_ref whr = ORWL_WH_REF_INITIALIZER(0);
      orwl_wh_ref_assign(&whr, &rh->whr);
      orwl_wh* wh = orwl_wh_ref_get(&whr);
      P99_THROW_ASSERT(EINVAL, wh);
      orwl_wq *wq = wh->location;
      P99_THROW_ASSERT(ENOLCK, wq);
      P99_WRLOCK(wq->rwlock) {
        orwl_alloc_ref_assign(&wq->allocr, ar);
        wq->onhold = false;
      }
      orwl_wh_ref_destroy(&whr);
    }
  }
}

/**
 ** @brief Put the data of the location o @a HANDLE on hold for the
 ** current task and return it.
 **
 ** This can be seen as a smart copy operation. Its effect is that the
 ** returned entity holds a "local" copy of the data, only that this
 ** procedure might reuse a copy of the resource that is already
 ** available. Only when this is not possible, a new copy of the data
 ** is created.
 **
 ** @warning If the returned buffer is not taken care off hereafter,
 ** bad things can happen. Memory (and the contained data) can be lost
 ** and leaking.
 **
 ** The handle must have been acquired before the operation, but the
 ** lock that is obtained might be shared.
 **
 ** @see orwl_handle_replace to know what to do with such a buffer
 **
 ** Per default the location is "marked" such that we keep track that
 ** there now is an external link to that same resource. The parameter
 ** @a mark can be used to suppress that marking. Only use that if you
 ** know what you are doing.
 **/
O_RWL_DOCUMENT_SEED
inline
void orwl_handle_hold(orwl_handle* rh, orwl_alloc_ref* ar, p99_seed* seed, bool mark) {
  ORWL_TIMING(handle_hold) {
    if (orwl_handle_test(rh, 1, seed) == orwl_acquired) {
      orwl_wh_ref whr = ORWL_WH_REF_INITIALIZER(0);
      orwl_wh_ref_assign(&whr, &rh->whr);
      orwl_wh* wh = orwl_wh_ref_get(&whr);
      P99_THROW_ASSERT(EINVAL, wh);
      orwl_wq *wq = wh->location;
      P99_THROW_ASSERT(ENOLCK, wq);
      P99_WRLOCK(wq->rwlock) {
        wq->onhold = mark;
        orwl_alloc_ref_assign(ar, &wq->allocr);
      }
      orwl_wh_ref_destroy(&whr);
    }
  }
}

inline
P99_PROTOTYPE(void, orwl_handle_hold, orwl_handle*, orwl_alloc_ref*, p99_seed*, bool);
#define orwl_handle_hold(...)  P99_CALL_DEFARG(orwl_handle_hold, 4, __VA_ARGS__)
#define orwl_handle_hold_defarg_2() p99_seed_get()
#define orwl_handle_hold_defarg_3() true


/**
 ** @brief Scale the object corresponding to position @a pos in server
 ** thread @a serv to size @a size
 **
 ** This is a short cut to set the size of an object before doing any
 ** operation on it.
 **
 ** @see orwl_truncate for a more general operation that can be issued
 ** at any time of the execution.
 **
 ** @a size is the size of the object that is expected at the
 ** location. Generally this should be done with a @c sizeof operator,
 ** e.g something like <code>sizeof(double[n][n])</code>
 **
 ** @a pos and @a serv are optional. If they are omitted the position
 ** ::orwl_myloc in the default server is chosen.
 **
 **/
void orwl_scale(size_t size, size_t pos, orwl_server* serv);

P99_PROTOTYPE(void, orwl_scale, size_t, size_t, orwl_server*);
#define orwl_scale(...)  P99_CALL_DEFARG(orwl_scale, 3, __VA_ARGS__)
#define orwl_scale_defarg_1() SIZE_MAX
#define orwl_scale_defarg_2() 0

/**
 ** @brief Attach a new allocator @a alloc at location @a loc
 **
 ** This can evidently only work for allocation within the same
 ** process. As a result the allocator is changed for the location at
 ** server @a serv if @a loc belongs to it. If it doesn't @a alloc is
 ** attached to the local mirror of location @a loc in @a serv's
 ** address book.
 **/
void orwl_attach(orwl_alloc_ref* alloc, size_t loc, orwl_server* serv);

P99_PROTOTYPE(void, orwl_attach, orwl_alloc_ref*, size_t, orwl_server*);
#define orwl_attach(...)  P99_CALL_DEFARG(orwl_attach, 3, __VA_ARGS__)
#define orwl_attach_defarg_1() SIZE_MAX
#define orwl_attach_defarg_2() 0

/**
 ** @brief associate a specialized allocator to a location
 **
 ** @param size gives the size of the resource. Use ::ORWL_ALLOC_MAP,
 ** ::ORWL_ALLOC_FETCH or ::ORWL_ALLOC_SNATCH when you want to
 ** associate the whole of a pre-existing resource such as a file.
 **
 ** @param name associates a name to the resource. E.g, for files,
 ** this will be the filename, for segments the segment name, etc. You
 ** can provide that even for unnamed resources to ease debugging,
 ** eventually. default: 0
 **
 ** @param func is the associated function. default: the heap allocator
 **
 ** @param pos the ORWL location ID. default: the principal location
 ** for this thread
 **
 ** @param serv the server of the location. default: something
 ** reasonable
 **
 ** @return the allocation variable that is attached to that
 ** location. If you use that return value, you should use
 ** ::orwl_alloc_account immediately to count the reference.
 **
 ** @see orwl_allocate_file to force allocation in a file and for an
 ** example
 ** @see orwl_allocate_shm to force allocation in a segment
 ** @see orwl_allocate_anon to force an anonymous allocation
 ** @see orwl_allocate_hard to use a fixed address
 ** @see orwl_attach for the rules where this allocation is visible
 **/
inline
void orwl_allocate(orwl_alloc_ref* ar,
                   size_t size,
                   char const*name,
                   orwl_alloc_realloc_func* func,
                   size_t pos,
                   orwl_server* serv) {
  orwl_alloc*ret = P99_NEW(orwl_alloc, name);
  ret->alloc = func;
  orwl_alloc_realloc(ret, size);
  orwl_alloc_ref_replace(ar, ret);
  orwl_attach(ar, pos, serv);
}

inline
P99_PROTOTYPE(void, orwl_allocate,
              orwl_alloc_ref*,
              size_t,
              char const*,
              orwl_alloc_realloc_func*,
              size_t,
              orwl_server*);
#define orwl_allocate(...)  P99_CALL_DEFARG(orwl_allocate, 6, __VA_ARGS__)
#define orwl_allocate_defarg_2() 0
#define orwl_allocate_defarg_3() 0
#define orwl_allocate_defarg_4() SIZE_MAX
#define orwl_allocate_defarg_5() 0


/**
 ** @brief associate a fixed address @a data to a location @a loc
 **
 ** @param data is the address that is to be used by location @a loc.
 **
 ** @param size gives the size of the data block behin @a data that
 ** can be used by the location
 **
 ** @param loc the ORWL location ID. default: the principal location
 ** for this thread
 **
 ** @param name associates a name to the resource. Only used for
 ** debugging. default: 0
 **
 ** @param serv the server of the location. default: something
 ** reasonable
 **
 ** @return the allocation variable that is attached to that
 ** location. If you use that return value, you should use
 ** ::orwl_alloc_account immediately to count the reference.
 **
 ** @see orwl_allocate_file to force allocation in a file and for an
 ** example
 ** @see orwl_allocate_shm to force allocation in a segment
 ** @see orwl_allocate_anon to force an anonymous allocation
 ** @see orwl_attach for the rules where this allocation is visible
 **/
inline
void orwl_allocate_hard(orwl_alloc_ref* ar,
                        void* data,
                        size_t size,
                        size_t loc,
                        char const*name,
                        orwl_server* serv) {
  orwl_alloc * ret = P99_NEW(orwl_alloc, name);
  ret->alloc = orwl_alloc_realloc_hard;
  ret->data = data;
  ret->max_size = size;
  orwl_alloc_realloc(ret, size);
  orwl_alloc_ref_replace(ar, ret);
  orwl_attach(ar, loc, serv);
}

inline
P99_PROTOTYPE(void, orwl_allocate_hard,
              orwl_alloc_ref*,
              void*, size_t,
              size_t,
              char const*, orwl_server*);
#define orwl_allocate_hard(...)  P99_CALL_DEFARG(orwl_allocate_hard, 6, __VA_ARGS__)
#define orwl_allocate_hard_defarg_3() SIZE_MAX
#define orwl_allocate_hard_defarg_4() 0
#define orwl_allocate_hard_defarg_5() 0


/**
 ** @brief associate a file allocator to the principal location of
 ** this ORWL task.
 **
 ** A simple use of this would look like
 ** @code
 ** orwl_allocate_file(sizeof(double[][]), "toto");
 ** @endcode
 **
 ** If you want to keep the allocator around to manipulate the file
 ** later on:
 ** @code
 ** orwl_alloc* alloc = orwl_allocate_file(sizeof(double[][]), "toto");
 ** orwl_alloc_account(alloc);
 ** ...
 ** ...
 ** orwl_alloc_realloc(alloc, ORWL_ALLOC_UNLINK);
 ** orwl_alloc_delete(alloc);
 ** @endcode
 **
 ** @see orwl_allocate_shm to force allocation in a segment
 ** @see orwl_allocate_anon to force an anonymous allocation
 **/
#define orwl_allocate_file(...)                                \
P99_IF_LT(P99_NARG(__VA_ARGS__), 3)                            \
(o_rwl_allocate_file2(__VA_ARGS__))                            \
(o_rwl_allocate_file3(__VA_ARGS__))

#define o_rwl_allocate_file2(SIZE, NAME) orwl_allocate((SIZE), (NAME), orwl_alloc_realloc_file)
#define o_rwl_allocate_file3(SIZE, NAME, ...) orwl_allocate((SIZE), (NAME), orwl_alloc_realloc_file, __VA_ARGS__)

#define orwl_allocate_shm(...)                                 \
P99_IF_LT(P99_NARG(__VA_ARGS__), 3)                            \
(o_rwl_allocate_shm2(__VA_ARGS__))                             \
(o_rwl_allocate_shm3(__VA_ARGS__))

#define o_rwl_allocate_shm2(SIZE, NAME) orwl_allocate((SIZE), (NAME), orwl_alloc_realloc_shm)
#define o_rwl_allocate_shm3(SIZE, NAME, ...) orwl_allocate((SIZE), (NAME), orwl_alloc_realloc_shm, __VA_ARGS__)

#define orwl_allocate_anon(...)                                \
P99_IF_LT(P99_NARG(__VA_ARGS__), 3)                            \
(o_rwl_allocate_anon2(__VA_ARGS__))                            \
(o_rwl_allocate_anon3(__VA_ARGS__))

#define o_rwl_allocate_anon2(SIZE, NAME) orwl_allocate((SIZE), (NAME), orwl_alloc_realloc_anon)
#define o_rwl_allocate_anon3(SIZE, NAME, ...) orwl_allocate((SIZE), (NAME), orwl_alloc_realloc_anon, __VA_ARGS__)

inline
P99_PROTOTYPE(void*, orwl_handle_write_map, orwl_handle*, size_t*, p99_seed*);

inline
P99_PROTOTYPE(void*, orwl_handle_read_map, orwl_handle*, size_t*, p99_seed*);

/**
 ** @}
 **/

#endif      /* !ORWL_REMOTE_QUEUE_H_ */
