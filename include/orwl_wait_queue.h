/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the authors and copyright holders for this work are as follows:            */
/* all rights reserved,  2011 Emmanuel Jeanvoine, INRIA, France               */
/* all rights reserved,  2010-2014 Jens Gustedt, INRIA, France                */
/* all rights reserved,  2011 Matias E. Vara, INRIA, France                   */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#ifndef     ORWL_WAIT_QUEUE_H_
# define    ORWL_WAIT_QUEUE_H_

#include "orwl_register.h"
#include "orwl_endpoint.h"
#include "p99_count.h"
#include P99_ADVANCE_ID

#define atomic_compare_exchange_check(...)  P99_THROW_CALL_NOT_ZERO(atomic_compare_exchange_weak, EINVAL, __VA_ARGS__)

/**
 ** @addtogroup library_low
 ** @{
 **/

#ifdef __cplusplus
extern "C" {
#endif

/** @brief Return type for @c orwl functions
 **/
P99_DECLARE_ENUM(orwl_state,
                 orwl_valid , //= thrd_success,
                 orwl_invalid , //= thrd_error,
#ifndef EAGAIN
                 EAGAIN,
#define EAGAIN EAGAIN
#endif
                 orwl_again , //= EAGAIN,
#ifndef ENOLCK
                 ENOLCK,
#define ENOLCK ENOLCK
#endif
                 orwl_nolck , //= ENOLCK,
                 orwl_requested,
                 orwl_acquired
                );

/** @var orwl_state orwl_invalid
 ** call with an invalid object
 **/
/** @var orwl_state orwl_again
 ** a call observed a transient condition that made it fail, try again
 **/
/** @var orwl_state orwl_nolck
 ** ORWL locks are not available
 **/
/** @var orwl_state orwl_valid
 ** object valid, but not requested
 **/
/** @var orwl_state orwl_requested
 ** unspecific request was placed
 **/
/** @var orwl_state orwl_acquired
 ** unspecific request was acquired
 **/


#ifndef DOXYGEN
inline
P99_PROTOTYPE(orwl_state*, orwl_state_init, orwl_state *, orwl_state);
#define orwl_state_init(...) P99_CALL_DEFARG(orwl_state_init, 2, __VA_ARGS__)
#define orwl_state_init_defarg_1() orwl_invalid
#endif

DOCUMENT_INIT(orwl_state)
P99_DEFARG_DOCU(orwl_state_init)
inline
orwl_state* orwl_state_init
(orwl_state *el,                    /*!< wait queue to initialize */
 orwl_state val                     /*!< defaults to orwl_invalid */
) {
  *el = val;
  return el;
}

DOCUMENT_DESTROY(orwl_state)
inline
void orwl_state_destroy(orwl_state *el) {
  /* special care for bogus warning given by icc */
  (void)el;
}

DECLARE_NEW_DELETE(orwl_state);

P99_DECLARE_ONCE_CHAIN(orwl_wq);

P99_DECLARE_ONCE_CHAIN(orwl_wh);

DOCUMENT_INIT(orwl_wh)
P99_DEFARG_DOCU(orwl_wh_init)
orwl_wh* orwl_wh_init
(orwl_wh *wh, /*!< the handle to be initialized */
 size_t tok,  /*!< the initial number of tokens */
 orwl_wh * next /* a possible next element */
);

#ifndef DOXYGEN
P99_PROTOTYPE(orwl_wh*, orwl_wh_init, orwl_wh *, size_t, orwl_wh*);
#define orwl_wh_init(...) P99_CALL_DEFARG(orwl_wh_init, 3, __VA_ARGS__)
#define orwl_wh_init_defarg_1() 1u
#define orwl_wh_init_defarg_2() ((void*)0)
#endif

DOCUMENT_DESTROY(orwl_wh)
void orwl_wh_destroy(orwl_wh *wh);

#define ORWL_WH_REF_INITIALIZER(VAL) P99_TP_REF_INITIALIZER(VAL, orwl_wh_account)

P99_TP_REF_DECLARE(orwl_wh);


/**
 ** @brief A proactive locking object with FIFO policy.
 **
 ** Locks on such an object can only be achieved through a handle of
 ** type orwl_wh. Such a lock is not bound to the process or
 ** thread that issued the lock request, but to the handle object.
 **/
struct orwl_wq {

  /** @privatesection
   ** @{
   **/

  mtx_t mut;  /**< The mutex used to control the access to the queue */
  p99_rwl rwlock;  /**< The rwlock used to control the access to the data */
  orwl_wh_ref head;        /**< The head of the priority queue */
  orwl_wh_ref tail;        /**< The tail of the priority queue */
  uint64_t clock;       /**< A counter that is increased at each
                           event that this queue encounters. */
  orwl_alloc_ref allocr; /**< The data that is associated with this
                           queue */
  bool borrowed;        /**< True if the data is not to be released
                           from here. */
  bool onhold;        /**< True if the data is on hold outside a critical section. */
  /**
   ** @}
   **/

};


/**
 ** @brief The handle type corresponding to ::orwl_wq.
 **
 ** Locks through such a handle object are achieved in a two-step
 ** procedure. First, a lock is @em requested through
 ** ::orwl_wq_request. This binds the ::orwl_wh to a
 ** designated ::orwl_wq and appends the request to the FIFO
 ** queue of it. Then in a second step, ::orwl_wh_acquire waits
 ** until the request has become the first in the queue.
 **
 ** An ::orwl_wh is loaded with #tokens. Each ::orwl_wq_request places
 ** tokes on the corresponding handle and ::orwl_wh_acquire removes
 ** these tokens. It is up to the code that uses a handle to watch
 ** that the number of placed and removed tokens match.
 **
 ** Initially, an ::orwl_wh should always hold at least one token,
 ** since it is considered to be released, once the number of tokens
 ** has reached 0. Tokens can only be added if the actual number is
 ** greater than 0.
 **
 ** Depending on the field #svrTICK a handle can be in an inclusive or
 ** exclusive state. In inclusive state and when it is still at the
 ** tail of the corresponding queue subsequent calls ::orwl_wq_request
 ** may load additional tokens on the handle. Such a ::orwl_wq_request
 ** will always be refused for a handle that is in exclusive state.
 **
 ** ::orwl_wh_release releases the lock by popping the request from
 ** the FIFO. Thus, it allows for the following request in the FIFO
 ** (if any) to be acquired. A ::orwl_wh_release may only be performed
 ** if all tokens have been removed, i.e if all lock holders have had
 ** notice that the lock is acquired. If #tokens is not 0 whence
 ** ::orwl_wh_release is called, the call blocks until the condition
 ** is fulfilled.
 **
 **/
struct orwl_wh {

  /** @privatesection
   ** @{
   **/

  /** The location to which this wh links. */
  orwl_wq *location;
  /** The next wh in the priority queue. */
  orwl_wh_ref next;
  _Atomic(size_t) p99_cnt;
  /** @brief The number of tokens that have been loaded on this wh.
   **
   ** Generally this will reflect the number of threads that may
   ** wait for this handle to be acquired. A wh can only be released
   ** when this has dropped to zero and this counter should never be
   ** incremented again, once it has dropped to 0.
   **
   ** A wh should never be initialized with a value of 0, since this
   ** may immediately trigger destruction.
   **/
  p99_count tokens;
  /** @brief This is set iff the orwl_wh is acquired.
   **
   ** This allows to check for this condition atomically. Acquire can
   ** then be guaranteed by just blocking on this field.
   **/
  p99_notifier acq;
  /**
   ** @brief A clock tick noting the insertion of a wh in a wq
   **/
  uint64_t tick;
  /**
   ** @brief An ID of a local or remote orwl_wh in case that this
   ** orwl_wh represents an inclusive lock.
   **/
  uint64_t svrID;

  /**
   ** @}
   **/

};


DECLARE_NEW_DELETE(orwl_wh);
P99_TP_REF_FUNCTIONS(orwl_wh);


#ifndef DOXYGEN
#define orwl_wh_ref_init(...) P99_CALL_DEFARG(orwl_wh_ref_init, 2, __VA_ARGS__)
#endif


void
orwl_wh_ref_trigger(orwl_wh_ref *whr);


DECLARE_NEW_DELETE(orwl_wh_ref);

/**
 ** @see orwl_wq
 **/
#define ORWL_WQ_INITIALIZER                                    \
  {                                                            \
    .head = ORWL_WH_REF_INITIALIZER(0),                        \
    .tail = ORWL_WH_REF_INITIALIZER(0),                        \
    .clock = 1,                                                \
    .allocr = ORWL_ALLOC_REF_INITIALIZER(0),                   \
    .borrowed = false,                                         \
 }

DOCUMENT_INIT(orwl_wq)
P99_DEFARG_DOCU(orwl_wq_init)
orwl_wq* orwl_wq_init
(orwl_wq *wq,                    /*!< wait queue to initialize */
 int attr                        /*!< defaults to a plain mutex */
);

#ifndef DOXYGEN
P99_PROTOTYPE(orwl_wq*, orwl_wq_init, orwl_wq*, int);
#define orwl_wq_init(...) P99_CALL_DEFARG(orwl_wq_init, 2, __VA_ARGS__)
#define orwl_wq_init_defarg_1() (mtx_plain)
#endif

DOCUMENT_DESTROY(orwl_wq)
void orwl_wq_destroy(orwl_wq *wq);

DECLARE_NEW_DELETE(orwl_wq);

/**
 ** @brief Test @a wq for idleness.
 **
 ** Idleness here means that there is no ::orwl_wh in the FIFO queue
 ** of this location.
 **
 ** This supposes that @a wq is not a null pointer.
 **
 ** @related orwl_wq
 **/
inline
int orwl_wq_idle(orwl_wq *wq) {
  orwl_wh *wq_head = orwl_wh_ref_get(&wq->head);
  orwl_wh *wq_tail = orwl_wh_ref_get(&wq->tail);
  return !wq_head && !wq_tail;
}


/**
 ** @see orwl_wh
 **/
#define ORWL_WH_INITIALIZER(NEXT) {                            \
  .next = ORWL_WH_REF_INITIALIZER(NEXT),                       \
  .p99_cnt = ATOMIC_VAR_INIT(0),                               \
  .tokens = P99_COUNT_INITIALIZER(INT_MAX),                    \
  .acq = P99_NOTIFIER_INITIALIZER                              \
  }


/**
** @brief Insert a list of request @a wh into location @a wq.
**
** You call it by something like
** @code
** orwl_state state = orwl_wq_request(wq, wh);
** @endcode
**
** @return ::orwl_invalid if any of @a wh or @a wq was
** invalid. Otherwise returns ::orwl_requested.
**
** @related orwl_wq
**/
orwl_state orwl_wq_request(orwl_wq *wq, /*!< the queue to act on */
                           orwl_wh *wh
                          );

/**
** @brief Insert a list of request pair of <code>(wh,
** howmuch)</code> into location @a wq.
**
** You call it by something like
** @code
** orwl_state state = orwl_wq_try_request(wq, &wh, howmuch0)
** @endcode
**
** @return ::orwl_invalid if any of @c wh or @a wq was
** invalid. Otherwise returns ::orwl_requested.
**
** For a @c *wh that doesn't point to a null pointer, this inserts
** the handle @c **wh into the queue and places @a howmuch tokens on
** @c **wh. @c **wh will only be possible to be released if, first,
** it is acquired (that is it moves front in the FIFO) and then if
** all tokens are unloaded with ::orwl_wh_acquire or ::orwl_wh_test.
**
** A @c *wh that is a null pointer is considered to relate to the
** last such handle that is at the tail of the queue, if such a
** handle exists. Such a null-request will check if this trailing
** handle is in inclusive state and if so will place the tokens on
** that handle and return the address of that handle in @c *wh. If
** the trailing handle doesn't exist or if it is in exclusive state
** the call fails and returns ::orwl_invalid.
**
** The tokens are only considered to be loaded on @c **wh if the call
** is successful.
**
** @related orwl_wq
**/
orwl_state o_rwl_wq_try_request_locked(orwl_wq *wq, /*!< the queue to act on */
                                       orwl_wh_ref *whr);


/**
 ** @brief Insert a list of handles into the queue where we know
 ** that the lock is already held.
 **
 ** @related orwl_wq
 ** @private
 **/
void o_rwl_wq_request_append_locked(orwl_wq *wq,  /*!< the locked queue to act on */
                                    orwl_wh *wh  /*!< the first handle to be inserted */
                                   );

/**
 ** @brief Link this queue to a different data buffer
 **
 ** @related orwl_wq
 ** @private
 **/
void o_rwl_wq_link_wlocked(orwl_wq *wq,       /*!< the locked queue to act on */
                           orwl_buffer* data,    /*!< data buffer that is provided
                                                     from elsewhere */
                           bool borrowed      /*!< whether this location here
                                                  is responsible for the data */
                          );

/**
 ** @brief Unlink and return the data buffer of the queue of @a wh
 **
 ** @see orwl_wq_unlink_locked
 ** @related orwl_wh
 ** @private
 **/
void o_rwl_wh_unlink(orwl_wh* wh, orwl_alloc_ref* ar);

/**
 ** @brief Acquire a pending request on @c *wh. Blocking until the
 ** request is acquired.
 **
 ** @return ::orwl_invalid if @a wh was invalid, or if there
 ** was no request pending on @c *wh. Otherwise it eventually blocks and
 ** then returns ::orwl_acquired.
 **
 ** @related orwl_wh
 **/
P99_DEFARG_DOCU(orwl_wh_acquire)
orwl_state orwl_wh_acquire
(orwl_wh *wh       /*!< the handle to act upon */
);

/**
 ** @brief Test for a pending request on @c *wh. Never blocking.
 **
 ** @return ::orwl_invalid if @a wh was invalid, ::orwl_valid if
 ** there was no request pending on @c *wh, ::orwl_requested if a
 ** request is pending that is not yet acquired, and ::orwl_acquired
 ** if a request is already acquired.
 **
 ** The tokens are considered to be removed from @c *wh iff the call
 ** returns ::orwl_acquired.
 **
 ** @related orwl_wh
 **/
P99_DEFARG_DOCU(orwl_wh_test)
orwl_state orwl_wh_test(orwl_wh *wh /*!< the handle to act upon */
                       );
/**
 ** @brief Release a request on @c *wh. If it had been acquired this
 ** is blocking until all tokens are unloaded.
 **
 ** @return ::orwl_invalid if @a wh was invalid, or if there was no
 ** request acquired for @c *wh. Otherwise it returns ::orwl_valid.
 **
 ** @related orwl_wh
 **/
orwl_state orwl_wh_release(orwl_wh *wh /*!< the handle to act upon */);


#ifndef DOXYGEN
inline
P99_PROTOTYPE(uint64_t, orwl_wh_load_conditionally, orwl_wh *, uint64_t);
#define orwl_wh_load_conditionally(...) P99_CALL_DEFARG(orwl_wh_load_conditionally, 2, __VA_ARGS__)
#define orwl_wh_load_conditionally_defarg_1() 1
#endif

/**
 ** @brief if there still tokens on @a wh load @a howmuch additional tokens.
 **
 ** This supposes that the corresponding @c wq is not a null pointer
 ** and that its mutex is already locked.
 ** @see orwl_wh_unload
 **
 ** @related orwl_wh
 ** @private
 **/
P99_DEFARG_DOCU(orwl_wh_load_conditionally)
inline
uint64_t orwl_wh_load_conditionally
(orwl_wh *wh /*!< the handle to act upon */,
 uint64_t howmuch  /*!< defaults to 1 */) {
  return p99_count_inc_conditionally(&wh->tokens, howmuch);
}

#ifndef DOXYGEN
inline
P99_PROTOTYPE(uint64_t, orwl_wh_unload, orwl_wh *, uint64_t);
#define orwl_wh_unload(...) P99_CALL_DEFARG(orwl_wh_unload, 2, __VA_ARGS__)
#define orwl_wh_unload_defarg_1() UINT64_C(1)
#endif

/**
 ** @brief unload @a howmuch additional tokens from @a wh.
 **
 ** If by this action the token count drops to zero, eventual
 ** waiters for this @a wh are notified.
 **
 ** @return The number of tokens after the decrement.
 ** @warning If the number falls to @c 0 it is the responsibility of
 ** the caller to release @c *wh.
 **
 ** @see orwl_wh_load
 **
 ** @related orwl_wh
 ** @private
 */
P99_DEFARG_DOCU(orwl_wh_unload)
inline
uint64_t orwl_wh_unload
(orwl_wh *wh /*!< the handle to act upon */,
 uint64_t howmuch  /*!< defaults to 1 */) {
  return p99_count_dec(&wh->tokens, howmuch);
}


void o_rwl_wh_trigger(orwl_wh *wh /*!< the handle to act upon */
                     );


/**
 ** @related orwl_wq
 ** @private
 */
uint64_t* o_rwl_wq_map_rlocked(orwl_wq* wq, size_t* data_len);

/**
 ** @related orwl_wq
 ** @private
 */
void o_rwl_wq_resize_wlocked(orwl_wq* wq, size_t len);

/**
 ** @brief Obtain address and size of the data that is associated to a
 ** location for reading and writing
 ** @related orwl_wh
 ** The application may associate data to each location of which it
 ** also may control the size. Once the lock is acquired for a given
 ** handle, this data is available through ::orwl_wh_map, returning a
 ** pointer to the data and to its size. The pointer to the data will
 ** be invalid, as soon as the lock is again released.
 **
 ** @pre The handle @a wh must hold a lock on the location to
 ** which it is linked.
 **
 ** @param wh the handle in question
 ** @param data_len [out] the length of the data in number of elements
 **
 ** @return An address to access the data that is associated with the
 ** location.
 **
 ** @throw EINVAL If the handle @a wh only had not been
 ** requested for any kind of lock.
 **
 ** @warning The return address is only valid until @a wh is
 ** released.
 ** @warning The return address may (and will) be different between
 ** different calls to that function.
 ** @warning The new content of the data will only be visible for
 ** other lock handles once they obtain a lock after this handle
 ** releases its lock.
 **
 ** @todo Keep track if we have mapped this data for writing via a
 ** "dirty" flag.
 **/
uint64_t* orwl_wh_map(orwl_wh* wh, size_t* data_len);

/**
 ** @brief resize the data correspondig to @a wh to length @a data_len
 **
 ** @warning this accounts the length in words (i.e @c uint64_t) and
 ** not in bytes.
 **/
void orwl_wh_resize(orwl_wh* wh, size_t data_len);

#ifdef __cplusplus
}
#endif

/**
 ** @}
 **/

#endif
