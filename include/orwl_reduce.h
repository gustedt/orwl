/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the author and copyright holder for this work is                           */
/* all rights reserved,  2012 Jens Gustedt, INRIA, France                     */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#ifndef     ORWL_REDUCE_H_
# define    ORWL_REDUCE_H_

#include "orwl_comstate.h"

/**
 ** @brief Reserve local location ID @a LOC for use with a reduce
 ** collective operation.
 **/
#define ORWL_REDUCE_LOCATION(LOC) ORWL_COMSTATE_LOCATION(LOC, false, true)

/**
 ** @brief Reserve local location ID @a LOC for use with a all-reduce
 ** collective operation.
 **/
#define ORWL_ALLREDUCE_LOCATION(LOC) ORWL_COMSTATE_LOCATION(LOC, true, true)

#define O_RWL_REDUCE_DECLARE_(T, LOC, ROOT, SRV) ORWL_COMSTATE_DECLARE(T, LOC, ROOT, SRV)

#define O_RWL_REDUCE_DECLARE(...) O_RWL_REDUCE_DECLARE_(__VA_ARGS__)

/**
 ** @brief declare a variable that can be used with ::ORWL_REDUCE
 **
 ** @param T is the type of the reduction variable. It must be such
 ** that <code>typedef T NAME;</code> is a valid declaration. If you
 ** want to use an array, you should @c typedef it beforehand and use
 ** that type name for @a T.
 **
 ** @param LOC is the name of the new variable. It must be the same as
 ** the name of a location that has been declared with
 ** ::ORWL_LOCATIONS_PER_TASK in global scope.
 **
 ** @param ROOT is the id of the location that will hold the final
 ** result. It defaults to the corresponding id at task 0.
 **
 ** @param SRV is the server thread that handles this request. In most
 ** cases this may be omitted. It defaults to the usually unique
 ** server thread for the process.
 **/
P00_DOCUMENT_IDENTIFIER_ARGUMENT(ORWL_REDUCE_DECLARE, 1)
P99_DEFARG_DOCU(ORWL_REDUCE_DECLARE)
/**
 ** @see ORWL_REDUCE
 ** @see ORWL_REDUCE_LOCATION
 ** @see ORWL_LOCATIONS_PER_TASK
 **/
#ifdef DOXYGEN
#define ORWL_REDUCE_DECLARE(T, LOC, ROOT, SRV)
#else
#define ORWL_REDUCE_DECLARE(...) O_RWL_REDUCE_DECLARE(P99_CALL_DEFARG_LIST(ORWL_REDUCE_DECLARE, 4, __VA_ARGS__))
#define ORWL_REDUCE_DECLARE_defarg_2() SIZE_MAX
#define ORWL_REDUCE_DECLARE_defarg_3() orwl_server_get()
#endif

/**
 ** @brief Analogous to ::ORWL_REDUCE_DECLARE, only that the
 ** collective operation will be all-reduce, that is the result of the
 ** reduction will be communicated back to all the tasks.
 **/
#ifdef DOXYGEN
#define ORWL_ALLREDUCE_DECLARE(T, LOC, ROOT, SRV)
#else
#define ORWL_ALLREDUCE_DECLARE(...) O_RWL_REDUCE_DECLARE(P99_CALL_DEFARG_LIST(ORWL_ALLREDUCE_DECLARE, 4, __VA_ARGS__))
#define ORWL_ALLREDUCE_DECLARE_defarg_2() SIZE_MAX
#define ORWL_ALLREDUCE_DECLARE_defarg_3() orwl_server_get()
#endif

/**
 ** @brief perform a reduction operation on @a LOC
 **
 ** @param LOC must be a "variable" that is declared through
 ** ::ORWL_REDUCE_DECLARE, the type is the one that was given there.
 **
 ** @param OTHER is an identifier that will be used to specify the
 ** reduction operation. In the depending statement or block this is
 ** declared to be of the same type as @a LOC and holds a value that
 ** already has been collected from other tasks.
 **
 ** Here is an almost complete example that performs the global sum of
 ** @c double values, one for each task.
 **
 ** @code
 ** // in file scope, possibly in a header file:
 ** // declare a location that will be used to propagate the information
 ** ORWL_LOCATIONS_PER_TASK(..., reduce_loc, ...);
 ** ORWL_REDUCE_LOCATION(reduce_loc);
 **
 ** // in file scope, in a .c file:
 ** ORWL_LOCATIONS_PER_TASK_INSTANTIATION();
 **
 ** // inside your function:
 ** // declare the reduction. root is taken as 0 if omitted
 ** ORWL_REDUCE_DECLARE(double, reduce_loc, root) = init_val;
 **
 ** ... place your other requests ...
 **
 ** //
 ** orwl_schedule_grouped();
 **
 ** for (size_t orwl_phase = 0; orwl_phase < 10; ++orwl_phase) {
 **   // assign a value as you would to any other variable
 **   reduce_loc = my_precious_result;
 **
 **   // define your reduction operation in the depending block
 **   ORWL_REDUCE(reduce_loc, other) {
 **      reduce_loc += other;
 **   }
 **
 **
 **   // only the root can use the collected value, for other tasks
 **   // this only holds an intermediate value, now.
 **   if (orwl_myloc == root) {
 **      report(1, "global sum is %f", reduce_loc);
 **   }
 ** }
 ** @endcode
 **
 ** The depending block here just has a @c += operator. The local
 ** operation that is performed here is stuck together to form the
 ** overall reduction operation.
 **
 ** @remark the type @c T of the variable that is declared with
 ** ::ORWL_REDUCE_DECLARE must be such that <code>typedef T
 ** NAME;</code> is a valid declaration. In particular to be of an
 ** array type you have to @c typedef the type beforehand.
 **
 ** @remark @a LOC and @a OTHER act as local (@c auto) variables in
 ** their scope. You can take the address, pass it to functions etc as
 ** you would do for any other local variable.
 **
 ** @see ORWL_REDUCE_DECLARE
 ** @see ORWL_LOCATIONS_PER_TASK
 **/
P99_BLOCK_DOCUMENT
#define ORWL_REDUCE(LOC, OTHER) O_RWL_REDUCE(0, LOC, OTHER)

/**
 ** @brief perform a reduction operation on @a LOC that broadcasts the
 ** result of the operation to all participating tasks
 **
 ** The semantics are the same as for ::ORWL_REDUCE, only that the
 ** result at the end is available for all tasks.
 **
 ** @warning This is less efficient than ::ORWL_REDUCE since the
 ** communication has to go in both directions. So if you only need
 ** the result of the operation in one place just do that operation
 ** instead.
 **/
P99_BLOCK_DOCUMENT
/**
 ** @see ORWL_REDUCE
 ** @see ORWL_ALLREDUCE_DECLARE
 ** @see ORWL_LOCATIONS_PER_TASK
 **/
#define ORWL_ALLREDUCE(LOC, OTHER) O_RWL_REDUCE(1, LOC, OTHER)


#define O_RWL_REDUCE(ALL, LOC, OTHER)                                                                               \
P00_BLK_START                                                                                                       \
P00_BLK_START                                                                                                       \
P00_BLK_DECL(register orwl_comstate*const, o_rwl_red, &ORWL_COMSTATE[O_RWL_COMNAME(LOC)])                           \
  P00_BLK_DECL(register void*, o_rwl_data, &(LOC))                                                                  \
  P99_IF_EQ_1(ALL)(P00_BLK_BEFAFT((o_rwl_red->data = o_rwl_data),                                                   \
                                  orwl_comstate_broadcast(o_rwl_red),                                               \
                                  (o_rwl_red->data = 0)))()                                                         \
  /* Eventually blocks if the communication of the previous round wasn't terminated.*/                              \
  ORWL_SECTION(&o_rwl_red->here[0])                                                                                 \
  /* Obtain a pointer to the buffer in our virtual address space. */                                                \
  P00_BLK_DECL(register O_RWL_COMTYPE(LOC)*const, hval, orwl_handle2_write_map(&o_rwl_red->here[0]))                \
  /* Ensure that the computed value is transmitted towards the target */                                            \
  P00_BLK_AFTER(memcpy(hval, o_rwl_data, sizeof LOC))                                                               \
  /* Process the children in the tree if there are any */                                                           \
  for (unsigned o_rwl_i = 0; o_rwl_i < o_rwl_red->children; ++o_rwl_i)                                              \
    /* this will block until the data is available */                                                               \
    ORWL_SECTION(&o_rwl_red->there[o_rwl_i])                                                                        \
      /* This one is read only. */                                                                                  \
      P00_BLK_DECL(register O_RWL_COMTYPE(LOC)const*const, tval, orwl_handle2_read_map(&o_rwl_red->there[o_rwl_i])) \
      /* Have a new variable for syntactical convenience. Don't initialize, it might be VLA. */                     \
      P00_BLK_BEFORE(O_RWL_COMTYPE(LOC) OTHER)                                                                      \
      P00_BLK_BEFORE(memcpy(&OTHER, tval, sizeof OTHER))                                                            \
      /* Accumulate the value in the depending statement or block */

#define ORWL_REDUCE_DISCONNECT ORWL_COMSTATE_DISCONNECT
#define ORWL_ALLREDUCE_DISCONNECT ORWL_COMSTATE_DISCONNECT

#endif
