/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the author and copyright holder for this work is                           */
/* all rights reserved,  2010-2012 Jens Gustedt, INRIA, France                */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#ifndef     ORWL_RAND_H_
# define    ORWL_RAND_H_

#include "orwl_thread.h"
#include P99_ADVANCE_ID
#include "p99_enum.h"
#include "p99_rand.h"

/**
 ** @addtogroup library_support
 ** @{
 **/

P99_DECLARE_ONCE_CHAIN(orwl_rand);


uint64_t orwl_mix(uint64_t a, uint64_t b);

uint64_t orwl_challenge(uint64_t a);

/**
 ** @brief A string that contains the value of a shared secret between
 ** all the participating processes of an ORWL run
 **
 ** This serves to encrypt the communication over sockets between
 ** different processes. This does not really ensure the privacy
 ** of the exchanged data but it guarantees that processes from
 ** different runs will not easily mix.
 **
 ** You probably don't have to touch that variable from user code.
 **
 **/
ORWL_DECLARE_ENV(ORWL_SECRET);

/**
 ** @}
 **/

#endif      /* !ORWL_RAND_H_ */
