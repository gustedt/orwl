/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the authors and copyright holders for this work are as follows:            */
/* all rights reserved,  2016-2017 Farouk Mansouri, INRIA, France             */
/* all rights reserved,  2016-2017 Jens Gustedt, INRIA, France                */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#ifndef     ORWL_AFFINITY_H_
#define     ORWL_AFFINITY_H_

#if defined(DOXYGEN) || (defined(HAVE_HWLOC) && defined(HAVE_TREEMATCH))

/**
 ** @file
 **
 ** @brief Optimize thread to core affinity.
 **
 ** This module uses the hwloc and treematch libraries to map threads
 ** that correspond to user tasks to CPU cores.
 **
 ** Detection of these libraries is done automatically through
 ** Makefiles that are found in the scripts directory.
 **/

#include <hwloc.h>
#include "orwl.h"
#include <treematch.h>

ORWL_DECLARE_ENV(ORWL_AFFINITY);
ORWL_DECLARE_ENV(ORWL_AFFINITY_OVERSUBSCRIBE);
ORWL_DECLARE_ENV(ORWL_AFFINITY_BINDING);

#define O_RWL_MAX_DEPENDENCY 10000


/**
 ** @brief Dependency structure for each task
 **/
typedef struct o_rwl_dependency o_rwl_dependency;
struct o_rwl_dependency {
  _Atomic(size_t) nb;                     /*!< number of neigboors */
  size_t loc_ids[O_RWL_MAX_DEPENDENCY];   /*!< location id of neigboors */
};

#define O_RWL_INIT_DEPENDENCY {0}

/**
 ** @brief save read/write data dependencies
 **
 ** This is called automatically by ::orwl_handle_write_insert and ::orwl_handle_read_insert.
 **
 ** @return 0 if ok and construct a global variable of dependecies: o_rwl_com.
 ** @see orwl_handle_write_insert
 ** @see orwl_handle_read_insert
 **/
int orwl_dependency_set(size_t task_src, size_t task_dst, size_t location_id);

/**
 ** @brief save size of used locations
 **
 ** This is called automatically by ::orwl_scale.
 **
 ** @return 0 if ok and construct a global variable of locations: o_rwl_locations.
 ** @see orwl_scale
 **/
int orwl_locations_size_set(size_t location_id, size_t location_size);

/**
 ** @brief initialize affinity data structure according to execution behaviours (tasks, locations, ...)
 **
 ** This is called automatically by ::orwl_init.
 **
 ** @return 0 if ok and init global variables: o_rwl_com, o_rwl_locations
 ** @see orwl_init
 **/
int orwl_affinity_init(size_t locations_task, size_t locations_total,
                       size_t locations_offset, size_t locations_local);

/**
 ** @brief destruct affinity data structure
 **
 ** This is called through atexit() at the end of the program
 ** execution.
 **
 **/
void orwl_affinity_destroy(void);

/**
 ** @brief construct affinity matrix from dependencies and locations size and compute treematch solution
 **
 ** This is called from ::orwl_init, executed only by task 0.
 **
 ** @return 0 if ok and construct global variables: o_rwl_aff_mat, o_rwl_topology, o_rwl_solution
 ** @see orwl_init
 **/
int orwl_affinity_compute(size_t, size_t);

/**
 ** @brief set the binding with hwloc
 **
 ** This is called from ::orwl_init, executed only by task 0.
 **
 ** @return 0 if ok and construct global variables: o_rwl_hwloc_topology
 ** @see orwl_init
 **/
int orwl_affinity_set(size_t orwl_task_id, bool auxiliary);

#endif
#endif
