/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the authors and copyright holders for this work are as follows:            */
/* all rights reserved,  2011 Emmanuel Jeanvoine, INRIA, France               */
/* all rights reserved,  2010-2014, 2016 Jens Gustedt, INRIA, France          */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#ifndef     ORWL_SERVER_H_
# define    ORWL_SERVER_H_

#include "orwl_helpers.h"

/**
 ** @addtogroup library_low
 ** @{
 **/

/** @brief Make ORWL much more verbose. */
extern bool orwl_verbose;

/** @brief the last time a global barrier has been visited */
extern _Atomic(double) orwl_epoch;

/** @brief the number of times a global barrier has been visited */
extern _Atomic(size_t) orwl_epoch_count;

/**
 ** @brief Set the verbosity of this ORWL execution
 **
 ** @see orwl_verbose for the variable that then is set to @c true if
 ** the %environment variable was set
 **/
ORWL_DECLARE_ENV(ORWL_VERBOSE);

/**
 ** @brief The list of ORWL task ids for the current process
 **
 ** Usually this should be set by the launch script. The format of
 ** this string is relatively unconstrained. Something like <code>"0 1 2
 ** 3"</code> or <code>"0xaB, 007, 10"</code> would do.
 **/
ORWL_DECLARE_ENV(ORWL_TIDS);

/**
 ** @brief The list of ORWL location ids for the current process
 **
 ** Usually this should be set by the launch script. The format of
 ** this string is relatively unconstrained. Something like <code>"0 1 2
 ** 3"</code> or <code>"0xaB, 007, 10"</code> would do.
 **
 ** If this is not set, the list of locations is deduced automatically
 ** from ::ORWL_TIDS.
 **/
ORWL_DECLARE_ENV(ORWL_LOCIDS);


/**
 ** @brief The list of strings, one for each task, separated by |.
 **
 ** Usually this should be set by the launch script.
 **/
ORWL_DECLARE_ENV(ORWL_TARGS);

P99_DECLARE_ONCE_CHAIN(orwl_server);

P99_DECLARE_STRUCT(o_rwl_grouping);
P99_POINTER_TYPE(o_rwl_grouping);
P99_LIFO_DECLARE(o_rwl_grouping_ptr);

struct o_rwl_grouping {
  o_rwl_grouping_ptr p99_lifo;
  uint64_t pri;
  uint64_t remP;
  bool incl;
  p99_notifier ok;
  p99_notifier launched;
};

inline
o_rwl_grouping* o_rwl_grouping_init(o_rwl_grouping* g, uint64_t pri, bool incl) {
  if (g) {
    *g = (o_rwl_grouping) { .pri = pri, .incl = incl };
  }
  return g;
}

/**
 ** @brief A data structure that represents an ORWL server thread
 **
 ** Such a thread is responsible to receive incoming connections from
 ** other processes (possibly remote) an dispatches the data to
 ** callback threads that will handle the requests.
 **/
struct orwl_server {
  int fd_listen;           /*!< this servers file descriptor */
  int err;                 /*!< an error that occurred through processing */
  orwl_endpoint ep;        /*!< the socket endpoint for this server */
  orwl_addr *aliases;      /*!< a list of addresses of this host */
  p99_notifier up_srv;     /*!< notify successful startup of server */
  p99_notifier term_srv;   /*!< notify termination of server */
  p99_notifier up_ab;      /*!< notify successful startup of addressbook */
  p99_notifier up_sched;   /*!< notify successful scheduling of insertions */
  size_t max_connections;  /*!< maximum number of simultaneous connections */
  size_t max_queues;       /*!< number of locations served by this */
  orwl_wq *wqs;            /*!< the priority queues of these
                              locations, if any */
  orwl_wh_ref *whs;            /*!< handles to block the server, if any */
  char* info;              /*!< an informative string that is
                             presented in the terminal */
  size_t info_len;         /*!< the length of #info */
  orwl_graph* graph;       /*!< the graph used for distributed init */
  orwl_address_book* ab;   /*!< the address book used for distributed init */
  thrd_t id;            /*!< the ID of this server thread */
  mtx_t launch;            /*!< serialize the launching of threads */
  p99_notifier* id_initialized; /*!< needed during initialization */
  uint64_t* global_barrier; /*!< the phase information this server knows about */
  size_t global_barrier_len; /*!< number of location depending of this server */
  mtx_t   global_barrier_mtx; /*!< mutex access to the global barrier */
  cnd_t   global_barrier_cnd; /*!< synchronize modifications of the global barrier */
  size_t ll;
  size_t const* locids;      /*!< location ids */
  size_t lt;
  size_t const* tids;        /*!< tasks ids */
  char const*const* targs;   /*!< task specific arguments for orwl_tenv */
  size_t const* tlens;        /*!< total string lengths of the strings in targs */
  orwl_barrier local_barrier;
  size_t local_elected;
  size_t unblocked_locations; /*!< needed during initialization */
  _Atomic(size_t) block_count; /*!< the number of processes blocking the server */
  p99_count waiters_cnt;
  P99_LIFO(o_rwl_grouping_ptr) waiters_remote;
  P99_LIFO(o_rwl_grouping_ptr) waiters_local;
  size_t first_location;          /*!< the least numbered location local to this server */
  FILE* outgraph;
  size_t volatile node_offset;         /*!< offset of locations inside the node */
  _Atomic(size_t)*volatile node_total; /*!< if set, points to total of locations in the node */
};


/**
 ** @brief Return a default server thread for the actual thread
 **/
P99_DECLARE_THREAD_LOCAL(orwl_server*, o_rwl_server_loc);
#define orwl_server_local P99_THREAD_LOCAL(o_rwl_server_loc)

orwl_server* o_rwl_server_get(void);

/**
 ** @brief Return a default server thread for ORWL
 **
 ** Per default this returns ::orwl_server_local. If that is not set
 ** it sets it to a process global variable and returns that value.
 **
 ** This function is used a lot as a default arguments to a lot of
 ** ORWL functions. Usually this is nothing you'd have to worry about.
 **/
inline
orwl_server* orwl_server_get(void) {
  orwl_server* srv = orwl_server_local;
  if (P99_LIKELY(srv)) return srv;
  else return o_rwl_server_get();
}

#define ORWL_SERVER_INITIALIZER(NAME, MAXC, ADDR, PORT)          \
{                                                                \
  .fd_listen = -1,                                               \
    /*.host = ORWL_HOST_INITIALIZER(NAME.host, ADDR, PORT, 2),*/ \
  .block_count = ATOMIC_VAR_INIT(0),                             \
  .waiters_remote = P99_LIFO_INITIALIZER(0),                     \
  .waiters_local = P99_LIFO_INITIALIZER(0),                      \
  .max_connections = MAXC                                        \
}

DOCUMENT_INIT(orwl_server)
P99_DEFARG_DOCU(orwl_server_init)
orwl_server*
orwl_server_init(orwl_server *serv,       /*!< [out] the object to initialize */
                 size_t max_connections,  /*!< [in] maximum socket queue length,
                                            defaults to 20 */
                 size_t max_queues,       /*!< [in] the maximum number of locations,
                                            defaults to 0 */
                 char const* endp         /*!< [in] defaults to the
                                            null address */
                );

#ifndef DOXYGEN
P99_PROTOTYPE(orwl_server*, orwl_server_init, orwl_server *, size_t, size_t, char const*);
#define orwl_server_init(...) P99_CALL_DEFARG(orwl_server_init, 4, __VA_ARGS__)
#define orwl_server_init_defarg_1() P99_0(size_t)
#define orwl_server_init_defarg_2() P99_0(size_t)
#define orwl_server_init_defarg_3() P99_0(char const*)
#endif



DOCUMENT_DESTROY(orwl_server)
void orwl_server_destroy(orwl_server *serv);

DECLARE_NEW_DELETE(orwl_server);

/**
 ** @brief lauch a server that fulfills rpc requests
 ** @msc
 **   caller1,procedure1,thread1,main,server,thread2,procedure2,caller2;
 **   main -> server [label="orwl_server_create()", URL="\ref orwl_server_create()"];
 **   caller1 -> server [label="orwl_send(procedure1, ...)", URL="\ref orwl_send()"];
 **   caller2 -> server [label="orwl_send(procedure2, ...)", URL="\ref orwl_send()"];
 **   server->thread1 [label="orwl_proc_create(procedure1, ...)", URL="\ref orwl_proc_create()"];
 **   thread1->procedure1 [label="procedure1(...)"];
 **   server->thread2 [label="orwl_proc_create(procedure2, ...)", URL="\ref orwl_proc_create()"];
 **   procedure1->caller1 [label="orwl_proc_untie_caller()", URL="\ref orwl_proc_untie_caller()"];
 **   thread2->procedure2 [label="procedure2(...)"];
 **   procedure1->thread1 [label="return"];
 **   procedure2->caller2 [label="orwl_proc_untie_caller()", URL="\ref orwl_proc_untie_caller()"];
 **   thread1->main [label="thrd_exit()"];
 **   procedure2->thread2 [label="return"];
 **   thread2->main [label="thrd_exit()"];
 **   server->main [label="thrd_exit()"];
 ** @endmsc
 **/
DECLARE_THREAD(orwl_server);

/**
 ** @related orwl_server
 **/
void orwl_server_close(orwl_server *serv);

#ifndef DOXYGEN
P99_PROTOTYPE(void, orwl_server_close, orwl_server *);
#define orwl_server_close(...) P99_CALL_DEFARG(orwl_server_close, 1, __VA_ARGS__)
#define orwl_server_close_defarg_0() orwl_server_get()
#endif


/**
 ** @related orwl_server
 **/
P99_DEFARG_DOCU(orwl_server_terminate)
void
orwl_server_terminate(orwl_server *serv   /*!< the server to terminate */
                     );

#ifndef DOXYGEN
P99_PROTOTYPE(void, orwl_terminate, orwl_server *);
#define orwl_server_terminate(...) P99_CALL_DEFARG(orwl_server_terminate, 1, __VA_ARGS__)
#define orwl_server_terminate_defarg_0() orwl_server_get()
#endif


/**
 ** @brief Block the server initially.
 **
 ** Use this to first insert all handles in their queues and then kick
 ** off the system with ::orwl_server_unblock.
 **
 ** @return The number of blocking request that were present before this one.
 ** @related orwl_server
 **/
size_t orwl_server_block(orwl_server *serv, size_t nb);

#ifndef DOXYGEN
P99_PROTOTYPE(size_t, orwl_server_block, orwl_server *, size_t);
#define orwl_server_block(...) P99_CALL_DEFARG(orwl_server_block, 2, __VA_ARGS__)
#define orwl_server_block_defarg_0() orwl_server_get()
#define orwl_server_block_defarg_1() 1
#endif


/**
 ** @brief Unblock the server.
 **
 ** Use this to kick off the system after having inserted all handles
 ** in their queues.
 **
 ** @return The number of blocking request that remain after this one.
 **
 ** @see orwl_server_block.
 ** @related orwl_server
 **/
size_t orwl_server_unblock(orwl_server *serv, size_t nb);

#ifndef DOXYGEN
P99_PROTOTYPE(size_t, orwl_server_unblock, orwl_server *, size_t);
#define orwl_server_unblock(...) P99_CALL_DEFARG(orwl_server_unblock, 2, __VA_ARGS__)
#define orwl_server_unblock_defarg_0() orwl_server_get()
#define orwl_server_unblock_defarg_1() 1
#endif

/**
 ** @brief Unblock the server once all the local locations are correctly
 **        initialized.
 **
 ** @param srv is a pointer on the local ::orwl_server
 ** @param nb_locations is the number of locations to wait before unblocking
 **        the server
 ** @see orwl_server_block
 ** @see orwl_server_unblock
 **/
void orwl_server_delayed_unblock(orwl_server *srv, size_t nb_locations);

/**
 ** @}
 **/


/**
 ** @addtogroup process_model
 ** @{
 **/

void
orwl_start(size_t max_queues,       /*!< [in] the maximum number of locations,
                                      defaults to 0 */
           size_t max_connections,  /*!< [in] maximum socket queue length,
                                      defaults to 20 */
           orwl_server *serv,       /*!< [out] the server object to initialize */
           bool block,              /*!< [in] block the server when launching,
              defaults to false */
           char const* endp,         /*!< [in] defaults to the
                                      null address */
           size_t locations_amount   /*!< [in] defaults to the number of locations per task */
          );

#ifndef DOXYGEN
P99_PROTOTYPE(void, orwl_start, size_t, size_t, orwl_server *, bool, char const*, size_t);
#define orwl_start(...) P99_CALL_DEFARG(orwl_start, 6, __VA_ARGS__)
#define orwl_start_defarg_0() p99_0(size_t)
#define orwl_start_defarg_1() P99_0(size_t)
#define orwl_start_defarg_2() orwl_server_get()
#define orwl_start_defarg_3() false
#define orwl_start_defarg_4() P99_0(char const*)
#define orwl_start_defarg_5() orwl_locations_amount
#endif

void
o_rwl_stop(orwl_server *serv);

/**
 ** @brief Stop the ORWL server if necessary
 **
 ** This is only necessary if you want to do things after the server
 ** thread ends. If this is not called from the application code, it
 ** is called automatically after ::exit of the process.
 **/
#ifndef DOXYGEN
P99_PROTOTYPE(void, orwl_stop, orwl_server *);
#ifdef ORWL_USE_TIMING
# define orwl_stop(...) ORWL_TIMER(orwl_stop) P99_CALL_DEFARG(o_rwl_stop, 1, __VA_ARGS__)
#else
# define orwl_stop(...) P99_CALL_DEFARG(o_rwl_stop, 1, __VA_ARGS__)
#endif
#define o_rwl_stop_defarg_0() orwl_server_get()
#endif


inline
bool
orwl_alive(orwl_server *serv) {
  return serv->fd_listen != -1;
}

#ifndef DOXYGEN
inline
P99_PROTOTYPE(bool, orwl_alive, orwl_server *);
#define orwl_alive(...) P99_CALL_DEFARG(orwl_alive, 1, __VA_ARGS__)
#define orwl_alive_defarg_0() orwl_server_get()
#endif

/**
 ** @brief Schedule all the requests that have been inserted via
 ** ::orwl_handle_read_insert and ::orwl_write_insert.
 **
 ** The requests are inserted according to the priorities that had
 ** been given in the respective calls. This mechanism allows to
 ** insert a whole batch of requests in a prescribed order.
 **
 ** @remark This needs global synchronization between all the
 ** locations. So for every location of the server such a call has to
 ** issued.
 **
 ** @param myloc is the principal location that this thread or process
 ** is acting upon. If omitted, the location is computed from the
 ** ::orwl_mytid.
 **
 ** @param locations_amount the number of locations, starting with @a
 ** myloc, that is concerned by this call. If omitted it defaults to
 ** the number of locations per task, ::orwl_locations_amount.
 **
 ** @param srv the corresponding ::orwl_server thread. Filled with a
 ** reasonable default if omitted.
 **
 ** @param auxiliary if this task or operations is considered
 ** auxiliary, such that it is not considered for binding by the
 ** affinity module. Uses orwl_auxiliary if omitted.
 **/
void
orwl_schedule(size_t myloc, size_t locations_amount, orwl_server *srv, bool auxiliary);

#ifndef DOXYGEN
P99_PROTOTYPE(void, orwl_schedule, size_t, size_t, orwl_server *, bool);
#define orwl_schedule(...) P99_CALL_DEFARG(orwl_schedule, 4, __VA_ARGS__)
#define orwl_schedule_defarg_0() SIZE_MAX
#define orwl_schedule_defarg_1() orwl_locations_amount
#define orwl_schedule_defarg_2() orwl_server_get()
#define orwl_schedule_defarg_3() orwl_auxiliary
P99_PROTOTYPE(void, orwl_schedule_grouped, size_t, size_t, orwl_server *);
#define orwl_schedule_grouped(...) P99_CALL_DEFARG(orwl_schedule_grouped, 3, __VA_ARGS__)
#define orwl_schedule_grouped_defarg_0() SIZE_MAX
#define orwl_schedule_grouped_defarg_1() orwl_locations_amount
#define orwl_schedule_grouped_defarg_2() orwl_server_get()
#endif

P99_PROTOTYPE(void, orwl_wait_and_load_init_files,
              size_t, size_t const*,
              char const *, char const *, char const *, orwl_server *);
#define orwl_wait_and_load_init_files(...) P99_CALL_DEFARG(orwl_wait_and_load_init_files, 6, __VA_ARGS__)
#define orwl_wait_and_load_init_files_defarg_0() P99_0(size_t)
#define orwl_wait_and_load_init_files_defarg_1() P99_0(size_t const*)
#define orwl_wait_and_load_init_files_defarg_2() P99_0(const char*)
#define orwl_wait_and_load_init_files_defarg_3() P99_0(const char*)
#define orwl_wait_and_load_init_files_defarg_4() P99_0(const char*)
#define orwl_wait_and_load_init_files_defarg_5() orwl_server_get()


void o_rwl_init(orwl_server *srv, size_t locations_amount);

#ifdef DOXYGEN
/**
 ** @brief Init ORWL according to the task model
 **
 ** This function supposes that the application uses one of the
 ** predefined programming models, see @ref ORWLprogramming. In
 ** particular it needs that the macro ::ORWL_LOCATIONS_PER_TASK macro
 ** has been called once in global scope to determine the locations
 ** that are associated to each task.
 **
 ** The parameters of this function have reasonable defaults. They
 ** basically serve to promote information that is only available in
 ** the context of the application to the ORWL library. Don't touch
 ** them if you don't have to.
 **
 ** @see orwl_stop_task
 ** @see orwl_stop
 **/
#define orwl_init(...)
#else
P99_PROTOTYPE(void, orwl_init, orwl_server*, size_t);
#ifdef ORWL_USE_TIMING
# define orwl_init(...) ORWL_TIMER(orwl_init) P99_CALL_DEFARG(o_rwl_init, 2, __VA_ARGS__)
#else
# define orwl_init(...) P99_CALL_DEFARG(o_rwl_init, 2, __VA_ARGS__)
#endif
#define o_rwl_init_defarg_0() orwl_server_get()
#define o_rwl_init_defarg_1() orwl_locations_amount
#endif

/**
 ** @}
 **/

/**
 ** @addtogroup library_low
 ** @{
 **/

enum {
  /**
   ** @brief The number of additional items that are transferred in
   ** the header of a ::orwl_push operation.
   **/
  orwl_push_header
    = orwl_server_callback_header
      + orwl_proc_release_header,
  orwl_push_withdata = 1,
  orwl_push_keep = 2,
};

#define ORWL_PUSH_HEADER 4

static_assert(orwl_push_header == ORWL_PUSH_HEADER, "compare macro and define");

/* Trigger the remote release of the FIFO */
void orwl_trigger(orwl_server *srv, orwl_endpoint const ep,
                  uint64_t whID);

/* Transfer the data to the remote FIFO, which then is the owner of
   the data. */
void orwl_transfer(orwl_server *srv, orwl_endpoint const ep,
                   orwl_wq *wq, uint64_t whID);

/* Copy the data to the remote FIFO, remain owner of the data. */
void orwl_push_copy(orwl_server *srv, orwl_endpoint const ep,
                    orwl_wq *wq, uint64_t whID);


P99_PROTOTYPE(void, orwl_open_group_graph, orwl_server*);
#define orwl_open_group_graph(...) P99_CALL_DEFARG(orwl_open_group_graph, 1, __VA_ARGS__)
#define orwl_open_group_graph_defarg_0() orwl_server_get()

P99_PROTOTYPE(void, orwl_close_group_graph, orwl_server*);
#define orwl_close_group_graph(...) P99_CALL_DEFARG(orwl_close_group_graph, 1, __VA_ARGS__)
#define orwl_close_group_graph_defarg_0() orwl_server_get()


/**
 ** @}
 **/

#endif      /* !ORWL_SERVER_H_ */
