/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the author and copyright holder for this work is                           */
/* all rights reserved,  2016 Jens Gustedt, INRIA, France                     */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#ifndef     ORWL_SPLIT_H_
# define    ORWL_SPLIT_H_

#include "orwl_alloc.h"
#include "orwl_handle2.h"

P99_DECLARE_STRUCT(orwl_split_location);

/**
 ** @brief An auxiliary data type to specify a "partial" resource.
 **
 ** - orwl_split_location::loc is the global location ID that
 **   represents the partial resource.
 ** - orwl_split_location::size is the desired size of the part.
 **
 ** @see orwl_split
 ** @see ORWL_SPLIT
 **/
struct orwl_split_location {
  size_t loc;
  size_t size;
};

/**
 ** @brief Launch a thread that maintains a dynamic split of a data
 ** resource.
 **
 ** This launches a thread that does the work behind the
 ** scenes.
 **
 ** see ORWL_SPLIT for a description of the approach.
 **/
void orwl_split(bool readonly, size_t n, orwl_split_location[n]);


/**
 ** @brief Launch a thread that maintains a dynamic split of a data
 ** resource.
 **
 ** The argument list is a list of initializers for
 ** #orwl_split_location, that is a list with elements of the form
 **
 ** @code
 **  { 34 }, { 42, 1024}, { .loc = 37, .size = 37}, ...
 ** @endcode
 **
 ** The parts will be taken in order as they appear in the call, where
 ** the first always corresponds to the master location that is to be
 ** split up.
 **
 ** Two cases for the sizes of the master location and its parts can
 ** apply. The first is that the size of the master location is
 ** already set to some value. Then the other location receive parts
 ** that correspond to their announced sizes, or an equal share if
 ** their size is @c 0.
 **
 ** If no size for the master location is know, the sizes of the parts
 ** are summed up and the master location is scaled accordingly.
 **
 ** The argument @a RO in addition sees if the access to the master
 ** location will be read-only or also writable.
 **
 **  - If it is readonly, we must assume that someone else will
 **    provide data for the master location. So we insert the handle
 **    at the very end of the queue of the master. The parts then will
 **    receive data floating from there, so they will be first on
 **    their location.
 **
 **  - If it is writable, the opposite strategy for insertion is
 **    chosen; the master is first in its queue, to give the data
 **    provider a chance to write first. The parts are supposed to
 **    receive data from somebody else, so they are inserted at the
 **    end of their queues.
 **
 ** @see orwl_split for a version that can be used with a varying
 ** number of workers
 **/
#define ORWL_SPLIT(RO, ...)                                                              \
  orwl_split(                                                                            \
             (RO),                                                                       \
             sizeof((orwl_split_location[]){ __VA_ARGS__ })/sizeof(orwl_split_location), \
             (orwl_split_location[]){ __VA_ARGS__ })

#endif
