/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the author and copyright holder for this work is                           */
/* all rights reserved,  2010-2013 Jens Gustedt, INRIA, France                */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#ifndef     ORWL_HEADER_H_
# define    ORWL_HEADER_H_


#include "orwl_int.h"

/**
 ** @addtogroup library_low Low level implementation
 ** @{
 **/

P99_DECLARE_ENUM(orwl_header_enum,
                 orwl_header_chal,
                 orwl_header_repl,
                 orwl_header_remo);

P99_CONSTANT(int, orwl_header_size, orwl_header_chal);
P99_CONSTANT(int, orwl_header_ret, orwl_header_chal);
P99_CONSTANT(int, orwl_header_async, orwl_header_repl);

typedef uint64_t orwl_header[orwl_header_enum_amount];

#define ORWL_HTON64(VAL)                                       \
(((uint64_t)htonl((uint32_t)(VAL)))                            \
 | (((uint64_t)htonl((uint32_t)((VAL) >> 32))) << 32))

#define ORWL_NTOH64(VAL)                                       \
(((uint64_t)ntohl((uint32_t)(VAL)))                            \
 | (((uint64_t)ntohl((uint32_t)((VAL) >> 32))) << 32))



inline
uint64_t orwl_hton64(uint64_t val) {
  return ORWL_HTON64(val);
}

inline
uint64_t orwl_ntoh64(uint64_t val) {
  return ORWL_NTOH64(val);
}

#define ORWL_HOSTORDER UINT64_C(0x0807060504030201)
#define ORWL_NETWORDER ORWL_HTON64(ORWL_HOSTORDER)

#define ORWL_HEADER_INITIALIZER(CHAL, ASYNC) {                 \
  [orwl_header_chal] = (CHAL),                                 \
  [orwl_header_async] = (ASYNC),                               \
  [orwl_header_remo] = ORWL_NETWORDER,                         \
}

/**
 ** @}
 **/

#endif      /* !ORWL_HEADER_H_ */
