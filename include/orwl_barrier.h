/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the author and copyright holder for this work is                           */
/* all rights reserved,  2010-2012 Jens Gustedt, INRIA, France                */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
/*
** orwl_barrier.h
**
** Made by Jens Gustedt
** Login   <gustedt@damogran.loria.fr>
**
** Started on  Wed Nov 24 23:17:01 2010 Jens Gustedt
** Last update Wed Nov 24 23:17:01 2010 Jens Gustedt
*/

/**
 ** @file
 ** @brief Provide a fallback implementation for barriers.
 **/

#ifndef     ORWL_BARRIER_H_
# define    ORWL_BARRIER_H_

/**
 ** @addtogroup library_support
 ** @{
 **/

#include "p99_count.h"

P99_DECLARE_STRUCT(orwl_barrier);

/**
 ** @brief a simple barrier type to synchronize threads
 **
 ** This is meant to synchronize a fixed number of threads. The number
 ** must be given at initialization. No provisions are made have a
 ** thread entering or leaving the set.
 **/
struct orwl_barrier {
  p99_count side[3];
};

/** @related orwl_barrier **/
int orwl_barrier_destroy(orwl_barrier* barrier);
/** @related orwl_barrier **/
int orwl_barrier_init(orwl_barrier* barrier, unsigned count);
/**
 ** @brief Wait for the all members of the crowd to reach the barrier.
 **
 ** @return All threads but one arbitrarily elected one return @c
 ** 0. The elected thread returns 1.
 **
 ** @related orwl_barrier
 **/
bool orwl_barrier_wait(orwl_barrier* barrier, size_t howmuch);

#define orwl_barrier_wait(...) P99_CALL_DEFARG(orwl_barrier_wait, 2, __VA_ARGS__)
#define orwl_barrier_wait_defarg_1() SIZE_C(1)

/**
 ** @}
 **/

#endif      /* !ORWL_BARRIER_H_ */
