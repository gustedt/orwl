/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the author and copyright holder for this work is                           */
/* all rights reserved,  2013 Rodrigo Campos, INRIA, France                   */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#ifndef ORWL_DEEP_INSTR_H_
#define ORWL_DEEP_INSTR_H_

// If we include orwl.h, as we used to do before, it fails to compile with weird
// errors. We should fix the madness in the future
//#include "orwl.h"

#include "p99_atomic.h"     // _Atomic and helpers
#include "p99_compiler.h"   // _Thread_local
#include "p99_type.h"       // P99_DECLARE_STRUCT
#include "p99_block.h"      // P99_NOP

// _POSIX_C_SOURCE >= 199309L and linking with -ltr is needed for clock_gettime()
#include <time.h>           // clock_gettime() and friends
#include <stdint.h>         // uint64_t and friends
#include <stdbool.h>        // bool

// We don't use ORWL_GIGA (def in orwl_time.h) because including it increases
// compilation time (for the "light" users of this file)
#define O_RWL_DEEP_INSTR_GIGA UINT64_C(1000000000)

#ifndef DEEP_INSTRUMENTATION

#define orwl_deep_instr_init(...)                P99_NOP
#define orwl_deep_instr_start(...)               P99_NOP
#define orwl_deep_instr_stop(...)                P99_NOP
#define orwl_deep_instr_thread_start(...)        P99_NOP
#define orwl_deep_instr_thread_stop(...)         P99_NOP
#define orwl_deep_instr_func_start(...)          P99_NOP
#define orwl_deep_instr_func_stop(...)           P99_NOP
#define orwl_deep_instr_inline_start(...)        P99_NOP
#define orwl_deep_instr_inline_stop(...)         P99_NOP
#define orwl_deep_instr_update(...)              P99_NOP
#define orwl_deep_instr_thread_not_internal(...) P99_NOP
#define orwl_deep_instr_thread_internal(...)     P99_NOP
#define orwl_deep_instr_print_stats(...)         P99_NOP


#else

P99_DECLARE_STRUCT(o_rwl_deep_tlocal_stats);
struct o_rwl_deep_tlocal_stats {

  /* When getting stats, we store the CPU used before function starts in these
   * thread-local variables, to compare it to CPU used after
   * Stored in nanoseconds */
  uint64_t started_at;

  /* Time elapsed in this thread, in nanoseconds */
  uint64_t elapsed_funcs;
  uint64_t elapsed_inline;
  uint64_t elapsed_threads;

  /* Hoy many calls in the call chain are inside the library, so we only
   * meassure when entering and exiting the library */
  unsigned int nested_level;

  /* Some threads created by the library are for user functions and not internal
   * functions. So we want to skip thread level instrumentation in those cases */
  // XXX: By using "not_internal" and no init, this value is false by default,
  // so all threads are internal by default.
  bool thread_not_internal;

  /* Total calls of this thread to the deep instrumentation infrastracture (take
   * into account that nested calls are ignored) */
  unsigned int calls;
};

P99_DECLARE_STRUCT(o_rwl_deep_global_stats);
struct o_rwl_deep_global_stats {

  /* Total times accummulated on threads, in nanoseconds */
  _Atomic(uint64_t) time_funcs;
  _Atomic(uint64_t) time_inline;
  _Atomic(uint64_t) time_threads;

  /* Total calls to the deep instrumentation infrastracture (take into account
   * that nested calls are ignored) */
  _Atomic(unsigned) calls;

  /* Overhead doing the init while using LD_PRELOAD, in nanoseconds */
  uint64_t init_overhead;
};

/* Use a thread-local variable to store things per thread */
extern _Thread_local o_rwl_deep_tlocal_stats o_rwl_deep_tlocal;

/* And a global variable to store stats for all threads */
extern o_rwl_deep_global_stats o_rwl_deep_global;


/*
 * XXX: Some limitations/restrictions when using this approach
 *
 * This "_start" and "_stop" functions are supossed to be called on library
 * inline functions we want to instrument (at the begining and at the end
 * respectively). The idea, basically, is that we meassure the CPU time spent in
 * the library, if we put this instrumentations in all user-visible functions.
 * But as the instrumented function can, internally, call some other function
 * that is instrumented too, we need to know the "nestedness level". And, so, if
 * this call to a instrumented function is actually made from inside the
 * library, we don't gather stats there (because the first call to the library
 * is already doing it).
 *
 * But one big LIMITATION of this approach is when the library calls a user
 * function (if the library takes a pointer to a user function, for example). In
 * that case, we will messure it as library time, when in fact the time was
 * spent in the user function.
 *
 * For now we are not doing this, so we don't have to worry. But this
 * limitation is definitely something to keep in mind.
 *
 */


void orwl_deep_instr_init(void);
void orwl_deep_instr_print_stats(void);

inline void orwl_deep_instr_start(void) {

  // Only instrument the first call to the lib, don't instrument nested calls
  o_rwl_deep_tlocal_stats *tlocal = &o_rwl_deep_tlocal;
  tlocal->nested_level++;
  if (tlocal->nested_level > 1)
    return;

  tlocal->calls++;

  struct timespec usage;
  int r = clock_gettime(CLOCK_THREAD_CPUTIME_ID, &usage);
  if (r)
    abort();

  tlocal->started_at = usage.tv_sec * O_RWL_DEEP_INSTR_GIGA + usage.tv_nsec;
}

inline void orwl_deep_instr_stop(uint64_t *dst) {

  // Only stop the timer when we are the last library call in the call chain
  o_rwl_deep_tlocal_stats *tlocal = &o_rwl_deep_tlocal;
  tlocal->nested_level--;
  if (tlocal->nested_level > 0)
    return;

  struct timespec usage;
  int r = clock_gettime(CLOCK_THREAD_CPUTIME_ID, &usage);
  if (r)
    abort();

  uint64_t stop_at = usage.tv_sec * O_RWL_DEEP_INSTR_GIGA + usage.tv_nsec;

  *dst += stop_at - tlocal->started_at;
}

inline void orwl_deep_instr_thread_not_internal(void) {

  o_rwl_deep_tlocal_stats *tlocal = &o_rwl_deep_tlocal;
  tlocal->thread_not_internal = true;

  /*
   * Right now orwl_deep_instr_thread_start() must ALWAYS be run before starting
   * the thread routine (we can't instrument library internal threads
   * otherwise), but if the routine is a user thread then we must not instrument
   * the whole thread and only instrument function calls (or inline functions)
   * made from that thread. But all instrumentation is ignored if nested_level
   * is > 0 because it thinks some other is instrumenting it (and
   * orwl_deep_instr_thread_start() sets it to 1). But in the case of a user
   * thread, this is not true. We should not instrument the whole thread time,
   * and so we should return the nested_level to zero
   *
   * Also, right now this might only happen with nested_level 1 as
   * thread_start() sets it to 1 (in the future maybe with zero if we find a way
   * to set this very early). So the assert checks that we are on the expected
   * situations and this is not used for weird things.
   */
  assert(tlocal->nested_level == 0 || tlocal->nested_level == 1);
  tlocal->nested_level = 0;
}

inline void orwl_deep_instr_thread_internal(void) {

  o_rwl_deep_tlocal_stats *tlocal = &o_rwl_deep_tlocal;
  tlocal->thread_not_internal = false;

  /*
   * See comment on orwl_deep_instr_thread_not_internal() to have more context.
   * But basically, when we want to transform a thread to an internal thread, it
   * is expected that all instrumentation was done and so the nested_level
   * should be zero (if not, some call to "_stop()" is missing and better find
   * what is happening)
   */
  assert(tlocal->nested_level == 0);
}

inline void orwl_deep_instr_thread_start(void) {

  o_rwl_deep_tlocal_stats *tlocal = &o_rwl_deep_tlocal;

  /* The thread local variable "thread_not_internal" is actually never set when
   * we check here. That is because it is set in the routine itself and that is
   * executed just after this function. So this check is always false (right
   * now) and we get to ignore the not library internal thread because "_stop()"
   * has the same check.
   * But is better to leave the check here, as this is where we should check. If
   * in the future we manage to set this variable before the routine (that would
   * be ideal), no obscure bugs will happen because we don't have this check
   */
  if (tlocal->thread_not_internal)
    return;

  orwl_deep_instr_start();
}

inline void orwl_deep_instr_thread_stop(void) {

  o_rwl_deep_tlocal_stats *tlocal = &o_rwl_deep_tlocal;

  if (tlocal->thread_not_internal) {
    /* This thread can potentially be used for internal or external purposes in
     * the future. But external purposes always change the thread-local
     * variable, so we should "reset" the value */
    orwl_deep_instr_thread_internal();

    return;
  }

  orwl_deep_instr_stop(&tlocal->elapsed_threads);
}

inline void orwl_deep_instr_inline_start(void) {
  orwl_deep_instr_start();
}

inline void orwl_deep_instr_inline_stop(void) {
  o_rwl_deep_tlocal_stats *tlocal = &o_rwl_deep_tlocal;
  orwl_deep_instr_stop(&tlocal->elapsed_inline);
}

inline void orwl_deep_instr_func_start(void) {
  orwl_deep_instr_start();
}

inline void orwl_deep_instr_func_stop(void) {
  o_rwl_deep_tlocal_stats *tlocal = &o_rwl_deep_tlocal;
  orwl_deep_instr_stop(&tlocal->elapsed_funcs);
}

/* Overhead doing the init while using LD_PRELOAD, in nanoseconds */
inline void orwl_deep_instr_set_init_overhead(uint64_t overhead) {

  o_rwl_deep_global_stats *global = &o_rwl_deep_global;
  global->init_overhead = overhead;
}

inline void orwl_deep_instr_update(void) {

  o_rwl_deep_tlocal_stats *tlocal = &o_rwl_deep_tlocal;
  o_rwl_deep_global_stats *global = &o_rwl_deep_global;

  atomic_fetch_add(&global->time_funcs, tlocal->elapsed_funcs);
  atomic_fetch_add(&global->time_inline, tlocal->elapsed_inline);
  atomic_fetch_add(&global->time_threads, tlocal->elapsed_threads);
  atomic_fetch_add(&global->calls, tlocal->calls);
  tlocal->elapsed_funcs = 0;
  tlocal->elapsed_inline = 0;
  tlocal->elapsed_threads = 0;
  tlocal->calls = 0;
}


#endif  // DEEP_INSTRUMENTATION
#endif  // ORWL_DEEP_INSTR_H
