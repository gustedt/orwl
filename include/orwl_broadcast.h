/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the author and copyright holder for this work is                           */
/* all rights reserved,  2012 Jens Gustedt, INRIA, France                     */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#ifndef     ORWL_BROADCAST_H_
# define    ORWL_BROADCAST_H_

#include "orwl_comstate.h"

/**
 ** @brief Reserve local location ID @a LOC for use with a broadcast
 ** collective operation.
 **/
#define ORWL_BROADCAST_LOCATION(LOC) ORWL_COMSTATE_LOCATION(LOC, true, false)

#define O_RWL_BROADCAST_DECLARE_(T, LOC, ROOT, SRV) ORWL_COMSTATE_DECLARE(T, LOC, ROOT, SRV)

#define O_RWL_BROADCAST_DECLARE(...) O_RWL_BROADCAST_DECLARE_(__VA_ARGS__)

/**
 ** @brief declare a variable that can be used with ::ORWL_BROADCAST
 **
 ** @param T is the type of the variable. It must be such
 ** that <code>typedef T NAME;</code> is a valid declaration. If you
 ** want to use an array, you should @c typedef it beforehand and use
 ** that type name for @a T.
 **
 ** @param LOC is the name of the new variable. It must be the same as
 ** the name of a location that has been declared with
 ** ::ORWL_LOCATIONS_PER_TASK in global scope and that has been
 ** dedicated to a broadcast operation with ::ORWL_BROADCAST_LOCATION.
 **
 ** @param ROOT is the id of the location that will hold the final
 ** result. It defaults to the corresponding id at task 0.
 **
 ** @param SRV is the server thread that handles this request. In most
 ** cases this may be omitted. It defaults to the usually unique
 ** server thread for the process.
 **
 ** @see ORWL_BROADCAST for the broadcast operation itself.
 ** @see ORWL_LOCATIONS_PER_TASK for the declaration of all locations of a task
 ** @see ORWL_BROADCAST_LOCATION for the reservation of a location to a broadcast collective operation
 **/
P99_DEFARG_DOCU(ORWL_BROADCAST_DECLARE)
P00_DOCUMENT_IDENTIFIER_ARGUMENT(ORWL_BROADCAST_DECLARE, 1)
#ifdef DOXYGEN
#define ORWL_BROADCAST_DECLARE(T, LOC, ROOT, SRV)
#else
#define ORWL_BROADCAST_DECLARE(...) O_RWL_BROADCAST_DECLARE(P99_CALL_DEFARG_LIST(ORWL_BROADCAST_DECLARE, 4, __VA_ARGS__))
#define ORWL_BROADCAST_DECLARE_defarg_2() SIZE_MAX
#define ORWL_BROADCAST_DECLARE_defarg_3() orwl_server_get()
#endif

/**
 ** @brief perform a broadcast operation on @a LOC
 **
 ** @param LOC must be a "variable" that is declared through
 ** ::ORWL_BROADCAST_DECLARE, the type is the one that was given there.
 **
 ** Here is an almost complete example that performs the diffusion of
 ** a @c double value.
 **
 ** @code
 ** // in file scope, possibly in a header file:
 ** // declare a location that will be used to propagate the information
 ** ORWL_LOCATIONS_PER_TASK(..., broadcast_loc, ...);
 ** ORWL_BROADCAST_LOCATION(broadcast_loc);
 **
 ** // in file scope, in a .c file:
 ** ORWL_LOCATIONS_PER_TASK_INSTANTIATION();
 **
 ** // inside your function:
 ** // declare the reduction. root is taken as 0 if omitted
 ** ORWL_BROADCAST_DECLARE(double, broadcast_loc, root) = init_val;
 **
 ** ... place your other requests ...
 **
 ** //
 ** orwl_schedule_grouped();
 **
 ** for (size_t orwl_phase = 0; orwl_phase < 10; ++orwl_phase) {
 **   // assign a value as you would to any other variable, but only the one
 **   // at "root" will be transmitted to the other tasks.
 **   if (orwl_myloc == root) broadcast_loc = my_precious_result;
 **
 **   // do the operation
 **   ORWL_BROADCAST(broadcast_loc);
 **
 **
 **   // now everybody can use the collected value.
 **   report(1, "global sum is %f", broadcast_loc);
 ** }
 ** @endcode
 **
 ** @remark the type @c T of the variable that is declared with
 ** ::ORWL_BROADCAST_DECLARE must be such that <code>typedef T
 ** NAME;</code> is a valid declaration. In particular to be of an
 ** array type you have to @c typedef the type beforehand.
 **
 ** @see ORWL_BROADCAST_DECLARE
 ** @see ORWL_LOCATIONS_PER_TASK
 **/
P99_BLOCK_DOCUMENT
#define ORWL_BROADCAST(LOC)                                     \
(                                                               \
 (ORWL_COMSTATE[O_RWL_COMNAME(LOC)].data = &(LOC)),             \
 (orwl_comstate_broadcast(&ORWL_COMSTATE[O_RWL_COMNAME(LOC)])), \
 (ORWL_COMSTATE[O_RWL_COMNAME(LOC)].data = 0))

#define ORWL_BROADCAST_DISCONNECT ORWL_COMSTATE_DISCONNECT

#endif
