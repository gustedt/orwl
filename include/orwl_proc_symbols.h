/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the authors and copyright holders for this work are as follows:            */
/* all rights reserved,  2011 Emmanuel Jeanvoine, INRIA, France               */
/* all rights reserved,  2010-2013 Jens Gustedt, INRIA, France                */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#ifndef     ORWL_PROC_SYMBOLS_H_
# define    ORWL_PROC_SYMBOLS_H_

#include "orwl_proc.h"

/**
 ** @addtogroup library_low
 ** @{
 **/

DECLARE_ORWL_PROC_FUNC(orwl_server_callback, uint64_t funcID);
DECLARE_ORWL_PROC_FUNC(orwl_proc_write_request, uint64_t wqPOS, uint64_t whID, uint64_t port);
DECLARE_ORWL_PROC_FUNC(orwl_proc_read_request, uint64_t wqPOS, uint64_t cliID, uint64_t svrID, uint64_t port);
DECLARE_ORWL_PROC_FUNC(orwl_proc_request_insert, uint64_t pri, uint64_t excl);
DECLARE_ORWL_PROC_FUNC(orwl_proc_write_insert, uint64_t remP, uint64_t wqPOS, uint64_t whID, uint64_t port);
DECLARE_ORWL_PROC_FUNC(orwl_proc_read_insert, uint64_t remP, uint64_t wqPOS, uint64_t cliID, uint64_t port);
DECLARE_ORWL_PROC_FUNC(orwl_proc_read_insert_again, uint64_t remP);
DECLARE_ORWL_PROC_FUNC(orwl_proc_release, uintptr_t whID, uint64_t borrow, uint64_t read_len);
DECLARE_ORWL_PROC_FUNC(orwl_proc_trigger, uintptr_t whID);
DECLARE_ORWL_PROC_FUNC(orwl_proc_do_nothing, void);
DECLARE_ORWL_PROC_FUNC(orwl_proc_check_initialization, uint64_t id_pow2);
DECLARE_ORWL_PROC_FUNC(orwl_proc_barrier, uint64_t hid, uint64_t phase, uint64_t remaining);
DECLARE_ORWL_PROC_FUNC(orwl_proc_terminate, void);
DECLARE_ORWL_PROC_FUNC(orwl_proc_final, void);
DECLARE_ORWL_PROC_FUNC(orwl_proc_block, void);
DECLARE_ORWL_PROC_FUNC(orwl_proc_unblock, void);

/**
 ** @}
 **/

#endif      /* !ORWL_PROC_SYMBOLS_H_ */
