/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the author and copyright holder for this work is                           */
/* all rights reserved,  2013-2015 Jens Gustedt, INRIA, France                */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#ifndef ORWL_DOTPRODUCT_H_
# define ORWL_DOTPRODUCT_H_ 1
# include "p99.h"

#define O_RWL_DOTPLUS(I) accu[I] + mul[I]

#define O_RWL_DOTASSIGN(I) accu[I] = add[I]

#define O_RWL_DOTMULT(I) tmpA[I] * tmpB[I]

#define O_RWL_DOTFMAK(I) accu[I] += A[k+(I)] * B[k+(I)]
#define O_RWL_DOTCASE(I)                                       \
  case (stride-(I)):                                           \
  P99_MARK(Start of case with remainder stride-I);             \
  O_RWL_DOTFMAK(stride-(I)-1)


#define O_RWL_DOTPRODUCT_DEFINE(SUFF, STRIDE)                                   \
inline                                                                          \
P99_BUILTIN_TYPE(SUFF)                                                          \
P99_PASTE3(o_rwl_dotproduct, SUFF, STRIDE)(size_t n,                            \
                                           P99_BUILTIN_TYPE(SUFF) const A[n],   \
                                           P99_BUILTIN_TYPE(SUFF) const B[n]) { \
  enum { stride = (STRIDE) };                                                   \
  typedef P99_BUILTIN_TYPE(SUFF) type;                                          \
  type accu[stride] = P99_INIT;                                                 \
  type tmpA[stride];                                                            \
  type tmpB[stride];                                                            \
  register size_t k = 0;                                                        \
  P99_MARK(Unroll in packets of STRIDE);                                        \
  for (register size_t const upp = (n / stride)*stride;                         \
       k < upp;                                                                 \
       k += stride) {                                                           \
    P99_AASSIGN(tmpA, (A+k), STRIDE);                                           \
    P99_AASSIGN(tmpB, (B+k), STRIDE);                                           \
    type const mul[stride] = { P99_REPEAT(O_RWL_DOTMULT, STRIDE), };            \
    type const add[stride] = { P99_REPEAT(O_RWL_DOTPLUS, STRIDE), };            \
    P99_UNROLL(O_RWL_DOTASSIGN, STRIDE);                                        \
  }                                                                             \
  P99_MARK(Finish the last part that doesn t fit.);                             \
  switch (n % stride) {                                                         \
  default: abort();                                                             \
    P99_UNROLL(O_RWL_DOTCASE, STRIDE);                                          \
  case 0:;                                                                      \
    P99_MARK(Start of case with remainder 0);                                   \
  }                                                                             \
  P99_MARK(Add up the accumulator array into one                                \
           P99_BUILTIN_TYPE(SUFF)                                               \
           and return.);                                                        \
  return P99_SUMS(P99_ACCESSORS(accu, STRIDE));                                 \
}                                                                               \
P99_MACRO_END(O_RWL_DOTPRODUCT_DEFINE, SUFF, STRIDE)

#define O_RWL_DOTPRODUCT_INSTANTIATE(SUFF, STRIDE)                            \
P99_BUILTIN_TYPE(SUFF)                                                        \
P99_PASTE3(o_rwl_dotproduct, SUFF, STRIDE)(size_t n,                          \
                                           P99_BUILTIN_TYPE(SUFF) const A[n], \
                                           P99_BUILTIN_TYPE(SUFF) const B[n])

O_RWL_DOTPRODUCT_DEFINE(d, 2);
O_RWL_DOTPRODUCT_DEFINE(d, 3);
O_RWL_DOTPRODUCT_DEFINE(d, 4);
O_RWL_DOTPRODUCT_DEFINE(d, 5);
O_RWL_DOTPRODUCT_DEFINE(d, 6);
O_RWL_DOTPRODUCT_DEFINE(d, 7);
O_RWL_DOTPRODUCT_DEFINE(d, 8);

O_RWL_DOTPRODUCT_DEFINE(ld, 2);
O_RWL_DOTPRODUCT_DEFINE(ld, 3);
O_RWL_DOTPRODUCT_DEFINE(ld, 4);
O_RWL_DOTPRODUCT_DEFINE(ld, 5);
O_RWL_DOTPRODUCT_DEFINE(ld, 6);
O_RWL_DOTPRODUCT_DEFINE(ld, 7);
O_RWL_DOTPRODUCT_DEFINE(ld, 8);

O_RWL_DOTPRODUCT_DEFINE(f, 2);
O_RWL_DOTPRODUCT_DEFINE(f, 3);
O_RWL_DOTPRODUCT_DEFINE(f, 4);
O_RWL_DOTPRODUCT_DEFINE(f, 5);
O_RWL_DOTPRODUCT_DEFINE(f, 6);
O_RWL_DOTPRODUCT_DEFINE(f, 7);
O_RWL_DOTPRODUCT_DEFINE(f, 8);

#ifndef __STDC_NO_COMPLEX__

O_RWL_DOTPRODUCT_DEFINE(dc, 2);
O_RWL_DOTPRODUCT_DEFINE(dc, 3);
O_RWL_DOTPRODUCT_DEFINE(dc, 4);
O_RWL_DOTPRODUCT_DEFINE(dc, 5);
O_RWL_DOTPRODUCT_DEFINE(dc, 6);
O_RWL_DOTPRODUCT_DEFINE(dc, 7);
O_RWL_DOTPRODUCT_DEFINE(dc, 8);

O_RWL_DOTPRODUCT_DEFINE(ldc, 2);
O_RWL_DOTPRODUCT_DEFINE(ldc, 3);
O_RWL_DOTPRODUCT_DEFINE(ldc, 4);
O_RWL_DOTPRODUCT_DEFINE(ldc, 5);
O_RWL_DOTPRODUCT_DEFINE(ldc, 6);
O_RWL_DOTPRODUCT_DEFINE(ldc, 7);
O_RWL_DOTPRODUCT_DEFINE(ldc, 8);

O_RWL_DOTPRODUCT_DEFINE(fc, 2);
O_RWL_DOTPRODUCT_DEFINE(fc, 3);
O_RWL_DOTPRODUCT_DEFINE(fc, 4);
O_RWL_DOTPRODUCT_DEFINE(fc, 5);
O_RWL_DOTPRODUCT_DEFINE(fc, 6);
O_RWL_DOTPRODUCT_DEFINE(fc, 7);
O_RWL_DOTPRODUCT_DEFINE(fc, 8);

#endif

#if p99_has_attribute(vector_size)

# define O_RWL_DOT_AKI(I) *(dvector const*)(A+k+(I)*vec)
# define O_RWL_DOT_BKI(I) *(dvector const*)(B+k+(I)*vec)
# define O_RWL_DOTFMAI(I)  acc[I] += tmpa[I] * tmpb[I]

# define O_RWL_DOTPRODUCTPV_DEFINE(SUFF, VEC, UNR)                                       \
inline                                                                                   \
P99_BUILTIN_TYPE(SUFF)                                                                   \
P99_PASTE3(o_rwl_dotproductv, SUFF, VEC)(size_t n,                                       \
                                         P99_BUILTIN_TYPE(SUFF) const A[n],              \
                                         P99_BUILTIN_TYPE(SUFF) const B[n]) {            \
  enum { vec = (VEC), unr = (UNR), stride = vec, stridevec = unr*vec, };                 \
  typedef P99_BUILTIN_TYPE(SUFF) type;                                                   \
  typedef P99_VECTOR(type, dvector, vec);                                                \
  typedef union ovector ovector;                                                         \
  union ovector { dvector v; type a[vec]; };                                             \
  P99_MARK(Test if both vectors are equally aligned.);                                   \
  if (((uintptr_t)A % alignof(dvector)) != ((uintptr_t)B % alignof(dvector))) {          \
    P99_MARK(If they are not equally aligned fall back to the default orwl_dotproduct.); \
    return P99_PASTE3(o_rwl_dotproduct, SUFF, VEC)(n, A, B);                             \
  } else {                                                                               \
    ovector ac = { .v = P99_INIT, };                                                     \
    P99_MARK(Advance both vectors until they fall on the alignment of the vector type);  \
    while ((uintptr_t)A % sizeof(dvector)) {                                             \
      ac.a[0] += A[0] * B[0];                                                            \
      ++A; ++B; --n;                                                                     \
    }                                                                                    \
    dvector acc[unr] = { ac.v };                                                         \
    P99_MARK(Now use the full vector code for the part that fits to                      \
             the alignment.and the unrolling.);                                          \
    register size_t k = 0;                                                               \
    for (register size_t const upp = (n / stridevec)*stridevec;                          \
         k < upp; k += (stridevec)) {                                                    \
      dvector const tmpa[unr] = { P99_REPEAT(O_RWL_DOT_AKI, UNR), };                     \
      dvector const tmpb[unr] = { P99_REPEAT(O_RWL_DOT_BKI, UNR), };                     \
      P99_UNROLL(O_RWL_DOTFMAI, UNR);                                                    \
    }                                                                                    \
    P99_MARK(Add up the accumulator vectors into one.);                                  \
    ovector accU = { .v = P99_SUMS(P99_ACCESSORS(acc, UNR)) };                           \
    P99_MARK(The part that still fits the alignment of the vector.);                     \
    for (register size_t const upp = (n / vec)*vec;                                      \
         k < upp;                                                                        \
         k += vec) {                                                                     \
      accU.v += *(dvector const*)(A+k) * *(dvector const*)(B+k);                         \
    }                                                                                    \
    P99_MARK(Finish the last part that doesn t fit.);                                    \
    type accu[vec] = { P99_ACCESSORS(accU.a, VEC), };                                    \
    switch (n % vec) {                                                                   \
      P99_UNROLL(O_RWL_DOTCASE, VEC);                                                    \
    case 0:;                                                                             \
      P99_MARK(Start of case with remainder 0);                                          \
    default:;                                                                            \
    }                                                                                    \
    P99_MARK(Add up the accumulator vector into one                                      \
             P99_BUILTIN_TYPE(SUFF)                                                      \
             and return.);                                                               \
    return P99_SUMS(P99_ACCESSORS(accu, VEC));                                           \
  }                                                                                      \
}                                                                                        \
P99_MACRO_END(O_RWL_DOTPRODUCTPV_DEFINE, VEC)

O_RWL_DOTPRODUCTPV_DEFINE(d, 2, 8);
O_RWL_DOTPRODUCTPV_DEFINE(d, 4, 8);
O_RWL_DOTPRODUCTPV_DEFINE(d, 8, 8);

O_RWL_DOTPRODUCTPV_DEFINE(ld, 2, 8);
O_RWL_DOTPRODUCTPV_DEFINE(ld, 4, 8);
O_RWL_DOTPRODUCTPV_DEFINE(ld, 8, 8);

O_RWL_DOTPRODUCTPV_DEFINE(f, 4, 8);
O_RWL_DOTPRODUCTPV_DEFINE(f, 8, 8);

# ifdef P00_COMPLEX_VECTOR_EXTENSION

O_RWL_DOTPRODUCTPV_DEFINE(dc, 2, 8);
O_RWL_DOTPRODUCTPV_DEFINE(dc, 4, 8);
O_RWL_DOTPRODUCTPV_DEFINE(dc, 8, 8);

O_RWL_DOTPRODUCTPV_DEFINE(ldc, 2, 8);
O_RWL_DOTPRODUCTPV_DEFINE(ldc, 4, 8);
O_RWL_DOTPRODUCTPV_DEFINE(ldc, 8, 8);

O_RWL_DOTPRODUCTPV_DEFINE(fc, 2, 8);
O_RWL_DOTPRODUCTPV_DEFINE(fc, 4, 8);
O_RWL_DOTPRODUCTPV_DEFINE(fc, 8, 8);

# endif

# define O_RWL_DOTPRODUCTPV_INSTANTIATE(SUFF, VEC, UNR)                       \
P99_BUILTIN_TYPE(SUFF)                                                        \
P99_PASTE3(o_rwl_dotproductv, SUFF, VEC)(size_t n,                            \
                                           P99_BUILTIN_TYPE(SUFF) const A[n], \
                                           P99_BUILTIN_TYPE(SUFF) const B[n])


#if __AVX__NON_FUNCTIONING
# define orwl_dotproductd o_rwl_dotproductvd4
#elif __SSE__
# define orwl_dotproductd o_rwl_dotproductvd2
#else
# define orwl_dotproductd o_rwl_dotproductd4
#endif

#if __AVX__NON_FUNCTIONING
# define orwl_dotproductld o_rwl_dotproductvld2
#else
# define orwl_dotproductld o_rwl_dotproductld2
#endif

#if __AVX__NON_FUNCTIONING
# define orwl_dotproductf o_rwl_dotproductvf8
#elif __SSE__
# define orwl_dotproductf o_rwl_dotproductvf4
#else
# define orwl_dotproductf o_rwl_dotproductf8
#endif

#ifndef __STDC_NO_COMPLEX__

#if defined(P00_COMPLEX_VECTOR_EXTENSION) && __AVX__NON_FUNCTIONING
# define orwl_dotproductdc o_rwl_dotproductvdc2
#else
# define orwl_dotproductdc o_rwl_dotproductdc2
#endif

#if defined(P00_COMPLEX_VECTOR_EXTENSION) && __AVX__NON_FUNCTIONING
# define orwl_dotproductldc o_rwl_dotproductvldc2
#else
# define orwl_dotproductldc o_rwl_dotproductldc2
#endif

#if defined(P00_COMPLEX_VECTOR_EXTENSION) && __AVX__NON_FUNCTIONING
# define orwl_dotproductfc o_rwl_dotproductvfc4
#else
# define orwl_dotproductfc o_rwl_dotproductfc2
#endif

#endif

#else

/* No vector support available */
#define orwl_dotproductd o_rwl_dotproductd4
#define orwl_dotproductld o_rwl_dotproductld2
#define orwl_dotproductf o_rwl_dotproductf8

# ifndef __STDC_NO_COMPLEX__

/* No vector support available */
#define orwl_dotproductd o_rwl_dotproductd4
#define orwl_dotproductld o_rwl_dotproductld2
#define orwl_dotproductf o_rwl_dotproductf8

# endif

#endif

#define p00_gen_orwl_dotproductd orwl_dotproductd
#define p00_gen_orwl_dotproductf orwl_dotproductf
#define p00_gen_orwl_dotproductld orwl_dotproductld
#define p00_gen_orwl_dotproductdc orwl_dotproductdc
#define p00_gen_orwl_dotproductfc orwl_dotproductfc
#define p00_gen_orwl_dotproductldc orwl_dotproductldc

#define orwl_dotproduct(N, A, B)                                                     \
P99_GEN_EXPR(orwl_dotproduct, ((A)[0]+(B)[0]), P99_STD_FLOATING_EXTS)((N), (A), (B))

#endif
