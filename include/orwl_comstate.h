/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the author and copyright holder for this work is                           */
/* all rights reserved,  2012-2013 Jens Gustedt, INRIA, France                */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#ifndef     ORWL_COMSTATE_H_
# define    ORWL_COMSTATE_H_

#include "orwl_server.h"
#include "orwl_generic.h"

#define O_RWL_COMNAME(LOC) P99_PASTID3(o_rwl, LOC, com)
#define O_RWL_COMTYPE(LOC) P99_PASTID4(o_rwl, LOC, com, type)
#define O_RWL_COMALL(LOC) P99_PASTID4(o_rwl, LOC, com, all)
#define O_RWL_COMONE(LOC) P99_PASTID4(o_rwl, LOC, com, one)

/**
 ** @brief internal data structure for communication primitives, don't use it
 ** directly.
 **/
P99_DECLARE_STRUCT(orwl_comstate);

struct orwl_comstate {
  size_t const location;
  size_t const root;
  size_t const locations_amount;
  size_t const nl;
  size_t const child;
  size_t const children;
  size_t const sizes[2];
  size_t const parent;
  orwl_server*const srv;
  bool const all:1;
  bool const one:1;
  bool const is_init:1;
  bool is_connected:1;
  orwl_handle2 here[2];
  orwl_handle2 there[2];
  orwl_handle2 pare;
  void * data;
  size_t el_size;
};

P99_PROTOTYPE(orwl_comstate, orwl_comstate_initializer, size_t, size_t, size_t, orwl_server*, bool, bool);

#define ORWL_COMSTATE_INITIALIZER(ALL, ONE, LOC, ROOT, SRV) orwl_comstate_initializer((LOC), (ROOT), orwl_locations_amount, (SRV), (ALL), (ONE))

inline
orwl_comstate* orwl_comstate_init(orwl_comstate* p, size_t loc, size_t r, size_t locations_amount, orwl_server* srv, bool all, bool one) {
  if (p && !p->is_connected) {
    orwl_comstate dummy = orwl_comstate_initializer(loc, r, locations_amount, srv, all, one);
    memcpy(p, &dummy, sizeof *p);
  }
  return p;
}

P99_PROTOTYPE(void, orwl_comstate_disconnect, orwl_comstate*);
P99_PROTOTYPE(void, orwl_comstate_set, orwl_comstate*, size_t);
P99_PROTOTYPE(void, orwl_comstate_insert, orwl_comstate*, size_t);
P99_PROTOTYPE(void, orwl_comstate_reset, orwl_comstate*, size_t);
P99_PROTOTYPE(void, orwl_comstate_broadcast, orwl_comstate*);
P99_PROTOTYPE(void, orwl_comstate_gather, orwl_comstate*);

inline
void orwl_comstate_destroy(orwl_comstate* p) {
  //
}

#define ORWL_COMSTATE_LOCATION(LOC, ALL, ONE)                  \
enum {                                                         \
  O_RWL_COMNAME(LOC) = (LOC),                                  \
  O_RWL_COMALL(LOC) = !!(ALL),                                 \
  O_RWL_COMONE(LOC) = !!(ONE),                                 \
}

#define ORWL_COMSTATE_DECLARE_(LOC, ROOT, SRV)                 \
orwl_comstate_set                                              \
(orwl_comstate_init                                            \
 (&ORWL_COMSTATE[O_RWL_COMNAME(LOC)],                          \
  O_RWL_COMNAME(LOC),                                          \
  (ROOT),                                                      \
  orwl_locations_amount,                                       \
  (SRV),                                                       \
  O_RWL_COMALL(LOC),                                           \
  O_RWL_COMONE(LOC)),                                          \
 sizeof(O_RWL_COMTYPE(LOC)));                                  \
auto O_RWL_COMTYPE(LOC) LOC


#define ORWL_COMSTATE_DECLARE(T, LOC, ROOT, SRV)               \
typedef T O_RWL_COMTYPE(LOC);                                  \
ORWL_COMSTATE_DECLARE_(LOC, (ROOT), SRV)

#define ORWL_COMSTATE_DECLARE_ARRAY(T, LOC, ROOT, SRV)         \
register orwl_server *const o_rwl_srv = (SRV);                 \
typedef T O_RWL_COMTYPE(LOC)[ORWL_NT(o_rwl_srv->ab)];          \
ORWL_COMSTATE_DECLARE_(LOC, (ROOT), o_rwl_srv)

#define ORWL_COMSTATE_DISCONNECT(LOC)                           \
  orwl_comstate_disconnect(&ORWL_COMSTATE[O_RWL_COMNAME(LOC)])


#endif
