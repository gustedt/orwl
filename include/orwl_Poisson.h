// This may look like C code, but it really is -*- mode: c; coding: utf-8 -*-
//
#ifndef ORWL_POISSON_H
#define ORWL_POISSON_H

/// @file

#include "p99_rand.h"


/// @brief Sample a Poisson distribution with given mean.
size_t orwl_Poisson(p99_seed seed[static 1], double mean);


#endif
