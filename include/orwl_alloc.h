/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the authors and copyright holders for this work are as follows:            */
/* all rights reserved,  2012-2013, 2016 Jens Gustedt, INRIA, France          */
/* all rights reserved,  2013 Rodrigo Campos, INRIA, France                   */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#ifndef     ORWL_ALLOC_H_
#define     ORWL_ALLOC_H_

#include "orwl_posix.h"
#include "p99_try.h"
#include "p99_tp.h"
#include "orwl_document.h"
#include "orwl_deep_instr.h"

/**
 ** @addtogroup library_low
 ** @{
 **/

P99_DECLARE_STRUCT(orwl_alloc);


P99_TP_REF_DECLARE(orwl_alloc);

/**
 ** @brief initializer expression for ::orwl_alloc_ref
 **
 ** @warning if this is used for a static allocation @a VAL must be a
 ** null pointer constant
 **
 ** @see P99_TP_REF_INITIALIZER
 ** @related orwl_alloc_ref
 **/
#define ORWL_ALLOC_REF_INITIALIZER(VAL) P99_TP_REF_INITIALIZER(VAL, orwl_alloc_account)

/**
 ** @brief The special cases that are handled by the allocation
 ** routines of type  ::orwl_alloc_realloc_func.
 **
 ** @remark You should only use these constants in an allocation
 ** routine to capture the different cases.
 **
 ** @remark For a use in application code as parameter to such a
 ** function use the macros (in all upper case), instead.
 **
 ** @see orwl_alloc_realloc_func
 ** @see ORWL_ALLOC_CLOSE
 ** @see ORWL_ALLOC_UNLINK
 ** @see ORWL_ALLOC_UNMAP
 ** @see ORWL_ALLOC_MAP
 ** @see ORWL_ALLOC_FETCH
 ** @see ORWL_ALLOC_SNATCH
 **/
P99_DECLARE_ENUM(orwl_alloc_cases,
                 orwl_alloc_close,
                 orwl_alloc_unlink,
                 orwl_alloc_unmap,
                 orwl_alloc_map,
                 orwl_alloc_fetch,
                 orwl_alloc_snatch);

/** @brief A special value for functions of type ::orwl_alloc_realloc_func */
#define ORWL_ALLOC_MAP ((size_t)(-orwl_alloc_map))

/** @brief A special value for functions of type ::orwl_alloc_realloc_func */
#define ORWL_ALLOC_FETCH ((size_t)(-orwl_alloc_fetch))

/** @brief A special value for functions of type ::orwl_alloc_realloc_func */
#define ORWL_ALLOC_SNATCH ((size_t)(-orwl_alloc_snatch))

/** @brief A special value for functions of type ::orwl_alloc_realloc_func */
#define ORWL_ALLOC_UNMAP ((size_t)(-orwl_alloc_unmap))

/** @brief A special value for functions of type ::orwl_alloc_realloc_func */
#define ORWL_ALLOC_UNLINK ((size_t)(-orwl_alloc_unlink))

/** @brief A special value for functions of type ::orwl_alloc_realloc_func */
#define ORWL_ALLOC_CLOSE ((size_t)(-orwl_alloc_close))

static_assert(!ORWL_ALLOC_CLOSE, "ORWL_ALLOC_CLOSE should always be 0");

/** @brief The maximum size of a allocation for functions of type
    ::orwl_alloc_realloc_func */
#define ORWL_ALLOC_MAX ((size_t)(-orwl_alloc_cases_amount))

inline
char const* orwl_alloc_getname(size_t val, char buf[32]) {
  if (-val <= (size_t)orwl_alloc_cases_max) {
    return orwl_alloc_cases_getname(-val);
  } else {
    sprintf(buf, "%zu", val);
    return buf;
  }
}

#ifndef DOXYGEN
#define orwl_alloc_getname(...) P99_CALL_DEFARG(orwl_alloc_getname, 2, __VA_ARGS__)
#define orwl_alloc_getname_defarg_1() ((char[32])P99_INIT)
#endif


/**
 ** @brief An API for a reallocation function.
 **
 ** The API for this type of function is similar to the one for @c
 ** realloc. That is for an allocation that currently no data a call
 ** to the function with a positive valued size argument will allocate
 ** that amount of bytes, update the ::orwl_alloc argument and return
 ** a pointer to the newly allocated resource.
 **
 ** There are several models of data @b persistence that are supported by
 ** ORWL. In general higher persistence comes with a performance hit,
 ** or stated differently, the less persistence the easier it is for
 ** the system to handle the allocation efficiently.
 **
 ** <dl>
 **
 **   <dt><b>permanent or file:</b></dt>
 **   <dd>In this case the resources
 **   corresponds to a permanently accessibly entity that is kept
 **   across booting the computer. Usually such a resource corresponds
 **   to a named file in the file system of the platform. Such
 **   resources can be accessed (through their name) by several
 **   processes. In case of a remote file system this can even be from
 **   processes on different compute nodes. Writing to the underlying
 **   persistent storage may incur performance penalties.</dd>
 **
 **  <ul> <li>::orwl_alloc_realloc_file with a valid file name as
 **   ::orwl_alloc::name falls into this category.</li></ul>
 **
 **   <dt><b>named segment:</b></dt>
 **   <dd>In this case the resources corresponds to
 **   a entity that is kept until the next boot of the
 **   computer. Usually such a resource corresponds to pseudo file in
 **   some special file system. Such resources can be accessed
 **   (through their name) by several processes, but usually this
 **   can't be done from processes on different compute nodes.
 **
 **     <ul><li>
 **     ::orwl_alloc_realloc_shm with a valid name for a segment as
 **     ::orwl_alloc::name falls into this category.
 **     </li></ul>
 **
 **   </dd>
 **
 **   <dt><b>anonymous segment:</b></dt>
 **   <dd>Similar to a named segment, but only
 **   accessible by the process that allocated the segment. So the
 **   data will be definitively lost when the process exits.
 **
 **     <ul><li> ::orwl_alloc_realloc_file with @c 0 as ::orwl_alloc::name
 **     falls into this category. But there is not much to gain with
 **     this, use ::orwl_alloc_realloc_shm or ::orwl_alloc_realloc_anon, instead.
 **     </li></ul>
 **
 **     <ul><li> ::orwl_alloc_realloc_shm with @c 0 as
 **     ::orwl_alloc::name has this model. Compared to
 **     ::orwl_alloc_realloc_anon this wastes a file descriptor of the
 **     system and might observe stronger restrictions on possible
 **     size of the segment.</li></ul>
 **
 **     <ul><li>::orwl_alloc_realloc_anon implements this model. Depending on
 **     the available routines, this might be less efficient than
 **     ::orwl_alloc_realloc_shm.
 **
 **     <ul>
 **       <li>
 **       It could have to copy data around more often.
 **       </li>
 **
 **       <li>
 **       It could have to block data for longer periods in RAM than
 **       necessary.
 **       </li>
 **     </ul>
 **
 **     </li></ul>
 **   </dd>
 **
 **
 **   <dt><b>heap:</b></dt>
 **   <dd>Similar to an anonymous segment, but implementing no
 **   persistence at all. Once an allocation is reallocated with a
 **   size that decreases (a truncation) the abandoned data is lost.
 **
 **   <ul><li>The default allocator ::orwl_alloc_realloc implements this
 **     model.</li></ul>
 **
 **   </dd>
 ** </dl>
 **
 **
 ** Another property of different types of resources is @b
 ** coherence. For resources that can be viewed of consisting of one
 ** "hidden" or external copy (e.g a file or memory in an accelerator)
 ** and an in memory copy, this describes the fact if these two copies
 ** are updated if one of them changes. Generally, if there are such
 ** different copies they can diverge (but mustn't!) and action must
 ** be taken to enforce coherence.
 **
 ** There are some special values for the size argument, as given
 ** below. If a different value from those is passed as an argument
 ** the resources is tailored to that given size (if that is different
 ** from the actual one) and the consistency model as described under
 ** ::ORWL_ALLOC_MAP is applied.
 **
 **  - ::ORWL_ALLOC_UNLINK Disconnect external storage. File
 **    persistence or named segment persistence are reduced to
 **    anonymous segment persistence. This will not directly affect
 **    the data in the running process as long as it is not released
 **    with @c size = 0. In particular, for data without external copy
 **    this is a no-op. After such a call it will generally not be
 **    possible to reestablish the data, once the resource is released
 **    or once the process is terminated.
 **
 **    [external copy is lost] [in memory copy is kept]
 **
 **  - @c 0 or ::ORWL_ALLOC_CLOSE Flush all data to the external
 **    copy (if there is such a copy) and disconnect the data from the
 **    address space of the calling thread. All pointers to the data
 **    become invalid.
 **
 **    This will release the resource (if any) and returns @c 0. For
 **    data without external copy such as heap allocations this means
 **    that the data is lost afterward. For external data such as
 **    files or segments this means that connection to the underlying
 **    storage is lost, but could be reestablished later, see the case
 **    of ORWL_ALLOC_MAP, below.
 **
 **    [external copy can be reestablished] [in memory copy is lost]
 **
 **  - ::ORWL_ALLOC_UNMAP
 **
 **    Release the resource and return @c 0. All pointers to the data
 **    become invalid.
 **
 **    Other than with ::ORWL_ALLOC_CLOSE, in the same program
 **    execution it will be possible to reestablish the data, with a
 **    call with the parameter ::ORWL_ALLOC_MAP, ::ORWL_ALLOC_FETCH or
 **    ::ORWL_ALLOC_SNATCH even if there is no external copy.
 **
 **    For the model of synchronization with a external copy of the
 **    resource, see ::ORWL_ALLOC_MAP, ::ORWL_ALLOC_FETCH and
 **    ::ORWL_ALLOC_SNATCH, respectively.
 **
 **    [external copy is kept] [in memory copy is inaccessible]
 **
 **  - ::ORWL_ALLOC_MAP -- synchronized write
 **
 **    - If previously there hadn't been neither a external nor
 **      in-memory copy a new zero initialized object is created.
 **    - If previously there had been a external copy of the
 **      resource it becomes accessible in memory.
 **    - If only an in-memory copy existed, the version of that copy
 **      becomes accessible.
 **
 **    For a resource that has been accessed through this
 **    ::ORWL_ALLOC_UNMAP will reinforce consistency and write the
 **    data to the external copy, if applicable.
 **
 **    [external copy is reestablished] [in memory copy is reestablished]
 **
 **  - ::ORWL_ALLOC_FETCH -- copy on write
 **
 **    If previously there had been a external copy of the resource
 **    it becomes accessible in-memory. If there hadn't been such a
 **    external copy a new zero initialized object is created.
 **
 **    For a resource with external copy that has been accessed
 **    through this ::ORWL_ALLOC_UNMAP will throw away the in-memory
 **    copy.
 **
 **    For a resource without external copy this mode is equivalent to
 **    ::ORWL_ALLOC_MAP.
 **
 **    [external copy is reestablished] [in memory copy maybe lost]
 **
 **  - ::ORWL_ALLOC_SNATCH -- write through
 **
 **    If previously there hadn't been an in-memory copy a new zero
 **    initialized object is created, otherwise access is granted to
 **    that in-memory copy. If previously there existed a external
 **    copy, that copy is lost.
 **
 **    For a resource that has been accessed through this
 **    ::ORWL_ALLOC_UNMAP will reinforce consistency and write the
 **    data to the external copy, if applicable.
 **
 **    [external copy is lost] [in memory copy is reestablished]
 **
 ** @warning Additional allocators (e.g graphics memory or other
 ** devices) should respect one of the persistence and coherence
 ** models as described above and document their choice.
 **/
typedef void* orwl_alloc_realloc_func(orwl_alloc*, size_t);

/**
 ** @brief Manage different types of memory resources
 **
 ** ORWL may attach different types of memory resources to a
 ** location. Per default this will be memory on the heap, allocated
 ** with @c malloc and friends, but provided are also memory that is
 ** mapped to a file or a inter-process memory segment. As extension
 ** you might have a resource mapped to special memory for your GPU,
 ** or another device driver.
 **
 ** @see orwl_alloc_realloc_func for a description of the API for
 ** reallocation.
 **/
struct orwl_alloc {
  /**
   ** @brief A textual name of the resource
   **
   ** For files and segments this would all naturally be their name,
   ** for other types this might be just @c 0 or any other type of
   ** data that helps to identify the resource.
   **
   ** @warning The parameter to ::orwl_alloc_init will be copied with
   ** @c strcpy (or equivalent) before being stored here.
   **
   ** @warning This C string is <code>free</code>d when this allocator
   ** is destroyed with ::orwl_alloc_destroy.
   **/
  char const* name;
  /**
   ** @brief A type dependent data pointer
   **
   ** This is used by #alloc to store the state of an allocation. ORWL
   ** will never look at that but always go through #alloc to get a
   ** pointer to whatever that will provide.
   **
   ** @remark The allocator function is responsible to release that
   ** area when this allocator is destroyed.
   **
   ** @private
   **/
  void * data;

  /**
   ** @brief The offset of an allocation inside a resource
   **
   ** @remark This field makes only sense for some of the #alloc
   ** functions, such as #orwl_alloc_realloc_file,
   ** #orwl_alloc_realloc_shm and #orwl_alloc_realloc_part. For others
   ** it will generally be just @c 0.
   **
   ** @remark Some allocation types have restrictions on the values
   ** that are allowed for #offset. In particular
   ** #orwl_alloc_realloc_file and #orwl_alloc_realloc_shm require
   ** that this is a multiple of the blocksize of the underlying file
   ** or segment.
   **
   **/
  size_t offset;

  /**
   ** @brief The actual size of the resource
   **
   ** This field is expected to be maintained by the #alloc function
   ** and it should be safe to query it.
   **/
  size_t size;

  /**
   ** @brief The "maximum" size of the resource
   **
   ** @warning This field is <em>not</em> expected to be maintained by
   ** the #alloc function so you can not rely on it to be consistent.
   **
   ** @private
   **/
  size_t max_size;
  /**
   ** @brief A reference counter for this allocation
   **
   ** @warning Don't use this directly but use the function
   ** ::orwl_alloc_account to add a reference to the count.
   **
   ** This count starts with a value of one and will be taken into
   ** account for ::orwl_alloc_destroy and ::orwl_alloc_delete.
   **
   ** @private
   **/
  _Atomic(size_t) p99_cnt;
  /**
   ** @brief The (re)allocation function for this resource
   **
   ** @see orwl_alloc_realloc for heap allocated memory, default
   ** @see orwl_alloc_realloc_file for file mapped memory
   ** @see orwl_alloc_realloc_shm for segment mapped memory
   ** @see orwl_alloc_realloc_anon for segment mapped memory
   **/
  orwl_alloc_realloc_func* alloc;
};

/**
 ** @brief Allocate an ORWL resource on the heap
 ** @related orwl_alloc
 **
 ** This is the default allocation function for the resource that is
 ** associated to a location. It is a wrapper around ::realloc.
 **/
inline
void* orwl_alloc_realloc(orwl_alloc* a, size_t size) {
  if (!a) {
    return 0;
  }
  orwl_deep_instr_inline_start();
  register orwl_alloc_realloc_func*const alloc = a->alloc;
  if (P99_LIKELY(!alloc) || P99_UNLIKELY(a->alloc == orwl_alloc_realloc)) {
    register void* adata = a->data;
    register size_t const asize = a->size;
    if (-size > (size_t)orwl_alloc_cases_max) {
      if (P99_LIKELY(size != asize)) {
        adata = realloc(adata, size);
        if (P99_UNLIKELY(!adata)) P99_THROW(ENOMEM);
        a->data = adata;
        a->size = size;
      }
    } else {
      switch (-size) {
      case orwl_alloc_map: break;
      case orwl_alloc_snatch: break;
      case orwl_alloc_fetch: break;
      case orwl_alloc_close:
        free(adata);
        a->data = 0;
        a->size = 0;
      case orwl_alloc_unmap:
        adata = 0;
        break;
      }
    }
    orwl_deep_instr_inline_stop();
    return adata;
  } else {
    orwl_deep_instr_inline_stop();
    return alloc(a, size);
  }
}



#define ORWL_ALLOC_INITIALIZER { .name = 0, .p99_cnt = ATOMIC_VAR_INIT(0), }

P99_MACRO_END(orwl_alloc);

DOCUMENT_INIT(orwl_alloc)
inline
orwl_alloc* orwl_alloc_init(orwl_alloc* a, char const* name, orwl_alloc_realloc_func* alloc, size_t size, size_t offset) {
  if (a) {
    extern char* (strdup)(const char*);
    *a = (orwl_alloc)ORWL_ALLOC_INITIALIZER;
    atomic_init(&a->p99_cnt, 0u);
    if (name) a->name = strdup(name);
    if (alloc) a->alloc = alloc;
    if (offset) a->offset = offset;
    if (size) orwl_alloc_realloc(a, size);
    else {
      /* Allow to fetch information from an external copy. */
      if (name) orwl_alloc_realloc(a, ORWL_ALLOC_MAP);
    }
  }
  return a;
}

#ifndef DOXYGEN
inline
P99_PROTOTYPE(orwl_alloc*, orwl_alloc_init, orwl_alloc *, char const*, orwl_alloc_realloc_func*, size_t, size_t);
#define orwl_alloc_init(...) P99_CALL_DEFARG(orwl_alloc_init, 5, __VA_ARGS__)
#define orwl_alloc_init_defarg_1() 0
#define orwl_alloc_init_defarg_2() 0
#define orwl_alloc_init_defarg_3() 0
#define orwl_alloc_init_defarg_4() 0
#endif

P99_MACRO_END(orwl_alloc);

DOCUMENT_DESTROY(orwl_alloc)
/**
 ** @warning this takes the reference counting of ::orwl_alloc::p99_cnt
 ** into account. A real destruction will only be effected if that
 ** count has been @c 1, previously.
 **
 ** @return the previous value of ::orwl_alloc::p99_cnt
 **/
inline
void orwl_alloc_destroy(orwl_alloc* a) {
  if (P99_UNLIKELY(!a)) P99_THROW(EINVAL);
  void* p = a->data;
  if (p) {
    orwl_alloc_realloc_func* alloc = a->alloc;
    if (!alloc) alloc = orwl_alloc_realloc;
    alloc(a, ORWL_ALLOC_CLOSE);
    a->data = 0;
    a->max_size = 0;
    a->size = 0;
    a->alloc = 0;
  }
  free((void*)(a->name));
}

P99_MACRO_END(orwl_alloc);

/**
 ** @brief Delete an ::orwl_alloc
 **
 ** @warning this takes the reference counting of ::orwl_alloc::p99_cnt
 ** into account. A real deletion including the @c free of the object
 ** pointed to by @a a will only be effected if that count has been @c
 ** 1, previously.
 **/
inline
void orwl_alloc_delete(orwl_alloc const* a) {
  orwl_deep_instr_inline_start();
  if (P99_UNLIKELY(!a)) P99_THROW(EINVAL);
  orwl_alloc_destroy((orwl_alloc*)a);
  free((orwl_alloc*)a);
  orwl_deep_instr_inline_stop();
}

P99_TP_REF_FUNCTIONS(orwl_alloc);

#define O_RWL_ALLOC_MAP(NAME, ALLOC, SIZE)                                                   \
P00_BLK_START                                                                                \
P00_BLK_DECL(orwl_alloc_ref*const, P99_FILEID(alloc0), (ALLOC))                              \
  P00_BLK_BEFORE(orwl_alloc_ref P99_FILEID(alloc1)                                           \
                 = ORWL_ALLOC_REF_INITIALIZER(P99_FILEID(alloc0) ? 0 : P99_NEW(orwl_alloc))) \
  P00_BLK_BEFORE((P99_FILEID(alloc0)                                                         \
                  ? orwl_alloc_ref_assign(&P99_FILEID(alloc1),                               \
                                          P99_FILEID(alloc0))                                \
                  : P99_NOP))                                                                \
  P99_GUARDED_BLOCK                                                                          \
  (register orwl_alloc*const, /* TYPE */                                                     \
   P99_FILEID(alloc),         /* NAME */                                                     \
   orwl_alloc_ref_get(&P99_FILEID(alloc1)),                                                  \
   ,                                                                                         \
   (orwl_alloc_realloc(P99_FILEID(alloc), ORWL_ALLOC_UNMAP),                                 \
    orwl_alloc_ref_destroy(&P99_FILEID(alloc1))))                                            \
  P00_BLK_DECL(register void*const, NAME, orwl_alloc_realloc(P99_FILEID(alloc), (SIZE)))

#define ORWL_ALLOC_SECTION(...)                                \
P99_IF_LT(P99_NARG(__VA_ARGS__), 3)                            \
(O_RWL_ALLOC_MAP(__VA_ARGS__, ORWL_ALLOC_MAP))                 \
(O_RWL_ALLOC_MAP(__VA_ARGS__))


#ifndef DOXYGEN
P99_PROTOTYPE(void, orwl_alloc_copy, orwl_alloc_ref *, char const*, orwl_alloc_realloc_func*);
#define orwl_alloc_copy(...) P99_CALL_DEFARG(orwl_alloc_copy, 3, __VA_ARGS__)
#define orwl_alloc_copy_defarg_2() 0
#else
/**
 ** @brief copy an ::orwl_alloc object and return that copy in the
 ** same reference object @c *obj
 **
 ** @remark @a name and @a func have suitable defaults if they are
 ** omitted
 **
 ** @related orwl_alloc_ref
 **/
void orwl_alloc_copy(orwl_alloc_ref *obj, char const*name, orwl_alloc_realloc_func*func);
#endif

#ifndef DOXYGEN
P99_PROTOTYPE(void, orwl_alloc_copyone, orwl_alloc_ref*, orwl_alloc_ref*, char const*, orwl_alloc_realloc_func*);
#define orwl_alloc_copyone(...) P99_CALL_DEFARG(orwl_alloc_copyone, 4, __VA_ARGS__)
#define orwl_alloc_copyone_defarg_2() 0
#define orwl_alloc_copyone_defarg_3() 0
#else
/**
 ** @brief copy or move an ::orwl_alloc object referenced by @c
 ** *source to @c *target
 **
 ** If @c *source holds the only reference to the object (ie. the
 ** counter is @c 1) the reference is just moved to @c
 ** *target. Otherwise the object is copied and the reference to the
 ** new copy is returned in @c *target.
 **
 ** @remark in any of the two cases @c *source will hold the value @c
 ** 0, afterward.
 **
 ** @remark @a name and @a func have suitable defaults if they are
 ** omitted
 **
 ** @related orwl_alloc_ref
 **/
void orwl_alloc_copyone(orwl_alloc_ref* target, orwl_alloc_ref* source, char const*name, orwl_alloc_realloc_func*func);
#endif

/**
 ** @brief Allocate an ORWL resource in a file
 ** @related orwl_alloc
 **
 ** This uses ::open, ::close and ::unlink to access the file that is
 ** given through the ::orwl_alloc::name component. The file is mapped
 ** through ::mmap into the address space.
 **
 ** There are special naming conventions that manage the file access
 ** mode, that is similar to what is used by perl and some other
 ** languages. Leading '<>' characters in the name are not considered
 ** to be part of the name itself but are used to chose the mode:
 **
 **   '<' or no prefix opens copy-on-write. The only allowed realloc
 **       mode is "fetch".
 **
 **   '>' opens read-write, creates a new file if necessary and
 **       truncates the file to 0 length.
 **
 **   '>>' opens read-write, creates a new file if necessary and
 **        provides the current contents through the allocation
 **
 ** @remark Besides the obvious reasons, an allocation of a file may
 ** also fail if the offset member of the #orwl_alloc is not a
 ** multiple of the file's blocksize.
 **/
void* orwl_alloc_realloc_file(orwl_alloc*, size_t);

/**
 ** @brief Allocate an ORWL resource in a memory segment
 ** @related orwl_alloc
 **
 ** This uses ::shm_open, ::close and ::shm_unlink to access the
 ** memory segment that is given through the ::orwl_alloc::name
 ** component. The memory segment is mapped through ::mmap into the
 ** address space.
 **
 ** @remark If the ::orwl_alloc::name component is not set, the
 ** segment is anonymous.
 **
 ** @remark This allocator reserves one file descriptor per
 ** segment. This has the advantage of being portable on all POSIX
 ** allowing to resize the segment, even if it is anonymous.
 **
 ** @remark Besides the obvious reasons, an allocation of a segment
 ** may also fail if the orwl_alloc::offset member is not a multiple
 ** of the blocksize.
 **
 ** @see orwl_alloc_realloc_anon for a less portable anonymous version
 ** that doesn't waste a file descriptor.
 **/
void* orwl_alloc_realloc_shm(orwl_alloc*, size_t);


#if defined(MAP_ANONYMOUS) || defined(MAP_ANON)
# ifndef MAP_ANONYMOUS
#  define MAP_ANONYMOUS MAP_ANON
# endif
#endif

#if defined(MAP_ANONYMOUS) || defined(DOXYGEN)
/**
 ** @brief Allocate an ORWL resource in an anonymous memory segment
 ** @related orwl_alloc
 **
 ** @warning This uses the feature @c MAP_ANONYMOUS that is not
 ** guaranteed by POSIX but present on many POSIX systems.
 **
 ** @warning If the feature @c MREMAP_MAYMOVE is not present on the
 ** platform, resizing of such an allocation may systematically result
 ** in copy operation.
 **/
void* orwl_alloc_realloc_anon(orwl_alloc*, size_t);
#endif

/**
 ** @brief Relate to an otherwise obtained address.
 ** @related orwl_alloc
 **
 ** This can be used to handle an address that we received by other
 ** means, e.g a device address or just some static array, with the
 ** same glue as dynamic allocation.
 **
 ** @remark For this type of "allocation" a non-zero
 ** orwl_alloc::offset makes not much sense and should be avoided.
 **/
void* orwl_alloc_realloc_hard(orwl_alloc*, size_t);

/**
 ** @brief Relate some contiguous part of another allocation @a a.
 ** @related orwl_alloc
 **
 ** This can be used to handle just a part of @a a, and thereby divide
 ** @a into pieces that can be handled by different threads.
 **
 ** @remark For this type of "allocation" a non-zero @a size makes no
 ** sense. So this function will fail an return @c 0.
 **/
orwl_alloc* orwl_alloc_partial(orwl_alloc* a, size_t offset, size_t size);



/**
 ** @}
 **/

#endif
