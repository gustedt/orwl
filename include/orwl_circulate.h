/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the author and copyright holder for this work is                           */
/* all rights reserved,  2013, 2016 Jens Gustedt, INRIA, France               */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#ifndef     ORWL_CIRCULATE_H_
# define    ORWL_CIRCULATE_H_

#include "orwl_server.h"
#include "orwl_helpers.h"

P99_DECLARE_STRUCT(orwl_circulate);

/**
 ** @brief A handle circulation to manage the circulation of data
 **/
struct orwl_circulate {
  orwl_alloc_ref current;
  orwl_alloc_ref subsequent;
  p99_notifier received;
  orwl_handle2 hand[3];
};

/**
 ** @brief Initialize an ::orwl_circulate data structure
 **
 ** @see orwl_circulate_insert
 ** @see ORWL_CIRCULATE_SECTION
 ** @related orwl_circulate
 **/
#define ORWL_CIRCULATE_INITIALIZER { .hand = { P99_DUPL(3, ORWL_HANDLE2_INITIALIZER), }, }

/**
 ** @brief Insert a circulation handle @a circ at location @a loc
 **
 ** @a loc is a @em relative location id, i.e usually a location name
 ** as it has been defined with ::ORWL_LOCATIONS_PER_TASK.
 **
 ** @a sense determines the sense of the circular shift. The default,
 ** @c 1, has as an effect that this task next will see the data of
 ** next higher numbered task etc. @c -1 has the inverse effect, the
 ** next data to see would be the one from the previous, lower
 ** numbered task
 **.
 ** @related orwl_circulate
 **/
O_RWL_DOCUMENT_SEED
P99_DEFARG_DOCU(orwl_circulate_insert)
void orwl_circulate_insert(orwl_circulate*const circ,    /*!< [in,out] the handle(s) for the circulation request */
                           size_t loc,                   /*!< the relative location id */
                           size_t sense,                 /*!< the sense of the circulation, defaults to 1. */
                           p99_seed* seed,               /*!< [in,out] defaults to a thread local seed */
                           orwl_server * srv,
                           size_t orwl_locations_amount, /*!< the number of locations per task */
                           orwl_mirror * location        /*!< [in,out] the location(s) for the request */
                          );

#define orwl_circulate_insert(...)  P99_CALL_DEFARG(orwl_circulate_insert, 7, __VA_ARGS__)

#define orwl_circulate_insert_defarg_2() 1
#define orwl_circulate_insert_defarg_3() p99_seed_get()
#define orwl_circulate_insert_defarg_4() orwl_server_get()
#define orwl_circulate_insert_defarg_5() orwl_locations_amount
#define orwl_circulate_insert_defarg_6() P99_RVAL(orwl_mirror*)


inline
void orwl_circulate_disconnect(orwl_circulate* circ) {
  p99_notifier_block(&circ->received);
  orwl_handle2_disconnect(circ->hand, P99_ALEN(circ->hand));
  orwl_alloc_ref_destroy(&circ->current);
  orwl_alloc_ref_destroy(&circ->subsequent);
}

/**
 ** @brief Map the data of the circulation handle into the address space.
 **
 ** @remark This is a read-only mapping, thus the @c const
 ** qualification of the return type. Write access can be achieved
 ** directly through the ::orwl_alloc that is passed around with
 ** ::ORWL_CIRCULATE_SECTION, anyhow.
 **
 ** @remark This can only be used inside a critical section that is
 ** marked with ::ORWL_CIRCULATE_SECTION.
 **
 ** @related orwl_circulate
 **
 ** @see ORWL_ALLOC_MAP to access the data exclusively before or after
 ** a critical section that is marked with ::ORWL_CIRCULATE_SECTION.
 **/
inline
void const* orwl_circulate_map(orwl_circulate* circ) {
  return orwl_alloc_realloc(orwl_alloc_ref_get(&circ->current), ORWL_ALLOC_MAP);
}

/**
 ** @related orwl_circulate
 **/
P99_PROTOTYPE(void, orwl_circulate_replace, orwl_circulate*, orwl_alloc_ref*, p99_seed*);

/**
 ** @related orwl_circulate
 **/
P99_PROTOTYPE(void, orwl_circulate_hold, orwl_circulate*, orwl_alloc_ref*, p99_seed*);

/**
 ** @brief Synchronize the task on circulating data @a NEXT
 **
 ** @param CIRC is a pointer to a circulation handle ::orwl_circulate
 ** @param NEXT is a pointer to a ::orwl_alloc pointer.
 **
 ** Before entering the section, @c *NEXT represents a data block that
 ** has been obtained previously by the task. Inside the section @em
 ** read access can be obtain to the data by means of
 ** ::orwl_circulate_map.
 **
 ** When leaving the section, @c *NEXT has been passed around in the
 ** cyclic ring of tasks that has been created. That is the data that
 ** has been hold previous has been passed over to the next task in
 ** row; in turn, the data from the previous task has been received
 ** and is available through @c *NEXT.
 **
 ** Before entering and after leaving such a critical section, the
 ** access through the ::orwl_alloc @a NEXT is exclusive. In contrast
 ** to that the access during the critical section is inclusive and so
 ** the data cannot be modified while inside the critical section. The
 ** principal advantage of that is that while we are working in the
 ** critical section, a remote transfer (which also only needs
 ** inclusive access) may already be performed. Thus computation and
 ** communication may overlap.
 **
 ** @related orwl_circulate
 **/
#define ORWL_CIRCULATE_SECTION(CIRC, NEXT)                                           \
P00_BLK_START                                                                        \
P00_BLK_DECL(orwl_alloc_ref*const, P99_FILEID(next), (NEXT))                         \
P00_BLK_DECL(p99_seed*, P99_FILEID(circseed), p99_seed_get())                        \
P99_GUARDED_BLOCK                                                                    \
  (orwl_circulate*const, /* TYPE */                                                  \
   P99_FILEID(circ),     /* NAME */                                                  \
   (CIRC),               /* INITIAL */                                               \
   orwl_circulate_replace(P99_FILEID(circ), P99_FILEID(next), P99_FILEID(circseed)), \
   orwl_circulate_hold(P99_FILEID(circ), P99_FILEID(next), P99_FILEID(circseed)))    \
  ORWL_SECTION(&P99_FILEID(circ)->hand[1], 1, P99_FILEID(circseed))



#endif
