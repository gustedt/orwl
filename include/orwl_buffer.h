/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the author and copyright holder for this work is                           */
/* all rights reserved,  2011-2013 Jens Gustedt, INRIA, France                */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
/*
** orwl_buffer.h
**
** Made by Jens Gustedt
** Login   <gustedt@damogran.loria.fr>
**
** Started on  Tue Jul  5 08:25:17 2011 Jens Gustedt
** Last update Tue Jul  5 08:25:17 2011 Jens Gustedt
*/

#ifndef     ORWL_BUFFER_H_
# define    ORWL_BUFFER_H_

#include "orwl_alloc.h"
#include "orwl_new.h"

/**
 ** @addtogroup library_low
 ** @{
 **/


P99_DECLARE_STRUCT(orwl_buffer);
P99_DECLARE_STRUCT(aiocb);

#if _POSIX_ASYNCHRONOUS_IO <= 0
# ifndef SIGEV_THREAD
#  define SIGEV_NONE 0
#  define SIGEV_SIGNAL 2
#  define SIGEV_THREAD 3
union sigval {
  int    sival_int;    //!< integer signal value
  void*  sival_ptr;    //!< pointer signal value
};
struct sigevent {
  int                      sigev_notify;            //!< notification type
  int                      sigev_signo;             //!< signal number
  union sigval             sigev_value;             //!< signal value
  void(*)(union sigval)    sigev_notify_function;   //!< notification function
  (pthread_attr_t*)        sigev_notify_attributes; //!< notification attributes
};
# endif
struct aiocb {
  int             aio_fildes;     //!< File descriptor.
  off_t           aio_offset;     //!< File offset.
  volatile void  *aio_buf;        //!< Location of buffer.
  size_t          aio_nbytes;     //!< Length of transfer.
  int             aio_reqprio;    //!< Request priority offset.
  struct sigevent aio_sigevent;   //!< Signal number and value.
  int             aio_lio_opcode; //!< Operation to be performed.
};
#endif

/**
 ** @brief A data buffer as passed around between different ORWL
 ** functions.
 **/
struct orwl_buffer {
  aiocb aio;
  orwl_alloc_ref allocr;
};

#define ORWL_AIOCB_INITIALIZER(LEN, ...)                       \
  {                                                            \
  .aio_fildes = -1,                                            \
    .aio_buf = (__VA_ARGS__),                                  \
    .aio_nbytes = (LEN),                                       \
      .aio_sigevent = {                                        \
    .sigev_notify = SIGEV_NONE,                                \
    }                                                          \
  }


/**
 ** @see orwl_buffer
 **/
#define ORWL_BUFFER_INITIALIZER64(LEN, DATA) {                  \
    .aio = ORWL_AIOCB_INITIALIZER(sizeof(uint64_t[LEN]), DATA), \
    .allocr = ORWL_ALLOC_REF_INITIALIZER(0),                    \
 }


#define ORWL_BUFFER_INITIALIZER8(LEN, DATA, ALLOC) {           \
    .aio = ORWL_AIOCB_INITIALIZER(LEN, DATA),                  \
    .allocr = ORWL_ALLOC_REF_INITIALIZER(ALLOC),               \
 }


#ifndef DOXYGEN
inline
P99_PROTOTYPE(orwl_buffer*, orwl_buffer_init, orwl_buffer *, size_t, uint64_t*);
#define orwl_buffer_init(...) P99_CALL_DEFARG(orwl_buffer_init, 3, __VA_ARGS__)
#define orwl_buffer_init_defarg_1() P99_0(size_t)
#define orwl_buffer_init_defarg_2() P99_0(uint64_t*)
#endif

/**
 ** @related orwl_buffer
 **/
inline
orwl_buffer* orwl_buffer_init(orwl_buffer *buf, size_t len, uint64_t* data) {
  if (buf) {
    buf->aio = (aiocb)ORWL_AIOCB_INITIALIZER(len, data);
    (void)orwl_alloc_ref_init(&buf->allocr, (void*)0);
  }
  return buf;
}

/**
 ** @related orwl_buffer
 **/
inline
void orwl_buffer_destroy(orwl_buffer *buf) {
  orwl_alloc_ref_destroy(&buf->allocr);
  /* Make sure that we don't leak old pointer values. */
  buf->aio = (aiocb)ORWL_AIOCB_INITIALIZER(0, 0);
}

DECLARE_NEW_DELETE(orwl_buffer);

/**
 ** @related orwl_buffer
 ** @brief Advance the buffer @a buf by @a res 8 byte words
 **/
inline
void orwl_buffer_advance(orwl_buffer *buf, ssize_t res) {
  buf->aio.aio_buf += (res*sizeof(uint64_t));
  buf->aio.aio_nbytes -= (res*sizeof(uint64_t));
}

/**
 ** @brief prepare synchronous or asynchronous IO
 **
 ** @param mess the message buffer
 ** @param out write operation if @c true, read otherwise
 **
 ** @return the new offset after the IO operation would have completed
 **/
size_t orwl_buffer_prepare(int fd, orwl_buffer* mess, bool out, size_t offset);

/**
 ** @brief synchronous or asynchronous IO on a set of buffers.
 **
 ** @param n the number of message buffers
 ** @param mess the array of message buffers
 ** @param blocking synchronous operation if @c true, asynchronous otherwise.
 **
 ** @remark After the IO is finished ::orwl_buffer_close should be
 ** called later to close the file descriptors.
 **
 ** @remark If the operation is asynchronous, ::orwl_buffer_complete
 ** should be called later to ensure that all IO is terminated.
 **
 ** @return the number of request that are already known to be
 ** completed. If this is a blocking request, this should be @a
 ** n. Otherwise this might be any number between @c 0 and @a n.
 **/
size_t orwl_buffer_io(size_t n, orwl_buffer mess[n], bool blocking);

void orwl_buffer_complete(size_t n, orwl_buffer mess[n]);
void orwl_buffer_close(size_t n, orwl_buffer mess[n]);


/**
 ** @struct iovec
 ** @brief A data buffer as seen by the low level socket functions.
 **/
struct iovec;

P99_DECLARE_STRUCT(iovec);

/**
 ** @related iovec
 **/
inline
iovec* iovec_init(iovec *buf, size_t len, uint64_t* data) {
  if (buf) {
    *buf = P99_LVAL(iovec, .iov_len = len, .iov_base = data);
  }
  return buf;
}

/**
 ** @related iovec
 **/
inline
void iovec_destroy(iovec *buf) {
  /* empty */
}

/**
 ** @brief convert an ::orwl_buffer to an ::iovec
 **/
inline
iovec orwl_buffer2iovec(orwl_buffer volatile const* buf) {
  return P99_LVAL(iovec, .iov_len = buf->aio.aio_nbytes, .iov_base = (void*)buf->aio.aio_buf);
}

/**
 ** @brief convert an ::iovec to an ::orwl_buffer
 **/
inline
orwl_buffer iovec2buffer(iovec iovec) {
  return (orwl_buffer)ORWL_BUFFER_INITIALIZER8(iovec.iov_len, iovec.iov_base, 0);
}

/**
 ** @related iovec
 ** @brief Advance the buffer @a buf by @a res bytes
 **/
inline
void iovec_advance(iovec *buf, ssize_t res) {
  buf->iov_base = (char*)(buf->iov_base) + res;
  buf->iov_len -= res;
}


/**
 ** @}
 **/

#endif      /* !ORWL_BUFFER_H_ */
