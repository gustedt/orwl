/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the authors and copyright holders for this work are as follows:            */
/* all rights reserved,  2010-2016 Jens Gustedt, INRIA, France                */
/* all rights reserved,  2013 Rodrigo Campos, INRIA, France                   */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#ifndef     ORWL_THREAD_H_
# define    ORWL_THREAD_H_

#include "orwl_int.h"
#include "orwl_time.h"
#include "orwl_notifier.h"
#include "orwl_document.h"
#include "orwl_deep_instr.h"
#include "p99_block.h"
#include "p99_rwl.h"

/**
 ** @addtogroup environment
 ** @{
 **/

#ifdef DOXYGEN
/**
 ** @brief Declare and document an environment variable that is to be
 ** use by your program.
 **
 ** With a declaration of
 **
 ** @code
 ** // @brief This is my input file
 ** ORWL_DECLARE_ENV(MY_FILE)
 ** @endcode
 **
 ** the environment variable @c MY_FILE can be referred in the code as
 **
 ** @code
 ** char const *my_file = MY_FILE();
 ** if (strcmp(my_file, "toto.txt") ...
 ** @endcode
 **
 ** If you are using doxygen to document your code, this has also the
 ** effect that that variable will be listed in the environment
 ** variables page of your documentation, just as it is done for such
 ** variables that are
 ** @link env.html
 ** used by the ORWL library.
 ** @endlink
 **
 ** @see ORWL_DEFINE_ENV must be used in just one .c file to ensure
 ** that the function is always generated.
 **/
#define ORWL_DECLARE_ENV(NAME)                                                                                                                           \
/** \xrefitem env "environment variable" "Environment Variables" Environment variable <code>NAME</code> should be retrieved by calling this function **/ \
/** \see ORWL_DECLARE_ENV                                           **/                                                                                  \
inline                                                                                                                                                   \
char const* NAME() {                                                                                                                                     \
  return getenv(#NAME);                                                                                                                                  \
}                                                                                                                                                        \
P99_MACRO_END(NAME, _env)
#else
#define ORWL_DECLARE_ENV(NAME)                                 \
inline                                                         \
char const* NAME(void) {                                       \
  return getenv(#NAME);                                        \
}                                                              \
P99_MACRO_END(NAME, _env)
#endif

/**
 ** @brief Force the generation of the function for environment variable @a NAME
 **
 ** @see ORWL_DECLARE_ENV
 **/
#define ORWL_DEFINE_ENV(NAME) P99_INSTANTIATE(char const*, NAME, void)

/**
 ** @}
 **/


/**
 ** @addtogroup library_support
 ** @{
 **/

P99_DECLARE_THREAD_LOCAL(size_t, o_rwl_phase);

#define ORWL_TERM_ALERT "\033[1;31m"
#define ORWL_TERM_REMARK "\033[1;34m"
#define ORWL_TERM_OFF "\033[0m"
#define ORWL_TERM_CLEAR "\033[2K\r"

/**
 ** @brief @c false if @c stderr is connected to a files
 **/
extern bool orwl_terminal;

/**
 ** @brief A file name to be used as @c stderr
 **
 ** If this environment variable is set the standard stream @c stderr
 ** will be connected to a file of that name. This takes effect as
 ** early as possible during the initialization of ORWL.
 **/
ORWL_DECLARE_ENV(ORWL_STDERR);

/**
 ** @brief A default version of something like a phase your thread is
 ** in.
 **
 ** This variable here is thread local.
 **/
#define orwl_phase P99_THREAD_LOCAL(o_rwl_phase)

/**
 ** @brief Internally use by report().
 **/
#ifdef __GNUC__
__attribute__((format(printf, 5, 6)))
#endif
extern void orwl_report(size_t myloc, size_t nl, size_t sl, size_t phase, char const* format, ...);

#ifdef __GNUC__
__attribute__((format(printf, 7, 8)))
#endif
extern void orwl_trace(size_t myloc, size_t nl, size_t sl, size_t phase, char const func[], size_t line, char const* format, ...);

/**
 ** @brief Internally use by progress().
 **/
#ifdef __GNUC__
__attribute__((format(printf, 6, 7)))
#endif
extern void orwl_progress(size_t t, size_t myloc, size_t nl, size_t sl, size_t phase, char const* format, ...);

/**
 ** @brief Report to @c stderr if the first argument has a true value.
 **
 ** The other arguments should just be as for the @c printf family,
 ** i.e a format followed with a list of arguments. No format is
 ** equivalent to giving a format of @c "".
 **
 ** The output is prefixed with an identification of the thread. This
 ** is either
 **  - the triplet consisting of ::orwl_myloc, ::orwl_nl, ::orwl_sl
 **    followed the global variable #orwl_phase or of a local variable with
 **    the same name
 **  - if this information is not available the return of @c thrd_current.
 **/
#define report(...)                                            \
P99_IF_LT(P99_NARG(__VA_ARGS__), 2)                            \
 (report1(__VA_ARGS__, (char const[]){""}))                    \
 (report1(__VA_ARGS__))

#define report1(exp, ...)                                                            \
do {                                                                                 \
  if ((bool)exp) {                                                                   \
    orwl_server* srv = orwl_server_get();                                            \
    orwl_address_book* ab = srv ? srv->ab : 0;                                       \
    orwl_report(ORWL_MYLOC(ab), ORWL_NL(ab), ORWL_SL(srv), orwl_phase, __VA_ARGS__); \
  }                                                                                  \
 } while(0)

#define O_RWL_REPORT_2(...) report(__VA_ARGS__)
#define O_RWL_REPORT_3(EXP, FORMAT, ...) O_RWL_REPORT_2(EXP, FORMAT, P99_FORMATS(__VA_ARGS__))

#define REPORT(EXP, ...)                                                                                \
P99_IF_LT(P99_NARG(__VA_ARGS__), 2)(O_RWL_REPORT_2(EXP, __VA_ARGS__))(O_RWL_REPORT_3(EXP, __VA_ARGS__))

/**
 ** @brief Report progress to @c stderr if @a expr is fulfilled. @a t
 ** is supposed to be a progress count.
 **
 ** @a t modulo 4 is used to print a spinning \.
 **
 ** The remainder of the arguments are interpreted (and printed) as
 ** for ::report.
 **/
#define progress(exp, t, ...)                                                             \
do {                                                                                      \
  if ((bool)exp) {                                                                        \
    orwl_server* srv = orwl_server_get();                                                 \
    orwl_address_book* ab = srv ? srv->ab : 0;                                            \
    orwl_progress(t, ORWL_MYLOC(ab), ORWL_NL(ab), ORWL_SL(srv), orwl_phase, __VA_ARGS__); \
  }                                                                                       \
 } while(0)

#define O_RWL_PROGRESS_2(...) progress(__VA_ARGS__)
#define O_RWL_PROGRESS_3(EXP, T, FORMAT, ...) O_RWL_PROGRESS_2(EXP, T, FORMAT, P99_FORMATS(__VA_ARGS__))

#define PROGRESS(EXP, T, ...)                                                                                     \
P99_IF_LT(P99_NARG(__VA_ARGS__), 2)(O_RWL_PROGRESS_2(EXP, T, __VA_ARGS__))(O_RWL_PROGRESS_3(EXP, T, __VA_ARGS__))

/**
 ** @brief Trace the execution to @c stderr if the first argument has
 ** a "true" value.
 **
 ** The arguments are interpreted (and printed) as for ::report, but
 ** additionally prefixed with a time stamp that emulates a global
 ** clock. That time stamp consists of a pair of values in hexadecimal
 ** representation.
 **  - The first of this pair counts the number of times that the
 **    global barrier has been passed.
 **  - The second value are the micro-seconds that have elapsed
 **    since the last barrier.
 **
 ** This is meant to do a post mortem analysis of an
 ** execution. Something like <code>sort mylogfile*.log</code> should
 ** give you a merge of a set of logfiles that merges the lines
 ** according to their time stamps.
 **
 ** Although this method doesn't give an ideal global clock, this
 ** should only be perturbed by something in the order of magnitude of
 ** perceived network latency. So distant events that are known to
 ** have a casual relation should order well.
 **
 ** @remark This should not be abused for too fine measurements: the
 ** clock is not precise enough for that and also the overhead of this
 ** function may cause Heisenberg effects.
 **/
#define trace(...)                                             \
P99_IF_LT(P99_NARG(__VA_ARGS__), 2)                            \
 (trace1(__VA_ARGS__, (char const[]){""}))                     \
 (trace1(__VA_ARGS__))

#define trace1(exp, ...)                                                                                \
do {                                                                                                    \
  if ((bool)(exp)) {                                                                                    \
    orwl_server* srv = orwl_server_get();                                                               \
    orwl_address_book* ab = srv ? srv->ab : 0;                                                          \
    orwl_trace(ORWL_MYLOC(ab), ORWL_NL(ab), ORWL_SL(srv), orwl_phase, __func__, __LINE__, __VA_ARGS__); \
  }                                                                                                     \
 } while(0)

#define O_RWL_TRACE_2(...) trace(__VA_ARGS__)
#define O_RWL_TRACE_3(EXP, FORMAT, ...) O_RWL_TRACE_2(EXP, FORMAT, P99_FORMATS(__VA_ARGS__))

#define TRACE(EXP, ...)                                                                               \
P99_IF_LT(P99_NARG(__VA_ARGS__), 2)(O_RWL_TRACE_2(EXP, __VA_ARGS__))(O_RWL_TRACE_3(EXP, __VA_ARGS__))

P99_DECLARE_ONCE_CHAIN(orwl_thread);
P99_DECLARE_ONCE_CHAIN(mtx_t);
P99_DECLARE_ONCE_CHAIN(cnd_t);

P99_DECLARE_STRUCT(orwl_thread_cntrl);

/**
 ** @brief A structure that holds temporary state for detached threads.
 **
 ** This implements an execution model where a newly launched thread
 ** will still interact with its launcher through some shared data D.
 ** - When launched the callee has write access to D. It can use D to
 **   communicate some information to the caller. Once it has finished
 **   with that write access, it calls ::orwl_thread_cntrl_freeze to
 **   tell the caller. After that D must only be considered readable
 **   and not writable anymore.
 ** - Before the caller can read the modified D it has to call
 **   ::orwl_thread_cntrl_wait_for_callee which blocks until the
 **   caller has given up the write access. Once it has read the
 **   information from D that it needs it calls ::orwl_thread_cntrl_detach
 **   to tell the caller that D can be safely destroyed.
 ** - Before the callee destroys D and exits it has to call
 **   ::orwl_thread_cntrl_wait_for_caller to be sure that D is not
 **   needed anymore.
 **
 ** All these functions should mostly act asynchronously such that the
 ** overhead of this procedure is minimized. Therefore the times
 ** - of the callee between startup and freeze
 ** - of the caller between wait and detach
 ** should be held as short as possible.
 **/
struct orwl_thread_cntrl {
  p99_notifier notCaller;
  p99_notifier notCallee;
};

/**
 ** @related orwl_thread_cntrl
 **/
inline
orwl_thread_cntrl* orwl_thread_cntrl_init(orwl_thread_cntrl* det) {
  if (det) {
    p99_notifier_init(&det->notCaller);
    p99_notifier_init(&det->notCallee);
  }
  return det;
}

/**
 ** @related orwl_thread_cntrl
 **/
inline
void orwl_thread_cntrl_destroy(orwl_thread_cntrl* det) {
  /* empty */
}

/**
 ** @related orwl_thread_cntrl
 **/
inline
void orwl_thread_cntrl_freeze(orwl_thread_cntrl* det) {
  p99_notifier_set(&det->notCallee);
}

/**
 ** @related orwl_thread_cntrl
 **/
inline
void orwl_thread_cntrl_detach(orwl_thread_cntrl* det) {
  p99_notifier_set(&det->notCaller);
}

/**
 ** @related orwl_thread_cntrl
 **/
inline
void orwl_thread_cntrl_wait_for_caller(orwl_thread_cntrl* det) {
  p99_notifier_block(&det->notCaller);
}

/**
 ** @related orwl_thread_cntrl
 **/
inline
void orwl_thread_cntrl_wait_for_callee(orwl_thread_cntrl* det) {
  p99_notifier_block(&det->notCallee);
}

P99_DECLARE_DELETE(orwl_thread_cntrl);

/**
 ** @brief Interface to thrd_create() for joinable threads.
 **/
extern int orwl_thrd_create_joinable(thrd_t *restrict thread,
                                     thrd_start_t start_routine,
                                     void *restrict arg);

/**
 ** @brief Interface to thrd_create() for detached threads.
 **
 ** Launch a detached thread that runs @a start_routine with parameter
 ** @a arg. @a arg completely passes in the responsibility of the
 ** called thread. It should have been allocated by ::P99_NEW and
 ** @a start_routine should use the correct @c typename_delete
 ** function, where @c typename is the real type of @a arg.
 **
 ** The third parameter @a det allows for a finer control of the
 ** execution, see ::orwl_thread_cntrl. If it is @c 0, the default,
 ** the effect is that the thread is simply detached and no other
 ** interaction between caller and callee takes place.
 **/
P99_WARN_UNUSED_RESULT
extern int orwl_thrd_launch(thrd_start_t start_routine,
                            void *restrict arg,
                            orwl_thread_cntrl* det,
                            size_t location);

#define orwl_thrd_launch(...) P99_CALL_DEFARG(orwl_thrd_launch, 4, __VA_ARGS__)
#define orwl_thrd_launch_defarg_2() P99_0(orwl_thread_cntrl*)
#define orwl_thrd_launch_defarg_3() SIZE_MAX



/**
 ** @brief Interface to thrd_create() for detached threads.
 **/
P99_WARN_UNUSED_RESULT
inline
int orwl_thrd_create_detached(thrd_start_t start_routine,
                              void *restrict arg) {
  return orwl_thrd_launch(start_routine, arg, 0);
}

/**
 ** @brief Wait for all threads that have been created detached.
 **/
extern void orwl_thrd_wait_detached(void);

/**
 ** @brief Terminate all threads that are inactive
 **
 ** Don't use that while there are a lot of threads created.
 **/
extern void orwl_thrd_pool_purge(void);

/**
 ** @brief Return statistics about the number of threads.
 **/
extern void orwl_thrd_count(size_t ret[3]);

#ifdef DOXYGEN
/**
 ** @brief Define the function that is to be executed by a thread
 ** creation for type @a T.
 **
 ** For more details see also ::DECLARE_THREAD. With the example from there we
 ** should use this in the corresponding .c file as
 ** @code
 ** DEFINE_THREAD(worker) {
 **  Arg->ret = 43 * Arg->val;
 ** }
 ** @endcode
 **
 ** Anywhere in your code you could spawn a thread and then join it
 ** with something like this
 ** @code
 ** worker work = { .val = 37 };
 ** thrd_t id;
 ** worker_create_joinable(&work, &id);
 ** ... do something in parallel ...
 ** worker *reply = worker_join(id);
 ** ... do something with reply->ret ...
 ** @endcode
 **
 ** Beware that in this simple example the variable @c work lives on
 ** the stack of the calling function. So this function must
 ** imperatively call @c worker_join before it exits. If you can't be
 ** sure of that, use calls to @c worker_new and @c worker_delete
 ** (declared with DECLARE_NEW_DELETE(worker)) to allocate and free
 ** the memory.
 ** @code
 ** worker *work = worker_new();
 ** worker->val = 37;
 ** thrd_t id;
 ** worker_create_joinable(&work, &id);
 ** ... do something in parallel ...
 ** ... maybe return from the calling function ...
 ** ... but keep the `id' handy ...
 ** worker *reply = worker_join(id);
 ** ... do something with reply->ret ...
 ** ... don't forget to free the memory ...
 ** worker_delete(reply);
 ** @endcode
 **
 ** Another case occurs when you want the thread to be created in a
 ** detached state. In such a case you don't have
 ** much control on when the thread is finished so you @b must
 ** use calls to @c worker_new to allocate the memory.
 ** After your thread has terminated, @c worker_delete will
 ** automatically be called to free the space that had been allocated.
 ** @code
 ** worker *work = worker_new();
 ** worker->val = 37;
 ** worker_create_detached(work);
 ** ... do something in parallel ...
 ** ... maybe return from the calling function ...
 ** ... at the very end of your main, wait for all detached threads ...
 ** orwl_thrd_wait_detached();
 ** @endcode
 **
 ** The second argument @a STATIC may contain a storage class
 ** specifier. The only reasonable choice besides leaving empty (or
 ** out) is probably @c static, in which case all functions that will
 ** be defined through this will be static. It defaults to an empty
 ** token.
 **/
#define DEFINE_THREAD(T, STATIC)

/**
 ** @brief Some simple thread launching mechanism.
 **
 ** This declares two functions to create and join a thread that
 ** receives an argument of type @a T.
 ** @code
 ** typedef worker {
 **    int val;
 **    int ret;
 ** } worker;
 ** DECLARE_THREAD(worker);
 ** @endcode
 **
 ** This declares functions that are accessible by the program and
 ** can be seen as declared as
 ** @code
 ** int worker_create(worker* arg, thrd_t *id);
 ** int worker_create_joinable(worker* arg, thrd_t *id);
 ** worker *worker_join(thrd_t id);
 ** int worker_create_detached(worker* arg);
 ** void worker_start(worker*const arg);
 ** @endcode
 ** Here the semantics are similar to thrd_create()  and
 ** thrd_join() with the following differences:
 **
 ** - The argument that the thread receives and returns is of type @a T*
 ** - The return value of @c worker_join will normally be the same as
 **   the input value @c arg for the corresponding call to @c worker_create.
 ** - The worker thread callback function itself is @c worker_start
 **    and must be created with the macro ::DEFINE_THREAD.
 ** - @c worker_create may be called with @c id set to a null pointer,
 **    this then is equivalent to calling @c worker_create_detached. The
 **    thread is then created detached, as obviously you don't know
 **    any thread id to join to. All threads that are created this way
 **    should be awaited for at the end of the main program. Use
 **    ::orwl_thrd_wait_detached() for that purpose.
 **
 ** The second argument @a STATIC may contain a storage class
 ** specifier. The only reasonable choice besides leaving empty (or
 ** out) is probably @c static, in which case all functions that will
 ** be declared through this will be static. It defaults to an empty
 ** token.
 **/
#define DECLARE_THREAD(T, STATIC)                                                                         \
/*! @note This is the callback for each thread of type T */                                               \
/*! @see DEFINE_THREAD for details on how to launch threads */                                            \
/*! @related T */                                                                                         \
/*! @param Arg the T object for this call */                                                              \
STATIC void P99_PASTID2(T, start)(T *const Arg);                                                          \
/*! @brief Launch a thread for type T */                                                                  \
/*! @related T */                                                                                         \
/*! @param arg the T object for this call that will be passed to \ref T ## _start */                      \
/*! @param id if not a null pointer, the thread can be joined on this @a id */                            \
STATIC int P99_PASTID2(T, create)(T* arg, thrd_t *id);                                                    \
/*! @brief Launch a joinable thread for type T */                                                         \
/*! @related T */                                                                                         \
/*! @param arg the T object for this call that will be passed to \ref T ## _start */                      \
/*! @param id must not be a null pointer, the thread can be joined on this @a id */                       \
/*! @warning The arguments @a arg and @a id remain under the responsability of the caller. */             \
STATIC int P99_PASTID2(T, create_joinable)(T* arg, thrd_t *id);                                           \
/*! @brief Join a callback procedure of type T that has been launched with \ref T ## _create_joinable. */ \
/*! @related T */                                                                                         \
/*! @param id the thread that is to be joined */                                                          \
/*! @return the original argument for which this thread was created */                                    \
STATIC T* P99_PASTID2(T, join)(thrd_t id);                                                                \
/*! @brief Launch a detached thread for type T */                                                         \
/*! @related T */                                                                                         \
/*! @param arg the T object for this call that will be passed to \ref T ## _start */                      \
/*! @warning The argument @a arg should not be modified by the caller, hereafter. */                      \
STATIC int P99_PASTID2(T, create_detached)(T* arg);                                                       \
/*! @brief Launch a thread for type T and wait until it detaches */                                       \
/*! @related T */                                                                                         \
/*! @param arg the T object for this call that will be passed to \ref T ## _start */                      \
/*! @param det is used by callee and caller to regulate the start up phase \ref T ## _start */            \
/*! @warning The argument @a arg should not be modified by the caller. */                                 \
/*! @warning It may be modified by the callee until he calls ::orwl_thread_cntrl_freeze. */               \
/*! @see orwl_thread_cntrl */                                                                             \
STATIC inline int P99_PASTID2(T, launch)(T* arg, orwl_thread_cntrl* det)

#define ORWL_DECLARE_TASK(T)

/**
 ** @brief Define the function that will be executed for ORWL task
 ** type @a T.
 **/
#define ORWL_DEFINE_TASK(T)

#else
#define O_RWL_DECLARE_THREAD(T, STATIC)                                        \
STATIC void P99_PASTID2(T, start)(T* Arg);                                     \
P99_WARN_UNUSED_RESULT                                                         \
STATIC inline int P99_PASTID2(T, join)(thrd_t id) {                            \
  int ret = thrd_join(id, &ret);                                               \
  return ret;                                                                  \
}                                                                              \
STATIC inline int P99_PASTID2(T, start_joinable)(void* arg) {                  \
  T *Arg = (T*)arg;                                                            \
  P99_PASTID2(T, start)(Arg);                                                  \
  return 0;                                                                    \
}                                                                              \
STATIC inline int P99_PASTID2(T, start_detached)(void* arg) {                  \
  T *Arg = (T*)arg;                                                            \
  P99_PASTID2(T, start)(Arg);                                                  \
  P99_PASTID2(T, delete)(Arg);                                                 \
  return 0;                                                                    \
}                                                                              \
STATIC inline int P99_PASTID2(T, user_start_detached)(void* arg) {             \
  orwl_deep_instr_thread_not_internal();                                       \
  return P99_PASTID2(T, start_detached)(arg);                                  \
}                                                                              \
P99_WARN_UNUSED_RESULT                                                         \
STATIC inline int P99_PASTID2(T, create_joinable)(T* arg, thrd_t *id) {        \
  return orwl_thrd_create_joinable(id, P99_PASTID2(T, start_joinable), arg);   \
}                                                                              \
P99_WARN_UNUSED_RESULT                                                         \
STATIC inline int P99_PASTID2(T, create_detached)(T* arg) {                    \
  return orwl_thrd_create_detached(P99_PASTID2(T, start_detached), arg);       \
}                                                                              \
P99_WARN_UNUSED_RESULT                                                         \
STATIC inline int P99_PASTID2(T, operation)(T* arg, size_t locid) {            \
  return orwl_thrd_launch(P99_PASTID2(T, start_detached), arg, 0, locid);      \
}                                                                              \
P99_WARN_UNUSED_RESULT                                                         \
STATIC inline int P99_PASTID2(T, launch)(T* arg, orwl_thread_cntrl* det) {     \
  return orwl_thrd_launch(P99_PASTID2(T, start_detached), arg, det);           \
}                                                                              \
P99_WARN_UNUSED_RESULT                                                         \
STATIC inline int P99_PASTID2(T, create)(T* arg, thrd_t *id) {                 \
  if (id)                                                                      \
    return orwl_thrd_create_joinable(id, P99_PASTID2(T, start_joinable), arg); \
  else                                                                         \
    return orwl_thrd_create_detached(P99_PASTID2(T, start_detached), arg);     \
}                                                                              \
P99_MACRO_END(declare_thread)

#define DECLARE_THREAD(...) P99_IF_LT(P99_NARG(__VA_ARGS__), 2)(O_RWL_DECLARE_THREAD(__VA_ARGS__,))(O_RWL_DECLARE_THREAD(__VA_ARGS__))

// XXX: All threads created by the library to run user functions should call
// orwl_deep_instr_thread_not_internal() first, so we don't mix the stats with
// library internal threads and user threads. As all user code is run using this
// interface, all user code run ends up calling
// orwl_deep_instr_thread_not_internal(). But if you might change this, you
// should really take this into account
#define O_RWL_DECLARE_TASK(T, STATIC)                                                       \
DECLARE_THREAD(T, STATIC);                                                                  \
STATIC inline int P99_PASTID2(T, create_task)(T* arg, size_t tid) {                         \
  return orwl_thrd_launch(P99_PASTID2(T, user_start_detached), arg, 0, ORWL_LOCATION(tid)); \
}                                                                                           \
P99_MACRO_END(orwl_declare_task)


#define ORWL_DECLARE_TASK(...) P99_IF_LT(P99_NARG(__VA_ARGS__), 2)(O_RWL_DECLARE_TASK(__VA_ARGS__,))(O_RWL_DECLARE_TASK(__VA_ARGS__))

#define O_RWL_DEFINE_THREAD_1(T)                                      \
P99_INSTANTIATE(int, P99_PASTID2(T, join), thrd_t);                   \
P99_INSTANTIATE(int, P99_PASTID2(T, create), T*, thrd_t*);            \
P99_INSTANTIATE(int, P99_PASTID2(T, create_joinable), T*, thrd_t*);   \
P99_INSTANTIATE(int, P99_PASTID2(T, create_detached), T*);            \
P99_INSTANTIATE(int, P99_PASTID2(T, operation), T*, size_t);          \
P99_INSTANTIATE(int, P99_PASTID2(T, launch), T*, orwl_thread_cntrl*); \
P99_INSTANTIATE(int, P99_PASTID2(T, start_joinable), void*);          \
P99_INSTANTIATE(int, P99_PASTID2(T, start_detached), void*);          \
P99_INSTANTIATE(int, P99_PASTID2(T, user_start_detached), void*);     \
void P99_PASTID2(T, start)(T *const Arg)

#define O_RWL_DEFINE_THREAD_2(T, STATIC)                       \
STATIC void P99_PASTID2(T, start)(T *const Arg)

#define DEFINE_THREAD(...) P99_IF_LT(P99_NARG(__VA_ARGS__), 2)(O_RWL_DEFINE_THREAD_1(__VA_ARGS__))(O_RWL_DEFINE_THREAD_2(__VA_ARGS__))

#define ORWL_DEFINE_TASK(T)                                    \
P99_INSTANTIATE(int, P99_PASTID2(T, create_task), T*, size_t); \
DEFINE_THREAD(T)

#endif


/**
 ** @brief Initialize the local variables of a thread of type @a NAME
 **
 ** For this to work the argument list must contain names of the
 ** fields that had been declared for @a NAME. The @c struct @a NAME
 ** must have been declared with ::P99_DEFINE_STRUCT.
 ** @see P99_DECLARE_STRUCT
 ** @see P99_DEFINE_STRUCT
 ** @see DECLARE_THREAD
 ** @see DEFINE_THREAD
 **/
#define ORWL_THREAD_USE(NAME, ...) P99_STRUCT_USE(NAME, Arg, __VA_ARGS__)

DECLARE_NEW_DELETE(thrd_t);

/**
 ** @}
 **/


#endif      /* !ORWL_THREAD_H_ */
