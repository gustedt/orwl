/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the author and copyright holder for this work is                           */
/* all rights reserved,  2016 Jens Gustedt, INRIA, France                     */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#ifndef     ORWL_ALLOC_QUEUE_H_
#define     ORWL_ALLOC_QUEUE_H_


/**
 ** @brief Establish a buffering queue of maximum length @a max
 ** between read location @a readloc and write location @a writeloc
 **
 ** The idea of this tool is to release constraints on a data
 ** dependency between tasks by creating two threads, one reader and
 ** one writer. @a readloc is read iteratively, its data is copied and
 ** stored in a list of available data. Asynchronously, the writer
 ** takes the data is from the head of that list as it arrives and
 ** iteratively feeds into @a writeloc.
 **
 ** Both threads work asynchronously and are block as long as they
 ** can't operate. That is:
 **
 ** The reader blocks
 **
 ** - if there is no data available in @a readloc
 ** - the queue contains @a max elements
 **
 ** The writer blocks
 **
 ** - if there is no data in the queue
 ** - @a writeloc cannot be acquired.
 **
 ** As their names indicate, internally @a readloc will be accessed by
 ** a handle for reading, @a writeloc by another one for
 ** writing. Initially, these are inserted last (respectively first)
 ** into the fifo of the location.
 **
 ** Parameter @a max can be omitted, and defaults to the arbitrary
 ** value @c 10.
 **/
void orwl_alloc_queue(size_t readloc, size_t writeloc, size_t max);


#ifndef DOXYGEN
#define orwl_alloc_queue(...) P99_CALL_DEFARG(orwl_alloc_queue, 3, __VA_ARGS__)
#define orwl_alloc_queue_defarg_2() 10u
#endif


#endif
