/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the authors and copyright holders for this work are as follows:            */
/* all rights reserved,  2011 Emmanuel Jeanvoine, INRIA, France               */
/* all rights reserved,  2010-2013 Jens Gustedt, INRIA, France                */
/* all rights reserved,  2013 Rodrigo Campos, INRIA, France                   */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#ifndef     ORWL_H_
# define    ORWL_H_

#include "orwl_keys.h"
#include "orwl_server.h"
#include "orwl_socket.h"
#include "orwl_broadcast.h"
#include "orwl_circulate.h"
#include "orwl_gather.h"
#include "orwl_reduce.h"
#include "p99_c99_default.h"
#include "p99_str.h"

/**
 ** @file
 ** @brief Use this to include all of ORWL.
 **/

/**
 ** @def void
 **
 ** here it is
 **/
#define void void

/**
 ** @mainpage ORWL - Ordered Read-Write Locks
 **
 ** @section credits Credits and Rights
 ** @subsection author Principal Author and Maintainer
 ** @author <a href="http://icube-icps.unistra.fr/index.php/Jens_Gustedt">Jens Gustedt</a>
 ** @date 2010 - 2017
 **
 ** For other contributors please see the file scripts/AUTHORS.txt in the distribution.
 **
 ** @subsection version Version
 **
 ** The version that this documentation describes can be identified
 ** via the macros ::ORWL_VERSION_DATE, namely @c $Format:%cd$. It also is
 ** tagged with an hexadecimal ID tag that is given in
 ** ::ORWL_VERSION_ID, namely @c $Format:%H$.
 **
 ** @subsection overview Overview
 **
 ** ORWL is a reference implementation of Ordered Read-Write Locks, a
 ** programming model and API for parallel and distributed
 ** computing. ORWL is at the same time simple to use and efficient
 ** and provides deadlock-freeness and equity for iterative programs.
 **
 ** Its particularity is to give a simple abstraction of resources
 ** that are shared between different tasks of an application and to
 ** which access is regulated via a FIFO, one per resource.
 **
 ** @subsection toc Contents
 ** - \subpage installation
 ** - \subpage configuration
 ** - \subpage ORWLprogramming
 ** - \subpage library
 **
 ** @subsection copyright Copyright
 ** Copyright &copy; 2010-2013 Jens Gustedt, INRIA, France, http://www.inria.fr/
 **
 ** @htmlinclude SHORTLICENCE-open.txt
 **
 ** @subsection license License
 **
 **/

/**
 ** @page installation Installation
 **
 ** @section sources Sources
 **
 ** The source code will always be publicly available. You will either obtain
 ** a tarball or check out the directory tree by using @c git. To use git
 ** @code
 ** cd myGitRepositories
 ** git clone PATH/TO/THE/REMOTE/GIT/REPOSITORY orwl
 ** cd orwl
 ** @endcode
 **
 ** Once you are in there you need another piece of software that is
 ** distributed separately, <a
 ** href="p99.gforge.inria.fr">P99</a>. This is handled as a @c
 ** submodule
 **
 ** @code
 ** git submodule init
 ** git submodule update
 ** @endcode
 **
 ** This should checkout P99 into the correct subdirectory, namely @c
 ** p99-source, from the globally accessible git repository.
 **
 ** If you get ORWL from a tarball you also have to get yourself a
 ** tarball of the recent P99 version and untar this at the above
 ** mentioned place.
 **
 ** @section configuration Configuration
 **
 ** This library currently uses
 **
 ** - GNU <code>make</code>
 ** - <code>getconf</code>
 ** - <code>tr</code>
 ** - <code>sed</code>
 ** - and your C compiler
 **
 ** to determine most of its configuration automatically at compile
 ** time. Relatively recent POSIX systems should not have any
 ** particular problem with that.
 **
 ** Your C compiler should be complying to the C99 standard. ORWL uses
 ** P99 under the hood, so you will probably not get away with a
 ** compiler that only has C89 with no extension gearing towards
 ** C99. It can be fine-tuned by the following @c make parameters
 ** - CC the name of the compiler
 ** - OPT additional options that you want to pass to the compilation phase
 ** example
 ** @code
 ** make CC=clang OPT='-march=native'
 ** @endcode
 **
 ** For details on specific fine tuning see @ref configuration_parameters.
 **/

#ifdef DOXYGEN

#define O_RWL_DOCUMENT_CONFIG(NAME)                                                                                           \
/*! @note Generally configuration options for ORWL are determined at compile time, see @ref configuration "configuration". */

/**
 ** @addtogroup configuration_parameters Configuration parameters
 ** @{
 **/

/**
 ** @addtogroup linux_configuration Linux specific configuration
 ** @{
 **/

/**
 ** Define this macro to compile the library without futex
 ** support. This is probably only useful for testing code coverage.
 ** @remark Futex is a concept that depends on support via the Linux kernel.
 **/
O_RWL_DOCUMENT_CONFIG(NO_FUTEX)
#define NO_FUTEX

/**
 ** @}
 **/

/**
 ** @addtogroup time_configuration Configuration parameters that concern measurement etc of time.
 ** @{
 **/

/**
 ** @brief Use fine grained timings to instrument ORWL
 **
 ** If this macro is defined the ORWL library and eventually
 ** application code are instrumented to obtain fine grained timings.
 **
 ** @see ORWL_TIMING
 ** @see ORWL_TIMER
 **/
O_RWL_DOCUMENT_CONFIG(ORWL_USE_TIMING)
#define ORWL_USE_TIMING

/**
 ** Define this macro to compile the library without @c nanosleep. @c
 ** usleep will be used instead. This is probably only useful for
 ** testing code coverage.
 ** @see NO_USLEEP
 **/
O_RWL_DOCUMENT_CONFIG(NO_NANOSLEEP)
#define NO_NANOSLEEP


/**
 ** Define this macro to compile the library without @c usleep. If @c
 ** nanosleep isn't available, too, a hack using @c select will be
 ** used instead. This is probably only useful for testing code
 ** coverage.  @see NO_NANOSLEEP
 **/
O_RWL_DOCUMENT_CONFIG(NO_USLEEP)
#define NO_USLEEP

/**
 ** @}
 **/


/**
 ** @addtogroup posix_configuration POSIX configuration parameters
 ** @brief ORWL uses a lot of different POSIX features
 **
 ** Some of these features are optional and can be replaced when this
 ** is detected at compile time.
 ** @{
 **/

/**
 ** This option will be set automatically if support for the POSIX
 ** barrier extension has been detected.
 **
 ** @remark Not used by ORWL itself
 **/
O_RWL_DOCUMENT_CONFIG(POSIX_BARRIERS)
#define POSIX_BARRIERS


/**
 ** This option will be set automatically if support for the POSIX
 ** IP version 6 extension has been detected.
 **/
O_RWL_DOCUMENT_CONFIG(POSIX_IPV6)
#define POSIX_IPV6


/**
 ** This option will be set automatically if support for the POSIX
 ** semaphores extension has been detected.
 **
 ** @remark Not used by ORWL itself
 **/
O_RWL_DOCUMENT_CONFIG(POSIX_SEMAPHORES)
#define POSIX_SEMAPHORES


/**
 ** This option will be set automatically to a positive value if
 ** support for the POSIX thread process-shared synchronization
 ** option has been detected.
 **
 ** @remark Not used by ORWL itself
 **/
O_RWL_DOCUMENT_CONFIG(POSIX_THREAD_PROCESS_SHARED)
#define POSIX_THREAD_PROCESS_SHARED

/**
 ** This option will be set automatically to a positive value if
 ** support for the POSIX thread safe function option has been
 ** detected.
 **/
O_RWL_DOCUMENT_CONFIG(POSIX_THREAD_SAFE_FUNCTIONS)
#define POSIX_THREAD_SAFE_FUNCTIONS

/**
 ** This option will be set automatically if support for the POSIX
 ** timeouts extension has been detected.
 **
 ** This option is passed to P99 such that it may (or may not) add the
 ** timeout interfaces to the mutex and condition interface.
 **/
O_RWL_DOCUMENT_CONFIG(POSIX_TIMEOUTS)
#define POSIX_TIMEOUTS

/**
 ** @}
 **/

/**
 ** @}
 **/

/**
 ** @addtogroup models Models for parallel and distributed computing
 ** @{
 **/

/**
 ** @addtogroup process_model T/P: the one-task-per-process model of execution
 ** @{
 **/

/**
 ** @}
 **/


/**
 ** @addtogroup thread_model T/T: the one-task-per-thread model of execution
 **
 ** @{
 **/

/**
 ** @}
 **/

/**
 ** @addtogroup operation_model O/T: the one-operation-per-thread model of execution
 **
 ** @{
 **/

/**
 ** @}
 **/

/**
 ** @}
 **/


/**
 ** @addtogroup library The ORWL Library
 ** @{
 **/

/**
 ** @addtogroup library_critical User interfaces for access to critical resources
 ** @{
 **/

/**
 ** @}
 **/

/**
 ** @addtogroup library_iterative User interfaces for iterative access
 ** @{
 **/

/**
 ** @}
 **/

/**
 ** @addtogroup library_generic Type generic interfaces for single or iterative access
 ** @{
 **/

/**
 ** @}
 **/

/**
 ** @addtogroup library_init Initialization of local and remote locations
 ** @{
 **/

/**
 ** @}
 **/

/**
 ** @addtogroup library_support Compiler and system support features
 ** @{
 **/

/**
 ** @}
 **/

/**
 ** @addtogroup library_low Low level implementation
 ** @{
 **/

/**
 ** @}
 **/


/**
 ** @addtogroup environment Read values from the environment
 **
 ** Environment variables should be declared with the help of macros
 ** ::ORWL_DECLARE_ENV and ::ORWL_DEFINE_ENV. They can then be
 ** accessed as simple functions and are listed on a
 ** @link env.html
 ** special page.
 ** @endlink
 **
 ** @{
 **/

/**
 ** @}
 **/


/**
 ** @}
 **/




#endif /* configuration options */

#endif      /* !ORWL_H_ */
