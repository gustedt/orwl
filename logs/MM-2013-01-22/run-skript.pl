#!/bin/sh -f
#  This may look like complete nonsense, but it really is -*- mode: perl; coding: utf-8 -*-
eval 'exec perl -wS -x $0 ${1+"$@"}'
if 0;               #### for this magic, see findSvnAuthors ####
#!perl

use English;

# Run the matrix multiplication test code for different parameters

# The problem size is N*K*M where A is an N*K matrix and B is a K*M
# matrix. We suppose that all problem sizes are always a power of 2.
# The algorithm work by dividing the matrices into nt rows and
# columns, respectively.
#
# Set N = nt * n
#     K = nt * k
#     M = nt * m
#
# N*K*M = nt^3 * n * k * m
#       = nt^3 * n0^3
#       = (nt * n0)^3
#       = (2^{lt} * 2^{l0})^3
#       = 2^{3*(lt + lo0)}

for (my $expo = 5; $expo < 13; ++$expo) {
    my $size = (1 << (3*$expo));
    print STDERR "starting size $size\n";
  LOG:
    for (my $lt = 0; ($lt < $expo) && ($lt < 11); ++$lt) {
        my $nt = (1 << $lt);
        my $l0 = $expo - $lt;
        my $n0 = (1 << $l0);
        last LOG if ($n0 < 32);
        for (my $lp = 0; $lp <= $lt; ++$lp) {
            my $np = (1 << $lp);
            print STDERR "starting $nt tasks ($np tasks per process) with $n0 block size\n";
            system("time ./run_tutorials_local.pl ./orwl_tutorial_threads_matmult $nt $np -n $n0 -k $n0 -m $n0 -i 10");
        }
    }
}
