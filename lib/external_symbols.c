/** @file external_symbols.c
 ** @brief External refferences in the library
 **/
#include "orwl_posix.h"
#include "orwl_affinity.h"
#include "p99_threads.h"
typedef void* (*o_rwl_FT)(void*);
void* (*const external_functions[])(void*) = {
	(o_rwl_FT)abort,
 	(o_rwl_FT)accept,
 	(o_rwl_FT)aio_error,
 	(o_rwl_FT)aio_suspend,
 	(o_rwl_FT)bind,
 	(o_rwl_FT)calloc,
 	(o_rwl_FT)close,
 	(o_rwl_FT)connect,
 	(o_rwl_FT)exit,
 	(o_rwl_FT)exp,
 	(o_rwl_FT)fclose,
 	(o_rwl_FT)fcntl,
 	(o_rwl_FT)feof,
 	(o_rwl_FT)fflush,
 	(o_rwl_FT)fgets,
 	(o_rwl_FT)flockfile,
 	(o_rwl_FT)fopen,
 	(o_rwl_FT)fprintf,
 	(o_rwl_FT)fputc,
 	(o_rwl_FT)fputs,
 	(o_rwl_FT)free,
 	(o_rwl_FT)freeaddrinfo,
 	(o_rwl_FT)freopen,
 	(o_rwl_FT)fseek,
 	(o_rwl_FT)ftruncate,
 	(o_rwl_FT)funlockfile,
 	(o_rwl_FT)fwrite,
 	(o_rwl_FT)getaddrinfo,
 	(o_rwl_FT)getenv,
 	(o_rwl_FT)gethostname,
 	(o_rwl_FT)getpeername,
 	(o_rwl_FT)getsockname,
 	(o_rwl_FT)getsockopt,
 	(o_rwl_FT)hwloc_bitmap_snprintf,
 	(o_rwl_FT)hwloc_get_nbobjs_by_depth,
 	(o_rwl_FT)hwloc_get_obj_by_depth,
 	(o_rwl_FT)hwloc_get_type_depth,
 	(o_rwl_FT)hwloc_set_cpubind,
 	(o_rwl_FT)hwloc_topology_destroy,
 	(o_rwl_FT)hwloc_topology_get_depth,
 	(o_rwl_FT)hwloc_topology_init,
 	(o_rwl_FT)hwloc_topology_load,
 	(o_rwl_FT)hwloc_topology_set_all_types_filter,
 	(o_rwl_FT)inet_ntop,
 	(o_rwl_FT)jrand48,
 	(o_rwl_FT)ldexp,
 	(o_rwl_FT)lio_listio,
 	(o_rwl_FT)listen,
 	(o_rwl_FT)log,
 	(o_rwl_FT)log10,
 	(o_rwl_FT)log2,
 	(o_rwl_FT)longjmp,
 	(o_rwl_FT)malloc,
 	(o_rwl_FT)memcmp,
 	(o_rwl_FT)memcpy,
 	(o_rwl_FT)memmove,
 	(o_rwl_FT)memset,
 	(o_rwl_FT)mmap,
 	(o_rwl_FT)mremap,
 	(o_rwl_FT)munmap,
 	(o_rwl_FT)nanosleep,
 	(o_rwl_FT)open,
 	(o_rwl_FT)perror,
 	(o_rwl_FT)pow,
 	(o_rwl_FT)printf,
 	(o_rwl_FT)pthread_cond_broadcast,
 	(o_rwl_FT)pthread_cond_init,
 	(o_rwl_FT)pthread_cond_wait,
 	(o_rwl_FT)pthread_create,
 	(o_rwl_FT)pthread_detach,
 	(o_rwl_FT)pthread_exit,
 	(o_rwl_FT)pthread_getspecific,
 	(o_rwl_FT)pthread_join,
 	(o_rwl_FT)pthread_key_create,
 	(o_rwl_FT)pthread_key_delete,
 	(o_rwl_FT)pthread_mutex_destroy,
 	(o_rwl_FT)pthread_mutex_init,
 	(o_rwl_FT)pthread_mutex_lock,
 	(o_rwl_FT)pthread_mutex_unlock,
 	(o_rwl_FT)pthread_mutexattr_init,
 	(o_rwl_FT)pthread_mutexattr_settype,
 	(o_rwl_FT)pthread_self,
 	(o_rwl_FT)pthread_setspecific,
 	(o_rwl_FT)puts,
 	(o_rwl_FT)read,
 	(o_rwl_FT)realloc,
 	(o_rwl_FT)recv,
 	(o_rwl_FT)regcomp,
 	(o_rwl_FT)regexec,
 	(o_rwl_FT)regfree,
 	(o_rwl_FT)rename,
 	(o_rwl_FT)round,
 	(o_rwl_FT)sched_yield,
 	(o_rwl_FT)select,
 	(o_rwl_FT)send,
 	(o_rwl_FT)setsockopt,
 	(o_rwl_FT)setvbuf,
 	(o_rwl_FT)shm_open,
 	(o_rwl_FT)shm_unlink,
 	(o_rwl_FT)shutdown,
 	(o_rwl_FT)snprintf,
 	(o_rwl_FT)socket,
 	(o_rwl_FT)sprintf,
 	(o_rwl_FT)sqrt,
 	(o_rwl_FT)stpcpy,
 	(o_rwl_FT)strchr,
 	(o_rwl_FT)strcmp,
 	(o_rwl_FT)strcpy,
 	(o_rwl_FT)strcspn,
 	(o_rwl_FT)strdup,
 	(o_rwl_FT)strerror,
 	(o_rwl_FT)strerror_r,
 	(o_rwl_FT)strlen,
 	(o_rwl_FT)strncmp,
 	(o_rwl_FT)strncpy,
 	(o_rwl_FT)strtol,
 	(o_rwl_FT)strtoull,
 	(o_rwl_FT)syscall,
 	(o_rwl_FT)sysconf,
 	(o_rwl_FT)timespec_get,
 	(o_rwl_FT)tm_build_affinity_mat,
 	(o_rwl_FT)tm_build_tree_from_topology,
 	(o_rwl_FT)tm_compute_mapping,
 	(o_rwl_FT)tm_display_topology,
 	(o_rwl_FT)tm_enable_oversubscribing,
 	(o_rwl_FT)tm_free_affinity_mat,
 	(o_rwl_FT)tm_free_solution,
 	(o_rwl_FT)tm_free_topology,
 	(o_rwl_FT)tm_free_tree,
 	(o_rwl_FT)tm_get_local_topology_with_hwloc,
 	(o_rwl_FT)tm_topology_set_binding_constraints,
 	(o_rwl_FT)unlink,
 	(o_rwl_FT)vfprintf,
};
#ifdef DOXYGEN
void call_externals(void) {
	((o_rwl_FT)abort)((void*)0);
 	((o_rwl_FT)accept)((void*)0);
 	((o_rwl_FT)aio_error)((void*)0);
 	((o_rwl_FT)aio_suspend)((void*)0);
 	((o_rwl_FT)bind)((void*)0);
 	((o_rwl_FT)calloc)((void*)0);
 	((o_rwl_FT)close)((void*)0);
 	((o_rwl_FT)connect)((void*)0);
 	((o_rwl_FT)exit)((void*)0);
 	((o_rwl_FT)exp)((void*)0);
 	((o_rwl_FT)fclose)((void*)0);
 	((o_rwl_FT)fcntl)((void*)0);
 	((o_rwl_FT)feof)((void*)0);
 	((o_rwl_FT)fflush)((void*)0);
 	((o_rwl_FT)fgets)((void*)0);
 	((o_rwl_FT)flockfile)((void*)0);
 	((o_rwl_FT)fopen)((void*)0);
 	((o_rwl_FT)fprintf)((void*)0);
 	((o_rwl_FT)fputc)((void*)0);
 	((o_rwl_FT)fputs)((void*)0);
 	((o_rwl_FT)free)((void*)0);
 	((o_rwl_FT)freeaddrinfo)((void*)0);
 	((o_rwl_FT)freopen)((void*)0);
 	((o_rwl_FT)fseek)((void*)0);
 	((o_rwl_FT)ftruncate)((void*)0);
 	((o_rwl_FT)funlockfile)((void*)0);
 	((o_rwl_FT)fwrite)((void*)0);
 	((o_rwl_FT)getaddrinfo)((void*)0);
 	((o_rwl_FT)getenv)((void*)0);
 	((o_rwl_FT)gethostname)((void*)0);
 	((o_rwl_FT)getpeername)((void*)0);
 	((o_rwl_FT)getsockname)((void*)0);
 	((o_rwl_FT)getsockopt)((void*)0);
 	((o_rwl_FT)hwloc_bitmap_snprintf)((void*)0);
 	((o_rwl_FT)hwloc_get_nbobjs_by_depth)((void*)0);
 	((o_rwl_FT)hwloc_get_obj_by_depth)((void*)0);
 	((o_rwl_FT)hwloc_get_type_depth)((void*)0);
 	((o_rwl_FT)hwloc_set_cpubind)((void*)0);
 	((o_rwl_FT)hwloc_topology_destroy)((void*)0);
 	((o_rwl_FT)hwloc_topology_get_depth)((void*)0);
 	((o_rwl_FT)hwloc_topology_init)((void*)0);
 	((o_rwl_FT)hwloc_topology_load)((void*)0);
 	((o_rwl_FT)hwloc_topology_set_all_types_filter)((void*)0);
 	((o_rwl_FT)inet_ntop)((void*)0);
 	((o_rwl_FT)jrand48)((void*)0);
 	((o_rwl_FT)ldexp)((void*)0);
 	((o_rwl_FT)lio_listio)((void*)0);
 	((o_rwl_FT)listen)((void*)0);
 	((o_rwl_FT)log)((void*)0);
 	((o_rwl_FT)log10)((void*)0);
 	((o_rwl_FT)log2)((void*)0);
 	((o_rwl_FT)longjmp)((void*)0);
 	((o_rwl_FT)malloc)((void*)0);
 	((o_rwl_FT)memcmp)((void*)0);
 	((o_rwl_FT)memcpy)((void*)0);
 	((o_rwl_FT)memmove)((void*)0);
 	((o_rwl_FT)memset)((void*)0);
 	((o_rwl_FT)mmap)((void*)0);
 	((o_rwl_FT)mremap)((void*)0);
 	((o_rwl_FT)munmap)((void*)0);
 	((o_rwl_FT)nanosleep)((void*)0);
 	((o_rwl_FT)open)((void*)0);
 	((o_rwl_FT)perror)((void*)0);
 	((o_rwl_FT)pow)((void*)0);
 	((o_rwl_FT)printf)((void*)0);
 	((o_rwl_FT)pthread_cond_broadcast)((void*)0);
 	((o_rwl_FT)pthread_cond_init)((void*)0);
 	((o_rwl_FT)pthread_cond_wait)((void*)0);
 	((o_rwl_FT)pthread_create)((void*)0);
 	((o_rwl_FT)pthread_detach)((void*)0);
 	((o_rwl_FT)pthread_exit)((void*)0);
 	((o_rwl_FT)pthread_getspecific)((void*)0);
 	((o_rwl_FT)pthread_join)((void*)0);
 	((o_rwl_FT)pthread_key_create)((void*)0);
 	((o_rwl_FT)pthread_key_delete)((void*)0);
 	((o_rwl_FT)pthread_mutex_destroy)((void*)0);
 	((o_rwl_FT)pthread_mutex_init)((void*)0);
 	((o_rwl_FT)pthread_mutex_lock)((void*)0);
 	((o_rwl_FT)pthread_mutex_unlock)((void*)0);
 	((o_rwl_FT)pthread_mutexattr_init)((void*)0);
 	((o_rwl_FT)pthread_mutexattr_settype)((void*)0);
 	((o_rwl_FT)pthread_self)((void*)0);
 	((o_rwl_FT)pthread_setspecific)((void*)0);
 	((o_rwl_FT)puts)((void*)0);
 	((o_rwl_FT)read)((void*)0);
 	((o_rwl_FT)realloc)((void*)0);
 	((o_rwl_FT)recv)((void*)0);
 	((o_rwl_FT)regcomp)((void*)0);
 	((o_rwl_FT)regexec)((void*)0);
 	((o_rwl_FT)regfree)((void*)0);
 	((o_rwl_FT)rename)((void*)0);
 	((o_rwl_FT)round)((void*)0);
 	((o_rwl_FT)sched_yield)((void*)0);
 	((o_rwl_FT)select)((void*)0);
 	((o_rwl_FT)send)((void*)0);
 	((o_rwl_FT)setsockopt)((void*)0);
 	((o_rwl_FT)setvbuf)((void*)0);
 	((o_rwl_FT)shm_open)((void*)0);
 	((o_rwl_FT)shm_unlink)((void*)0);
 	((o_rwl_FT)shutdown)((void*)0);
 	((o_rwl_FT)snprintf)((void*)0);
 	((o_rwl_FT)socket)((void*)0);
 	((o_rwl_FT)sprintf)((void*)0);
 	((o_rwl_FT)sqrt)((void*)0);
 	((o_rwl_FT)stpcpy)((void*)0);
 	((o_rwl_FT)strchr)((void*)0);
 	((o_rwl_FT)strcmp)((void*)0);
 	((o_rwl_FT)strcpy)((void*)0);
 	((o_rwl_FT)strcspn)((void*)0);
 	((o_rwl_FT)strdup)((void*)0);
 	((o_rwl_FT)strerror)((void*)0);
 	((o_rwl_FT)strerror_r)((void*)0);
 	((o_rwl_FT)strlen)((void*)0);
 	((o_rwl_FT)strncmp)((void*)0);
 	((o_rwl_FT)strncpy)((void*)0);
 	((o_rwl_FT)strtol)((void*)0);
 	((o_rwl_FT)strtoull)((void*)0);
 	((o_rwl_FT)syscall)((void*)0);
 	((o_rwl_FT)sysconf)((void*)0);
 	((o_rwl_FT)timespec_get)((void*)0);
 	((o_rwl_FT)tm_build_affinity_mat)((void*)0);
 	((o_rwl_FT)tm_build_tree_from_topology)((void*)0);
 	((o_rwl_FT)tm_compute_mapping)((void*)0);
 	((o_rwl_FT)tm_display_topology)((void*)0);
 	((o_rwl_FT)tm_enable_oversubscribing)((void*)0);
 	((o_rwl_FT)tm_free_affinity_mat)((void*)0);
 	((o_rwl_FT)tm_free_solution)((void*)0);
 	((o_rwl_FT)tm_free_topology)((void*)0);
 	((o_rwl_FT)tm_free_tree)((void*)0);
 	((o_rwl_FT)tm_get_local_topology_with_hwloc)((void*)0);
 	((o_rwl_FT)tm_topology_set_binding_constraints)((void*)0);
 	((o_rwl_FT)unlink)((void*)0);
 	((o_rwl_FT)vfprintf)((void*)0);
}
	void* abort(void*);
 	void* accept(void*);
 	void* aio_error(void*);
 	void* aio_suspend(void*);
 	void* bind(void*);
 	void* calloc(void*);
 	void* close(void*);
 	void* connect(void*);
 	void* exit(void*);
 	void* exp(void*);
 	void* fclose(void*);
 	void* fcntl(void*);
 	void* feof(void*);
 	void* fflush(void*);
 	void* fgets(void*);
 	void* flockfile(void*);
 	void* fopen(void*);
 	void* fprintf(void*);
 	void* fputc(void*);
 	void* fputs(void*);
 	void* free(void*);
 	void* freeaddrinfo(void*);
 	void* freopen(void*);
 	void* fseek(void*);
 	void* ftruncate(void*);
 	void* funlockfile(void*);
 	void* fwrite(void*);
 	void* getaddrinfo(void*);
 	void* getenv(void*);
 	void* gethostname(void*);
 	void* getpeername(void*);
 	void* getsockname(void*);
 	void* getsockopt(void*);
 	void* hwloc_bitmap_snprintf(void*);
 	void* hwloc_get_nbobjs_by_depth(void*);
 	void* hwloc_get_obj_by_depth(void*);
 	void* hwloc_get_type_depth(void*);
 	void* hwloc_set_cpubind(void*);
 	void* hwloc_topology_destroy(void*);
 	void* hwloc_topology_get_depth(void*);
 	void* hwloc_topology_init(void*);
 	void* hwloc_topology_load(void*);
 	void* hwloc_topology_set_all_types_filter(void*);
 	void* inet_ntop(void*);
 	void* jrand48(void*);
 	void* ldexp(void*);
 	void* lio_listio(void*);
 	void* listen(void*);
 	void* log(void*);
 	void* log10(void*);
 	void* log2(void*);
 	void* longjmp(void*);
 	void* malloc(void*);
 	void* memcmp(void*);
 	void* memcpy(void*);
 	void* memmove(void*);
 	void* memset(void*);
 	void* mmap(void*);
 	void* mremap(void*);
 	void* munmap(void*);
 	void* nanosleep(void*);
 	void* open(void*);
 	void* perror(void*);
 	void* pow(void*);
 	void* printf(void*);
 	void* pthread_cond_broadcast(void*);
 	void* pthread_cond_init(void*);
 	void* pthread_cond_wait(void*);
 	void* pthread_create(void*);
 	void* pthread_detach(void*);
 	void* pthread_exit(void*);
 	void* pthread_getspecific(void*);
 	void* pthread_join(void*);
 	void* pthread_key_create(void*);
 	void* pthread_key_delete(void*);
 	void* pthread_mutex_destroy(void*);
 	void* pthread_mutex_init(void*);
 	void* pthread_mutex_lock(void*);
 	void* pthread_mutex_unlock(void*);
 	void* pthread_mutexattr_init(void*);
 	void* pthread_mutexattr_settype(void*);
 	void* pthread_self(void*);
 	void* pthread_setspecific(void*);
 	void* puts(void*);
 	void* read(void*);
 	void* realloc(void*);
 	void* recv(void*);
 	void* regcomp(void*);
 	void* regexec(void*);
 	void* regfree(void*);
 	void* rename(void*);
 	void* round(void*);
 	void* sched_yield(void*);
 	void* select(void*);
 	void* send(void*);
 	void* setsockopt(void*);
 	void* setvbuf(void*);
 	void* shm_open(void*);
 	void* shm_unlink(void*);
 	void* shutdown(void*);
 	void* snprintf(void*);
 	void* socket(void*);
 	void* sprintf(void*);
 	void* sqrt(void*);
 	void* stpcpy(void*);
 	void* strchr(void*);
 	void* strcmp(void*);
 	void* strcpy(void*);
 	void* strcspn(void*);
 	void* strdup(void*);
 	void* strerror(void*);
 	void* strerror_r(void*);
 	void* strlen(void*);
 	void* strncmp(void*);
 	void* strncpy(void*);
 	void* strtol(void*);
 	void* strtoull(void*);
 	void* syscall(void*);
 	void* sysconf(void*);
 	void* timespec_get(void*);
 	void* tm_build_affinity_mat(void*);
 	void* tm_build_tree_from_topology(void*);
 	void* tm_compute_mapping(void*);
 	void* tm_display_topology(void*);
 	void* tm_enable_oversubscribing(void*);
 	void* tm_free_affinity_mat(void*);
 	void* tm_free_solution(void*);
 	void* tm_free_topology(void*);
 	void* tm_free_tree(void*);
 	void* tm_get_local_topology_with_hwloc(void*);
 	void* tm_topology_set_binding_constraints(void*);
 	void* unlink(void*);
 	void* vfprintf(void*);

#endif
