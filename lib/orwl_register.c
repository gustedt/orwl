/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the author and copyright holder for this work is                           */
/* all rights reserved,  2010-2013 Jens Gustedt, INRIA, France                */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#include "orwl_register.h"
#include "orwl_thread.h"
#include "orwl_server.h"
#include "orwl_proc.h"

/**
 ** @related orwl_register
 **/
void orwl_domain_call(orwl_domain domain, size_t id, void *arg) {
  orwl_register const* field = domain + id;
  orwl_proc* proc = arg;
  orwl_register_call(field, proc);
}

orwl_register const* orwl_register_init(orwl_register const* field) {
  if (!field) return 0;
  if (!*(field->regptr)) {
    P99_CRITICAL {
      if (!*(field->regptr)) {
        orwl_register const*base = (field->start);
        for (size_t i = 0; base[i].fptr || base[i].dptr; ++i)
          *(base[i].regptr) = &(base[i]);
      }
    }
  }
  return field;
}

P99_INSTANTIATE(size_t, orwl_register_id, orwl_register const *);
P99_INSTANTIATE(void*, orwl_register_get, orwl_register const *);
P99_INSTANTIATE(void, orwl_register_call, orwl_register const*, void*);
P99_INSTANTIATE(void, orwl_domain_call, orwl_domain, size_t, void*);

void orwl_types_init(void) {
  P99_INIT_CHAIN(orwl_types);
}
