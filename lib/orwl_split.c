/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the author and copyright holder for this work is                           */
/* all rights reserved,  2016 Jens Gustedt, INRIA, France                     */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#include "orwl_split.h"
#include "orwl_server.h"
#include "orwl_helpers.h"
#include "orwl_generic.h"

P99_DECLARE_STRUCT(o_rwl_split);

P99_DEFINE_STRUCT(o_rwl_split,
                  /* This control protects the data structure as a
                     whole. */
                  orwl_thread_cntrl cntrl,
                  bool volatile finished,
                  bool readonly,
                  size_t n,
                  orwl_handle2* hdl,
                  orwl_split_location* locs
                 );

static
void o_rwl_split_delete(o_rwl_split* os) {
  orwl_handle2_delete(os->hdl);
  free(os->locs);
  free(os);
}

DECLARE_THREAD(o_rwl_split, static);

static
void alloc_save(orwl_alloc* target, orwl_alloc* source) {
  if (source && source->size) {
    P99_THROW_ASSERT(EINVAL, target->size == source->size);
    void* ptarget = orwl_alloc_realloc(target, ORWL_ALLOC_MAP);
    P99_THROW_ASSERT(EINVAL, ptarget);
    void* psource = orwl_alloc_realloc(source, ORWL_ALLOC_MAP);
    P99_THROW_ASSERT(EINVAL, psource);
    memcpy(ptarget, psource, target->size);
  }
}

static
void check_and_save(size_t n, orwl_handle2 hdl[n], orwl_alloc_ref aref[n], p99_seed* seed) {
  orwl_alloc_ref tmp = ORWL_ALLOC_REF_INITIALIZER(0);
  for (size_t i = 0; i < n; ++i) {
    orwl_alloc* part = orwl_alloc_ref_get(&aref[i]);
    orwl_handle2_hold(&hdl[i], &tmp, seed, false);
    orwl_alloc* existing = orwl_alloc_ref_get(&tmp);
    if (part != existing) {
      alloc_save(part, existing);
      orwl_handle2_replace(&hdl[i], &aref[i], seed);
    }
  }
  orwl_alloc_ref_destroy(&tmp);
}

static
void condition_handles(bool readonly, orwl_thread_cntrl* cntrl,
                       size_t n, orwl_handle2 hdl[n], orwl_split_location locs[n],
                       p99_seed* seed) {
  orwl_server*const server = orwl_server_get();

  /* The minus signs in the following insertions are not
     accidental. They ensure that if the negated value is 1 (for true)
     the insertion takes place a the very end of the queue. */
  if (readonly)
    orwl_read_insert(&hdl[0], locs[0].loc, -readonly, seed, server);
  else
    orwl_write_insert(&hdl[0], locs[0].loc, -readonly, seed, server);

  for (size_t i = 1; i < n; ++i) {
    orwl_handle2_write_insert(&hdl[i], locs[i].loc, -!readonly, seed, server);
  }

  // tell our caller that we have done all insertions
  orwl_thread_cntrl_freeze(cntrl);
  // wait until all insertions are scheduled
  p99_notifier_block(&server->up_sched);
}

static
void condition_allocations(size_t n, orwl_handle2 hdl[n], orwl_alloc_ref aref[n], orwl_split_location locs[n], p99_seed* seed) {
  // Check if master already has an allocation.
  orwl_handle2_hold(&hdl[0], &aref[0], seed, false);
  orwl_alloc* all = orwl_alloc_ref_get(&aref[0]);
  if (!all) {
    all = P99_NEW(orwl_alloc);
    P99_THROW_ASSERT(ENOMEM, all);
    orwl_alloc_ref_replace(&aref[0], all);
    orwl_handle2_replace(&hdl[0], &aref[0], seed);
  }

  {
    size_t size[n];         P99_MEMZERO(size_t, size, n);
    // Look how many of the sub-locations have a size set. Distribute
    // the amount that is not accounted for to those that haven't.
    size_t missing = 0;
    size_t accounted = 0;
    for (size_t i = 1; i < n; ++i) {
      orwl_handle2_hold(&hdl[i], &aref[i], seed, false);
      orwl_alloc* a = orwl_alloc_ref_get(&aref[i]);
      size[i] = locs[i].size
                ? locs[i].size
                : (a ? a->size : 0);
      if (size[i]) accounted += size[i];
      else ++missing;
    }

    if (all->size) {
      // distribute shares
      P99_THROW_ASSERT(EINVAL, accounted <= all->size);
      for (size_t i = 1; missing; ++i) {
        if (!size[i]) {
          size[i] = (all->size - accounted)/missing;
          accounted += size[i];
          --missing;
        }
      }
    } else {
      // allocate the master
      void* p = orwl_alloc_realloc(all, accounted);
      P99_THROW_ASSERT(ENOMEM, p);
    }
    orwl_alloc_ref tmp = ORWL_ALLOC_REF_INITIALIZER(0);
    for (size_t i = 1, offset = 0; i < n; ++i) {
      orwl_alloc* part = orwl_alloc_partial(all, offset, size[i]);
      orwl_alloc_ref_replace(&aref[i], part);
      /* Don't loose existing data. */
      orwl_handle2_hold(&hdl[i], &tmp, seed, false);
      alloc_save(part, orwl_alloc_ref_get(&tmp));
      orwl_handle2_replace(&hdl[i], &aref[i], seed);
      offset += size[i];
    }
    orwl_alloc_ref_destroy(&tmp);
  }

}

static
void pass_and_check(size_t n, orwl_handle2 hdl[n], orwl_alloc_ref aref[n], p99_seed* seed) {
  orwl_handle2_next(hdl, n, seed);
  /* The other(s) have the data */
  orwl_handle2_acquire(hdl, n, seed);
  check_and_save(n, hdl, aref, seed);
}


DEFINE_THREAD(o_rwl_split, static) {
  ORWL_THREAD_USE(o_rwl_split, hdl, readonly, n, locs);
  o_rwl_split*const obj = Arg;
  p99_seed*const seed = p99_seed_get();

  // condition the handles
  condition_handles(readonly, &obj->cntrl, n, hdl, locs, seed);

  // This holds references to the previous orwl_alloc, such that we
  // can be sure that the orwl_alloc hasn't been recycled when we go
  // into a new iteration.
  orwl_alloc_ref aref[n]; P99_MEMSET(aref, (orwl_alloc_ref)ORWL_ALLOC_REF_INITIALIZER(0), n);

  /* Initialize the split */
  //orwl_handle2_acquire(&hdl[0], 1, seed);

  orwl_handle2_acquire(hdl, n, seed);
  condition_allocations(n, hdl, aref, locs, seed);

  /* If the master location is writable, they get first access. */
  if (readonly) goto WORKER;
  else goto MASTER;

  // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  // +++++++++++++++ The main iteration loop ++++++++++++++++++++
  for (;;) {
MASTER:
    pass_and_check(1, hdl, aref, seed);
    if (obj->finished) break;
WORKER:
    pass_and_check(n-1, hdl+1, aref+1, seed);
    if (obj->finished) break;
  }
  // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


  // unlink all references to the orwl_alloc
  for (size_t i = 0; i < n; ++i) {
    orwl_alloc_ref_destroy(&aref[i]);
  }
  orwl_disconnect(hdl, n, seed);
  orwl_thread_cntrl_wait_for_caller(&obj->cntrl);
}

void o_rwl_split_finish(void* obj) {
  o_rwl_split* os = obj;
  os->finished = true;
  orwl_thread_cntrl_detach(&os->cntrl);
}

static
o_rwl_split* o_rwl_split_init(o_rwl_split* os, bool readonly, size_t n, orwl_split_location locs[n]) {
  if (os) {
    *os = (o_rwl_split) {
      .hdl = P99_CALLOC(orwl_handle2, n),
       .n = n,
        .finished = false,
         .readonly = readonly,
          .locs = memcpy(P99_MALLOC(orwl_split_location[n]), locs, sizeof(orwl_split_location[n])),
    };

    orwl_thread_cntrl_init(&os->cntrl);
    orwl_at_task_exit(o_rwl_split_finish, os);

    P99_THROW_CALL_THRD(o_rwl_split_create_detached, os);
    orwl_thread_cntrl_wait_for_callee(&os->cntrl);
  }
  return os;
}

void orwl_split(bool readonly, size_t n, orwl_split_location locs[n]) {
  P99_NEW(o_rwl_split, readonly, n, locs);
}
