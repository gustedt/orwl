/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the authors and copyright holders for this work are as follows:            */
/* all rights reserved,  2011 Emmanuel Jeanvoine, INRIA, France               */
/* all rights reserved,  2010-2014 Jens Gustedt, INRIA, France                */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#include "orwl_server.h"
#include P99_ADVANCE_ID

DEFINE_ORWL_PROC_FUNC(orwl_proc_do_nothing, void) {
  /* special care for bogus warning given by icc */
  (void)Arg;
}

DEFINE_ORWL_PROC_FUNC(orwl_proc_terminate, void) {
  if (Arg && Arg->srv) {
    trace(1, "termination signal from remote");
    orwl_server_terminate(Arg->srv);
  }
}

static
void orwl_proc_write_request_static(orwl_proc* Arg, p99_notifier * noti, uint64_t wqPOS, uint64_t whID, uint64_t port) {
  ORWL_TIMER() {
    Arg->ret = 0;
    if (wqPOS < Arg->srv->max_queues) {
      orwl_wq *srv_wq = &Arg->srv->wqs[wqPOS];
      /* Create a handle and insert it in the queue.  Request two tokens,
         one for this function here when it acquires below, the other one to
         block until the remote issues a release. */
      orwl_wh *srv_wh = P99_NEW(orwl_wh, 1);
      orwl_wh_ref srv_whr = ORWL_WH_REF_INITIALIZER(srv_wh);
      orwl_state state = orwl_wq_request(srv_wq, srv_wh);
      if (noti) {
        p99_notifier_set(noti);
        noti = 0;
      }
      if (state == orwl_requested) {
        orwl_endpoint ep = { .addr = orwl_proc_getpeer(Arg), };
        orwl_addr_set_port(&ep.addr, (uint16_t)port);
        /* Acknowledge the creation of the wh and send back its id. */
        Arg->ret = (uintptr_t)srv_wh;
        orwl_wq* wq = srv_wh->location;
        /* add a reference for the remote */
        orwl_wh_load_conditionally(srv_wh, 1);
        orwl_proc_untie_caller(Arg);
        /* Wait until the lock on wh is obtained. Only free the token
           later, after we have pushed back to the remote. */
        ORWL_TIMER(wh_acquire)
        state = orwl_wh_acquire(srv_wh);
        P99_THROW_ASSERT(ENOLCK, state == orwl_acquired);
        P99_WRLOCK(wq->rwlock)
        ORWL_TIMER(transfer)
        orwl_transfer(Arg->srv, ep, wq, whID);
      }
      orwl_wh_ref_trigger(&srv_whr);
    }
  }
  if (noti) {
    p99_notifier_set(noti);
    noti = 0;
  }
}

static
void orwl_proc_read_request_static(orwl_proc* Arg, p99_notifier * noti, uint64_t wqPOS, uint64_t cliID, uint64_t svrID, uint64_t port) {
  ORWL_TIMER() {
    orwl_state state = orwl_invalid;
    bool piggyback = false;
    orwl_wh_ref srv_wh = ORWL_WH_REF_INITIALIZER(0);
    Arg->ret = 0;
    if (wqPOS < Arg->srv->max_queues) {
      /* extract wq and the remote wh ID */
      orwl_wq *srv_wq = &Arg->srv->wqs[wqPOS];

      /* First check if a previously inserted inclusive handle can be
         re-used. Always request two tokens, one for this function here
         when it acquires below, the other one to block until the remote
         issues a release */
      do {
        ORWL_TIMER(locked)
        P99_MUTUAL_EXCLUDE(srv_wq->mut) {
          ORWL_TIMER(try)
          state = o_rwl_wq_try_request_locked(srv_wq, &srv_wh);
          switch (state) {
          case orwl_again:
            P99_THROW_ASSERT(EINVAL, !orwl_wh_ref_get(&srv_wh));
            break;
          case orwl_requested:
            P99_THROW_ASSERT(EINVAL, orwl_wh_ref_get(&srv_wh));
            piggyback = (svrID == (uintptr_t)orwl_wh_ref_get(&srv_wh));
            break;
          default:
            ORWL_TIMER(request) {
              P99_THROW_ASSERT(EINVAL, !orwl_wh_ref_get(&srv_wh));
              orwl_wh* wh = P99_NEW(orwl_wh, 1);
              orwl_wh_ref_replace(&srv_wh, wh);
              /* mark it as being inclusive */
              wh->svrID = (uintptr_t)wh;
              o_rwl_wq_request_append_locked(srv_wq, wh);
              state = orwl_requested;
            }
          }
        }
      } while (state == orwl_again);
    }
    if (noti) {
      p99_notifier_set(noti);
      noti = 0;
    }
    P99_THROW_ASSERT(ENOLCK, state == orwl_requested);
    orwl_endpoint ep = { .addr = orwl_proc_getpeer(Arg), };
    orwl_addr_set_port(&ep.addr, (uint16_t)port);
    /* Acknowledge the creation of the wh and send back its ID. */
    orwl_wh* wh = orwl_wh_ref_get(&srv_wh);
    Arg->ret = (uintptr_t)wh;
    /* account for the remote only if we introduced a new handle */
    if (piggyback) {
      orwl_proc_untie_caller(Arg);
    } else {
      orwl_wh_load_conditionally(wh, 1);
      orwl_proc_untie_caller(Arg);

      /* Wait until the lock on wh is obtained. Only free the
         token later, after we have pushed back to the remote. */
      ORWL_TIMER(wh_acquire)
      state = orwl_wh_acquire(wh);
      P99_THROW_ASSERT(ENOLCK, state == orwl_acquired);
      ORWL_TIMER(push_copy)
      orwl_push_copy(Arg->srv, ep, wh->location, cliID);
    }
    orwl_wh_ref_trigger(&srv_wh);
  }
}

DEFINE_ORWL_PROC_FUNC(orwl_proc_write_request, uint64_t wqPOS, uint64_t whID, uint64_t port) {
  /* extract wq and the remote wh ID */
  ORWL_PROC_READ(Arg, orwl_proc_write_request, uint64_t wqPOS, uint64_t whID, uint64_t port);
  orwl_proc_write_request_static(Arg, 0, wqPOS, whID, port);
}

DEFINE_ORWL_PROC_FUNC(orwl_proc_read_request, uint64_t wqPOS, uint64_t cliID, uint64_t svrID, uint64_t port) {
  /* Extract wq and the remote handle IDs from Arg */
  ORWL_PROC_READ(Arg, orwl_proc_read_request, uint64_t wqPOS, uint64_t cliID, uint64_t svrID, uint64_t port);
  orwl_proc_read_request_static(Arg, 0, wqPOS, cliID, svrID, port);
}

DEFINE_ORWL_PROC_FUNC(orwl_proc_request_insert, uint64_t pri, uint64_t incl) {
  ORWL_PROC_READ(Arg, orwl_proc_request_insert, uint64_t pri, uint64_t incl);
  o_rwl_grouping* p = P99_NEW(o_rwl_grouping, pri, incl);
  P99_LIFO_PUSH(&Arg->srv->waiters_remote, p);
  Arg->ret = (uintptr_t)p;
}


DEFINE_ORWL_PROC_FUNC(orwl_proc_write_insert, uint64_t remP, uint64_t wqPOS, uint64_t whID, uint64_t port) {
  ORWL_TIMER() {
    ORWL_PROC_READ(Arg, orwl_proc_write_insert, uint64_t remP, uint64_t wqPOS, uint64_t whID, uint64_t port);
    o_rwl_grouping* p = (o_rwl_grouping*)(uintptr_t)remP;
    ORWL_TIMER(block)
    p99_notifier_block(&p->ok);
    ORWL_TIMER(static)
    orwl_proc_write_request_static(Arg, &p->launched, wqPOS, whID, port);
  }
}

DEFINE_ORWL_PROC_FUNC(orwl_proc_read_insert, uint64_t remP, uint64_t wqPOS, uint64_t cliID, uint64_t port) {
  ORWL_TIMER() {
    ORWL_PROC_READ(Arg, orwl_proc_read_insert, uint64_t remP, uint64_t wqPOS, uint64_t cliID, uint64_t port);
    o_rwl_grouping* p = (o_rwl_grouping*)(uintptr_t)remP;
    ORWL_TIMER(block)
    p99_notifier_block(&p->ok);
    ORWL_TIMER(static)
    orwl_proc_read_request_static(Arg, &p->launched, wqPOS, cliID, 0, port);
  }
}

DEFINE_ORWL_PROC_FUNC(orwl_proc_read_insert_again, uint64_t remP) {
  ORWL_TIMER() {
    ORWL_PROC_READ(Arg, orwl_proc_read_insert_again, uint64_t remP);
    o_rwl_grouping* p = (o_rwl_grouping*)(uintptr_t)remP;
    ORWL_TIMER(block)
    p99_notifier_block(&p->ok);
    p99_notifier_set(&p->launched);
  }
}



/* this is executed first on the client when the lock is acquired and */
/* then on the server when the lock is released. */
DEFINE_ORWL_PROC_FUNC(orwl_proc_release, uintptr_t whID, uint64_t flags, uint64_t read_len) {
  ORWL_TIMER() {
    ORWL_PROC_READ(Arg, orwl_proc_release, uintptr_t whID, uint64_t flags, uint64_t read_len);
    bool withdata = (flags & orwl_push_withdata);
    bool keep = (flags & orwl_push_keep);

    Arg->ret = orwl_valid;
    // extract the wh for Arg
    P99_THROW_ASSERT(ENOLCK, whID);
    orwl_wh* wh = (void*)whID;
    orwl_wh_ref whr = ORWL_WH_REF_INITIALIZER(wh);
    orwl_wq* wq = wh->location;
    P99_THROW_ASSERT(ENOLCK, wq);
    if (withdata) {
      P99_WRLOCK(wq->rwlock) {
        if (Arg->fd != -1) {
          /* This is a remote connection */
          P99_THROW_ASSERT(EINVAL, Arg->n == 1);
          /* Receive the data in place. */
          o_rwl_wq_resize_wlocked(wq, read_len);
          if (read_len) {
            size_t mlen = 0;
            orwl_buffer mes1 =
              ORWL_BUFFER_INITIALIZER8(0, o_rwl_wq_map_rlocked(wq, &mlen), 0);
            mes1.aio.aio_nbytes = mlen * sizeof(uint64_t);
            orwl_alloc_ref_assign(&mes1.allocr, &wq->allocr);
            ORWL_TIMER(3_2recv_, mes1.aio.aio_nbytes)
            orwl_recv_(Arg->fd, &mes1, Arg->remoteorder);
            orwl_buffer_destroy(&mes1);
          }
        } else {
          /* Link the buffer in the case that this was a local connection. */
          if (Arg->n == 2) {
            o_rwl_wq_link_wlocked(wq, &Arg->mes[1], keep);
            orwl_buffer_destroy(&Arg->mes[1]);
          }
        }
      }
    }
    /* Now that we have taken over the data, if necessary, we untie
       the caller. We still have a token loaded on wh, so we still
       have it acquired, in particular wh still will be in front of
       wq. */
    orwl_proc_untie_caller(Arg);
    /* Only now release our hold on wh. */
    orwl_wh_ref_trigger(&whr);
  }
}

/* this is executed on the server when a lock is released that doesn't
   need a data transfer.*/
DEFINE_ORWL_PROC_FUNC(orwl_proc_trigger, uintptr_t whID) {
  ORWL_TIMER() {
    ORWL_PROC_READ(Arg, orwl_proc_trigger, uintptr_t whID);
    orwl_state ret = orwl_valid;
    // extract the wh for Arg
    P99_THROW_ASSERT(ENOLCK, whID);
    orwl_wh* wh = (void*)whID;
    orwl_wh_ref whr = ORWL_WH_REF_INITIALIZER(wh);
    Arg->ret = ret;
    orwl_proc_untie_caller(Arg);
    /* Only now release our hold on wh. */
    orwl_wh_ref_trigger(&whr);
  }
}

DEFINE_ORWL_PROC_FUNC(orwl_proc_check_initialization, uint64_t id) {
  ORWL_PROC_READ(Arg, orwl_proc_check_initialization, uint64_t id);
  p99_notifier_block(&Arg->srv->id_initialized[id]);
}

/* Block until the task "id" is in the same "phase" as "hid". Returns
   the length len of the (cyclic) interval of task ids starting at
   "id" that the remote server knows about that also are in
   "phase". So all tasks i with id <= i < id+len can be considered
   ok. */
DEFINE_ORWL_PROC_FUNC(orwl_proc_barrier, uint64_t hid, uint64_t phase, uint64_t remaining) {
  ORWL_PROC_READ(Arg, orwl_proc_barrier, uint64_t hid, uint64_t phase, uint64_t remaining);
  orwl_server *const srv = Arg->srv;
  p99_notifier_block(&srv->up_ab);
  size_t len = srv->global_barrier_len;
  size_t known = len - remaining;
  uint64_t* global_barrier = srv->global_barrier;
  P99_MUTUAL_EXCLUDE(srv->global_barrier_mtx) {
    /* This is the id the query was all about */
    size_t id = (hid + known) % len;
    /* memorize the information known at the other end */
    while (hid != id) {
      if (global_barrier[hid] == (phase - 1))
        global_barrier[hid] = phase;
      hid = (hid + 1) % len;
    }
    while(global_barrier[id] < phase)
      P99_THROW_CALL_THRD(cnd_wait, &srv->global_barrier_cnd, &srv->global_barrier_mtx);
    /* Now scan for the next id that isn't in the same phase, yet, if
       any. */
    size_t i = 1;
    while ((i < remaining)
           && (global_barrier[(id + i) % len] >= phase))
      ++i;
    Arg->ret = i;
  }
}

DEFINE_ORWL_PROC_FUNC(orwl_proc_block, void) {
  Arg->ret = orwl_server_block(Arg->srv);
}

DEFINE_ORWL_PROC_FUNC(orwl_proc_unblock, void) {
  Arg->ret = orwl_server_unblock(Arg->srv);
}

DEFINE_ORWL_PROC_FUNC(orwl_proc_final, void) {
  p99_notifier_set(&Arg->srv->term_srv);
  Arg->ret = 0;
}

DEFINE_ORWL_TYPE_DYNAMIC(orwl_proc,
                         ORWL_REGISTER_ALIAS(orwl_proc_do_nothing, orwl_proc),
                         ORWL_REGISTER_ALIAS(orwl_proc_terminate, orwl_proc),
                         ORWL_REGISTER_ALIAS(orwl_proc_final, orwl_proc),
                         ORWL_REGISTER_ALIAS(orwl_proc_write_request, orwl_proc),
                         ORWL_REGISTER_ALIAS(orwl_proc_read_request, orwl_proc),
                         ORWL_REGISTER_ALIAS(orwl_proc_request_insert, orwl_proc),
                         ORWL_REGISTER_ALIAS(orwl_proc_write_insert, orwl_proc),
                         ORWL_REGISTER_ALIAS(orwl_proc_read_insert, orwl_proc),
                         ORWL_REGISTER_ALIAS(orwl_proc_read_insert_again, orwl_proc),
                         ORWL_REGISTER_ALIAS(orwl_proc_release, orwl_proc),
                         ORWL_REGISTER_ALIAS(orwl_proc_trigger, orwl_proc),
                         ORWL_REGISTER_ALIAS(orwl_proc_check_initialization, orwl_proc),
                         ORWL_REGISTER_ALIAS(orwl_proc_barrier, orwl_proc),
                         ORWL_REGISTER_ALIAS(orwl_proc_block, orwl_proc),
                         ORWL_REGISTER_ALIAS(orwl_proc_unblock, orwl_proc)
                        );
