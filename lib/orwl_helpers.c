/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the authors and copyright holders for this work are as follows:            */
/* all rights reserved,  2011 Emmanuel Jeanvoine, INRIA, France               */
/* all rights reserved,  2011-2014, 2016-2017 Jens Gustedt, INRIA, France     */
/* all rights reserved,  2013 Rodrigo Campos, INRIA, France                   */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#include "orwl_server.h"
#include "p99_c99_default.h"
#include "p99_c99_throw.h"
#include "p99_str.h"

DEFINE_NEW_DELETE(orwl_vertex);
DEFINE_NEW_DELETE(orwl_graph);
DEFINE_NEW_DELETE(orwl_address_book);
DEFINE_NEW_DELETE(orwl_thread_local);

P99_DEFINE_DEFARG(orwl_vertex_init,, P99_0(size_t));
P99_DEFINE_DEFARG(orwl_graph_init,, P99_0(size_t));
P99_DEFINE_DEFARG(orwl_address_book_init,, P99_0(size_t));

P99_INSTANTIATE(orwl_vertex*, orwl_vertex_init, orwl_vertex*, size_t);
P99_INSTANTIATE(void, orwl_vertex_destroy, orwl_vertex*);
P99_INSTANTIATE(orwl_graph*, orwl_graph_init, orwl_graph *, size_t);
P99_INSTANTIATE(void, orwl_graph_destroy, orwl_graph *);
P99_INSTANTIATE(orwl_address_book*, orwl_address_book_init, orwl_address_book *, size_t);
P99_INSTANTIATE(void, orwl_address_book_destroy, orwl_address_book *);
P99_INSTANTIATE(orwl_thread_local*, orwl_thread_local_init, orwl_thread_local*);
P99_INSTANTIATE(void, orwl_thread_local_destroy, orwl_thread_local*);
P99_INSTANTIATE(void, orwl_thread_local_free, void*);
P99_INSTANTIATE(orwl_thread_local*, orwl_address_book_get_local, orwl_address_book *, orwl_thread_local*);
P99_INSTANTIATE(size_t *, o_rwl_ab_nl, orwl_address_book *, size_t*);
P99_INSTANTIATE(size_t, orwl_tasks2locations, size_t, size_t const[], size_t, size_t[]);

P99_PURE_FUNCTION
size_t o_rwl_srv_sl(orwl_server* srv, size_t def) {
  return srv ? srv->first_location : def;
}

static
char * remove_eol(char *str) {
  size_t len = strlen(str) - 1;
  if (str[len] == '\n')
    str[len] = '\0';
  return str;
}

P99_PURE_FUNCTION
static
bool o_rwl_graph_find_neighbor(orwl_vertex *vertex, size_t id) {
  if (vertex != NULL)
    for (size_t i = 0 ; i < vertex->nb_neighbors ; i++)
      if (vertex->neighbors[i] == id) return true;
  return false;
}

static
size_t extract_size_t_from_str(int start, int end, char const *original_str) {
  return strtouz(original_str + start);
}

static
void extract_str_from_str(int start, int end, char const *original_str, char *dest_str) {
  const size_t size = end - start;
  memcpy(dest_str, &original_str[start], size);
  dest_str[size] = '\0';
}

static
bool orwl_graph_extract_line(orwl_graph *graph,
                             char const *str, int pass,
                             size_t *nb_neighbors,
                             regex_t *re_connection,
                             regex_t *re_attributes) {

  regmatch_t connection_match[3]= { P99_RVAL(regmatch_t) };
  regmatch_t attributes_match[4]= { P99_RVAL(regmatch_t) };

  /* matching a connection line */
  if (regexec(re_connection, str, 3, connection_match, 0) == 0) {
    size_t src_vertex = extract_size_t_from_str(connection_match[1].rm_so,
                        connection_match[1].rm_eo,
                        str);
    size_t dst_vertex = extract_size_t_from_str(connection_match[2].rm_so,
                        connection_match[2].rm_eo,
                        str);
    if (pass == 0) {
      nb_neighbors[src_vertex]++;
    } else {
      if (graph->vertices[src_vertex].neighbors == NULL) {
        size_t nb = nb_neighbors[src_vertex];
        orwl_vertex_init(&graph->vertices[src_vertex], nb);
        for (size_t i = 0 ; i < nb ; i++)
          graph->vertices[src_vertex].neighbors[i] = SIZE_MAX;
      }
      size_t i = 0;
      while (graph->vertices[src_vertex].neighbors[i] != SIZE_MAX) i++;
      graph->vertices[src_vertex].neighbors[i] = dst_vertex;
    }
  }
  /* matching an attribute line (only at the second pass) */
  if ((pass == 1) && (regexec(re_attributes, str, 4, attributes_match, 0) == 0)) {
    size_t vertex = extract_size_t_from_str(attributes_match[1].rm_so,
                                            attributes_match[1].rm_eo,
                                            str);

    size_t color = extract_size_t_from_str(attributes_match[2].rm_so,
                                           attributes_match[2].rm_eo,
                                           str);

    char label[16] = {0};
    extract_str_from_str(attributes_match[3].rm_so,
                         attributes_match[3].rm_eo,
                         str,
                         &label[0]);
    graph->vertices[vertex].color = color;
    strcpy(graph->vertices[vertex].label, label);
  }
  return true;
}

void orwl_graph_read(orwl_graph ** graph, char const* file, size_t nb_vertices) {
  FILE*volatile f = fopen_throw(file, "r");
  *graph = P99_NEW(orwl_graph, nb_vertices);
  char msg[LINE_MAX] = {0};
  size_t nb_neighbors[nb_vertices];
  for (size_t i = 0 ; i < nb_vertices ; i++)
    nb_neighbors[i] = 0;

  const char *connection = "^[[:blank:]]*([[:digit:]]+)[[:blank:]]*->[[:blank:]]*([[:digit:]]+)[[:blank:]]*$";
  const char *attributes = "^[[:blank:]]*([[:digit:]]+)[[:blank:]]*\\[color=\"([[:digit:]]+)\",[[:blank:]]*label=\"([[:digit:]]+-[[:alnum:]]+)\"\\]$";
  regex_t re_connection, re_attributes;

  P99_TRY {
    int res = regcomp(&re_connection, connection, REG_EXTENDED);
    if (res) P99_THROW(res);

    P99_TRY {
      res = regcomp(&re_attributes, attributes, REG_EXTENDED);
      if (res) P99_THROW(res);

      P99_TRY {
        for (size_t pass = 0 ; pass < 2 ; pass++) {
          if (fseek(f, 0, SEEK_SET) != 0) {
            P99_HANDLE_ERRNO {
P99_XDEFAULT : {
                perror("error when seeking the begin of the file");
                orwl_graph_delete(*graph);
                P99_THROW(p99_errno);
              }
            }
          }
          while (fgets(msg, LINE_MAX, f) != NULL) {
            if (!orwl_graph_extract_line(*graph, remove_eol(msg), pass, nb_neighbors, &re_connection, &re_attributes)) {
              printf("error while extracting a line in the graph file\n");
              orwl_graph_delete(*graph);
              P99_THROW_ERRNO;
            }
          }
        }
      } P99_FINALLY {
        regfree(&re_attributes);
      }
    } P99_FINALLY {
      regfree(&re_connection);
    }
  } P99_FINALLY {
    fclose(f);
  }
}

static
bool orwl_address_book_extract_line(orwl_address_book *ab, char const *str) {
  /*
  A line contains id and endpoint, separated by a tab character.
     */
  char* endptr = 0;
  size_t id = strtoull(str, &endptr, 10);
  if (endptr == str) P99_THROW(EINVAL);
  /* isblank is locale independent */
  while (isblank(endptr[0]))
    ++endptr;
  if (strlen(endptr) < 2) return false;
  trace(0, "endpoint %zu detected as %s", id, endptr);
  orwl_endpoint_parse(endptr, &ab->eps[id]);
  trace(0, "endpoint %zu written as %s", id, orwl_endpoint_print(&ab->eps[id]));
  return true;
}


void orwl_address_book_read(orwl_address_book **ab, FILE *f) {
  size_t nb_loc = 0;
  size_t nb_loc_max = 8;
  char const** lines = P99_REALLOC(0, char*[nb_loc_max]);
  lines[0] = 0;
  do {
    char * theLine = calloc_throw(LINE_MAX, 1);
    if (!fgets(theLine, LINE_MAX, f) || theLine[0] == '\n') {
      free(theLine);
      break;
    }
    remove_eol(theLine);
    lines[nb_loc] = theLine;
    ++nb_loc;
    if (nb_loc >= nb_loc_max) {
      nb_loc_max *= 2;
      lines = P99_REALLOC(lines, char*[nb_loc_max]);
    }
    lines[nb_loc] = 0;
  } while (!feof(f));

  P99_TRY {
    if (*ab) orwl_address_book_delete(*ab);
    *ab = P99_NEW(orwl_address_book, nb_loc);
    size_t faulty = 0;
    for (size_t i = 0; i < nb_loc; i++) {
      if (!orwl_address_book_extract_line(*ab, lines[i])) ++faulty;
    }
    nb_loc -= faulty;
  } P99_CATCH(int code) {
    for (size_t i = 0; i < nb_loc; i++) {
      free((void*)(lines[i]));
    }
    free(lines);
    if (code) {
      report(1, "reading of address book failed");
      orwl_address_book_delete(*ab);
      P99_RETHROW;
    }
  }

  for (size_t i = 0; i < nb_loc; ++i) {
    orwl_mirror_connect(&(*ab)->mirrors[i], (*ab)->eps[i]);
  }
}

void o_rwl_address_book_read(orwl_address_book **ab, char const *file) {
  FILE*const f = fopen_throw(file, "r");
  P99_TRY {
    orwl_address_book_read(ab, f);
  } P99_CATCH(int err) {
    fclose(f);
    if (err) P99_RETHROW;
  }
}


void o_rwl_write_address_book(orwl_server *serv,
                              const char *filename,
                              size_t nb_id,
                              size_t const list_id[nb_id]) {
  size_t flen = strlen(filename);
  char tmp[flen + 4];
  memcpy(tmp, filename, flen);
  memcpy(tmp+flen, "_0", 3);
  FILE*const out = fopen_throw(tmp, "w");
  P99_TRY {
    orwl_write_address_book(serv, out, nb_id, list_id);
  } P99_CATCH(int err) {
    fclose(out);
    if (err) P99_RETHROW;
    /* Only move the file to the expected name, once it is completely
       written. */
    rename(tmp, filename);
  }
}

void orwl_write_address_book(orwl_server *serv,
                             FILE *out,
                             size_t nb_id,
                             size_t const list_id[nb_id]) {
  enum { maxlen = 256 };
  char line[maxlen] = P99_INIT;
  orwl_endpoint ep = serv->ep;
  setvbuf(out, 0, _IOLBF, 0);
  for (size_t i = nb_id-1 ; i < nb_id ; i--) {
    ep.index = i;
    int len = snprintf(line, maxlen, "%zu\t", list_id[i]);
    /* write the endpoint description in place */
    orwl_endpoint_print(&ep, line + len);
    size_t lentot = strlen(line);
    if (lentot > maxlen) P99_THROW(EMSGSIZE);
    line[lentot + 0] = '\n';
    line[lentot + 1] = '\0';
    if (fputs(line, out) == EOF) P99_THROW_ERRNO;
  }
}

void orwl_wait_until_file_is_here(const char *filename) {
  struct stat buf[1] = P99_INIT;
  while (!stat(filename, buf)) sleepfor(1E-3);
}

static
size_t find_min(size_t len, size_t const list[len]) {
  size_t ret = list[0];
  for (size_t i = 1; i < len; ++i)
    if (list[i] < ret) ret = list[i];
  return ret;
}

ORWL_DEFINE_ENV(ORWL_LOCAL_AB);
ORWL_DEFINE_ENV(ORWL_GLOBAL_AB);
ORWL_DEFINE_ENV(ORWL_OUTGRAPH);
ORWL_DEFINE_ENV(ORWL_STDOUT);

void orwl_wait_and_load_init_files(size_t nb_id, size_t const list_id[nb_id],
                                   const char *id_filename,
                                   const char *ab_filename,
                                   const char *graph_filename,
                                   orwl_server *serv) {
  if (!list_id) {
    nb_id = serv->ll;
    list_id = serv->locids;
  }
  if (!id_filename) {
    id_filename = ORWL_LOCAL_AB();
  }
  if (!ab_filename) {
    ab_filename = ORWL_GLOBAL_AB();
    /* Under some weird circumstances it seems that this is needed if
       the address book is not set. */
    if (!ab_filename) ab_filename = "< no global address book file >";
  }
  /* Write out the address for our local locations and read back the
     address book for the whole run. */
  if (!id_filename) {
    orwl_write_address_book(serv, stdout, nb_id, list_id);
    fclose(stdout);
    orwl_address_book_read(&serv->ab, stdin);
  } else {
    if (!ab_filename) P99_THROW(ENOENT);
    o_rwl_write_address_book(serv, id_filename, nb_id, list_id);
    orwl_wait_until_file_is_here(id_filename);
    o_rwl_address_book_read(&serv->ab, ab_filename);
  }
  char const*const stdout_name = ORWL_STDOUT();
  if (stdout_name && stdout_name[0]) {
    fclose(stdout);
    trace(orwl_verbose, "re-opening stdout to %s", stdout_name);
    P99_THROW_CALL_VOIDP(freopen, EINVAL, stdout_name, "w+", stdout);
  }

  /* Set up the barrier. */
  serv->ll = nb_id;
  if (!serv->locids) {
    size_t * locids = P99_MALLOC(size_t[nb_id]);
    memcpy(locids, list_id, sizeof(size_t[nb_id]));
    serv->locids = locids;
  }
  serv->first_location = find_min(nb_id, list_id);
  size_t nl = ORWL_NL(serv->ab);
  serv->id_initialized = p99_notifier_vrealloc(serv->id_initialized, nl);
  serv->global_barrier = calloc(sizeof(serv->global_barrier), nl);
  serv->global_barrier_len = nl;
  orwl_barrier_init(&serv->local_barrier, serv->ll);

  serv->ab->next = (serv->first_location + serv->ll) % nl;

  trace(orwl_verbose, "next server reachable at position %zu/%zu is %s",
        serv->ab->next,
        nl,
        orwl_endpoint_print(&serv->ab->eps[serv->ab->next]));


  if (graph_filename)
    orwl_graph_read(&serv->graph, graph_filename, nl);
  if (ORWL_OUTGRAPH()) orwl_open_group_graph(serv);
  if (serv->lt == 1 && serv->locids) ORWL_MYLOC(serv->ab) = serv->locids[0];
  p99_notifier_set(&serv->up_ab);
}

void orwl_open_group_graph(orwl_server* srv) {
  char name[256] = { 0 };
  char const* basename = ORWL_OUTGRAPH();
  if (!basename) basename = "group_graph";
  snprintf(name, 256, "%s_%zu.dot", basename, srv->first_location);
  srv->outgraph = fopen_throw(name, "w");
}

void orwl_close_group_graph(orwl_server* srv) {
  if (srv->outgraph) {
    fclose(srv->outgraph);
    srv->outgraph = 0;
  }
}

void orwl_make_distant_connection(size_t dest_id,
                                  orwl_mirror *location,
                                  orwl_server *server) {
  orwl_endpoint there = server->ab->eps[dest_id];
  orwl_mirror_connect(location, there,, server);
  report(0, "connected to %s", orwl_endpoint_print(&there));
}

void orwl_make_local_connection(size_t dest_id,
                                orwl_mirror *location,
                                orwl_server *server) {
  orwl_endpoint there = server->ep;
  orwl_mirror_connect(location, there, dest_id, server);
  report(0, "connected to %s", orwl_endpoint_print(&there));
}

static
void o_rwl_rpc_check_colored_init_finished(size_t id,
    orwl_graph *graph,
    orwl_address_book *ab,
    p99_seed *seed) {
  orwl_endpoint there = ab->eps[id];
  /* warning, this is a blocking operation */
  orwl_rpc(0, &there, seed, orwl_proc_check_initialization, id);
}

size_t orwl_get_neighbors_in_undirected_graph(orwl_vertex **my_neighbors,
    size_t id,
    orwl_graph *graph) {
  size_t current = 0;
  orwl_vertex * me = &graph->vertices[id];
  /* first we add the neighbors where I request locks */
  for (size_t i = 0 ; i < me->nb_neighbors ; i++)
    my_neighbors[current++] = &graph->vertices[me->neighbors[i]];
  /* then we add the neighbors that request my locks */
  for (size_t i = 0 ; i < graph->nb_vertices ; i++) {
    if (i != id)
      if (o_rwl_graph_find_neighbor(&graph->vertices[i], id)) {
        bool found = false;
        for (size_t j = 0 ; j < current ; j++) {
          if (&graph->vertices[i] == my_neighbors[j]) {
            found = true;
            break;
          }
        }
        if (!found)
          my_neighbors[current++] = &graph->vertices[i];
      }
  }
  return current;
}

static
P99_PURE_FUNCTION
size_t orwl_get_id_from_vertex(orwl_graph *graph, orwl_vertex *vertex) {
  for (size_t i = 0 ; i < graph->nb_vertices; i++)
    if (&graph->vertices[i] == vertex)
      return i;
  return SIZE_MAX;
}

bool orwl_wait_to_initialize_locks(size_t id,
                                   orwl_server* serv,
                                   p99_seed *seed) {
  orwl_vertex * me = &serv->graph->vertices[id];
  orwl_vertex * my_neighbors[serv->graph->nb_vertices]; /* upper limit */
  for (size_t i = 0 ; i < serv->graph->nb_vertices ; i++)
    my_neighbors[i] = NULL;

  size_t nb = orwl_get_neighbors_in_undirected_graph(my_neighbors,
              id,
              serv->graph);
  for (size_t i = 0 ; i < nb ; i++) {
    assert(my_neighbors[i]->color != me->color);
    /* we ensure that the neighbors with a lower color are initialized */
    if (my_neighbors[i]->color < me->color) {
      size_t vertex_id = orwl_get_id_from_vertex(serv->graph, my_neighbors[i]);
      if (vertex_id == SIZE_MAX) {
        printf("error when getting the id number of a vertex");
        return false;
      }
      o_rwl_rpc_check_colored_init_finished(vertex_id, serv->graph, serv->ab, seed);
    }
  }

  return true;
}

/* Block until the location "loc" is in the same "phase" as "myloc". Returns
   the length len of the (cyclic) interval of task locs starting at
   "loc" that the remote server knows about that also are in
   "phase". So all tasks i with loc <= i < loc+len can be considered
   ok. Returns 0 if all tasks are known to be in "phase". */
static
size_t o_rwl_rpc_check_barrier(size_t loc,
                               size_t myloc,
                               uint64_t phase,
                               size_t remaining,
                               orwl_address_book *ab,
                               p99_seed *seed) {
  uint64_t ret = orwl_rpc(0, &ab->eps[loc], seed, orwl_proc_barrier, myloc, phase, remaining);
  trace(ret == UINT64_MAX, "warning: received poisoned return from orwl_rpc");
  return ret;
}

void orwl_global_barrier_init(size_t myloc, orwl_server *server) {
  server->global_barrier[myloc] = 0;
}

int orwl_global_barrier_wait(size_t myloc,
                             size_t locations_amount,
                             orwl_server *server,
                             p99_seed *seed) {
  p99_notifier_block(&server->up_ab);
  if (myloc == SIZE_MAX) myloc = ORWL_MYLOC(server->ab);
  if (server->ab->error) {
    TRACE(1, "an error had previously occurred for task %s: %s",
          server->ab->lost, strerror(server->ab->error));
    P99_THROW(server->ab->error);
  }
  uint64_t phase = 0;
  uint64_t *const global_barrier = server->global_barrier;
  size_t nb_loc = server->global_barrier_len;
  mtx_t*const global_barrier_mtx = &server->global_barrier_mtx;
  cnd_t*const global_barrier_cnd = &server->global_barrier_cnd;
  P99_MUTUAL_EXCLUDE(*global_barrier_mtx) {
    for (size_t i = 0; i < locations_amount; ++i)
      ++global_barrier[myloc + i];
    phase = global_barrier[myloc];
  }
  if (orwl_barrier_wait(&server->local_barrier, locations_amount)) {
    P99_THROW_CALL_THRD(cnd_broadcast, global_barrier_cnd);
    for (size_t i = locations_amount; i < nb_loc ;) {
      size_t task = (myloc + i) % nb_loc;
      size_t remaining = nb_loc - i;
      size_t volatile checked = 1;
      if (!orwl_endpoint_similar(&server->ab->eps[myloc], &server->ab->eps[task])) {
        if (global_barrier[task] < phase) {
          P99_TRY {
            checked = o_rwl_rpc_check_barrier(task, myloc, phase, remaining, server->ab, seed);
            trace(checked == SIZE_MAX, "warning: received poisoned return from orwl_rpc");
          } P99_CATCH(int err) {
            switch (err) {
            case 0: break;
            case EAGAIN:;
            case EBADF:;
            case ECONNABORTED:;
            case ECONNREFUSED:;
            case ECONNRESET:;
            case ENOTCONN:;
            case EPERM:;
              /* The other end has disappeared. The only valid reason
                 could be that it already knows that everybody is in
                 and that this the final barrier. In any case this
                 barrier is not valid anymore, leave it. */
              server->ab->error = err;
              server->ab->lost = task;
              REPORT(1, "We lost %s on error %s", task, err);
              checked = remaining;
              break;
            case EINTR:; /* This one should have been caught previously. */
            default:;
              trace(1, "considered fatal: %s", strerror(err));
              P99_RETHROW;
            }
          }
          if (checked > remaining) checked = remaining;
        }
      }
      /* share the information with the other task in the same server, here. */
      P99_MUTUAL_EXCLUDE(*global_barrier_mtx) {
        P99_THROW_CALL_THRD(cnd_broadcast, global_barrier_cnd);
        for (size_t ii = 0; ii < checked; ++ii) {
          size_t taski = (task + ii) % nb_loc;
          /* The task might already be ahead and waiting in the
             barrier for the next phase. So only update if it has been
             in the previous phase. */
          if (global_barrier[taski] == (phase - 1))
            global_barrier[taski] = phase;
        }
      }
      i += checked;
    }
  }
  if (orwl_barrier_wait(&server->local_barrier, locations_amount)) {
    server->local_elected = myloc;
    double epoch = seconds();
    atomic_store_explicit(&orwl_epoch, epoch, memory_order_release);
    atomic_fetch_add_explicit(&orwl_epoch_count, 1, memory_order_acq_rel);
  }
  return !myloc;
}

bool orwl_wait_to_start(size_t id,
                        size_t nb_ll,
                        orwl_server *server,
                        p99_seed *seed) {
  p99_notifier_set(&server->id_initialized[id]);
  orwl_vertex * me = &server->graph->vertices[id];
  orwl_vertex * my_neighbors[server->graph->nb_vertices]; /* upper limit */
  size_t nb = orwl_get_neighbors_in_undirected_graph(my_neighbors,
              id,
              server->graph);

  for (size_t i = 0 ; i < nb ; i++) {
    assert(my_neighbors[i]->color != me->color);
    if (my_neighbors[i]->color > me->color) {
      size_t vertex_id = orwl_get_id_from_vertex(server->graph, my_neighbors[i]);
      if (vertex_id == SIZE_MAX) {
        printf("error when getting the id number of a vertex");
        return false;
      }
      o_rwl_rpc_check_colored_init_finished(vertex_id, server->graph, server->ab, seed);
    }
  }

  orwl_server_delayed_unblock(server, nb_ll);
  return true;
}

void print_statistics(size_t id) {
  printf("print_statistics does nothing\n");
}

char const* orwl_tgetenv(char const* name,
                         size_t mytid,
                         orwl_server* serv) {
  char const* ret = 0;
  assert(name);
  assert(serv);
  /* targs is an array of pointers to char arrays that are themselves
     a sequence of strings that contain name=value pairs.
     tlens contains the total length of char array. */
  char const*const*const targs = serv->targs;
  if ((mytid < SIZE_MAX/2) && targs) {
    char const* targ = targs[mytid];
    if (targ && targ[0]) {
      assert(serv->tlens);
      char const* tend = targ + serv->tlens[mytid];
      size_t namelen = strlen(name);
      while (targ < tend) {
        size_t len = strlen(targ);
        if (namelen <= len) {
          if (!memcmp(name, targ, namelen)) {
            if (targ[namelen]) {
              if (targ[namelen] == '=') {
                /* we found it and it is set to something */
                ret = targ+namelen+1;
                break;
              }
            } else {
              /* we found it and it is empty */
              ret = targ+namelen;
              break;
            }
          }
        }
        targ += len+1;
      }
    }
  }
  return ret;
}

void o_rwl_run_at_task_exit(void * li) {
  p99_callback(li);
  free(li);
}

# define O_RWL_AT_TASK_EXIT P99_TSS_LOCAL(o_rwl_at_task_exit)

int orwl_at_task_exit(p99_callback_voidptr_func func, void* obj) {
  return !P99_CALLBACK_PUSH(&O_RWL_AT_TASK_EXIT, func, obj);
}

size_t o_rwl_operations = 1;

void orwl_operations_set(size_t op_amount) {
  o_rwl_operations = op_amount;
}
