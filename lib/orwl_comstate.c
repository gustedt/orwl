/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the author and copyright holder for this work is                           */
/* all rights reserved,  2010-2013 Jens Gustedt, INRIA, France                */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#include "orwl_comstate.h"

P99_INSTANTIATE(orwl_comstate*, orwl_comstate_init, orwl_comstate*, size_t, size_t, size_t, orwl_server*, bool, bool);
P99_INSTANTIATE(void, orwl_comstate_destroy, orwl_comstate*);

P99_CONST_FUNCTION
size_t
chunkSize(size_t id, size_t nt) {
  if (!id) return nt;
  else if (id >= nt) return 0;
  else {
    ++id;
    size_t id2 = id << 1;
    size_t id1 = id2 - 1u;
    return
      1
      + (id1 < nt ? chunkSize(id1, nt) : 0)
      + (id2 < nt ? chunkSize(id2, nt) : 0);
  }
}

orwl_comstate orwl_comstate_initializer(size_t loc, size_t r, size_t locations_amount, orwl_server* srv, bool all, bool one) {
  size_t const myloc = ORWL_MYLOC(srv->ab);
  size_t const nl = ORWL_NL(srv->ab);
  size_t const root = (r == SIZE_MAX) ? loc : r;
  size_t const mytid = myloc / locations_amount;
  size_t const location = mytid*locations_amount + loc;
  size_t const nt = nl / locations_amount;
  size_t const rt = root / locations_amount;
  size_t const pos = (mytid - rt + nt) % nt + 1u;
  size_t child = ((pos << 1) - 1u);
  size_t const children = (child < nt)
                          ? (((child + 1) < nt)
                             ? 2 : 1)
                          : 0;
  size_t const sizes[2] = {
    [0] = (children > 0 ? chunkSize(child, nt) : 0),
    [1] = (children > 1 ? chunkSize(child + 1, nt) : 0),
  };
  child = ((child + rt) % nt) * locations_amount + loc;
  size_t parent = (((pos >> 1) - 1u + rt + nt) % nt);
  parent = parent*locations_amount + loc;
  return
  (orwl_comstate const) {
    .location = location,
     .locations_amount = locations_amount,
      .root = root,
       .nl = nl,
        .child = child,
         .children = children,
    .sizes = {
      [0] = sizes[0],
      [1] = sizes[1],
    },
    .parent = parent,
     .srv = srv,
      .all = all,
       .one = one,
        .is_init = true,
         .here = { ORWL_HANDLE2_INITIALIZER, ORWL_HANDLE2_INITIALIZER, },
          .there = { ORWL_HANDLE2_INITIALIZER, ORWL_HANDLE2_INITIALIZER, },
           .pare = ORWL_HANDLE2_INITIALIZER,
  };
}


void orwl_comstate_disconnect(orwl_comstate* arg) {
  if (arg->one) {
    orwl_disconnect(&arg->here[0]);
    if (arg->children)
      orwl_disconnect(arg->there, arg->children);
  }
  if (arg->all) {
    if (arg->children)
      orwl_disconnect(&arg->here[1]);
    if (arg->root != arg->location)
      orwl_disconnect(&arg->pare);
  }
}


void orwl_comstate_insert(orwl_comstate* arg,
                          size_t size) {
  if (!arg || !size) P99_THROW(EINVAL);

  orwl_scale(size, arg->location);
  if (arg->one) {
    orwl_write_insert(&arg->here[0], arg->location, 0);
    for (unsigned i = 0; i < arg->children; ++i)
      orwl_read_insert(&arg->there[i], (arg->child + i*arg->locations_amount)%arg->nl, 1);
  }

  if (arg->all) {
    if (arg->children)
      orwl_write_insert(&arg->here[1], arg->location, 2);
    if (arg->root != arg->location) {
      orwl_read_insert(&arg->pare, arg->parent, 3);
    }
  }
}

void orwl_comstate_reset(orwl_comstate* arg, size_t size) {
  if (arg->one) {
    /* rescale our location to the new size */
    ORWL_SECTION(&arg->here[0]) orwl_handle2_truncate(&arg->here[0], size);
    /* go through one lock cycle to ensure that all mirrors see the new size */
    for (unsigned i = 0; i < arg->children; ++i)
      /* this will block until the data is available */
      ORWL_SECTION(&arg->there[i]);
  }
  if (arg->all) {
    if (arg->root != arg->location) { ORWL_SECTION(&arg->pare) { P99_NOP; } }
    if (arg->children) {
      ORWL_SECTION(&arg->here[1]) {
        if (!arg->one) orwl_handle2_truncate(&arg->here[1], size);
      }
    }
  }
}

void orwl_comstate_set(orwl_comstate* arg, size_t size) {
  if (arg->is_connected) orwl_comstate_reset(arg, size);
  else {
    orwl_comstate_insert(arg, size);
    arg->is_connected = true;
  }
}

void orwl_comstate_broadcast(orwl_comstate* arg) {
  void* data = arg->data;
  if (arg->root != arg->location) {
    ORWL_SECTION(&arg->pare) {
      size_t size = 0;
      void const* tval = orwl_handle2_read_map(&arg->pare, &size);
      memcpy(data, tval, size);
    }
  }
  if (arg->children) {
    ORWL_SECTION(&arg->here[1]) {
      size_t size = 0;
      void * hval = orwl_handle2_write_map(&arg->here[1], &size);
      /* Store the actual value that we know for know in our
         location. */
      memcpy(hval, data, size);
    }
  }
}

/* Recursively copies a result vector as we receive it into the order
   that corresponds to the numbering of tasks. Returns the total
   number of elements that have been copied recursively. */
static
size_t chunkCopyRec(unsigned char* tar, unsigned char* sou, size_t els, size_t pos, size_t ro, size_t nt) {
  /* Determine the real id that corresponds to this position relative
     to the root. */
  size_t mytid = (pos + ro) % nt;
  /* First copy the element that we have in hand. */
  memcpy(&tar[mytid*els], sou, els);
  sou += els;
  /* We already know that we copied one element. */
  size_t ret = 1;
  /* We need the position in a numbering scheme that starts a 1, so
     just add 1. */
  ++pos;
  /* The positions of the children, still relative to the root. */
  size_t pos2 = (pos << 1);
  /* pos2 is always even and strictly greater than 0. Therefore its
     predecessor is never negative.*/
  size_t pos1 = pos2 - 1u;
  /* Now determine if the children really exist. That is if the
     computed children are inside the permitted range. */
  if (pos1 < nt) {
    size_t s = chunkCopyRec(tar, sou, els, pos1, ro, nt);
    sou += (s*els);
    ret += s;
  }
  if (pos2 < nt) {
    size_t s = chunkCopyRec(tar, sou, els, pos2, ro, nt);
    sou += (s*els);
    ret += s;
  }
  return ret;
}

void orwl_comstate_gather(orwl_comstate* arg) {
  void* data = arg->data;
  size_t s = arg->el_size;
  typedef unsigned char elem[s];
  elem *dta = data;
  size_t id = arg->location /arg->locations_amount;
  size_t nt = arg->nl /arg->locations_amount;
  size_t ro = arg->root /arg->locations_amount;
  size_t lo = arg->location/arg->locations_amount;
  ORWL_SECTION(&arg->here[0]) {
    elem * hval = orwl_handle2_write_map(&arg->here[0]);
    /* Store the actual value that we know for know in our
       location. */
    memcpy(&hval[0], &dta[id], s);
    size_t pos = 1;
    for (unsigned i = 0; i < arg->children; ++i) {
      /* this will block until the data is available */
      ORWL_SECTION(&arg->there[i]) {
        /* This one is read only. */
        elem const* tval = orwl_handle2_read_map(&arg->there[i]);
        /* Accumulate the values */
        memcpy(&hval[pos], tval, sizeof(elem[arg->sizes[i]]));
        pos += arg->sizes[i];
      }
    }
    if (ro == lo) {
      chunkCopyRec((void*)dta, (void*)hval, sizeof(elem), 0, ro, nt);
    }
  }
}
