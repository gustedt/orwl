/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the authors and copyright holders for this work are as follows:            */
/* all rights reserved,  2011 Emmanuel Jeanvoine, INRIA, France               */
/* all rights reserved,  2011-2013 Jens Gustedt, INRIA, France                */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#include "orwl_time.h"
#include "p99_defarg.h"

P99_INSTANTIATE(struct timespec, seconds2timespec, double);
P99_INSTANTIATE(double, timespec2seconds, struct timespec);
P99_INSTANTIATE(struct timeval, seconds2timeval, double);
P99_INSTANTIATE(double, timeval2seconds, struct timeval);

P99_INSTANTIATE(struct timespec, useconds2timespec, uint64_t);
P99_INSTANTIATE(uint64_t, timespec2useconds, struct timespec);
P99_INSTANTIATE(struct timeval, useconds2timeval, uint64_t);
P99_INSTANTIATE(uint64_t, timeval2useconds, struct timeval);

P99_INSTANTIATE(struct timeval, timespec2timeval, struct timespec);
P99_INSTANTIATE(struct timespec, timeval2timespec, struct timeval);
P99_INSTANTIATE(double, useconds2seconds, uint64_t);
P99_INSTANTIATE(uint64_t, seconds2useconds, double t);

P99_INSTANTIATE(struct timespec, timespec_diff, struct timespec, struct timespec);
P99_INSTANTIATE(struct timespec, timespec_sum, struct timespec, struct timespec);
P99_INSTANTIATE(struct timespec*, timespec_minus, struct timespec*, struct timespec const*);
P99_INSTANTIATE(struct timespec*, timespec_add, struct timespec *, struct timespec const*);

char const* orwl_seconds2str(double sec, char tmp[static 32]) {
  if (sec > 0.0) {
    double dlog = log10(sec);
    long ilog = dlog;
    ilog += 3000;
    ilog -= ilog % 3;
    ilog -= 3000;
    double factor = pow(10.0, (double)ilog);
    double scal  = sec / factor;
    if (scal < 1.0) {
      ilog -= 3;
      scal *= ORWL_KILO;
    }
    char const* str = 0;
    switch (ilog) {
    case -24 : str = "y"; break;
    case -21 : str = "z"; break;
    case -18 : str = "a"; break;
    case -15 : str = "f"; break;
    case -12 : str = "p"; break;
    case -9 : str = "n"; break;
    case -6 : str = "u"; break;
    case -3 : str = "m"; break;
    case  0 : str = ""; break;
    case  3 : str = "k"; break;
    case  6 : str = "M"; break;
    case  9 : str = "G"; break;
    case 12 : str = "T"; break;
    case 15 : str = "P"; break;
    case 18 : str = "E"; break;
    case 21 : str = "Z"; break;
    case 24 : str = "Y"; break;
    }
    if (str) {
      sprintf(tmp, "%#7.3f%s", scal, str);
    } else {
      sprintf(tmp, "%#7.3fE%+ld", scal, ilog);
    }
  } else {
    sprintf(tmp, "%#7.3f", 0.0);
  }
  return tmp;
}

char const* orwl_bytes2str(uint64_t number, char tmp[static 32]) {
  if (number) {
    double dlog2 = log2(number);
    unsigned ilog = dlog2;
    ilog /= 10ul;
    ilog *= 10ul;
    double factor = pow(2.0, (double)ilog);
    double scal  = number / factor;
    char const* str = 0;
    switch (ilog) {
    case  0u : str = ""; break;
    case  10u : str = "Ki"; break;
    case  20u : str = "Mi"; break;
    case  30u : str = "Gi"; break;
    case  40u : str = "Ti"; break;
    case  50u : str = "Pi"; break;
    case  60u : str = "Ei"; break;
    case  70u : str = "Zi"; break;
    case  80u : str = "Yi"; break;
    }
    if (str) {
      sprintf(tmp, "%#7.3f %s", scal, str);
    } else {
      sprintf(tmp, "%#7.3f 2^%u", scal, ilog);
    }
  } else {
    sprintf(tmp, "%#7.3f", 0.0);
  }
  return tmp;
}


P99_INSTANTIATE(uint64_t, useconds);
P99_INSTANTIATE(double, seconds);

P99_INSTANTIATE(struct timespec, orwl_gettime);
P99_DEFINE_ONCE_CHAIN(orwl_gettime) {
}

void sleepfor(double t) {
  double start = seconds();
  while (t > 0.0) {
    struct timespec req = seconds2timespec(t);
    if (thrd_sleep(&req, 0) == thrd_success) return;
    double now = seconds();
    double passed = now - start;
    if (passed >= t) return;
    t -= passed;
    start = now;
  }
}

#if defined(O_RWL_BSD_TIMER_INTERFACES) || defined(DOXYGEN)
P99_INSTANTIATE(struct timeval const*, timeradd,
                struct timeval const*,
                struct timeval const*,
                struct timeval*);
P99_INSTANTIATE(struct timeval const*, timersub,
                struct timeval const*,
                struct timeval const*,
                struct timeval*);
P99_INSTANTIATE(void, timerclear, struct timeval*);
P99_INSTANTIATE(int, timerisset, struct timeval const*);
P99_INSTANTIATE(int, timercmp0, struct timeval const*);
P99_INSTANTIATE(int, timercmp, struct timeval const*, struct timeval const*);
#endif
