/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the authors and copyright holders for this work are as follows:            */
/* all rights reserved,  2016-2017 Farouk Mansouri, INRIA, France             */
/* all rights reserved,  2016-2017 Jens Gustedt, INRIA, France                */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#ifdef HAVE_HWLOC
#ifdef HAVE_TREEMATCH

#include "orwl_affinity.h"

ORWL_DEFINE_ENV(ORWL_AFFINITY);
ORWL_DEFINE_ENV(ORWL_AFFINITY_OVERSUBSCRIBE);
ORWL_DEFINE_ENV(ORWL_AFFINITY_BINDING);


/**
 ** @brief Dependencies vector
 **/
static o_rwl_dependency(*communications)[] = 0;

/**
 ** @brief Location sizes vector
 **/
static double* locations = 0;

/**
 ** @brief mutex to manage concurent writes on o_rw_locations
 **/
static mtx_t locations_lock;

static size_t core_nb;
static size_t loc_nb;
static size_t loc_node;
static size_t loc_node_offs;
static size_t loc_offs;
static size_t loc_task;
static size_t loc_total;
static size_t op_nb;
static size_t op_task;
static size_t task_nb;

static size_t hyper;

/**
 ** @brief Treematch affinity matrix
 **/
static tm_affinity_mat_t* affinity = 0;

/**
 ** @brief Treematch topology
 **/
static tm_topology_t* tm_topology = 0;

/**
 ** @brief Treematch affinity solution
 **/
static tm_solution_t* tm_solution = 0;

/**
 ** @brief Hwloc topology
 **/
static hwloc_topology_t hwloc_topology;

static int dependency_init(void) {
  P99_THROW_ASSERT(EINVAL, op_nb);
  communications = P99_CALLOC(o_rwl_dependency, op_nb * op_nb);

  if (loc_total) {
    locations = P99_CALLOC(double, loc_total);
  }

  if (mtx_init(&locations_lock, mtx_plain)) {
    report(1, "mutex locations init failed");
    return 1;
  }

  return 0;
}

static void dependency_print(void) {
  report(1, "Dependencies of task graph:");
  o_rwl_dependency(*com_mat)[op_nb] = communications;
  for (size_t i = 0; i < op_nb; i++) {
    for (size_t j = 0; j < op_nb; j++) {
      size_t nb = com_mat[i][j].nb;
      if (nb) {
        report(1, "%zu->%zu: dependency: %zu| location_ids:", i, j, nb);
        for (size_t s = 0; s < nb; s++)
          fprintf(stderr, "\t%zu,", com_mat[i][j].loc_ids[s]);
        fprintf(stderr, "\n");
      }
    }
  }
  report(1, "Locations size");
  for (size_t i = 0; i < orwl_nl; i++) {
    fprintf(stderr, "\t[%zu] = %g", i, locations[i]);
  }
  fputc('\n', stderr);
}


static int dependency_destroy(void) {
  mtx_destroy(&locations_lock);
  free(communications);
  free(locations);
  return 0;
}

/*
tm_topology_t* o_rwl_get_local_topo_from_hwloc(hwloc_topology_t topology)
{
  tm_topology_t *res = NULL;
  hwloc_obj_t *objs = NULL;
  unsigned topodepth,depth;
  int nb_nodes,i;

  // work on depth //
  topodepth = hwloc_topology_get_depth(topology);

  res                  = (tm_topology_t*)MALLOC(sizeof(tm_topology_t));
  res->nb_constraints  = 0;
  res->constraints     = NULL;
  res->nb_levels       = topodepth;
  res->node_id         = (int**)MALLOC(sizeof(int*)*res->nb_levels);
  res->node_rank       = (int**)MALLOC(sizeof(int*)*res->nb_levels);
  res->nb_nodes        = (size_t*)MALLOC(sizeof(size_t)*res->nb_levels);
  res->arity           = (int*)MALLOC(sizeof(int)*res->nb_levels);

  // Build TreeMatch topology //
  for( depth = 0 ; depth < topodepth ; depth++ ){
    nb_nodes = hwloc_get_nbobjs_by_depth(topology, depth);
    res->nb_nodes[depth] = nb_nodes;
    res->node_id[depth] = (int*)MALLOC(sizeof(int)*nb_nodes);
    res->node_rank[depth] = (int*)MALLOC(sizeof(int)*nb_nodes);

    objs = (hwloc_obj_t*)MALLOC(sizeof(hwloc_obj_t)*nb_nodes);
    objs[0] = hwloc_get_next_obj_by_depth(topology,depth,NULL);
    hwloc_get_closest_objs(topology,objs[0],objs+1,nb_nodes-1);
    res->arity[depth] = objs[0]->arity;

    if (depth == topodepth -1){
      res->nb_constraints = nb_nodes;
      res->nb_proc_units = nb_nodes;
    }
    // printf("%d:",res->arity[depth]); //

    // Build process id tab //
    for (i = 0; i < nb_nodes; i++){
      res->node_id[depth][i] = objs[i]->os_index;
      res->node_rank[depth][objs[i]->os_index] = i;
      // if(depth==topodepth-1) //
    }
    FREE(objs);
  }

  // printf("\n"); //
  return res;
}
*/

static int build_affinity(tm_affinity_mat_t** aff_mat) {
  P99_THROW_ASSERT(EINVAL, locations && communications);
  double** mat = P99_MALLOC(double * [op_nb]);
  o_rwl_dependency(*com_mat)[op_nb] = communications;
  for (size_t i = 0 ; i < op_nb ; i++)
    mat[i] = P99_CALLOC(double, op_nb);
  for (size_t i = 0; i < op_nb; i++) {
    for (size_t j = 0; j < op_nb; j++) {
      if (com_mat[i][j].nb) {
        for (size_t s = 0; s < com_mat[i][j].nb; s++) {
          // add 1 if there is a dependency, even if location_size is 0
          double increment = locations[com_mat[i][j].loc_ids[s]];
          mat[i][j] += increment ? increment : 1;
        }
      }
    }
  }
  if (op_task > 1) {
    report(orwl_verbose, "connect operations inside the same task");
    for (size_t i0 = 0; i0 < op_nb; i0 += op_task) {
      for (size_t i = i0; i < (i0 + op_task); i += 1) {
        for (size_t j = i0; j < (i0 + op_task); j += 1) {
          if (i != j) ++mat[i][j];
        }
      }
    }
  }
  if (orwl_verbose) {
    report(1, "Matrix of communication");
    for (size_t i = 0; i < op_nb; i++) {
      double sum = 0;
      for (size_t j = 0; j < op_nb; j++) {
        fprintf(stderr, "\t%g", mat[i][j]);
        sum += mat[i][j];
      }
      fprintf(stderr, "\t%g\n", sum);
    }
  }
  *aff_mat = tm_build_affinity_mat(mat, op_nb);
  dependency_destroy();
  return 0;
}


static int affinity_mapping(tm_topology_t**  topology, tm_affinity_mat_t** aff_mat) {
  P99_THROW_ASSERT(EINVAL, *aff_mat);
  *topology = tm_get_local_topology_with_hwloc();
  int levels = ((*topology)->nb_levels) - 1;
  core_nb = (*topology)->nb_nodes[levels];
  size_t cores = core_nb;
  // Decide if this execution has several processes on the same node.
  // This is the share in cores that we may use.
  double share = 1;
  if (loc_node) {
    share *= loc_nb;
    share /= loc_node;
  }
  // Decide if we have to constrain the set of cores that we use.
  char const* bind = ORWL_AFFINITY_BINDING();
  if (bind || share < 1 || (hyper > 1 && task_nb <= cores / hyper)) {
    // The constrained set of cores that we will use.
    int constraints[cores];
    if (bind) {
      size_t nmb = 0;
      char* pos;
      for (size_t i = 0; i < cores; ++i) {
        constraints[i] = strtol(bind, &pos, 0);
        if (bind == pos) {
          break;
        } else {
          bind = pos;
          ++nmb;
        }
      }
      cores = nmb;
    } else if (share < 1) {
      report(orwl_verbose, "proc share is %g", share);
      size_t core_offs = (cores * loc_node_offs) / loc_node;
      size_t core_proc = (cores * loc_nb) / loc_node;
      for (size_t i = 0; i < core_proc && i+core_offs < cores; ++i) {
        report(orwl_verbose, "chosing core %zu", core_offs + i);
        constraints[i] = core_offs + i;
      }
      cores = core_proc;
      hyper = 1;
    } else {
      cores /= hyper;
      for (size_t i = 0; i < cores; ++i) {
        constraints[i] = hyper * i;
      }
    }
    tm_topology_set_binding_constraints(constraints, cores, *topology);
  }
  // Now, check if we have enough cores. If not oversubscribe.
  char const* over = ORWL_AFFINITY_OVERSUBSCRIBE();
  if (over || (op_nb > cores)) {
    size_t ov = over
                ? strtoull(over)
                : (op_nb / cores) + !!(op_nb % cores);
    report(orwl_verbose, "oversubscribing with factor %zu", ov);
    if (ov > 1)
      tm_enable_oversubscribing(*topology, ov);
  }
  report(orwl_verbose, "Topo:");
  if (orwl_verbose) tm_display_topology(*topology);
  tm_tree_t* comm_tree = tm_build_tree_from_topology(*topology, *aff_mat, NULL, NULL);
  tm_solution = tm_compute_mapping(*topology, comm_tree);

  report(orwl_verbose, "Solution size: %zu", tm_solution->sigma_length);
  report(orwl_verbose, "Binding is:");
  for (size_t i = 0; i < tm_solution->sigma_length; i++)
    report(orwl_verbose, "operation %zu: core %d", i, tm_solution->sigma[i]);

  tm_free_tree(comm_tree);
  return 0;
}

static int op_bind(int id, hwloc_topology_t topology, bool auxiliary) {
  hwloc_obj_t obj;
  int depth = hwloc_topology_get_depth(topology);

  /* Get my core. */
  obj = hwloc_get_obj_by_depth(topology, depth - 1, id);
  if (obj) {
    if (!auxiliary || hyper < 2) {
      /* And try to bind ourself there. */
      int s = hwloc_set_cpubind(topology, obj->cpuset, HWLOC_CPUBIND_THREAD);
      if (orwl_verbose || s < 0) {
        size_t len = 3 + core_nb*CHAR_BIT*2;
        char str[len];
        hwloc_bitmap_snprintf(str, len, obj->cpuset);
        report(1, "binding %d to cpuset %s", id, str);
      }
      if (s == -1) P99_THROW_ERRNO;
    }
  } else {
    report(1, "No valid object for core id %d.", id);
    P99_THROW(EINVAL);
  }

  return 0;
}


static void init_hwloc_topo(void) {
  hwloc_topology_init(&hwloc_topology);  // initialization
#if HWLOC_API_VERSION < 0x20000
  hwloc_topology_ignore_all_keep_structure(hwloc_topology);
#else
  hwloc_topology_set_all_types_filter(hwloc_topology, HWLOC_TYPE_FILTER_KEEP_STRUCTURE);
#endif
  hwloc_topology_load(hwloc_topology);   // actual detection
  int cores = hwloc_get_nbobjs_by_type(hwloc_topology, HWLOC_OBJ_CORE);
  int pus = hwloc_get_nbobjs_by_type(hwloc_topology, HWLOC_OBJ_PU);
  hyper = (cores > 0 && pus > 0) ? pus / cores : 1;
  report(orwl_verbose, "we see %d/%d PUs/cores, hyperthreading factor %zu", pus, cores, hyper);
}

int orwl_affinity_init(size_t locations_task, size_t locations_total,
                       size_t locations_offset, size_t locations_local) {
  loc_total = locations_total;
  loc_task  = locations_task;
  op_task   = orwl_operations;
  loc_offs  = locations_offset;
  loc_nb    = locations_local;
  task_nb = locations_local / locations_task;
  op_nb   = (locations_local / locations_task) * op_task;
  if (orwl_verbose) {
    report(1, "Initialization of affinity data structures");
    report(1, "Total location number\t%zu", loc_total);
    report(1, "locations per task\t%zu", loc_task);
    report(1, "operations per task\t%zu", op_task);
    report(1, "process: location offset\t%zu", loc_offs);
    report(1, "process: locations\t%zu", loc_nb);
    report(1, "process: tasks\t%zu, operations\t%zu", task_nb, op_nb);
  }
  dependency_init();
  return 0;
}

int orwl_dependency_set(size_t loc_src, size_t loc_dst, size_t location_id) {
  P99_THROW_ASSERT(EINVAL, communications);
  o_rwl_dependency(*com_mat)[op_nb] = communications;
  size_t src = ((loc_src - loc_offs) / loc_task) * op_task;
  size_t dst = ((loc_dst - loc_offs) / loc_task) * op_task;
  if (src < op_nb && dst < op_nb) {
    size_t nb = atomic_fetch_add(&com_mat[src][dst].nb, 1);
    com_mat[src][dst].loc_ids[nb] = location_id;
  }
  return 0;
}

int orwl_locations_size_set(size_t location_id, size_t location_size) {
  P99_THROW_ASSERT(EINVAL, locations);
  P99_MUTUAL_EXCLUDE(locations_lock) {
    locations[location_id] = location_size;
  }
  return 0;
}

void orwl_affinity_destroy(void) {
  tm_free_affinity_mat(affinity);
  hwloc_topology_destroy(hwloc_topology);
  tm_free_solution(tm_solution);
  tm_free_topology(tm_topology);
}


int orwl_affinity_compute(size_t offset, size_t total) {
  loc_node_offs = offset;
  loc_node = total;
  char const* env = ORWL_AFFINITY();
  if (env) {
    int env_ = atoi(env);
    report(orwl_verbose, " ORWL_AFFINITY = %d", env_);
    if (0 < env && env_ <= 2) {
      if (orwl_verbose) dependency_print();
      build_affinity(&affinity); /// Build the treematch communication matrix (only the master thread do that) ///
      init_hwloc_topo();     /// init and load hwloc toplogy
      affinity_mapping(&tm_topology, &affinity); /// Map the tasks ///
      atexit(orwl_affinity_destroy);
    } else report(orwl_verbose, "Affinity disabled. Set ORWL_AFFINITY>0 to enable");
  } else report(orwl_verbose, " ORWL_AFFINITY env variable NULL");
  return 0;
}

int orwl_affinity_set(size_t loc_id, bool auxiliary) {
  //// Use HWLOC to map threads ////
  char const* env = ORWL_AFFINITY();
  if (env) {
    int env_ = atoi(env);
    report(orwl_verbose, "ORWL_AFFINITY = %d", env_);
    if (env_ == 1) {
      size_t op_id = ((loc_id - loc_offs) / loc_task) * op_task;
      if (tm_solution && hwloc_topology) {
        report(orwl_verbose, "task %zu -> core %d", op_id, tm_solution->sigma[op_id]);
        op_bind(tm_solution->sigma[op_id], hwloc_topology, auxiliary);
        return 0;
      } else {
        report(orwl_verbose, "Solution or Affinity_matrix NULL");
        return 1;
      }
    } else report(orwl_verbose, "Affinity disabled. Set ORWL_AFFINITY > 0 to enable");
  } else report(orwl_verbose, "ORWL_AFFINITY env variable NULL");
  return 0;
}

#endif
#endif
