/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the author and copyright holder for this work is                           */
/* all rights reserved,  2018 Jens Gustedt, INRIA, France                     */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#include <orwl_normal.h>


typedef double (*variate_t)(p99_seed seed[static 1]);

/// @brief Sampling a centralized normal distribution by using
/// Marsaglia's ziggurat method for sampling probability distributions
/// with continuous, decreasing density function.
///
/// For a distribution to fit into that model we have to provide the
/// density function. In addition we need the computed values for R,
/// the point where the tail of the distribution starts, and tail, a
/// function that implements the computation of the tail of the
/// distribution.
///
/// We do @b not follow Marsaglia's implementation directly, though.
///
/// - It juggles with somehow unprecise integers of unknown width,
///   in particular it seems to assume that @c long is exactly 32
///   bit wide.
/// - It mixes @c float and @c double for no obvious reason. Such
///   a special handling is clearly outdated nowadays, where all
///   implementations of real values use IEEE doubles.
/// - It uses a random generator with a static state variable.
///   This is not suitable in a parallel context where the state
///   should reside on the stack.
/// - That RG provdes only 32 bits @b and only has a period of
///   2^32. Too short for large simulations.
/// - It has a hidden dependency between the choice of the integer
///   for the patch and the result value that is computed.
///
/// We use the constants that Marsaglia provides for the
/// computation of the intervals. But then
/// - This is relatively generic, so this could be applied to other distributions, too.
///   Nevertheless the optimizer has all the chances to do its work.
/// - We use the 64 random bit generators as a base that P99 provides.
///
/// This implementation is extracted from the parXXL project.
/// Copyright (c) 2007-2008 Jens Gustedt, INRIA, France

enum {
  exponent = 7,
  steps = (1 << exponent),
};

/// @brief Auxiliary function to sample #orwl_normal0 in the tail, i.e
/// for large values.
static
double
tail(double u, p99_seed seed[static 1]);

/// @brief The interval endpoints.
static
double const X[steps];

/// @brief The values of the #density at the interval endpoints #X.
static
double const F[steps];

/// @brief The difference of subsequent density values #F.
static
double const D[steps];

/// @brief The quotient of subsequent interval endpoints #X.
static
double const Q[steps];

static
int const exponent1 = exponent + 1;

static
size_t const steps1 = steps - 1;

static
double
value(double u, size_t i);

static
bool
inRect(double u, size_t i);

static
bool
reject(p99_seed seed[static 1], double x, size_t i);

static
bool symmetric = true;

static
double density(double x) {
  return exp(-0.5 * x * x);
}

/// @brief The right-most interval point, the start of the tail of
/// the distribution.
#define R 3.442619855899



#define ZIGRAND(u, i, s)                                       \
  do {                                                         \
    u = p99_drand(seed);                                       \
    if (symmetric) {                                           \
      u = ldexp(u, exponent1);                                 \
      i = u;                                                   \
      u -= i;                                                  \
      s = i % 2;                                               \
      i >>= 1;                                                 \
    } else {                                                   \
      u = ldexp(u, exponent);                                  \
      i = u;                                                   \
      u -= i;                                                  \
    }                                                          \
  } while (false)

inline
double
value(double u, size_t i) {
  double ret = X[i] * u;
  return ret;
}

bool
inRect(double u, size_t i) {
  return u < Q[i];
}

bool
reject(p99_seed seed[static 1],
       double x,
       size_t i) {
  return (orwl_normal0(seed)*D[i] >= (density(x) - F[i]));
}

double
orwl_normal0(p99_seed seed[static 1]) {
  size_t i;
  double u;
  double x;
  bool s = false;
  do {
    ZIGRAND(u, i, s);
    if (i < steps1) {
      x = value(u, i);
      // For the top most strip, i == 0, the rectangular part is
      // empty, we only have to test for he part under the curve.
      // For the other cases we have the possibility of a cheap
      // accept.
      if (i && inRect(u, i)) break;
    } else {
      // The bottom strip. Either we have a cheap accept or we
      // have to do a more expensive computation for the tail.
      x = (inRect(u, i) ? value(u, i) : tail(u, seed));
      break;
    }
  } while (reject(seed, x, i));
  if (symmetric)
    // Use the sign
    return s ? -x : x;
  else
    return x;
}

double
tail(double u, p99_seed seed[static 1]) {
  static
    double const r1 = 1.0 / R;
  static
    double const r2 = r1 * r1;
  double x;
  double y;
  do {
    x = -log(p99_drand(seed));
    y = ldexp(-log(p99_drand(seed)), 1);
  } while (y < x * x * r2);
  double ret = fma(x, r1, R);
  // The intial value is just used to obtain a sign.
  return (u < 0.5) ? ret : -ret;
}

static
double const X[128] = {
  0X1.16DB47E193D17P-2,
  0X1.73949184DB93AP-2,
  0X1.B4C8FECE48E02P-2,
  0X1.E8E576E43FB83P-2,
  0X1.0A936DA5E557FP-1,
  0X1.1E0CE6B59698AP-1,
  0X1.2F98D6BB4F3F9P-1,
  0X1.3FABEE1911CB4P-1,
  0X1.4E94C08C0BA97P-1,
  0X1.5C8AFDBF0215CP-1,
  0X1.69B7B213F3F4CP-1,
  0X1.763A1600EEC58P-1,
  0X1.822A858AF0E62P-1,
  0X1.8D9C6A9D35E22P-1,
  0X1.989F85C753B12P-1,
  0X1.A340D1BAF5AFEP-1,
  0X1.AD8B2506A1364P-1,
  0X1.B787A7C516F22P-1,
  0X1.C13E2B014E845P-1,
  0X1.CAB56AC6A38BBP-1,
  0X1.D3F340DDA6105P-1,
  0X1.DCFCCC51C59D9P-1,
  0X1.E5D6909F51B52P-1,
  0X1.EE848E9568258P-1,
  0X1.F70A5866C8F31P-1,
  0X1.FF6B21FFFE304P-1,
  0X1.03D4E7391C5ADP+0,
  0X1.07E47D87A40EDP+0,
  0X1.0BE58456FF4A5P+0,
  0X1.0FD911B97F22EP+0,
  0X1.13C024B2C7EBFP+0,
  0X1.179BA80463FE6P+0,
  0X1.1B6C7492C972FP+0,
  0X1.1F335374A10F2P+0,
  0X1.22F0FFBAA1E4FP+0,
  0X1.26A627FB9D11AP+0,
  0X1.2A536FAE30E2EP+0,
  0X1.2DF97057E7EF6P+0,
  0X1.3198BA982D90CP+0,
  0X1.3531D7146A439P+0,
  0X1.38C54749B902FP+0,
  0X1.3C538647EF78EP+0,
  0X1.3FDD09591D2A1P+0,
  0X1.436240982AD99P+0,
  0X1.46E39778DE05FP+0,
  0X1.4A617543306C9P+0,
  0X1.4DDC3D83A5B8P+0,
  0X1.515450720F451P+0,
  0X1.54CA0B4FFD345P+0,
  0X1.583DC8BFF3215P+0,
  0X1.5BAFE11654813P+0,
  0X1.5F20AAA4DFC16P+0,
  0X1.62907A0176EBBP+0,
  0X1.65FFA248E0169P+0,
  0X1.696E755E16B81P+0,
  0X1.6CDD4426B88A3P+0,
  0X1.704C5EC50CB7EP+0,
  0X1.73BC14D01A2C7P+0,
  0X1.772CB58A39DD4P+0,
  0X1.7A9E90168B8ECP+0,
  0X1.7E11F3ADAEB9P+0,
  0X1.81872FD21DB71P+0,
  0X1.84FE9484873B6P+0,
  0X1.88787278810A3P+0,
  0X1.8BF51B49EF334P+0,
  0X1.8F74E1B37C6B5P+0,
  0X1.92F819C682BF2P+0,
  0X1.967F1924C7B03P+0,
  0X1.9A0A373C73F1EP+0,
  0X1.9D99CD86B58B2P+0,
  0X1.A12E37C983368P+0,
  0X1.A4C7D45D01A3P+0,
  0X1.A867047516E4FP+0,
  0X1.AC0C2C6FC6382P+0,
  0X1.AFB7B428FE7A1P+0,
  0X1.B36A075498D64P+0,
  0X1.B72395DF5B73BP+0,
  0X1.BAE4D457EE119P+0,
  0X1.BEAE3C60CD0E4P+0,
  0X1.C2804D2C6B16FP+0,
  0X1.C65B8C04DBAC2P+0,
  0X1.CA4084E091E34P+0,
  0X1.CE2FCB05F8C34P+0,
  0X1.D229F9BFEEFDBP+0,
  0X1.D62FB52580B86P+0,
  0X1.DA41AAF79A344P+0,
  0X1.DE609397E09B9P+0,
  0X1.E28D331C6723CP+0,
  0X1.E6C85A849B015P+0,
  0X1.EB12E91486BBCP+0,
  0X1.EF6DCDDC7D392P+0,
  0X1.F3DA097460823P+0,
  0X1.F858AFF31CBFP+0,
  0X1.FCEAEB2CA5F17P+0,
  0X1.00C8FEA1720D4P+1,
  0X1.0327A1CC4CF5EP+1,
  0X1.05921D1C4D769P+1,
  0X1.08093FE3E40E1P+1,
  0X1.0A8DED0EC371AP+1,
  0X1.0D211DD28B00FP+1,
  0X1.0FC3E4D95F278P+1,
  0X1.12777201834F3P+1,
  0X1.153D16D45743DP+1,
  0X1.18164BE0C1C39P+1,
  0X1.1B04B731F6BCCP+1,
  0X1.1E0A342CF08F6P+1,
  0X1.2128DD36BDF09P+1,
  0X1.246317A6B53CP+1,
  0X1.27BBA2B5DBC92P+1,
  0X1.2B35AA5EBEE3EP+1,
  0X1.2ED4DF8099571P+1,
  0X1.329D9725E32F7P+1,
  0X1.3694F3A3740D9P+1,
  0X1.3AC11B8E206D6P+1,
  0X1.3F29848D3B416P+1,
  0X1.43D75B60BCA1DP+1,
  0X1.48D61806D601P+1,
  0X1.4E3456B0E3A1BP+1,
  0X1.54052012A04A4P+1,
  0X1.5A61EDF7E8F32P+1,
  0X1.616DFF7C8F54AP+1,
  0X1.695C2BE68EDC9P+1,
  0X1.7279DD4AC3F9DP+1,
  0X1.7D45EB36EB842P+1,
  0X1.8AA73E440FFBCP+1,
  0X1.9C8E0C7C8098FP+1,
  0X1.B8A7C476D2BE8P+1,
  0X1.DB4668FE7E4A4P+1,
};

static
double const F[128] = {
  0X1.ED5CF060D53DDP-1,
  0X1.DF6071934C0B2P-1,
  0X1.D37A74FFB7E58P-1,
  0X1.C8D923F9E0685P-1,
  0X1.BF19B6810E617P-1,
  0X1.B6042CF903CC9P-1,
  0X1.AD750B7255A2BP-1,
  0X1.A55418110D2B1P-1,
  0X1.9D8FDFAEC7BFBP-1,
  0X1.961B4C1AFE58BP-1,
  0X1.8EEC3C5BBFB44P-1,
  0X1.87FAA61A739F6P-1,
  0X1.814005219CC7DP-1,
  0X1.7AB6F9C656C23P-1,
  0X1.745B04D027F2BP-1,
  0X1.6E2856A006C23P-1,
  0X1.681BAB4EBDC26P-1,
  0X1.62322FC593A68P-1,
  0X1.5C696D348E88FP-1,
  0X1.56BF39249A244P-1,
  0X1.5131A8EFE6187P-1,
  0X1.4BBF07C6C218BP-1,
  0X1.4665CEA500FCP-1,
  0X1.41249DC646453P-1,
  0X1.3BFA374538795P-1,
  0X1.36E57AA69826FP-1,
  0X1.31E5612065D08P-1,
  0X1.2CF8FA78591CP-1,
  0X1.281F6A5D24475P-1,
  0X1.2357E62428F93P-1,
  0X1.1EA1B2D9EFCBEP-1,
  0X1.19FC239747FB3P-1,
  0X1.1566980FB8BB2P-1,
  0X1.10E07B5015E59P-1,
  0X1.0C6942A5BBCACP-1,
  0X1.08006CA84DDE6P-1,
  0X1.03A58060E6682P-1,
  0X1.FEB0191503B12P-2,
  0X1.F62F4DD0454A9P-2,
  0X1.EDC7D75B7711P-2,
  0X1.E578F9F2C9375P-2,
  0X1.DD4204B582987P-2,
  0X1.D52250CD9B951P-2,
  0X1.CD1940AD1B149P-2,
  0X1.C5263F5E989C9P-2,
  0X1.BD48BFE6A41E7P-2,
  0X1.B5803CB422F26P-2,
  0X1.ADCC371DF416EP-2,
  0X1.A62C36EC664E3P-2,
  0X1.9E9FC9ED3AD13P-2,
  0X1.97268391186BFP-2,
  0X1.8FBFFC9176155P-2,
  0X1.886BD29E2263P-2,
  0X1.8129A811A7658P-2,
  0X1.79F923ABE117BP-2,
  0X1.72D9F0523036BP-2,
  0X1.6BCBBCD4C4729P-2,
  0X1.64CE3BB887D8DP-2,
  0X1.5DE12305426ECP-2,
  0X1.57042C17986DCP-2,
  0X1.503713768FB43P-2,
  0X1.497998AC51EA5P-2,
  0X1.42CB7E21E8C57P-2,
  0X1.3C2C88FDB8DD6P-2,
  0X1.359C810485CBDP-2,
  0X1.2F1B307CCFE9FP-2,
  0X1.28A8641461084P-2,
  0X1.2243EAC7E206DP-2,
  0X1.1BED95CC57525P-2,
  0X1.15A5387A66038P-2,
  0X1.0F6AA83B46CF9P-2,
  0X1.093DBC774F1A1P-2,
  0X1.031E4E85FB6AP-2,
  0X1.FA18733ED2788P-3,
  0X1.EE0EB59E61862P-3,
  0X1.E21F21D12332EP-3,
  0X1.D64978F7CF9D6P-3,
  0X1.CA8D7F9AC2022P-3,
  0X1.BEEAFD99D711P-3,
  0X1.B361BE1EB801BP-3,
  0X1.A7F18F918FB5CP-3,
  0X1.9C9A43902C0F3P-3,
  0X1.915BAEE792BFP-3,
  0X1.8635A99016373P-3,
  0X1.7B280EABFD4BAP-3,
  0X1.7032BC88D676BP-3,
  0X1.655594A396D55P-3,
  0X1.5A907BAFACE61P-3,
  0X1.4FE359A138233P-3,
  0X1.454E19BAA0E71P-3,
  0X1.3AD0AA9DD7FA4P-3,
  0X1.306AFE6193143P-3,
  0X1.261D0AAAEBE72P-3,
  0X1.1BE6C8CBDA97P-3,
  0X1.11C835E71B72AP-3,
  0X1.07C1531A2B49CP-3,
  0X1.FBA44B5C4DE8DP-4,
  0X1.E7F56EA105FBCP-4,
  0X1.D4762CA983A56P-4,
  0X1.C126AC0117764P-4,
  0X1.AE071DC7AF28EP-4,
  0X1.9B17BE7E63EEEP-4,
  0X1.8858D6F54FF2EP-4,
  0X1.75CABD60E5DBBP-4,
  0X1.636DD69E8C212P-4,
  0X1.514297B239A5DP-4,
  0X1.3F4987896AD6EP-4,
  0X1.2D8341133A33BP-4,
  0X1.1BF075C20AA01P-4,
  0X1.0A91F09183C3P-4,
  0X1.F2D13368BD128P-5,
  0X1.D0EAF63395868P-5,
  0X1.AF738C17A5015P-5,
  0X1.8E6DB483BC1BAP-5,
  0X1.6DDC9DD1FE249P-5,
  0X1.4DC3FCBD997P-5,
  0X1.2E282B724ADADP-5,
  0X1.0F0E539C89B77P-5,
  0X1.E0F951D57E233P-6,
  0X1.A4F57A25D9CC1P-6,
  0X1.6A23FA9D5F274P-6,
  0X1.309CEE4E09981P-6,
  0X1.F100847645162P-7,
  0X1.83F4BED19339CP-7,
  0X1.1A9B6B3FC1939P-7,
  0X1.6BA8B0FFB627FP-8,
  0X1.5DE9E33726F2P-9,
  0X1P+0,
};

static
double const D[128] = {
  0X1.2A30F9F2AC23P-5,
  0X1.BF8FD9B12656P-6,
  0X1.7CBF927284B4P-6,
  0X1.542A20BAEFA6P-6,
  0X1.37EDAF1A40DCP-6,
  0X1.22B13101529CP-6,
  0X1.11E430D5C53CP-6,
  0X1.041E6C290EF4P-6,
  0X1.F10E18915AD8P-7,
  0X1.DD24E4F259CP-7,
  0X1.CBC3EFCFA91CP-7,
  0X1.BC6590530538P-7,
  0X1.AEA83E35B5E4P-7,
  0X1.A242D6D18168P-7,
  0X1.96FD3D8BB3EP-7,
  0X1.8CAB8C084C2P-7,
  0X1.832AD4523FF4P-7,
  0X1.7A5EE24A86F8P-7,
  0X1.7230A4414764P-7,
  0X1.6A8D03FD192CP-7,
  0X1.63640D2D02F4P-7,
  0X1.5CA84A48FFFP-7,
  0X1.564E4870472CP-7,
  0X1.504C37AEADB4P-7,
  0X1.4A99A04372F8P-7,
  0X1.452F27A81498P-7,
  0X1.4006618C959CP-7,
  0X1.3B19AA032D2P-7,
  0X1.366406CD352CP-7,
  0X1.31E10E3ED388P-7,
  0X1.2D8CD28E4B54P-7,
  0X1.2963D0A9F42CP-7,
  0X1.2562E1E3D004P-7,
  0X1.21872FE8B564P-7,
  0X1.1DCE2A9686B4P-7,
  0X1.1A357F5B7B18P-7,
  0X1.16BB11D9DD9P-7,
  0X1.135CF59923E4P-7,
  0X1.10196897CCD2P-7,
  0X1.0CEECE99C732P-7,
  0X1.09DBAD15BB36P-7,
  0X1.06DEA7A8D3DCP-7,
  0X1.03F67CFCE06CP-7,
  0X1.01220410101P-7,
  0X1.FCC053A09EP-8,
  0X1.F75FDDFD1F88P-8,
  0X1.F220CCA04B04P-8,
  0X1.ED01658BB6EP-8,
  0X1.E8000C63722CP-8,
  0X1.E31B3FCADF4P-8,
  0X1.DE519708995P-8,
  0X1.D9A1BFE895A8P-8,
  0X1.D50A7CD4EC94P-8,
  0X1.D08AA31EBF6P-8,
  0X1.CC2119719374P-8,
  0X1.C7CCD66C384P-8,
  0X1.C38CDF5AF108P-8,
  0X1.BF60470F267P-8,
  0X1.BB462CD15A84P-8,
  0X1.B73DBB6A804P-8,
  0X1.B34628422E64P-8,
  0X1.AF5EB28F7278P-8,
  0X1.AB86A29A4938P-8,
  0X1.A7BD490BFA04P-8,
  0X1.A401FE4CC464P-8,
  0X1.A05421ED7878P-8,
  0X1.9CB31A1BB86CP-8,
  0X1.991E531FC05CP-8,
  0X1.95953EE2AD2P-8,
  0X1.9217547C53B4P-8,
  0X1.8EA40FC7CCFCP-8,
  0X1.8B3AF0FDED6P-8,
  0X1.87DB7C54EC04P-8,
  0X1.848539A48B7P-8,
  0X1.8137B40E1E4CP-8,
  0X1.7DF279A7CA68P-8,
  0X1.7AB51B2A72BP-8,
  0X1.777F2BA1B368P-8,
  0X1.7450401D5E24P-8,
  0X1.7127EF63E1EAP-8,
  0X1.6E05D1A5097EP-8,
  0X1.6AE9802C74D2P-8,
  0X1.67D295132A06P-8,
  0X1.64C0AAEF90FAP-8,
  0X1.61B35C831D72P-8,
  0X1.5EAA4464DA9EP-8,
  0X1.5BA4FCA7F42CP-8,
  0X1.58A31E7D3DE8P-8,
  0X1.55A441CE985CP-8,
  0X1.52A7FCD2E784P-8,
  0X1.4FADE3991D9AP-8,
  0X1.4CB587889CC2P-8,
  0X1.49BE76D4E5A2P-8,
  0X1.46C83BE22A04P-8,
  0X1.43D25C97E48CP-8,
  0X1.40DC599E051CP-8,
  0X1.3DE5AD808AABP-8,
  0X1.3AEDCBB47ED1P-8,
  0X1.37F41F782566P-8,
  0X1.34F80A86C2F2P-8,
  0X1.31F8E39684D6P-8,
  0X1.2EF5F494B3AP-8,
  0X1.2BEE78913FCP-8,
  0X1.28E19946A173P-8,
  0X1.25CE6C259BA9P-8,
  0X1.22B3EEC527B5P-8,
  0X1.1F91028CECEFP-8,
  0X1.1C6467630A33P-8,
  0X1.192CB512F93AP-8,
  0X1.15E853086DD1P-8,
  0X1.12956DD2539CP-8,
  0X1.0F31E9A93C6P-8,
  0X1.0BBB50DF84298P-8,
  0X1.082EBC9F472D8P-8,
  0X1.0488B58DEFB88P-8,
  0X1.00C508A325A48P-8,
  0X1.F9BD14B4E953P-9,
  0X1.F19D7D5C1236P-9,
  0X1.E91AAB1CAA5D8P-9,
  0X1.E01EBD7D22B9P-9,
  0X1.D68BFC43D5268P-9,
  0X1.CC38627AAC798P-9,
  0X1.C0E560973868P-9,
  0X1.B42F1692C7718P-9,
  0X1.A5654E474698CP-9,
  0X1.931C4AFF99FE6P-9,
  0X1.79677EC8455DEP-9,
  0.0,
};

static
double const Q[128] = {
  0X0.0000000000000P+0,
  0X1.803C6D4F93A8FP-1,
  0X1.B3911E9B804FDP-1,
  0X1.C96D1A883D2E4P-1,
  0X1.D58014742E52EP-1,
  0X1.DD2487ADCB4D5P-1,
  0X1.E26896F5FBF3FP-1,
  0X1.E641170F50CA7P-1,
  0X1.E92F39746C221P-1,
  0X1.EB7D8A7CCD9EAP-1,
  0X1.ED5A0A98BC7C9P-1,
  0X1.EEE2A3186B513P-1,
  0X1.F02B9C88C7351P-1,
  0X1.F143339D7D787P-1,
  0X1.F233B16D764D8P-1,
  0X1.F304B35B5D59P-1,
  0X1.F3BBFB4B67D5EP-1,
  0X1.F45DF82CD25B9P-1,
  0X1.F4EE220C3043DP-1,
  0X1.F56F39B2B0507P-1,
  0X1.F5E37591F6CCDP-1,
  0X1.F64CA218DBB21P-1,
  0X1.F6AC395F78BD5P-1,
  0X1.F70374C1451AAP-1,
  0X1.F7535A22E3D3DP-1,
  0X1.F79CC61506B24P-1,
  0X1.F7E073A948FEP-1,
  0X1.F81F028FC2ADEP-1,
  0X1.F858FBE99F8ADP-1,
  0X1.F88ED61F8E777P-1,
  0X1.F8C0F7F61E36DP-1,
  0X1.F8EFBB0B5013DP-1,
  0X1.F91B6DDDF8427P-1,
  0X1.F9445577B49F4P-1,
  0X1.F96AAECC7E5E7P-1,
  0X1.F98EAFDE8E73BP-1,
  0X1.F9B088B20FF65P-1,
  0X1.F9D06419A6A63P-1,
  0X1.F9EE6862EE1B5P-1,
  0X1.FA0AB7E8A2982P-1,
  0X1.FA25718F03B33P-1,
  0X1.FA3EB12E1F177P-1,
  0X1.FA568FECFF9B7P-1,
  0X1.FA6D24902FE34P-1,
  0X1.FA8283BD8F44DP-1,
  0X1.FA96C0371D81AP-1,
  0X1.FAA9EB0E19352P-1,
  0X1.FABC13CF91F8FP-1,
  0X1.FACD48AB5F4E1P-1,
  0X1.FADD96964622EP-1,
  0X1.FAED0967F6925P-1,
  0X1.FAFBABF570E44P-1,
  0X1.FB0988284AC3DP-1,
  0X1.FB16A7133B4F5P-1,
  0X1.FB23110445452P-1,
  0X1.FB2ECD94C9BA1P-1,
  0X1.FB39E3B7C2E56P-1,
  0X1.FB4459C65D654P-1,
  0X1.FB4E358B1E8DP-1,
  0X1.FB577C4BBFA39P-1,
  0X1.FB6032D1E043P-1,
  0X1.FB685D72AD163P-1,
  0X1.FB70001593E7AP-1,
  0X1.FB771E3A1A365P-1,
  0X1.FB7DBAFCE8335P-1,
  0X1.FB83D91C1719AP-1,
  0X1.FB897AFACF29CP-1,
  0X1.FB8EA2A43F27AP-1,
  0X1.FB9351CDF4F98P-1,
  0X1.FB9789D99CEC8P-1,
  0X1.FB9B4BD62B197P-1,
  0X1.FB9E9880706ABP-1,
  0X1.FBA170431AC57P-1,
  0X1.FBA3D3361DD1BP-1,
  0X1.FBA5C11D7FBA4P-1,
  0X1.FBA7396782FC8P-1,
  0X1.FBA83B2A23E8EP-1,
  0X1.FBA8C51FDDB9DP-1,
  0X1.FBA8D5A3A81CBP-1,
  0X1.FBA86AAC1A8C1P-1,
  0X1.FBA781C59EDC5P-1,
  0X1.FBA6180B97B6P-1,
  0X1.FBA42A205A48CP-1,
  0X1.FBA1B423D4107P-1,
  0X1.FB9EB1A8ADE0CP-1,
  0X1.FB9B1DA7B43FCP-1,
  0X1.FB96F271420E9P-1,
  0X1.FB92299C5D1EP-1,
  0X1.FB8CBBF324034P-1,
  0X1.FB86A15C1886FP-1,
  0X1.FB7FD0BFB9735P-1,
  0X1.FB783FE9C00DP-1,
  0X1.FB6FE3652F8B4P-1,
  0X1.FB66AE52354DBP-1,
  0X1.FB5C92349C858P-1,
  0X1.FB517EB94BD58P-1,
  0X1.FB456170E2019P-1,
  0X1.FB38257D095FFP-1,
  0X1.FB29B32D77103P-1,
  0X1.FB19EF88B6409P-1,
  0X1.FB08BBBBC73BCP-1,
  0X1.FAF5F46A249P-1,
  0X1.FAE170D5CADC4P-1,
  0X1.FACB01D4366F8P-1,
  0X1.FAB27081A26DCP-1,
  0X1.FA977C9EC13D6P-1,
  0X1.FA79DA7E004A6P-1,
  0X1.FA59305B35722P-1,
  0X1.FA3512E9CB952P-1,
  0X1.FA0D00CFBB6CDP-1,
  0X1.F9E05CA2EFDC3P-1,
  0X1.F9AE64CCB1F64P-1,
  0X1.F97628687C107P-1,
  0X1.F93677B627E76P-1,
  0X1.F8EDCDE8CDE13P-1,
  0X1.F89A30BCAA7BBP-1,
  0X1.F838FFD4EC0EAP-1,
  0X1.F7C6A977E305FP-1,
  0X1.F73E31C89895DP-1,
  0X1.F69868793C53P-1,
  0X1.F5CA83EF26E1FP-1,
  0X1.F4C3825DE9F38P-1,
  0X1.F366D2AFAEE48P-1,
  0X1.F1803C6A0781BP-1,
  0X1.EEA42F70CEEACP-1,
  0X1.E9C885D9A666BP-1,
  0X1.DF5993967D2A6P-1,
  0X1.DAB48848D3C15P-1,
};

double orwl_normal(p99_seed seed[static 1], double mean, double sdev);
