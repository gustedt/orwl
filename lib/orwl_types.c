/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the author and copyright holder for this work is                           */
/* all rights reserved,  2010-2013 Jens Gustedt, INRIA, France                */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
/**
 ** @file orwl_types.c
 **
 ** @brief Collect the type information in one big meta register
 **/


#include "orwl_wait_queue.h"
#include "orwl_proc.h"
#include P99_ADVANCE_ID

DEFINE_ORWL_TYPES(orwl_proc) {
  orwl_register_init(ORWL_FTAB(orwl_types));
  for (size_t i = 0; ; ++i) {
    void *R = orwl_register_get(ORWL_FTAB(orwl_types) + i);
    if (!R) break;
    orwl_register_init(*(orwl_register**)R);
  }
}
