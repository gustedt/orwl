/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the authors and copyright holders for this work are as follows:            */
/* all rights reserved,  2011 Emmanuel Jeanvoine, INRIA, France               */
/* all rights reserved,  2011-2014 Jens Gustedt, INRIA, France                */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
/* For the compilation of the calibration code, ORWL_TIMER must always
   trigger. */
#undef ORWL_USE_TIMING
#define ORWL_USE_TIMING 1

#include "orwl.h"
#include "p99_defarg.h"
#include "p99_new.h"
#include "p99_qsort.h"

ORWL_DEFINE_ENV(ORWL_TIMING_TYPE);
ORWL_DEFINE_ENV(ORWL_TIMING_CALIBRATE);

P99_INSTANTIATE(void, orwl_stat_add, orwl_stat *, size_t, double);

static
P99_PURE_FUNCTION
size_t
name_size(orwl_timing_element* el) {
  size_t ret = 2 + (el->p00_name ? strlen(el->p00_name) : 0);
  return ret + (el->p00_up ? name_size(el->p00_up) : 0);
}

static
char*
name_compose(orwl_timing_element* el, char * name) {
  size_t ret = (el->p00_name ? strlen(el->p00_name) :0);
  char * start = name;
  if (el->p00_up) {
    start = name_compose(el->p00_up, start);
    start[0] = '>';
    start[1] = '\0';
    ++start;
  }
  if (ret) memcpy(start, el->p00_name, ret+1);
  return start + ret;
}

P99_DECLARE_ENUM(pel_type,
                 pel_def,
                 human,
                 extended);

P99_DEFINE_ENUM(pel_type);

static
void orwl_timing_element_print(FILE *out, orwl_timing_element* el, int namelen, int nblen, pel_type t) {
  P99_SPIN_EXCLUDE(&el->p00_stats.p00_cat) {
    uint64_t nb = el->p00_stats.p00_nb;
    if (nb) {
      double time = el->p00_stats.p00_time;
      double mu = time/nb;
      double time2 = el->p00_stats.p00_time2;
      double var = (time2 - (time * mu)) / nb;
      uint64_t count = el->p00_stats.p00_cnt;
      double min = el->p00_stats.p00_min;
      double max = el->p00_stats.p00_max;
      if (var < 0.0) var = 0.0;
      char name[name_size(el)];
      name_compose(el, name);
      switch (t) {
      case human:
        fprintf(out,
                "TIMING: %-*s\t%*"PRIu64"\t%-8ss\t%-8ss\t%-8ss\t%-8s\t%-8s%s\n",
                namelen,
                name,
                nblen,
                nb,
                orwl_seconds2str(time),
                orwl_seconds2str(mu),
                orwl_seconds2str(sqrt(var)),
                count ? orwl_bytes2str(count) : "",
                (count && (time > 0.0)) ? orwl_seconds2str(count/time) : "",
                count ? "/s" : ""
               );
        break;
      case extended:;
        fprintf(out,
                "TIMING: %-*s\t%*"PRIu64"\t%24.12E\t%24.12E\t%24.12E\t%24.12E\t%24.12E\t%24"PRIu64"\t%24.12E\t%24.12E\n",
                namelen,
                name,
                nblen,
                nb,
                time,
                min,
                mu,
                max,
                sqrt(var),
                count,
                count/time,
                count/max
               );
        break;
      default:;
        fprintf(out,
                "TIMING: %-*s\t%*"PRIu64"\t%24.12E\t%24.12E\t%24.12E\t%24"PRIu64"\t%24.12E\n",
                namelen,
                name,
                nblen,
                nb,
                time,
                mu,
                sqrt(var),
                count,
                count/time
               );
        break;
      }
    }
  }
}

/* Insert the predefined elements in the lifo. This is done statically
   at compile time. Therefore we create and initialize a table with
   these elements. The initialization is done such that each element
   points to its predecessor in the table. The last element of the
   table is then inserted in the global lifo. */

#define O_RWL_TIMING_ELEMENT_STATIC(NAME, X, I)                \
  [I] = {                                                      \
    .p00_stats = ORWL_STAT_INTIALIZER,                         \
    .p00_name = P99_STRINGIFY(X),                              \
    .p99_lifo = (I) ? &(NAME)[I-1] : 0,                        \
    .p00_up = (I) ? &(NAME)[0] : 0,                            \
  }

static orwl_timing_element timing_elements_static[P99_NARG(ORWL_TIMING_LIST)] = {
  P99_FOR(timing_elements_static, P99_NARG(ORWL_TIMING_LIST), P00_SEQ, O_RWL_TIMING_ELEMENT_STATIC, ORWL_TIMING_LIST),
};

#define HEAD_INITIAL (&timing_elements_static[P99_NARG(ORWL_TIMING_LIST)-1])

static P99_LIFO(orwl_timing_element_ptr) head  = P99_LIFO_INITIALIZER(HEAD_INITIAL);

static
once_flag timing_print_guard;

static
void timing_print_atexit(void);

/* If we do any timing we will also time the total time of the
   execution.  Additionally this point of measure will also record the
   number of bytes that were received during the whole run. */
static struct timespec  o_rwl_timing_start;
_Atomic(size_t) o_rwl_timing_recv = ATOMIC_VAR_INIT(0);
_Atomic(int) o_rwl_timing_fd = ATOMIC_VAR_INIT(0);

void orwl_timing_print_stats(void) {
  trace(orwl_verbose, "executing timing");
  call_once(&timing_print_guard, timing_print_atexit);
}

P99_DECLARE_ONCE_CHAIN(orwl_smalloc);

P99_DEFINE_ONCE_CHAIN(orwl_timing, orwl_smalloc) {
  trace(orwl_verbose, "inserting timing");
  /* Start the overall timing. This only triggers at the beginning of
     an execution when the first ORWL_TIMER is encountered. */
  o_rwl_timing_start = orwl_gettime();
  atexit(orwl_timing_print_stats);
}

once_flag o_rwl_timing_print_installer;

void orwl_timing_element_insert(void* el) {
  P99_INIT_CHAIN(orwl_timing);
  orwl_timing_element* (*elp)[3] = el;
  orwl_timing_element * el0 = (*elp)[0];
  orwl_timing_element * p00_t_prev = (*elp)[1];
  orwl_timing_element * el1 = (*elp)[2];
  P99_LIFO_PUSH(&head, el0);
  el0->p00_up = (p00_t_prev ? p00_t_prev : el1);
}

/* Now link the table elements to the named elements that are
   accessible globally. */

#define O_RWL_TIMING_ELEMENT(NAME, X, I) .X = &(NAME)[I]

static orwl_timing timing_info = {
  P99_FOR(timing_elements_static, P99_NARG(ORWL_TIMING_LIST), P00_SEQ, O_RWL_TIMING_ELEMENT, ORWL_TIMING_LIST),
};

P99_CONST_FUNCTION
orwl_timing * orwl_timing_info(void) {
  return &timing_info;
}


static
P99_PURE_FUNCTION
int o_rwl_cmp_timing(void const*a, void const* b, void*context) {
  orwl_timing_element const*const* A0 = a;
  orwl_timing_element const*const* B0 = b;
  orwl_timing_element const* A = *A0;
  orwl_timing_element const* B = *B0;
  char const* Ast = A->p00_name;
  char const* Bst = B->p00_name;
  if (!Ast) return 1;
  if (!Bst) return -1;
  /* We don't use strcmp since this gives non satisfactory results for
     special characters such as '_" or '/' */
  while (*Ast && *Bst && *Ast == *Bst) {
    ++Ast; ++Bst;
  }
  char Ac = *Ast;
  char Bc = *Bst;
  /* this correction only works for the execution char set ASSII */
  if (Ac > 'A' && !isalpha(Ac)) Ac &= 0x3F;
  if (Bc > 'A' && !isalpha(Bc)) Bc &= 0x3F;
  return Ac - Bc;
}

size_t volatile o_rwl_cal_inc = 1;
size_t volatile o_rwl_cal_zero = 0;
size_t volatile o_rwl_cal_res = 0;
double volatile o_rwl_cal_dres = 0.0;

#define O_RWL_CAL_LOOP(NAME, TYPE, CNT, BOUND, STATEMENT, RES, ...) \
ORWL_TIMER(NAME) {                                                  \
  TYPE inc = o_rwl_cal_inc;                                         \
  __VA_ARGS__ ;                                                     \
  CNT = o_rwl_cal_zero - inc;                                       \
  while (P99_LIKELY((CNT += inc) < (BOUND))) STATEMENT;             \
  o_rwl_cal_dres = (double)(RES);                                   \
}                                                                   \
P99_MACRO_END(O_RWL_CAL_LOOP, NAME)

#define O_RWL_CALIBRATE(NAME, TYPE, CNT, BOUND, STATEMENT, RES, ...)                   \
O_RWL_CAL_LOOP(NAME, TYPE, CNT, BOUND, STATEMENT, RES, __VA_ARGS__);                   \
O_RWL_CAL_LOOP(P99_PASTE2(2, NAME), TYPE, CNT, (2*BOUND), STATEMENT, RES, __VA_ARGS__)

/**
 ** @brief Calibrate the different parts of the run.
 **
 ** This is not meant to be called by user code, but called
 ** automatically at the end of a ORWL run from the timing routines.
 **
 ** It measures
 **  - the time for 1000 increments in memory
 **  - the time for 1000 increments in registers
 **  - the time for calls to ::ORWL_TIMER
 **
 ** These are then taken into account by the time analyzer script.
 **/
static
void orwl_calibrate(void) {
  ORWL_TIMER() {
    P99_CRITICAL {
      static size_t volatile icnt;
      static double volatile dcnt;
      static _Atomic(size_t) acnt;
      // don't change the name of the timers
      ORWL_TIMER(memory) {
        O_RWL_CALIBRATE(MIinc, size_t, icnt, SIZE_C(1000000),, icnt, P99_NOP);
        O_RWL_CALIBRATE(MFinc, double, dcnt, 1000000.0,, dcnt, P99_NOP);
        O_RWL_CALIBRATE(MAinc, size_t, icnt, SIZE_C(1000000), atomic_fetch_add(&acnt, inc), icnt,
        register size_t icnt; atomic_store(&acnt, (o_rwl_cal_zero - inc)));
      }
    }
    P99_CRITICAL {
      // don't change the name of the timers
      ORWL_TIMER(instruction) {
        O_RWL_CALIBRATE(MIinc, size_t, icnt, SIZE_C(1000000),, icnt, register size_t icnt);
        O_RWL_CALIBRATE(MFinc, double, dcnt, 1000000.0,, dcnt, register double dcnt);
        O_RWL_CALIBRATE(MFmult, size_t, icnt, SIZE_C(1000000), dcnt *= dmult, dcnt,
        register size_t icnt;
        register double dmult = 2.0 * o_rwl_cal_inc, dcnt = o_rwl_cal_zero - dmult);
        O_RWL_CALIBRATE(MFadd, size_t, icnt, SIZE_C(1000000), dcnt += dinc, dcnt,
        register size_t icnt;
        register double dinc = o_rwl_cal_inc, dcnt = o_rwl_cal_zero - dinc);
        O_RWL_CALIBRATE(Msqrt, size_t, icnt, SIZE_C(1000000), sqrt(dcnt += dinc), dcnt,
        register size_t icnt;
        register double dinc = o_rwl_cal_inc, dcnt = o_rwl_cal_zero + dinc);
      }
    }
    // don't change the name of the timers
    ORWL_TIMER(timer1000) {
      for (size_t cnt = 0; cnt < 1000; ++cnt)
        ORWL_TIMER(timer0)
        ORWL_TIMER(timer1)
        ORWL_TIMER(timer2)
        ORWL_TIMER(timer3);
    }
  }
}

#define O_RWL_TIMING_ACCOUNT(NAME, COUNT)                      \
  orwl_stat_add(&(info->NAME->p00_stats),                      \
                (COUNT),                                       \
                total_time)


static
void timing_print_atexit(void) {
  /* Assure once that all stores are visible and then proceed with
     relaxed memory constraints. */
  atomic_thread_fence(memory_order_seq_cst);
  /* Stop the total timing and register the amount of data that has
     been transferred. */
  double const total_time = timespec2seconds(timespec_diff(o_rwl_timing_start, orwl_gettime()));
  orwl_timing* info = orwl_timing_info();
  O_RWL_TIMING_ACCOUNT(_orwl, 0);
  O_RWL_TIMING_ACCOUNT(recv, atomic_load_explicit(&o_rwl_timing_recv, memory_order_relaxed));
  O_RWL_TIMING_ACCOUNT(fd, atomic_load_explicit(&o_rwl_timing_fd, memory_order_relaxed));
  size_t count[3];
  orwl_thrd_count(count);
  O_RWL_TIMING_ACCOUNT(callback, count[1]);
  O_RWL_TIMING_ACCOUNT(thread, count[2]);
  if (P99_LIFO_TOP(&head) == HEAD_INITIAL) return;
  int namelen = 12;
  int nblen = 2;
  if (ORWL_TIMING_CALIBRATE()) orwl_calibrate();
  P99_LIFO_TABULATE(orwl_timing_element, table, &head);
  P99_ASORT(table, o_rwl_cmp_timing, 0);
  for (size_t i = 0; i < P99_ALEN(table); ++i) {
    size_t el_nb = table[i]->p00_stats.p00_nb;
    if (el_nb && table[i]->p00_name) {
      size_t l = name_size(table[i]);
      if (l > namelen) namelen = l;
      size_t lg = (size_t)log10(el_nb);
      if (lg > nblen) nblen = lg;
    }
  }
  ++nblen;
  char const* var = ORWL_TIMING_TYPE();
  pel_type t = (var ? pel_type_parse(var) : pel_def);
  int slen =  24;
  switch (t) {
  case extended:
    fprintf(stderr,
            "TIMING: %-*s\t%*s\t%*s\t%*s\t%*s\t%*s\t%*s\t%*s\t%*s\t%*s\n",
            namelen,
            "#pt of measure",
            nblen,
            "n",
            slen,
            "time",
            slen,
            "min",
            slen,
            "time/n",
            slen,
            "max",
            slen,
            "dev",
            slen,
            "count",
            slen,
            "count/time",
            slen,
            "count/max"
           );
    break;
  case human:
    slen =  9;
  default:
    fprintf(stderr,
            "TIMING: %-*s\t%*s\t%*s\t%*s\t%*s\t%*s\t%*s\n",
            namelen,
            "#pt of measure",
            nblen,
            "n",
            slen,
            "time",
            slen,
            "time/n",
            slen,
            "dev",
            slen,
            "count",
            slen,
            "count/time"
           );
  }
  for (size_t i = 0; i < P99_ALEN(table); ++i)
    orwl_timing_element_print(stderr, table[i], namelen, nblen, t);
}
