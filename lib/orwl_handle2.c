/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the authors and copyright holders for this work are as follows:            */
/* all rights reserved,  2011 Emmanuel Jeanvoine, INRIA, France               */
/* all rights reserved,  2010-2013, 2016 Jens Gustedt, INRIA, France          */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#include "orwl_server.h"

P99_INSTANTIATE(orwl_handle2*, orwl_handle2_init, orwl_handle2*);
P99_INSTANTIATE(void, orwl_handle2_destroy, orwl_handle2*);

DEFINE_NEW_DELETE(orwl_handle2);

static
orwl_mirror* mirror_location(orwl_handle2* rh2) {
  orwl_mirror* ret = 0;
  if (rh2) {
    bool par = (rh2->clock % 2);
    ret = rh2->pair[par].rq;
  }
  return ret;
}

static
void o_rwl_new_request2(orwl_mirror* location, orwl_handle2* rh2, p99_seed* seed, bool par) {
  if (rh2->state[par] != orwl_requested)
    rh2->state[par] = rh2->inclusive
                      ? orwl_handle_read_request(location, &rh2->pair[par], 1, seed)
                      : orwl_handle_write_request(location, &rh2->pair[par], 1, seed);
}

static
orwl_state o_rwl_request2(orwl_mirror* location, orwl_handle2* rh2, p99_seed* seed, bool inclusive) {
  rh2->state[0] = orwl_invalid;
  rh2->state[1] = orwl_invalid;
  bool par = (rh2->clock % 2);
  if (!mirror_location(rh2)) {
    rh2->inclusive = inclusive;
    o_rwl_new_request2(location, rh2, seed, par);
  }
  return rh2->state[par];
}

orwl_state orwl_handle2_write_request(orwl_mirror location[], orwl_handle2 rh2[], size_t size, p99_seed* seed) {
  orwl_state ret = orwl_invalid;
  if (location && rh2) {
    for (size_t i = 0; i < size; ++i) {
      ret = o_rwl_request2(location + i, rh2 + i, seed, false);
      if (ret != orwl_requested) break;
    }
  }
  return ret;
}



orwl_state orwl_handle2_read_request(orwl_mirror location[], orwl_handle2 rh2[], size_t size, p99_seed* seed) {
  orwl_state ret = orwl_invalid;
  if (location && rh2) {
    for (size_t i = 0; i < size; ++i)
      ORWL_TIMER(body) {
      ret = o_rwl_request2(location + i, rh2 + i, seed, true);
      if (ret != orwl_requested) break;
    }
  }
  return ret;
}

static
void o_rwl_request2_grouped(orwl_handle2 * rh2,
                            size_t locid,
                            size_t priority,
                            p99_seed* seed,         /*!< [in,out] defaults to a thread local seed */
                            orwl_server * srv,
                            orwl_mirror * location,
                            bool inclusive) {
  rh2->state[0] = orwl_invalid;
  rh2->state[1] = orwl_invalid;
  bool par = (rh2->clock % 2);
  if (!mirror_location(rh2)) {
    rh2->inclusive = inclusive;
    if (rh2->state[par] != orwl_requested) {
      if (rh2->inclusive)
        orwl_handle_read_insert(&rh2->pair[par], locid, priority, seed, srv, location);
      else
        orwl_handle_write_insert(&rh2->pair[par], locid, priority, seed, srv, location);
    }
    rh2->state[par] = orwl_requested;
  }
}

void orwl_handle2_write_insert(orwl_handle2 * rh2,
                               size_t locid,
                               size_t priority,
                               p99_seed* seed,
                               orwl_server * srv,
                               orwl_mirror * location
                              ) {
  if (locid == SIZE_MAX) locid = ORWL_MYLOC(srv->ab);
  if (priority == SIZE_MAX) priority = ORWL_MYLOC(srv->ab);
  if (rh2)
    o_rwl_request2_grouped(rh2, locid, priority, seed, srv, location, false);
}

void orwl_handle2_read_insert(orwl_handle2 * rh2,
                              size_t locid,
                              size_t priority,
                              p99_seed* seed,
                              orwl_server * srv,
                              orwl_mirror * location
                             ) {
  if (priority == SIZE_MAX) priority = ORWL_MYLOC(srv->ab);
  if (rh2)
    o_rwl_request2_grouped(rh2, locid, priority, seed, srv, location, true);
}

orwl_state orwl_handle2_next(orwl_handle2 rh0[], size_t size, p99_seed* seed)  {
  orwl_state ret = orwl_state_amount;
  for (size_t i = 0; i < size; ++i)
    ORWL_TIMER(body) {
    orwl_handle2* rh2 = rh0 + i;
    bool par = (rh2->clock % 2);
    orwl_handle2_acquire(rh2, 1, seed);
    if (rh2->state[par] == orwl_acquired) {
      rh2->state[par] =  mirror_location(rh2)
                         ? orwl_handle_release(&rh2->pair[par], 1u, seed)
                         : orwl_invalid;
    }
    ++(rh2->clock);
    ret = (rh2->state[par] < ret ? rh2->state[par] : ret);
  }
  return ret;
}

orwl_state orwl_handle2_acquire(orwl_handle2 rh0[], size_t size, p99_seed* seed) {
  orwl_state ret = orwl_state_amount;
  for (size_t i = 0; ((ret != orwl_invalid) && (i < size)); ++i)
    ORWL_TIMER(body) {
    orwl_handle2* rh2 = rh0 + i;
    bool par = (rh2->clock % 2);
    orwl_state *const state = rh2->state;
    orwl_mirror *const location = mirror_location(rh2);
    switch (state[par]) {
    case orwl_requested:
      state[par] = location
                   ? orwl_handle_acquire(&rh2->pair[par], 1)
                   : orwl_invalid;
    case orwl_acquired: break;
    /* otherwise leave the timer in a controlled way */
    default: state[par] = ret = orwl_invalid;
    }
    if (state[par] == orwl_acquired) {
      o_rwl_new_request2(location, rh2, seed, !par);
      if (state[!par] != orwl_requested) state[par] = orwl_invalid;
    }
    ret = (state[par] < ret ? state[par] : ret);
  }
  return ret;
}

orwl_state orwl_handle2_disconnect(orwl_handle2 rh0[], size_t size, p99_seed* seed) {
  orwl_state ret = orwl_state_amount;
  for (size_t i = 0; i < size; ++i)
    ORWL_TIMER(body) {
    orwl_handle2* rh2 = rh0 + i;
    orwl_state *const state = rh2->state;
    orwl_handle *const pair = rh2->pair;
    /* Make sure to start with the active of the pair. */
    for (unsigned par0 = 0; par0 < 2; ++par0) {
      unsigned par = (rh2->clock+par0) % 2;
      if (pair[par].rq) {
        switch (orwl_handle_test(&pair[par], 1u, seed)) {
        case orwl_requested:
          orwl_handle_acquire(&pair[par], 1u);
        case orwl_acquired:
          orwl_handle_release(&pair[par], 1u, seed);
        default: ;
        }
      }
      ret = (state[par] < ret ? state[par] : ret);
    }
  }
  return ret;
}

orwl_state orwl_handle2_release(orwl_handle2 rh0[], size_t size, p99_seed* seed) {
  orwl_state ret = orwl_state_amount;
  for (size_t i = 0; i < size; ++i)
    ORWL_TIMER(body) {
    orwl_handle2* rh2 = rh0 + i;
    bool par = (rh2->clock % 2);
    rh2->state[par] =  mirror_location(rh2)
                       ? orwl_handle_release(&rh2->pair[par], 1u, seed)
                       : orwl_invalid;
    ret = (rh2->state[par] < ret ? rh2->state[par] : ret);
  }
  return ret;
}

orwl_state orwl_handle2_test(orwl_handle2 rh0[], size_t size, p99_seed* seed) {
  orwl_state ret = orwl_state_amount;
  for (size_t i = 0; i < size; ++i)
    ORWL_TIMER(body) {
    orwl_handle2* rh2 = rh0 + i;
    bool par = (rh2->clock % 2);
    rh2->state[par] =  mirror_location(rh2)
                       ? orwl_handle_test(&rh2->pair[par], 1u, seed)
                       : orwl_invalid;
    ret = (rh2->state[par] < ret ? rh2->state[par] : ret);
  }
  return ret;
}

void* orwl_handle2_write_map(orwl_handle2* rh2, size_t* data_len, p99_seed* seed) {
  void* ret = 0;
  ORWL_TIMER()
  if (orwl_handle2_acquire(rh2, 1u, seed) == orwl_acquired) {
    bool par = (rh2->clock % 2);
    ret = orwl_handle_write_map(&rh2->pair[par], data_len, seed);
  } else {
    if (data_len) *data_len = 0;
  }
  return ret;
}

void* orwl_handle2_read_map(orwl_handle2* rh2, size_t* data_len, p99_seed* seed) {
  void* ret = 0;
  ORWL_TIMER()
  if (orwl_handle2_acquire(rh2, 1u, seed) == orwl_acquired) {
    bool par = (rh2->clock % 2);
    ret = orwl_handle_read_map(&rh2->pair[par], data_len, seed);
  } else {
    if (data_len) *data_len = 0;
  }
  return ret;
}

size_t orwl_handle2_offset(orwl_handle2* rh2, p99_seed* seed) {
  size_t ret = 0;
  ORWL_TIMER()
  if (orwl_handle2_acquire(rh2, 1u, seed) == orwl_acquired) {
    bool par = (rh2->clock % 2);
    ret = orwl_handle_offset(&rh2->pair[par], seed);
  }
  return ret;
}


void orwl_handle2_truncate(orwl_handle2* rh2, size_t data_len, p99_seed* seed) {
  ORWL_TIMER()
  if (orwl_handle2_acquire(rh2, 1u, seed) == orwl_acquired) {
    bool par = (rh2->clock % 2);
    orwl_handle_truncate(&rh2->pair[par], data_len, seed);
  }
}

void orwl_handle2_replace(orwl_handle2* rh2, orwl_alloc_ref *alloc, p99_seed* seed) {
  ORWL_TIMER()
  if (orwl_handle2_test(rh2, 1u, seed) == orwl_acquired) {
    bool par = (rh2->clock % 2);
    orwl_handle_replace(&rh2->pair[par], alloc, seed);
  }
}

void orwl_handle2_hold(orwl_handle2* rh2, orwl_alloc_ref* ar, p99_seed* seed, bool mark) {
  ORWL_TIMER()
  if (orwl_handle2_test(rh2, 1u, seed) == orwl_acquired) {
    bool par = (rh2->clock % 2);
    orwl_handle_hold(&rh2->pair[par], ar, seed, mark);
  }
}
