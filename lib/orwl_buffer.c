/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the author and copyright holder for this work is                           */
/* all rights reserved,  2011-2013 Jens Gustedt, INRIA, France                */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#include "orwl_buffer.h"
#include "orwl_server.h"

P99_INSTANTIATE(orwl_buffer*, orwl_buffer_init, orwl_buffer *, size_t, uint64_t*);
P99_INSTANTIATE(void, orwl_buffer_destroy, orwl_buffer*);
P99_INSTANTIATE(void, orwl_buffer_advance, orwl_buffer *buf, ssize_t res);
P99_INSTANTIATE(iovec*, iovec_init, iovec *, size_t, uint64_t*);
P99_INSTANTIATE(void, iovec_destroy, iovec*);
P99_INSTANTIATE(iovec, orwl_buffer2iovec, orwl_buffer volatile const* buf);
P99_INSTANTIATE(orwl_buffer, iovec2buffer, iovec iovec);
P99_INSTANTIATE(void, iovec_advance, iovec *, ssize_t);

DEFINE_NEW_DELETE(orwl_buffer);

void orwl_buffer_close(size_t n, orwl_buffer mess[n]) {
  size_t found = 0;
  int maxfd = -1;
  for (size_t i = 0; i < n; ++i) {
    if (mess[i].aio.aio_fildes > maxfd) {
      ++found;
      maxfd = mess[i].aio.aio_fildes;
    }
  }
  switch (found) {
  /* All had been closed already. */
  case 0: break;
  /* The normal case is that we use just one file descriptor. */
  case 1:
    orwl_socket_sig_close(maxfd);
    for (size_t i = 0; i < n; ++i)
      mess[i].aio.aio_fildes = -1;
    break;
  /* Otherwise we don't know if there have been duplicates in the
     set. Keep track of all descriptors that we touch. */
  default: {
    bool *const seen = P99_CALLOC(bool, maxfd + 1);
    for (size_t i = 0; i < n; ++i) {
      if (!seen[mess[i].aio.aio_fildes]) {
        orwl_socket_sig_close(mess[i].aio.aio_fildes);
        seen[mess[i].aio.aio_fildes] = true;
      }
      mess[i].aio.aio_fildes = -1;
      mess[i].aio.aio_lio_opcode = LIO_NOP;
    }
    free(seen);
  }
  }
}

static
size_t aio_complete(size_t n, struct aiocb* aiocb_list[n], int mode) {
  size_t completed = 0;
  while (completed < n) {
    // reset in case we had to start over
    completed = 0;
    for (size_t i = 0; i < n; ++i) {
      if (aiocb_list[i]) {
RETRY:;
        int err = aio_error(aiocb_list[i]);
        switch (err) {
        case 0:              // the request completed successfully.
          ++completed;
          aiocb_list[i] = 0;
          break;
        case EINPROGRESS:;   // the request has not been completed yet.
          break;
        case EAGAIN:;
#if EAGAIN != EWOULDBLOCK
        case EWOULDBLOCK:;
#endif
          if (lio_listio(LIO_NOWAIT, aiocb_list+i, 1, 0)) {
            // still not going, give use a break;
            sleepfor(1E-6);
            goto RETRY;
          }
          break;
        default: P99_THROW(err);
        }
      } else {
        ++completed; // the request completed successfully
      }
    }
    if (mode == LIO_WAIT)
      while (completed < n) {
        if (!aio_suspend((struct aiocb const*const*)aiocb_list, n, 0)) {
          ++completed;
        } else {
          sleepfor(1E-6);
        }
      } else break;
  }
  return completed;
}

void orwl_buffer_complete(size_t n, orwl_buffer mess[n]) {
  struct aiocb* aiocb_list[n];
  for (size_t i = 0; i < n; ++i) {
    aiocb_list[i] = &mess[i].aio;
  }
  aio_complete(n, aiocb_list, LIO_WAIT);
}

size_t orwl_buffer_prepare(int fd, orwl_buffer* mess, bool out, size_t offset) {
  /* Ensure that there are no pending requests on the buffer. */
  orwl_buffer_complete(1, mess);
  orwl_buffer_close(1, mess);
  /* Register all requests without erasing the aio_nbytes and aio_buf
     field. */
  int const opcode = out ? LIO_WRITE : LIO_READ;
  mess->aio.aio_fildes = fd;
  mess->aio.aio_offset = offset;
  mess->aio.aio_sigevent = P99_LVAL(struct sigevent);
  mess->aio.aio_lio_opcode = opcode;
  return offset + mess->aio.aio_nbytes;
}

size_t orwl_buffer_io(size_t n, orwl_buffer mess[n], bool blocking) {
  int error = 0;
  /* Only test against the dynamic limit if n is larger than the
     minimum value for the max. */
#if _POSIX_AIO_LISTIO_MAX > 0
  if (n > _POSIX_AIO_LISTIO_MAX)
#endif
#if _SC_AIO_LISTIO_MAX >= 0
  {
    long const aio_listio_max = P99_SC_AIO_LISTIO_MAX();
    if (0 < aio_listio_max && aio_listio_max < n) P99_THROW(EINVAL);
  }
#endif
  /* Pass the requests to lio_listio. The normal case should be that
     this is just one context switch and the error paths are only
     chosen under congestion. */
  struct aiocb* aiocb_list[n];
  for (size_t i = 0; i < n; ++i) {
    aiocb_list[i] = &mess[i].aio;
  }
  int const mode = blocking ? LIO_WAIT : LIO_NOWAIT;
  size_t ret = blocking ? n : 0;
  if (lio_listio(mode, aiocb_list, n, 0)) {
    switch (errno) {
    case EINVAL: /* mode invalid, or n exceeds the limit
                    AIO_LISTIO_MAX, should never be reached. */
      P99_THROW(EINVAL);
      break;
    case EAGAIN:; /* Out of resources or AIO_MAX operations
                     exceeded. Try again. */
    case EINTR:;  /* mode was LIO_WAIT and a signal was caught before
                     all I/O operations completed.  (This may even be
                     one of the signals used for asynchronous I/O
                     completion notification.) . */
      ret = aio_complete(n, aiocb_list, mode);
      break;
    default:      /* EIO One of more of the operations specified by
                     aiocb_list failed.  The application can check the
                     status of each operation using aio_return(3). */
      trace(1, "errno is %s", p99_errno_getname(errno));
      for (size_t i = 0; i < n; ++i) {
        int err = aio_error(aiocb_list[i]);
        trace(1, "buf %zu/%zu on fd %d: status %s",
              i, n, aiocb_list[i]->aio_fildes,
              err ? p99_errno_getname(err) : "completed");
        if (err) error = err;
      }
      if (error) P99_THROW(error);
      break;
    }
  }
  return ret;
}
