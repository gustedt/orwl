/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the authors and copyright holders for this work are as follows:            */
/* all rights reserved,  2011 Emmanuel Jeanvoine, INRIA, France               */
/* all rights reserved,  2010-2014 Jens Gustedt, INRIA, France                */
/* all rights reserved,  2011 Matias E. Vara, INRIA, France                   */
/* all rights reserved,  2013 Rodrigo Campos, INRIA, France                   */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#include "orwl_socket.h"
#include "p99_atomic.h"
#include "orwl_server.h"

P99_DEFINE_ENUM(orwl_state);

P99_DEFINE_ONCE_CHAIN(orwl_wq, orwl_thread) {
}

orwl_wq* orwl_wq_init(orwl_wq *wq,
                      int attr) {
  if (!wq) return 0;
  memset(wq, 0, sizeof *wq);
  P99_THROW_CALL_THRD(mtx_init, &wq->mut, attr);
  p99_rwl_init(&wq->rwlock);
  (void)orwl_wh_ref_init(&wq->head, (void*)0);
  (void)orwl_wh_ref_init(&wq->tail, (void*)0);
  (void)orwl_alloc_ref_init(&wq->allocr, (void*)0);
  wq->clock = 1;
  wq->borrowed = false;
  return wq;
}

void orwl_wq_destroy(orwl_wq *wq) {
  register size_t const max_tries = 1000;
  if (!wq) return;
  orwl_wh* wq_head = orwl_wh_ref_get(&wq->head);
  for(size_t tries = 1; wq_head; ++tries) {
    P99_WRLOCK(wq->rwlock) {
      P99_MUTUAL_EXCLUDE(wq->mut) {
        if (wq_head && p99_count_value(&wq_head->tokens)) {
          /* Waiting for wq to be emptied. Once it has become empty the
             thread that has to release the head will be waiting for the
             lock that we are holding. */
          p99_count_wait(&wq_head->tokens);
        }
      }
    }
    /* The release operation may take some time if there is data push
       to be performed. So we leave the processor to those who need
       this. */
    sched_yield();
    REPORT(!(tries % 10), "wait queue not empty when trying to destroy it. %s tries so far.", tries);
    if (tries > max_tries) P99_THROW(EDEADLK);
    wq_head = orwl_wh_ref_get(&wq->head);
  }
  orwl_alloc_ref_destroy(&wq->allocr);
  mtx_destroy(&wq->mut);
  p99_rwl_destroy(&wq->rwlock);
  orwl_wq_init(wq);
}

DEFINE_NEW_DELETE(orwl_wq);

P99_DEFINE_ONCE_CHAIN(orwl_wh,
                      orwl_thread) {
}

orwl_wh* orwl_wh_init(orwl_wh *wh,
                      size_t tok,
                      orwl_wh *next) {
  if (!wh) return 0;
  memset(wh, 0, sizeof *wh);
  (void)orwl_wh_ref_init(&wh->next, next);
  atomic_init(&wh->p99_cnt, 0);
  p99_count_init(&wh->tokens, tok);
  p99_notifier_init(&wh->acq);
  return wh;
}

void orwl_wh_destroy(orwl_wh *wh) {
  size_t cnt = atomic_load_explicit(&wh->p99_cnt, memory_order_consume);
  trace(cnt, "destruction of wh %p with cnt %zu", wh, cnt);
  P99_THROW_ASSERT(EINVAL, !wh->location);
  P99_THROW_ASSERT(EINVAL, !orwl_wh_ref_get(&wh->next));
  p99_count_destroy(&wh->tokens);
  p99_notifier_destroy(&wh->acq);
  orwl_wh_init(wh, 0);
}

DEFINE_NEW_DELETE(orwl_wh);

DEFINE_NEW_DELETE(orwl_wh_ref);

P99_TP_REF_DEFINE(orwl_wh);

/* This supposes that wq != 0 */
P99_INSTANTIATE(int, orwl_wq_idle, orwl_wq*);

/* This supposes that the corresponding wq != 0 */
P99_INSTANTIATE(uint64_t, orwl_wh_load_conditionally, orwl_wh *wh, uint64_t howmuch);
/* This supposes that the corresponding wq != 0 */
P99_INSTANTIATE(uint64_t, orwl_wh_unload, orwl_wh *wh, uint64_t howmuch);


void
o_rwl_wh_trigger(orwl_wh *wh) {
  if (wh && !orwl_wh_unload(wh)) {
    orwl_wh_release(wh);
  }
}


void
orwl_wh_ref_trigger(orwl_wh_ref *whr) {
  if (whr) {
    orwl_wh* wh = P99_TP_XCHG(whr, 0);
    if (wh && !orwl_wh_unload(wh))
      orwl_wh_release(wh);
    orwl_wh_discount(wh);
  }
}


void o_rwl_wq_request_append_locked(orwl_wq *wq, orwl_wh *wh) {
  orwl_wh * wh1 = 0;
  for (orwl_wh* wh0 = wh; wh0; wh0 = orwl_wh_ref_get(&wh0->next)) {
    wh0->location = wq;
    wh1 = wh0;
  }
  for (;;) {
    P99_TP_STATE(orwl_wh_ptr) state_tail = P99_TP_STATE_INITIALIZER(&wq->tail, wh1);
    orwl_wh * wq_tail = P99_TP_STATE_GET(&state_tail);
    /* Find the last ptr in the existing list to append the first new element. */
    P99_TP_STATE(orwl_wh_ptr) state_last
      = P99_TP_STATE_INITIALIZER((wq_tail ? &wq_tail->next : &wq->head), wh);
    if (!P99_TP_STATE_GET(&state_last) && P99_TP_STATE_COMMIT(&state_last)) {
      orwl_wh_account(wh);
      /* Nobody can insert another element before we update the
         tail. */
      if (!P99_TP_STATE_COMMIT(&state_tail)) continue;
      orwl_wh_account(wh1);
      orwl_wh_discount(wq_tail);
      /* If this is the only element in the list, nobody would notify
         waiters that the lock is acquired. So we have to set the
         notifier here directly. */
      if (!wq_tail) p99_notifier_set(&wh->acq);
      break;
    }
  }
}

orwl_state orwl_wq_request(orwl_wq *wq, orwl_wh *wh) {
  if (wq && wh) {
    P99_MUTUAL_EXCLUDE(wq->mut) {
      o_rwl_wq_request_append_locked(wq, wh);
    }
  }
  return orwl_requested;
}

orwl_state o_rwl_wq_try_request_locked(orwl_wq *wq, orwl_wh_ref* whr) {

  P99_THROW_ASSERT(ENOLCK, whr);
  P99_THROW_ASSERT(EINVAL, !orwl_wh_ref_get(whr));
  if (P99_UNLIKELY(orwl_wq_idle(wq)))
    return orwl_invalid;
  orwl_state ret = orwl_invalid;

  /* if the wh is a null pointer, take this as a request to add to the
     last handle if it exists */
  P99_TP_STATE(orwl_wh_ptr) state_tail = P99_TP_STATE_INITIALIZER(&wq->tail, 0);
  register orwl_wh *wq_tail = P99_TP_STATE_GET(&state_tail);
  if (wq_tail
      // detect if this last element is a read handle
      && wq_tail->svrID) {
    // Detect if it is still in use or just being released.
    if (P99_LIKELY(orwl_wh_load_conditionally(wq_tail, 1))) {
      if (P99_LIKELY(P99_TP_STATE_CHECK(&state_tail))) {
        orwl_wh_ref_replace(whr, wq_tail);
        ret = orwl_requested;
      } else {
        /* something interrupted us better retry */
        o_rwl_wh_trigger(wq_tail);
        ret = orwl_again;
      }
    } else {
      P99_THROW(EINVAL);
    }
  }
  return ret;
}

orwl_state orwl_wh_acquire(orwl_wh *wh) {
  if (wh) {
    p99_notifier_block(&wh->acq);
  } else {
    P99_THROW(EINVAL);
  }
  return orwl_acquired;
}

orwl_state orwl_wh_test(orwl_wh *wh) {
  orwl_state ret = orwl_invalid;
  if (wh) {
    ret = (p99_notifier_load(&wh->acq)) ? orwl_acquired : orwl_requested;
  }
  return ret;
}

orwl_state orwl_wh_release(orwl_wh *wh) {
  orwl_state ret = orwl_invalid;
  ORWL_TIMER() {
    P99_THROW_ASSERT(ENOLCK, wh);
    orwl_wq *wq = wh->location;
    if (wq) {
      ORWL_TIMER(wh_acquire)
      orwl_wh_acquire(wh);
      /* This wait must be done outside the critical section, such
         that others might insert request while we are waiting. */
      p99_count_wait(&wh->tokens);
      /* The tokens field of a wh will never be touched, once it
         fell to 0 so there should be no race condition on wh,
         here. We still have to take the mutex to ensure that we
         protect head and tail while we are splicing wh out of the
         queue. */
      P99_MUTUAL_EXCLUDE(wq->mut) {
        wh->location = 0;
        /* mappings of the data will not be valid across critical
           sections, purge */
        orwl_alloc* alloc = orwl_alloc_ref_get(&wq->allocr);
        if (alloc) {
          if (wq->onhold) {
            orwl_alloc_ref_replace(&wq->allocr, 0);
            wq->onhold = false;
          } else orwl_alloc_realloc(alloc, ORWL_ALLOC_UNMAP);
        }
        for (;;) {
          /* wh must be the head of the list */
          P99_TP_STATE(orwl_wh_ptr) state_head = P99_TP_STATE_INITIALIZER(&wq->head, 0);
          P99_TP_STATE(orwl_wh_ptr) state_tail = P99_TP_STATE_INITIALIZER(&wq->tail, 0);
          P99_TP_STATE(orwl_wh_ptr) state_next = P99_TP_STATE_INITIALIZER(&wh->next, 0);
          /* If there is no next element, wh must be the tail of the
             list. The new tail must be set to 0, too. */
          orwl_wh* wh_head = P99_TP_STATE_GET(&state_head);
          orwl_wh* wh_tail = P99_TP_STATE_GET(&state_tail);
          orwl_wh* wh_next = P99_TP_STATE_GET(&state_next);
          if (wh_next
              || ((wh == wh_tail)
                  && P99_TP_STATE_COMMIT(&state_tail))) {
            if (!wh_next && (wh == wh_tail)) orwl_wh_discount(wh_tail);
            P99_TP_STATE_SET(&state_head, wh_next);
            if ((wh != wh_head)
                ||(!P99_TP_STATE_COMMIT(&state_head))
                /* nobody else should have access to wh anymore, so this has to succeed. */
                ||(!P99_TP_STATE_COMMIT(&state_next)))
              P99_THROW(EINVAL);
            if (wh_next) {
              /* Unlock potential acquirers */
              p99_notifier_set(&wh_next->acq);
            }
            orwl_wh_discount(wh_head);
            P99_THROW_ASSERT(EINVAL, !P99_TP_GET(&wh->next));
            ret = orwl_valid;
            break;
          }
        }
      }
    } else {
      orwl_wh_ref_trigger(&wh->next);
    }
  }
  return ret;
}

uint64_t* o_rwl_wq_map_rlocked(orwl_wq* wq, size_t* data_len) {
  /* Check if data_len contains one of the special values for
     allocation. */
  orwl_alloc* alloc = orwl_alloc_ref_get(&wq->allocr);
  void* buf = alloc ? orwl_alloc_realloc(alloc, ORWL_ALLOC_MAP) : 0;
  size_t nbytes = alloc ? alloc->size : 0;
  if (data_len) *data_len = nbytes/sizeof(uint64_t);
  return nbytes ? buf : 0;
}

void o_rwl_wq_link_wlocked(orwl_wq *wq,       /*!< the locked queue to act on */
                           orwl_buffer* data,    /*!< data buffer that is provided
                                      from elsewhere */
                           bool borrowed      /*!< whether this location here
                                       is responsible for the data */
                          ) {
  o_rwl_wq_resize_wlocked(wq, 0);
  orwl_alloc_ref_assign(&wq->allocr, &data->allocr);
  wq->borrowed = borrowed;
}

uint64_t* orwl_wh_map(orwl_wh* wh, size_t* data_len) {
  uint64_t* ret = 0;
  orwl_state state;
  ORWL_TIMER() {
    ORWL_TIMER(wh_acquire)
    state = orwl_wh_acquire(wh);
    if (state == orwl_acquired) {
      orwl_wq *wq = wh->location;
      P99_THROW_ASSERT(ENOLCK, wq);
      P99_RDLOCK(wq->rwlock)
      ret = o_rwl_wq_map_rlocked(wq, data_len);
    } else {
      if (data_len) *data_len = 0;
      P99_THROW(EINVAL);
    }
  }
  return ret;
}

void o_rwl_wq_resize_wlocked(orwl_wq* wq, size_t len) {
  if (wq->borrowed) {
    orwl_alloc_ref_destroy(&wq->allocr);
    wq->borrowed = false;
    if (!len) return;
  }
  size_t const blen      =          len * sizeof(uint64_t);
  orwl_alloc* alloc = orwl_alloc_ref_get(&wq->allocr);
  void* buf = alloc ? orwl_alloc_realloc(alloc, ORWL_ALLOC_MAP) : 0;
  size_t nbytes = alloc ? alloc->size : 0;
  if (P99_UNLIKELY((!len && !nbytes) || (len*sizeof(uint64_t) == nbytes))) return;
  /* zero out memory that is returned to the system */
  if (nbytes > blen) {
    uint64_t* data = buf;
    if (data)
      memset(&data[len], 0, nbytes - blen);
  }
  /* See if this wq already has an allocator. If not create one. */
  if (!alloc) {
    alloc = P99_NEW(orwl_alloc);
    orwl_alloc_ref_replace(&wq->allocr, alloc);
    buf = orwl_alloc_realloc(alloc, blen);
  }

  /* zero out newly allocated memory */
  if (nbytes < blen) {
    if (P99_LIKELY(buf))
      memset(((char*)buf)+nbytes, 0, blen - nbytes);
    /* We wanted to enlarge the buffer but we ran out of virtual
       address space. There is not much that we can do about it: keep
       the data that we still possess, cross fingers and return. */
    else return;
  }
  if (buf && alloc) {
    orwl_alloc_realloc(alloc, ORWL_ALLOC_UNMAP);
  }
}

void orwl_wh_resize(orwl_wh* wh, size_t len) {
  orwl_state state;
  ORWL_TIMER() {
    ORWL_TIMER(wh_acquire)
    state = orwl_wh_acquire(wh);
    if (state == orwl_acquired) {
      orwl_wq *wq = wh->location;
      P99_THROW_ASSERT(ENOLCK, wq);
      P99_WRLOCK(wq->rwlock)
      o_rwl_wq_resize_wlocked(wq, len);
    }
  }
}

static
void o_rwl_wq_unlink_locked(orwl_wq* wq, orwl_alloc_ref* ar) {
  ORWL_TIMER() {
    if (wq) {
      orwl_alloc* wq_alloc = orwl_alloc_ref_get(&wq->allocr);
      if (wq_alloc)
        ORWL_TIMER(copyone) {
        orwl_alloc_copyone(ar, &wq->allocr);
        orwl_alloc_ref_destroy(&wq->allocr);
      } else {
        trace(wq->borrowed, "request to unlink a borrowed allocation, ignored");
      }
    }
  }
}

void o_rwl_wh_unlink(orwl_wh* wh, orwl_alloc_ref* ar) {
  orwl_state state;
  ORWL_TIMER() {
    ORWL_TIMER(wh_acquire)
    state = orwl_wh_acquire(wh);
    if (state == orwl_acquired) {
      orwl_wq *wq = wh->location;
      P99_THROW_ASSERT(ENOLCK, wq);
      P99_WRLOCK(wq->rwlock)
      o_rwl_wq_unlink_locked(wq, ar);
    }
  }
}

P99_INSTANTIATE(void, orwl_state_destroy, orwl_state*);
P99_INSTANTIATE(orwl_state*, orwl_state_init, orwl_state *, orwl_state);

DEFINE_NEW_DELETE(orwl_state);
