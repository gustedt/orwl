/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the author and copyright holder for this work is                           */
/* all rights reserved,  2010, 2012 Jens Gustedt, INRIA, France               */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
/*
** orwl_barrier.c
**
** Made by (Jens Gustedt)
** Login   <gustedt@damogran.loria.fr>
**
** Started on  Wed Nov 24 23:35:18 2010 Jens Gustedt
** Last update Sun May 12 01:17:25 2002 Speed Blue
*/

#include "orwl_barrier.h"

int orwl_barrier_destroy(orwl_barrier* barrier) {
  if (p99_count_value(&barrier->side[1]) || p99_count_value(&barrier->side[2])) return EBUSY;
  p99_count_destroy(&barrier->side[0]);
  p99_count_destroy(&barrier->side[1]);
  p99_count_destroy(&barrier->side[2]);
  return 0;
}

int orwl_barrier_init(orwl_barrier* barrier, unsigned count) {
  p99_count_init(&barrier->side[0], count);
  p99_count_init(&barrier->side[1], 0u);
  p99_count_init(&barrier->side[2], 0u);
  return 0;
}

bool orwl_barrier_wait(orwl_barrier* barrier, size_t howmuch) {
  bool last = 0;
  /* When starting from outside, side[0] is count and both others are
     0. */
  /* side[i] == count, side[i + 1] == 0, side[i + 2] == 0 */
  for (unsigned i = 0; i < 3u; ++i) {
    p99_count_inc(&barrier->side[(i + 1u) % 3u], howmuch);
    last = !p99_count_dec(&barrier->side[i], howmuch);
    if (P99_LIKELY(!last)) p99_count_wait(&barrier->side[i]);
    /* side[i] == 0, side[i + 1] == count, side[i + 2] == 0 */
  }
  return last;
}
