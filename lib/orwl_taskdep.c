#include "orwl_taskdep.h"

P99_GETOPT_DEFINE(v, bool, orwl_taskdep_verbose, false, "verbose", "verbose output");

#pragma weak orwl_taskdep_status_
int64_t orwl_taskdep_status_;

orwl_taskdep_type* orwl_taskdep_type_init(orwl_taskdep_type* task, int argc, char* argv[argc+1]) {
  if (task) {
    *task = (orwl_taskdep_type) {
        .argc = argc,
        .argv = argv,
    };
  }
  return task;
}

void orwl_taskdep_type_destroy(orwl_taskdep_type *task) {
   // empty
}

P99_DEFINE_DELETE(orwl_taskdep_type);

static char const orwl_taskdep_unique[16];

void orwl_taskdep_start(orwl_taskdep_blob state[static 1], int64_t const tid[static 1]) {
  if (orwl_taskdep_verbose) {
    report(1, "startup for task %" PRId64, tid[0]);
  }
  state[0].pdata[0] = (char*)orwl_taskdep_unique;
}

// The default shutdown function.
void orwl_taskdep_shutdown(orwl_taskdep_blob state[static 1], int64_t const tid[static 1]) {
  if (state && state[0].pdata[0] != orwl_taskdep_unique) {
     report(1, "task %" PRId64 ", warning: default shutdown for non-default state %p", tid[0], state);
  }
}
