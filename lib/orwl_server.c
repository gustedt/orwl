/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the authors and copyright holders for this work are as follows:            */
/* all rights reserved,  2011 Emmanuel Jeanvoine, INRIA, France               */
/* all rights reserved,  2016 Farouk Mansouri, INRIA, France                  */
/* all rights reserved,  2010-2014, 2016-2017 Jens Gustedt, INRIA, France     */
/* all rights reserved,  2013 Rodrigo Campos, INRIA, France                   */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#include "orwl_server.h"
#include "orwl_socket.h"
#include "orwl_deep_instr.h"
#include "orwl_affinity.h"
#include "p99_qsort.h"

static
orwl_server * o_rwl_initial_srv = 0;

static
void o_rwl_stop_(void) {
  orwl_stop(o_rwl_initial_srv);
}

_Atomic(double) orwl_epoch;
_Atomic(size_t) orwl_epoch_count;


P99_DECLARE_ONCE_CHAIN(orwl_smalloc);

P99_DEFINE_ONCE_CHAIN(orwl_server,
                      orwl_smalloc,
                      orwl_thread,
                      orwl_proc,
                      orwl_wq,
                      orwl_wh,
                      orwl_handle,
                      orwl_rand) {
  /* Make sure that orwl_stop is called at least once for this
     process. */
  trace(orwl_verbose, "inserting stop");
  atexit(o_rwl_stop_);
}

P99_INSTANTIATE(o_rwl_grouping*, o_rwl_grouping_init, o_rwl_grouping*, uint64_t, bool);

void o_rwl_init(orwl_server *srv, size_t locations_amount) {
  ORWL_TIMER() {
    orwl_deep_instr_init();
    atomic_store_explicit(&orwl_epoch, seconds(), memory_order_relaxed);

    if (orwl_server_local == srv) {
      o_rwl_initial_srv = srv;
    }
    /* local server initialization */
    ORWL_TIMER(start)
    orwl_start(0, 10 + (locations_amount * 2), srv, true, 0, locations_amount);
    if (!orwl_alive()) P99_THROW(orwl_nolck);

    /* Synchronize with all other processes to know who is up. */
    ORWL_TIMER(files) {
      orwl_wait_and_load_init_files(0, 0, 0, 0, 0, srv);
      p99_notifier_block(&srv->up_ab);
    }
#if HAVE_AFFINITY > 0
    orwl_affinity_init(locations_amount, orwl_nl, srv->first_location, srv->max_queues);
#endif
  }
}

static inline
bool o_rwl_server_check(orwl_server* serv, size_t line) {
  bool ret = p99_notifier_load(&serv->up_srv);
  trace(orwl_verbose && !ret, "server %p: shutting down in line %zu", serv, line);
  return ret;
}

#define o_rwl_server_check(S) o_rwl_server_check(S, __LINE__)

void
orwl_schedule(size_t myloc, size_t locations_amount, orwl_server *srv, bool auxiliary) {
  p99_notifier_block(&srv->up_ab);
  if (myloc == SIZE_MAX) myloc = ORWL_MYLOC(srv->ab);
  int fd = -1;
  char name[32] = { 0 };
  bool first = false;
  // If this is a distributed execution, we have to check if several
  // processes landed on the same node.
  if (myloc == srv->first_location && srv->max_queues < orwl_nl) {
    // construct a unique name for a shared segment
    uint64_t const chal = orwl_challenge(42);
    snprintf(name, sizeof name, "/orwl%08" PRIX64, chal);
    report(orwl_verbose, "we only have %zu/%zu locations: distributed, shared segment %s",
           srv->max_queues, orwl_nl, name);
    // Open the shared segment. Only one process will succeed this
    // first try.
    fd = shm_open(name, O_RDWR|O_CREAT|O_EXCL, S_IRUSR|S_IWUSR);
    if (fd < 0) {
      // all others will come later
      fd = shm_open(name, O_RDWR, 0);
    } else {
      // the first process initializes the shared object
      first = true;
      ftruncate(fd, sizeof *srv->node_total);
    }
  }
  ORWL_TIMER(1bar)
  orwl_global_barrier_wait(myloc, locations_amount, srv);
  if (fd >= 0) {
    // As soon as the shared segment is opened by everybody, we can
    // unlink it. It will stay alive (but invisible) as long as any
    // file descriptor to is kept open.
    if (first) shm_unlink(name);
    void* addr = mmap(0, sizeof *srv->node_total, PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);
    if (addr != MAP_FAILED) {
      srv->node_total = addr;
      srv->node_offset = atomic_fetch_add(srv->node_total, srv->max_queues);
    }
  }
  orwl_global_barrier_wait(myloc, locations_amount, srv);
#if HAVE_AFFINITY > 0
  // With the knowledge if this node is shared between different
  // processes we may attempt to compute thread affinity.
  if (myloc == srv->first_location) {
    orwl_affinity_compute(srv->node_offset, srv->node_total ? *srv->node_total : srv->max_queues);
  }
#endif
  if (myloc == srv->local_elected)
    ORWL_TIMER(2elected) {
    ORWL_TIMER(1unblock)
    orwl_server_unblock(srv);
    ORWL_TIMER(2bar)
    orwl_global_barrier_wait(myloc, locations_amount, srv);
    p99_notifier_set(&srv->up_sched);
  } else
    ORWL_TIMER(3bar)
    orwl_global_barrier_wait(myloc, locations_amount, srv);
#if HAVE_AFFINITY > 0
  orwl_affinity_set(myloc, auxiliary);
#else
  // Avoid bogus messages about unused variable it the affinity module
  // is not provided.
  (void)auxiliary;
#endif
}

bool orwl_verbose = 0;

ORWL_DEFINE_ENV(ORWL_VERBOSE);
ORWL_DEFINE_ENV(ORWL_TIDS);
ORWL_DEFINE_ENV(ORWL_LOCIDS);
ORWL_DEFINE_ENV(ORWL_TARGS);

P99_INSTANTIATE(orwl_server*, orwl_server_get);

orwl_server* o_rwl_server_get(void) {
  static
  orwl_server srv = ORWL_SERVER_INITIALIZER(0, 0, 0, 0);
  orwl_server * srv_loc = orwl_server_local;
  if (!srv_loc) orwl_server_local = &srv;
  return &srv;
}

/**
 ** @brief A default value for the amount of possible connections per
 ** ORWL location.
 **/
P99_CONSTANT(int, orwl_connections_per_location, 4);

orwl_server* orwl_server_init(orwl_server *serv,
                              size_t max_connections,
                              size_t max_queues,
                              char const* endp) {
  P99_INIT_CHAIN(orwl_server);
  if (serv) {
    /* Initialize all simple fields of the DS. Unfortunately this
       can't be just a struct assignment since there is a const field
       hidden in the lifos. gcc doesn't allow such copy operation,
       whereas clang does... */
    memcpy(serv, &(orwl_server) {
      .fd_listen = -1,
       .max_queues = max_queues,
        .wqs = max_queues ? orwl_wq_vnew(max_queues) : P99_0(void*),
         .whs = max_queues ? orwl_wh_ref_vnew(max_queues) : P99_0(void*),
          .ep = (orwl_endpoint)ORWL_ENDPOINT_INITIALIZER(ORWL_ADDR_ANY, 0),
           .max_connections =
             (max_connections
              ? max_connections
              : orwl_connections_per_location*max_queues),
    }, sizeof *serv);

    /* Now initialize all fields that might need a dynamic
       initialization. Do this in declaration order to ease
       maintenance. */
    if (endp && endp[0]) orwl_endpoint_parse(endp, &serv->ep);

    p99_notifier_init(&serv->up_srv);
    p99_notifier_init(&serv->term_srv);
    p99_notifier_init(&serv->up_ab);
    p99_notifier_init(&serv->up_sched);

    P99_THROW_CALL_THRD(mtx_init, &serv->launch, mtx_timed|mtx_recursive);
    P99_THROW_CALL_THRD(mtx_init, &serv->global_barrier_mtx, mtx_timed);
    P99_THROW_CALL_THRD(cnd_init, &serv->global_barrier_cnd);

    atomic_init(&serv->block_count, 0);
    p99_count_init(&serv->waiters_cnt);
    (void)p99_lifo_init(&serv->waiters_remote, 0);
    (void)p99_lifo_init(&serv->waiters_local, 0);

  }
  return serv;
}

void orwl_server_close(orwl_server *serv) {
  if (serv) {
    if (serv->fd_listen != -1) {
      int fd = serv->fd_listen;
      serv->fd_listen = -1;
      orwl_socket_close(fd);
      fflush(0);
    }
  }
}


void orwl_server_terminate(orwl_server *serv) {
  trace(orwl_verbose, "received termination signal");
  if (serv) {
    serv->err = EHOSTUNREACH;
    p99_notifier_unset(&serv->up_srv);
  }
}

static
void o_rwl_server_wait_remotes(orwl_server *serv) {
  if (serv->wqs) {
    for (size_t tries = 100; tries; --tries) {
      size_t finished = 0;
      for (size_t i = 0; i < serv->max_queues; ++i) {
        orwl_wh* head = orwl_wh_ref_get(&serv->wqs[i].head);
        if (head) {
          TRACE(orwl_verbose, "somebody %s still inserted at %s", head, i);
        } else
          ++finished;
      }
      if (finished == serv->max_queues) break;
      TRACE(orwl_verbose, "waiting for %s remotes to terminate (%s)", serv->max_queues - finished, tries);
      sleepfor(1E-2);
    }
    REPORT(orwl_verbose, "all remotes are done");
  }
}


void orwl_server_destroy(orwl_server *serv) {
  if (serv->aliases) {
    P99_TRY {
      orwl_server_close(serv);
      free(serv->info);
      free(serv->aliases);
      free((void*)serv->locids);
      free((void*)serv->tids);
      if (serv->targs) {
        free((void*)serv->targs[0]);
        free((void*)serv->targs);
      }
      free((void*)serv->tlens);
      orwl_wh_ref_vdelete(serv->whs);
      /* force the queues to be empty */
      if (serv->wqs)
        for (size_t i = 0; i < serv->max_queues; ++i) {
          P99_TRY {
            while (orwl_wh_ref_get(&serv->wqs[i].head))
              P99_TRY {
              orwl_wh_ref_trigger(&serv->wqs[i].head);
            } P99_CATCH();
          } P99_CATCH();
        }
      orwl_wq_vdelete(serv->wqs);
      p99_notifier_vdelete(serv->id_initialized);
      free(serv->global_barrier);
      orwl_address_book_delete(serv->ab);
      orwl_graph_delete(serv->graph);
      orwl_server_init(serv);
      serv->aliases = 0;
    } P99_CATCH();
  }
}

DEFINE_NEW_DELETE(orwl_server);

static
void o_rwl_getsocket(orwl_endpoint* ep, int *fdp, int *mfds) {
  if (!ep->sock_type) ep->sock_type = SOCK_STREAM;
  int fd;
  P99_TRY {
RETRY:
    fd = socket(ep->addr.sa.sa_family, ep->sock_type);
    if (orwl_verbose) (void)atomic_fetch_max_explicit(&o_rwl_timing_fd, fd, memory_order_acq_rel);
    if (P99_UNLIKELY(fd == -1)) {
      if ((errno != EPROTONOSUPPORT) || (ep->sock_type == SOCK_STREAM))
        P99_THROW_ERRNO;
      if (mfds) perror("unable to create socket, forcing tcp");
      ep->sock_type = SOCK_STREAM;
      goto RETRY;
    }
    /* set to non blocking operation for the socket */
    int opts = fcntl(fd, F_GETFL);
    if (P99_UNLIKELY(opts < 0))
      P99_THROW_ERRNO;
    opts |= O_NONBLOCK;
    if (P99_UNLIKELY(fcntl(fd, F_SETFL, opts) < 0))
      P99_THROW_ERRNO;
    if (P99_UNLIKELY(bind(fd, &ep->addr.sa, sizeof ep->addr)))
      P99_THROW_ERRNO;
    if (fdp) {
      *fdp = fd;
      if (mfds && *mfds < fd) *mfds = fd;
    } else close(fd);
  } P99_CATCH(int code) {
    errno = 0;
    switch (code) {
    case 0: break;
    case EADDRINUSE: break;
    default: P99_RETHROW;
    }
  }
}


/* This is a list of global DNS servers that have a high probability
   to be up. They are just used to connect an UDP socket to them, no
   data is exchanged. This is the only POSIX complying trick that we
   found to read the host's own routing tables and by that to know his
   own global address, if it has any.
   - The listed host don't even have to be up. They just must have
     entries in the routing table.
   - There are several for each version. Even if accidentally we are
     inside of one of the domains of these sites, there should always
     be an external address left that give us our own address with the
     widest scope.*/
static
orwl_addr o_rwl_global[] = {
  // INRIA, IPv4 France
  ORWL_IN4(ORWL_IN4_ADDR(193, 51, 208, 13), ORWL_IN4_PORT(53)),
  // Google, IPv4 USA
  ORWL_IN4(ORWL_IN4_ADDR(8, 8, 8, 8), ORWL_IN4_PORT(53)),
  // OpenNIC, IPv2 Australia
  ORWL_IN4(ORWL_IN4_ADDR(58, 6, 115, 43), ORWL_IN4_PORT(53)),
  // OpenDNS
  ORWL_IN4(ORWL_IN4_ADDR(208, 67, 222, 222), ORWL_IN4_PORT(53)),
#if POSIX_IPV6 > 0
  // OpenNIC, USA
  ORWL_IN6(ORWL_IN6_ADDR(0x2001, 0x0470, 0x1f07, 0x380b, 0, 0, 0, 0x0001), ORWL_IN6_PORT(53)),
  // OpenDNS
  ORWL_IN6(ORWL_IN6_ADDR(0x2620, 0x000c, 0xcc00, 0, 0, 0, 0, 0x0002), ORWL_IN6_PORT(53)),
  // DHCPv6 (does that work?)
  ORWL_IN6(ORWL_IN6_ADDR(0xff02, 0, 0, 0, 0, 0, 0x0001, 0x0002), ORWL_IN6_PORT(53)),
#endif
};

DEFINE_THREAD(orwl_server) {
  orwl_posix_init();
  orwl_verbose = ORWL_VERBOSE();
  report(0, "starting server as %s", orwl_endpoint_print(&Arg->ep));
  char const* volatile errorstr = 0;
  sa_family_t sa_family = AF_INET;
  /* Obtain a socket for the address that was asked for. */
  /* A set of extra addresses that might be configured later. */
  orwl_endpoint addr[] = {
    Arg->ep,
    { .addr = ORWL_IN4(ORWL_IN4_LOOPBACK), .sock_type = Arg->ep.sock_type },
#if POSIX_IPV6 > 0
    { .addr = ORWL_IN6(ORWL_IN6_LOOPBACK), .sock_type = Arg->ep.sock_type },
#endif
  };
  int mfds = -1;
  P99_TRY {
    o_rwl_getsocket(addr, &Arg->fd_listen, &mfds);
  } P99_CATCH(int code) {
    TRACE(code, "could not bind %s, code %s: %s",
          orwl_endpoint_print(addr), code, strerror(code));
  }

  int fds[] = {
    Arg->fd_listen,
    -1,
#if POSIX_IPV6 > 0
    -1,
#endif
  };

  enum { fdsN = P99_ALEN(fds) };

  if (P99_LIKELY(Arg->fd_listen >= 0)) {
    p99_seed* seed = p99_seed_get();

    /* Now that we have a valid file descriptor, protect its closing. */
    P99_TRY {

      /* If the port was not yet specified find and store it. */
      in_port_t port = orwl_addr_get_port(&addr->addr);
      if (!port) {
        orwl_endpoint_local(Arg->fd_listen, &Arg->ep);
        addr[0] = Arg->ep;
        port = orwl_addr_get_port(&addr->addr);
      }

      /* collect IP aliases from different source such that we can
         figure out how to advertise ourselves. */
      orwl_addr * aliases = orwl_addr_aliases(hostname());
      /* count the number of addresses that we found */
      size_t number = 0;
      while (memcmp(aliases + number, &ORWL_ADDR_EMPTY, sizeof *aliases)) ++number;
      /* if we were started with a restrictive IP address, filter the
         addresses with larger scope out. */

      orwl_ip_scope const scope = orwl_addr2scope(&addr->addr);
      if (scope > orwl_ip_any) {
        report(orwl_verbose, "specified server\t%-8s URL %s",
        orwl_ip_scope_getname(orwl_addr2scope(&addr->addr)) + 8,
        orwl_endpoint_print(&Arg->ep)
              );

        /* Add the address that we received to the pool of
           addresses. */
        aliases[number] = addr[0].addr;
        ++number;
        orwl_addr_sort(number, aliases);
        /* Skip all addresses that have a larger scope. */
        orwl_ip_scope enabled = orwl_addr2scope(&addr[0].addr);
        size_t invalid = 0;
        for (size_t i = 0; i < number; ++i) {
          if (orwl_addr2scope(&aliases[i]) <= enabled) break;
          ++invalid;
        }
        number -= invalid;
        memmove(aliases, aliases + invalid, sizeof(orwl_addr[number]));
      }
      aliases = P99_REALLOC(aliases, orwl_addr[number + P99_ALEN(o_rwl_global) + 1]);
      memset(aliases + number, 0, sizeof(orwl_addr[P99_ALEN(o_rwl_global) + 1]));

      /* Now look for interfaces that we can't bind. These shouldn't
         be advertised. */
      size_t invalid = 0;
      for (size_t i = 0; i < number; ++i) {
        orwl_addr_set_port(&aliases[i], 0);
        orwl_addr addr = aliases[i];
        int fd = socket(addr.sa.sa_family);
        if (bind(fd, &addr.sa, sizeof addr)) {
          orwl_endpoint ep = ORWL_ENDPOINT_INITIALIZER(aliases[i], 0);
          report(0, "no interface for\t%-8s URL %s",
                 orwl_ip_scope_getname(orwl_addr2scope(&ep.addr)) + 8,
                 orwl_endpoint_print(&ep)
                );
          ++invalid;
          aliases[i] = ORWL_ADDR_EMPTY;
        } else {
          errno = 0;
          orwl_addr_set_port(&aliases[i], port);
        }
        close(fd);
      }
      /* Now try to connect to known global addresses to find out
         addresses that perhaps do not appear in dns or /etc/hosts and
         to check if inet6 is really connected */
      for (size_t i = 0; i < P99_ALEN(o_rwl_global); ++i) {
        int volatile fd = -1;
        P99_TRY {
          fd = socket(o_rwl_global[i].sa.sa_family, SOCK_DGRAM);
          if (fd != -1) {
            if (!orwl_connect(fd, &o_rwl_global[i])) {
              if (o_rwl_global[i].sa.sa_family == AF_INET6) sa_family = AF_INET6;
              aliases[number] = orwl_endpoint_local(fd)->addr;
              orwl_addr_set_port(&aliases[number], port);
              ++number;
            }
          }
        } P99_CATCH() {
          if (fd != -1) close(fd);
        }
      }

      /* If there have been invalid addresses sort them out. */
      if (invalid) {
        orwl_addr_sort(number, aliases);
        number -= invalid;
        invalid = 0;
      }
      orwl_addr *prev = &aliases[0];
      for (size_t i = 1; i < number; ++i)
        if (!memcmp(prev, &aliases[i], sizeof *prev)) {
          aliases[i] = ORWL_ADDR_EMPTY;
          ++invalid;
        } else {
          prev = &aliases[i];
        }
      if (invalid) {
        orwl_addr_sort(number, aliases);
        number -= invalid;
        invalid = 0;
      }
      aliases = P99_REALLOC(aliases, orwl_addr[number + 1]);

      for (size_t i = 0; i < number; ++i) {
        /* Force all addresses to INET if we found that INET6 isn't
           working. */
        if (sa_family == AF_INET) aliases[i].sa.sa_family = AF_INET;
        orwl_endpoint ep = ORWL_ENDPOINT_INITIALIZER(aliases[i], 0);
        report(orwl_verbose, "server could be bound to \t%-8s URL %s, %s",
               orwl_ip_scope_getname(orwl_addr2scope(&ep.addr)) + 8,
               orwl_endpoint_print(&ep),
               sa_family_getname(aliases[i].sa.sa_family)
              );
      }


      /* Now decide which address is to be advertised. This is the
         valid address of widest permissible scope. Don't change the
         address if we had one of the same scope already. */
      if (scope == orwl_ip_any || scope > orwl_addr2scope(&aliases[0]))
        Arg->ep.addr = aliases[0];
      Arg->aliases = aliases;

      /* Try to bind the loopback addresses with the same port. If
         this doesn't work we can be sure that the first bind
         captured that already. */
      for (size_t i = 1; i < P99_ALEN(addr); ++i) {
        orwl_addr_set_port(&addr[i].addr, port);
        P99_TRY {
          o_rwl_getsocket(&addr[i], &fds[i], &mfds);
        } P99_CATCH();
      }

      /* In any case we try to bind s*/
      for (unsigned i  = 0; i < fdsN; ++i) {
        if (fds[i] != -1) {
          if (P99_UNLIKELY(listen(fds[i],  Arg->max_connections)))
            P99_THROW_ERRNO;
          P99_TRY {
            orwl_endpoint * ep = orwl_endpoint_local(fds[i]);
            report(orwl_verbose, "server listening to \t%-8s URL %s, %s",
            orwl_ip_scope_getname(orwl_addr2scope(&ep->addr)) + 8,
            orwl_endpoint_print(ep),
            sa_family_getname(ep->addr.sa.sa_family)
                  );
          } P99_CATCH();
        }
      }

      // we are now up and listening
      p99_notifier_set(&Arg->up_srv);

      for (uint64_t t = 1; o_rwl_server_check(Arg); ++t)
        ORWL_TIMER(loop) {
        /* Do this work before being connected */
        uint64_t const chal = p99_rand(seed);
        uint64_t const repl = orwl_challenge(chal);
        orwl_buffer header = ORWL_BUFFER_INITIALIZER64(orwl_header_enum_amount, (orwl_header)ORWL_HEADER_INITIALIZER(0, 0));

        if (Arg->info && Arg->info_len) progress(1, t, "%s", Arg->info);
        if (!(t % 10)) {
          TRACE(orwl_verbose, "server phase %s", t);
          fflush(0);
        }

        if (P99_UNLIKELY(!repl)) {
          errorstr = "orwl_server cannot serve without a secret";
          P99_THROW(errno ? errno : EINVAL);
        }
        errno = 0;
        fd_set rfds;
        int retval = 0;
        for (; o_rwl_server_check(Arg);) {
          FD_ZERO(&rfds);
          for (unsigned i  = 0; i < fdsN; ++i)
            if (fds[i] != -1) FD_SET(fds[i], &rfds);
          ORWL_TIMER(select)
          retval = select(mfds + 1, &rfds, 0, 0, &P99_LVAL(struct timeval, .tv_usec = 10000));

          if (retval == 0) continue;
          if (retval > 0) break;

          switch (errno) {
          case 0: break;
          case EINTR: errno = 0; break;
          case EBADF: errno = 0; break;
          default: P99_THROW_ERRNO;
          }
        }

        for (unsigned i  = 0; i < fdsN; ++i)
          if (fds[i] != -1 && FD_ISSET(fds[i], &rfds))
            ORWL_TIMER(spawn) {
            int volatile fd = -1;
            ORWL_TIMER(accept)
            P99_TRY {
              fd = orwl_accept(fds[i]);
            } P99_CATCH(int err) {
              if (err && !p99_notifier_load(&Arg->up_srv)) return;
            }
            if (orwl_verbose) (void)atomic_fetch_max_explicit(&o_rwl_timing_fd, fd, memory_order_acq_rel);
            if (fd == -1) P99_THROW_ERRNO;
            report(0, "connection from %s", orwl_endpoint_print(orwl_endpoint_remote(fd)));
            /* Now that we have a valid file descriptor, protect its closing. */
            P99_TRY {
              uint64_t *data = (void*)header.aio.aio_buf;
              bool async = false;
              uint64_t remo = 0;
              ORWL_TIMER(challenge) {
                /* Receive a challenge from the new connection */
                ORWL_TIMER(1recv_)
                orwl_recv_(fd, &header, 0);
                async = data[orwl_header_async];
                remo = data[orwl_header_remo];
                uint64_t const rchal = data[orwl_header_chal];
                data[orwl_header_repl] = orwl_challenge(rchal);
                data[orwl_header_chal] = chal;
                /* challenge / reply of the new connection */
                ORWL_TIMER(2send_)
                orwl_send_(fd, 0, 1, &header);
                ORWL_TIMER(3_1recv_)
                orwl_recv_(fd, &header, 0);
              }
              if (data[1] == repl) {
                size_t len = data[orwl_header_size];
                P99_THROW_ASSERT(ENOBUFS, len);
                ORWL_TIMER(launch) {
                  orwl_proc *sock = P99_NEW(orwl_proc, fd, Arg, async, remo, 1, &((orwl_buffer)ORWL_BUFFER_INITIALIZER64(len, 0)));
                  P99_MUTUAL_EXCLUDE(Arg->launch)
                  P99_THROW_CALL_THRD(orwl_proc_create_detached, sock);
                  /* The spawned thread will close the fd. */
                  fd = -1;
                }
              } else {
                diagnose(fd, "You are not authorized to talk on fd %d", fd);
              }
            } P99_CATCH(int code) {
              if (fd != -1) close(fd);
              switch (code) {
              default: P99_RETHROW;
              case EINVAL:;
              case 0:;
              }
            }
          }
      }
    } P99_CATCH(int code) {
      report(orwl_verbose, ORWL_TERM_CLEAR);
      if (!errorstr) errorstr = "orwl_server finished";
      if (orwl_verbose && code) p99_jmp_report(code);
      if (Arg->fd_listen != -1) {
        for (unsigned i  = 1; i < fdsN; ++i)
          if (fds[i] >= 0) orwl_socket_close(fds[i]);
        orwl_server_close(Arg);
        Arg->fd_listen = -1;
      }
      sleepfor(1E-2);
      if (Arg->err && !code) {
        switch (Arg->err) {
        case EHOSTUNREACH: break;
        case ECONNREFUSED: break;
        default:
          trace(orwl_verbose, "terminated: %s", strerror(Arg->err));
          orwl_server_destroy(Arg);
          abort();
        }
      }
      sleepfor(1E-2);
      if (errno) {
        trace(orwl_verbose, "%s: %s", errorstr, strerror(errno));
        sleepfor(1E-2);
      }
      if (code) {
        /* Some error codes are produced by us directly to force
           termination of the server. */
        switch (code) {
        case EBADF: break;
        default:
          trace(orwl_verbose, "cut off server loop, %s: %s", errorstr, strerror(code));
          sleepfor(1E-2);
          errno = 0;
          p99_notifier_set(&Arg->up_srv, 0);
        }
      }
      trace(orwl_verbose, "server %p: end of error processing for code %d", Arg, code);
    }
    trace(orwl_verbose, "server %p: finished listening", Arg);
  }
  trace(orwl_verbose, "server %p: done", Arg);
}

size_t orwl_server_block(orwl_server *srv, size_t nb) {
  P99_THROW_ASSERT(EINVAL, srv->whs);
  /* only the first blocking event triggers insertion of the
     blockers */
  size_t prev = atomic_fetch_add_explicit(&srv->block_count, nb, memory_order_acq_rel);
  if (!prev) {
    for (uint64_t i = 0; i < srv->max_queues; ++i) {
      P99_THROW_ASSERT(EINVAL, !orwl_wh_ref_get(&srv->whs[i]));
      orwl_wh_ref_replace(&srv->whs[i], P99_NEW(orwl_wh));
      orwl_wh *whp = orwl_wh_ref_get(&srv->whs[i]);
      orwl_wq_request(&srv->wqs[i], whp);
    }
  }
  return prev;
}

static
int o_rwl_grouping_compar(void const* a, void const* b, void* context) {
  o_rwl_grouping const*const* A0 = a;
  o_rwl_grouping const*const* B0 = b;
  o_rwl_grouping const* A = *A0;
  o_rwl_grouping const* B = *B0;
  return (A->pri < B->pri)
         ? -1
         : ((A->pri > B->pri)
            ? 1
            : (A->incl > B->incl
               ? -1
               : ((A->incl < B->incl)
                  ? 1
                  : 0)));
}

static
void o_rwl_server_schedule(P99_LIFO(o_rwl_grouping_ptr)* list) {
  ORWL_TIMER()
  if (P99_LIFO_TOP(list)) {
    TRACE(0, "scheduling requests %s", list);
    P99_LIFO_TABULATE(o_rwl_grouping, table, list);
    P99_ASORT(table, o_rwl_grouping_compar, 0);
    size_t len = P99_ALEN(table);
    for (size_t pos = 0; pos < len;)
      ORWL_TIMER(perprio) {
      /* We have to group requests with same priority and incl/excl
         type together. This is important to guarantee that all read
         requests with the same priority are scheduled
         simultaneously. Otherwise read requests could deadlock. */
      uint64_t pri = table[pos]->pri;
      bool incl = table[pos]->incl;
      size_t run = pos;
      while (run < len && (pri == table[run]->pri) && (incl == table[run]->incl)) ++run;
      /* First launch the whole group. */
      REPORT(0, "launching %s of priority %s %s", run-pos, pri, bool_getname(incl));
      for (size_t i = pos; i < run; ++i) {
        p99_notifier_set(&table[i]->ok);
        REPORT(0, "launching %s of priority %s %s", i, pri, bool_getname(incl));
      }
      /* Then wait that the whole group has succeeded the
         insertion. */
      ORWL_TIMER(block)
      for (; pos < run; ++pos) {
        p99_notifier_block(&table[pos]->launched);
        REPORT(0, "launched %s of priority %s %s", pos, pri, bool_getname(incl));
      }
    }
    ORWL_TIMER(free)
    for (size_t pos = 0; pos < len; ++pos)
      free(table[pos]);
    TRACE(0, "scheduling requests %s terminated", list);
  }
}

P99_DECLARE_STRUCT(o_rwl_sched);
P99_DEFINE_STRUCT(o_rwl_sched, P99_LIFO(o_rwl_grouping_ptr)* list);

#define O_RWL_SCHED_INITIALIZER(L) { .list = (L), }

static
void o_rwl_sched_destroy(o_rwl_sched *task) {
  /* empty */
}
P99_DECLARE_DELETE(o_rwl_sched, static);

DECLARE_THREAD(o_rwl_sched, static);
DEFINE_THREAD(o_rwl_sched, static) {
  ORWL_THREAD_USE(o_rwl_sched, list);
  o_rwl_server_schedule(list);
}

size_t orwl_server_unblock(orwl_server *srv, size_t nb) {
  P99_THROW_ASSERT(EINVAL, srv->whs);
  /* only the last unblocking event triggers removal of the
     blockers */
  size_t prev = atomic_fetch_sub_explicit(&srv->block_count, nb, memory_order_acq_rel);
  TRACE(0, "previous count is %s", prev);
  if (prev == 1) {
    atomic_thread_fence(memory_order_seq_cst);
    thrd_t thrdId[2] = { P99_INIT };
    o_rwl_sched sched[2] = {
      [0] = O_RWL_SCHED_INITIALIZER(&srv->waiters_local),
      [1] = O_RWL_SCHED_INITIALIZER(&srv->waiters_remote),
    };
    for (unsigned i = 0; i < 2; ++i) P99_THROW_CALL_THRD(o_rwl_sched_create_joinable, &sched[i], &thrdId[i]);
    ORWL_TIMER(2join)
    for (unsigned i = 0; i < 2; ++i) P99_THROW_CALL_THRD(o_rwl_sched_join, thrdId[i]);
    ORWL_TIMER(6count_wait)
    p99_count_wait(&srv->waiters_cnt);
    ORWL_TIMER(8checkQueues)
    for (uint64_t i = 0; i < srv->max_queues; ++i) {
      orwl_wh * wh = orwl_wh_ref_get(&srv->whs[i]);
      ORWL_TIMER(wh_acquire)
      orwl_wh_acquire(wh);
      orwl_wh_ref_trigger(&srv->whs[i]);
    }
  }
  return prev - 1;
}

void orwl_server_delayed_unblock(orwl_server *srv, size_t nb_tasks) {
  P99_CRITICAL {
    srv->unblocked_locations++;
  }
  if (srv->unblocked_locations == nb_tasks)
    orwl_server_unblock(srv);
}

static
size_t findNumbers(char const* list, size_t len, size_t tlist[len]) {
  size_t ret = 0;
  char *endptr = 0;
  do {
    unsigned long long val = strtoull(list, &endptr, 0);
    if (tlist) {
      P99_THROW_ASSERT(ENOBUFS, ret < len);
      tlist[ret] = val;
    }
    if (list == endptr) break;
    ++ret;
    list = endptr;
    if (!list[0]) break;
    list += strcspn(list, "01233456789");
  } while (list[0]);
  return ret;
}

static
size_t findStrings(char* list, char* elist[], char const*const sep) {
  size_t ret = 0;
  while (*list) {
    if (elist) elist[ret] = list;
    ++ret;
    list += strcspn(list, sep);
    if (*list) {
      list[0] = 0;
      ++list;
    }
  }
  return ret;
}

void
orwl_start(size_t max_queues,       /*!< [in] the maximum number of locations, defaults to 0 */
           size_t max_connections,  /*!< [in] maximum socket queue length, defaults to a multiple of max_queues */
           orwl_server *serv,       /*!< [out] the server object to initialize */
           bool block,              /*!< [in] block the server when launching, defaults to false */
           char const* endp,        /*!< [in] defaults to the null address */
           size_t locations_amount
          ) {
  P99_INIT_CHAIN(orwl_types);
  char const* env_tids = ORWL_TIDS();
  size_t const lt =
    (env_tids
     ? findNumbers(env_tids, 0, 0)
     : 1);
  size_t (*tids)[lt] = env_tids ? P99_ALLOC(*tids) : 0;
  if (env_tids)
    findNumbers(env_tids, lt, *tids);
  char const* env_locids = ORWL_LOCIDS();
  size_t const ll =
    (env_locids
     ? findNumbers(env_locids, 0, 0)
     : (tids
        ? lt * locations_amount
        : 1));
  size_t (*locids)[ll] = (env_locids || tids) ? P99_ALLOC(*locids) : 0;
  if (env_locids) {
    findNumbers(env_locids, ll, *locids);
  } else if (tids) {
    orwl_tasks2locations(lt, *tids, ll, *locids);
  }
  if (ll) max_queues = ll;
  if (!max_connections) max_connections = orwl_connections_per_location*max_queues;
  orwl_server_init(serv, max_connections, max_queues, endp);
  P99_THROW_CALL_THRD(orwl_server_create, serv, &serv->id);
  /* give the server the chance to fire things up */
  p99_notifier_block(&serv->up_srv);
  if (block) orwl_server_block(serv);
  serv->lt = lt;
  serv->ll = ll;
  serv->tids = *tids;
  serv->locids = *locids;
  char const*const env_targs = ORWL_TARGS();
  if (env_targs && env_targs[0]) {
    trace(1, "targs is %s", env_targs);
    char** targs = P99_MALLOC(char*[strlen(env_targs)]);
    /* This strdup should not be freed here since targs will point to
       the partial strings during the whole execution. */
    size_t const len =
      findStrings(P99_STRDUP(env_targs), targs, "|");
    targs = P99_REALLOC(targs, char*[len]);
    serv->targs = (void*)targs;
    size_t* tlens = P99_MALLOC(size_t[len]);
    for (size_t i = 0; i < len; ++i) {
      tlens[i] = strlen(targs[i]);
      findStrings(targs[i], 0, ";");
    }
    serv->tlens = tlens;
  } else {
    trace(orwl_verbose, "no targs");
    serv->targs = 0;
    serv->tlens = 0;
  }
}

void
o_rwl_stop(orwl_server *serv) {
  if (serv) {
    /* Ensure that all this is only called once for the local server
       process. */
    if (orwl_server_local == serv) {
      static atomic_flag flag = ATOMIC_FLAG_INIT;
      if (atomic_flag_test_and_set_explicit(&flag, memory_order_acq_rel)) return;
      /* Be sure to wait for all other threads to finish. */
      fflush(0);
      orwl_thrd_wait_detached();
      fflush(0);
      o_rwl_server_wait_remotes(serv);
    }
    fflush(0);
    orwl_close_group_graph(serv);

    /* For the purpose of this termination phase, establish a token
    ring among the ORWL processes. A first propagation starting at the
    0th process tells everybody that 0 is terminating. Then, a second
    progation tells everybody that everybody else is terminating. */
    P99_TRY {
      size_t first = serv->first_location;
      size_t next = serv->ab->next;
      p99_seed *seed = p99_seed_get();
      if (first) p99_notifier_block(&serv->term_srv);
      trace(orwl_verbose, "%zu, we know that 0 is terminating", first);
      /* unset the notifier before we propagate the information */
      p99_notifier_unset(&serv->term_srv);
      orwl_rpc(serv, &serv->ab->eps[next], seed, orwl_proc_final);
      p99_notifier_block(&serv->term_srv);
      trace(orwl_verbose, "%zu, we know that everybody is terminating.", first);
      /* The last in the ring shouldn't re-connect to 0, since that
         will be dead by now. */
      if (next) orwl_rpc(serv, &serv->ab->eps[next], seed, orwl_proc_final);
    } P99_CATCH(int err) {
      if (err) {
        trace(1, "termination ring caught an error.");
        p99_jmp_report(err);
      }
    }
    fflush(0);
    if (orwl_server_local == serv) {
      /* The termination ring needed the thread pool. Now we can stop
         all threads */
      orwl_thrd_wait_detached();
      orwl_thrd_pool_purge();
    }
    trace(orwl_verbose, "terminating");
    orwl_server_terminate(serv);
    fflush(0);
    trace(orwl_verbose, "joining server");
    int err = orwl_server_join(serv->id);
    trace(orwl_verbose, "server joined with code %d", err);
    fflush(0);
    orwl_server_close(serv);
    fflush(0);
    orwl_server_destroy(serv);
    fflush(0);
  } else
    trace(orwl_verbose, "stop without server.");

  orwl_deep_instr_print_stats();
  fflush(0);
  trace(orwl_verbose, "stopped.");
}

P99_DECLARE_STRUCT(o_rwl_push_unlocked);
P99_DEFINE_STRUCT(o_rwl_push_unlocked,
                  orwl_server *srv,
                  orwl_endpoint ep,
                  uint64_t whID
                 );

static
o_rwl_push_unlocked* o_rwl_push_unlocked_init(o_rwl_push_unlocked *el,
    orwl_server *srv,
    orwl_endpoint const ep,
    uint64_t whID) {
  if (el) {
    *el = (o_rwl_push_unlocked) {
      .srv = srv,
       .ep = ep,
        .whID = whID,
    };
  }
  return el;
}

static
void o_rwl_push_unlocked_destroy(o_rwl_push_unlocked *el) {
  // empty
}

P99_DECLARE_DELETE(o_rwl_push_unlocked, static);
DECLARE_THREAD(o_rwl_push_unlocked, static);

DEFINE_THREAD(o_rwl_push_unlocked, static) {
  ORWL_THREAD_USE(o_rwl_push_unlocked, srv, ep, whID);
  ORWL_TIMER(rpc)
  orwl_rpc(srv, &ep, p99_seed_get(), orwl_proc_trigger, whID);
}

void orwl_trigger(orwl_server *srv, orwl_endpoint const ep,
                  uint64_t whID) {
  ORWL_TIMER(create)
  P99_THROW_CALL_THRD(o_rwl_push_unlocked_create_detached, P99_NEW(o_rwl_push_unlocked, srv, ep, whID));
}

P99_DECLARE_STRUCT(o_rwl_remote);

P99_DEFINE_STRUCT(o_rwl_remote,
                  orwl_thread_cntrl* det,
                  orwl_server *srv,
                  orwl_endpoint ep,
                  orwl_wq *wq,
                  uint64_t whID,
                  bool keep
                 );

o_rwl_remote* o_rwl_remote_init(o_rwl_remote* el,
                                orwl_thread_cntrl* det,
                                orwl_server *srv,
                                orwl_endpoint ep,
                                orwl_wq *wq,
                                uint64_t whID,
                                bool keep
                               ) {
  if (el) {
    *el = (o_rwl_remote) {
      .det = det,
       .srv = srv,
        .ep = ep,
         .wq = wq,
          .whID = whID,
           .keep = keep,
    };
  }
  return el;
}

void o_rwl_remote_destroy(o_rwl_remote* el) {
  if (el->det) {
    orwl_thread_cntrl_wait_for_caller(el->det);
    orwl_thread_cntrl_delete(el->det);
  }
}

P99_DECLARE_DELETE(o_rwl_remote, static);
DECLARE_THREAD(o_rwl_remote, static);

DEFINE_THREAD(o_rwl_remote, static) {
  ORWL_THREAD_USE(o_rwl_remote, det, srv, ep, wq, whID, keep);
  p99_seed * seed = p99_seed_get();
  uint64_t buffer[orwl_push_header] = {
    [0] = ORWL_OBJID(orwl_proc_release),
    [1] = whID,
    [2] = orwl_push_withdata | (keep ? orwl_push_keep : 0),
  };
  enum { push_buffers = 2 };
  orwl_buffer mess[push_buffers] = {
    ORWL_BUFFER_INITIALIZER64(orwl_push_header, buffer),
    ORWL_BUFFER_INITIALIZER64(0, 0),
  };
  P99_RDLOCK(wq->rwlock) {
    orwl_thread_cntrl_freeze(det);
    mess[1].aio.aio_buf = o_rwl_wq_map_rlocked(wq, &mess[1].aio.aio_nbytes);
    buffer[3] = mess[1].aio.aio_nbytes;
    mess[1].aio.aio_nbytes *= sizeof(uint64_t);
    orwl_alloc_ref_assign(&mess[1].allocr, &wq->allocr);
    ORWL_TIMER(send_remote, mess[0].aio.aio_nbytes + mess[1].aio.aio_nbytes) {
      P99_TRY {
        orwl_send_remote(srv, &ep, seed, push_buffers, mess, true);
      } P99_CATCH(int code) {
        trace(code, "remote send from %s didn't succeed: %s",
              srv ? orwl_endpoint_print(&srv->ep) : "<null>",
              strerror(code));
      }
    }
  }
  orwl_buffer_destroy(&mess[0]);
  orwl_buffer_destroy(&mess[1]);
}


void orwl_transfer(orwl_server *srv, orwl_endpoint const ep,
                   orwl_wq *wq, uint64_t whID) {
  /*
    Send a request to the other side
    - to remove the remote wh ID and to transfer the data, if any,
    or,
    - pass over a pointer of the data.
  */
  P99_THROW_ASSERT(ENOLCK, wq);
  if (srv && orwl_endpoint_similar(&srv->ep, &ep)) {
    p99_seed * seed = p99_seed_get();
    uint64_t buffer[orwl_push_header] = {
      [0] = ORWL_OBJID(orwl_proc_release),
      [1] = whID,
      [2] = orwl_push_withdata,
    };
    enum { push_buffers = 2 };
    orwl_buffer mess[push_buffers] = {
      ORWL_BUFFER_INITIALIZER64(orwl_push_header, buffer),
      ORWL_BUFFER_INITIALIZER64(0, 0),
    };
    mess[1].aio.aio_buf = o_rwl_wq_map_rlocked(wq, &mess[1].aio.aio_nbytes);
    buffer[3] = mess[1].aio.aio_nbytes;
    mess[1].aio.aio_nbytes *= sizeof(uint64_t);
    /* Should perhaps be done unconditionally */
    if (mess[1].aio.aio_buf) {
      orwl_alloc_ref_mv(&mess[1].allocr, &wq->allocr);
    }
    ORWL_TIMER(send_local, mess[0].aio.aio_nbytes + mess[1].aio.aio_nbytes)
    orwl_send_local(srv, &ep, seed, push_buffers, mess);
    orwl_buffer_destroy(&mess[0]);
    orwl_buffer_destroy(&mess[1]);
  } else {
    orwl_thread_cntrl* det =  P99_NEW(orwl_thread_cntrl);
    o_rwl_remote *rem = P99_NEW(o_rwl_remote, det, srv, ep, wq, whID, false);
    P99_THROW_CALL_THRD(o_rwl_remote_launch, rem, det);
    /* Wait that the callee has taken the mutex. */
    orwl_thread_cntrl_wait_for_callee(det);
    orwl_thread_cntrl_detach(det);
  }
}

void orwl_push_copy(orwl_server *srv, orwl_endpoint const ep,
                    orwl_wq *wq, uint64_t whID) {
  /*
    Send a request to the other side
    - to remove the remote wh ID and to transfer the data, if any,
    or,
    - pass over a pointer of the data.
  */
  P99_THROW_ASSERT(ENOLCK, wq);
  if (srv && orwl_endpoint_similar(&srv->ep, &ep)) {
    p99_seed * seed = p99_seed_get();
    uint64_t buffer[orwl_push_header] = {
      [0] = ORWL_OBJID(orwl_proc_release),
      [1] = whID,
      [2] = orwl_push_withdata | orwl_push_keep,
    };
    enum { push_buffers = 2 };
    orwl_buffer mess[push_buffers] = {
      ORWL_BUFFER_INITIALIZER64(orwl_push_header, buffer),
      ORWL_BUFFER_INITIALIZER64(0, 0),
    };
    P99_RDLOCK(wq->rwlock) {
      mess[1].aio.aio_buf = o_rwl_wq_map_rlocked(wq, &mess[1].aio.aio_nbytes);
      buffer[3] = mess[1].aio.aio_nbytes;
      mess[1].aio.aio_nbytes *= sizeof(uint64_t);
      orwl_alloc_ref_assign(&mess[1].allocr, &wq->allocr);
    }
    ORWL_TIMER(send_local, mess[0].aio.aio_nbytes + mess[1].aio.aio_nbytes)
    orwl_send_local(srv, &ep, seed, push_buffers, mess);
    orwl_buffer_destroy(&mess[0]);
    orwl_buffer_destroy(&mess[1]);
  } else {
    orwl_thread_cntrl* det =  P99_NEW(orwl_thread_cntrl);
    o_rwl_remote *rem = P99_NEW(o_rwl_remote, det, srv, ep, wq, whID, true);
    P99_THROW_CALL_THRD(o_rwl_remote_launch, rem, det);
    /* Wait that the callee has taken the mutex. */
    orwl_thread_cntrl_wait_for_callee(det);
    orwl_thread_cntrl_detach(det);
  }
}

uint64_t o_rwl_rpc(orwl_server *srv, orwl_endpoint const* there, p99_seed *seed, size_t n, orwl_buffer mess[n]) {
  uint64_t ret = 0;
  /* We use a remote connection if either the local server address is
     not known or we see that the endpoint "there" is actually on
     server "srv" */
  if (!srv || !orwl_endpoint_similar(&srv->ep, there))
    ORWL_TIMER(send_remote, mess[0].aio.aio_nbytes + (n>0 ? mess[1].aio.aio_nbytes : 0))
    ret = orwl_send_remote(srv, there, seed, n, mess, false);
  else
    ORWL_TIMER(send_local, mess[0].aio.aio_nbytes + (n>0 ? mess[1].aio.aio_nbytes : 0))
    ret = orwl_send_local(srv, there, seed, n, mess);
  return ret;
}


uint64_t orwl_send_remote(orwl_server *srv, orwl_endpoint const* there, p99_seed *seed, size_t n, orwl_buffer mess[n], bool async) {
  uint64_t volatile ret = 0;
  ORWL_TIMER() {
    report(0, "sending remote: %s ?= %s",
           srv ? orwl_endpoint_print(&srv->ep) : "<null>",
           orwl_endpoint_print(there));
    /* do all this work before opening the socket */
    uint64_t const chal = p99_rand(seed);
    uint64_t const repl = orwl_challenge(chal);
    orwl_buffer volatile header =
      ORWL_BUFFER_INITIALIZER64(
        orwl_header_enum_amount,
        (orwl_header)ORWL_HEADER_INITIALIZER(chal, async));
    /* Use tcp as a default socket type. */
    int sock_type = there->sock_type;
    if (!sock_type) sock_type = SOCK_STREAM;
    if (P99_UNLIKELY(!repl)) {
      report(1, "cannot send without a secret\n");
      P99_THROW(errno ? errno : EINVAL);
    }

    int volatile fd = socket(there->addr.sa.sa_family, sock_type);
    if (orwl_verbose) (void)atomic_fetch_max_explicit(&o_rwl_timing_fd, fd, memory_order_acq_rel);
    if (P99_UNLIKELY(fd < 0)) {
      P99_HANDLE_ERRNO {
      default:
        perror("orwl_send could not open socket");
        P99_THROW(p99_errno);
      }
      P99_THROW(EINVAL);
    }

    /* Now that we have a valid file descriptor, protect its closing. */
    bool volatile success = false;
    P99_TRY {
      /* connect and do a challenge / receive authentication with the
         other side */
      ORWL_TIMER(connect)
      for (unsigned tries = 0; tries < 10; ++tries) {
        P99_TRY {
          int err;
          getsockopt(fd, SOL_SOCKET, SO_ERROR, &err, &P99_LVAL(socklen_t, sizeof err));
          trace(err, "socket had error: %s", strerror(err));
          if (P99_UNLIKELY(orwl_connect(fd, &there->addr))) P99_THROW_ERRNO;
        } P99_CATCH(int code) {
          switch (code) {
          case 0:
            tries = 1000;
            break;
          case EAGAIN:;
          case EALREADY:;
          case EINTR:;
          case ETIMEDOUT:;
          case ECONNREFUSED:;
            errno = 0;
            report(orwl_verbose, "orwl_send could not connect socket, address is %s, %s",
                   orwl_endpoint_print(there),
                   strerror(code));
            if (tries < 10) {
              sleepfor(1E-3);
              break;
            } else P99_RETHROW;
          default: P99_RETHROW;
          }
        }
      }
      ORWL_TIMER(challenge) {
        ORWL_TIMER(1send_) orwl_send_(fd, 0, 1, &header);
        ORWL_TIMER(2recv_) orwl_recv_(fd, &header, 0);
      }
      uint64_t *data = (void*)header.aio.aio_buf;
      uint64_t const rchal = data[orwl_header_chal];
      uint64_t const rrepl = data[orwl_header_repl];
      uint64_t const rremo = data[orwl_header_remo];
      /* The communication was successful */
      if (P99_LIKELY(rrepl == repl))
        ORWL_TIMER(success) {
        /* The other side is authorized. Send the answer and the size of
           the message to the other side. */
        data[orwl_header_repl] = orwl_challenge(rchal);
        data[orwl_header_size] = mess[0].aio.aio_nbytes/sizeof(uint64_t);
        ORWL_TIMER(3_1send_)
        orwl_send_(fd, 0, 1, &header);
        /* The authorized empty message indicates termination.
           If not so, we now send the real message. */
        if (mess[0].aio.aio_nbytes) ORWL_TIMER(message) {
          orwl_tcp_cork(fd, true);
          size_t total = 0;
          for (size_t i = 0; i < n; ++i) total += mess[i].aio.aio_nbytes;
          P99_TRY {
            ORWL_TIMER(3_2send_, total) {
              orwl_send_(fd, rremo, n, mess);
              orwl_tcp_cork(fd, false);
            }
          } P99_CATCH(int code) {
            if (code) {
              trace(code, "payload send of %s to %s (%zu bytes) didn't succeed: %s",
                    srv ? orwl_endpoint_print(&srv->ep) : "<null>",
                    orwl_endpoint_print(there),
                    total,
                    strerror(code));
              P99_RETHROW;
            }
          }
          /* Receive a final message, until the other end closes the
             connection. */
          ORWL_TIMER(4recv_) {
            ret = -1;
            P99_TRY {
              orwl_recv_(fd, &header, rremo);
              ret = data[orwl_header_ret];
            } P99_CATCH(int code) {
              trace(code, "recv request of %s from %s didn't succeed: %s",
                    srv ? orwl_endpoint_print(&srv->ep) : "<null>",
                    orwl_endpoint_print(there),
                    strerror(code));
              switch (code) {
              case EINVAL:;
              case ENOMEM:;
              case ENOTSOCK:;
                P99_RETHROW;
              }
              trace(code, "recv error of %s from %s is considered transient, poisoning return value",
                    srv ? orwl_endpoint_print(&srv->ep) : "<null>",
                    orwl_endpoint_print(there));
            }
          }
        }
        P99_TRY {
          orwl_socket_sig_close(fd);
        } P99_CATCH(int code) {
          trace(code, "close request of %s from %s didn't succeed: %s",
                srv ? orwl_endpoint_print(&srv->ep) : "<null>",
                orwl_endpoint_print(there),
                strerror(code));
        }
        fd = -1;
        success = true;
      } else  {
        /* The other side is not authorized. Terminate. */
        diagnose(fd, "fd %d, you are not who you pretend to be", fd);
        data[orwl_header_chal] = 0;
        data[orwl_header_repl] = 0;
        ORWL_TIMER(send_3)
        orwl_send_(fd, 0, 1, &header);
      }
    } P99_CATCH(int code) {
      trace(!success || code, "send request %s to %s didn't succeed: %s",
            srv ? orwl_endpoint_print(&srv->ep) : "<null>",
            orwl_endpoint_print(there),
            strerror(code));
      fflush(0);
      if (code) P99_RETHROW;
    }
    P99_TRY {
      /* The FD may now be closed with RST. */
      if (fd != -1) close(fd);
    } P99_CATCH(int code) {
      trace(code, "close request of socket from %s to %s didn't succeed: %s",
            srv ? orwl_endpoint_print(&srv->ep) : "<null>",
            orwl_endpoint_print(there),
            strerror(code));
      fflush(0);
    }
  }
  return ret;
}


uint64_t orwl_send_local(orwl_server *srv, orwl_endpoint const* there, p99_seed *seed, size_t n, orwl_buffer mess[n]) {
  uint64_t ret = 0;
  /* This is supposed to be a local connection, directly create the
     thread without going through the server socket. */
  orwl_thread_cntrl* det =  P99_NEW(orwl_thread_cntrl);
  orwl_proc *sock = P99_NEW(orwl_proc,
                            -1, srv, false,,
                            n,
                            mess,
                            det);
  P99_MUTUAL_EXCLUDE(srv->launch) {
    P99_THROW_CALL_THRD(orwl_proc_launch, sock, det);
    if (srv->info && srv->info_len) progress(1, (uintptr_t)sock, "%s", srv->info);
  }
  /* Wait that the caller has copied the message and returned the
     control information, and tell him in turn that we have read the
     result. */
  orwl_thread_cntrl_wait_for_callee(det);
  ret = sock->ret;
  orwl_thread_cntrl_detach(det);
  return ret;
}


P99_INSTANTIATE(bool, orwl_alive, orwl_server *);
