/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the authors and copyright holders for this work are as follows:            */
/* all rights reserved,  2011 Emmanuel Jeanvoine, INRIA, France               */
/* all rights reserved,  2011-2013 Jens Gustedt, INRIA, France                */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#include "orwl_instrument.h"

void print_statistics_instr(size_t id) {
  printf("Nothing is done\n");
}

orwl_state orwl_handle2_write_request_instr(orwl_mirror* location, orwl_handle2* rh2, size_t flag) {
  return orwl_write_request(location, rh2);
}

orwl_state orwl_handle2_read_request_instr(orwl_mirror* location, orwl_handle2* rh2, size_t flag) {
  return orwl_read_request(location, rh2);
}

orwl_state orwl_handle2_acquire_instr(orwl_handle2* rh2, size_t flag) {
  return orwl_acquire(rh2);
}

orwl_state orwl_disconnect2_instr(orwl_handle2* rh2, size_t flag) {
  return orwl_handle2_disconnect(rh2);
}

orwl_state orwl_handle2_test_instr(orwl_handle2* rh2, size_t flag) {
  return orwl_test(rh2);
}

void* orwl_handle2_write_map_instr(orwl_handle2* rh2, size_t flag) {
  return orwl_write_map(rh2);
}

void const* orwl_handle2_read_map_instr(orwl_handle2* rh2, size_t flag) {
  return orwl_read_map(rh2);
}

void orwl_handle2_truncate_instr(orwl_handle2* rh2, size_t data_len, size_t flag) {
  orwl_truncate(rh2, data_len);
}
