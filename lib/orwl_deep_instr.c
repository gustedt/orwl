/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the author and copyright holder for this work is                           */
/* all rights reserved,  2013 Rodrigo Campos, INRIA, France                   */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#include "orwl_deep_instr.h"
#include "p99_defarg.h"       // P99_INSTANTIATE()
#include <stdio.h>            // printf()


#ifdef DEEP_INSTRUMENTATION

_Thread_local o_rwl_deep_tlocal_stats o_rwl_deep_tlocal;
o_rwl_deep_global_stats o_rwl_deep_global;

P99_INSTANTIATE(void, orwl_deep_instr_start, void);
P99_INSTANTIATE(void, orwl_deep_instr_stop, uint64_t*);
P99_INSTANTIATE(void, orwl_deep_instr_inline_start, void);
P99_INSTANTIATE(void, orwl_deep_instr_inline_stop, void);
P99_INSTANTIATE(void, orwl_deep_instr_func_start, void);
P99_INSTANTIATE(void, orwl_deep_instr_func_stop, void);
P99_INSTANTIATE(void, orwl_deep_instr_thread_start, void);
P99_INSTANTIATE(void, orwl_deep_instr_thread_stop, void);
P99_INSTANTIATE(void, orwl_deep_instr_update, void);
P99_INSTANTIATE(void, orwl_deep_instr_thread_not_internal, void);
P99_INSTANTIATE(void, orwl_deep_instr_thread_internal, void);
P99_INSTANTIATE(void, orwl_deep_instr_set_init_overhead, uint64_t);


void orwl_deep_instr_print_stats(void) {

  // Hack to print stats
  uint64_t func = atomic_load(&o_rwl_deep_global.time_funcs);
  printf("lib time (ns) spent on funcs: %lu\n", func);

  uint64_t iinline = atomic_load(&o_rwl_deep_global.time_inline);
  printf("lib time (ns) spent on inline funcs: %lu\n", iinline);

  uint64_t threads = atomic_load(&o_rwl_deep_global.time_threads);
  printf("lib time (ns) spent on internal threads: %lu\n", threads);

  printf("lib total time (ns): %lu\n", func + iinline + threads);

  unsigned calls = atomic_load(&o_rwl_deep_global.calls);
  printf("Number of calls to deep-instrumentation: %u\n", calls);

  printf("lib init time (ns) spent: %lu\n", o_rwl_deep_global.init_overhead);

  return;
}

void orwl_deep_instr_init(void) {

  /* Check precision is at least reported to be good (1 ns) on this system */
  struct timespec prec;
  int err = clock_getres(CLOCK_THREAD_CPUTIME_ID, &prec);
  if (err)
    abort();

  if (!(prec.tv_sec == 0 && prec.tv_nsec == 1))
    abort();

  /* All global variables are 0 init and 0 is what we want, so no init needed */
}


#endif // DEEP_INSTRUMENTATION
