/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the authors and copyright holders for this work are as follows:            */
/* all rights reserved,  2011 Emmanuel Jeanvoine, INRIA, France               */
/* all rights reserved,  2010-2013, 2016 Jens Gustedt, INRIA, France          */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#include "orwl_server.h"

P99_DEFINE_ONCE_CHAIN(orwl_proc,
                      orwl_rand) {
}


P99_INSTANTIATE(orwl_proc*, orwl_proc_init, orwl_proc *,
                int,
                orwl_server*,
                bool,
                uint64_t,
                size_t,
                orwl_buffer*,
                orwl_thread_cntrl*);

void orwl_proc_untie_caller(orwl_proc *sock) {
  if (sock->is_untied) return;
  REPORT(0, "detaching %s", sock);
  if (sock->det) {
    /* This thread is launched locally. */
    orwl_thread_cntrl_freeze(sock->det);
  } else ORWL_TIMER(4send) {
    /* Ack the termination of the call */
    orwl_buffer header = ORWL_BUFFER_INITIALIZER64(orwl_header_enum_amount, (orwl_header)ORWL_HEADER_INITIALIZER(sock->ret, 0));
    /* This message still has a content, namely the return value of
       the function call. */
    orwl_send_(sock->fd, sock->remoteorder, 1, &header);
    orwl_socket_wait_close(sock->fd);
    sock->fd = -1;
    orwl_buffer_destroy(&header);
  }
  sock->is_untied = true;
}

void orwl_proc_destroy(orwl_proc *sock) {
  orwl_proc_untie_caller(sock);
  if (sock->det) {
    orwl_thread_cntrl_wait_for_caller(sock->det);
    orwl_thread_cntrl_delete(sock->det);
  }
  orwl_buffer_vdelete(sock->mes);
  *sock = P99_LVAL(orwl_proc);
}

P99_DEFINE_DELETE(orwl_proc);

DEFINE_ORWL_PROC_FUNC(orwl_server_callback, uint64_t funcID) {
  ORWL_PROC_READ(Arg, orwl_server_callback, uint64_t funcID);
  orwl_domain_call(ORWL_FTAB(orwl_proc), funcID, Arg);
}

DEFINE_THREAD(orwl_proc) {
  P99_THROW_ASSERT(EINVAL, Arg->mes[0].aio.aio_buf);
  REPORT(0, "starting %s", Arg);
  P99_TRY {
    if (Arg->fd != -1) {
      ORWL_TIMER(3_2recv_, Arg->mes[0].aio.aio_nbytes) {
        orwl_recv_(Arg->fd, &Arg->mes[0], Arg->remoteorder);
      }
      if (!Arg->srv) P99_THROW(1);
    }
    /* do something with mess here */
    orwl_server_callback(Arg);
  } P99_CATCH(int code) {
    orwl_proc_untie_caller(Arg);
    REPORT(0, "ending %s, code %s", Arg, code);
  }
}

orwl_addr orwl_proc_getpeer(orwl_proc const*Arg) {
  orwl_addr addr = ORWL_ADDR_ANY;
  int ret = orwl_getpeername(Arg->fd, &addr);
  return (ret == -1) ? Arg->srv->ep.addr : addr;
}
