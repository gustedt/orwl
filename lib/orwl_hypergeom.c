// This may look like C code, but it really is -*- mode: c; coding: utf-8 -*-
//


// @file
//
// Sample the hypergeometric distribution. Taken from the implementation
// of Zechner and Niederl:
//
// Implemented by H. Zechner, January 1994
// Revised by F. Niederl, July 1994
//
// See their instructive comments on the implementation below.
//
// Heavily modified, adapted to C++ and integrated to parXXL:
// (C) Jens Gustedt, 2006
//
// integrated to ORWL
// (C) Jens Gustedt, 2018

#include "orwl_hypergeom.h"
#include "orwl_logfak.h"
#include <stdbool.h>

/******************************************************************
 *                                                                *
 * Hypergeometric Distribution - Patchwork Rejection/Inversion    *
 *                                                                *
 ******************************************************************
 *                                                                *
 * The basic algorithms work for parameters 1 <= n <= M <= N/2.   *
 * Otherwise parameters are re-defined in the set-up step and the *
 * random number K is adapted before delivering.                  *
 * For l = m-max(0,n-N+M) < 10  Inversion method hmdu is applied: *
 * The random numbers are generated via modal down-up search,     *
 * starting at the mode m. The cumulative probabilities           *
 * are avoided by using the technique of chop-down.               *
 * For l >= 10  the Patchwork Rejection method  hprs is employed: *
 * The area below the histogram function f(x) in its              *
 * body is rearranged by certain point reflections. Within a      *
 * large center interval variates are sampled efficiently by      *
 * rejection from uniform hats. Rectangular immediate acceptance  *
 * regions speed up the generation. The remaining tails are       *
 * covered by exponential functions.                              *
 *                                                                *
 ******************************************************************
 *                                                                *
 * FUNCTION :   - hprsc samples a random number from the          *
 *                Hypergeometric distribution with parameters     *
 *                N (number of red and black balls), M (number    *
 *                of red balls) and n (number of trials)          *
 *                valid for N >= 2, M,n <= N.                     *
 * REFERENCE :  - H. Zechner (1994): Efficient sampling from      *
 *                continuous and discrete unimodal distributions, *
 *                Doctoral Dissertation, 156 pp., Technical       *
 *                University Graz, Austria.                       *
 * SUBPROGRAMS: - flogfak(k)  ... log(k!) with long integer k     *
 *              - gsl_rng_uniform_pos(gslR) ...                   *
 *                (0,1)-Uniform generator with gslR an  gsl_rng*  *
 *                form the gnu scientific library                 *
 *              - hmdu(gslR,N,M,n) ... Hypergeometric generator   *
 *                for l<10                                        *
 *              - hprs(gslR,N,M,n) ... Hypergeometric generator   *
 *                for l>=10 with const gsl_rng*  gslR,            *
 *                long integer  N , M , n.                        *
 *                                                                *
 * Implemented by H. Zechner, January 1994                        *
 * Revised by F. Niederl, July 1994                               *
 ******************************************************************/

static
double drandUniform(p99_seed seed[static 1], double bound) {
  return p99_drand(seed)*bound;
}

#define drandUniform(...) drandUniform2(__VA_ARGS__, 1, )
#define drandUniform2(S, B, ...) drandUniform(S, B)

static
double fc_lnpk (size_t k, size_t N_Mn, size_t M, size_t n) {
  return (orwl_flogfak (k)
          + orwl_flogfak (M - k)
          + orwl_flogfak (n - k)
          + orwl_flogfak (N_Mn + k));
}

static
bool final_accept (double Y, size_t X, double C, size_t N_Mn, size_t M, size_t n) {
  double right = C - fc_lnpk (X, N_Mn, M, n);
  return log(Y) <= right;
}

static
bool quick_accept(double Y, double A, double B, double P) {
  return Y <= fma(A, B, P);
}

/*       Hypergeometric (N, M, n) - modal down-up sequential search */

static
size_t hmdu (p99_seed seed[static 1], size_t const N, size_t const M, size_t const n) {
  double Mp = M + 1;
  double np = n + 1;
  double p = Mp / (N + 2.0);
  double nu = np * p;  /* mode, real       */
  size_t N_Mn = N - M - n;
  size_t m = nu;
  if (m == nu && p == 0.5) {
    --m;
  }
  size_t mp = m + 1;  /* mode, integer    */

  /* mode probability, using the external function orwl_flogfak(k) = ln(k!)    */
  double fm
    = exp(orwl_flogfak (N - M)
          - orwl_flogfak (N_Mn + m)
          - orwl_flogfak (n - m)
          + orwl_flogfak (M)
          - orwl_flogfak (M - m)
          - orwl_flogfak (m)
          - orwl_flogfak (N)
          + orwl_flogfak (N - n)
          + orwl_flogfak (n));

  /* safety bound  -  guarantees at least 17 significant decimal digits   */
  /*                  b = min(n, (size_t)(nu + k*c'))                   */
  size_t b = fma(11.0, sqrt(fma(nu, ((1.0 - p)*(1.0 - n/(double)(N))), 1.0)), nu);
  if (b > n) b = n;

  // This loop has a very slow probability of restarting. In
  // practise it never occurs. By that we get away with just one RN.
  for (;;) {
    double U = drandUniform(seed);

    // quickly accept the mode
    if (U <= fm) return (m);
    U -= fm;

    double d = fm;
    {
      double c = fm;
      // down- and upward search from the mode.
      // In fact the probabilities decrease quickly around the mode,
      // so on expectation this loop has only a constant number of
      // iterations.
      for (size_t iI = 1; iI <= m; iI++) {
        /* downward search  */
        {
          register size_t K = mp - iI;
          c *= (double)(K) / (np - K);
          c *= (double)(N_Mn + K) / (Mp - K);
          if (U <= c) return (K - 1);
          U -= c;
        }
        /* upward search    */
        {
          register size_t K = m + iI;
          d *= (double)(np - K) / K;
          d *= (double)(Mp - K) / (N_Mn + K);
          if (U <= d) return (K);
          U -= d;
        }
      }
    }

    /* upward search from K = 2m + 1 to K = b                               */
    for (size_t K = mp + m; K <= b; K++) {
      d *= (double)(np - K) / K;
      d *= (double)(Mp - K) / (N_Mn + K);
      if (U <= d) return (K);
      U -= d;
    }
  }
}

/*              Hypergeometric (N, M, n) - Patchwork-rejection method  */
static
size_t hprs (p99_seed seed[static 1], size_t const N, size_t const M, size_t const n) {

  double Mp = M + 1;
  double np = n + 1;
  ssize_t const N_Mn = N - M - n;

  double p = Mp / (N + 2.0);
  double nu = np * p;  /* main parameters   */

  /* approximate deviation of reflection points k2, k4 from nu - 1/2      */
  double U = sqrt(nu * (1.0 - p) * (1.0 - (n + 2.0) / (N + 3.0)) + 0.25);

  /* mode m, reflection points k2 and k4, and points k1 and k5, which     */
  /* delimit the centre region of h(x)                                    */
  /* k2 = ceil (nu - 1/2 - U),    k1 = 2*k2 - (m - 1 + delta_ml)          */
  /* k4 = floor(nu - 1/2 + U),    k5 = 2*k4 - (m + 1 - delta_mr)          */
  size_t const m = nu;
  size_t k2 = ceil(nu - 0.5 - U);
  if (k2 >= m)
    k2 = m - 1;
  size_t const k4 = nu - 0.5 + U;
  ssize_t const k1 = k2 + k2 - m + 1; /* delta_ml = 0      */
  ssize_t const k5 = k4 + k4 - m;     /* delta_mr = 1      */

  /* range width of the critical left and right centre region             */
  double const dl = k2 - k1;
  double const dr = k5 - k4;

  /* recurrence constants r(k) = p(k)/p(k-1) at k = k1, k2, k4+1, k5+1    */
  double const r1
    = (np / (double)(k1) - 1.0)
    * (Mp - k1)
    / (double)(N_Mn + k1);
  double const r2
    = (np / (double)(k2) - 1.0)
    * (Mp - k2)
    / (double)(N_Mn + k2);
  double const r4
    = (np / (double)(k4 + 1L) - 1.0)
    * (M - k4)
    / (double)(N_Mn + k4 + 1L);
  double const r5
    = (np / (double)(k5 + 1L) - 1.0)
    * (M - k5)
    / (double)(N_Mn + k5 + 1L);

  /* reciprocal values of the scale parameters of expon. tail envelopes   */
  double const ll = log(r1);  /* expon. tail left  */
  double const lr = -log(r5);  /* expon. tail right */

  /* hypergeom. constant, necessary for computing function values f(k)    */
  double const c_pm = fc_lnpk(m, N_Mn, M, n);

  /* function values f(k) = p(k)/p(m)  at  k = k2, k4, k1, k5             */
  double const f2 = exp(c_pm - fc_lnpk(k2, N_Mn, M, n));
  double const f4 = exp(c_pm - fc_lnpk(k4, N_Mn, M, n));
  double const f1 = exp(c_pm - fc_lnpk(k1, N_Mn, M, n));
  double const f5 = exp(c_pm - fc_lnpk(k5, N_Mn, M, n));

  /* area of the two centre and the two exponential tail regions          */
  /* area of the two immediate acceptance regions between k2, k4          */
  double const p1 = f2 * (dl + 1.0);      /* immed. left       */
  double const p2 = f2 * dl + p1;         /* centre left       */
  double const p3 = f4 * (dr + 1.0) + p2; /* immed. right      */
  double const p4 = f4 * dr + p3;         /* centre right      */
  double const p5 = f1 / ll + p4;         /* expon. tail left  */
  double const p6 = f5 / lr + p5;         /* expon. tail right */

  for (;;) {
    register size_t X;
    register double Y; /* (X, Y) <-> (V, W) */

    /* generate uniform number U -- U(0, p6)                                */
    U = drandUniform(seed, p6);

    /* case distinction corresponding to U                                  */
    if (U < p2) {       /* centre left       */

      /* immediate acceptance region R2 = [k2, m) *[0, f2),  X = k2, ... m -1 */
      if (U < p1)
        return (k2 + (size_t)(U / f2));

      U -= p1;

      /* immediate acceptance region R1 = [k1, k2)*[0, f1),  X = k1, ... k2-1 */
      Y = U / dl;
      if (Y < f1)
        return (k1 + (size_t)(U / f1));

      /* computation of candidate X < k2, and its counterpart V > k2          */
      /* either squeeze-acceptance of X or acceptance-rejection of V          */
      ssize_t Dk = (size_t)drandUniform(seed, dl) + 1;

      X = k2 - Dk;
      if (quick_accept(Y, -Dk, (f2 - f2 / r2), f2)) return (X);

      double W = f2 + f2 - Y;
      if (W < 1.0) {
        size_t V = k2 + Dk;
        if (quick_accept(W, Dk, (1.0 - f2) / (dl + 1.0), f2)
            || final_accept(W, V, c_pm, N_Mn, M, n))
          return (V);
      }
    } else if (U < p4) {         /* centre right      */

      /* immediate acceptance region R3 = [m, k4+1)*[0, f4), X = m, ... k4    */
      if (U < p3)
        return (k4 - (size_t)((U - p2) / f4));

      U -= p3;

      /* immediate acceptance region R4 = [k4+1, k5+1)*[0, f5)                */
      Y = U / dr;
      if (Y < f5)
        return (k5 - (size_t)(U / f5));

      /* computation of candidate X > k4, and its counterpart V < k4          */
      /* either squeeze-acceptance of X or acceptance-rejection of V          */
      ssize_t Dk = (size_t)drandUniform(seed, dr) + 1;

      X = k4 + Dk;
      if (quick_accept(Y, -Dk, (f4 - f4 * r4), f4))
        return (X);

      double W = f4 + f4 - Y;
      if (W < 1.0) {
        size_t V = k4 - Dk;
        if (quick_accept(W, Dk, (1.0 - f4) / dr, f4)
            || final_accept(W, V, c_pm, N_Mn, M, n))
          return (V);
      }
    } else if (U < p5) {        /* expon. tail left  */
      Y = drandUniform(seed);
      ssize_t Dk = (size_t)(1.0 - log (Y) / ll);

      /* 0 <= X <= k1 - 1  */
      if (Dk > k1) continue;
      X = k1 - Dk;

      /* Y -- U(0, h(x))   */
      Y *= (U - p4) * ll;

      if (quick_accept(Y, -Dk, (f1 - f1 / r1), f1))
        return (X);

    } else { /* expon. tail right */
      Y = drandUniform(seed);
      ssize_t Dk = (size_t)(1.0 - log (Y) / lr);
      X = k5 + Dk;

      /* k5 + 1 <= X <= n  */
      if (X > n) continue;

      /* Y -- U(0, h(x))   */
      Y *= (U - p5) * lr;

      /* quick accept of X */
      if (quick_accept(Y, -Dk, (f5 - f5 * r5), f5))
        return (X);
    }

    /* acceptance-rejection test of candidate X from the original area      */
    /* test, whether  Y <= f(X),    with  Y = U*h(x)  and  U -- U(0, 1)     */
    /* log f(X) = log( m! (M - m)! (n - m)! (N - M - n + m)! )              */
    /*          - log( X! (M - X)! (n - X)! (N - M - n + X)! )              */
    /* by using an external function for log k!                             */
    if (final_accept(Y, X, c_pm, N_Mn, M, n))
      return (X);
  }
}

/*          Hypergeometric (N, M, n) - Combination                  */
size_t orwl_randHypergeom (p99_seed seed[static 1], size_t N, size_t M, size_t n) {
  assert(M <= N);
  assert(n <= N);
  // If n is too large, swap
  if (n > M) { size_t tmp = n; n = M; M = tmp; }
  assert(n <= M);
  if (!n) return 0;
  if (N==M) return n;
  size_t ret =
    (n <= (N/2))
    ? ((M <= (N/2))
       ? ((((n*M) / N) < 10)
          ? hmdu(seed, N, M, n)
          : hprs(seed, N, M, n))
       : n - orwl_randHypergeom(seed, N, N - M, n))
    : M - orwl_randHypergeom(seed, N, M, N - n);
  assert(ret <= n);
  assert(ret <= M);
  return ret;
}

void
orwl_varRandHypergeom (p99_seed seed[static 1],
                       size_t total,
                       size_t whites,
                       size_t draws,
                       size_t draw[static draws]) {
  for (size_t d = 0; d < draws; ++d) {
    assert(total >= draw[d]);
    size_t resW = orwl_randHypergeom(seed, total, whites, draw[d]);
    total -= draw[d];
    whites -= resW;
    draw[d] = resW;
  }
}

size_t hyperprox(p99_seed* seed, size_t N1, size_t N2, size_t T);
