/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the authors and copyright holders for this work are as follows:            */
/* all rights reserved,  2011 Emmanuel Jeanvoine, INRIA, France               */
/* all rights reserved,  2010-2014 Jens Gustedt, INRIA, France                */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#include "orwl_socket.h"
#include "orwl_server.h"
#include "p99_c99_default.h"

#undef setsockopt
#define setsockopt(...) P99_THROW_CALL_ZERO(setsockopt, 5, __VA_ARGS__)

void orwl_endpoint_destroy(orwl_endpoint *endpoint);

#define O_RWL_FAM(NAME) [NAME] = #NAME

enum {
  o_rwl_unspec = AF_UNSPEC,
  o_rwl_inet = AF_INET,
#ifdef AF_INET6
  o_rwl_inet6 = AF_INET6,
#endif
  o_rwl_fam_max
};

char const* sa_family_getname(sa_family_t fam) {
  static char const* names[] = {
    O_RWL_FAM(AF_UNSPEC),
    O_RWL_FAM(AF_INET),
#ifdef AF_INET6
    O_RWL_FAM(AF_INET6),
#endif
    [o_rwl_fam_max] = "<unknown protocol>"
  };
  return (fam < o_rwl_fam_max) && names[fam]
         ? names[fam] + 3
         : names[o_rwl_fam_max];
}




DEFINE_NEW_DELETE(orwl_endpoint);

orwl_endpoint* orwl_endpoint_parse(char const* name, orwl_endpoint* ep) {
  ORWL_TIMER()
  if (ep && name && name[0]) {
    *ep = P99_LVAL(orwl_endpoint const);
    char const prefix[7 + 1] = "orwl://";
    if (strstr(name, prefix) == name) {
      name += 7;
    }
    {
      char const* name1 = 0;
      if (name[0] == '[') {
        ++name;
        name1 = strchr(name, ']');
        if (!name1) return 0;
      } else {
        name1 = strchr(name, ':');
      }
      {
        char *host = 0;
        /* If it is set, name1 points to a char inside name[]. */
        if (name1) {
          size_t len = name1 - name;
          host = malloc(len + 1);
          memcpy(host, name, len);
          host[len] = '\0';
        }
        orwl_addr * aliases = orwl_addr_aliases(host ? host : name);
        if (aliases) ep->addr = aliases[0];
        free(aliases);
        free(host);
      }
      if (!name1) goto FINISH;
      name = name1;
      if (name[0] == ']') ++name;
    }
    if (name[0] && name[0] == ':') {
      ++name;
      orwl_addr_set_port(&ep->addr, htons(strtou16(name)));
      name += strcspn(name, ":/");
    }
    if (name[0] && name[0] == ':') {
      ++name;
      size_t len = strcspn(name, ":/");
      if (len) {
        char const *sockname = memcpy((char [20]) {0}, name, len);
        ep->sock_type = orwl_sock_type_get(sockname);
        name += len;
      }
    }
    if (name[0] && name[0] == '/') {
      ++name;
      ep->index = strtou64(name);
    }
  }
FINISH:
  return ep;
}

char const* orwl_endpoint_print(orwl_endpoint const* ep, char*restrict name) {
  ORWL_TIMER() {
    name[0] = '\0';
    char host[256] = "";
    ORWL_TIMER(name)
    if (orwl_addr2scope(&ep->addr) == orwl_ip_any) {
      hostname(host);
    } else {
      bool paren = ep->addr.sa.sa_family != AF_INET;
      if (paren) host[0] = '[';
      orwl_inet_ntop(&ep->addr, host + paren);
      if (paren) strcat(host, "]");
    }
    if (!host[0] || !strcmp(host, "::")) ORWL_TIMER(name2) hostname(host);
    char const* sock_type = 0;
    switch (ep->sock_type) {
    case SOCK_STREAM: break;
    case 0: break;
    default: sock_type = orwl_sock_type_name(ep->sock_type); break;
    }
    P99_STRCATS(name, "orwl://", host, ":",
                PRIu(ntohs(orwl_addr_get_port(&ep->addr))),
                sock_type ? ":" : "",
                sock_type ? sock_type : "",
                "/");
    if (ep->index) P99_STRCATS(name, PRIu(ep->index));
  }
  return name;
}


P99_CONSTANT(uint64_t, maxlen, UINT64_C(1) << 13);
P99_CONSTANT(int, maxtries, 1000);

void orwl_send_(int fd, uint64_t remo, size_t n, orwl_buffer volatile mess[n]) {
  iovec bbuf[n];
  uint64_t* buf[n];
  size_t count[n];
  P99_UNUSED(count);
  for (size_t i = 0; i < n; ++i) {
    bbuf[i] = orwl_buffer2iovec(&mess[i]);
    count[i] = bbuf[i].iov_len;
    trace((i && !orwl_alloc_ref_get(&mess[i].allocr)), "buffer %p[%zu/%zu] of size %zu without alloc",
          mess, i, n, count[i]);
    /* We only have to translate the message buffer, if we have an
       order that is different from network order and different from
       the order of the remote host. */
    buf[i] = ((ORWL_HOSTORDER != ORWL_NETWORDER)
              && (remo != ORWL_NETWORDER))
             ? calloc(mess[i].aio.aio_nbytes, 1)
             : (void*)0;
    if (buf[i]) {
      ORWL_TIMER(hton, count[i])
      orwl_hton(buf[i], (void*)mess[i].aio.aio_buf, mess[i].aio.aio_nbytes/sizeof(uint64_t));
      bbuf[i].iov_base = buf[i];
    }
  }

  P99_TRY {
    for (size_t i = 0; i < n; ++i)
      ORWL_TIMER(inner, count[i]) {
      for (size_t tries = maxtries; bbuf[i].iov_len;) {
        /* Don't stress the network layer by sending too large messages
           at a time. */
        int flags = 0
#ifdef MSG_MORE
        | ((bbuf[i].iov_len > maxlen) ? MSG_MORE : 0)
#endif
#ifdef MSG_NOSIGNAL
        | MSG_NOSIGNAL
#endif
        ; /* leave this semicolon in place. */
        size_t const clen = (bbuf[i].iov_len > maxlen) ? maxlen : bbuf[i].iov_len;
        ssize_t res = 0;
        ORWL_TIMER(send, res)
        res = send(fd, bbuf[i].iov_base, clen, flags);
        if (P99_LIKELY(res > 0)) {
          if (tries != maxtries) {
            trace(1, "recovering after a series of %zu failures", maxtries-tries);
            tries = maxtries;
          }
          iovec_advance(&bbuf[i], res);
          /* If we weren't able to send everything at once, give it
             some time to breath. */
          if (res < clen) {
            trace(orwl_verbose, "partial send of %zd (%zu) bytes, %zu remaining",
                  res, clen, bbuf[i].iov_len);
            sleepfor(0.05);
          }
        } else {
          --tries;
          if (!tries) {
            if (!res || !errno) errno = ENOTCONN;
            trace(orwl_verbose, "no progress on socket, abandonning: %s", strerror(errno));
            P99_THROW_ERRNO;
          }
          switch (errno) {
          case 0: ;
            int err = orwl_socket_error(fd);
            trace(err, "socket had error: %s", strerror(err));
            break;
          case EINTR:
            errno = 0;
            if (orwl_verbose) perror("orwl_send_ was interrupted, retrying");
            break;
          default:
            trace(orwl_verbose, "unexpected error, abandonning: %s", strerror(errno));
            P99_THROW_ERRNO;
          }
          sleepfor(1E-6);
        }
      }
    }
  } P99_FINALLY {
    for (size_t i = 0; i < n; ++i) {
      free(buf[i]);
    }
  }
}

void orwl_recv_(int fd, orwl_buffer volatile const* mess, uint64_t remo) {
  if (fd == -1) P99_THROW(EBADF);
  iovec bbuf = orwl_buffer2iovec(mess);
  size_t count = bbuf.iov_len;
  if (!mess->aio.aio_nbytes) report(1, "orwl_recv_ with len 0, skipping\n");
  else
    ORWL_TIMER(inner, count) {
    (void)atomic_fetch_add_explicit(&o_rwl_timing_recv, count, memory_order_acq_rel);
    for (size_t tries = maxtries; bbuf.iov_len;) {
      /* Don't stress the network layer by receiving too large messages
         at a time. */
      size_t const clen = bbuf.iov_len;
      ssize_t res = 0;
      ORWL_TIMER(recv, res)
      res = recv(fd, bbuf.iov_base, clen, MSG_WAITALL);
      if (P99_LIKELY(res > 0)) {
        if (tries != maxtries) {
          trace(1, "recovering after a series of %zu failures", maxtries-tries);
          tries = maxtries;
        }
        iovec_advance(&bbuf, res);
      } else {
        --tries;
        if (!tries) {
          if (!res || !errno) errno = ENOTCONN;
          trace(orwl_verbose, "no progress on socket, abandonning: %s", strerror(errno));
          P99_THROW_ERRNO;
        }
        switch (errno) {
        case 0: ;
          int err = orwl_socket_error(fd);
          trace(err, "socket had error: %s", strerror(err));
          break;
        case EINTR:
          errno = 0;
          trace(orwl_verbose, "orwl_recv_ was interrupted, retrying");
          break;
        default:
          trace(orwl_verbose, "recv error, abandonning: %s", strerror(errno));
          P99_THROW_ERRNO;
        }
        fflush(0);
        sleepfor(1E-4);
      }
    }
    /* We only have to translate the message buffer, if we have an
       order that is different from network order and different from
       the order of the remote host. */
    if ((ORWL_HOSTORDER != ORWL_NETWORDER)
        && (remo != ORWL_NETWORDER)) {
      ORWL_TIMER(ntoh, count)
      orwl_ntoh((void*)mess->aio.aio_buf, 0, mess->aio.aio_nbytes/sizeof(uint64_t));
    }
  }
}


#if (POSIX_IPV6 > 0) || DOXYGEN
orwl_addr orwl_addr6(orwl_addr const*A) {
  orwl_addr ret = { .sa.sa_family = AF_INET6 };
  switch (A->sa.sa_family) {
  case AF_INET: {
    orwl_ip_scope scope = orwl_addr2scope(A);
    uint8_t const* bytes = (void const*)&A->sin.sin_addr.s_addr;
    switch (scope) {
    /* Link local and private addresses can not be sensibly translate to IPv6. */
    case orwl_ip_link: return ORWL_ADDR_EMPTY;
    case orwl_ip_privat: return ORWL_ADDR_EMPTY;
    case orwl_ip_any:      ret.sin6.sin6_addr = in6addr_any; break;
    case orwl_ip_loopback: ret.sin6.sin6_addr = in6addr_loopback; break;
    default: ret = (orwl_addr const)ORWL_IN6(ORWL_IN6_IN4(bytes[0], bytes[1], bytes[2], bytes[3])); break;
    }
    ret.sin6.sin6_port = A->sin.sin_port;
    break;
  }
  case AF_INET6: ret.sin6 = A->sin6;
  }
  return ret;
}

struct in6_addr addr2net6(orwl_addr const*A) {
  return orwl_addr6(A).sin6.sin6_addr;
}
#endif


orwl_endpoint*
orwl_endpoint_local(int fd, orwl_endpoint * A) {
  if (P99_UNLIKELY(fd == -1
                   || !A
                   || orwl_getsockname(fd, &A->addr)
                   || getsockopt(fd, SOL_SOCKET, SO_TYPE, &A->sock_type)))
    P99_THROW_ERRNO;
  return A;
}

orwl_endpoint*
orwl_endpoint_remote(int fd, orwl_endpoint * A) {
  if (P99_UNLIKELY(fd == -1
                   || !A
                   || orwl_getpeername(fd, &A->addr)
                   || getsockopt(fd, SOL_SOCKET, SO_TYPE, &A->sock_type)))
    P99_THROW_ERRNO;
  return A;
}

int orwl_socket_tune(int fd) {
  if (fd != -1) {
#ifndef ORWL_TCP_DELAY
    orwl_tcp_nodelay(fd, true);
#endif
    errno = 0;
#ifdef SO_REUSEADDR
    /* Since we are doing blocking send / receive the probability that
       we have a walking duplicate of an ancient package is
       minimal. Thus allow the reuse of ports. */
    if (orwl_setsockflag(fd, SOL_SOCKET, SO_REUSEADDR, true) < 0)
      P99_HANDLE_ERRNO {
P99_XDEFAULT :
      perror("setting socket to SO_REUSEADDR failed");
    }
#endif
#ifdef SO_LINGER
    /* We will never have pending packets since all messages are
       acknowledge by our protocol. Ensure that a socket close always
       places and RST and not a FIN message. By that we avoid pending
       TIME_WAIT sockets hanging arround. */
    if (setsockopt(fd, SOL_SOCKET, SO_LINGER,
                   &P99_LVAL(struct linger,
                             .l_onoff = 1,
                             .l_linger = 10),
                   sizeof(struct linger)) < 0)
      P99_HANDLE_ERRNO {
P99_XDEFAULT :
      perror("setting socket to linger failed");
    }
  }
#endif
  return fd;
}

int orwl_setsockopt(int fd, int lev, int optname, int optval) {
  return setsockopt(fd, lev, optname, &P99_LVAL(int, optval), sizeof(int));
}

int orwl_setsockflag(int fd, int lev, int optname, bool optval) {
  return setsockopt(fd, lev, optname, &P99_LVAL(int, optval), sizeof(int));
}

int orwl_getsockopt(int fd, int lev, int optname) {
  int ret = 0;
  if (getsockopt(fd, lev, optname, &ret, &P99_LVAL(socklen_t, sizeof ret))) {
    ret = -errno;
    errno = 0;
  }
  return ret;
}

int orwl_socket_error(int fd) {
  int ret = 0;
  if (getsockopt(fd, SOL_SOCKET, SO_ERROR, &ret, &P99_LVAL(socklen_t, sizeof ret))) {
    ret = errno;
    errno = 0;
  }
  return ret;
}

void orwl_socket_close(int fd) {
  /* A dummy buffer used to flush the sockets. This may be static
     since we don't care at all for the contents; threads might write
     to this in parallel. */
  static unsigned char o_rwl_cb[2 << 12];
  fflush(0);
  if (fd >= 0) ORWL_TIMER() {
    /* clear all errors on the socket */
    orwl_socket_error(fd);
    ORWL_TIMER(select_loop)
    /* don't accept connections anymore */
    if (!shutdown(fd, SHUT_RDWR)) {
      fd_set rfds;
      for (unsigned tries = 10; tries; --tries) {
        errno = 0;
        /* See if anybody is still talking to us. */
        FD_ZERO(&rfds);
        FD_SET(fd, &rfds);
        int ret = 0;
        ORWL_TIMER(select)
        ret = select(fd + 1, &rfds, 0, 0, &P99_LVAL(struct timeval, .tv_usec = 1000));
        /* we had a time out so everything is fine */
        if (!ret) break;
        if (ret < 0) {
          /* the socket is down the drain */
          if (errno != EINTR) break;
        } else {
          ssize_t re = 0;
          ORWL_TIMER(read)
          re = read(fd, o_rwl_cb, sizeof o_rwl_cb);
          trace(orwl_verbose && re >= 0,
                "Somebody was still talking on %d, emptied pipe by %zd bytes",
                fd, re);
          if (re < 0) break;
        }
      }
    }
    /* no we may start to think of really closing it */
    errno = 0;
    ORWL_TIMER(close_loop)
    while (close(fd) && errno == EINTR)
      errno = 0;
    errno = 0;
  }
}

static char o_rwl_byte[1];

void orwl_socket_sig_close(int fd) {
  register int const flag = 0
#ifdef MSG_OOB
                            | MSG_OOB
#endif
#ifdef MSG_DONTWAIT
                            | MSG_DONTWAIT
#endif
#ifdef MSG_NOSIGNAL
                            | MSG_NOSIGNAL
#endif
                            ;
  /* send a dummy byte to the other side to acknowledge the end of
     the conversation. */
  while (!send(fd, o_rwl_byte, 1, flag)) errno = 0;
  errno = 0;
  while (close(fd) && errno == EINTR)
    errno = 0;
  errno = 0;
}

void orwl_socket_wait_close(int fd) {
  register int const flag = 0
#ifdef MSG_OOB
                            | MSG_OOB
#endif
#ifdef MSG_WAITALL
                            | MSG_WAITALL
#endif
                            ;
  /* An ACK of that last message either comes to us through a
     successful recv operation or if the other side closes the
     connection. Both are fine since we don't receive any data, only
     the information that the other side has read our message (or
     wouldn't be able to read it, anyhow) is important. */
  while (!recv(fd, o_rwl_byte, 1, flag)) errno = 0;
  errno = 0;
  while (close(fd) && errno == EINTR)
    errno = 0;
  errno = 0;
}

P99_INSTANTIATE(orwl_addr*, orwl_addr_init, orwl_addr *);
P99_INSTANTIATE(struct in_addr, addr2net, orwl_addr const*);
P99_INSTANTIATE(bool, orwl_addr_eq, orwl_addr const*, orwl_addr const*);
P99_INSTANTIATE(bool, orwl_endpoint_eq, orwl_endpoint const*, orwl_endpoint const*);
P99_INSTANTIATE(bool, orwl_endpoint_similar, orwl_endpoint const*, orwl_endpoint const*);
P99_INSTANTIATE(in_port_t, orwl_addr_get_port, orwl_addr const*);
P99_INSTANTIATE(orwl_endpoint*, orwl_endpoint_init, orwl_endpoint *, orwl_addr, uint64_t);
P99_INSTANTIATE(orwl_addr*, orwl_addr_set_port, orwl_addr*, in_port_t);
P99_INSTANTIATE(int, orwl_getsockname, int, orwl_addr*);
P99_INSTANTIATE(int, orwl_getpeername, int, orwl_addr*);
P99_INSTANTIATE(int, orwl_getsockflag, int, int, int, bool *);
P99_INSTANTIATE(int, orwl_tcp_nodelay, int, bool);
P99_INSTANTIATE(void, orwl_tcp_cork, int, bool);
P99_INSTANTIATE(int, orwl_accept, int, orwl_addr*);
P99_INSTANTIATE(int, orwl_connect, int, orwl_addr const*);
