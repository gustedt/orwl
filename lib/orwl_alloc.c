/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the author and copyright holder for this work is                           */
/* all rights reserved,  2012-2014, 2016 Jens Gustedt, INRIA, France          */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#include "orwl_server.h"
#include "p99_new.h"
#include "p99_try.h"

void *mremap(void *old_address, size_t old_size,
             size_t new_size, int flags, ... /* void *new_address */);

#define mmap(...) P99_THROW_CALL_VOIDP(mmap, ENOMEM, __VA_ARGS__)
#define mremap(...) P99_THROW_CALL_VOIDP(mremap, ENOMEM, __VA_ARGS__)

#define close(...) P99_THROW_CALL_ZERO(close, 0, __VA_ARGS__)
#define fstat(...) P99_THROW_CALL_ZERO(fstat, 0, __VA_ARGS__)
#define ftruncate(...) P99_THROW_CALL_ZERO(ftruncate, 0, __VA_ARGS__)
#define munmap(...) P99_THROW_CALL_ZERO(munmap, 0, __VA_ARGS__)
#define shm_unlink(...) P99_THROW_CALL_ZERO(shm_unlink, 0, __VA_ARGS__)
#define unlink(...) P99_THROW_CALL_ZERO(unlink, 0, __VA_ARGS__)

#define open(...) P99_THROW_CALL_NEG(open, 0, __VA_ARGS__)
#define shm_open(...) P99_THROW_CALL_NEG(shm_open, 0, __VA_ARGS__)

P99_INSTANTIATE(void*, orwl_alloc_realloc, orwl_alloc*, size_t);
P99_INSTANTIATE(orwl_alloc*, orwl_alloc_init, orwl_alloc*, char const*, orwl_alloc_realloc_func, size_t, size_t);
P99_INSTANTIATE(void, orwl_alloc_destroy, orwl_alloc*);
P99_INSTANTIATE(void, orwl_alloc_delete, orwl_alloc const*);
P99_INSTANTIATE(char const*, orwl_alloc_getname, size_t, char[32]);

P99_TP_REF_DEFINE(orwl_alloc);

P99_DECLARE_STRUCT(o_rwl_fd);

struct o_rwl_fd {
  int fd;
  orwl_alloc_cases mode;
  void* data;
};

static
void o_rwl_mmap(o_rwl_fd* a, size_t offset, size_t size) {
  P99_CONSTANT(int, prot, PROT_READ | PROT_WRITE);
#ifdef MAP_POPULATE
  int flags = MAP_POPULATE;
#else
  int flags = 0;
#endif
  if (a->fd >= 0) {
    switch (a->mode) {
    case orwl_alloc_fetch:
      flags |= MAP_PRIVATE;
      break;
    default:
      flags |= MAP_SHARED;
      break;
    }
  } else {
    flags |= MAP_PRIVATE;
#ifdef MAP_ANONYMOUS
    flags |= MAP_ANONYMOUS;
#endif
  }
  a->data = mmap(0, size, prot, flags, a->fd, offset);
}

static
void o_rwl_fd_destroy(o_rwl_fd* file) {
  if (file && (file->fd >= 0))
    close(file->fd);
}

P99_DECLARE_DELETE(o_rwl_fd, static);

static
void* o_rwl_fd_realloc(orwl_alloc* a, size_t size) {
  if (P99_UNLIKELY(!a)) P99_THROW(EINVAL);
  register o_rwl_fd*const file = a->data;
  register size_t const osize = a->size;
  register size_t const offset = a->offset;
  register void*const data = a->data;
  /* Special rules apply if offset is not 0, because then it makes no
     sense to ask to resize the underlying file or segment. */
  if (offset) {
    switch(-size) {
    /* If this is a real size, just check that we have enough room. */
    default:
      if (offset+size > a->max_size)
        P99_THROW(EINVAL);
      file->mode = orwl_alloc_map;
      break;
    /* closing and unmapping is the same */
    case orwl_alloc_close:;
    case orwl_alloc_unmap: {
      if (data) {
        munmap(data, osize);
        file->mode = orwl_alloc_unmap;
        file->data = 0;
      }
      return 0;
    }
    case orwl_alloc_unlink: P99_THROW(EINVAL);
    case orwl_alloc_snatch:;
    case orwl_alloc_fetch:;
    case orwl_alloc_map:;
      if (!osize) {
        struct stat buf = P99_INIT;
        fstat(file->fd, &buf);
        a->max_size = buf.st_size;
      }
      if (offset+osize >= a->max_size)
        P99_THROW(EINVAL);
      file->mode = -size;
      if (osize) {
        size = osize;
      } else {
        /* Otherwise, if no valuable info for size can be found set it
           such that the mapped area reaches from offset to the end of
           the file. */
        size = a->max_size - offset;
      }
    }
    if (size != osize) {
      if (data) munmap(data, osize);
      o_rwl_mmap(file, a->offset, size);
      a->size = size;
    }
    return file->data;
  }
  if (-size <= (size_t)orwl_alloc_cases_max) {
    // Recover a new value for size in the special cases.
    switch(-size) {
    default: size = osize; break;
    case orwl_alloc_close: break;
    case orwl_alloc_unlink: P99_THROW(EINVAL);
    case orwl_alloc_unmap: break;
    case orwl_alloc_snatch:;
    case orwl_alloc_fetch:;
    case orwl_alloc_map:;
      file->mode = -size;
      if (osize) size = osize;
      else {
        struct stat buf = P99_INIT;
        fstat(file->fd, &buf);
        size = buf.st_size;
        a->max_size = size;
        break;
      }
    }
  } else file->mode = orwl_alloc_map;
  if (P99_UNLIKELY(osize == size)) {
    // nothing to do
    if (size) {
      if (!file) P99_THROW(EINVAL);
      else return file->data;
    } else {
      return 0;
    }
  } else {
    if (P99_UNLIKELY(!file)) P99_THROW(EINVAL);
    register void*const p = file->data;
    file->data = 0;
    a->size = 0;
    if (p) munmap(p, osize);
    if (size == ORWL_ALLOC_UNMAP) {
      file->mode = orwl_alloc_unmap;
      return 0;
    }
    if (size) {
      if (file->fd >= 0) {
        if (file->mode == orwl_alloc_snatch)
          /* Throw away any previous contents of the file, such that the
             mapping doesn't issue a read operation. */
          ftruncate(file->fd, 0);
        if (file->mode != orwl_alloc_fetch) {
          /* Tailor the file to the desired size. For snatch, the
             combination of the two ftruncate calls throws away old
             pages and zero-allocates new pages. */
          ftruncate(file->fd, size);
          a->max_size = size;
        }
      }
      o_rwl_mmap(file, a->offset, size);
      a->size = size;
      return file->data;
    } else {
      // destroy the handle and close the fd
      a->data = 0;
      o_rwl_fd_delete(file);
      return 0;
    }
  }
}

#define O_RWL_PREFIX_file ""
#define O_RWL_PREFIX_shm "/"


#define O_RWL_FD(K, OPEN, UNLINK)                                      \
typedef o_rwl_fd P99_PASTE2(o_rwl_realloc_, K);                        \
static                                                                 \
P99_PASTE2(o_rwl_realloc_, K)*                                         \
P99_PASTE3(o_rwl_realloc_, K, _init)(P99_PASTE2(o_rwl_realloc_, K)* f, \
                                     char const*name, size_t offset) { \
  if (f) {                                                             \
    *f = P99_RVAL(o_rwl_fd);                                           \
    if (name) {                                                        \
      unsigned dir = 1;                                                \
      if (name[0] == '<') {                                            \
        --dir;                                                         \
        ++name;                                                        \
      } else while (name[0] == '>') {                                  \
          ++dir;                                                       \
          ++name;                                                      \
      }                                                                \
      int flags = 0;                                                   \
      switch (dir) {                                                   \
      case 0:;                                                         \
      case 1:  flags = O_RDONLY; break;                                \
      case 2: flags = O_RDWR | O_CREAT | O_TRUNC; break;               \
      default: flags = O_RDWR | O_CREAT; break;                        \
      }                                                                \
      f->fd = OPEN(name, flags, S_IRUSR|S_IWUSR);                      \
    } else {                                                           \
      char tmpname[PATH_MAX];                                          \
      sprintf(tmpname,                                                 \
              P99_PASTE2(O_RWL_PREFIX_, K)                             \
              "orwl_alloc_tmp_%.08" PRIX64,                            \
              p99_rand());                                             \
      f->fd = OPEN(tmpname, O_RDWR | O_CREAT, S_IRUSR|S_IWUSR);        \
      UNLINK(tmpname);                                                 \
    }                                                                  \
  }                                                                    \
  return f;                                                            \
}                                                                      \
void* P99_PASTE2(orwl_alloc_realloc_, K)(orwl_alloc* a, size_t size) { \
  if (P99_UNLIKELY(!a)) P99_THROW(EINVAL);                             \
  if (-size <= (size_t)orwl_alloc_cases_max)                           \
    switch(-size) {                                                    \
    case orwl_alloc_close: break;                                      \
    case orwl_alloc_snatch: break;                                     \
    case orwl_alloc_fetch: break;                                      \
    case orwl_alloc_map: break;                                        \
    case orwl_alloc_unmap: break;                                      \
    case orwl_alloc_unlink:                                            \
      if (a->name) {                                                   \
        P99_TRY {                                                      \
          UNLINK(a->name);                                             \
        } P99_CATCH(int err) {                                         \
          switch (err) {                                               \
          case 0: break;                                               \
          case ENOENT: break;                                          \
          default: P99_RETHROW;                                        \
          }                                                            \
        }                                                              \
      }                                                                \
      return 0;                                                        \
    default:                                                           \
      TRACE(1, "unexpected %s (%s)",                                   \
            orwl_alloc_cases_getname(-size), size);                    \
      return 0;                                                        \
    }                                                                  \
  if (P99_UNLIKELY(size && !a->data)) {                                \
    /* allocate for the first time */                                  \
    P99_PASTE2(o_rwl_realloc_, K)* file                                \
      = P99_NEW(P99_PASTE2(o_rwl_realloc_, K), a->name, a->offset);    \
    if (P99_UNLIKELY(!file)) P99_THROW(ENOMEM);                        \
    /* obtain the size information from the fd */                      \
    struct stat buf = P99_INIT;                                        \
    if (file->fd >= 0                                                  \
        && !fstat(file->fd, &buf)                                      \
        && !(a->offset % buf.st_blksize)                               \
        && (a->offset+size < buf.st_size)) {                           \
      a->data = file;                                                  \
      a->size = buf.st_size;                                           \
      a->max_size = buf.st_size;                                       \
    } else {                                                           \
      o_rwl_fd_delete(file);                                           \
      P99_THROW_ERRNO;                                                 \
    }                                                                  \
  }                                                                    \
  return o_rwl_fd_realloc(a, size);                                    \
}                                                                      \
P99_MACRO_END(O_RWL_FD_, K)

O_RWL_FD(file, open, unlink);
O_RWL_FD(shm, shm_open, shm_unlink);

static
void* o_rwl_remap(void * odata, size_t msize, size_t size) {
  enum {
    prot = (PROT_READ | PROT_WRITE),
    mflags =
      (MAP_PRIVATE
#ifdef MAP_ANONYMOUS
       | MAP_ANONYMOUS
#endif
      ),
    fd = -1,
  };
  void * ret =
    /* If we have odata and mremap use it. Otherwise allocate a new
       segment and copy the data over. */
#ifdef MREMAP_MAYMOVE
    odata ?  mremap(odata, msize, size, MREMAP_MAYMOVE) :
#endif
    mmap(0, size, prot, mflags, fd, 0);
#ifndef MREMAP_MAYMOVE
  if (ret) {
    memcpy(ret, odata, msize);
    munmap(odata, msize);
  }
#endif
  return ret;
}

#ifdef MAP_ANONYMOUS
void* orwl_alloc_realloc_anon(orwl_alloc* a, size_t size) {
  if (!a) return 0;
  register void* adata = a->data;
  register size_t const asize = a->size;
  register size_t const msize = a->max_size;
  if (-size > (size_t)orwl_alloc_cases_max) {
    if (P99_LIKELY(size != asize)) {
      /* If we don't have remap, only copy the data if we can't avoid
         it. This is, as here, if the size grows, or later in case we
         close a segment who's max_size is larger than the actual
         content. */
#ifndef MREMAP_MAYMOVE
      if (size > msize)
#endif
        adata = o_rwl_remap(a->data, msize, size);
      a->data = adata;
      a->max_size = size;
      a->size = size;
    }
  } else if (adata) switch (-size) {
    case orwl_alloc_unlink:
      /* Return all memory that we allocated to the system. */
      munmap(adata, msize);
      a->data = 0;
      a->max_size = 0;
      a->size = 0;
      break;
    case orwl_alloc_close:
      /* Keep all memory that we allocated for the last non-null value
         of a->size. In case the platform does not have a native
         remap, we have to ensure that we truncate the segment to the
         last known value. */
#ifndef MREMAP_MAYMOVE
      if (asize != msize)
        o_rwl_remap(adata, msize, asize);
#endif
      a->max_size = asize;
      adata = 0;
      a->size = 0;
      break;
    case orwl_alloc_snatch:;
      if (!asize) {
        memset(adata, 0, msize);
        a->size = msize;
      }
      break;
    case orwl_alloc_fetch:;
    case orwl_alloc_map:;
      if (asize) break;
      /* Reconstitute all memory that we allocated. */
      a->size = msize;
      break;
    }
  return adata;
}
#endif

void* orwl_alloc_realloc_hard(orwl_alloc* a, size_t size) {
  if (!a) return 0;
  register void* adata = a->data;
  register size_t const asize = a->size;
  register size_t const msize = a->max_size;
  if (-size > (size_t)orwl_alloc_cases_max) {
    if (P99_LIKELY(size != asize)) {
      if (size < msize)
        a->size = size;
      else
        a->size = msize;
    }
  } else switch (-size) {
    case orwl_alloc_unlink:
      a->data = 0;
      a->max_size = 0;
      a->size = 0;
      break;
    case orwl_alloc_unmap:;
    case orwl_alloc_close:;
      /* Basically do nothing */
      a->size = 0;
      break;
    case orwl_alloc_fetch:;
    case orwl_alloc_map:;
      /* Reconstitute the whole chunk */
      a->size = msize;
      break;
    }
  return adata;
}

static
void* o_rwl_alloc_realloc_partial(orwl_alloc* a, size_t size) {
  if (!a) return 0;
  register orwl_alloc* other = a->data;
  if (!other) return 0;
  register char* ret = 0;
  register size_t const asize = a->size;
  register size_t const offset = a->offset;
  register size_t const osize = other->size;
  switch (-size) {
  case orwl_alloc_unmap:;
    /* do nothing */
    break;
  case orwl_alloc_close:;
  case orwl_alloc_unlink:
    a->data = 0;
    a->max_size = 0;
    a->size = 0;
    orwl_alloc_discount(other);
    break;
  case orwl_alloc_snatch:;
  case orwl_alloc_fetch:;
  case orwl_alloc_map:;
    size = asize ? asize : osize-offset;
  default:
    if (offset+size <= osize) {
      a->size = size;
      ret = orwl_alloc_realloc(other, ORWL_ALLOC_MAP);
      if (ret) ret += offset;
    }
    break;
  }
  return ret;
}

orwl_alloc* orwl_alloc_partial(orwl_alloc* a, size_t offset, size_t size) {
  orwl_alloc* ret = 0;
  if (a && size && (offset+size <= a->size)) {
    ret = P99_NEW(orwl_alloc, 0, o_rwl_alloc_realloc_partial, size, offset);
    if (ret) {
      orwl_alloc_account(a);
      ret->data = a;
      ret->size = size;
    }
  }
  return ret;
}


void orwl_alloc_copy(orwl_alloc_ref* ar, char const* name, orwl_alloc_realloc_func* allocf) {
  if (P99_UNLIKELY(!ar)) P99_THROW(EINVAL);
  orwl_alloc* ret = P99_NEW(orwl_alloc, name, allocf);
  orwl_alloc* a = orwl_alloc_ref_get(ar);
  size_t size = a->size;
  void* ndata = orwl_alloc_realloc(ret, size);
  void const* odata = orwl_alloc_realloc(a, size);
  memcpy(ndata, odata, size);
  /* This liberates the previous alloc and makes ar point to the
     copy. */
  orwl_alloc_ref_replace(ar, ret);
}

void orwl_alloc_copyone(orwl_alloc_ref* target, orwl_alloc_ref* source, char const* name, orwl_alloc_realloc_func* allocf) {
  if (P99_UNLIKELY(!source || !target)) P99_THROW(EINVAL);
  orwl_alloc_ref_mv(target, source);
  orwl_alloc* a = orwl_alloc_ref_get(target);
  size_t cnt = atomic_load(&a->p99_cnt);
  if (cnt > 1u) {
    /* This data has another client. Copy it. */
    orwl_alloc_copy(target, name, allocf);
  } else if (!P99_LIKELY(cnt)) {
    /* Otherwise the count had been 0. In real life this shouldn't
       happen. We don't have anything to do in any case. */
    P99_FPRINTF(stderr, "threre (%s) is no client (%s) ?\n", a, cnt);
  }
}
