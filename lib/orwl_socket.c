/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the author and copyright holder for this work is                           */
/* all rights reserved,  2010-2013 Jens Gustedt, INRIA, France                */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#include "orwl_socket.h"

#include "orwl_server.h"
#include "p99_qsort.h"

uint64_t orwl_hton64(uint64_t val);
uint64_t orwl_ntoh64(uint64_t val);

P99_DEFINE_ENUM(orwl_header_enum);

P99_INSTANTIATE(void, orwl_hton, uint64_t*, uint64_t const*, size_t);
P99_INSTANTIATE(void, orwl_ntoh, uint64_t*, uint64_t const*, size_t);

void orwl_ntoa(orwl_addr const* addr, char *restrict name) {
  orwl_endpoint ep = { .addr = *addr };
  orwl_endpoint_print(&ep, name);
}


enum {
  orwl_ip_privat_192 = orwl_ip_privat,
  orwl_ip_privat_172 = orwl_ip_privat,
  orwl_ip_privat_10 = orwl_ip_privat,
};


P99_DEFINE_ENUM(orwl_ip_scope);


orwl_ip_scope orwl_addr2scope(orwl_addr const* A) {
  orwl_ip_scope ret = orwl_ip_any;
  if (!A) return ret;
  switch (A->sa.sa_family) {
  case AF_INET: {
    uint32_t h = ntohl(A->sin.sin_addr.s_addr);
    if (!h) return orwl_ip_any;
    switch (ORWL_IN_MASK(h, 8)) {
    /* The loopback network */
    case 0x7F000000:
      return orwl_ip_loopback;
    /* The private /8 network */
    case 0x0A000000:
      return orwl_ip_privat_10;
    }
    switch (ORWL_IN_MASK(h, 16)) {
    /* Link privat addresses */
    case 0xA9FE0000: return orwl_ip_link;
    /* The private /16 network */
    case 0xC0A80000: return orwl_ip_privat_192;
    }
    /* The private /12 network */
    if (ORWL_IN_MASK(h, 12) == 0xAC100000) return orwl_ip_privat_172;
    else return orwl_ip_global;
  }
#if defined(POSIX_IPV6) && (POSIX_IPV6 > 0)
  case AF_INET6: {
    /**/ if (IN6_IS_ADDR_UNSPECIFIED(&A->sin6.sin6_addr)) ret = orwl_ip_any;
    else if (IN6_IS_ADDR_LOOPBACK(&A->sin6.sin6_addr)) ret = orwl_ip_loopback;
    else if (IN6_IS_ADDR_LINKLOCAL(&A->sin6.sin6_addr)) ret = orwl_ip_link;
    else if (IN6_IS_ADDR_SITELOCAL(&A->sin6.sin6_addr)) ret = orwl_ip_privat;
    else ret = orwl_ip_global;
  }
  break;
#endif
  case AF_UNIX: ret = orwl_ip_loopback; break;
  }
  return ret;
}

P99_INSTANTIATE(char const*, orwl_inet_ntop, orwl_addr const* addr, char*restrict buf, size_t size);
P99_DEFINE_DEFARG(orwl_inet_ntop,,, );

/* Implement a simple relation between protocol name and relevance.
   Unknown protocols will have a 0 in the table to trigger the
   replacement by the maximum value in the access function. */
#define O_RWL_AF_VAL(NAME, X, I)   [X] = P99_ADD(I, 1)
#define O_RWL_AF_VALS(...) P99_FOR(, P99_NARG(__VA_ARGS__), P00_SEQ, O_RWL_AF_VAL, __VA_ARGS__)

static
signed const o_rwl_af_orders[] = {
  O_RWL_AF_VALS(ORWL_AF_LIST)
};

enum { o_rwl_af_orders_len = P99_ALEN(o_rwl_af_orders) };

static
signed o_rwl_af_order(sa_family_t fam) {
  if (fam >= o_rwl_af_orders_len || !o_rwl_af_orders[fam]) return P99_NARG(ORWL_AF_LIST);
  else return o_rwl_af_orders[fam] - 1;
}

static
signed o_rwl_compar_addr(void const * a, void const * b, void* context) {
  orwl_addr const* A = a;
  orwl_addr const* B = b;
  // The first sort criterion is the scope
  orwl_ip_scope ia = orwl_addr2scope(A);
  orwl_ip_scope ib = orwl_addr2scope(B);
  int64_t diff = ib - ia;
  /* If that is not decisive, compare the addresses themselves. */
  if (P99_UNLIKELY(!diff)) {
    sa_family_t fa = A->sa.sa_family;
    sa_family_t fb = B->sa.sa_family;
    if (fa == fb) {
      switch (fa) {
      case AF_INET: return memcmp(&A->sin.sin_addr.s_addr, &B->sin.sin_addr.s_addr, sizeof(in_addr_t));
#if POSIX_IPV6 > 0
      case AF_INET6: return memcmp(A->sin6.sin6_addr.s6_addr, B->sin6.sin6_addr.s6_addr, sizeof A->sin6.sin6_addr.s6_addr);
#endif
      default: return memcmp(A->raw.raw_data, B->raw.raw_data, sizeof A->raw.raw_data);
      }
    } else {
      return o_rwl_af_order(fa) - o_rwl_af_order(fb);
    }
  }
  return diff;
}

orwl_addr* orwl_addr_sort(size_t n, orwl_addr ali[n]) {
  qsort_s(ali, n, sizeof *ali, o_rwl_compar_addr, 0);
  return ali;
}

orwl_addr * orwl_addr_aliases(char const *name) {
  struct addrinfo *res = 0;
  struct addrinfo hints = {
    .ai_family = AF_UNSPEC,
    .ai_flags = AI_V4MAPPED | AI_ALL | AI_CANONNAME | (name ? 0 : AI_PASSIVE),
    .ai_socktype = SOCK_STREAM,
  };
  orwl_addr * ali = 0;
  orwl_addr * act = 0;
  if (getaddrinfo(name, P99_0(char*), &hints, &res)) {
    ali = calloc(sizeof *ali, 1);
    act = ali;
  } else {
    size_t total = 0;
    for (struct addrinfo *p = res; p; p = p->ai_next)
      ++total;
    report(0, "%s's canonical name is %s, we found %zu addresses",
           (name ? name : "<unspecific>"),
           (res ? res->ai_canonname : "unknonwn"),
           total);
    // The last element will be all 0 as a stop marker.
    ali = calloc(sizeof *ali, total + 1);
    act = ali;
    for (struct addrinfo *p = res; p; p = p->ai_next) {
      memcpy(act, p->ai_addr, p->ai_addrlen);
      ++act;
    }
  }
  if (res) freeaddrinfo(res);
  return orwl_addr_sort(act - ali, ali);
}

static
signed const o_rwl_st_orders[] = {
  O_RWL_AF_VALS(ORWL_SOCK_TYPE)
};

enum { o_rwl_st_orders_len = P99_ALEN(o_rwl_st_orders) };

static
signed o_rwl_st_order(unsigned st) {
  if (st >= o_rwl_st_orders_len || !o_rwl_st_orders[st]) return -1;
  else return o_rwl_st_orders[st] - 1;
}

static
char const* o_rwl_st_names[] = {
  ORWL_SOCK_NAME
};

char const* orwl_sock_type_name(unsigned st) {
  signed pos = o_rwl_st_order(st);
  if (pos < 0) return "";
  else return o_rwl_st_names[pos];
}

#define O_RWL_ST_INV(NAME, X, I)   [I] = X
#define O_RWL_ST_INVS(...) P99_FOR(, P99_NARG(__VA_ARGS__), P00_SEQ, O_RWL_ST_INV, __VA_ARGS__)


static
signed const o_rwl_st_invs[] = {
  O_RWL_ST_INVS(ORWL_SOCK_TYPE)
};

enum { o_rwl_st_invs_len = P99_ALEN(o_rwl_st_invs) };

static
signed o_rwl_st_inv(unsigned st) {
  if (st >= o_rwl_st_invs_len || !o_rwl_st_invs[st]) return -1;
  else return o_rwl_st_invs[st];
}

signed
orwl_sock_type_get(char const* name) {
  for (unsigned i = 0; i < P99_NARG(ORWL_SOCK_NAME); ++i)
    if (!strcmp(name, o_rwl_st_names[i])) return o_rwl_st_inv(i);
  return -1;
}



P99_INSTANTIATE(char const*, hostname, char *, size_t);
