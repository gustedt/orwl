/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the author and copyright holder for this work is                           */
/* all rights reserved,  2010-2011, 2013 Jens Gustedt, INRIA, France          */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#include "orwl_circulate.h"

P99_INSTANTIATE(void, orwl_circulate_disconnect, orwl_circulate*);
P99_INSTANTIATE(void const*, orwl_circulate_map, orwl_circulate*);

void orwl_circulate_insert(orwl_circulate*const circ,    /*!< [in,out] the handle(s) for the circulation request */
                           size_t loc,                   /*!< the relative location id */
                           size_t sense,                 /*!< the sense of the circulation, defaults to 1. */
                           p99_seed* seed,               /*!< [in,out] defaults to a thread local seed */
                           orwl_server * srv,
                           size_t orwl_locations_amount, /*!< the number of locations per task */
                           orwl_mirror * location        /*!< [in,out] the location(s) for the request */
                          ) {
  register size_t const mytid = ORWL_MYTID(srv->ab);
  register size_t const here = ORWL_LOCATION(mytid, loc);
  register size_t const other = ORWL_LOCATION((mytid + sense) % ORWL_NT(srv->ab), loc);
  register bool const sameProcess = orwl_endpoint_similar(&srv->ab->eps[here], &srv->ab->eps[other]);
  trace(orwl_verbose, "locations %zu and %zu are handled in %s",
        here, other,
        (sameProcess ? "the same process" : "different processes"));

  orwl_handle2_write_insert(&circ->hand[0], here, 0, seed, srv, location);
  orwl_handle2_read_insert(&circ->hand[1], here, 1, seed, srv, location);
  if (!sameProcess)
    /* If the other location is handled by a different process, we
       insert the request concurrently to the other read request that
       that server has for its own location. Thereby the data transfer
       can be done in parallel to the remote handling and we may thus
       overlap the communication. */
    orwl_handle2_read_insert(&circ->hand[2], other, 1, seed, srv, location);
  else
    /* If the other location is handled by the same process, we insert
       the request as exclusive behind our own inclusive
       request. Thereby we can get hold of the orwl_alloc exclusively
       and avoid to copy the data. */
    orwl_handle2_write_insert(&circ->hand[2], other, 2, seed, srv, location);
}

P99_DECLARE_STRUCT(o_rwl_circulate);
P99_DEFINE_STRUCT(o_rwl_circulate,
                  orwl_circulate *circ
                 );

static
o_rwl_circulate* o_rwl_circulate_init(o_rwl_circulate *el,
                                      orwl_circulate *circ) {
  if (el) {
    *el = (o_rwl_circulate) {
      .circ = circ,
    };
  }
  return el;
}

static
void o_rwl_circulate_destroy(o_rwl_circulate *el) {
  // empty
}

P99_DECLARE_DELETE(o_rwl_circulate, static);
DECLARE_THREAD(o_rwl_circulate, static);

void orwl_circulate_replace(orwl_circulate* circ, orwl_alloc_ref* next, p99_seed* seed) {
  ORWL_TIMER() {
    ORWL_TIMER(acquire)
    orwl_handle2_acquire(&circ->hand[0], 1, seed);
    orwl_handle2_replace(&circ->hand[0], next, seed);
    orwl_alloc_ref_mv(&circ->current, next);
    orwl_handle2_next(&circ->hand[0], 1, seed);
    p99_notifier_unset(&circ->received);
    P99_THROW_CALL_THRD(o_rwl_circulate_create_detached, P99_NEW(o_rwl_circulate, circ));
  }
}

DEFINE_THREAD(o_rwl_circulate, static) {
  ORWL_THREAD_USE(o_rwl_circulate, circ);
  p99_seed* seed = p99_seed_get();
  ORWL_TIMER() {
    ORWL_TIMER(acquire)
    orwl_handle2_acquire(&circ->hand[2], 1, seed);
    orwl_handle2_hold(&circ->hand[2], &circ->subsequent, seed);
    orwl_handle2_next(&circ->hand[2], 1, seed);
    p99_notifier_set(&circ->received);
  }
}

void orwl_circulate_hold(orwl_circulate* circ, orwl_alloc_ref* next, p99_seed* seed) {
  ORWL_TIMER() {
    p99_notifier_block(&circ->received);
    orwl_alloc_ref_replace(&circ->current, 0);
    orwl_alloc_ref_mv(next, &circ->subsequent);
  }
}
