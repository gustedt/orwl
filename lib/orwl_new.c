/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the author and copyright holder for this work is                           */
/* all rights reserved,  2010-2011, 2013 Jens Gustedt, INRIA, France          */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#include "p99_threads.h"
#include "orwl_new.h"
#include "p99_callback.h"

static p99_callback_stack o_rwl_smalloc_pool = P99_LIFO_INITIALIZER(0);

static
void o_rwl_smalloc_free(void* p) {
  fprintf(stderr, "smalloc freeing %p\n", p);
  free(p);
}

/** **/
void* o_rwl_smalloc(size_t size, void* est) {
  if (est) return est;
  void * ret = malloc(size);
  P99_CALLBACK_PUSH(&o_rwl_smalloc_pool, o_rwl_smalloc_free, ret);
  return ret;
}

static
void o_rwl_smalloc_cleanup(void) {
  p99_callback(&o_rwl_smalloc_pool);
}

P99_DEFINE_ONCE_CHAIN(orwl_smalloc) {
  atexit(o_rwl_smalloc_cleanup);
}
