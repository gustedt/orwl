/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the authors and copyright holders for this work are as follows:            */
/* all rights reserved,  2011 Emmanuel Jeanvoine, INRIA, France               */
/* all rights reserved,  2016 Farouk Mansouri, INRIA, France                  */
/* all rights reserved,  2010-2014, 2016-2017 Jens Gustedt, INRIA, France     */
/* all rights reserved,  2013 Rodrigo Campos, INRIA, France                   */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#include "orwl_socket.h"
#include "orwl_server.h"
#include "orwl_affinity.h"

P99_DEFINE_ONCE_CHAIN(orwl_mirror, orwl_wq, orwl_rand, orwl_handle) {
  // empty
}

P99_DEFINE_ONCE_CHAIN(orwl_handle, orwl_mirror, orwl_wh) {
  // empty
}

P99_INSTANTIATE(orwl_mirror*, orwl_mirror_init, orwl_mirror*, orwl_server*, orwl_endpoint);
P99_INSTANTIATE(void, orwl_mirror_destroy, orwl_mirror*);

DEFINE_NEW_DELETE(orwl_mirror);

void orwl_mirror_connect(orwl_mirror *rq, orwl_endpoint endp, int64_t index, orwl_server* srv) {
  if (index >= 0) endp.index = index;
  orwl_mirror_init(rq, srv, endp);
}

P99_INSTANTIATE(orwl_handle*, orwl_handle_init, orwl_handle*);
P99_INSTANTIATE(void, orwl_handle_destroy, orwl_handle*);

DEFINE_NEW_DELETE(orwl_handle);

P99_INSTANTIATE(bool, orwl_inclusive, orwl_handle*);
P99_INSTANTIATE(orwl_state, orwl_handle_acquire, orwl_handle*, size_t, p99_seed*);
P99_INSTANTIATE(orwl_state, orwl_handle_test, orwl_handle*, size_t, p99_seed*);

P99_INSTANTIATE(void, orwl_handle_replace, orwl_handle*, orwl_alloc_ref*, p99_seed*);
P99_INSTANTIATE(void, orwl_handle_hold, orwl_handle*, orwl_alloc_ref*, p99_seed*, bool);


static
orwl_state orwl_write_request_static(orwl_mirror * rq, orwl_handle * rh, p99_seed *seed, o_rwl_grouping* p) {
  orwl_state state = orwl_invalid;
  ORWL_TIMER() {
    if (rq && rh && !orwl_wh_ref_get(&rh->whr)) {
      // insert two wh in the local queue
      // this one will be referenced locally
      orwl_wh* wh_req = P99_NEW(orwl_wh, 1);
      orwl_wh_ref whr = ORWL_WH_REF_INITIALIZER(wh_req);
      // this one will be referenced remotely
      orwl_wh* wh_rcb = P99_NEW(orwl_wh, 1, wh_req);
      state = orwl_wq_request(&rq->local, wh_rcb);
      P99_THROW_ASSERT(ENOLCK, state == orwl_requested);
      P99_THROW_ASSERT(EINVAL, !rh->rq);
      /* Send the insertion request with the id of wh_rcb to the other
         side. As result retrieve the ID on the other side that is to be
         released when we release here. */
      if (!p) ORWL_TIMER(rpc) {
        rh->svrID = orwl_rpc(rq->srv, &rq->there, seed, orwl_proc_write_request,
                             rq->there.index,
                             (uintptr_t)wh_rcb,
                             orwl_addr_get_port(&rq->srv->ep.addr)
                            );
        trace(rh->svrID == UINT64_MAX, "warning: received poisoned return from orwl_rpc");
      } else ORWL_TIMER(insert_rpc) {
        uint64_t remP = p->remP;
        p99_notifier_set(&p->launched);
        p = 0;
        rh->svrID = orwl_rpc(rq->srv, &rq->there, seed, orwl_proc_write_insert,
                             remP,
                             rq->there.index,
                             (uintptr_t)wh_rcb,
                             orwl_addr_get_port(&rq->srv->ep.addr)
                            );
        trace(rh->svrID == UINT64_MAX, "warning: received poisoned return from orwl_rpc");
      }
      P99_THROW_ASSERT(EINVAL, rh->svrID && (rh->svrID != ORWL_SEND_ERROR));
      P99_THROW_ASSERT(EINVAL, !orwl_wh_ref_get(&rh->whr));

      /* Link us to the queue and the handle */
      orwl_wh_ref_mv(&rh->whr, &whr);
      rh->rq = rq;
    }
  }
  return state;
}

orwl_state orwl_handle_write_request(orwl_mirror rq0[], orwl_handle rh0[], size_t size, p99_seed *seed) {
  orwl_state state = orwl_invalid;
  if (rq0 && rh0)
    for (size_t i = 0; i < size; ++i)
      state = orwl_write_request_static(rq0 + i, rh0 + i, seed, 0);
  return state;
}

static
orwl_state orwl_read_request_static(orwl_mirror * rq, orwl_handle * rh, p99_seed *seed, o_rwl_grouping* p) {
  orwl_state state = orwl_invalid;
  ORWL_TIMER() {
    orwl_wh* wh_rcb = 0;
    orwl_wh_ref wh_incr = ORWL_WH_REF_INITIALIZER(0);
    if (rq && rh && !orwl_wh_ref_get(&rh->whr)) {
      /* Create two handles that are to be inserted, here, in case it
         is not possible to reuse an existing read handle. Do this
         before we enter the critical section.*/
      // this one will be referenced locally
      orwl_wh* wh_req = P99_NEW(orwl_wh, 1);
      // this one will be referenced remotely
      wh_rcb = P99_NEW(orwl_wh, 1, wh_req);
      orwl_wq * wq = &rq->local;
      P99_MUTUAL_EXCLUDE(wq->mut) {
        /* first try to piggyback the latest wh in the local list */
        for (;;) {
          state = o_rwl_wq_try_request_locked(wq, &wh_incr);
          if (P99_LIKELY(state != orwl_again)) break;
          /* If there is just one element in the queue, that element may
             be just being released. Capture that event and unlock the
             mutex to let the releasing thread finish its work. The lock
             and retry. */
          P99_THROW_CALL_THRD(mtx_unlock, &wq->mut);
          P99_THROW_CALL_THRD(mtx_lock, &wq->mut);
        }
        orwl_wh* wh_inc = orwl_wh_ref_get(&wh_incr);
        P99_THROW_ASSERT(EINVAL, !wh_inc || wh_inc->svrID);
        P99_THROW_ASSERT(EINVAL, !wh_inc || (state == orwl_requested));

        /* Send the insertion request to the other side. This consists
           of sending wh_rcb, for the case that this is will be
           considered a new request, and the svrID that was memorized on
           wh_inc, if any. As result retrieve the ID on the other side
           that is to be released when we release here. */
        if (P99_LIKELY(!p)) ORWL_TIMER(rpc) {
          rh->svrID = orwl_rpc(rq->srv, &rq->there, seed, orwl_proc_read_request,
                               rq->there.index,
                               (uintptr_t)wh_rcb,
                               wh_inc ? wh_inc->svrID : 0,
                               orwl_addr_get_port(&rq->srv->ep.addr)
                              );
          trace(rh->svrID == UINT64_MAX, "warning: received poisoned return from orwl_rpc");
        } else {
          uint64_t remP = p->remP;
          p99_notifier_set(&p->launched);
          p = 0;
          /* Since we compute the insertions in order, in insert mode
             there is no risk that another insert invalidates a remote
             read insertion that is already present. We only have to
             watch that we notify the remote insertion request,
             here. */
          if (wh_inc) ORWL_TIMER(insert_again_rpc) {
            rh->svrID = wh_inc->svrID;
            orwl_rpc(rq->srv, &rq->there, seed, orwl_proc_read_insert_again, remP);
          } else ORWL_TIMER(insert_rpc) {
            rh->svrID = orwl_rpc(rq->srv, &rq->there, seed, orwl_proc_read_insert,
                                 remP,
                                 rq->there.index,
                                 (uintptr_t)wh_rcb,
                                 orwl_addr_get_port(&rq->srv->ep.addr)
                                );
            trace(rh->svrID == UINT64_MAX, "warning: received poisoned return from orwl_rpc");
          }
        }
        P99_THROW_ASSERT(EINVAL, rh->svrID);

        /* Link us to rq */
        rh->rq = rq;
        if (wh_inc && wh_inc->svrID == rh->svrID) {
          /* remote and local queue have still the same last element, a
             corresponding inclusive pair at the tail of the their
             list. */
          orwl_wh_ref_mv(&rh->whr, &wh_incr);
        } else {
          /* A pair of new handles is inserted in the local queue. The
             first is the dummy handle that will be used by the remote
             to notify us when the lock is achieved. */
          P99_THROW_ASSERT(EINVAL, !orwl_wh_ref_get(&rh->whr));
          orwl_wh_ref_replace(&rh->whr, wh_req);
          wh_req->svrID = rh->svrID;
          o_rwl_wq_request_append_locked(wq, wh_rcb);
          wh_rcb = 0;
          // If we added a token to an existing handle but now we
          // see that this corresponds to a different priority
          // (because remotely there was something in between) we
          // have to unload this again.
          orwl_wh_ref_trigger(&wh_incr);
        }
      }
    }

    /* Finally we didn't use that dummy element. */
    if (wh_rcb) {
      o_rwl_wh_trigger(wh_rcb);
      orwl_wh_ref_destroy(&wh_rcb->next);
      orwl_wh_delete(wh_rcb);
    }

    state = orwl_requested;
  }
  return state;
}

orwl_state orwl_handle_read_request(orwl_mirror rq0[], orwl_handle rh0[], size_t size, p99_seed *seed) {
  orwl_state state = orwl_invalid;
  if (rq0 && rh0)
    for (size_t i = 0; i < size; ++i)
      state =orwl_read_request_static(rq0 + i, rh0 + i, seed, 0);
  return state;
}

#define O_RWL_GRQ_TASK_TYPES                                   \
o_rwl_grouping* p,                                             \
  orwl_mirror * rq,                                            \
  orwl_handle * rh,                                            \
  size_t myloc,                                                \
  orwl_thread_cntrl *det,                                      \
  bool incl

#define O_RWL_GRQ_TASK_NAMES p, rq, rh, myloc, det, incl

P99_DECLARE_STRUCT(o_rwl_grq_task);
P99_DEFINE_STRUCT(o_rwl_grq_task, O_RWL_GRQ_TASK_TYPES);

static
o_rwl_grq_task* o_rwl_grq_task_init(o_rwl_grq_task* task, O_RWL_GRQ_TASK_TYPES) {
  if (task) {
    *task = P99_STRUCT_LITERAL(o_rwl_grq_task, O_RWL_GRQ_TASK_NAMES);
  }
  return task;
}

static
void o_rwl_grq_task_destroy(o_rwl_grq_task *task) {
  /* empty */
}

P99_DECLARE_DELETE(o_rwl_grq_task, static);

DECLARE_THREAD(o_rwl_grq_task, static inline);

DEFINE_THREAD(o_rwl_grq_task, static inline) {
  ORWL_TIMER() {
    ORWL_THREAD_USE(o_rwl_grq_task, O_RWL_GRQ_TASK_NAMES);
    p99_seed* seed = p99_seed_get();
    trace(0, "going rpc %s address family %d",
          orwl_endpoint_print(&rq->there), rq->there.addr.sa.sa_family);
    p->remP = orwl_rpc(rq->srv, &rq->there, seed, orwl_proc_request_insert,
                       p->pri, p->incl);
    trace(p->remP == -1, "warning: received poisoned return from orwl_rpc");
    orwl_thread_cntrl_freeze(det);
    orwl_thread_cntrl_wait_for_caller(det);
    ORWL_TIMER(block)
    p99_notifier_block(&p->ok);
    if (incl)
      ORWL_TIMER(read_request)
      orwl_read_request_static(rq, rh, seed, p);
    else
      ORWL_TIMER(write_request)
      orwl_write_request_static(rq, rh, seed, p);
    p99_count_dec(&rq->srv->waiters_cnt);
    if (rq->srv->outgraph) {
      fprintf(rq->srv->outgraph, "\"%zu\" -> \"%s\" { label = \"%s\" }\n",
              myloc,
              orwl_endpoint_print(&rq->there),
              bool_getname(incl));
    }
    free(det);
  }
}

static
void o_rwl_insert(orwl_mirror * rq, orwl_handle * rh, uint64_t priority, p99_seed* seed, bool incl) {
  ORWL_TIMER()
  if (rq && rh) {
    p99_count_inc(&rq->srv->waiters_cnt);
    o_rwl_grouping* p = P99_NEW(o_rwl_grouping, priority, incl);
    P99_LIFO_PUSH(&rq->srv->waiters_local, p);
    orwl_thread_cntrl *det = P99_NEW(orwl_thread_cntrl);
    size_t myloc = orwl_myloc;
    o_rwl_grq_task * task = P99_NEW(o_rwl_grq_task, O_RWL_GRQ_TASK_NAMES);
    P99_THROW_CALL_THRD(o_rwl_grq_task_launch, task, det);
    orwl_thread_cntrl_wait_for_callee(det);
    orwl_thread_cntrl_detach(det);
  }
}

void orwl_handle_write_insert(orwl_handle* rh, size_t locid, size_t priority, p99_seed* seed, orwl_server* srv, orwl_mirror*rq) {
  /// farouk patch ///
#if HAVE_AFFINITY > 0
  orwl_dependency_set(orwl_myloc, locid, locid);
#endif
  /// farouk patch end ///
  if (locid == SIZE_MAX) locid = ORWL_MYLOC(srv->ab);
  if (priority == SIZE_MAX) priority = ORWL_MYLOC(srv->ab);
  if (!rq) rq = &srv->ab->mirrors[locid];
  o_rwl_insert(rq, rh, priority, seed, false);
}

void orwl_handle_read_insert(orwl_handle* rh, size_t locid, size_t priority, p99_seed* seed, orwl_server* srv, orwl_mirror*rq) {
  /// farouk patch ///
#if HAVE_AFFINITY > 0
  //printf(" Reading: Thread id %d Location id %d -> Thread id %d location id %d \n", orwl_myloc/3  , orwl_myloc , locid/3, locid);
  orwl_dependency_set(orwl_myloc, locid, locid);
#endif
  /// farouk patch end ///
  if (priority == SIZE_MAX) priority = ORWL_MYLOC(srv->ab);
  if (!rq) rq = &srv->ab->mirrors[locid];
  o_rwl_insert(rq, rh, priority, seed, true);
}

orwl_state orwl_handle_release(orwl_handle rh0[], size_t size, p99_seed *seed) {
  orwl_state state = orwl_valid;
  for (size_t i = 0; i < size; ++i)
    ORWL_TIMER(body) {
    orwl_handle *rh = rh0 + i;
    P99_THROW_ASSERT(EINVAL, rh);
    /* To be able to re-initialize rh in the middle of the procedure,
       store all necessary data on the stack.*/
    orwl_wh_ref whr = ORWL_WH_REF_INITIALIZER(0);
    orwl_wh_ref_mv(&whr, &rh->whr);
    orwl_wh* const wh = orwl_wh_ref_get(&whr);
    P99_THROW_ASSERT(ENOLCK, wh);
    orwl_mirror*const rq = rh->rq;
    uint64_t const svrID = rh->svrID;
    orwl_server *srv = rq->srv;
    orwl_endpoint const there = rq->there;
    orwl_wq*const wq = wh->location;
    P99_THROW_ASSERT(EINVAL, svrID);
    P99_THROW_ASSERT(EINVAL, !wh->svrID || svrID == wh->svrID);
    bool withdata = !wh->svrID;

    /* Detect if we are the last user of this handle.  We have to take
       care of the remote transfer/trigger between the moment the
       tokens drop to 0 and we release wh. */
    P99_WRLOCK(wq->rwlock)
    if (!orwl_wh_unload(wh)) {
      /* Ensure that the handles will not be highjacked by another
         read-request. */
      wh->svrID = 0;
      rh->svrID = 0;
      if (withdata)
        ORWL_TIMER(transfer)
        orwl_transfer(srv, there, wq, svrID);
      else
        ORWL_TIMER(trigger)
        orwl_trigger(srv, there, svrID);

      /* We were the last to have a reference to this handle so we may
         destroy it. */
      state = orwl_wh_release(wh);
    }
    orwl_wh_ref_destroy(&whr);
    orwl_handle_destroy(rh);
  }
  return state;
}

P99_INSTANTIATE(void, orwl_handle_resize, orwl_handle*, size_t);

P99_INSTANTIATE(void*, orwl_handle_write_map, orwl_handle*, size_t*, p99_seed*);
P99_INSTANTIATE(void*, orwl_handle_read_map, orwl_handle*, size_t*, p99_seed*);
P99_INSTANTIATE(size_t, orwl_handle_offset, orwl_handle*, p99_seed*);
P99_INSTANTIATE(void, orwl_handle_truncate, orwl_handle*, size_t, p99_seed*);
P99_INSTANTIATE(void, orwl_handle_attach, orwl_handle*, orwl_alloc_ref*, p99_seed*);
P99_INSTANTIATE(void, orwl_allocate,
                orwl_alloc_ref *,
                size_t, char const*, orwl_alloc_realloc_func*,
                size_t, orwl_server*);
P99_INSTANTIATE(void, orwl_allocate_hard,
                orwl_alloc_ref*,
                void*, size_t,
                size_t,
                char const*, orwl_server*);

P99_DECLARE_STRUCT(o_rwl_scale_state);


#define O_RWL_SCALE_STATE_TYPES_VARS                           \
 orwl_mirror* rq,                                              \
 size_t data_len,                                              \
 orwl_alloc_ref* ar,                                           \
 orwl_thread_cntrl *det

#define O_RWL_SCALE_STATE_VARS                                 \
  rq,                                                          \
  data_len,                                                    \
  ar,                                                          \
  det


/**
 ** @brief The internal state for a call to ::orwl_scale.
 **/
P99_DEFINE_STRUCT(o_rwl_scale_state, O_RWL_SCALE_STATE_TYPES_VARS);

static
o_rwl_scale_state* o_rwl_scale_state_init(o_rwl_scale_state* scale, O_RWL_SCALE_STATE_TYPES_VARS) {
  if (scale) {
    *scale = P99_STRUCT_LITERAL(o_rwl_scale_state, O_RWL_SCALE_STATE_VARS);
  }
  return scale;
}

static
void o_rwl_scale_state_destroy(o_rwl_scale_state* scale) {
  /* empty */
  free(scale->det);
}

P99_DECLARE_DELETE(o_rwl_scale_state, static);

DECLARE_THREAD(o_rwl_scale_state, static);

DEFINE_THREAD(o_rwl_scale_state, static) {
  ORWL_THREAD_USE(o_rwl_scale_state, O_RWL_SCALE_STATE_VARS);
  ORWL_TIMER() {
    p99_seed * seed = p99_seed_get();
    orwl_handle first = ORWL_HANDLE_INITIALIZER;
    orwl_handle_write_request(rq, &first, 1u, seed);
    orwl_thread_cntrl_freeze(det);
    orwl_thread_cntrl_wait_for_caller(det);
    orwl_handle_acquire(&first, 1u, seed);
    if (ar) orwl_handle_attach(&first, ar, seed);
    else orwl_handle_truncate(&first, data_len, seed);
    orwl_handle_release(&first, 1u, seed);
  }
}

void orwl_scale(size_t size, size_t pos, orwl_server* serv) {
  if (!serv) serv = orwl_server_get();
  if (pos == SIZE_MAX) pos = ORWL_MYLOC(serv->ab);
  /// patch farouk ///
#if HAVE_AFFINITY > 0
  //printf("Size of location %zu is %zu\n", pos, size);
  orwl_locations_size_set(pos,size);
#endif
  /// patch farouk end ///
  orwl_thread_cntrl * det = P99_NEW(orwl_thread_cntrl);
  if (!serv || !serv->ab || !serv->ab->mirrors) P99_THROW(EINVAL);
  orwl_mirror* mirr = &serv->ab->mirrors[pos];

  P99_THROW_CALL_THRD(o_rwl_scale_state_launch,
                      P99_NEW(o_rwl_scale_state,
                              mirr,
                              size,
                              (orwl_alloc_ref*)0,
                              det),
                      det);
  orwl_thread_cntrl_wait_for_callee(det);
  orwl_thread_cntrl_detach(det);
}

void orwl_attach(orwl_alloc_ref* ar, size_t pos, orwl_server* serv) {
  if (!serv) serv = orwl_server_get();
  if (pos == SIZE_MAX) pos = ORWL_MYLOC(serv->ab);
  orwl_thread_cntrl * det = P99_NEW(orwl_thread_cntrl);
  if (!serv || !serv->ab || !serv->ab->mirrors) P99_THROW(EINVAL);
  orwl_mirror* mirr = &serv->ab->mirrors[pos];

  P99_THROW_CALL_THRD(o_rwl_scale_state_launch,
                      P99_NEW(o_rwl_scale_state,
                              mirr,
                              0,
                              ar,
                              det),
                      det);
  orwl_thread_cntrl_wait_for_callee(det);
  orwl_thread_cntrl_detach(det);
}
