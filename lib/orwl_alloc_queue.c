/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the author and copyright holder for this work is                           */
/* all rights reserved,  2016 Jens Gustedt, INRIA, France                     */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#include "orwl_alloc.h"
#include "orwl_server.h"
#include "orwl_helpers.h"
#include "orwl_generic.h"
#include "p99_threads.h"
#include "p99_cm.h"

P99_DECLARE_STRUCT(aq);

struct aq {
  p99_cm cm;               //!< protect this data structure and signal changes
  unsigned max;            //!< maximum number of elements
  unsigned w;              //!< actual write position (mod max)
  unsigned r;              //!< actual read position (mod max)
  unsigned users;          //!< number of users of this queue
  orwl_alloc_ref d[];      //!< the stored elements in position mod max
};

/* We always have one reader thread and one writer thread and the
   task-thread that sees it twice. */
enum { aq_users = 4, };

/**
 ** @brief Size of an @c aq if the flexible array member has @a max
 ** entries.
 **/
static
size_t aq_size(unsigned max) {
  return offsetof(aq, d)+sizeof(aq[max]);
}

/**
 ** @brief Zero out a whole @c aq
 **/
static
void aq_zero(aq* q, unsigned max) {
  memset(q, 0, offsetof(aq, d));
  P99_MEMZERO(orwl_alloc_ref, q->d, max);
}

static
aq* aq_init(aq* q, unsigned max) {
  if (q) {
    aq_zero(q, max);
    q->max = max;
    q->users = aq_users;
    p99_cm_init(&q->cm);
  }
  return q;
}

static
aq* aq_new(unsigned max) {
  return aq_init(malloc(aq_size(max)), max);
}

static
void aq_destroy(aq* q) {
  for (size_t i = 0; i < q->max; ++i) {
    orwl_alloc_ref_destroy(&q->d[i]);
  }
  p99_cm_destroy(&q->cm);
  aq_zero(q, q->max);
}

static
void aq_delete(aq* q) {
  aq_destroy(q);
  free(q);
}

/**
 ** @brief The number of elements that are available in the queue.
 **
 ** This is just the difference of the positions. Since we always
 ** block insertion if this would go beyond @c max or if no slot is
 ** available, unsigned arithmetic does the trick here, even if the
 ** writer position has wrapped, and the reader position hasn't.
 **/
static
unsigned available(aq* q) {
  return q->w - q->r;
}

static
unsigned finished(aq* q) {
  return q->users < aq_users;
}

static
bool full(aq* q) {
  return available(q) >= q->max;
}

static
bool empty(aq* q) {
  return !available(q);
}

/**
 ** @brief Append an element to the queue.
 **
 ** This will block execution of the calling thread as long as there
 ** is no slot to place the element available.
 **/
static
bool aq_append(aq* q, orwl_alloc_ref* aref) {
  bool cont = true;
  P99_CM_EXCLUDE(&q->cm) {
    while ((cont = !finished(q))) {
      // Ensure that there is a slot to place our pointer
      if (full(q)) {
        p99_cm_wait(&q->cm);
      } else {
        orwl_alloc_ref_assign(&q->d[q->w % q->max], aref);
        ++q->w;
        break;
      }
    }
  }
  return cont;
}

/**
 ** @brief Obtain the first element of the queue.
 **
 ** This will block execution of the calling thread as long as there
 ** is no element available.
 **/
static
bool aq_pop(aq* q, orwl_alloc_ref* aref) {
  bool cont = true;
  P99_CM_EXCLUDE(&q->cm) {
    while ((cont = !finished(q))) {
      // Ensure that there is a pointer available
      if (empty(q)) {
        p99_cm_wait(&q->cm);
      } else {
        orwl_alloc_ref_mv(aref, &q->d[q->r % q->max]);
        ++q->r;
        break;
      }
    }
  }
  return cont;
}

/**
 ** @brief Announce that the calling thread has finished using this
 ** queue.
 **/
static
unsigned aq_finish(aq* q) {
  unsigned ret = 0;
  P99_CM_EXCLUDE(&q->cm) {
    --q->users;
    ret = q->users;
  }
  if (!ret) aq_delete(q);
  return ret;
}

P99_DECLARE_STRUCT(aq_thrd);

P99_DEFINE_STRUCT(aq_thrd,
                  /* This control protects the data structure as a
                     whole. */
                  orwl_thread_cntrl cntrl,
                  size_t pos,
                  bool reader,
                  aq* q);

static
aq_thrd* aq_thrd_init(aq_thrd* thrd, size_t pos, bool reader, aq* q) {
  if (thrd) {
    *thrd = (aq_thrd) {
      .pos = pos,
       .reader = reader,
        .q = q,
    };
    orwl_thread_cntrl_init(&thrd->cntrl);
  }
  return thrd;
}

static
void aq_thrd_delete(aq_thrd* thrd) {
  free(thrd);
}

static
void aq_thrd_reader(aq_thrd* Arg) {
  ORWL_THREAD_USE(aq_thrd, pos, q);
  aq_thrd* obj = Arg;

  orwl_server* server = orwl_server_get();
  p99_seed* seed = p99_seed_get();
  orwl_handle2 hdl = ORWL_HANDLE2_INITIALIZER;

  // insert at the last position
  orwl_handle2_read_insert(&hdl, pos, -1);
  // tell our caller that we have done all insertions
  orwl_thread_cntrl_freeze(&obj->cntrl);
  // wait until all insertions are scheduled
  p99_notifier_block(&server->up_sched);

  orwl_alloc_ref tmp = ORWL_ALLOC_REF_INITIALIZER(0);
  do {
    // copy the current data into a newly created orwl_alloc
    ORWL_SECTION(&hdl, 1, seed) {
      size_t s;
      void const* source = orwl_read_map(&hdl, &s);
      orwl_alloc_ref_replace(&tmp, P99_NEW(orwl_alloc, 0, 0, s));
      orwl_alloc* alloc = orwl_alloc_ref_get(&tmp);
      void* target = orwl_alloc_realloc(alloc, ORWL_ALLOC_MAP);
      memcpy(target, source, s);
    }
    // store this new orwl_alloc in the queue
  } while (aq_append(q, &tmp));
  orwl_alloc_ref_destroy(&tmp);
  aq_finish(q);
  orwl_handle2_disconnect(&hdl);
}

static
void aq_thrd_writer(aq_thrd* Arg) {
  ORWL_THREAD_USE(aq_thrd, pos, q);
  aq_thrd* obj = Arg;

  orwl_server* server = orwl_server_get();
  p99_seed* seed = p99_seed_get();
  orwl_handle2 hdl = ORWL_HANDLE2_INITIALIZER;

  orwl_handle2_write_insert(&hdl, pos, 0);
  // tell our caller that we have done all insertions
  orwl_thread_cntrl_freeze(&obj->cntrl);
  // wait until all insertions are scheduled
  p99_notifier_block(&server->up_sched);

  orwl_alloc_ref tmp = ORWL_ALLOC_REF_INITIALIZER(0);
  // obtain the top element that has been stored in the queue
  while (aq_pop(q, &tmp)) {
    ORWL_SECTION(&hdl, 1, seed) {
      orwl_handle2_replace(&hdl, &tmp, seed);
    }
  }
  orwl_alloc_ref_destroy(&tmp);

  aq_finish(q);
  orwl_handle2_disconnect(&hdl);
}

DECLARE_THREAD(aq_thrd, static);

DEFINE_THREAD(aq_thrd, static) {
  ORWL_THREAD_USE(aq_thrd, reader);
  aq_thrd* obj = Arg;
  if (reader)
    aq_thrd_reader(Arg);
  else
    aq_thrd_writer(Arg);
  orwl_thread_cntrl_wait_for_caller(&obj->cntrl);
}

/**
 ** @brief Ensure that a queue is released when the calling thread
 ** finishes execution.
 **/
static
void aq_thrd_at_end(void* o) {
  aq_thrd* thrd = o;
  aq_finish(thrd->q);
  orwl_thread_cntrl_detach(&thrd->cntrl);
}

static
void aq_queueN(size_t readloc, size_t writeloc, size_t max) {
  aq* q = aq_new(max);
  aq_thrd* reader = P99_NEW(aq_thrd, readloc, true, q);
  orwl_at_task_exit(aq_thrd_at_end, reader);
  P99_THROW_CALL_THRD(aq_thrd_create_detached, reader);
  orwl_thread_cntrl_wait_for_callee(&reader->cntrl);

  aq_thrd* writer = P99_NEW(aq_thrd, writeloc, false, q);
  orwl_at_task_exit(aq_thrd_at_end, writer);
  P99_THROW_CALL_THRD(aq_thrd_create_detached, writer);
  orwl_thread_cntrl_wait_for_callee(&writer->cntrl);
}

P99_DECLARE_STRUCT(aq_thrd0);

P99_DEFINE_STRUCT(aq_thrd0,
                  /* This control protects the data structure as a
                     whole. */
                  orwl_thread_cntrl cntrl,
                  bool volatile finished,
                  size_t readloc,
                  size_t writeloc);

static
aq_thrd0* aq_thrd0_init(aq_thrd0* thrd, size_t readloc, size_t writeloc) {
  if (thrd) {
    *thrd = (aq_thrd0) {
      .readloc = readloc,
       .writeloc = writeloc,
        .finished = false,
    };
    orwl_thread_cntrl_init(&thrd->cntrl);
  }
  return thrd;
}

static
void aq_thrd0_delete(aq_thrd0* thrd) {
  free(thrd);
}

DECLARE_THREAD(aq_thrd0, static);

DEFINE_THREAD(aq_thrd0, static) {
  ORWL_THREAD_USE(aq_thrd0, readloc, writeloc);
  aq_thrd0* obj = Arg;

  orwl_server* server = orwl_server_get();
  p99_seed* seed = p99_seed_get();
  orwl_handle2 hdl[2] = { ORWL_HANDLE2_INITIALIZER, ORWL_HANDLE2_INITIALIZER, };

  // insert in the last position
  orwl_handle2_read_insert(&hdl[0], readloc, -1);
  // insert in the first position
  orwl_handle2_write_insert(&hdl[1], writeloc, 0);
  // tell our caller that we have done all insertions
  orwl_thread_cntrl_freeze(&obj->cntrl);
  // wait until all insertions are scheduled
  p99_notifier_block(&server->up_sched);

  size_t prevSize = 0;
  while (!obj->finished) {
    ORWL_SECTION(hdl, 2, seed) {
      size_t s;
      void const* source = orwl_read_map(&hdl[0], &s);
      if (prevSize != s) orwl_handle2_truncate(&hdl[1], s);
      void* target = orwl_write_map(&hdl[1], &prevSize);
      P99_THROW_ASSERT(EINVAL, s == prevSize);
      memcpy(target, source, s);
    }
  }

  orwl_handle2_disconnect(hdl, 2);
}


/**
 ** @brief Ensure that a queue is released when the calling thread
 ** finishes execution.
 **/
static
void aq_thrd0_at_end(void* o) {
  aq_thrd0* thrd = o;
  thrd->finished = true;
  orwl_thread_cntrl_detach(&thrd->cntrl);
}

static
void aq_queue0(size_t readloc, size_t writeloc) {
  aq_thrd0* thrd = P99_NEW(aq_thrd0, readloc, writeloc);
  orwl_at_task_exit(aq_thrd0_at_end, thrd);
  P99_THROW_CALL_THRD(aq_thrd0_create_detached, thrd);
  orwl_thread_cntrl_wait_for_callee(&thrd->cntrl);
}

void orwl_alloc_queue(size_t readloc, size_t writeloc, size_t max) {
  if (max) aq_queueN(readloc, writeloc, max);
  else aq_queue0(readloc, writeloc);
}
