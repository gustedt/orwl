/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the authors and copyright holders for this work are as follows:            */
/* all rights reserved,  2011 Emmanuel Jeanvoine, INRIA, France               */
/* all rights reserved,  2010-2014, 2016 Jens Gustedt, INRIA, France          */
/* all rights reserved,  2013 Rodrigo Campos, INRIA, France                   */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#include "orwl_comstate.h"
#include "orwl_helpers.h"
#include "orwl_deep_instr.h"

ORWL_DEFINE_ENV(ORWL_STDERR);

static
size_t
orwl_report0(size_t myloc, size_t nl, size_t sl, size_t phase, char const* format,
             int lennl, size_t headlen, char head[headlen]) {
  static char const form0[] = "%s:%zX: %s\n";
  static char const form1[] = "%0*zX/%zX/%0*zX:%08zX: %s\n";
  return
    ((myloc == SIZE_MAX)
     ? snprintf(head, headlen, form0,
                THRD2STR(thrd_current()),
                phase, format)
     : snprintf(head, headlen, form1,
                lennl, myloc,
                nl,
                lennl, sl,
                phase, format));
}

void orwl_report(size_t myloc, size_t nl, size_t sl, size_t phase, char const* format, ...) {
  orwl_deep_instr_func_start();
  P99_INIT_CHAIN(orwl_thread);
  int const lennl = snprintf(0, 0, "%zX", nl);
  size_t headlen = 1 + orwl_report0(myloc, nl, sl, phase, format, lennl, 0, 0);
  char head[headlen];
  memset(head, 0, headlen);
  orwl_report0(myloc, nl, sl, phase, format, lennl, headlen, head);
  va_list ap;
  va_start(ap, format);
  vfprintf(stderr, head, ap);
  va_end(ap);
  orwl_deep_instr_func_stop();
}

static
size_t
orwl_trace0(size_t myloc, size_t nl, size_t sl, size_t phase,
            char const func[], size_t line, char const* format,
            int lennl, double epoch, size_t epoch_count,
            size_t headlen, char head[headlen]) {
  static char const form0[] = "%04zX:%012" PRIX64 ":%s:%zX::%s:%zu: %s\n";
  static char const form1[] = "%04zX:%012" PRIX64 ":%0*zX/%zX/%0*zX:%08zX::%s:%zu: %s\n";
  return
    ((myloc == SIZE_MAX)
     ? snprintf(head, headlen, form0,
                epoch_count,
                seconds2useconds(epoch),
                THRD2STR(thrd_current()),
                phase, func, line,
                format)
     : snprintf(head, headlen, form1,
                epoch_count,
                seconds2useconds(epoch),
                lennl, myloc,
                nl,
                lennl, sl,
                phase, func, line,
                format));
}

void orwl_trace(size_t myloc, size_t nl, size_t sl, size_t phase,
                char const func[], size_t line, char const* format, ...) {
  orwl_deep_instr_func_start();
  P99_INIT_CHAIN(orwl_thread);
  int const lennl = snprintf(0, 0, "%zX", nl);
  double const epoch = seconds() - atomic_load_explicit(&orwl_epoch, memory_order_consume);
  size_t const epoch_count = atomic_load_explicit(&orwl_epoch_count, memory_order_consume);
  size_t headlen = 1 + orwl_trace0(myloc, nl, sl, phase, func, line, format,
                                   lennl, epoch, epoch_count, 0, 0);
  char head[headlen];
  memset(head, 0, headlen);
  orwl_trace0(myloc, nl, sl, phase, func, line, format,
              lennl, epoch, epoch_count, headlen, head);
  va_list ap;
  va_start(ap, format);
  vfprintf(stderr, head, ap);
  va_end(ap);
  orwl_deep_instr_func_stop();
}

bool orwl_terminal = true;

static
size_t orwl_progress0(size_t t, size_t myloc, size_t nl, size_t sl, size_t phase, char const* format,
                      int lennl, size_t headlen, char head[headlen]) {
  static char const form0[] = "%0*jX:%08zX: (%c) %s%c";
  static char const form1[] = "%0*zX/%zX/%0*zX:%08zX: (%c) %s%c";
  static char const img[] = "|\\-/";
  return
    ((myloc == SIZE_MAX)
     ? snprintf(head, headlen, form0,
                (int)(sizeof(uintmax_t)/4),
  (union { uintmax_t ret; thrd_t cur; }) { .cur = thrd_current()} .ret,
  phase, img[t%4], format, (orwl_terminal ? '\r' : '\n'))
  : snprintf(head, headlen, form1,
             lennl, myloc,
             nl,
             lennl, sl,
             phase, img[t%4], format, (orwl_terminal ? '\r' : '\n')));
}

void orwl_progress(size_t t, size_t myloc, size_t nl, size_t sl, size_t phase, char const* format, ...) {
  int const lennl = snprintf(0, 0, "%zX", nl);
  size_t headlen = 1 + orwl_progress0(t, myloc, nl, sl, phase, format, lennl, 0, 0);
  char head[headlen];
  memset(head, 0, headlen);
  orwl_progress0(t, myloc, nl, sl, phase, format, lennl, headlen, head);
  va_list ap;
  va_start(ap, format);
  vfprintf(stderr, head, ap);
  va_end(ap);
}

static p99_count counter_act = P99_COUNT_INITIALIZER(0);
static _Atomic(size_t) counter_tot = ATOMIC_VAR_INIT(0);
static _Atomic(size_t) counter_max = ATOMIC_VAR_INIT(0);

P99_DEFINE_ONCE_CHAIN(mtx_t) {
}

P99_DEFINE_ONCE_CHAIN(cnd_t) {
  /* empty */
}

P99_DEFINE_ONCE_CHAIN(orwl_thread,
                      orwl_gettime,
                      orwl_rand,
                      mtx_t,
                      cnd_t) {
  orwl_verbose = ORWL_VERBOSE();
  char const*const stderr_name = ORWL_STDERR();
  if (stderr_name && stderr_name[0]) {
    trace(orwl_verbose, "re-opening stderr to %s", stderr_name);
    fclose(stderr);
    P99_THROW_CALL_VOIDP(freopen, EINVAL, stderr_name, "w+", stderr);
    trace(orwl_verbose, "re-opened stderr to %s", stderr_name);
    orwl_terminal = false;
  }
}

P99_INSTANTIATE(orwl_thread_cntrl*, orwl_thread_cntrl_init, orwl_thread_cntrl*);
P99_INSTANTIATE(void, orwl_thread_cntrl_destroy, orwl_thread_cntrl*);
P99_INSTANTIATE(void, orwl_thread_cntrl_freeze, orwl_thread_cntrl*);
P99_INSTANTIATE(void, orwl_thread_cntrl_detach, orwl_thread_cntrl*);
P99_INSTANTIATE(void, orwl_thread_cntrl_wait_for_caller, orwl_thread_cntrl*);
P99_INSTANTIATE(void, orwl_thread_cntrl_wait_for_callee, orwl_thread_cntrl*);

P99_DEFINE_DELETE(orwl_thread_cntrl);

int orwl_thrd_create_joinable(thrd_t *restrict thread,
                              thrd_start_t start_routine,
                              void *restrict arg) {
  return thrd_create(thread, start_routine, arg);
}

/* The detached cases are a bit more involved. We wrap another function
 * around the user thread function since we have to do some
 * preparation and clean up to do.
 *
 * Therefore we need a @c struct that combines the function pointer
 * and its argument.
 *
 * This mechanism now implements a thread pool of inactive threads
 * that recycle themselves.
 */

P99_DECLARE_STRUCT(o_rwl_launch_arg);
P99_POINTER_TYPE(o_rwl_launch_arg);
P99_LIFO_DECLARE(o_rwl_launch_arg_ptr);

struct  o_rwl_launch_arg {
  p99_notifier up;
  o_rwl_launch_arg* p99_lifo;
  thrd_t id;
  orwl_thread_cntrl *det;
  bool own;
  thrd_start_t start_routine;
  void *arg;
  size_t location;
};

static P99_LIFO(o_rwl_launch_arg_ptr) thrd_pool = P99_LIFO_INITIALIZER(0);


static inline
o_rwl_launch_arg* o_rwl_launch_arg_init(o_rwl_launch_arg *rt,
                                        thrd_start_t start_routine,
                                        void* arg,
                                        orwl_thread_cntrl* det,
                                        size_t location) {
  if (!rt) return 0;
  *rt = (o_rwl_launch_arg) {
    .det = det ? det : P99_NEW(orwl_thread_cntrl),
     .own = !det,
      .start_routine = start_routine,
       .arg = arg,
        .location = location,
  };
  /* leave the calls for the platforms that emulate futex */
  p99_notifier_init(&rt->up);
  /* initially a thread should be able to start without fuzz */
  p99_notifier_set(&rt->up);
  return rt;
}

static inline
noreturn
int o_rwl_launch_arg_exit(void* arg);

/* This is used to launch an existing thread in the tread pool on a
   new function. */
static inline
void o_rwl_launch_arg_launch(o_rwl_launch_arg *rt,
                             thrd_start_t start_routine,
                             void* arg,
                             orwl_thread_cntrl* det,
                             size_t location) {
  if (start_routine == o_rwl_launch_arg_exit) {
    rt->det = 0;
    rt->own = false;
  } else {
    rt->det = det ? det : P99_NEW(orwl_thread_cntrl);
    rt->own = !det;
  }
  rt->start_routine = start_routine;
  rt->arg = arg;
  rt->location = location;
  p99_notifier_set(&rt->up);
}

static inline
P99_PROTOTYPE(o_rwl_launch_arg*, o_rwl_launch_arg_init, o_rwl_launch_arg *, thrd_start_t, void*, orwl_thread_cntrl*, size_t);
#define o_rwl_launch_arg_init(...) P99_CALL_DEFARG(o_rwl_launch_arg_init, 5, __VA_ARGS__)

#define o_rwl_launch_arg_init_defarg_1() P99_0(thrd_start_t)
#define o_rwl_launch_arg_init_defarg_2() P99_0(void*)
#define o_rwl_launch_arg_init_defarg_3() P99_0(orwl_thread_cntrl*)
#define o_rwl_launch_arg_init_defarg_4() SIZE_MAX

static inline
void o_rwl_launch_arg_destroy(o_rwl_launch_arg *rt) {
  /* wait if the creator might still be needing the semaphore */
  if (rt->own) {
    orwl_thread_cntrl_wait_for_caller(rt->det);
    orwl_thread_cntrl_delete(rt->det);
  }
  *rt = (o_rwl_launch_arg) {
    .up = P99_NOTIFIER_INITIALIZER,
     .id = rt->id,
  };
  /* leave the call for the platforms that emulate futex */
  p99_notifier_init(&rt->up);
}

P99_DECLARE_DELETE(o_rwl_launch_arg, static);

static inline
noreturn
int o_rwl_launch_arg_exit(void* arg) {
  /* Basically, this function will never be called, report it if it
     happens occasionally. */
  report(1, "exiting pool thread %s via function", THRD2STR(thrd_current()));
  p99_count_dec(&counter_act);
  fflush(0);
  thrd_exit(0);
}

static inline
int o_rwl_launch_arg_wrapper(void *routine_arg) {
  int ret = EXIT_SUCCESS;
  o_rwl_launch_arg* Routine_Arg = routine_arg;
  Routine_Arg->id = thrd_current();
  for (bool sane = true; sane;) {
    orwl_deep_instr_thread_start();

    // Spinning for a while before blocking helps A LOT if work comes just after
    // we call to p99_notifier_block() that ends in a syscall. And 1000
    // iterations seems a good enough number to spin as reduces some cases (like
    // tutorials/half-active in a 24 cores machine) the overall wall-clock time
    // to run in 46%, a little improvement on tutorials/warmup and no effect at
    // all on the matrix multiplication
    // XXX: This "abuses" that P99 notifier is built over futex, we might create
    // a new structure for this use case if we realize p99_notifier doesn't
    // really fit.
    bool avoid_block = false;
    for (int i = 0; i < 1000; i++) {
      unsigned ftx_val = p99_futex_load(&Routine_Arg->up);
      if (ftx_val == 1) {
        avoid_block = true;
        break;
      }
    }

    if (!avoid_block)
      p99_notifier_block(&Routine_Arg->up);

    orwl_thread_cntrl *det = Routine_Arg->det;
    bool own = Routine_Arg->own;
    thrd_start_t start_routine = Routine_Arg->start_routine;
    void *restrict arg = Routine_Arg->arg;
    size_t location = Routine_Arg->location;
    orwl_deep_instr_thread_stop();
    if (start_routine == o_rwl_launch_arg_exit) break;
    P99_TRY {
      /* This should be fast since usually there should never be a waiter
         blocked on this counter. */
      P99_ACCOUNT(counter_act) {
        orwl_deep_instr_thread_start();
        /* If we received a real location ID, register the thread for that ID. */
        if (location != SIZE_MAX) orwl_myloc = location;
        /* The application routine must call orwl_thread_cntrl_freeze to unblock the
           caller. */
        if (own) orwl_thread_cntrl_freeze(det);
        orwl_deep_instr_thread_stop();
        orwl_deep_instr_thread_start();
        start_routine(arg);
        orwl_deep_instr_thread_stop();
        orwl_deep_instr_thread_start();
        atomic_fetch_add_explicit(&counter_tot, 1, memory_order_acq_rel);
        atomic_fetch_max_explicit(&counter_max, p99_count_value(&counter_act), memory_order_acq_rel);
        orwl_deep_instr_thread_stop();
      }
    } P99_CATCH(int err) {
      if (err) {
        orwl_server * srv = orwl_server_get();
        orwl_address_book *ab = srv->ab;
        p99_seed* seed = p99_seed_get();
        orwl_endpoint ep = srv->ep;
        orwl_endpoint* eps = 0;
        size_t nl = 0;
        if (ab) {
          /* Be careful to have all information on the stack, other
             threads are perhaps shutting us down. */
          ep = srv->ep;
          eps = ab->eps;
          nl = ab->nl;
          if (eps && nl) {
            eps = memcpy(orwl_endpoint_vnew(nl), eps, nl);
          }
        }
        fflush(stderr);
        sleepfor(p99_drand(seed)*1E-2);
        p99_jmp_report(err);
        fflush(stderr);
        report(1, "thread caught exception, shutting down: %s", strerror(err));
        orwl_server_close(srv);
        sleepfor(1E-2);
        orwl_server_close(srv);
        trace(orwl_verbose, "server shut down");
        size_t off = p99_rand(seed);
        if (ab) {
          if (eps && nl) {
            for (size_t i = 0; i < nl; ++i) {
              size_t pos = (i + off) % nl;
              if (!orwl_endpoint_similar(&ep, &eps[pos])) {
                P99_TRY {
                  TRACE(orwl_verbose, "sending terminate (%s/%s) to %s",
                  i, nl,
                  orwl_endpoint_print(&eps[pos]));
                  orwl_rpc(0, &eps[pos], seed, orwl_proc_terminate);
                } P99_CATCH();
              }
            }
          }
        }
        orwl_endpoint_vdelete(eps);
        trace(orwl_verbose, "terminated other servers, going down");
        orwl_server_terminate(srv);
        fflush(0);
        sane = false;
      }
    }
    orwl_deep_instr_thread_start();
    o_rwl_launch_arg_destroy(Routine_Arg);
    if (sane) P99_LIFO_PUSH(&thrd_pool, Routine_Arg);
    if (location != SIZE_MAX) {
      /* The following ensures that the task_exit callbacks are
         called, now. Otherwise, they would only be called when the
         thread of the task exits. */
      if (p99_tss_get(&o_rwl_at_task_exit))
        P99_THROW_CALL_THRD(p99_tss_set, &o_rwl_at_task_exit, 0);
    }
    orwl_deep_instr_thread_stop();
  }
  orwl_deep_instr_update();
  report(orwl_verbose, "exiting pool thread (%s)",
         THRD2STR(Routine_Arg->id));
  o_rwl_launch_arg_delete(Routine_Arg);
  fflush(0);
  return ret;
}


int orwl_thrd_launch(thrd_start_t start_routine,
                     void *restrict arg,
                     orwl_thread_cntrl* det,
                     size_t location) {
  int ret = 0;
  ORWL_TIMER() {
    /* Be sure to allocate the pair on the heap to leave full control
       to o_rwl_launch_arg_wrapper() of what to do with it. */
    o_rwl_launch_arg *Routine_Arg = P99_LIFO_POP(&thrd_pool);
    if (!Routine_Arg) {
      Routine_Arg = P99_NEW(o_rwl_launch_arg, start_routine, arg, det, location);
      if (!Routine_Arg) ret = thrd_nomem;
      else {
        thrd_t id = P99_INIT;
        ret = thrd_create(&id,
                          o_rwl_launch_arg_wrapper,
                          Routine_Arg);
        if (ret) free(Routine_Arg);
        else ret = thrd_detach(id);
      }
    } else {
      o_rwl_launch_arg_launch(Routine_Arg, start_routine, arg, det, location);
    }
    if (Routine_Arg) {
      if (!det) ORWL_TIMER(wait) {
        det = Routine_Arg->det;
        /* Wait until the routine is accounted for */
        orwl_thread_cntrl_wait_for_callee(det);
        /* Notify that Routine_Arg may safely be deleted thereafter */
        orwl_thread_cntrl_detach(det);
      }
    }
  }
  return ret;
}

P99_INSTANTIATE(int, orwl_thrd_create_detached, thrd_start_t, void *restrict);

void orwl_thrd_wait_detached(void) {
  p99_count_wait(&counter_act);
}

void orwl_thrd_pool_purge(void) {
  for (o_rwl_launch_arg *thrd = P99_LIFO_POP(&thrd_pool);
       thrd;
       thrd = P99_LIFO_POP(&thrd_pool))
    o_rwl_launch_arg_launch(thrd, o_rwl_launch_arg_exit, 0, 0, 0);
}

DEFINE_NEW_DELETE(thrd_t);

void orwl_thrd_count(size_t ret[3]) {
  ret[0] = p99_count_value(&counter_act);
  ret[1] = atomic_load(&counter_tot);
  ret[2] = atomic_load(&counter_max);
}
