/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the author and copyright holder for this work is                           */
/* all rights reserved,  2018 Jens Gustedt, INRIA, France                     */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */

#include <orwl_Poisson.h>
#include <orwl_normal.h>

size_t orwl_Poisson(p99_seed seed[static 1], double lambda) {
  size_t ret = 0;
  if (lambda <= 30.0) {
    double U = p99_drand(seed);
    double P = exp(-lambda);
    double S = P;
    while (U > S) {
      ++ret;
      P *= lambda;
      P /= ret;
      S += P;
    }
  } else {
    // continuity correction
    ret = (size_t) round(orwl_normal(seed, lambda, sqrt(lambda)));
  }
  return ret;
}
