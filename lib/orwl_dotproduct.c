/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the author and copyright holder for this work is                           */
/* all rights reserved,  2010-2011, 2013 Jens Gustedt, INRIA, France          */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#include "orwl_dotproduct.h"

O_RWL_DOTPRODUCT_INSTANTIATE(ld, 2);
O_RWL_DOTPRODUCT_INSTANTIATE(d, 4);
O_RWL_DOTPRODUCT_INSTANTIATE(f, 8);
O_RWL_DOTPRODUCT_INSTANTIATE(d, 2);
O_RWL_DOTPRODUCT_INSTANTIATE(f, 4);

double
orwl_dotproductd(size_t n, double const A[n], double const B[n]);

long double
orwl_dotproductld(size_t n, long double const A[n], long double const B[n]);

float
orwl_dotproductf(size_t n, float const A[n], float const B[n]);


#ifndef __STDC_NO_COMPLEX__

double complex
orwl_dotproductdc(size_t n, double complex const A[n], double complex const B[n]);

long double complex
orwl_dotproductldc(size_t n, long double complex const A[n], long double complex const B[n]);

float complex
orwl_dotproductfc(size_t n, float complex const A[n], float complex const B[n]);

#endif
