/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the authors and copyright holders for this work are as follows:            */
/* all rights reserved,  2015-2016 Farouk Mansouri, INRIA, France             */
/* all rights reserved,  2016 Jens Gustedt, INRIA, France                     */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#ifndef FIFO_H
#define FIFO_H

#include <opencv/highgui.h>
#include <opencv/cv.h>


typedef struct Element Element;
struct Element {
  IplImage *frame;
  Element *suivant;
};

typedef struct File File;
struct File {
  Element *premier;
};

File *initialiser(void);
void enfiler(File *file, IplImage *new_frame);
IplImage * defiler(File *file);
int file_size(File *file);

#endif

