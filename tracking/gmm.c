/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the authors and copyright holders for this work are as follows:            */
/* all rights reserved,  2015-2016 Farouk Mansouri, INRIA, France             */
/* all rights reserved,  2016 Jens Gustedt, INRIA, France                     */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#include "tracing.h"
#include "gmm.h"
#include "wrapper.h"

//#define DECOMPOSE
//#define DEBUG


void gmm(orwl_handle2 *there, orwl_handle2 *here, orwl_handle2 *there_sec , orwl_handle2 *here_sec, orwl_handle2 there_sec_gmm[], int loop,  int loop_size ,int mytid, int size_h , int size_w, int split_nbre ) {
  ORWL_TIMER(GMM)
  ORWL_POTI_SECTION(loop, loop_size, "GMM_Task_N°%d", 2) {

    IplImage *fore_merge = cvCreateImage(cvSize(size_w, size_h), IPL_DEPTH_8U, 1);
    IplImage *frame_split = 0;

    if (loop ==0) {
      gmm_init();
    }

    ORWL_SECTION(there) {   /// reading from producer ///
      double const** rval = orwl_read_map(there);
      frame_split = (IplImage *) (*rval);
    }

    ORWL_POTI_EVENT("1");

    ////  spliting location ////

#ifdef DEBUG
    printf(" \n I'm a task GMM N° %d | %p | %p \n", loop, frame_split , frame_split->imageData );
#endif

    ORWL_SECTION(here_sec) {   /// reading offset of splitted and merged images ///
      double ** w2val = orwl_write_map(here_sec);
      w2val[0] =  (double*) frame_split;
      w2val[1] =  (double*) fore_merge;
      // printf(" \n I'm a task GMM N° %d frame | img %d | fore %d \n", loop, (*w2val), fore );
    }

    int temp_nbre = split_nbre;
    int vect[split_nbre];
    for (int i=0; i<split_nbre; i++) {
      vect[i]=i;
    }

    /// reading sub-foregrounds ///
    while( temp_nbre > 0) {
      for (int i=0; i<split_nbre; i++) {
        if (vect[i] != 999) {
          if( orwl_test(&(there_sec_gmm[vect[i]])) == orwl_acquired ) { /// sub task terminated
            ORWL_SECTION(&(there_sec_gmm[vect[i]])) {
            }
            vect[i] = 999;
            temp_nbre--;
            break;
          }
        }
      }
    }


    /* for (int i=0; i<split_nbre; i++)
     {
     ORWL_SECTION(&(there_sec_gmm[i])) {
          double const** rval = orwl_read_map(&(there_sec_gmm[i]));
          frame_split2[i] = (IplImage *) (*rval);
         }
     } */

    ORWL_POTI_EVENT("2");

    /// writing foreground ///
    ORWL_SECTION(here) {
      double ** w2val = orwl_write_map(here);
      (*w2val) =  (double*) fore_merge;
      // printf(" \n I'm a task GMM N° %d frame | img %d | fore %d \n", loop, (*w2val), fore );
    }
  }
}



void img_split ( IplImage * input, IplImage * img[], int num) {
  int size_h  = input->height; int size_w = input->width ;
  CvRect rec[num][num];

  int k=0;
  for (int i = 0; i< size_h; i=i+(size_h/num)) {
    int s=0;
    for (int j = 0; j< size_w; j=j+(size_w/num)) {
      rec[s][k] = cvRect(j, i, size_w/num, size_h/num);
      // printf("rec [%d][%d] cordinates are : %d  %d  %d  %d \n", s,k, i, j, size_h/num, size_w/num);
      s++;
    }
    k++;
  }

  for (int i = 0; i< num; i++) {
    for (int j = 0; j< num; j++) {
      cvSetImageROI(input, rec[i][j]);
      img[(i*num)+j] = cvCreateImage(cvSize(size_w/num, size_h/num),input->depth,input->nChannels);
      cvCopy(input,  img[(i*num)+j], NULL);
      cvResetImageROI(input);
    }
  }

}

void img_merge (  IplImage * input[], IplImage ** output, int num) {
  int size_h  = (input[0]->height)*num; int size_w = (input[0]->width)*num ;
  CvRect rec[num][num];

  int k=0;
  for (int i = 0; i< size_h; i=i+(size_h/num)) {
    int s=0;
    for (int j = 0; j< size_w; j=j+(size_w/num)) {
      rec[s][k] = cvRect(j, i, size_w/num, size_h/num);
      //printf("rec [%d][%d] cordinates are : %d  %d  %d  %d \n", s,k, i, j, size_h/num, size_w/num);
      s++;
    }
    k++;
  }

  IplImage * out = *output ;

  for (int i = 0; i< num; i++) {
    for (int j = 0; j< num; j++) {
      cvSetImageROI(out, rec[i][j]);
      cvCopy(input[(i*num)+j], out , NULL);
      cvResetImageROI(out);
      cvReleaseImage(&input[(i*num)+j]);
    }
  }

}

void gmm_sub(orwl_handle2 *there, orwl_handle2 *here, int loop,  int loop_size ,int mytid, int size_h , int size_w, int split_nbre ) {
  int num_split = mytid-10;
  ORWL_TIMER(GMM_SUB)
  ORWL_POTI_SECTION(loop, loop_size, "GMM_Split_N°%d", num_split) {

    IplImage * frame = 0;
    IplImage * fore = 0;

    int x= 0;
    int y = (num_split % split_nbre) * (size_h/split_nbre);

    CvRect rec = cvRect(x, y, size_w, size_h/split_nbre);
    IplImage * sub_frame = cvCreateImage(cvSize(size_w, size_h/split_nbre),8,  3);
    IplImage * sub_fore = cvCreateImage(cvSize(size_w, size_h/split_nbre),8, 1);

    IplImage * sub_frame_temp;  sub_frame_temp = cvCreateImage(cvSize(size_w, size_h/split_nbre),8, 3);
    IplImage * sub_fore_temp;   sub_fore_temp = cvCreateImage(cvSize(size_w, size_h/split_nbre),8, 1);

    ORWL_SECTION(there) {
      double const** rval = orwl_read_map(there);
      frame = (IplImage *) rval[0];
      fore = (IplImage *) rval[1];
    }

    ORWL_POTI_EVENT("1");

    IplImage *frame_copy = frame;

    //// Selecing sub data frome Frame ////

    CvMat img_temp, fore_temp;
    cvGetSubRect(frame_copy, &img_temp, rec);
    cvGetSubRect(fore, &fore_temp, rec);
    cvCopy(&img_temp,  sub_frame, NULL);

    //sub_frame = cvGetImage(&img_temp, sub_frame_temp);
    //sub_fore = cvGetImage(&fore_temp, sub_fore_temp);

    ORWL_POTI_EVENT("11");

#ifdef DEBUG
    printf(" \n I'm a task GMM Split %d N° %d | %d | %d | %d \n ", mytid ,loop, x, y, split_nbre);
#endif

//// GMM ////

    gmm_apply( sub_frame, sub_fore /*fore_decomp*/, num_split);

    /// comparison to full images ////
    /*for (int j = 0; j <size ; ++j)
    {
         if ( (fore->imageData[j] - fore_recomp->imageData[j]) != 0 )
          printf(" \n Foreground - value of pixels not equals %d :  %f  |  %f \n ", j , fore->imageData[j], fore_recomp->imageData[j]);
    }*/

    ORWL_POTI_EVENT("2");

    ORWL_SECTION(here) {  } /// waiting until all sub-tasks done ///
  }
}

