/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the authors and copyright holders for this work are as follows:            */
/* all rights reserved,  2016 Farouk Mansouri, INRIA, France                  */
/* all rights reserved,  2016 Jens Gustedt, INRIA, France                     */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#ifndef TRACING_H
#define TRACING_H 1
#include "orwl_tracking_pipeline.h"

typedef char orwl_poti_ident[30];

#ifdef POTI
void orwl_poti_init(void);
// Ensure that gcc and clang check the format for validity
# ifdef __GNUC__
__attribute__((format(printf, 3, 4)))
# endif
char const* orwl_poti_start(int loop, orwl_poti_ident s, char const format[], ...);
void orwl_poti_event (orwl_poti_ident const s, int loop, char const c[]);
void orwl_poti_stop(orwl_poti_ident const s, int loop, int loop_size);
#else
// Create empty inline functions that can be optimized away.
# ifdef __GNUC__
__attribute__((format(printf, 3, 4)))
# endif
inline char const* orwl_poti_start(int loop, orwl_poti_ident s, char const format[], ...) { return 0; }
inline void orwl_poti_event (orwl_poti_ident const s, int loop, char const c[]) { /* empty */ }
inline void orwl_poti_stop(orwl_poti_ident const s, int loop, int loop_size) { /* empty */ }
inline void orwl_poti_init(void) { /* empty */ }
#endif

#define ORWL_POTI_INIT() orwl_poti_init()

#define ORWL_POTI_START(LOOP, SIZE, ...)                                                               \
int orwl_poti_loop = (LOOP);                                                                           \
int orwl_poti_loop_size = (SIZE);                                                                      \
char const* orwl_poti_string = orwl_poti_start(orwl_poti_loop, (orwl_poti_ident){ 0 }, "" __VA_ARGS__)

#define ORWL_POTI_EVENT(N) orwl_poti_event(orwl_poti_string, orwl_poti_loop, "" N "")

#define ORWL_POTI_STOP() orwl_poti_stop(orwl_poti_string, orwl_poti_loop, orwl_poti_loop_size)

#define ORWL_POTI_SECTION(LOOP, SIZE, ...)                     \
P00_BLK_DECL(int, orwl_poti_loop, (LOOP))                      \
P00_BLK_DECL(int, orwl_poti_loop_size, (SIZE))                 \
P00_BLK_DECL(char const*const,                                 \
             orwl_poti_string,                                 \
             orwl_poti_start(orwl_poti_loop,                   \
                             (orwl_poti_ident){ 0 },           \
                             "" __VA_ARGS__))                  \
 P00_BLK_AFTER(ORWL_POTI_STOP())

#endif
