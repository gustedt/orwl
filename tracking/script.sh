#!/bin/bash
# Indique au système que l'argument qui suit est le programme utilisé pour exécuter ce fichier
# En règle générale, les "#" servent à mettre en commentaire le texte qui suit comme ici

echo Liste des fichiers :
ls -la

sh ../scripts/simple-threaded-run.sh test1 18 ./orwl_tracking_pipeline 100 videos/M6_Motorway_Traffic.mp4
 
exit 0
