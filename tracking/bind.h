/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the author and copyright holder for this work is                           */
/* all rights reserved,  2016 Farouk Mansouri, INRIA, France                  */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#ifndef BIND_H
#define BIND_H 1

#include <sched.h>
#include <hwloc.h>


int orwl_bind_policy_0(unsigned mytid, hwloc_topology_t topology);
int orwl_bind_policy_1(unsigned mytid, hwloc_topology_t topology);
int orwl_bind_policy_2(unsigned mytid, hwloc_topology_t topology);
int orwl_bind_policy_3(unsigned mytid,unsigned cup_size);
int orwl_bind_policy_4(unsigned mytid, hwloc_topology_t topology);
int orwl_bind_policy_5(unsigned mytid, hwloc_topology_t topology);
int orwl_bind_policy_6(unsigned mytid, hwloc_topology_t topology);
int orwl_bind_policy_hwloc(unsigned id, hwloc_topology_t topology) ;

#endif
