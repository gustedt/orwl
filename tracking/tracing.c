/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the authors and copyright holders for this work are as follows:            */
/* all rights reserved,  2016 Farouk Mansouri, INRIA, France                  */
/* all rights reserved,  2016 Jens Gustedt, INRIA, France                     */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#include "tracing.h"
#include <stdarg.h>

#ifdef POTI

void orwl_poti_init(void) {
  poti_header (1, 0);
  //Defining my types
  poti_DefineContainerType ("ROOT", "0", "ROOT");
  poti_DefineContainerType("THREAD", "ROOT", "THREAD");
  poti_DefineStateType("STATE", "THREAD", "STATE");
  //poti_DefineLinkType("LINK", "ROOT", "THREAD", "THREAD", "LINK");
  poti_DefineEventType ("EV", "ROOT", "EV");
  //poti_DefineVariableType ("VAR", "ROOT", "VAR", "1 1 0");

  //define values and color for the STATE type
  poti_DefineEntityValue("r", "STATE", "running", "0.0 1.0 0.0");
  poti_DefineEntityValue("i", "STATE", "idle", "1.0 0 0");

  //define values and color for the LINK type
  // poti_DefineEntityValue("c", "LINK", "communication", "0 0 1");
  double t1 = seconds();
  //Create my root container and containers for two threads
  poti_CreateContainer (t1, "root", "ROOT", "0", "root");
}

char const* orwl_poti_start(int loop, orwl_poti_ident s, char const format[], ...) {

  va_list va;
  va_start(va, format);
  vsnprintf(s, 30, format, va);
  va_end(va);

  if (loop ==0 ) {
    double t1 = seconds();
    flockfile(stdout);
    poti_CreateContainer (t1, s, "THREAD", "root", s);
    funlockfile(stdout);
  }

  double t1 = seconds();
  flockfile(stdout);
  poti_PushState (t1, s, "STATE", "r");
  funlockfile(stdout);
  return s;
}

void orwl_poti_stop(orwl_poti_ident const s, int loop, int loop_size) {
  double t1 = seconds();
  flockfile(stdout);
  poti_PopState  (t1, s, "STATE");
  funlockfile(stdout);
  if ( loop == loop_size-1) {
    double t1 = seconds();
    flockfile(stdout);
    poti_DestroyContainer (t1, "THREAD", s);
    funlockfile(stdout);
  }

}


void orwl_poti_event (orwl_poti_ident const s, int loop, char const* c) {
  double t1 = seconds();
  flockfile(stdout);
  poti_NewEvent(t1, s, "EV", c);
  funlockfile(stdout);
}

#else
// Instantiate the empty versions of the functions, such that the
// debugger can find them if necessary.
void orwl_poti_init(void);
char const* orwl_poti_start(int loop, orwl_poti_ident s, char const format[], ...);
void orwl_poti_event (orwl_poti_ident const s, int loop, char const c[]);
void orwl_poti_stop(orwl_poti_ident const s, int loop, int loop_size);
#endif
