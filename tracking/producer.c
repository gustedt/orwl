/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the authors and copyright holders for this work are as follows:            */
/* all rights reserved,  2015-2016 Farouk Mansouri, INRIA, France             */
/* all rights reserved,  2016 Jens Gustedt, INRIA, France                     */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#include "tracing.h"
#include "producer.h"
#include <stdlib.h>


CvCapture* cap;
CvCapture* cap1;

//#define DEBUG

void producer_init(CvSize* my_size, char * video_path) {
  cap = cvCaptureFromFile(video_path); // "videos/Surveillance_cameraHD.mp4");  // AVG-TownCentre
  int frame_height = cvGetCaptureProperty(cap, CV_CAP_PROP_FRAME_HEIGHT);
  int frame_width = cvGetCaptureProperty(cap, CV_CAP_PROP_FRAME_WIDTH);

#ifdef DEBUG
  printf(" \n Cap properties of %s : %d  | %d  ", video_path, frame_height, frame_width);
#endif

  (*my_size).width =  frame_width;
  (*my_size).height = frame_height;
}



void producer (orwl_handle2 *here, orwl_handle2 *here_sec , int loop , int loop_size,  int size_h , int size_w ) {
  ORWL_TIMER(PRODUCER)
  ORWL_POTI_SECTION(loop, loop_size, "PRODUCER_Task_N°%d", 1) {
    ORWL_POTI_EVENT("1");

// if (loop==0) cap1 = cvCaptureFromFile("videos/M6_Motorway_Traffic.mp4");  // AVG-TownCentre

    IplImage* img = cvQueryFrame(cap); /// Have to make a hard copy of img. cvQueryFrame returns static pointer

    if (img == NULL) {
      fprintf (stderr, "couldn't open image file: %s\n", "lena.jpg");
      exit(EXIT_FAILURE);
    }

    ORWL_POTI_EVENT("2");

    IplImage *img1 =  cvCreateImage(cvGetSize(img), img->depth, img->nChannels);   /// possible optimisation is to use statics buffers
    cvCopy(img , img1, NULL);                      /// Because of img intitialisation, its memory page is located in Master thread.
    IplImage *img2 = img1;

#ifdef DEBUG
    printf(" \n I'm a task producer N° %d | img %p | img2 %p | %p \n", orwl_mytid, img1, img2, *img);  /// img et img* ne bougent pas (probléme)
#endif

    ORWL_POTI_EVENT("3");

    /// writing image ///
    ORWL_SECTION(here_sec) {
      double ** wval2 = orwl_write_map(here_sec);
      (*wval2)  = (double*) img2;
    }

    /// writing image ///
    ORWL_SECTION(here) {
      double ** wval = orwl_write_map(here);
      (*wval) = (double*) img1;
    }
  }
}

