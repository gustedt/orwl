/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the authors and copyright holders for this work are as follows:            */
/* all rights reserved,  2015-2016 Farouk Mansouri, INRIA, France             */
/* all rights reserved,  2016 Jens Gustedt, INRIA, France                     */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#include "tracing.h"
#include "erode.h"

//#define DEBUG

void dilate(orwl_handle2 *there, orwl_handle2 *here, int loop, int loop_size, int mytid, int size_h , int size_w  ) {
  ORWL_TIMER(DILATE)
  ORWL_POTI_SECTION(loop, loop_size, "DILATE%d_Task_N°%d", mytid, 4) {

#ifdef DEBUG
    printf(" \n I'm a task Dilate N° %d \n", loop);
#endif

    IplImage *fore_in ;//= cvCreateImage(cvSize(size_w, size_h), IPL_DEPTH_8U, 1);
    IplImage *fore_out = cvCreateImage(cvSize(size_w, size_h), IPL_DEPTH_8U, 1);

    /// reading image ///
    ORWL_SECTION(there) {
      double const** rval = orwl_read_map(there);
      fore_in = (IplImage *) *rval ;
    }

    ORWL_POTI_EVENT("1");

    /// some processing ///
    cvDilate(fore_in, fore_out, 0, 1);
    cvReleaseImage(&fore_in);

    ORWL_POTI_EVENT("2");

    ORWL_SECTION(here) {
      double ** w2val = orwl_write_map(here);
      (*w2val) = (double*) fore_out;
    }
  }
}
