/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the authors and copyright holders for this work are as follows:            */
/* all rights reserved,  2015-2016 Farouk Mansouri, INRIA, France             */
/* all rights reserved,  2012-2013, 2016 Jens Gustedt, INRIA, France          */
/* all rights reserved,  2013 Rodrigo Campos, INRIA, France                   */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#include "producer.h"
#include "tracing.h"
#include "gmm.h"
#include "erode.h"
#include "dilate.h"
#include "blob.h"
#include "tracking.h"
#include "consumer.h"

//#define TIMING
#define DEBUG

#define TASK_SPLIT 4

#ifdef BLOB_SPLIT
#define TASK_SPLIT_BLOB 2
#endif

hwloc_topology_t topology;

ORWL_LOCATIONS_PER_TASK_INSTANTIATION();

task_obj* task_obj_init(task_obj* task, double my_loop, int  my_size_h, int  my_size_w) {
  if (task) {
    *task = (task_obj) {
      .loop = my_loop,
       .size_h = my_size_h,
        .size_w = my_size_w,
    };
  }
  return task;
}

void task_obj_destroy(task_obj *task) {
  /* empty */
}

P99_DECLARE_DELETE(task_obj);
P99_DEFINE_DELETE(task_obj);
ORWL_DECLARE_TASK(task_obj);
//! [declare task specific state]


int main(int argc, char **argv) {
  if (argc < 2) {
    REPORT(1, "only " ORWL_TERM_ALERT "%s commandline argument(s)" ORWL_TERM_OFF ", this ain't enough", argc);
    return EXIT_FAILURE;
  }

  hwloc_topology_init(&topology);  // initialization
  hwloc_topology_load(topology);   // actual detection

  double loop = strtod(argv[1]);
  char * video_path =  argv[2];
  //! [condition ORWL]
  orwl_init();
  //! [condition ORWL]

  CvSize my_size= cvSize(0, 0);
  producer_init(&my_size, video_path);
// printf(" \n TESTs: %d  | %d ", (my_size).height  , (my_size).width);

  ORWL_POTI_INIT();

  //! [launch one thread per task]
  for (size_t i = 0; i < orwl_lt; i++) {

    task_obj* task = P99_NEW(task_obj, loop, (my_size).height , (my_size).width);
    task_obj_create_task(task, orwl_tids[i]);
  }

  //hwloc_topology_destroy(topology);   Master thread don't wait for the tasks threads ... here the destroy occur before the threads use it

  /* #ifdef POTI
   poti_DestroyContainer ((float) clock()/CLOCKS_PER_SEC, "ROOT", "root");
   #endif*/
  //! [launch one thread per task]
  return EXIT_SUCCESS;
}

//! [start the task specific function definition]
ORWL_DEFINE_TASK(task_obj) {
  ORWL_THREAD_USE(task_obj, loop, size_h, size_w);
  //! [start the task specific function definition]
  ORWL_TIMER(DEF_TASKS) {
    int size = 16; //size_h * size_w; // size of location/ It contain only @ of pointers

#ifdef DEBUG
    printf(" \n Size_h : %d | Size_w : %d | Size:  | %d \n", size_h, size_w,  size );
    printf(" \n nombre de tache split %d \n", TASK_SPLIT );
#endif
    //! [condition this task]
    /* Scale our own location(s) to the appropriate buffer size. */
    orwl_scale(sizeof(double[size]));      /// posible to dynamically define the size ( only the first task define a location for the image )
    //! [condition this task]
    orwl_scale(sizeof(double[size]), ORWL_LOCATION(orwl_mytid, sec_loc));

    orwl_scale(sizeof(double[size]), ORWL_LOCATION(orwl_mytid, split_loc));
    /* Sleep a bit to give the user the occasion to admire the following
       message. */
    sleepfor(0.5 * p99_drand());
    /* Now ORWL knows the total amount of tasks that will be
       performed. Avoid being too verbose so let only task 0 tell it. */
    PROGRESS(1, orwl_myloc,
             "<-------- the start of the report line now should contain location ID (%s) and total number (%s)",
             orwl_myloc, orwl_nl);

    //! [connect the resources]
    /* Create handles for the locations that we are interested in. We
       will create a chain of dependencies from task 0 to task 1 etc. */

    orwl_handle2 here = ORWL_HANDLE2_INITIALIZER;
    orwl_handle2 there = ORWL_HANDLE2_INITIALIZER;
    orwl_handle2 here_sec = ORWL_HANDLE2_INITIALIZER;
    orwl_handle2 here_split = ORWL_HANDLE2_INITIALIZER;
    orwl_handle2 there_split = ORWL_HANDLE2_INITIALIZER;
    orwl_handle2 there_split_gmm[TASK_SPLIT] =   { P99_DUPL(TASK_SPLIT, ORWL_HANDLE2_INITIALIZER) };
#ifdef BLOB_SPLIT
    orwl_handle2 there_split_blob[TASK_SPLIT_BLOB] =   { P99_DUPL(TASK_SPLIT_BLOB, ORWL_HANDLE2_INITIALIZER) };
#endif

    /// dependencies structure ///
    if (!orwl_mytid)
      orwl_write_insert(&here, ORWL_LOCATION(orwl_mytid, main_loc), orwl_mytid);    /// Producer task writes on their own location (main_loc)

    if (orwl_mytid)
      orwl_write_insert(&here, ORWL_LOCATION(orwl_mytid, main_loc), orwl_mytid);     /// All others tasks write on their own location (main_loc)

    if (orwl_mytid>0 && orwl_mytid<=10) {
      orwl_read_insert(&there, ORWL_LOCATION(orwl_mytid - 1, main_loc), orwl_mytid);   /// All tasks except Producer read from the location of precedent one
      printf(" %zu | %zu \n", orwl_mytid - 1, orwl_mytid);
    }

    if (!orwl_mytid)
      orwl_write_insert(&here_sec, ORWL_LOCATION(0, sec_loc), 0);     /// Producer task writes the frame on its second location (sec_loc)

    else if (orwl_mytid == 8) {
      orwl_read_insert(&here_sec, ORWL_LOCATION(0, sec_loc), 8);    /// Tracking task reads from sec_loc location of the Producer and stores on FIFO
      printf(" %d | %zu \n", 0, orwl_mytid);
    }

    if ( (orwl_mytid == 1) || ( (orwl_mytid >= 10) && (orwl_mytid < 10+TASK_SPLIT) )) {
      orwl_write_insert(&here_split, ORWL_LOCATION(orwl_mytid, split_loc), 1);     /// Split tasks and GMM task write on their own spliting location (split_loc)
    }

    if ( (orwl_mytid == 1) ) {
      for (int i= 10 ; i < 10+TASK_SPLIT ; i++) {
        orwl_read_insert(&(there_split_gmm[i-10]), ORWL_LOCATION(i, split_loc), 10);      /// GMM task read from each split location (split_loc) of sub tasks
        printf(" %d | %zu \n", i, orwl_mytid);
      }
    }

    if ( (orwl_mytid >= 10) && (orwl_mytid < 10+TASK_SPLIT) ) {
      orwl_read_insert(&there_split, ORWL_LOCATION(1, split_loc), 10);     /// Each split task reads from split_loc of GMM
      printf(" %d | %zu \n", 1, orwl_mytid);
    }
    
    if ( (orwl_mytid == 7) ) {
      orwl_write_insert(&here_split, ORWL_LOCATION(orwl_mytid, split_loc), 1);     /// Blob Split sub tasks and blob task write on their own spliting location (split_loc)
    }

#ifdef BLOB_SPLIT
    if ( (orwl_mytid >= 10+TASK_SPLIT) ) {
      orwl_write_insert(&here_split, ORWL_LOCATION(orwl_mytid, split_loc), 1);     /// Blob Split sub tasks and blob task write on their own spliting location (split_loc)
    }

    if ( (orwl_mytid == 7) ) {
      for (int i= 10+TASK_SPLIT ; i < 10+TASK_SPLIT+TASK_SPLIT_BLOB ; i++) {
        orwl_read_insert(&(there_split_blob[i-(10+TASK_SPLIT)]), ORWL_LOCATION(i, split_loc), 10);      /// Blob task read from each split location (split_loc) of sub tasks
        printf(" %d | %zu \n", i, orwl_mytid);
      }
    }

    if ( (orwl_mytid >= 10+TASK_SPLIT) ) {
      orwl_read_insert(&there_split, ORWL_LOCATION(7, split_loc), 10);     /// Each split task reads from split_loc of BLOB
      printf(" %d | %zu \n", 7, orwl_mytid);
    }
#endif

    /* Now synchronize to have all requests inserted orderly at the
       other end. */
    orwl_schedule();

    //! [connect the resources]

    //! [perform the task]
    /* All tasks create a critical section that guarantees exclusive
       access to their location. */

    for (size_t i = 0; i <loop ; ++i) {

      switch (orwl_mytid) {     //// switch task ////

      case 0:
        producer(&here, &here_sec ,i, (int) loop, size_h, size_w);
        break;

      case 1:
        gmm(&there, &here, &there_split, &here_split , &there_split_gmm[0], i , (int) loop, orwl_mytid, size_h, size_w, TASK_SPLIT);
        break;

      case 2:
        erode(&there, &here, i , (int) loop, orwl_mytid, size_h, size_w);
        break;

      case 3:
        dilate(&there, &here, i , (int) loop, orwl_mytid, size_h, size_w);
        break;

      case 4:
        dilate(&there, &here, i, (int) loop,orwl_mytid, size_h, size_w);
        break;

      case 5:
        dilate(&there, &here, i, (int) loop, orwl_mytid, size_h, size_w);
        break;

      case 6:
        dilate(&there, &here, i, (int) loop, orwl_mytid, size_h, size_w);
        break;

#ifdef BLOB_SPLIT
      case 7:
        blob(&there, &here, &here_split,  &there_split_blob[0], i, (int) loop, orwl_mytid, size_h, size_w, TASK_SPLIT_BLOB );
        break;
#else 
      case 7:
        blob(&there, &here, &here_split,  NULL, i, (int) loop, orwl_mytid, size_h, size_w, 0 );
        break;
#endif

      case 8:
        track(&there, &here, &here_sec, i, (int) loop, orwl_mytid, size_h, size_w);
        break;

      case 9:
        consumer(&there, &here, i,(int) loop, orwl_mytid, size_h, size_w);
        break;

      ///// split tasks of GMM ////

      case 10 ... (9+TASK_SPLIT):
        gmm_sub(&there_split, &here_split, i,(int) loop, orwl_mytid, size_h, size_w, TASK_SPLIT );
        break;

      ///// split task of BLOB ////
#ifdef BLOB_SPLIT
      case (10+TASK_SPLIT) ... (10+TASK_SPLIT+TASK_SPLIT_BLOB):
        blob_sub(&there_split, &here_split, i,(int) loop, orwl_mytid, size_h, size_w, TASK_SPLIT_BLOB , TASK_SPLIT );
        break;
#endif
      }
    }

    //! [perform the task]

    for (int i= 0 ; i < TASK_SPLIT; i++) orwl_disconnect(&(there_split_gmm[i]));
#ifdef BLOB_SPLIT
    for (int i= 0 ; i < TASK_SPLIT_BLOB ; i++) orwl_disconnect(&(there_split_blob[i]));
#endif

    orwl_disconnect(&here_split);
    orwl_disconnect(&there_split);
    orwl_disconnect(&here_sec);
    orwl_disconnect(&here);
    orwl_disconnect(&there);

    //! [say good bye]
    orwl_stop_task();
    //! [say good bye]
  }
}
