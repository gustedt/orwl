#!/bin/bash
# Indique au système que l'argument qui suit est le programme utilisé pour exécuter ce fichier
# En règle générale, les "#" servent à mettre en commentaire le texte qui suit comme ici

echo Liste des fichiers :
ls -la

ffmpeg -i videos/tracking_out.avi -i videos/tracking_out_1.avi -filter_complex "blend=all_mode=difference" -c:v libx264 -crf 18 -c:a copy videos/diff.avi
 
exit 0
