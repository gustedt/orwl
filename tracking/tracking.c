/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the authors and copyright holders for this work are as follows:            */
/* all rights reserved,  2015-2016 Farouk Mansouri, INRIA, France             */
/* all rights reserved,  2016 Jens Gustedt, INRIA, France                     */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#include "tracing.h"
#include "tracking.h"
#include "wrapper.h"

//#define DEBUG

File *maFile;
int compteur;

void track(orwl_handle2 *there, orwl_handle2 *here, orwl_handle2 *here_sec, int loop, int loop_size, int mytid, int size_h , int size_w ) {
  ORWL_TIMER(TRACK)
  ORWL_POTI_SECTION(loop, loop_size, "TRACKING_Task_N°%d", 6) {

#ifdef DEBUG
    printf(" \n I'm a task Tracking N° %d \ %d\n", loop, loop_size);
#endif

    IplImage *frame_in ;
    void ** blobs_in  = malloc (sizeof(void *));

    if (loop ==0) {
      maFile = initialiser();
      compteur = 0;
    }


    while ( ( orwl_test(there) != orwl_acquired ) && (compteur < loop_size) ) {
      // printf(" \n I'm a task tracking N° %d | while N°1 %d \n", loop, orwl_test(there));
      while ( orwl_test(here_sec) != orwl_acquired ) {
        // printf(" \n I'm a task tracking N° %d | while N°2 %d \n", loop, orwl_test(here_sec));
        if ( (file_size(maFile) !=0)  &&  ( orwl_test(there) == orwl_acquired ) ) {
          // printf(" \n I'm a task tracking N° %d | IF N°1 %d \n", loop, orwl_test(there));
          goto saut;
        }
      }
      /// reading image from PRODUCER ///
      ORWL_SECTION(here_sec) {
        double ** w2val2 = orwl_read_map(here_sec);
        frame_in = (IplImage *) (*w2val2);
      }

      enfiler(maFile, frame_in);
      compteur++;
    }

saut:
    /// reading blobs from BLOB ///
    ORWL_SECTION(there) {
      double const** rval = orwl_read_map(there);
      (* blobs_in) =    (void*)  (*rval);
    }

    IplImage *frame_in2 = defiler(maFile);

    ORWL_POTI_EVENT("1");

    /// some processing ///
    track_apply(frame_in2, frame_in2, blobs_in );

    free(blobs_in);

    ORWL_POTI_EVENT("2");

    /// writing
    ORWL_SECTION(here) {
      double ** w2val = orwl_write_map(here);
      *w2val = (double*) frame_in2   ;
    }
  }
}
