// This may look like C code, but it really is -*- mode: c++; coding: utf-8 -*-
//
// Except for parts copied from previous work and as explicitly stated below,
// the authors and copyright holders for this work are as follows:
// all rights reserved,  2015-2016 Farouk Mansouri, INRIA, France
// all rights reserved,  2016 Jens Gustedt, INRIA, France
//
// This file is part of the ORWL project. You received this file as as
// part of a confidential agreement and you may generally not
// redistribute it and/or modify it, unless under the terms as given in
// the file LICENSE.  It is distributed without any warranty; without
// even the implied warranty of merchantability or fitness for a
// particular purpose.
//
#include "wrapper.h"


using namespace cv;
using namespace cvb;
using namespace std;

//#define TIMING
//#define DEBUG
#define bgMax 16

template< typename T >
inline
T maxof(T x, T y) {
  return (((x) > (y)) ? (x) : (y));
}
template< typename T >
inline
T minof(T x, T y) {
  return (((x) < (y)) ? (x) : (y));
}
#define EPSILON 2.2204460492503131e-16

typedef std::pair<int, int> intPair;

BackgroundSubtractorMOG2 bg[bgMax];

int test() {
  return 10;
}


void gmm_init() {
  //bg = createBackgroundSubtractorMOG2();//MOG2 Background subtractor
  //bg->setVarThreshold(128);
  //bg->setDetectShadows(false);
  for (int i=0; i<bgMax; i++) {
    bg[i].setBool("detectShadows", false);
    //bg[i].setVarThreshold(128);
  }
#ifdef DEBUG
  cout << " GMM initilized " << endl ;
#endif
}

void gmm_apply( IplImage * frame, IplImage * fore, int i) {
  Mat mat_frame(frame);
  Mat mat_fore(fore);
  //mat_frame = cvarrToMat(frame);   /// not a copy, just pointer
  bg[i](mat_frame, mat_fore, -1);
  mat_frame.release();  /// release the frame image created in producer.c
}

void blob_init(void ** blob_in) {
  // this new is probably a memory leak
  *blob_in = (void*)new CvBlobs();
  //cout << "adresse blobs 1" <<  *blobs << "  " << blobs << endl;
}



void blob_detect(IplImage * fore_in, IplImage * fore_out, void ** blobs_in , int size_h , int size_w, int mytid) {
  // this new is probably a memory leak
  CvBlobs * blobs = new CvBlobs();
  IplImage * img_label=cvCreateImage(cvSize(size_w, size_h), IPL_DEPTH_LABEL, 1);
  unsigned int result=cvLabel(fore_in, img_label, *blobs);

#ifdef DEBUG
  if (mytid==7) for (CvBlobs::const_iterator it=(*blobs).begin(); it!=(*blobs).end(); ++it) {
      cout << "Tid: " << mytid << " cvLabel "<< it->first <<" Blob #" << it->second->label << ": Area=" << it->second->area << ", Centroid=(" << it->second->centroid.x << ", " << it->second->centroid.y << ")" <<
           " MinX " << it->second->minx << " MaxX "<<it->second->maxx
           << " MinY " << it->second->miny << " MaxY "<<it->second->maxy << endl;
    }
#endif
  cvReleaseImage(&img_label);
  *blobs_in = blobs;
}


void blobs_compare(void ** blobs_in1 , void ** blobs_in2) {

  CvBlobs * blobs_merge1 = (CvBlobs *) (*blobs_in1);
  CvBlobs * blobs_merge2 = (CvBlobs *) (*blobs_in2);

  cout << "*********************************************************************************************" << endl;
  cout << "*********************************************************************************************" << endl;
  cout << "********************************////////////////////////*************************************" << endl;

  if (blobs_merge2->size() != blobs_merge1->size() )
    cout << "size blob_in original: "
         << blobs_merge1->size()
         << "| size blob_in merged: "
         << blobs_merge2->size()
         << endl;

//if (blobs_merge2->size() > blobs_merge1->size() )

  for (size_t i=1; i<= blobs_merge1->size(); i++) {
    CvBlobs::const_iterator it1 = blobs_merge1->find(i);
    CvBlob * temp1 = it1->second;
    CvBlob * temp2=NULL;

    for (size_t j=1; j<= blobs_merge2->size(); j++) {
      CvBlobs::const_iterator it2 = blobs_merge2->find(j);
      if ((it2->second->area == it1->second->area) && (it2->second->minx == it1->second->minx)) {
        temp2  = it2->second;
        break;
      }
    }


    if (temp2 == NULL ) {
      cout << "/**Alert**/ -A- pas de correspondant pour le blob N° : " << i << endl;
      blob_print(temp1);
    }
    //else /*blob_compare (temp1,temp2)*/;



  }


  for (size_t i=1; i<= blobs_merge2->size(); i++) {
    CvBlobs::const_iterator it2 = blobs_merge2->find(i);
    CvBlob * temp2 = it2->second;
    CvBlob * temp1=NULL;

    for (size_t j=1; j<= blobs_merge1->size(); j++) {
      CvBlobs::const_iterator it1 = blobs_merge1->find(j);
      if ((it1->second->area == it2->second->area) && (it1->second->minx == it2->second->minx)) {
        temp1  = it1->second;
        break;
      }
    }

    if (temp1 == NULL ) {
      cout << "/**Alert**/ -B- pas de correspondant pour le blob N° : " << i << endl;
      blob_print(temp2);
    }
    //else /*blob_compare (temp1,temp2)*/;
  }

  cout << "********************************////////////////////////*************************************" << endl;
  cout << "*********************************************************************************************" << endl;

}


void blob_print(CvBlob * blob1) {
  cout << "Blob prinnt : " << endl ;
  cout << "Label : " << blob1->label << endl ;
  cout << "Area : " << blob1->area << endl ;
  cout << "m00 : " << blob1->m00 << endl ;
  cout << "Minx : " << blob1->minx << endl ;
  cout << "Maxx : " << blob1->maxx << endl ;
  cout << "Miny : " << blob1->miny << endl ;
  cout << "Maxy : " << blob1->maxy << endl ;
  cout << "Centroide.x : " << blob1->centroid.x << endl ;
  cout << "Centroide.y : " << blob1->centroid.y << endl ;
  cout << "m10 : " << blob1->m10 << endl ;
  cout << "m01 : " << blob1->m01 << endl ;
  cout << "m11 : " << blob1->m11 << endl ;
  cout << "m20 : " << blob1->m20 << endl ;
  cout << "m02 : " << blob1->m02 << endl ;
  cout << "u11 : " << blob1->u11 << endl ;
  cout << "u02 : " << blob1->u02 << endl ;
  cout << "u20 : " << blob1->u20 << endl ;
  cout << "n11 : " << blob1->n11 << endl ;
  cout << "n02 : " << blob1->n02 << endl ;
  cout << "n20 : " << blob1->n20 << endl ;
  cout << "p1 : " << blob1->p1 << endl ;
  cout << "p2 : " << blob1->p2 << endl ;

  CvContourChainCode * coutour1 = &(blob1->contour);
  cout << "startingpoint of contour : X= " << coutour1->startingPoint.x << " Y=" << coutour1->startingPoint.y << endl;

  /* CvContourPolygon *polygone1 = cvConvertChainCodesToPolygon(&(blob1->contour));
    for(CvContourPolygon::iterator it = (*polygone1).begin(); it != (*polygone1).end(); ++it)
    {
    cout << "x: " << it->x << "  y: " << it->y << endl ;
    } */

  cout <<  (blob1->internalContours).size() << endl;

  for(CvContoursChainCode::iterator it = (blob1->internalContours).begin(); it != (blob1->internalContours).end(); ++it) {
    // cout << "x: " << it->x << "  y: " << it->y << endl ;
    cout << "startingpoint of contours : X= " << (*it)->startingPoint.x << " Y= "<< (*it)->startingPoint.y << endl;
  }

}



void blob_compare(CvBlob * blob1, CvBlob * blob2) {
  cout <<"----------------------------" <<endl;
  cout << "Blob original : " << endl ;
  cout << "Label : " << blob1->label << endl ;
  cout << "Area : " << blob1->area << endl ;
  cout << "m00 : " << blob1->m00 << endl ;
  cout << "Minx : " << blob1->minx << endl ;
  cout << "Maxx : " << blob1->maxx << endl ;
  cout << "Miny : " << blob1->miny << endl ;
  cout << "Maxy : " << blob1->maxy << endl ;
  cout << "Centroide.x : " << blob1->centroid.x << endl ;
  cout << "Centroide.y : " << blob1->centroid.y << endl ;
  cout << "m10 : " << blob1->m10 << endl ;
  cout << "m01 : " << blob1->m01 << endl ;
  cout << "m11 : " << blob1->m11 << endl ;
  cout << "m20 : " << blob1->m20 << endl ;
  cout << "m02 : " << blob1->m02 << endl ;
  cout << "u11 : " << blob1->u11 << endl ;
  cout << "u02 : " << blob1->u02 << endl ;
  cout << "u20 : " << blob1->u20 << endl ;
  cout << "n11 : " << blob1->n11 << endl ;
  cout << "n02 : " << blob1->n02 << endl ;
  cout << "n20 : " << blob1->n20 << endl ;
  cout << "p1 : " << blob1->p1 << endl ;
  cout << "p2 : " << blob1->p2 << endl ;

  CvContourChainCode * coutour1 = &(blob1->contour);
  cout << "startingpoint of contour : X= " << coutour1->startingPoint.x << " Y=" << coutour1->startingPoint.y << endl;

  //cout << "p2 : " << blob1->p2 << endl ;
  /* CvContourPolygon *polygone1 = cvConvertChainCodesToPolygon(&(blob1->contour));
   for(CvContourPolygon::iterator it = (*polygone1).begin(); it != (*polygone1).end(); ++it)
   {
   cout << "x: " << it->x << "  y: " << it->y << endl ;
   }*/


  for(CvContoursChainCode::iterator it = (blob1->internalContours).begin(); it != (blob1->internalContours).end(); ++it) {
    // cout << "x: " << it->x << "  y: " << it->y << endl ;
    cout << "startingpoint of contours : X= " << (*it)->startingPoint.x << " Y= "<< (*it)->startingPoint.y << endl;
  }


  cout <<"---------------" <<endl;
  cout << "Blob merged : " << endl ;
  cout << "Label : " << blob2->label << endl ;
  cout << "Area : " << blob2->area << endl ;
  cout << "m00 : " << blob2->m00 << endl ;
  cout << "Minx : " << blob2->minx << endl ;
  cout << "Maxx : " << blob2->maxx << endl ;
  cout << "Miny : " << blob2->miny << endl ;
  cout << "Maxy : " << blob2->maxy << endl ;
  cout << "Centroide.x : " << blob2->centroid.x << endl ;
  cout << "Centroide.y : " << blob2->centroid.y << endl ;
  cout << "m10 : " << blob2->m10 << endl ;
  cout << "m01 : " << blob2->m01 << endl ;
  cout << "m11 : " << blob2->m11 << endl ;
  cout << "m20 : " << blob2->m20 << endl ;
  cout << "m02 : " << blob2->m02 << endl ;
  cout << "u11 : " << blob2->u11 << endl ;
  cout << "u02 : " << blob2->u02 << endl ;
  cout << "u20 : " << blob2->u20 << endl ;
  cout << "n11 : " << blob2->n11 << endl ;
  cout << "n02 : " << blob2->n02 << endl ;
  cout << "n20 : " << blob2->n20 << endl ;
  cout << "p1 : " << blob2->p1 << endl ;
  cout << "p2 : " << blob2->p2 << endl ;

  CvContourChainCode * coutour2 = &(blob2->contour);
  cout << "startingpoint of contour : X= " << coutour2->startingPoint.x << " Y=" << coutour2->startingPoint.y << endl;

  /* CvContourPolygon *polygone2 = cvConvertChainCodesToPolygon(&(blob2->contour));
   for(CvContourPolygon::iterator it = (*polygone2).begin(); it != (*polygone2).end(); ++it)
   {
   cout << "x: " << it->x << "  y: " << it->y << endl ;
   }*/

  for(CvContoursChainCode::iterator it = (blob2->internalContours).begin(); it != (blob2->internalContours).end(); ++it) {
    // cout << "x: " << it->x << "  y: " << it->y << endl ;
    cout << "startingpoint of contours : X= " << (*it)->startingPoint.x << " Y= "<< (*it)->startingPoint.y << endl;
  }


  if (blob1->area != blob2->area)  cout << "/**Alert**/ Blob original area " << blob1->area << "| Blob merged area " << blob2->area << endl;
  if (blob1->m00 != blob2->m00)  cout << "/**Alert**/ Blob original m00 " << blob1->m00 << "| Blob merged m00 " << blob2->m00 << endl;
  if (blob1->minx != blob2->minx)  cout << "/**Alert**/ Blob original minx " << blob1->minx << "| Blob merged minx " << blob2->minx << endl;
  if (blob1->maxx != blob2->maxx)  cout << "/**Alert**/ Blob original maxx " << blob1->maxx << "| Blob merged maxx " << blob2->maxx << endl;
  if (blob1->miny != blob2->miny)  cout << "/**Alert**/ Blob original miny " << blob1->miny << "| Blob merged miny " << blob2->miny << endl;
  if (blob1->maxy != blob2->maxy)  cout << "/**Alert**/ Blob original maxy " << blob1->maxy << "| Blob merged maxy " << blob2->maxy << endl;
  if (blob1->centroid.x != blob2->centroid.x)  cout << "/**Alert**/ Blob original centroid.x " << blob1->centroid.x << "| Blob merged centroid.x " << blob2->centroid.x << endl;
  if (blob1->centroid.y != blob2->centroid.y)  cout << "/**Alert**/ Blob original centroid.y " << blob1->centroid.y << "| Blob merged centroid.y " << blob2->centroid.y << endl;
  if (blob1->m10 != blob2->m10)  cout << "/**Alert**/ Blob original m10 " << blob1->m10 << "| Blob merged m10 " << blob2->m10 << endl;
  if (blob1->m01 != blob2->m01)  cout << "/**Alert**/ Blob original m01 " << blob1->m01 << "| Blob merged m01 " << blob2->m01 << endl;
  if (blob1->m11 != blob2->m11)  cout << "/**Alert**/ Blob original m11 " << blob1->m11 << "| Blob merged m11 " << blob2->m11 << endl;
  if (blob1->m20 != blob2->m20)  cout << "/**Alert**/ Blob original m20 " << blob1->m20 << "| Blob merged m20 " << blob2->m20 << endl;
  if (blob1->m02 != blob2->m02)  cout << "/**Alert**/ Blob original m02 " << blob1->m02 << "| Blob merged m02 " << blob2->m02 << endl;
  if ((int)(blob1->u11 - blob2->u11) != 0)  cout << "/**Alert**/ Blob original u11 " << blob1->u11 << "| Blob merged u11 " << blob2->u11 << endl;
  if ((int)(blob1->u02 - blob2->u02) != 0 )  cout << "/**Alert**/ Blob original u02 " << blob1->u02 << "| Blob merged u02 " << blob2->u02 << endl;
  if ((int)((blob1->u20) - (blob2->u20)) != 0 )  cout << "/**Alert**/ Blob original u20 " << blob1->u20 << "| Blob merged u20 " << blob2->u20 << endl;
  if (blob1->n11 - blob2->n11 != 0)  cout << "/**Alert**/ Blob original n11 " << blob1->n11 << "| Blob merged n11 " << blob2->n11 << endl;
  if (fabs((double)((blob1->n02) - (blob2->n02))) != 0.0)   cout << "/**Alert**/ Blob original n02 " << blob1->n02 << "| Blob merged n02 " << blob2->n02 << endl;
  if ((blob1->n20) - (blob2->n20) != 0 )  cout << "/**Alert**/ Blob original n20 " << blob1->n20 << "| Blob merged n20 " << blob2->n20 << endl;
  if (coutour1->startingPoint.y - coutour2->startingPoint.y != 0) cout << "/**Alert**/ startingpoint of contour : Y1= " << coutour1->startingPoint.y << " Y2=" << coutour2->startingPoint.y << endl;
  printf("\n ** %lf | %lf | %lf **\n",(double)((blob1->n02) - (blob2->n02)) , fabs(((blob1->n02) - (blob2->n02))), 0.555);

}


void blobs_merge(void ** blobs_in, void ** blobs_out, size_t nbre_blobs, int size_h , int size_w) {

  CvBlobs * blobs [nbre_blobs] ;
  // these new are probably memory leaks
  CvBlobs * blobs_merge = new CvBlobs();
  CvBlobs * blobs_merge_temp = new CvBlobs();

  int label=1;
  int label_temp=1;

  for (size_t i = 0; i< nbre_blobs; i++) {
    blobs [i] = (CvBlobs *) blobs_in[i];
    for (CvBlobs::const_iterator it=(*(blobs[i])).begin(); it!=(*(blobs[i])).end(); ++it) {
      //blob_print(it->second);
      if ( ((it->second->maxy == ((size_h/nbre_blobs)-1)) && i<(nbre_blobs-1) ) || ( it->second->miny == 0) ) { ///  with overlap
        /// stocker les blobs qui sont sur la frontiére ///
        CvBlob *blob_temp = it->second ;
        blob_temp->miny = blob_temp->miny + (i*(size_h/nbre_blobs));
        blob_temp->maxy = blob_temp->maxy + (i*(size_h/nbre_blobs));
        //blob_temp->centroid.y = blob_temp->centroid.y + (i*(size_h/nbre_blobs));
        double m01 = blob_temp->m01;
        blob_temp->m01 = blob_temp->m01 + (i*(size_h/nbre_blobs)*blob_temp->area);
        blob_temp->centroid.y = blob_temp->m01/blob_temp->m00 ;
        blob_temp->m02 = blob_temp->m02 + ((i*(size_h/nbre_blobs))*(i*(size_h/nbre_blobs))*blob_temp->area) + 2*(i*(size_h/nbre_blobs))*m01 ;
        blob_temp->m11 = blob_temp->m11 + ((i*(size_h/nbre_blobs))*blob_temp->m10);
        blob_temp->contour.startingPoint.y = blob_temp->contour.startingPoint.y + (i*(size_h/nbre_blobs));

        /* Bug manu/souris
         * for(CvContoursChainCode::iterator it1 = (blob_temp->internalContours).begin(); it1 != (blob_temp->internalContours).end(); ++it1)
         {
           CvContourChainCode * coutour1 = *it1;
          (*it1)->startingPoint.y = (*it1)->startingPoint.y + (i*(size_h/nbre_blobs));
         }*/

        blobs_merge_temp->insert(make_pair(label_temp, blob_temp));
        label_temp++;

        /// afficher toutes les info sur les blobs et comparer les changements ///

      } else {
        /// rajouter les blobs indépendants avec mise à jour sur la coordonnée Y ///
        if (  it->second->area  > 250+ (250*i)) { /// filter by area
          CvBlob *blob_temp = it->second ;
          blob_temp->label = label;
          blob_temp->miny = blob_temp->miny + (i*(size_h/nbre_blobs));
          blob_temp->maxy = blob_temp->maxy + (i*(size_h/nbre_blobs));
          double m01 = blob_temp->m01;
          blob_temp->m01 = blob_temp->m01 + (i*(size_h/nbre_blobs)*blob_temp->area);
          blob_temp->centroid.y = blob_temp->m01/blob_temp->m00 ;
          blob_temp->m02 = blob_temp->m02 + ((i*(size_h/nbre_blobs))*(i*(size_h/nbre_blobs))*blob_temp->area) + 2*(i*(size_h/nbre_blobs))*m01 ;
          blob_temp->m11 = blob_temp->m11 + ((i*(size_h/nbre_blobs))*blob_temp->m10);
          blob_temp->contour.startingPoint.y = blob_temp->contour.startingPoint.y + (i*(size_h/nbre_blobs));

          /* Bug manu
           * for(CvContoursChainCode::iterator it1 = (blob_temp->internalContours).begin(); it1 != (blob_temp->internalContours).end(); ++it1)
           {
             CvContourChainCode * coutour1 = *it1;
            (*it1)->startingPoint.y = (*it1)->startingPoint.y + (i*(size_h/nbre_blobs));
           }*/

          blobs_merge->insert(make_pair(label, blob_temp));
          label++;
        }
      }
    }

  }

//cout << " Size blobs not in the border: " << blobs_merge->size() << endl;
//cout << " Size blobs in the border: " << blobs_merge_temp->size() << endl;

  std::vector<std::pair<int, int> > BlobsPairs;
  int label2=0;

  for (CvBlobs::const_iterator it=(*(blobs_merge_temp)).begin(); it!=(*(blobs_merge_temp)).end(); ++it) {
    label2++;
//cout << "Blob Merge bordure " <<" cvLabel "<< it->first << " Blob #" << it->second->label << ": Area=" << it->second->area << ", Centroid=(" << it->second->centroid.x << ", " << it->second->centroid.y  << ")"
// << " MinX " << it->second->minx << " MaxX "<<it->second->maxx
// << " MinY " << it->second->miny << " MaxY "<<it->second->maxy  << endl;

    for (CvBlobs::const_iterator it1=(*(blobs_merge_temp)).begin(); it1!=(*(blobs_merge_temp)).end(); ++it1) {
      if ( (it->second->maxy)+1 == it1->second->miny ) { /// if both blobs are in the same border
        if ( (it->second->maxx < it1->second->minx ) || (it->second->minx > it1->second->maxx ) ) { /// if there are not overlaping of x values
        } else {

//  cout << "Blob connected " <<" cvLabel "<< it->first << " Blob #" << it->second->label << ": Area=" << it->second->area << ", Centroid=(" << it->second->centroid.x << ", " << it->second->centroid.y  << ")"
//    << " MinX " << it->second->minx << " MaxX "<<it->second->maxx
//    << " MinY " << it->second->miny << " MaxY "<<it->second->maxy  << endl;

//    cout << "Blob connected " <<" cvLabel "<< it1->first << " Blob #" << it1->second->label << ": Area=" << it1->second->area << ", Centroid=(" << it1->second->centroid.x << ", " << it1->second->centroid.y  << ")"
//    << " MinX " << it1->second->minx << " MaxX "<<it1->second->maxx
//    << " MinY " << it1->second->miny << " MaxY "<<it1->second->maxy  << endl;

          CvContourPolygon *polygone1 = cvConvertChainCodesToPolygon(&(it->second->contour));
          CvContourPolygon *polygone2 = cvConvertChainCodesToPolygon(&(it1->second->contour));

          for(CvContourPolygon::iterator it_poly1 = (*polygone1).begin(); it_poly1 != (*polygone1).end(); ++it_poly1) {
            //  cout << "---" << endl;
            if ((it_poly1->y+1)%(size_h/nbre_blobs) ==0) {
              for(CvContourPolygon::iterator it_poly2 = (*polygone2).begin(); it_poly2 != (*polygone2).end(); ++it_poly2) {
                //  cout << "////" << endl;
                if (((it_poly2->y) == (it_poly1)->y+1) &&  ((it_poly2->y) == ((it_poly1+1)->y)+1 ) &&  ((it_poly2->y) == ((it_poly2+1)->y) )) {
                  int x1_min,x1_max,x2_min,x2_max;

                  x1_min = minof(it_poly1->x,(it_poly1+1)->x);
                  x1_max = maxof(it_poly1->x,(it_poly1+1)->x);

                  x2_min = minof(it_poly2->x,(it_poly2+1)->x);
                  x2_max = maxof(it_poly2->x,(it_poly2+1)->x);

                  //  cout << "x2: " << x2_min << "  y2: " << it_poly2->y << endl ;
                  //  cout << "x2: " << x2_max << "  y2: " << (it_poly2+1)->y << endl ;

                  //  cout << "x1: " << x1_min << "  y1: " << it_poly1->y << endl ;
                  //  cout << "x1: " << x1_max << "  y1: " << (it_poly1+1)->y << endl ;

                  if ( (x1_max < x2_min) || (x2_max < x1_min) ) {
                    //  cout << "pas de recouvrement" << endl;
                  } else {
                    //  cout << "recouvrement" << endl;
                    BlobsPairs.push_back(intPair(it->first, it1->first));
                  }
                }
              }
            }
          }
        }
      }
    }
  }

  std::vector<size_t> blob_list;

  for (size_t i=0; i< BlobsPairs.size(); i++) {
//cout << "Blob connected " <<"("<< BlobsPairs[i].first << " , " <<  BlobsPairs[i].second << " )" << endl;

    if(std::find(blob_list.begin(), blob_list.end(), BlobsPairs[i].first)==blob_list.end()) {
      blob_list.push_back(BlobsPairs[i].first);
      //  cout << "Blob connected " << BlobsPairs[i].first  << endl;
    }

    if(std::find(blob_list.begin(), blob_list.end(), BlobsPairs[i].second)==blob_list.end()) {
      blob_list.push_back(BlobsPairs[i].second);
      //   cout << "Blob connected " << BlobsPairs[i].second  << endl;
    }
  }

/// manage exception if there is a non connected blob on the border ///
  for (CvBlobs::const_iterator it=(*(blobs_merge_temp)).begin(); it!=(*(blobs_merge_temp)).end(); ++it) {
    bool exist=false;
    for (size_t i=0; i< blob_list.size(); i++) {
      if (it->first == blob_list[i]) {
        exist = true;
        break;
      }
    }

    if (exist == false) {
      //cout << " single blob on the border: " << it->first << endl;
      //blob_print(it->second);
      int pos = ((*(blobs_merge)).end())->first;
      CvBlob *temp_blob = it->second;
      temp_blob->label = pos;
      blobs_merge->insert(make_pair(pos+1, temp_blob));
    }
  }

  std::vector<int> File;
  std::vector<int> Marque;
  std::vector<std::vector<int> > blobs_m;

/// once the connected blobs identified merge them using the wide exporation graph algorithm (update) ///
  for (size_t i=0; i< blob_list.size(); i++) {
    if(std::find(Marque.begin(), Marque.end(), blob_list[i])==Marque.end()) {
      File.push_back(blob_list[i]);
      Marque.push_back(blob_list[i]);

// cout << "..........." << endl;
      // this new is probably a memory leak
      std::vector<int> *list_temp = new std::vector<int>;

      while ( File.size() != 0 ) {
        int s = File.front();
        File.erase(File.begin());
        //cout << s << endl;
        list_temp->push_back(s);
        Marque.push_back(s);

        for (size_t j=0; j< BlobsPairs.size(); j++) {
          if (BlobsPairs[j].first == s ) {
            int t = BlobsPairs[j].second;
            if(std::find(Marque.begin(), Marque.end(), t)==Marque.end()) {
              File.push_back(t);
              Marque.push_back(t);
              // cout << "pos "<< j << endl;
            }
          } else if (BlobsPairs[j].second == s ) {
            int t = BlobsPairs[j].first;
            if(std::find(Marque.begin(), Marque.end(), t)==Marque.end()) {
              File.push_back(t);
              Marque.push_back(t);
              // cout << "pos "<< j << endl;
            }
          }
        }
      }

      blobs_m.push_back(*list_temp);

    }
  }

  int pos = ((*(blobs_merge)).end())->first;
  for (size_t i=0; i<blobs_m.size(); i++) {
    // this new is probably a memory leak
    CvBlob *temp_blob = new  CvBlob();
    pos++;
//cout << "Connected : " << endl;
    for (size_t j=0; j<blobs_m[i].size(); j++) {
      CvBlobs::const_iterator it = blobs_merge_temp->find((blobs_m[i])[j]);
      //  cout << " Blob N° : " << it->first << " | " <<  it->second->label << endl;

      temp_blob->label = pos;
      if (temp_blob->miny!=0) temp_blob->miny = MIN(temp_blob->miny,it->second->miny); else temp_blob->miny = it->second->miny;
      temp_blob->maxy = MAX(temp_blob->maxy,it->second->maxy);
      if (temp_blob->minx!=0) temp_blob->minx = MIN(temp_blob->minx,it->second->minx); else temp_blob->minx = it->second->minx;
      temp_blob->maxx = MAX(temp_blob->maxx,it->second->maxx) ;

      temp_blob->area = temp_blob->area + it->second->area;
      temp_blob->m10 = temp_blob->m10 + it->second->m10;
      temp_blob->m01 = temp_blob->m01 + it->second->m01;
      temp_blob->m11 = temp_blob->m11 + it->second->m11;
      temp_blob->m20= temp_blob->m20 + it->second->m20;
      temp_blob->m02= temp_blob->m02 + it->second->m02;
      temp_blob->centroid.x = temp_blob->m10/temp_blob->area;
      temp_blob->centroid.y = temp_blob->m01/temp_blob->area ;
    }

    temp_blob->u11= temp_blob->m11 + (temp_blob->area * temp_blob->centroid.x * temp_blob->centroid.y) - (temp_blob->centroid.x * temp_blob->m01) - (temp_blob->centroid.y * temp_blob->m10);
    temp_blob->u20= temp_blob->m20 - (temp_blob->m10 * temp_blob->centroid.x * 2) + (temp_blob->centroid.x * temp_blob->centroid.x * temp_blob->m00);
    temp_blob->u02= temp_blob->m02 - (temp_blob->m01 * temp_blob->centroid.y * 2) + (temp_blob->centroid.y * temp_blob->centroid.y * temp_blob->m00);
    temp_blob->n11 = temp_blob->u11/ (temp_blob->m00*temp_blob->m00);
    temp_blob->n02 = temp_blob->u02/ (temp_blob->m00*temp_blob->m00);
    temp_blob->n20 = temp_blob->u20/ (temp_blob->m00*temp_blob->m00);
    temp_blob->p1 = (temp_blob->n20+temp_blob->n02);
    temp_blob->p2 = (temp_blob->n20-temp_blob->n02)*(temp_blob->n20-temp_blob->n02) + (4*(temp_blob->n11*temp_blob->n11));

    blobs_merge->insert(make_pair(pos, temp_blob));
    //cout << "  ----- Blob Merged : ----- " << endl;
    //blob_print(temp_blob);
  }

  /*for (CvBlobs::const_iterator it=(*(blobs_merge)).begin(); it!=(*(blobs_merge)).end(); ++it)
  {
    //cout << "Blob Merge " <<" cvLabel "<< it->first << " Blob #" << it->second->label << ": Area=" << it->second->area << ", Centroid=(" << it->second->centroid.x << ", " << it->second->centroid.y  << ")" \
   // << " MinX " << it->second->minx << " MaxX "<<it->second->maxx                                                                                                                                            \
   // << " MinY " << it->second->miny << " MaxY "<<it->second->maxy  << endl;
  }
  */

  (*blobs_out) = (void*) blobs_merge;

}



// this new is probably a memory leak
//
// please see if this global variable with dynamic initializer can be
// avoided
CvTracks *tracks = new CvTracks();
void track_apply( IplImage * img_in, IplImage * img_out, void ** blobs_in) {
  // cout << "adresse blobs 1" <<  *blobs_in  << "  " <<  blobs_in << endl;
  CvBlobs * blobs =(CvBlobs *) *blobs_in ;

  cvUpdateTracks(*blobs, *tracks, 8, 5);

#ifdef DEBUG
  for (CvBlobs::const_iterator it=(*blobs).begin(); it!=(*blobs).end(); ++it) {
    cout << "Blob #" << it->second->label << ": Area=" << it->second->area << ", Centroid=(" << it->second->centroid.x << ", " << it->second->centroid.y << ")" << endl;
  }
#endif

  cvRenderTracks(*tracks,img_in,img_out, CV_TRACK_RENDER_ID|CV_TRACK_RENDER_BOUNDING_BOX);
}
