/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the authors and copyright holders for this work are as follows:            */
/* all rights reserved,  2015-2016 Farouk Mansouri, INRIA, France             */
/* all rights reserved,  2016 Jens Gustedt, INRIA, France                     */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#ifndef GMM_H
#define GMM_H 1
#include "orwl_tracking_pipeline.h"
#include <omp.h>

void gmm(orwl_handle2 *there, orwl_handle2 *here, orwl_handle2 *there_sec , orwl_handle2 *here_sec, orwl_handle2 there_sec_gmm[], int loop, int loop_size, int mytid, int size_h , int size_w, int split_nbre );
void img_split ( IplImage * input, IplImage * img[], int num);
void img_merge (  IplImage * input[], IplImage ** output, int num);
void gmm_sub(orwl_handle2 *there, orwl_handle2 *here, int loop,  int loop_size ,int mytid, int size_h , int size_w , int split_nbre);

#endif
