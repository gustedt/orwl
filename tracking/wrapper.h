#ifndef WRAPPER_H
#define WRAPPER_H 1

#ifdef __cplusplus
#include <iostream>
#include <sstream>
#include <vector>
#include <opencv2/opencv.hpp>
#include <opencv2/video/background_segm.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/video/video.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <vector>
#include <cvblob.h>
#endif

#include <cv.h>
#include <highgui.h>
#include <sys/time.h>

enum { bgMax = 100 };

#ifdef __cplusplus
using namespace cvb;
extern "C" {
#else
  typedef struct CvBlob CvBlob;
#endif

int test (void);

void gmm_init(void);

void gmm_apply( IplImage * frame, IplImage * fore, int i);

void blob_init( void ** blob_in);

void blob_detect(IplImage * fore_in, IplImage * fore_out, void ** blobs_in ,int size_h , int size_w , int mytid);

void blobs_merge(void ** blobs_in, void ** blobs_out, size_t nbre_blobs, int size_h , int size_w);

void blobs_compare(void ** blobs_in1 , void ** blobs_in2);

void blob_compare(CvBlob * blob1, CvBlob * blob2);

void blob_print(CvBlob * blob1);

void track_apply( IplImage * img_in, IplImage * img_out, void ** blobs_in );

#ifdef __cplusplus
}
#endif

#endif
