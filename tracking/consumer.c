/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the authors and copyright holders for this work are as follows:            */
/* all rights reserved,  2015-2016 Farouk Mansouri, INRIA, France             */
/* all rights reserved,  2016 Jens Gustedt, INRIA, France                     */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#include "tracing.h"
#include "consumer.h"

//#define TIMING
//#define SAVING
//#define DISPLAY
//#define DEBUG

CvVideoWriter**out;

void consumer(orwl_handle2 *there, orwl_handle2 *here, int loop, int loop_size, int mytid, int size_h , int size_w ) {
  ORWL_TIMER(CONSUMER)
  ORWL_POTI_SECTION(loop, loop_size, "CONSUMER_Task_N°%d", 7) {

#ifdef DEBUG
    printf(" \n I'm a task Consumer N° %d \n", loop);
#endif

    IplImage *track_out;

    /// reading frame from TRACKING ///
    ORWL_SECTION(there) {
      double const** rval = orwl_read_map(there);
      track_out = (IplImage *)  (*rval);
    }

    ORWL_POTI_EVENT("1");

    /// displying ///
#ifdef DISPLAY
    cvNamedWindow("window", CV_WINDOW_AUTOSIZE);
    cvShowImage("window", track_out);
    cvWaitKey(0);
#endif

    /// saving ///
#ifdef SAVING
    if ( loop ==0 ) {
      out = cvCreateVideoWriter("tracking_out.avi", CV_FOURCC('D', 'I', 'V', 'X'), 30, cvSize((int)size_w,(int)size_h), 1);
    }
    int test = cvWriteFrame(out, track_out);
    printf(" \n I'm a task Consumer N° %d | video writer is : %d \n", loop, test);
    if (loop == loop_size-1 )   cvReleaseVideoWriter(&out);
#endif

    cvReleaseImage(&track_out);
    // cvDestroyWindow("window");

    ORWL_POTI_EVENT("2");

#ifdef TIMING
    static double last, cur;
    cur = seconds();
    // printf("Time of throuthput %ld s and %ld u_sec \n", cur.tv_sec-last.tv_sec, temp);
    printf("%g \n", cur-last);
    last = cur;
#endif
  }
}
