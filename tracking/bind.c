/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the authors and copyright holders for this work are as follows:            */
/* all rights reserved,  2016 Farouk Mansouri, INRIA, France                  */
/* all rights reserved,  2016 Jens Gustedt, INRIA, France                     */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#include "bind.h"

#define DEBUG

unsigned topo_nb_cores (void) {
  hwloc_topology_t topology;
  hwloc_topology_init(&topology);  // initialization
  hwloc_topology_load(topology);   // actual detection

  unsigned nbcores = hwloc_get_nbobjs_by_type(topology, HWLOC_OBJ_CORE);
  printf("%u cores\n", nbcores);

  hwloc_topology_destroy(topology);
  return nbcores;
}


//* @brief distribute with modulo
int orwl_bind_policy_hwloc(unsigned id, hwloc_topology_t topology) {
  unsigned depth = hwloc_topology_get_depth(topology);
#ifdef DEBUG
  printf("depth=%d\n",depth);
#endif

  /* Get my core. */
  hwloc_obj_t obj = hwloc_get_obj_by_depth(topology, depth-1, id);
  if (obj) {
    /* Get a copy of its cpuset that we may modify. */
    hwloc_cpuset_t cpuset = hwloc_bitmap_dup(obj->cpuset);

    char string[hwloc_obj_type_snprintf(0, 0, obj, 0)+1];
    hwloc_obj_type_snprintf(string, sizeof(string), obj, 0);
    printf("Index %u: %s\n", id, string);

    /* Get only one logical processor (in case the core is
       SMT/hyperthreaded). */
    hwloc_bitmap_singlify(cpuset);

    /* And try to bind ourself there. */
    int s = hwloc_set_cpubind(topology, cpuset, HWLOC_CPUBIND_THREAD);
    printf(" test %d \n", s);

    if (s==-1) {
      char *str;
      hwloc_bitmap_asprintf(&str, cpuset);
      printf("Binding thread %d to cpuset %s\n", id,str);

      int error = errno;
      hwloc_bitmap_asprintf(&str, obj->cpuset);
      fprintf(stderr,"Couldn't bind to cpuset %s: %s\n", str, strerror(error));
      free(str);
      exit(-1);
    }
    /* Free our cpuset copy */
    hwloc_bitmap_free(cpuset);
  } else {
    fprintf(stderr,"No valid object for core id %d!\n",id);
    exit(-1);
  }

  return 0;
}

//* @brief distribute with modulo
int orwl_bind_policy_0(unsigned mytid, hwloc_topology_t topology) {
  unsigned nbcores = hwloc_get_nbobjs_by_type(topology, HWLOC_OBJ_CORE);
#ifdef DEBUG
  printf("%d cores\n", nbcores);
#endif

  /*
  int depth;char string[128];unsigned i;
  int topodepth = hwloc_topology_get_depth(topology);
      for (depth = 0; depth < topodepth; depth++) {
        printf("*** Objects at level %d\n", depth);
        for (i = 0; i < hwloc_get_nbobjs_by_depth(topology, depth);
             i++) {
            hwloc_obj_type_snprintf(string, sizeof(string),
                       hwloc_get_obj_by_depth(topology, depth, i),
                       0);
            printf("Index %u: %s\n", i, string);
        }
    }
  */

  mytid %= nbcores;
  orwl_bind_policy_hwloc(mytid, topology) ;

  return 0;
}


//* @brief 1 core per numa node for the management
//*
//* execute with "taskset -c 7,15,23,31...."
int orwl_bind_policy_1(unsigned mytid, hwloc_topology_t topology) {
  unsigned nbcores = hwloc_get_nbobjs_by_type(topology, HWLOC_OBJ_CORE);
#ifdef DEBUG
  printf("%d cores\n", nbcores);
#endif

  unsigned id = (nbcores*(mytid/(nbcores-1)))+mytid%(nbcores-1);
  orwl_bind_policy_hwloc(id, topology) ;

  return 0;
}


//* @brief 2 cores per numa node for the management
//*
//* execute with "taskset -c 6,7,14,15,22,23,30,31...."
int orwl_bind_policy_2(unsigned mytid, hwloc_topology_t topology) {
  unsigned nbcores = hwloc_get_nbobjs_by_type(topology, HWLOC_OBJ_CORE);
#ifdef DEBUG
  printf("%d cores\n", nbcores);
#endif

  unsigned id = (nbcores*(mytid/(nbcores-2)))+mytid%(nbcores-2);
  orwl_bind_policy_hwloc(id, topology) ;

  return 0;
}


//* @brief 3 cores per numa node for the management
//*
//* execute with "taskset -c 7,15,23,31...."
int orwl_bind_policy_3(unsigned mytid, unsigned cup_size) {
  cpu_set_t cpuset;
  CPU_ZERO(&cpuset);
  CPU_SET((cup_size*(mytid/(cup_size-3)))+mytid%(cup_size-3), &cpuset);

  sched_setaffinity(0, sizeof(cpuset), &cpuset);
  /*printf(" Num thread : %s ******* Num CPU: %d \n", THRD2STR(thrd_current()), 0 );   */

  return 0;
}

//* @brief 1 numa node for (prod+gmm+gmm_sub) + 1 numa node for the rest
int orwl_bind_policy_4(unsigned mytid, hwloc_topology_t topology) {
#ifdef DEBUG
  unsigned nbcores = hwloc_get_nbobjs_by_type(topology, HWLOC_OBJ_CORE);
  printf("%d cores\n", nbcores);
#endif

  if (mytid >= 26) mytid -= 16 ;
  else if (mytid >=10) mytid += 6;

  orwl_bind_policy_hwloc(mytid, topology) ;
  return 0;
}

//* @brief 1 numa node for (prod+gmm+gmm_sub) + 1 numa node for the rest
int orwl_bind_policy_5(unsigned mytid, hwloc_topology_t topology) {
#ifdef DEBUG
  unsigned nbcores = hwloc_get_nbobjs_by_type(topology, HWLOC_OBJ_CORE);
  printf("%d cores\n", nbcores);
#endif

  if (mytid >= 26) mytid -= 16+6;
  else if (mytid >= 10) mytid += 6;
  else if (mytid >= 6) mytid += 2;

  orwl_bind_policy_hwloc(mytid, topology) ;
  return 0;
}


//* @brief 1 numa node for (prod+gmm+gmm_sub) + 1 numa node for the rest
//*
//* 14 split gmm
int orwl_bind_policy_6(unsigned mytid, hwloc_topology_t topology) {

#ifdef DEBUG
 unsigned nbcores = hwloc_get_nbobjs_by_type(topology, HWLOC_OBJ_CORE);
  printf("%d cores\n", nbcores);
#endif

  if (mytid==0) mytid = 0;
  else if (mytid==1) mytid = 15;
  else if (mytid<10) mytid += 14;
  else if (mytid<24) mytid -= 9;

  orwl_bind_policy_hwloc(mytid, topology) ;

  return 0;
}
