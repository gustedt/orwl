/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the author and copyright holder for this work is                           */
/* all rights reserved,  2015-2016 Farouk Mansouri, INRIA, France             */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#include "fifo.h"

File *initialiser() {
  // this malloc is probably a memory leak
  File *file = malloc(sizeof(*file));
  file->premier = NULL;
  return file;
}


void enfiler(File *file, IplImage *new_frame) {
  // this malloc is probably a memory leak
  Element *nouveau = malloc(sizeof(*nouveau));
  if (file == NULL || nouveau == NULL) {
    exit(EXIT_FAILURE);
  }

  nouveau->frame = new_frame;
  nouveau->suivant = NULL;

  if (file->premier != NULL) { /* La file n'est pas vide */
    /* On se positionne à la fin de la file */
    Element *elementActuel = file->premier;

    int i=0;

    while (elementActuel->suivant != NULL) {
      i++;
      elementActuel = elementActuel->suivant;
    }

    elementActuel->suivant = nouveau;
  }

  else { /* La file est vide, notre élément est le premier */
#ifdef DEBUG
    printf(" \n Taille fille : %d | %d \n", 0, new_frame);
#endif

    file->premier = nouveau;
  }

}


IplImage * defiler(File *file) {
  if (file == NULL) {
    exit(EXIT_FAILURE);
  }

  IplImage * imageDefile = 0;

  /* On vérifie s'il y a quelque chose à défiler */
  if (file->premier != NULL) {
    Element *elementDefile = file->premier;
    imageDefile = elementDefile->frame;
    file->premier = elementDefile->suivant;
    free(elementDefile);
  }
  return imageDefile;
}

int file_size(File *file) {
  int i=0;

  if (file == NULL ) {
    return(0);
//exit(EXIT_FAILURE);
  }

  if (file->premier != NULL) { /* La file n'est pas vide */
    /* On se positionne à la fin de la file */
    Element *elementActuel = file->premier;

    while (elementActuel->suivant != NULL) {
      i++;
      elementActuel = elementActuel->suivant;
    }
  }

  return(i);
}
