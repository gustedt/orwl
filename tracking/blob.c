/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the authors and copyright holders for this work are as follows:            */
/* all rights reserved,  2015-2016 Farouk Mansouri, INRIA, France             */
/* all rights reserved,  2016 Jens Gustedt, INRIA, France                     */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#include "tracing.h"
#include "wrapper.h"
#include "blob.h"

//#define DEBUG

void blob(orwl_handle2 *there, orwl_handle2 *here, orwl_handle2 *here_split, orwl_handle2 there_sec_blob[], int loop , int loop_size ,int mytid, int size_h , int size_w , int split) {
  ORWL_TIMER(BLOB)
  ORWL_POTI_SECTION(loop, loop_size, "BLOB_Task_N°%d", 5) {

#ifdef DEBUG
    printf(" \n I'm a task CCL N° %d \n", loop);
#endif

    IplImage *fore_in ;
    // These calls to malloc are probably memory leaks
    void ** blobs_in_sub  = malloc (split*sizeof(void *));
    void ** blobs_merge_out =   malloc (sizeof(void *));

#ifndef BLOB_SPLIT
    void ** blobs_in  = malloc (sizeof(void *));
    if ( loop ==0 ) { blob_init( blobs_in); }
    IplImage *fore_out_gray = cvCreateImage(cvSize(size_w, size_h), IPL_DEPTH_8U, 1);
#endif

    /// reading image from DILATE ///
    ORWL_SECTION(there) {
      IplImage *const* rval = orwl_read_map(there);
      fore_in = *rval;
    }

    ORWL_POTI_EVENT("1");

    ///// Spliting image ////

#ifdef DEBUG
    printf(" blob adresse 1 Section blob : %p \n", fore_in);
#endif

#ifdef BLOB_SPLIT
    /// writing image for split ///
    ORWL_SECTION(here_split) {
      IplImage ** w2val = orwl_write_map(here_split);
      *w2val =  fore_in;
    }

    /// reading sub-blobs from BLOB SPLIT ///
    for (int i=0; i<split; i++) {
      ORWL_SECTION(&(there_sec_blob[i])) {
        void *const* rval = orwl_read_map(&(there_sec_blob[i]));
        blobs_in_sub[i] = *rval;
      }
#ifdef DEBUG
      printf(" blob adresse 2 Section blob : %p \n", blobs_in_sub[i] );
#endif
    }

    blobs_merge(blobs_in_sub , blobs_merge_out , split, size_h, size_w);
    //// split done /////

#endif
    /// some processing ///

#ifndef BLOB_SPLIT
    blob_detect(fore_in,fore_out_gray, blobs_in, size_h, size_w, mytid);
#endif

    cvReleaseImage(&fore_in);
    //blobs_compare(blobs_in , blobs_merge_out);

    ORWL_POTI_EVENT("2");

#ifdef DEBUG
    printf(" blob adresse 1 : %p \n", (*blobs_merge_out));
#endif

    ORWL_SECTION(here) {
      void ** w2val = orwl_write_map(here);
      *w2val = (*blobs_merge_out);
      //*w2val = (*blobs_in);

#ifdef DEBUG
      printf(" blob adresse 1 Section blob : %p \n", (*w2val));
#endif
    }
  }
}




void blob_sub(orwl_handle2 *there, orwl_handle2 *here, int loop , int loop_size ,int mytid, int size_h , int size_w , int split_nbre, int split_nbre_gmm ) {
  int num_split = mytid-(10+split_nbre_gmm);
  ORWL_TIMER(BLOB_SPLIT)
  ORWL_POTI_SECTION(loop, loop_size, "BLOB_Split_N°%d", num_split) {

    IplImage *fore_in ;
    IplImage *fore_out_gray = cvCreateImage(cvSize(size_w, size_h/split_nbre), IPL_DEPTH_8U, 1);
    int y = (num_split % split_nbre) * (size_h/split_nbre);
    int x =0;

    CvRect rec = cvRect(x, y, size_w, size_h/split_nbre);

    /// automatiser le channel(1) et le detph(8) par argument dans la fonction
    IplImage * sub_fore = cvCreateImage(cvSize(size_w, size_h/split_nbre),8, 1);
    IplImage * sub_fore_temp = cvCreateImage(cvSize(size_w, size_h/split_nbre),8, 1);
    
    /*IplImage * sub_fore_temp =
      (loop==0)
      ? cvCreateImage(cvSize(size_w, size_h/split_nbre),8, 1)
      : 0;
    */
    /// reading sub_image from BLOB ///
    ORWL_SECTION(there) {
      IplImage *const* rval = orwl_read_map(there);
      fore_in = *rval;
    }

    CvMat fore_temp;
    cvGetSubRect(fore_in, &fore_temp, rec);
    sub_fore = cvGetImage(&fore_temp, sub_fore_temp);

    ORWL_POTI_EVENT("1");

    // this malloc is probably a memory leak
    void ** blobs_in  = malloc (sizeof(void *));

    /// some processing ///
    blob_detect(sub_fore,fore_out_gray, blobs_in, size_h/split_nbre, size_w, mytid);

    ORWL_POTI_EVENT("2");

#ifdef DEBUG
    printf(" blob sub adresse 1 : %p \n", sub_fore);
#endif
    /// writing sub-blob ///
    ORWL_SECTION(here) {
      void ** w2val = orwl_write_map(here);
      *w2val = (*blobs_in);
    }
  }
}
