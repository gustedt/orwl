#!/bin/bash
# Indique au système que l'argument qui suit est le programme utilisé pour exécuter ce fichier
# En règle générale, les "#" servent à mettre en commentaire le texte qui suit comme ici

echo Liste des fichiers :
ls -la

ffmpeg -y -i videos/tracking_out.avi -i videos/tracking_out_1.avi -filter_complex '[1:v]format=yuva444p,lut=c3=128,negate[video2withAlpha],[0:v][video2withAlpha]overlay[out]' -map [out] videos/diff.avi

 
exit 0
