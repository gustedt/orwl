/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the authors and copyright holders for this work are as follows:            */
/* all rights reserved,  2011 Emmanuel Jeanvoine, INRIA, France               */
/* all rights reserved,  2015-2016 Farouk Mansouri, INRIA, France             */
/* all rights reserved,  2011-2012, 2016 Jens Gustedt, INRIA, France          */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#ifndef ORWL_TRACKING_PIPELINE_H
#define ORWL_TRACKING_PIPELINE_H 1
#include <opencv/highgui.h>
#include <opencv/cv.h>
#include <poti.h>
#include <sched.h>
#include <bind.h>

//! [compile time parametrization]
#include "orwl.h"



ORWL_LOCATIONS_PER_TASK(main_loc, sec_loc, split_loc);

//! [compile time parametrization]

//! [declare task specific state]
P99_DECLARE_STRUCT(task_obj);
P99_DEFINE_STRUCT(task_obj,
                  double loop,
                  int size_h ,
                  int size_w
                 );

#endif
