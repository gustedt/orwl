/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the author and copyright holder for this work is                           */
/* all rights reserved,  2010-2012 Jens Gustedt, INRIA, France                */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#include "orwl.h"
#include "p99_c99_default.h"
#include "p99_str.h"

ORWL_KEYS(basic);
ORWL_KEYS_DEFINE();

P99_DECLARE_STRUCT(worker_thread);


/**
 ** Struct that contains the information that we want to pass on to
 ** each individual thread.
 **/
P99_DEFINE_STRUCT
(worker_thread,
 size_t offset,
 /** The thread id. **/
 size_t myloc,
 /** The number of iterations. **/
 size_t phases,
 /** A barrier that is used for initialization purposes. **/
 orwl_barrier* init_barr,
 /** A character array in the "server" thread that is used to show
  ** the progress of the application. **/
 char* info
);

worker_thread* worker_thread_init(worker_thread *arg, orwl_barrier* ba, size_t off, size_t m,
                                  size_t ph, char* inf) {
  if (arg)
    *arg = P99_LVAL(worker_thread const,
                    .offset = off,
                    .myloc = m,
                    .phases = ph,
                    .init_barr = ba,
                    .info = inf,
                   );
  return arg;
}

P99_PROTOTYPE(worker_thread*, worker_thread_init, worker_thread *, orwl_barrier*, size_t, size_t, size_t, char*);
/* Declare default arguments ... */
#define worker_thread_init(...) P99_CALL_DEFARG(worker_thread_init, 6, __VA_ARGS__)
/* ... of value 0 for all the arguments of the init function such that
   it may be used without parameters in worker_thread_vnew */
P99_DECLARE_DEFARG(worker_thread_init, P99_DUPL(6, 0));
P99_DEFINE_DEFARG(worker_thread_init, P99_DUPL(6, 0));

void worker_thread_destroy(worker_thread *arg) {
}

DECLARE_NEW_DELETE(worker_thread);
DEFINE_NEW_DELETE(worker_thread);

DECLARE_THREAD(worker_thread);


/** Define the function that each thread runs. **/
DEFINE_THREAD(worker_thread) {
  /* Lift all thread context fields to local variables of this thread */
  ORWL_THREAD_USE
  (worker_thread,
   offset,
   myloc,
   phases,
   init_barr,
   info);
  orwl_myloc = myloc;
  /**
   ** Initialization of some variables.
   **/
  size_t len = sizeof(uint64_t);
  {
    char const* env = getenv("ORWL_HANDLE_SIZE");
    if (env) {
      size_t len2 = strtouz(env);
      if (len2 > len) len = len2;
    }
  }

  /* Shift the info pointer to the position that this thread here is
   * due to manipulate. */
  if (info) info += 3*orwl_myloc + 1;

  /* Each thread holds 3 orwl_handle2. One for his own location and
   * one each for the location to the left and to the right. */
  orwl_handle2 handle[3] = { P99_DUPL(3, ORWL_HANDLE2_INITIALIZER) };

  /* The buffer for the reentrant pseudo random generator */
  p99_seed*const seed = p99_seed_get();

  /* Insert the three handles into the request queues. Positions 0 and
   * 2 are the neighbors, so these are only read requests. Position 1
   * is our own location where we want to write. */

  /* We have to be sure that the insertion into the three request
     queues is not mixed up. For the moment this is just guaranteed
     by holding a global mutex. */
  P99_CRITICAL {
    orwl_read_insert(&handle[0], ((orwl_nl + myloc - 1) % orwl_nl), !myloc ? 2 : (myloc%2));
    orwl_write_insert(&handle[1], myloc, !myloc ? 2 : (myloc%2));
    orwl_read_insert(&handle[2], ((orwl_nl + myloc + 1) % orwl_nl), !myloc ? 2 : (myloc%2));
  }
  orwl_barrier_wait(init_barr);

  p99_count_wait(&orwl_server_get()->waiters_cnt);

  /* Do some resizing, but only in the initial phase. This section
     will lock all locations but will only resize one of them. */
  ORWL_SECTION(handle) {
    orwl_truncate(&handle[1], len);
    REPORT(true, "handle resized to %s byte                                             \n",
           len);
  }


  for (orwl_phase = 1; orwl_phase < phases; ++orwl_phase) {

    /** Initial Phase: do something having requested the lock but not
     ** necessarily having it obtained.
     **/
    double const twait = 0.3;
    double const rwait = twait * p99_drand(seed);
    double const await = twait - rwait;

    /** Here for this dummy application we just take a nap **/
    sleepfor(await);


    /* Now work on our data. */
    int64_t diff[3] = { P99_TMIN(int64_t), P99_0(int64_t), P99_TMIN(int64_t) };

    /** Enter the critical section to access the different locations.
     ** This will block on entry until the locks are obtained.
     **/
    ORWL_SECTION(handle) {
      /* For handle 1 we have gained write access. Change the globally
       * shared data for the whole application. */
      {
        uint64_t* data = orwl_write_map(&handle[1]);
        assert(data);
        *data = orwl_phase;
      }
      /* For handle 0 and 2 we have read access. */
      {
        uint64_t const* data = orwl_read_map(&handle[0]);
        assert(data);
        diff[0] = *data > (orwl_phase - 1) ? *data - (orwl_phase - 1) : 0;
      }
      {
        uint64_t const* data = orwl_read_map(&handle[2]);
        assert(data);
        diff[2] = (orwl_phase - 1) > *data ? (orwl_phase - 1) - *data : 0;
      }
    }

    /** take another nap where a real application would do its work
     ** that needs access to the shared resource.
     **/
    sleepfor(rwait);

    /** Now write some data that keeps track of the local state of all
     ** threads. */
    if (info) {
      char num[10];
      sprintf(num, "  %zX", orwl_phase);
      memcpy(info, num + strlen(num) - 2, 2);
      if (diff[2] == P99_TMIN(int64_t))
        info[2] = '|';
      else
        info[2] = (abs(diff[2]) <= 2 ? ((char[]) { '-', '<', '.', '>', '+'})[diff[2] + 2] : '!');
      if (orwl_myloc == offset) {
        if (diff[0] == P99_TMIN(int64_t))
          info[-1] = '|';
        else
          info[-1] = (abs(diff[0]) <= 2 ? ((char[]) { '-', '<', '.', '>', '+'})[diff[0] + 2] : '!');
      }
    }
  }

  /** And at the very end of the lifetime of the thread cancel all
   ** locks such that no other thread is blocked and return.
   **/
  orwl_disconnect(handle);
}

int main(int argc, char **argv) {
  if (argc < 6) {
    report(1, "Usage: %s URL PHASES LOCAl GLOBAL OFFSET",
           argv[0]);
    REPORT(1, "only %s commandline arguments, this ain't enough",
           argc);
    return 1;
  }

  /* condition the run */
  size_t phases = str2uz(argv[2]);
  size_t number = str2uz(argv[3]);
  size_t nl = str2uz(argv[4]);
  size_t offset = str2uz(argv[5]);

  orwl_barrier init_barr;
  orwl_barrier_init(&init_barr, number + 1);

  /* To test both models of thread creation, half of the threads are
   * created detached and half joinable */
  thrd_t *const id = thrd_t_vnew(number/2);
  worker_thread *const arg = worker_thread_vnew(number/2);

  orwl_server* srv = 0;

  /* Protect the final destruction phase from preliminary exits */
  P99_TRY {
    /* start the server thread and initialize it properly */
    ORWL_KEY_SCALE(basic, number * 2);
    orwl_start(orwl_keys_total(), SOMAXCONN,,,,0);
    if (!orwl_alive()) P99_UNWIND_RETURN EXIT_FAILURE;
    orwl_server_block();
    srv = orwl_server_get();

    /* Now that the orwl server thread is up, in addition ensure that
       all detached threads are shut down correctly. */
    P99_TRY {
      /** The string "info" will be used as sort of common black board by
       ** the threads such that we can visualize their state. It has one
       ** suplementary char in front such that we may always address field
       ** -1 from the threads.
       */
      {
        size_t info_len = 3*nl;
        char* info = calloc(info_len + 2);
        memset(info, ' ', info_len + 1);
        for (size_t i = 3; i < info_len; i += 3)
          info[i] = '|';
        srv->info = info;
        srv->info_len = info_len;
      }

      REPORT(1, "%s: starting %s phases, %s/%s threads, offset %s",
      argv[0], phases, number, nl, offset);

      {
        /* set up the connections between the clients and the
         * server process. */
        FILE * f = fopen(argv[1], "r");
        orwl_address_book_read(&srv->ab, f);
        fclose(f);
        assert(nl == orwl_nl);
        srv->ll = 0;
        srv->first_location = offset;
      }

      orwl_open_group_graph();

      /* Fire up the worker threads. */
      for (size_t thread = 0; thread < number; ++thread) {
        if (thread%2) {
          /* The odd numbered ones are joinable and use an worker_thread that is
           * managed by this main thread, here. */
          size_t thread2 = thread/2;
          worker_thread_create_joinable(
            worker_thread_init(&arg[thread2],
          &init_barr,
          offset,
          thread + offset,
          phases,
          srv->info),
            &id[thread2]
          );
        } else {
          /* The even numbered ones are created detached and receive a
           * freshly created worker_thread for which they are responsible. */
          worker_thread_create_detached(
            P99_NEW(worker_thread,
                    &init_barr,
                    offset,
                    thread + offset,
                    phases,
                    srv->info)
          );
        }
      }

      orwl_barrier_wait(&init_barr);
      report(1, "everybody is in");
      orwl_server_unblock();
      report(1, "all request insertions locally launched");

      /* wait for even numbered threads, that have been started joinable. */
      for (size_t thread2 = 0; thread2 < number/2; ++thread2) {
        worker_thread_join(id[thread2]);
      }

    } P99_FINALLY {
      /* wait for the remaining threads, if any are still alive */
      /* and destruct ourselves */
      orwl_stop();
    }



  } P99_FINALLY {
    worker_thread_vdelete(arg);
    thrd_t_vdelete(id);
  }
  return EXIT_SUCCESS;
}
