/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the authors and copyright holders for this work are as follows:            */
/* all rights reserved,  2011-2012 Jens Gustedt, INRIA, France                */
/* all rights reserved,  2011 Matias E. Vara, INRIA, France                   */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#include "orwl.h"
#include "orwl_timing.h"
#include "orwl_thread.h"
#include "p99_c99_default.h"
#include "p99_str.h"

ORWL_KEYS(basic);
ORWL_KEYS_DEFINE();

P99_DECLARE_STRUCT(worker_thread);


/**
 ** Struct that contains the information that we want to pass on to
 ** each individual thread.
 **/
P99_DEFINE_STRUCT
(worker_thread,
 /** The thread id. **/
 size_t myloc,
 /** The number of iterations. **/
 size_t phases,
 /** An array of orwl_nl locations. **/
 orwl_mirror* left,
 /** A barrier that is used for initialization purposes. **/
 orwl_barrier* init_barr,
 /** A character array in the "server" thread that is used to show
  ** the progress of the application. **/
 char* info,
 /** The number of locations read **/
 size_t readers
);

worker_thread* worker_thread_init(worker_thread *arg, orwl_barrier* ba, size_t m,
                                  size_t ph, orwl_mirror* le, char* inf, size_t reads) {
  if (arg)
    *arg = P99_LVAL(worker_thread const,
                    .myloc = m,
                    .phases = ph,
                    .left = le,
                    .init_barr = ba,
                    .info = inf,
                    .readers = reads,
                   );
  return arg;
}

P99_PROTOTYPE(worker_thread*, worker_thread_init, worker_thread *, orwl_barrier*, size_t, size_t, orwl_mirror*, char*, size_t);
/* Initialize by all parameters set to 0 by default. */
#define worker_thread_init(...) P99_CALL_DEFARG(worker_thread_init, 7, __VA_ARGS__)
P99_DECLARE_DEFARG(worker_thread_init, P99_DUPL(7, 0));
P99_DEFINE_DEFARG(worker_thread_init, P99_DUPL(7, 0));

void worker_thread_destroy(worker_thread *arg) {
}

DECLARE_NEW_DELETE(worker_thread);
DEFINE_NEW_DELETE(worker_thread);

DECLARE_THREAD(worker_thread);


/** Define the function that each thread runs. **/
DEFINE_THREAD(worker_thread) {
  /* Lift all thread context fields to local variables of this thread */
  ORWL_THREAD_USE(worker_thread,
                  myloc,
                  phases,
                  left,
                  init_barr,
                  info,
                  readers
                 );
  orwl_myloc = myloc;
  REPORT(0, "starting thread, %s readers", readers);

  /**
   ** Initialization of some variables.
   **/

  /* Shift the info pointer to the position that this thread here is
   * due to manipulate. */
  if (info) info += 3*orwl_myloc + 1;


  /** Number of handlers, readers*2 for read and 1 for write **/
  size_t const nb_hand = 2*readers + 1;

  /* Each thread holds "nb_hand" orwl_handle2. One for his own location and for
   * the locations to the left and to the right. */
  orwl_handle2*const handle = orwl_handle2_vnew(nb_hand);

  /* The buffer for the reentrant pseudo random generator */
  p99_seed*const seed = p99_seed_get();

  P99_UNWIND_PROTECT {
    /* Insert the handles into the request queues. Positions 0 to "readers-1" and
     * "readers+1" to "2*readers" are the neighbors, so these are only read requests. Position "readers"
     * is our own location where we want to write. */
    ORWL_TIMER(worker_initialization) {
      orwl_barrier_wait(init_barr);
      /* Randomize, so the system will always be initialized differently. */
      sleepfor(p99_drand(seed) * 1E-1);
      /* We have to be sure that the insertion into the request
         queues is not mixed up. For the moment this is just guaranteed
         by holding a global mutex. */
      P99_CRITICAL {
        /* read request: left side   */
        orwl_read_request(left, handle, readers);
        /* write request: to my own position*/
        orwl_write_request(left + readers, handle + readers, 1);
        /* read request: rigth side   */
        orwl_read_request(left + readers + 1, handle + readers + 1, readers);
      }
      orwl_barrier_wait(init_barr);
    }
    /* Do some resizing = allocation, but only in an initial phase. */
    ORWL_TIMER(worker_allocation) {
      /* Do some precomputation of the desired data size */
      size_t len = 1;
      char const* env = getenv("ORWL_HANDLE_SIZE");
      if (env) {
        size_t len2 = strtouz(env) / sizeof(uint64_t);
        if (len2) len = len2;
      }

      /** A first critical section for which we ensure that we have all
          locks, but where we only modify our own location to resize it
          to our needs **/
      ORWL_SECTION(handle, nb_hand)
      orwl_truncate(&handle[readers], len);
    }

    for (orwl_phase = 1; orwl_phase < phases; ++orwl_phase) {

      /** Initial Phase: do something having requested the lock but not
       ** necessarily having it obtained.
       **/
      double const twait = 0.1;
      double const rwait = twait * p99_drand(seed);
      double const await = twait - rwait;

      /** Here for this dummy application we just take a nap **/
      ORWL_TIMER(worker_await)
      sleepfor(await);

      char diffLeft = P99_INIT;
      char diffRight = P99_INIT;

      /** Entering the critical section. During that we hold a write
       ** lock on our own location and read locks on the locations of
       ** the neighbors.
       **/
      ORWL_TIMER(worker_critical)
      ORWL_SECTION(handle, nb_hand) {
        memcpy(info, "..", 2);
        /* For handle [readers] we have gained write access. Change the globally
         * shared data for the whole application. */
        size_t data_len = 0;
        {
          uint64_t* data = orwl_write_map(&handle[readers]);
          assert(data);
          *data = orwl_phase;
        }
        /* For handle 0 and [readers+1] we have read access. */
        {
          uint64_t const* data = orwl_read_map(&handle[readers-1], &data_len);
          assert(data);
          uint64_t phaseRight = *data;

          diffRight = (phaseRight == (orwl_phase - 1))
                      ? '<'
                      : ((phaseRight == orwl_phase)
                         ? '>'
                         : '!');
        }
        ORWL_TIMER(worker_map) {
          uint64_t const* data = orwl_read_map(&handle[readers+1]);
          assert(data);
          uint64_t phaseLeft = *data;

          diffLeft = (phaseLeft == (orwl_phase - 1))
                     ? '<'
                     : ((phaseLeft == orwl_phase)
                        ? '>'
                        : '!');
        }

        /** take another nap where a real application would do its work
         ** that needs access to the shared resource.
         **/
        ORWL_TIMER(worker_rwait)
        sleepfor(rwait);

      } /* END of ORWL_SECTION */


      /** Now write some data that keeps track of the local state of all
       ** threads. */
      ORWL_TIMER(worker_info)
      if (info) {
        char num[10];
        sprintf(num, "  %zX", orwl_phase);
        memcpy(info, num + strlen(num) - 2, 2);
        info[2] = diffLeft ? diffLeft : '|';
        if (!orwl_myloc) {
          info[-1] = diffRight ? diffRight : '|';
        }
      }

    } /* END of phase loop */

P99_PROTECT:
    /** And at the very end of the lifetime of the thread cancel all
     ** locks such that no other thread is blocked and return.
     **/
    orwl_disconnect(handle, nb_hand);

    /** free the handles **/
    orwl_handle2_vdelete(handle);
  }
}

int main(int argc, char **argv) {
  if (argc < 3) {
    report(1, "Usage: %s PHASES LOCATIONS [READERS]",
           argv[0]);
    REPORT(1, "only %s commandline arguments, this ain't enough",
           argc);
    return 1;
  }

  /** Generate a random secret such that this location server is only
   ** accessible by itself. This is just a short cut for this simple
   ** application that doesn't speak to another remote participant.
   **/
  if (!getenv("ORWL_SECRET")) {
    size_t secret = p99_rand();
    char secretStr[32];
    sprintf(secretStr, "0x%zX", secret);
    setenv("ORWL_SECRET", secretStr, 1);
  }

  /* condition the run */
  size_t phases = str2uz(argv[1]);
  size_t nl = str2uz(argv[2]);
  /* number of orwl in both sides for read*/
  size_t orwl_readers = (argc > 3) ? str2uz(argv[3]) : 1;
  REPORT(1, "startup, %s readers", orwl_readers);

  /**
   ** "location" is a vector of locations with basically one location per
   ** thread. The extra ones are needed to model the wrap around
   ** at the boundaries
   **/
  orwl_mirror*const location_back = orwl_mirror_vnew(nl + 2*orwl_readers);
  orwl_mirror*const location = location_back + orwl_readers;
  /* To test both models of thread creation, half of the threads are
   * created detached and half joinable */
  thrd_t *const id = thrd_t_vnew(nl/2);
  worker_thread *const arg = worker_thread_vnew(nl/2);

  orwl_barrier init_barr;
  orwl_barrier_init(&init_barr, nl);

  P99_UNWIND_PROTECT {
    /* start the server thread and initialize it properly */
    ORWL_KEY_SCALE(basic, nl * 2);
    ORWL_TIMER(server_start) {
      orwl_start(orwl_keys_total(), SOMAXCONN, , false, ORWL_LOCALHOST, 0);
      if (!orwl_alive()) P99_UNWIND_RETURN EXIT_FAILURE;

      /** The string "info" will be used as sort of common black board by
       ** the threads such that we can visualize their state. It has one
       ** suplementary char in front such that we may always address
       ** field -1 from the threads.
       */
      size_t info_len = 3*nl;
      char* info = calloc(info_len + 2);
      memset(info, ' ', info_len + 1);
      for (size_t i = 3; i < info_len; i += 3)
        info[i] = '|';
      orwl_server_get()->info = info;
      orwl_server_get()->info_len = info_len;
    }

    P99_UNWIND_PROTECT {

      REPORT(1, "%s: starting %s phases, %s locations and threads",
      argv[0], phases, nl);

      ORWL_TIMER(connecting) {
        /* set up the connections between the clients and the
         * server thread. */
        orwl_endpoint there = orwl_server_get()->ep;

        for (ssize_t i = (-orwl_readers); i < (ssize_t)(nl+orwl_readers); ++i) {
          size_t gpos = (nl + i) % nl;
          orwl_mirror_connect(&location[i], there, ORWL_KEY(basic, gpos));
          report(0, "connected to %s", orwl_endpoint_print(&there));
        }
      }
      /* Fire up the worker threads. */
      ORWL_TIMER(launching)
      for (size_t thread = 0; thread < nl; ++thread) {
        if (thread%2) {
          /* The odd numbered ones are joinable and use an worker_thread that is
           * managed by this main thread, here. */
          size_t thread2 = thread/2;
          worker_thread_create_joinable(
            worker_thread_init(&arg[thread2],
                               &init_barr,
                               thread,
                               phases,
                               /* give it the starting
                                * position of the location on
                                * the left. */
                               &location[thread - orwl_readers],
                               orwl_server_get()->info,orwl_readers),
            &id[thread2]
          );
        } else {
          /* The even numbered ones are created detached and receive a
           * freshly created worker_thread for which they are responsible. */
          worker_thread_create_detached(
            P99_NEW(worker_thread,
                    &init_barr,
                    thread,
                    phases,
                    /* give it the starting
                     * position of the location on
                     * the left. */
                    &location[thread - orwl_readers],
                    orwl_server_get()->info,orwl_readers)
          );
        }
      }
      /* wait for even numbered threads, that have been started joinable. */
      ORWL_TIMER(join)
      for (size_t thread2 = 0; thread2 < nl/2; ++thread2) {
        worker_thread_join(id[thread2]);
      }

P99_PROTECT:
      /* wait for the remaining threads, if any are still alive */
      ORWL_TIMER(wait_detached)
      orwl_thrd_wait_detached();

      /* now we can safely destruct ourselves */
      orwl_stop();
    }

P99_PROTECT:
    worker_thread_vdelete(arg);
    thrd_t_vdelete(id);
    orwl_mirror_vdelete(location_back);
  }

  return 0;
}
