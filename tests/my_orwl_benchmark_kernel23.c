/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the authors and copyright holders for this work are as follows:            */
/* all rights reserved,  2011 Emmanuel Jeanvoine, INRIA, France               */
/* all rights reserved,  2011-2012 Jens Gustedt, INRIA, France                */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#include "orwl.h"
#include "orwl_qresource.h"
#include "p99_c99_default.h"
#include "p99_str.h"
#include "orwl_instrument.h"

/* The correpondence of the local layout of the 9 tasks in a one
   dimensional vector of mirrors. The main task in the center does all
   the computation. It is located first in the vector. */

/* 6 1 5 */
/* 3 0 4 */
/* 8 2 7 */

ORWL_LOCATIONS_PER_TASK(
  MAINPLUS, /*007*/// Un buffer qui contient toute la matrice Main plus les updates neighbouring locations
  NORTHSTRAIGHT,
  SOUTHSTRAIGHT,
  STRAIGHTWEST,
  STRAIGHTEAST,
  NORTHSTRAIGHTPLUS,
  SOUTHSTRAIGHTPLUS,
  STRAIGHTWESTPLUS,
  STRAIGHTEASTPLUS,
  //007 NORTHEAST,
  //007 NORTHWEST,
  //007 SOUTHEAST,
  //007 SOUTHWEST,
);

ORWL_LOCATIONS_PER_TASK_INSTANTIATION();
//P99_DEFINE_ENUM(orwl_locations);

ORWL_DECLARE_ENV(ORWL_L2); /*ORWL_DECLARE_ENV(NAME) Declare and document an environment variable that is to be use by your program.*/ 
ORWL_DEFINE_ENV(ORWL_L2);

//007 No need for task1_is_edge anymore because we do not need to update the corners anymore
/*static
  bool task1_is_edge(orwl_locations task1) { return task1 <= STRAIGHTEAST; } // define if the location of the task is an edge or a corner //007*/

static
bool task1_is_corner_plus(orwl_locations task1) { return task1 >= NORTHEASTPLUS; } //007

static
size_t relative_task(size_t row, size_t col, orwl_locations rel, size_t global_cols) {
  return ORWL_LOCATION(row*global_cols + col, rel); /*Given task ID TASK and local location ID LOCAL, compute a global ID for a location. */
}

/* Deal with the combinatorics of block boundaries by means of bit
   manipulation.

   The names of the different border combinations use a Plus for +,
   Minus for - and All for +/-.

   For the 2D blocks see the first "digit" for vertical and the second
   for horizontal. E.g borderPP translates to NORTH EAST, borderM0
   translates to SOUTH STRAIGHT, border0P is STRAIGHT EAST. */

enum borderBits {
  /* one dimensional vectors */
  border0 = 0x0,
  borderP = 0x1,
  borderM = 0x2,
  borderA = borderP | borderM,

  /* two dimensional matrices */

  /*      PA P0 PP      */
  /*     ---------      */
  /* PM | PM P0 PP | 00 */
  /* 0M | 0M 00 0P | 0P */
  /* 0A | MM M0 MP | MP */
  /*      ---------     */
  /*      MM M0 MA      */

  border00 = 0x0,
  border0P = 0x1,
  border0M = 0x2,
  border0A = border0P | border0M,

  borderP0 = 0x4,
  borderPP = borderP0 | border0P,
  borderPM = borderP0 | border0M,
  borderPA = borderP0 | border0A,

  borderM0 = 0x8,
  borderMP = borderM0 | border0P,
  borderMM = borderM0 | border0M,
  borderMA = borderM0 | border0A,

  borderA0 = borderM0 | borderP0,
  borderAP = borderA0 | border0P,
  borderAM = borderA0 | border0M,
  borderAA = borderA0 | border0A,

  /* three dimensional matrices */

  border000 = 0x0,
  border00P = 0x1,
  border00M = 0x2,
  border00A = border00P | border00M,

  border0P0 = 0x4,
  border0PP = border0P0 | border00P,
  border0PM = border0P0 | border00M,
  border0PA = border0P0 | border00A,

  border0M0 = 0x8,
  border0MP = border0M0 | border00P,
  border0MM = border0M0 | border00M,
  border0MA = border0M0 | border00A,

  border0A0 = border0M0 | border0P0,
  border0AP = border0A0 | border00P,
  border0AM = border0A0 | border00M,
  border0AA = border0A0 | border00A,

  borderP00 = 0x10,
  borderP0P = borderP00 | border00P,
  borderP0M = borderP00 | border00M,
  borderP0A = borderP00 | border00A,

  borderPP0 = borderP00 | border0P0,
  borderPPP = borderP00 | border0P0 | border00P,
  borderPPM = borderP00 | border0P0 | border00M,
  borderPPA = borderP00 | border0P0 | border00A,

  borderPM0 = borderP00 | border0M0,
  borderPMP = borderP00 | border0M0 | border00P,
  borderPMM = borderP00 | border0M0 | border00M,
  borderPMA = borderP00 | border0M0 | border00A,

  borderPA0 = borderP00 | border0M0 | border0P0,
  borderPAP = borderP00 | border0A0 | border00P,
  borderPAM = borderP00 | border0A0 | border00M,
  borderPAA = borderP00 | border0A0 | border00A,

  borderM00 = 0x20,
  borderM0P = borderM00 | border00P,
  borderM0M = borderM00 | border00M,
  borderM0A = borderM00 | border00A,

  borderMP0 = borderM00 | border0P0,
  borderMPP = borderM00 | border0P0 | border00P,
  borderMPM = borderM00 | border0P0 | border00M,
  borderMPA = borderM00 | border0P0 | border00A,

  borderMM0 = borderM00 | border0M0,
  borderMMP = borderM00 | border0M0 | border00P,
  borderMMM = borderM00 | border0M0 | border00M,
  borderMMA = borderM00 | border0M0 | border00A,

  borderMA0 = borderM00 | border0M0 | border0P0,
  borderMAP = borderM00 | border0A0 | border00P,
  borderMAM = borderM00 | border0A0 | border00M,
  borderMAA = borderM00 | border0A0 | border00A,

  borderA00 = borderP00 | borderM00,
  borderA0P = borderA00 | border00P,
  borderA0M = borderA00 | border00M,
  borderA0A = borderA00 | border00A,

  borderAP0 = borderA00 | border0P0,
  borderAPP = borderA00 | border0P0 | border00P,
  borderAPM = borderA00 | border0P0 | border00M,
  borderAPA = borderA00 | border0P0 | border00A,

  borderAM0 = borderA00 | border0M0,
  borderAMP = borderA00 | border0M0 | border00P,
  borderAMM = borderA00 | border0M0 | border00M,
  borderAMA = borderA00 | border0M0 | border00A,

  borderAA0 = borderA00 | border0M0 | border0P0,
  borderAAP = borderA00 | border0A0 | border00P,
  borderAAM = borderA00 | border0A0 | border00M,
  borderAAA = borderA00 | border0A0 | border00A,

};

typedef enum borderBits borderBits;

static
borderBits const task1_border[] = {
  [NORTHSTRAIGHT] = borderP0,
  [SOUTHSTRAIGHT] = borderM0,
  [STRAIGHTWEST] = border0M,
  [STRAIGHTEAST] = border0P,
  [NORTHEAST] = borderPP,
  [NORTHWEST] = borderPM,
  [SOUTHEAST] = borderMP,
  [SOUTHWEST] = borderMM,
};

static
bool is_border(borderBits borders, borderBits bord) {
  return (borders & bord) == bord;
}

/* The correpondence of the 12 neighboring tasks in a one dimensional
   vector of mirrors. So in total this makes up 21 mirrors per main
   task. */

/*     6 0 4     */
/*   ---------   */
/* 7 | 6 1 5 | 5 */
/* 2 | 3 0 4 | 3 */
/* b | 8 2 7 | 9 */
/*   ---------   */
/*     a 1 8     */

P99_DECLARE_ENUM(
  task_neighb,
  NORTHNORTH,
  SOUTHSOUTH,
  WESTWEST,
  EASTEAST
  //007NORTHEASTNORTH,
  //007NORTHEASTEAST,
  //007NORTHWESTNORTH,
  //007NORTHWESTWEST,
  //007SOUTHEASTSOUTH,
  //007SOUTHEASTEAST,
  //007SOUTHWESTSOUTH,
  //007SOUTHWESTWEST
);

P99_DEFINE_ENUM(task_neighb);

#define TASK_NEIGHB_AMOUNT 12

static_assert(task_neighb_amount == TASK_NEIGHB_AMOUNT, "task_neighb changed?");

P99_DECLARE_STRUCT(task_thread);
P99_DEFINE_STRUCT(task_thread,
                  size_t n,
                  size_t iterations,
                  size_t global_rows
                 );

task_thread* task_thread_init(task_thread* task) {
  if (task) {
    *task = (task_thread) {
      .n = 0,
    };
  }
  return task;
}

void task_thread_destroy(task_thread *task) {
  /* empty */
}

DECLARE_NEW_DELETE(task_thread);
DEFINE_NEW_DELETE(task_thread);
DECLARE_THREAD(task_thread);

static
void update_edge(orwl_locations task,
                 float frontier[],
                 size_t const n,
                 float const A[][n+2]) {//007
  size_t const np2m1 = n + 1;
  size_t const np2m2 = n ;
  assert(frontier);

  switch (task) {
  case NORTHSTRAIGHT: //007 les locations hethom lezem nécessairement yetbadlou b esemi o5ra 
    //007 memcpy(frontier, P99_RVAL(float const*, &A[0][1]), sizeof(float[n2]));
     memcpy(frontier, P99_RVAL(float const*, &A[1][1]), sizeof(float[np2m2]));
     //memcpy(&A[1][1], frontier, sizeof(float[np2m2])); //007 ghalta à 100% danger
    break;
  case SOUTHSTRAIGHT:
    //007 memcpy(frontier, P99_RVAL(float const*, &A[n1][1]), sizeof(float[n2]));
    memcpy(frontier, P99_RVAL(float const*, &A[np2m1][1]), sizeof(float[np2m2]));
    //memcpy(&A[np2m1][1], frontier, sizeof(float[np2m2]));//007 ghalta à 100% danger
    break;
  case STRAIGHTWEST:
    for (size_t i = 1 ; i < np2m1 ; i++)
      frontier[i - 1] = A[i][1];
    break;
  case STRAIGHTEAST:
    for (size_t i = 1 ; i < np2m1 ; i++)
      frontier[i - 1] = A[i][np2m2];
    break;
  default:
    report(1, "we have a problem here ...");
    assert(0);
  }
}

static
void update_edge_in_matrix(orwl_locations task,
                 float frontier[],
                 size_t const n,
                 float const A[][n+2]) {//007 danger change the names of the locations.
  size_t const np2m1 = n + 1;
  size_t const np2m2 = n ;
  assert(frontier);

  switch (task) {
  case NORTHSTRAIGHT:
    //007 memcpy(frontier, P99_RVAL(float const*, &A[0][1]), sizeof(float[n2]));
    //007 memcpy(frontier, P99_RVAL(float const*, &A[0][1]), sizeof(float[np2m2]));
    for (size_t i = 1 ; i < np2m1 ; i++)
      A[0][i] = frontier[i - 1] ;
    //007memcpy(&A[0][1], P99_RVAL(float const*, frontier), sizeof(float[np2m2]));
    break;
  case SOUTHSTRAIGHT:
    //007 memcpy(frontier, P99_RVAL(float const*, &A[n1][1]), sizeof(float[n2]));
    //007memcpy(&A[np2m1][1], P99_RVAL(float const*, frontier), sizeof(float[np2m2]));
    for (size_t i = 1 ; i < np2m1 ; i++)
      A[np2m1][i] = frontier[i - 1] ;
    break;
  case STRAIGHTWEST:
    for (size_t i = 1 ; i < np2m1 ; i++)
      // 007 frontier[i - 1] = A[i][1];
      A[i][0] = frontier[i - 1] ;
    break;
  case STRAIGHTEAST:
    for (size_t i = 1 ; i < np2m1 ; i++)
      //007 frontier[i - 1] = A[i][np2m2];
      A[i][np2m1] = frontier[i - 1];
    break;
  default:
    report(1, "we have a problem here ...");
    assert(0);
  }
}

/* 007 static
void update_corner(orwl_locations task,
                   float frontier[static 1],
                   size_t const n,
                   float const A[][n]) {
  size_t const n1 = n - 1;
  assert(frontier);

  switch (task) {
  case NORTHWEST:
    frontier[0] = A[0][0];
    break;
  case NORTHEAST:
    frontier[0] = A[0][n1];
    break;
  case SOUTHWEST:
    frontier[0] = A[n1][0];
    break;
  case SOUTHEAST:
    frontier[0] = A[n1][n1];
    break;
  default:
    report(1, "we have a problem here ...");
    assert(0);
  }
  } */

static
void* allocate_init(size_t size, p99_seed *seed) {
  float * ret = P99_MALLOC(float[size]);
  for (size_t i = 0; i < size; i++) {
    ret[i] = 10*p99_drand(seed);
  }
  return ret;
}

#define magic (float const){ 0.175F, }

p99_inline
float compute_inner(
  float old_za_ip1_j, float zr,
  float old_za_im1_j, float zb,
  float old_za_i_jp1, float zu,
  float old_za_i_jm1, float zv,
  float zz,
  float old_za_i_j) {
  float const qa
  = old_za_ip1_j * zr
    + old_za_im1_j * zb
    + old_za_i_jp1 * zu
    + old_za_i_jm1 * zv
    +                zz;
  return magic * (qa - old_za_i_j);
}

static
unsigned classify(size_t row, size_t global_rows,
                  size_t col, size_t global_cols) {
  /* An edge is internal if the neighboring matrix exists */
  return
    ((row > 0) ? borderP0 : border00)
    | ((row < (global_rows - 1)) ? borderM0 : border00)
    | ((col > 0) ? border0M :  border00)
    | ((col < (global_cols - 1)) ? border0P : border00);
}

static
void compute_task(task_thread* Arg,
                  orwl_server* srv,
                  size_t myloc) {
  ORWL_TIMER() {
    ORWL_THREAD_USE(task_thread,
                    n,
                    iterations,
                    global_rows
                   );
    /* Some data declarations */
    /* Data initialization */
    p99_seed *const seed = p99_seed_get();
    /* Za, Zb, Zu, Zr, Zv, and Zz initialization */
    size_t const nn = n * n;
    size_t const n1 = n - 1;
    size_t const n2 = n - 2;
    size_t const np2np2 = (n+2)*(n+2);
    float (* matrix[2])[np2np2]
    = {
      // This will be the first that the main task will be writing to,
      // no need to initialize.
      allocate_init (np2np2, seed),
      // Everybody will be starting by reading this.
      allocate_init(np2np2, seed),
    };
    
    float const(*const  zb)[n+2] = allocate_init(np2np2, seed); 
    float const(*const  zu)[n+2] = allocate_init(np2np2, seed); 
    float const(*const  zr)[n+2] = allocate_init(np2np2, seed); 
    float const(*const  zv)[n+2] = allocate_init(np2np2, seed); 
    float const(*const  zz)[n+2] = allocate_init(np2np2, seed); 

    orwl_handle2 hereplus_Write = ORWL_HANDLE2_INITIALIZER; // a read includes a write
    orwl_handle2 hereplus_Read = ORWL_HANDLE2_INITIALIZER;
    /* Determine the different positions where we find our neighbors. */
    size_t mtask = ORWL_TASK(myloc);

    /* This will be used to dispatch the neighbors to the correct vector
       position */
    size_t const global_cols = ORWL_TASK(orwl_nl) / global_rows;
    size_t const row = mtask / global_cols;
    size_t const col = mtask % global_cols;
    borderBits const borders = classify(row, global_rows, col, global_cols);

    bool const has_neighbor[task_neighb_amount] = {
      [NORTHNORTH] = is_border(borders, borderP0),
      [SOUTHSOUTH] = is_border(borders, borderM0),
      [WESTWEST] =  is_border(borders, border0M),
      [EASTEAST] =  is_border(borders, border0P),
      [NORTHEASTNORTH] = is_border(borders, borderPP),
      [NORTHEASTEAST] = is_border(borders, borderPP),
      [NORTHWESTNORTH] = is_border(borders, borderPM),
      [NORTHWESTWEST] = is_border(borders, borderPM),
      [SOUTHEASTSOUTH] = is_border(borders, borderMP),
      [SOUTHEASTEAST] = is_border(borders, borderMP),
      [SOUTHWESTSOUTH] = is_border(borders, borderMM),
      [SOUTHWESTWEST] = is_border(borders, borderMM),
    };

    ORWL_TIMER(init) {

      ORWL_TIMER(barrier1)
      orwl_global_barrier_wait(myloc, 1, srv);

      ORWL_TIMER(insert) {
        /***************************************************************************/
        /*                         Lock initialization step                        */
        /***************************************************************************/
        /* Send the lock requests */
        /* Take the local lock in write mode */
        orwl_write_insert(&hereplus_Write, myloc, 0, seed);
	/*Rather than taking the distant locks in read mode, we will take into read the whole big matrix(n+2)*(n+2)which will be saved in a big buffer 007*/	
        orwl_read_insert(&hereplus_Read, myloc, 0, seed); 
        /* Take the distant locks in read mode*/
      }
      ORWL_TIMER(schedule)
      orwl_schedule(myloc, 1, srv);
    }

    /* Fire ! */

    /***************************************************************************/
    /*                       Initialization iteration                          */
    /***************************************************************************/

    PROGRESS(1, 0, "Main task %s: intialization iteration", ORWL_TASK(myloc));

    ORWL_TIMER(first)
    //007 Do I need a read access on myloc ?? on the MAINPLUS location??
    ORWL_SECTION(&hereplus_Read, 1, seed){
      ORWL_SECTION(&hereplus_Write, 1, seed) {
	orwl_truncate(&hereplus_Write, sizeof(void*));
	float (** matrixP)[n+2] = orwl_write_map(&hereplus_Write);
	/* link the global matrix pointers to our current version */
	for (size_t i = 1 ; i < n ; i++) {
	  for (size_t j = 1 ; j < n; j++) {
            matrixP[i][j] = matrix[true][i][j]; //007 à verifier 
          }
        }
      }

    ORWL_TIMER(iterations, iterations) {
      /***************************************************************************/
      /*                         Computation iterations                          */
      /***************************************************************************/

      for (size_t iter = 0 ; iter < iterations ; iter++)
        ORWL_TIMER(iteration) {
        PROGRESS(1, iter, "Main task %s: iteration %s                              ", mtask, iter);
        bool parity = iter % 2;
	float (*const za)[n+2] = matrix[parity];
        float (*const old_za)[n+2] = matrix[!parity];
        /* Inner computation. We can do this without holding the lock:
           first we work on a copy of the matrix, and then this
           computation here only affects the inner part of it and not
           the boundary vectors. */
        ORWL_QSECTION(L2)
        ORWL_TIMER(comp)

        for (size_t i = 1 ; i < n ; i++) {
	  for (size_t j = 1 ; j < n; j++) {
            za[i][j] = compute_inner(
                         old_za[i + 1][j], zr[i][j],
                         old_za[i - 1][j], zb[i][j],
                         old_za[i][j + 1], zu[i][j],
                         old_za[i][j - 1], zv[i][j],
                         zz[i][j],
                         old_za[i][j]);
          }
        }

        ORWL_TIMER(boundary)
        ORWL_SECTION(&here, 1, seed)
        ORWL_TIMER(subtotal) {
          float (** matrixP)[n+2] = orwl_write_map(&hereplus_Write);
	  /* Now have it point to the actual matrix such that the
             communication tasks will be able to copy the boundary data
             from it. */
          /* link the global matrix pointers to our current version */
	  *matrixP = za;
        }
      }
      free((void*)zb);
      free((void*)zu);
      free((void*)zr);
      free((void*)zv);
      free((void*)zz);

      /* Cancel pending requests */
      orwl_disconnect(&hereplus_Write, 1, seed);
      orwl_disconnect(&hereplus_Read, 1, seed);
      /* Now that we disconnected we can free the main matrices, too. */
      free(matrix[false]);
      free(matrix[true]);
    }
  }
}

static
void update_global_task(task_thread* Arg, orwl_server* srv, size_t myloc, orwl_locations task1) {
  ORWL_TIMER() {
    ORWL_THREAD_USE(task_thread,
                    n,
                    iterations,
                    global_rows
                   );
    /* Data initialization */
    p99_seed *const seed = p99_seed_get();
    typedef float const line[n+2];
    //007 size_t const fsize = task1_is_edge(task1) ? n : 1;
    size_t const fsize =  n;
    typedef float fline[fsize];

    /* All remote servers can be considered to be up at that point */
    orwl_global_barrier_wait(myloc, 1, srv);

    /* Determine the different positions where we find our neighbors. */
    size_t mtask = ORWL_TASK(myloc);

    /* This will be used to dispatch the neighbors to the correct vector
       position */
    size_t global_cols = ORWL_TASK(orwl_nl) / global_rows;
    size_t row = mtask / global_cols;
    size_t col = mtask % global_cols;
    borderBits borders = classify(row, global_rows, col, global_cols);

    if (!is_border(borders, task1_border[task1])) {
      /* The data of this update task wouldn't be used by anybody. So
         just play well with the others but do nothing else. */
      orwl_schedule(myloc, 1, srv);
    } else {
      orwl_handle2 there_buffer_Read = ORWL_HANDLE2_INITIALIZER;
      orwl_handle2 here_mainplus_Write = ORWL_HANDLE2_INITIALIZER;

      /***************************************************************************/
      /*                         Lock initialization step                        */
      /***************************************************************************/
      /* Take a lock on the main task in write mode */
      //orwl_read_insert(&there, ORWL_LOCATION(ORWL_TASK(myloc), MAIN), 1, seed);

      /* Take a lock in read mode on the neighbouring location */
	  //007
	  orwl_write_insert(&here_mainplus_Write, ORWL_LOCATION (ORWL_TASK(myloc), MAINPLUS) , 2, seed);  //007 danger? name of location false // The legend says that the update global should be last in priority 
	  
	  switch (task1) {
	  case NORTHSTRAIGHTPLUS:
	    orwl_read_insert(&there_buffer_Read, relative_task(row-1, col, SOUTHSTRAIGHT, global_cols), 2, seed); 
	    break;
	  case SOUTHSTRAIGHTPLUS:
	    orwl_read_insert(&there_buffer_Read, relative_task(row+1, col, NORTHSTRAIGHT, global_cols), 2, seed); 
	    break;
	  case STRAIGHTWESTPLUS:
	    orwl_read_insert(&there_buffer_Read, relative_task(row, col-1, STRAIGHTEAST, global_cols), 2, seed); 
	    break;
	  case STRAIGHTEASTPLUS:
	    orwl_read_insert(&there_buffer_Read, relative_task(row, col+1, STRAIGHTWEST, global_cols), 2, seed); 
	    break;
	  default:
	    report(1, "we have a problem here ...");
	    assert(0);
	  }

	  orwl_schedule(myloc, 1, srv);

      /* Fire ! */

      /***************************************************************************/
      /*                       Initialization iteration                          */
      /***************************************************************************/
      ORWL_TIMER(first)
      ORWL_SECTION(&here_mainplus_Write, 1, seed) {
        /* Frontier resizing */
        orwl_truncate(&here_mainplus_Write, sizeof(line)); //007 danger yomken famma 7aja ghalta

        ORWL_SECTION(&there_buffer_Read, 1, seed) {
          fline *const* neighb_frontier = orwl_read_map(&there_buffer_Read);
          line *mtrx = orwl_write_map(&here_mainplus_Write);
          /* First input in frontier */
	  // 007  if (task1_is_edge(task1)) {
            update_edge_in_matrix(task1, *mtrx, n, *neighb_buffer);
	    // 007 } else {
	    //007  update_corner(task1, *frontier, n, *matrixP);
	    // 007}
        }
      }

      /***************************************************************************/
      /*                         Computation iterations                          */
      /***************************************************************************/
      ORWL_TIMER(iterations, iterations)
      for (size_t iter = 0 ; iter < iterations ; iter++)
        ORWL_TIMER(iteration) {
        /* update my buffer */
        ORWL_SECTION(&here_mainplus_Write, 1, seed)
        ORWL_TIMER(section)
        ORWL_SECTION(&there_buffer_Read, 1, seed)
        ORWL_TIMER(section) {
          fline *const* neighb_frontier = orwl_read_map(&there_buffer_Read);
          line *mtrx = orwl_write_map(&here_mainplus_Write);
	  // if (task1_is_edge(task1)) {
            ORWL_TIMER(edge)
            update_edge_in_matrix(task1, *mtrx, n, *neighb_buffer);
	    //} else {
            //ORWL_TIMER(corner)
            //update_corner(task1, *frontier, n, *matrixP);
	    //}
        }
      }

      /* Cancel pending requests */
      orwl_disconnect(&there, 1, seed);
      orwl_disconnect(&here, 1, seed);
    }
  }
}

static
void update_local_task(task_thread* Arg, orwl_server* srv, size_t myloc, orwl_locations task1) {
  ORWL_TIMER() {
    ORWL_THREAD_USE(task_thread,
                    n,
                    iterations,
                    global_rows
                   );
    /* Data initialization */
    p99_seed *const seed = p99_seed_get();
    typedef float const line[n+2]; //007 type d'un buffer pr contenir l'adresse et la taille de la grande matrice à lire MAINPLUS matrixP
    //007  size_t const fsize = task1_is_edge(task1) ? n : 1; //007
    size_t const fsize = n;
    typedef float fline[fsize]; //007 type d'un buffer pour contenir l'adresse et la taille de l'edge à updater (frontier) 

    /* All remote servers can be considered to be up at that point */
    orwl_global_barrier_wait(myloc, 1, srv);

    /* Determine the different positions where we find our neighbors. */
    size_t mtask = ORWL_TASK(myloc);

    /* This will be used to dispatch the neighbors to the correct vector
       position */
    size_t global_cols = ORWL_TASK(orwl_nl) / global_rows;
    size_t row = mtask / global_cols;
    size_t col = mtask % global_cols;
    borderBits borders = classify(row, global_rows, col, global_cols);

    if (!is_border(borders, task1_border[task1])) {
      /* The data of this update task wouldn't be used by anybody. So
         just play well with the others but do nothing else. */
      orwl_schedule(myloc, 1, srv);
    } else {
      orwl_handle2 here_buffer_Write = ORWL_HANDLE2_INITIALIZER;
      orwl_handle2 here_mainplus_Read = ORWL_HANDLE2_INITIALIZER;

      /***************************************************************************/
      /*                         Lock initialization step                        */
      /***************************************************************************/
      /* Take the local lock in write mode */
      orwl_write_insert(&here_buffer_Write, , 0, seed); // 007 The legend says that the local update should have priority over other operations.// danger? no name of the location.
      /* Take a lock on the main task in read mode */
      orwl_read_insert(&here_mainplus_Read, ORWL_LOCATION(ORWL_TASK(myloc), MAINPLUS), 0, seed); //007  MAIN 

      orwl_schedule(myloc, 1, srv);

      /* Fire ! */

      /***************************************************************************/
      /*                       Initialization iteration                          */
      /***************************************************************************/
      ORWL_TIMER(first)
      ORWL_SECTION(&here_buffer_Write, 1, seed) {
        /* Frontier resizing */
        orwl_truncate(&here_buffer_Write, sizeof(fline));

        ORWL_SECTION(&here_mainplus_Read, 1, seed) {
          line *const *matrixP = orwl_read_map(&here_mainplus_Read);
          fline *frontier = orwl_write_map(&here_buffer_Write);
          /* First input in frontier */ c
          //007if (task1_is_edge(task1)) {
            update_edge(task1, *frontier, n, *matrixP); //007 danger : implemenation of update edge not correct
	    //007} else {
            //007update_corner(task1, *frontier, n, *matrixP);
	    //007}
        }
      }

      /***************************************************************************/
      /*                         Computation iterations                          */
      /***************************************************************************/
      ORWL_TIMER(iterations, iterations)
      for (size_t iter = 0 ; iter < iterations ; iter++)
        ORWL_TIMER(iteration) {
        /* update my buffer */
        ORWL_SECTION(&here_buffer_Write, 1, seed)
        ORWL_TIMER(section)
        ORWL_SECTION(&here_mainplus_Read, 1, seed)
        ORWL_TIMER(section) {
          line *const* matrixP = orwl_read_map(&here_mainplus_Read);
          fline *frontier = orwl_write_map(&here_buffer_Write);
          //007if (task1_is_edge(task1)) {
            ORWL_TIMER(edge)
            update_edge(task1, *frontier, n, *matrixP);
	    //007} else {
            //007 ORWL_TIMER(corner)
            //007 update_corner(task1, *frontier, n, *matrixP);
	    //007}
        }
      }

      /* Cancel pending requests */
      orwl_disconnect(&here_buffer_Write, 1, seed);
      orwl_disconnect(&hereplus_Read, 1, seed);
    }
  }
}

DEFINE_THREAD(task_thread) {
  size_t myloc = orwl_myloc;
  orwl_server *const srv = orwl_server_get();
  orwl_locations task1 = ORWL_LOCAL(myloc);
  if (task1 == MAINPLUS) {
    compute_task(Arg, srv, myloc);
  } else { 
    if (task1 <= STRAIGHTEAST) { 
      update_local_task (Arg, srv, myloc, task1);
    } else  {
      update_global_task (Arg, srv, myloc, task1);
    }
  }
}

int main(int argc, char **argv) {
  if (argc < 4) {
    REPORT(1, "Usage: %s n it rows", argv[0]);
    REPORT(1, "    n    size of each block (n x n)");
    REPORT(1, "    it   number of iterations");
    REPORT(1, "    rows number of row-blocks per block column");
    return 1;
  }
  ORWL_TIMER() {
    size_t const n = strtouz(argv[1]);
    size_t const iterations = strtouz(argv[2]);
    size_t const global_rows = strtouz(argv[3]);

    /* local server initialization and address exchange */
    ORWL_TIMER(init)
    orwl_init();
    size_t l2max = SIZE_MAX;
    if (ORWL_L2()) {
      l2max = strtoull(ORWL_L2());
    }
    ORWL_QRESOURCE_INIT(L2, l2max);

    /* other than in most of the tutorials this code launches one thread
       per location, so every task is realized by 9 different
       threads. */
    TRACE(1, "we handle %s locations", orwl_ll);
    for (size_t i = 0; i < orwl_ll; i++)
      ORWL_TIMER(loop_body) {
      task_thread* task = P99_NEW(task_thread);
      *task = (task_thread) {
        .n = n,
         .iterations = iterations,
          .global_rows = global_rows,
      };
      task_thread_operation(task, orwl_locids[i]);
    }
  }
  return EXIT_SUCCESS;
}
