/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the author and copyright holder for this work is                           */
/* all rights reserved,  2010-2013 Jens Gustedt, INRIA, France                */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#include "orwl.h"

P99_DECLARE_ENUM(command,
                 finish,
                 block,
                 unblock
                );

P99_DEFINE_ENUM(command);

int main(int argc, char **argv) {
  /* This init here is necessary since we don't even start a server thread */
  orwl_types_init();
  orwl_address_book *ab = 0;
  {
    FILE * f = fopen(argv[1], "r");
    orwl_address_book_read(&ab, f);
    fclose(f);
  }
  if (argc > 1) {
    int argp = (argc == 2) ? 0 : 2;
    errno = 0;
    char mess[245];
    while ((!argp && fgets(mess, 32, stdin))
           || (argp && argp < argc && strcpy(mess, argv[argp++]))) {
      size_t len = strlen(mess);
      if (mess[len - 1] == '\n') {
        mess[len - 1] = 0;
        --len;
      }
      uint64_t ret = 0;
      command c = command_parse(mess);
      switch (c) {
      case finish:
        ret = orwl_rpc(0, &ab->eps[0], p99_seed_get(), orwl_proc_terminate);
        break;
      case block:
        ret = orwl_rpc(0, &ab->eps[0], p99_seed_get(), orwl_proc_block);
        break;
      case unblock:
        ret = orwl_rpc(0, &ab->eps[0], p99_seed_get(), orwl_proc_unblock);
        break;
      default:
        report(1, "unknown or ambiguous command %s", mess);
        continue;
      }
      REPORT(1, "%s server %s %s",
             command_getname(c), orwl_endpoint_print(&ab->eps[0]), ret);
      if (errno) {
        perror(mess);
        errno = 0;
      }
      if (c == finish)
        return ret ? EXIT_FAILURE : EXIT_SUCCESS;
    }
  } else {
    report(1, "Usage: %s address_book", argv[0]);
    return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}
