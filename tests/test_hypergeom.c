#include "orwl_hypergeom.h"

int main(int argc, char* argv[static restrict argc+1]){
  size_t bucket = 10;
  size_t draws = 4;
  switch (argc) {
  default: draws = strtoull(argv[2], 0, 0);
  case 2: bucket = strtoull(argv[1], 0, 0);
  case 1:;
  case 0:;
  }
  size_t draw[draws];
  for (size_t i = 0; i < draws; ++i) {
    draw[i] = bucket;
  }
  p99_seed*const seed = p99_seed_get();
  orwl_varRandHypergeom(seed, draws*bucket, (draws*bucket)/2, draws, draw);
  size_t sum = 0;
  for (size_t i = 0; i < draws; ++i) {
    printf("%zu,\t", draw[i]);
    sum += draw[i];
  }
  printf("sum = %zu\n", sum);
  return EXIT_SUCCESS;
}
