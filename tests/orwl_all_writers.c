/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the author and copyright holder for this work is                           */
/* all rights reserved,  2016 Daniel Salas, INSERM, France                    */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#include <stdio.h>
#include <stdint.h>

#include "orwl.h"
#include "p99_qsort.h"

/**
 ** @file
 **
 ** @brief A simple test where all task write to the same location
 **
 ** This currently only works when all tasks are in the same process.
 **/


ORWL_LOCATIONS_PER_TASK(count_loc);
ORWL_LOCATIONS_PER_TASK_INSTANTIATION();

P99_DECLARE_STRUCT(task_obj);
P99_DEFINE_STRUCT(  task_obj, float init_val);

task_obj* task_obj_init(task_obj* task, float init_val) {

  if (!task) return 0;

  *task = (task_obj) {   .init_val = init_val };
  return task;
}

void task_obj_destroy(task_obj *task) {
  /* empty */
}

P99_DECLARE_DELETE(task_obj);

P99_DEFINE_DELETE(task_obj);
ORWL_DECLARE_TASK(task_obj);


int main(int argc, char* argv[argc+1]) {
  orwl_init();

  float init_val = 2.3;

  trace(1, "orwl_lt=%zu, orwl_ll=%zu, orwl_ll modulo orwl_lt = %zu", orwl_lt, orwl_ll, orwl_ll%orwl_lt);

  for(size_t i=0; i<orwl_lt; i++) {
    task_obj* task = P99_NEW(task_obj, init_val);
    task_obj_create_task(task, orwl_tids[i]);
  }
  return EXIT_SUCCESS;
}

ORWL_DEFINE_TASK(task_obj) {

  trace(1,"orwl_mytid=%zu, count_loc=%d", orwl_mytid, count_loc);
  fflush(0);
  if(!orwl_mytid) {
    orwl_scale(sizeof(size_t), ORWL_LOCATION(orwl_mytid, count_loc));
  }

  trace(1,"orwl_ll= %zu", orwl_ll);
  orwl_handle count_hdl = ORWL_HANDLE_INITIALIZER;
  orwl_write_insert(&count_hdl, ORWL_LOCATION(0, count_loc), orwl_mytid);

  trace(1,"orwl_locations_amount=%d",orwl_locations_amount);

  orwl_schedule();
  trace(1,"before section");
  ORWL_SECTION(&count_hdl) {
    trace(1,"inside section");
    size_t* count_hdl_ptr = orwl_write_map(&count_hdl);
    trace(1, "count=%zu", *count_hdl_ptr);
    *count_hdl_ptr = orwl_mytid;
    trace(1, "count=%zu", *count_hdl_ptr);
  }

  trace(1,"after section");
  orwl_stop_task();
  trace(1,"after stop");
}
