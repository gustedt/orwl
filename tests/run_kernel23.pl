#!/bin/sh -f
#  This may look like complete nonsense, but it really is -*- mode: perl; coding: utf-8 -*-
eval 'exec perl -wS -x $0 ${1+"$@"}'
if 0;               #### for this magic, see findSvnAuthors ####
#!perl
#
#
# Except of parts copied from previous work and as explicitly stated below,
# the authors and copyright holders for this work are as follows:
# all rights reserved,  2012 Jens Gustedt, INRIA, France
#
# This file is part of the ORWL project. You received this file as as
# part of a confidential agreement and you may generally not
# redistribute it and/or modify it, unless under the terms as given in
# the file LICENSE.  It is distributed without any warranty; without
# even the implied warranty of merchantability or fitness for a
# particular purpose.

# Set up a simple cleanup handler to remove temporary files and a
# function to start tasks in the background.
sub cleanup(@);
sub background(@);
sub waitall(@);
sub chose($$);

use English;
use strict;

my @sig = qw(ABRT BUS FPE HUP ILL INT KILL PIPE POLL PWR QUIT SEGV
    STKFLT TERM TSTP TTIN TTOU URG USR1 USR2 VTALRM XCPU XFSZ );

foreach my $sig (@sig) {
    $SIG{$sig}  = \&cleanup;
}

END {
    cleanup();
}


## set up this individual run
my $run = int(rand(0xFFFFFFFF));
my ($col, $row, $step) = (@ARGV, 1);
my $nb = ${col} * ${row};
my $mmax = ${nb} - 1;

## environment variables needed for ORWL
$ENV{ORWL_SECRET} = int(rand(0xFFFFFFFF));
$ENV{ORWL_GLOBAL_AB} = "global_${run}.txt";

## Arrays needed for job control

my @pid;
my @locfiles;
my @garb = ($ENV{ORWL_GLOBAL_AB}, "$ENV{ORWL_GLOBAL_AB}_0");


##++$step if ($nb % $step);
my $num = int($nb / $step);
my $down = $nb;
if ($nb % $step) {
    $step = int($nb / ($num + 1));
    ++$step if ($nb % ($num + 1));
}
if ($nb % $step) {
    ++$num;
    my $rep = $step - 1 - ($nb % $step);
    $down = ($num - 1 - $rep) * $step;
}
## Launch the tasks.
for (my $i = 0; $i < $nb; $i += ${step}) {
    if ($i >= $down) {
        --$step;
        $down = $nb;
    }
    # group some main tasks into the same process
    my @mtask = ();
    for (my $j = $i; $j < $nb && $j < $i + $step; ++$j) {
        unshift(@mtask, $j);
    }
    $ENV{ORWL_LOCAL_AB} = "${run}_${i}.txt";
    $ENV{ORWL_TIDS} = "@mtask";
    push(@locfiles, $ENV{ORWL_LOCAL_AB});
    my @cmd = ("./orwl_benchmark_kernel23", "100", "10", "${col}");
    push(@pid, background(chose(0, $i), @cmd));
}

foreach my $file (@locfiles) {
    while (! -e $file) {
        # empty
    }
}

system("cat @locfiles > $ENV{ORWL_GLOBAL_AB}_0");
## Only move the file to the expected name, once it is completely written.
rename "$ENV{ORWL_GLOBAL_AB}_0", $ENV{ORWL_GLOBAL_AB};
unlink(@locfiles);

## Wait for all processes to finish
waitall(@pid);

############ End of processing, only function definitions beyond that
############ point.
exit 0;

sub chose($$) {
    my ($what, $i) = @_;
    my @what;
    if ($what) {
        if ($what == 1) {
            my $tmprun = "/tmp/${run}.cmd";
            system("echo run > $tmprun");
            push(@garb, $tmprun);
            @what = ("gdb", "-x", "$tmprun", "--args");
        } elsif ($what == 2) {
            @what = ("valgrind", "--log-file=kernel23-${i}.log", "--verbose", "--leak-check=full", "--show-reachable=yes", "--track-origins=yes");
        }
        unshift(@what, "xterm", "-l", "+wf", "-geometry", "120x25", "-hold", "-fn", '-*-courier-*-r-*', "-T", "Main task $i", "-e");
    }
    return @what;
}

sub cleanup(@) {
    my ($sig) = @_;
    if (defined($sig)) {
        print STDERR "caught SIG$sig\n";
    }
    unlink(@locfiles, @garb);
}

sub background (@) {
    my $job = int(rand(0xFFFFFFFF));
    if (defined($ENV{ORWL_ERRDIR})) {
        my $dir = sprintf("$ENV{ORWL_ERRDIR}/err-%08X", ${run});
        mkdir "$dir" || 0;
        my $err = sprintf("${dir}/err-%08X.txt", $job);
        $ENV{"ORWL_STDERR"} = $err;
    }
    my $pid = fork();
    exec @_ if (!$pid);
    return $pid;
}

sub waitall(@) {
    waitpid($_, 0) foreach (@_);
}
