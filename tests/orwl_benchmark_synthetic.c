/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the authors and copyright holders for this work are as follows:            */
/* all rights reserved,  2011 Emmanuel Jeanvoine, INRIA, France               */
/* all rights reserved,  2011-2013 Jens Gustedt, INRIA, France                */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#include "orwl.h"
#include "p99_c99_default.h"
#include "p99_str.h"
#include "orwl_instrument.h"

#define MEGA SIZE_C(0x100000)

ORWL_LOCATIONS_PER_TASK(MAIN);
ORWL_LOCATIONS_PER_TASK_INSTANTIATION();

P99_DECLARE_STRUCT(task);

/* The types of the parameters that each individual task receives.
   This must be in the same order as the two following lists of
   parameter names. */
#define TASK_TYPES_VARS                                        \
  size_t iterations,                                           \
    size_t inner_iterations,                                   \
    size_t shared_memory_size

/* The parameters that will be identical for all tasks. */
#define TASK_VARS                                              \
  iterations,                                                  \
    inner_iterations,                                          \
    shared_memory_size

P99_DEFINE_STRUCT(task, TASK_TYPES_VARS);

/* Declare the init function to receive exactly the same arguments as
   the fields of the type. */
task* task_init(task *arg, TASK_TYPES_VARS) {
  if (arg) {
    *arg = P99_STRUCT_LITERAL(task, TASK_VARS);
  }
  return arg;
}

void task_destroy(task *arg) {
  // empty
}

P99_DECLARE_DELETE(task);
P99_DEFINE_DELETE(task);

ORWL_DECLARE_TASK(task);

static
size_t
rel_index(size_t i, size_t mynum, size_t nt) {
  return (mynum + i) % nt;
}

/* Have theses as extern variables such that assignment to them can
   not be optimized out. */
uint64_t * my_data;
double d;

ORWL_DEFINE_TASK(task) {
  /* Lift the thread local variables on the stack. */
  ORWL_THREAD_USE(task, TASK_VARS);
  size_t mynum = orwl_mynum;
  size_t nt = orwl_nt;

  /* First calibrate the timers. This lets you estimate the overhead
     for such a timer operation. What you see on the innermost timer
     is just the cost of the scheduling noise. Then, each level of
     timer should add a constant amount of measured time. On my
     current machine this is about 300 ns if the thread has a core of
     its own. */
  ORWL_TIMER(timer_loop)
  for (size_t i = 0; i < 1000; ++i)
    ORWL_TIMER(0)
    ORWL_TIMER(1)
    ORWL_TIMER(2)
    ORWL_TIMER(3)
    ORWL_TIMER(4)
    ORWL_TIMER(5)
    ORWL_TIMER(6)
    ORWL_TIMER(7);


  orwl_handle2 * handle = orwl_handle2_vnew(nt);

  /* These (array of) pointers would be local to the place where we
     use them. The only reason to have them here on that level is that
     we want to bench this code. Therefore we must ensure that the
     compiler can't optimize the assignment away.*/
  extern uint64_t * my_data;

  /* Get a reference to the random generator state that is specific to
     this thread. Usually the performance cost of obtaining this
     pointer separately for each call should not be too expensive. But
     here we want to benchmark the low level functionality of the
     library so we try to avoid as much noise as possible. */
  p99_seed * seed = p99_seed_get();

  /* Scale the local resource to the wanted size */
  orwl_scale(shared_memory_size);


  /***************************************************************************/
  /*                              Connection step                            */
  /***************************************************************************/
  ORWL_TIMER(insert)
  for (size_t i = 0; i < nt; ++i)
    if (i == mynum)
      /* Take the local lock in write mode */
      orwl_write_insert(&handle[i], ORWL_LOCATION(i), 0, seed);
    else
      /* Take the distant locks in read mode*/
      orwl_read_insert(&handle[i], ORWL_LOCATION(i), 1, seed);

  ORWL_TIMER(schedule)
  orwl_schedule(mynum);

  /***************************************************************************/
  /*                       Initialization iteration                          */
  /***************************************************************************/
  ORWL_TIMER(init)
  ORWL_SECTION(&handle[mynum], 1, seed) {
    size_t len;
    uint64_t * my_data = orwl_write_map(&handle[mynum], &len, seed);
    len /= sizeof(uint64_t);
    /* Let's put garbage in the shared data */
    for (size_t i = 0; i < len; i++) {
      my_data[i] = p99_drand(seed);
    }
  }
  /* Take the distant locks to keep the correct number of phases */
  for (size_t i = 1 ; i < nt ; i++) {
    ORWL_SECTION(&handle[rel_index(i, mynum, nt)], 1, seed) {
      // empty
    }
  }

  /* Fire ! */
  /***************************************************************************/
  /*                         Computation iterations                          */
  /***************************************************************************/

  for (size_t iter = 0 ; iter < iterations ; iter++) {
    ORWL_TIMER(iteration) {
      report(1, "iteration %zu", iter);

      /* The following two blocks would normally each be put into a
         ORWL_SECTION, but we want to benchmark the individual
         parts. */
      ORWL_TIMER(computation) {
        ORWL_TIMER(acquire)
        orwl_acquire(&handle[mynum], , seed);

        ORWL_TIMER(map)
        my_data = orwl_write_map(&handle[mynum], , seed);

        ORWL_TIMER(inner) {
          /* CPU consuming computation */
          double a,b,c;
          extern double d;
          d = 0;
          for (size_t i = 0 ; i < 1000000 ; i++) {
            b = (i + 1) / (sin((i + 2) / (i + 1)) + 1.01);
            d = 0;
            for (size_t j = 0 ; j < inner_iterations ; j++) {
              c = (j + 3) / (cos((i + 4) / (i + 1)) + 1.02);
              a = (b * c) - (b / (i + (j * c)));
              d += a;
            }
          }
        }

        ORWL_TIMER(release)
        orwl_release_or_next(&handle[mynum], , seed);
      }

      ORWL_TIMER(communication) {
        ORWL_TIMER(acquire)
        for (size_t i = 1 ; i < nt ; i++)
          orwl_acquire(&handle[rel_index(i, mynum, nt)], , seed);

        ORWL_TIMER(map)
        for (size_t i = 1 ; i < nt ; i++)
          my_data = (uint64_t*)orwl_read_map(&handle[rel_index(i, mynum, nt)]);

        ORWL_TIMER(next)
        for (size_t i = 1 ; i < nt ; i++)
          orwl_release_or_next(&handle[rel_index(i, mynum, nt)], , seed);
      }
    }
  }
  orwl_disconnect(handle, nt, seed);
  orwl_handle2_vdelete(handle);
  orwl_stop_task();
}

int main(int argc, char **argv) {
  if (argc < 4) {
    REPORT(1, "only %s commandline arguments, this ain't enough", argc);
    return 1;
  }
  ORWL_TIMER() {
    size_t const iterations = strtouz(argv[1]);
    size_t const inner_iterations = strtouz(argv[2]);
    size_t const shared_memory_size = strtouz(argv[3]) * MEGA;

    REPORT(1, "doing %s (%s) iterations with %s byte per location", iterations, inner_iterations, shared_memory_size);
    ORWL_TIMER(init)
    orwl_init();

    for (size_t i = 0; i < orwl_lt; i++) {
      ORWL_TIMER(create_task)
      task_create_task(P99_NEW(task, iterations, inner_iterations, shared_memory_size), orwl_tids[i]);
    }
  }
  /* Wait that all threads have finished. This does not only concern
     the application threads that we launched above, but also
     threads that ORWL might have launched behind the scenes.

     The wait for the other threads happens after the return from
     main, if it is not done explicitly. */
  return EXIT_SUCCESS;
}
