
DIRS = ./include ./lib ./tests ./tutorials ./deep-instr-preload

ORWL_SOURCES = ./include/*.h ./lib/*.c ./lib/Makefile ./tests/orwl*.c ./tests/Makefile ./tutorials/*.c ./tutorials/Makefile
ORWL_DOXY =

ORWLFILES =  ${ORWL_SOURCES} ${ORWL_DOXY}

ORWLDISTRI = orwl.tgz orwl.zip orwl-html.tgz orwl-html.zip orwl-refman.pdf

CLEAN = ${patsubst %,%/clean,${DIRS}}
DISTCLEAN = ${patsubst %,%/distclean,${DIRS}}
TIDY = ${patsubst %,%/tidy,${DIRS}}

ifndef DATE
DATE := ${shell date --rfc-3339=date}
endif
ORWL_GFORGE = shell.gforge.inria.fr
ORWL_HTDOCS = /home/groups/orwl/htdocs



TAROPT := --dereference --owner=root --group=root

.PHONY : target clean ${DIRS} doxygen ${CLEAN} ${DISTCLEAN} ${TIDY}

all : ${DIRS}

clean : ${CLEAN}
	-$(RM) doxygen.log *~

distclean : ${DISTCLEAN}
	-$(RM) doxygen.log *~

tidy : ${TIDY}

tests : lib
	$(MAKE) -C $@

tutorials : lib
	$(MAKE) -C $@

lib : ./include
	$(MAKE) -C $@

deep-instr-preload:
	$(MAKE) -C $@

doxygen : documentation/target doxygen-orwl

documentation/target :
	$(MAKE) -C `dirname $@`

doxygen-orwl :
	doxygen

orwl-html-transfer : doxygen
	-rsync -az --no-g --no-p --chmod=ug+rw --progress -e 'ssh -ax' orwl-html/ ${ORWL_GFORGE}:${ORWL_HTDOCS}/orwl-html-new
	-ssh ${ORWL_GFORGE} mv ${ORWL_HTDOCS}/orwl-html ${ORWL_HTDOCS}/orwl-html-bak
	-ssh ${ORWL_GFORGE} chmod -R ug+rw ${ORWL_HTDOCS}/orwl-html-new
	-ssh ${ORWL_GFORGE} mv ${ORWL_HTDOCS}/orwl-html-new ${ORWL_HTDOCS}/orwl-html
	-ssh ${ORWL_GFORGE} rm -rf ${ORWL_HTDOCS}/orwl-html-bak
	-rsync -az --no-g --no-p --chmod=ug+rw --progress -e 'ssh -ax' documentation/ ${ORWL_GFORGE}:${ORWL_HTDOCS}/documentation-new
	-ssh ${ORWL_GFORGE} mv ${ORWL_HTDOCS}/documentation ${ORWL_HTDOCS}/documentation-bak
	-ssh ${ORWL_GFORGE} chmod -R ug+rw ${ORWL_HTDOCS}/documentation-new
	-ssh ${ORWL_GFORGE} mv ${ORWL_HTDOCS}/documentation-new ${ORWL_HTDOCS}/documentation
	-ssh ${ORWL_GFORGE} rm -rf ${ORWL_HTDOCS}/documentation-bak



.PHONY : ${ORWLDISTRI}

orwl-distribution : ${ORWLDISTRI}

orwl.tgz :
	git archive --format=tar HEAD ${ORWLFILES} | gzip -9 > $@

orwl.zip :
	git archive --format=zip -9 -o $@ HEAD ${ORWLFILES}

orwl-html.tgz orwl-html.zip orwl-refman.pdf : doxygen-orwl
	make -C orwl-latex refman.pdf
	cp orwl-latex/refman.pdf orwl-refman.pdf
	tar -czf orwl-html.tgz orwl-html
	zip -q -r orwl-html orwl-html/

./include :

./tests/clean :
	$(MAKE) -C tests clean

./lib/clean :
	$(MAKE) -C lib clean

./include/clean :
	$(MAKE) -C include clean

./tests/distclean :
	$(MAKE) -C tests distclean

./lib/distclean :
	$(MAKE) -C lib distclean

./include/distclean :
	$(MAKE) -C include distclean

./tests/tidy :
	$(MAKE) -C tests tidy

./lib/tidy :
	$(MAKE) -C lib tidy

./include/tidy :
	$(MAKE) -C include tidy

