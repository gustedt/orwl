// **********************************************
#include "orwl_taskdep.h"

// To simulate call by reference for Fortran we need a lot of
// temporaries.
#define FREF64(X) (int64_t const){ (X) }

P99_GETOPT_SYNOPSIS("A program derived from task graph arbreSylvain.");
char const*const orwl_taskdep_nnames[8] = {
	[0] = "node_1",
	[1] = "node_2",
	[2] = "node_3",
	[3] = "node_4",
	[4] = "node_5",
	[5] = "node_6",
	[6] = "node_7",
	[7] = "node_8",
};
// **********************************************


// Task graph "arbreSylvain"


// **********************************************
// External information needed for the task graph

// The data type through which tasks communicate.

enum { orwl_taskdep_vsize = 1, };
typedef double orwl_taskdep_data;

// ***************************************
// Types.

// The transfer type between tasks.
typedef orwl_taskdep_data orwl_taskdep_vector[orwl_taskdep_vsize];

// The task functions that are used.
//
typedef void orwl_taskdep_ftype(orwl_taskdep_blob state[static 1], int64_t const tid[static 1], int64_t const phase[static 1],
                int64_t const in_num[static 1], orwl_taskdep_vector in_mat[in_num[0]],
                int64_t const out_num[static 1], orwl_taskdep_vector out_mat[out_num[0]]);
typedef void orwl_taskdep_stype(orwl_taskdep_blob state[static 1], int64_t const num[static 1]);

// *****************************************************************************
// Static properties of the task graph

// we have 8 nodes/regions

extern void iterationStep(orwl_taskdep_blob state[static 1], int64_t const tid[static 1], int64_t const phase[static 1],
    int64_t const in_num[static 1], orwl_taskdep_vector in_mat[in_num[0]],
    int64_t const out_num[static 1], orwl_taskdep_vector out_mat[out_num[0]]);
extern void startup(orwl_taskdep_blob state[static 1], int64_t const num[static 1]);
extern void saveAll(orwl_taskdep_blob state[static 1], int64_t const num[static 1]);
char const*const orwl_taskdep_snames[8] = {
	[0] = "startup",
	[1] = "startup",
	[2] = "startup",
	[3] = "startup",
	[4] = "startup",
	[5] = "startup",
	[6] = "startup",
	[7] = "startup",
};
char const*const orwl_taskdep_fnames[8] = {
	[0] = "iterationStep",
	[1] = "iterationStep",
	[2] = "iterationStep",
	[3] = "iterationStep",
	[4] = "iterationStep",
	[5] = "iterationStep",
	[6] = "iterationStep",
	[7] = "iterationStep",
};
char const*const orwl_taskdep_dnames[8] = {
	[0] = "saveAll",
	[1] = "saveAll",
	[2] = "saveAll",
	[3] = "saveAll",
	[4] = "saveAll",
	[5] = "saveAll",
	[6] = "saveAll",
	[7] = "saveAll",
};

// Initializers for ports. The table index is the index at the source, since only
// these are guaranteed to be unique. The values here are pointers to const
// qualified compound literals, or 0 if no "init" has been given for the port.
static orwl_taskdep_vector const*const orwl_taskdep_defaults[8][1] = {
	[0] = {
		[0] = 0, // no pre-init for 57240/15685
	},
	[1] = {
		[0] = 0, // no pre-init for 196591/20508
	},
	[2] = {
		[0] = 0, // no pre-init for 177522/11
	},
	[3] = {
		[0] = 0, // no pre-init for 23652/82066
	},
	[4] = {
		[0] = 0, // no pre-init for 78042/82032
	},
	[6] = {
		[0] = 0, // no pre-init for 820367/4150
	},
	[7] = {
		[0] = 0, // no pre-init for 375557/32971
	},
};

// The const property for ports. The table index is the index at the source, since only
// these are guaranteed to be unique. .
static bool const orwl_taskdep_const[8][1] = {
	[0] = {
		[0] = 0, // 57240/15685
	},
	[1] = {
		[0] = 0, // 196591/20508
	},
	[2] = {
		[0] = 0, // 177522/11
	},
	[3] = {
		[0] = 0, // 23652/82066
	},
	[4] = {
		[0] = 0, // 78042/82032
	},
	[6] = {
		[0] = 0, // 820367/4150
	},
	[7] = {
		[0] = 0, // 375557/32971
	},
};

// For each output port, provide the textual information about the link
char const*const orwl_taskdep_oports[8][1] = {
	[0] = {
		[0] = "57240/15685",
	},
	[1] = {
		[0] = "196591/20508",
	},
	[2] = {
		[0] = "177522/11",
	},
	[3] = {
		[0] = "23652/82066",
	},
	[4] = {
		[0] = "78042/82032",
	},
	[6] = {
		[0] = "820367/4150",
	},
	[7] = {
		[0] = "375557/32971",
	},
};

// For each input port, provide the textual information about the link
char const*const orwl_taskdep_iports[8][3] = {
	[1] = {
		[0] = "57240/15685",
	},
	[2] = {
		[0] = "196591/20508",
	},
	[5] = {
		[0] = "177522/11",
		[1] = "820367/4150",
		[2] = "375557/32971",
	},
	[6] = {
		[0] = "23652/82066",
		[1] = "78042/82032",
	},
};

// The outdegree for each node
int64_t const orwl_taskdep_deg_out[8] = {
	[0] = 1,
	[1] = 1,
	[2] = 1,
	[3] = 1,
	[4] = 1,
	[5] = 0,
	[6] = 1,
	[7] = 1,
};

// The indegree for each node
int64_t const orwl_taskdep_deg_inp[8] = {
	[0] = 0,
	[1] = 1,
	[2] = 1,
	[3] = 0,
	[4] = 0,
	[5] = 3,
	[6] = 2,
	[7] = 0,
};

// The number of each output port, or -1 if not this number is not available
int64_t const*const orwl_taskdep_onum[8] = {
	[0] = (int64_t const[3]){
		[0] = 57240,
		[1] = -1,
		[2] = -1,
	},
	[1] = (int64_t const[3]){
		[0] = 196591,
		[1] = -1,
		[2] = -1,
	},
	[2] = (int64_t const[3]){
		[0] = 177522,
		[1] = -1,
		[2] = -1,
	},
	[3] = (int64_t const[3]){
		[0] = 23652,
		[1] = -1,
		[2] = -1,
	},
	[4] = (int64_t const[3]){
		[0] = 78042,
		[1] = -1,
		[2] = -1,
	},
	[5] = (int64_t const[3]){
		[0] = -1,
		[1] = -1,
		[2] = -1,
	},
	[6] = (int64_t const[3]){
		[0] = 820367,
		[1] = -1,
		[2] = -1,
	},
	[7] = (int64_t const[3]){
		[0] = 375557,
		[1] = -1,
		[2] = -1,
	},
};

// The number of each input port, or -1 if not this number is not available
int64_t const*const orwl_taskdep_inum[8] = {
	[0] = (int64_t const[3]){
		[0] = -1,
		[1] = -1,
		[2] = -1,
	},
	[1] = (int64_t const[3]){
		[0] = 15685,
		[1] = -1,
		[2] = -1,
	},
	[2] = (int64_t const[3]){
		[0] = 20508,
		[1] = -1,
		[2] = -1,
	},
	[3] = (int64_t const[3]){
		[0] = -1,
		[1] = -1,
		[2] = -1,
	},
	[4] = (int64_t const[3]){
		[0] = -1,
		[1] = -1,
		[2] = -1,
	},
	[5] = (int64_t const[3]){
		[0] = 11,
		[1] = 4150,
		[2] = 32971,
	},
	[6] = (int64_t const[3]){
		[0] = 82066,
		[1] = 82032,
		[2] = -1,
	},
	[7] = (int64_t const[3]){
		[0] = -1,
		[1] = -1,
		[2] = -1,
	},
};

// For each input port, provide the source port
static int64_t const orwl_taskdep_tail[8][3] = {
	[0] = {
		[0] = -1,
		[1] = -1,
		[2] = -1,
	},
	[1] = {
		[0] = 0,
		[1] = -1,
		[2] = -1,
	},
	[2] = {
		[0] = 1,
		[1] = -1,
		[2] = -1,
	},
	[3] = {
		[0] = -1,
		[1] = -1,
		[2] = -1,
	},
	[4] = {
		[0] = -1,
		[1] = -1,
		[2] = -1,
	},
	[5] = {
		[0] = 2,
		[1] = 6,
		[2] = 7,
	},
	[6] = {
		[0] = 3,
		[1] = 4,
		[2] = -1,
	},
	[7] = {
		[0] = -1,
		[1] = -1,
		[2] = -1,
	},
};

static void orwl_taskdep_usage(void) {
    printf("task number\tname\tfunction\tstartup\tshutdown\t(in ports)\t(out ports)\n");
    for (size_t i = 0; i < 8; ++i) {
        printf("%zu\t\t%s\t%s\t%s\t%s", i, orwl_taskdep_nnames[i], orwl_taskdep_fnames[i], orwl_taskdep_snames[i], orwl_taskdep_dnames[i]);
        if (orwl_taskdep_iports[i][0]) {
            printf("\t(%s", orwl_taskdep_iports[i][0]);
            for (size_t j = 1; j < 3; ++j) {
                if (orwl_taskdep_iports[i][j])
                   printf(", %s", orwl_taskdep_iports[i][j]);
            }
            printf(")");
        } else {
            printf("	()");
        }
        if (orwl_taskdep_oports[i][0]) {
            printf("\t(%s", orwl_taskdep_oports[i][0]);
            for (size_t j = 1; j < 1; ++j) {
                if (orwl_taskdep_oports[i][j])
                   printf(", %s", orwl_taskdep_oports[i][j]);
            }
            printf(")");
        } else {
            printf("	()");
        }
        printf("\n");
    }

}

P99_GETOPT_CALLBACK(orwl_taskdep_usage);

// *****************************************************
// Instantiate the ORWL structure.
ORWL_LOCATIONS_PER_TASK(
	orwl_task_interface0
);

ORWL_LOCATIONS_PER_TASK_INSTANTIATION();

ORWL_DECLARE_TASK(orwl_taskdep_type);

// *****************************************************
// Set up the ORWL structure that reflects dependencies.

// This graph is not iterated.
// Using orwl_handle.
// node_1	→ node_2	(57240/15685)
// 0	→ 1
// priorities: 0 1
// node_2	→ node_3	(196591/20508)
// 1	→ 2
// priorities: 0 1
// node_3	→ node_6	(177522/11)
// 2	→ 5
// priorities: 0 1
// node_4	→ node_7	(23652/82066)
// 3	→ 6
// priorities: 0 1
// node_5	→ node_7	(78042/82032)
// 4	→ 6
// priorities: 0 1
// node_7	→ node_6	(820367/4150)
// 6	→ 5
// priorities: 0 1
// node_8	→ node_6	(375557/32971)
// 7	→ 5
// priorities: 0 1

// ****************************************************************
// Per task specific initialization functions. They should ease some
// const propagation concerning the task ID.
static void orwl_taskdep_init_0(orwl_handle hdl_in[static 3], orwl_handle hdl_out[static 1]) {
	report(orwl_taskdep_verbose, "0, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(0, 0), 0, seed, server);

	report(orwl_taskdep_verbose, "0, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "0, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_1(orwl_handle hdl_in[static 3], orwl_handle hdl_out[static 1]) {
	report(orwl_taskdep_verbose, "1, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(1, 0), 0, seed, server);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(0, 0), 1, seed, server);

	report(orwl_taskdep_verbose, "1, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "1, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_2(orwl_handle hdl_in[static 3], orwl_handle hdl_out[static 1]) {
	report(orwl_taskdep_verbose, "2, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(2, 0), 0, seed, server);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(1, 0), 1, seed, server);

	report(orwl_taskdep_verbose, "2, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "2, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_3(orwl_handle hdl_in[static 3], orwl_handle hdl_out[static 1]) {
	report(orwl_taskdep_verbose, "3, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(3, 0), 0, seed, server);

	report(orwl_taskdep_verbose, "3, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "3, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_4(orwl_handle hdl_in[static 3], orwl_handle hdl_out[static 1]) {
	report(orwl_taskdep_verbose, "4, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(4, 0), 0, seed, server);

	report(orwl_taskdep_verbose, "4, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "4, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_5(orwl_handle hdl_in[static 3], orwl_handle hdl_out[static 1]) {
	report(orwl_taskdep_verbose, "5, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	// all output ports are const, no output insertion needed
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(2, 0), 1, seed, server);
	orwl_read_insert(&hdl_in[1], ORWL_LOCATION(6, 0), 1, seed, server);
	orwl_read_insert(&hdl_in[2], ORWL_LOCATION(7, 0), 1, seed, server);

	report(orwl_taskdep_verbose, "5, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "5, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_6(orwl_handle hdl_in[static 3], orwl_handle hdl_out[static 1]) {
	report(orwl_taskdep_verbose, "6, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(6, 0), 0, seed, server);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(3, 0), 1, seed, server);
	orwl_read_insert(&hdl_in[1], ORWL_LOCATION(4, 0), 1, seed, server);

	report(orwl_taskdep_verbose, "6, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "6, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_7(orwl_handle hdl_in[static 3], orwl_handle hdl_out[static 1]) {
	report(orwl_taskdep_verbose, "7, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(7, 0), 0, seed, server);

	report(orwl_taskdep_verbose, "7, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "7, finished orwl_taskdep_init");

};
static orwl_taskdep_vector const p_dummy = { 0 };

// Task function that is executed by task 0 (node_1)
static void orwl_taskdep_run_0(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #0 (node_1)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        startup(&state, &FREF64(0));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 0,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    orwl_taskdep_vector* mout = odeg ? &mat_out[0] : 0;

    // This graph is not iterated.
    static orwl_handle hdl_in[3] = {
         ORWL_HANDLE_INITIALIZER,         ORWL_HANDLE_INITIALIZER,         ORWL_HANDLE_INITIALIZER,    };
    static orwl_handle hdl_out[1] = {
         ORWL_HANDLE_INITIALIZER,
    };
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i)
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(0, i));
    ORWL_TIMER(start) orwl_taskdep_init_0(hdl_in, hdl_out);
    {
    int64_t const orwl_phase = 0;
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[0][i] != -1) {
            size_t port = orwl_taskdep_tail[0][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 1][port % 1]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 1][port % 1];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 1][port % 1]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     {
          orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[0][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(&mout[i], p_out[i], sizeof *(p_out[i]));
                       }
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   mout = orwl_write_map(&hdl_out[0], 0, seed);
               }
          // Run one iteration of the function for the node.
          ORWL_TIMER(func)
              iterationStep(&state, &FREF64(0), &orwl_phase,
                                 &FREF64(ideg), min,
                                 &FREF64(odeg), mout);
               // Copy the result into the target locations.
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i) {
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[0][i]) {
                           memcpy(p_out[i], &mout[i], sizeof *(p_out[i]));
                       }
                   }
                   orwl_release_or_next(hdl_out, odeg, seed);
               } else {
                   // This port can't be  const.
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
    } // end of iteration or block


    ORWL_TIMER(start)
        saveAll(&state, &FREF64(0));
    report(orwl_taskdep_verbose, "task #0 (node_1) ends");
}


// Task function that is executed by task 1 (node_2)
static void orwl_taskdep_run_1(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #1 (node_2)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        startup(&state, &FREF64(1));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    orwl_taskdep_vector* mout = odeg ? &mat_out[0] : 0;

    // This graph is not iterated.
    static orwl_handle hdl_in[3] = {
         ORWL_HANDLE_INITIALIZER,         ORWL_HANDLE_INITIALIZER,         ORWL_HANDLE_INITIALIZER,    };
    static orwl_handle hdl_out[1] = {
         ORWL_HANDLE_INITIALIZER,
    };
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i)
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(1, i));
    ORWL_TIMER(start) orwl_taskdep_init_1(hdl_in, hdl_out);
    {
    int64_t const orwl_phase = 0;
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[1][i] != -1) {
            size_t port = orwl_taskdep_tail[1][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 1][port % 1]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 1][port % 1];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 1][port % 1]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     {
          orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[1][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(&mout[i], p_out[i], sizeof *(p_out[i]));
                       }
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   mout = orwl_write_map(&hdl_out[0], 0, seed);
               }
          // Run one iteration of the function for the node.
          ORWL_TIMER(func)
              iterationStep(&state, &FREF64(1), &orwl_phase,
                                 &FREF64(ideg), min,
                                 &FREF64(odeg), mout);
               // Copy the result into the target locations.
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i) {
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[1][i]) {
                           memcpy(p_out[i], &mout[i], sizeof *(p_out[i]));
                       }
                   }
                   orwl_release_or_next(hdl_out, odeg, seed);
               } else {
                   // This port can't be  const.
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
    } // end of iteration or block


    ORWL_TIMER(start)
        saveAll(&state, &FREF64(1));
    report(orwl_taskdep_verbose, "task #1 (node_2) ends");
}


// Task function that is executed by task 2 (node_3)
static void orwl_taskdep_run_2(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #2 (node_3)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        startup(&state, &FREF64(2));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    orwl_taskdep_vector* mout = odeg ? &mat_out[0] : 0;

    // This graph is not iterated.
    static orwl_handle hdl_in[3] = {
         ORWL_HANDLE_INITIALIZER,         ORWL_HANDLE_INITIALIZER,         ORWL_HANDLE_INITIALIZER,    };
    static orwl_handle hdl_out[1] = {
         ORWL_HANDLE_INITIALIZER,
    };
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i)
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(2, i));
    ORWL_TIMER(start) orwl_taskdep_init_2(hdl_in, hdl_out);
    {
    int64_t const orwl_phase = 0;
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[2][i] != -1) {
            size_t port = orwl_taskdep_tail[2][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 1][port % 1]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 1][port % 1];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 1][port % 1]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     {
          orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[2][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(&mout[i], p_out[i], sizeof *(p_out[i]));
                       }
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   mout = orwl_write_map(&hdl_out[0], 0, seed);
               }
          // Run one iteration of the function for the node.
          ORWL_TIMER(func)
              iterationStep(&state, &FREF64(2), &orwl_phase,
                                 &FREF64(ideg), min,
                                 &FREF64(odeg), mout);
               // Copy the result into the target locations.
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i) {
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[2][i]) {
                           memcpy(p_out[i], &mout[i], sizeof *(p_out[i]));
                       }
                   }
                   orwl_release_or_next(hdl_out, odeg, seed);
               } else {
                   // This port can't be  const.
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
    } // end of iteration or block


    ORWL_TIMER(start)
        saveAll(&state, &FREF64(2));
    report(orwl_taskdep_verbose, "task #2 (node_3) ends");
}


// Task function that is executed by task 3 (node_4)
static void orwl_taskdep_run_3(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #3 (node_4)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        startup(&state, &FREF64(3));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 0,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    orwl_taskdep_vector* mout = odeg ? &mat_out[0] : 0;

    // This graph is not iterated.
    static orwl_handle hdl_in[3] = {
         ORWL_HANDLE_INITIALIZER,         ORWL_HANDLE_INITIALIZER,         ORWL_HANDLE_INITIALIZER,    };
    static orwl_handle hdl_out[1] = {
         ORWL_HANDLE_INITIALIZER,
    };
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i)
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(3, i));
    ORWL_TIMER(start) orwl_taskdep_init_3(hdl_in, hdl_out);
    {
    int64_t const orwl_phase = 0;
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[3][i] != -1) {
            size_t port = orwl_taskdep_tail[3][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 1][port % 1]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 1][port % 1];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 1][port % 1]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     {
          orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[3][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(&mout[i], p_out[i], sizeof *(p_out[i]));
                       }
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   mout = orwl_write_map(&hdl_out[0], 0, seed);
               }
          // Run one iteration of the function for the node.
          ORWL_TIMER(func)
              iterationStep(&state, &FREF64(3), &orwl_phase,
                                 &FREF64(ideg), min,
                                 &FREF64(odeg), mout);
               // Copy the result into the target locations.
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i) {
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[3][i]) {
                           memcpy(p_out[i], &mout[i], sizeof *(p_out[i]));
                       }
                   }
                   orwl_release_or_next(hdl_out, odeg, seed);
               } else {
                   // This port can't be  const.
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
    } // end of iteration or block


    ORWL_TIMER(start)
        saveAll(&state, &FREF64(3));
    report(orwl_taskdep_verbose, "task #3 (node_4) ends");
}


// Task function that is executed by task 4 (node_5)
static void orwl_taskdep_run_4(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #4 (node_5)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        startup(&state, &FREF64(4));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 0,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    orwl_taskdep_vector* mout = odeg ? &mat_out[0] : 0;

    // This graph is not iterated.
    static orwl_handle hdl_in[3] = {
         ORWL_HANDLE_INITIALIZER,         ORWL_HANDLE_INITIALIZER,         ORWL_HANDLE_INITIALIZER,    };
    static orwl_handle hdl_out[1] = {
         ORWL_HANDLE_INITIALIZER,
    };
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i)
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(4, i));
    ORWL_TIMER(start) orwl_taskdep_init_4(hdl_in, hdl_out);
    {
    int64_t const orwl_phase = 0;
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[4][i] != -1) {
            size_t port = orwl_taskdep_tail[4][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 1][port % 1]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 1][port % 1];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 1][port % 1]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     {
          orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[4][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(&mout[i], p_out[i], sizeof *(p_out[i]));
                       }
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   mout = orwl_write_map(&hdl_out[0], 0, seed);
               }
          // Run one iteration of the function for the node.
          ORWL_TIMER(func)
              iterationStep(&state, &FREF64(4), &orwl_phase,
                                 &FREF64(ideg), min,
                                 &FREF64(odeg), mout);
               // Copy the result into the target locations.
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i) {
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[4][i]) {
                           memcpy(p_out[i], &mout[i], sizeof *(p_out[i]));
                       }
                   }
                   orwl_release_or_next(hdl_out, odeg, seed);
               } else {
                   // This port can't be  const.
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
    } // end of iteration or block


    ORWL_TIMER(start)
        saveAll(&state, &FREF64(4));
    report(orwl_taskdep_verbose, "task #4 (node_5) ends");
}


// Task function that is executed by task 5 (node_6)
static void orwl_taskdep_run_5(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #5 (node_6)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        startup(&state, &FREF64(5));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 3,
        odeg  = 0,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    orwl_taskdep_vector* mout = odeg ? &mat_out[0] : 0;

    // This graph is not iterated.
    static orwl_handle hdl_in[3] = {
         ORWL_HANDLE_INITIALIZER,         ORWL_HANDLE_INITIALIZER,         ORWL_HANDLE_INITIALIZER,    };
    static orwl_handle hdl_out[1] = {
         ORWL_HANDLE_INITIALIZER,
    };
    ORWL_TIMER(start) orwl_taskdep_init_5(hdl_in, hdl_out);
    {
    int64_t const orwl_phase = 0;
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[5][i] != -1) {
            size_t port = orwl_taskdep_tail[5][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 1][port % 1]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 1][port % 1];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 1][port % 1]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     {
          // Run one iteration of the function for the node.
          ORWL_TIMER(func)
              iterationStep(&state, &FREF64(5), &orwl_phase,
                                 &FREF64(ideg), min,
                                 &FREF64(odeg), mout);
     }
    } // end of iteration or block


    ORWL_TIMER(start)
        saveAll(&state, &FREF64(5));
    report(orwl_taskdep_verbose, "task #5 (node_6) ends");
}


// Task function that is executed by task 6 (node_7)
static void orwl_taskdep_run_6(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #6 (node_7)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        startup(&state, &FREF64(6));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 2,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    orwl_taskdep_vector* mout = odeg ? &mat_out[0] : 0;

    // This graph is not iterated.
    static orwl_handle hdl_in[3] = {
         ORWL_HANDLE_INITIALIZER,         ORWL_HANDLE_INITIALIZER,         ORWL_HANDLE_INITIALIZER,    };
    static orwl_handle hdl_out[1] = {
         ORWL_HANDLE_INITIALIZER,
    };
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i)
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(6, i));
    ORWL_TIMER(start) orwl_taskdep_init_6(hdl_in, hdl_out);
    {
    int64_t const orwl_phase = 0;
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[6][i] != -1) {
            size_t port = orwl_taskdep_tail[6][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 1][port % 1]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 1][port % 1];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 1][port % 1]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     {
          orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[6][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(&mout[i], p_out[i], sizeof *(p_out[i]));
                       }
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   mout = orwl_write_map(&hdl_out[0], 0, seed);
               }
          // Run one iteration of the function for the node.
          ORWL_TIMER(func)
              iterationStep(&state, &FREF64(6), &orwl_phase,
                                 &FREF64(ideg), min,
                                 &FREF64(odeg), mout);
               // Copy the result into the target locations.
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i) {
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[6][i]) {
                           memcpy(p_out[i], &mout[i], sizeof *(p_out[i]));
                       }
                   }
                   orwl_release_or_next(hdl_out, odeg, seed);
               } else {
                   // This port can't be  const.
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
    } // end of iteration or block


    ORWL_TIMER(start)
        saveAll(&state, &FREF64(6));
    report(orwl_taskdep_verbose, "task #6 (node_7) ends");
}


// Task function that is executed by task 7 (node_8)
static void orwl_taskdep_run_7(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #7 (node_8)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        startup(&state, &FREF64(7));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 0,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    orwl_taskdep_vector* mout = odeg ? &mat_out[0] : 0;

    // This graph is not iterated.
    static orwl_handle hdl_in[3] = {
         ORWL_HANDLE_INITIALIZER,         ORWL_HANDLE_INITIALIZER,         ORWL_HANDLE_INITIALIZER,    };
    static orwl_handle hdl_out[1] = {
         ORWL_HANDLE_INITIALIZER,
    };
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i)
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(7, i));
    ORWL_TIMER(start) orwl_taskdep_init_7(hdl_in, hdl_out);
    {
    int64_t const orwl_phase = 0;
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[7][i] != -1) {
            size_t port = orwl_taskdep_tail[7][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 1][port % 1]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 1][port % 1];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 1][port % 1]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     {
          orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[7][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(&mout[i], p_out[i], sizeof *(p_out[i]));
                       }
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   mout = orwl_write_map(&hdl_out[0], 0, seed);
               }
          // Run one iteration of the function for the node.
          ORWL_TIMER(func)
              iterationStep(&state, &FREF64(7), &orwl_phase,
                                 &FREF64(ideg), min,
                                 &FREF64(odeg), mout);
               // Copy the result into the target locations.
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i) {
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[7][i]) {
                           memcpy(p_out[i], &mout[i], sizeof *(p_out[i]));
                       }
                   }
                   orwl_release_or_next(hdl_out, odeg, seed);
               } else {
                   // This port can't be  const.
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
    } // end of iteration or block


    ORWL_TIMER(start)
        saveAll(&state, &FREF64(7));
    report(orwl_taskdep_verbose, "task #7 (node_8) ends");
}


static void (*const orwl_taskdep_runs[])(int argc, char* argv[argc+1]) = {
    orwl_taskdep_run_0,
    orwl_taskdep_run_1,
    orwl_taskdep_run_2,
    orwl_taskdep_run_3,
    orwl_taskdep_run_4,
    orwl_taskdep_run_5,
    orwl_taskdep_run_6,
    orwl_taskdep_run_7,
};

int64_t const orwl_taskdep_nodes   = 8;
int64_t const orwl_taskdep_max_inp = 3;
int64_t const orwl_taskdep_max_out = 1;

// Add symbols for Fortran compatibility.

int64_t const ORWL_TASKDEP_FORT(orwl_taskdep_vsize)   = orwl_taskdep_vsize;

ORWL_TASKDEP_ALIAS(orwl_taskdep_deg_out);
ORWL_TASKDEP_ALIAS(orwl_taskdep_deg_inp);
ORWL_TASKDEP_ALIAS(orwl_taskdep_nodes);
ORWL_TASKDEP_ALIAS(orwl_taskdep_max_inp);
ORWL_TASKDEP_ALIAS(orwl_taskdep_max_out);

// A wrapper as generic task function that just launches the specific function
ORWL_DEFINE_TASK(orwl_taskdep_type) {
    ORWL_THREAD_USE(orwl_taskdep_type, argc, argv);
    int64_t const tid = orwl_mytid;
    orwl_taskdep_runs[tid](argc, argv);
}

int main(int argc, char* argv[argc+1]) {
  p99_getopt_initialize(&argc, &argv);

  ORWL_TIMER() {
    orwl_init();

    if (orwl_nt != 8) {
        report(1, "We need exactly 8 task, to process this graph, current value is %zu, exiting.", orwl_nt);
        return EXIT_FAILURE;
    }

    //! [launch one thread per task]
    for (size_t i = 0; i < orwl_lt; i++) {
      orwl_taskdep_type* task = P99_NEW(orwl_taskdep_type, argc, argv);
      orwl_taskdep_type_create_task(task, orwl_tids[i]);
    }
    //! [launch one thread per task]
  }
  return EXIT_SUCCESS;
}

// End of program for graph arbreSylvain
// *********************************************************************************

