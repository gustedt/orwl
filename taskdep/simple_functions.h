#ifndef SIMPLE_FUNCTIONS_H
#define SIMPLE_FUNCTIONS_H 1

#include <stdint.h>

typedef float floating;

typedef floating vtype[3];

void task_startup(orwl_taskdep_blob state[static restrict 1], int64_t const tid[static 1]);
void task_shutdown(orwl_taskdep_blob state[static restrict 1], int64_t const tid[static 1]);
void special(orwl_taskdep_blob state[static restrict 1], int64_t const tid[static 1], int64_t const phase[static 1],
             int64_t const in_num[static 1], vtype in_mat[restrict in_num[0]], _Bool skip_in[restrict in_num[0]],
             int64_t const out_num[static 1], vtype out_mat[restrict out_num[0]], _Bool skip_out[restrict out_num[0]]);
void regular(orwl_taskdep_blob state[static 1], int64_t const tid[static 1], int64_t const phase[static 1],
             int64_t const in_num[static 1], vtype in_mat[restrict in_num[0]], _Bool skip_in[restrict in_num[0]],
             int64_t const out_num[static 1], vtype out_mat[restrict out_num[0]], _Bool skip_out[restrict out_num[0]]);
void update(orwl_taskdep_blob state[static 1], int64_t const tid[static 1], int64_t const phase[static 1],
             int64_t const in_num[static 1], vtype in_mat[restrict in_num[0]], _Bool skip_in[restrict in_num[0]],
             int64_t const out_num[static 1], vtype out_mat[restrict out_num[0]], _Bool skip_out[restrict out_num[0]]);

#endif
