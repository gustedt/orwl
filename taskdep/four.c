// **********************************************
#include "orwl_taskdep.h"
#include "simple_functions.h"

// To simulate call by reference for Fortran we need a lot of
// temporaries.
#define FREF64(X) (int64_t const){ (X) }

P99_GETOPT_SYNOPSIS("A program derived from task graph 'four' with 4 nodes and 4 dependencies.");
char const*const orwl_taskdep_nnames[4] = {
	[0] = "+∞",
	[1] = "-∞",
	[2] = "Ping",
	[3] = "Pong",
};
// **********************************************


// Task graph "four"


// **********************************************
// External information needed for the task graph

// The data type through which tasks communicate.

enum { orwl_taskdep_vsize = 3, };
typedef floating orwl_taskdep_data;

// ***************************************
// Types.

// The transfer type between tasks.
typedef orwl_taskdep_data orwl_taskdep_vector[orwl_taskdep_vsize];

// The task functions that are used.
//
typedef void orwl_taskdep_ftype(orwl_taskdep_blob state[static restrict 1], int64_t const tid[static 1], int64_t const phase[static 1],
                int64_t const in_num[static 1], orwl_taskdep_vector in_mat[restrict in_num[0]], _Bool skip_in[restrict in_num[0]],
                int64_t const out_num[static 1], orwl_taskdep_vector out_mat[restrict out_num[0]], _Bool skip_out[restrict out_num[0]]);
typedef void orwl_taskdep_stype(orwl_taskdep_blob state[static restrict 1], int64_t const num[static 1]);

// *****************************************************************************
// Static properties of the task graph

// we have 4 nodes/regions

extern void regular(orwl_taskdep_blob state[static restrict 1], int64_t const tid[static 1], int64_t const phase[static 1],
    int64_t const in_num[static 1], orwl_taskdep_vector in_mat[restrict in_num[0]], _Bool skip_in[restrict in_num[0]],
    int64_t const out_num[static 1], orwl_taskdep_vector out_mat[restrict out_num[0]], _Bool skip_out[restrict out_num[0]]);
extern void orwl_taskdep_start(orwl_taskdep_blob state[static restrict 1], int64_t const num[static 1]);
extern void orwl_taskdep_shutdown(orwl_taskdep_blob state[static restrict 1], int64_t const num[static 1]);
char const*const orwl_taskdep_snames[4] = {
	[0] = "orwl_taskdep_start",
	[1] = "orwl_taskdep_start",
	[2] = "orwl_taskdep_start",
	[3] = "orwl_taskdep_start",
};
char const*const orwl_taskdep_fnames[4] = {
	[0] = "regular",
	[1] = "regular",
	[2] = "regular",
	[3] = "regular",
};
char const*const orwl_taskdep_dnames[4] = {
	[0] = "orwl_taskdep_shutdown",
	[1] = "orwl_taskdep_shutdown",
	[2] = "orwl_taskdep_shutdown",
	[3] = "orwl_taskdep_shutdown",
};

// Initializers for ports. The table index is the index at the source, since only
// these are guaranteed to be unique. The values here are pointers to const
// qualified compound literals, or 0 if no "init" has been given for the port.
static orwl_taskdep_vector const*const orwl_taskdep_defaults[4][1] = {
	[0] = {
		[0] = &(orwl_taskdep_vector const){ +0 }, // right
	},
	[1] = {
		[0] = &(orwl_taskdep_vector const){ +0 }, // left
	},
	[2] = {
		[0] = 0, // no pre-init for ping
	},
	[3] = {
		[0] = &(orwl_taskdep_vector const){ +0 }, // pong
	},
};

// The const property for ports. The table index is the index at the source, since only
// these are guaranteed to be unique. .
static bool const orwl_taskdep_const[4][1] = {
	[0] = {
		[0] = 1, // right
	},
	[1] = {
		[0] = 1, // left
	},
	[2] = {
		[0] = 0, // ping
	},
	[3] = {
		[0] = 0, // pong
	},
};

// For each output port, provide the textual information about the link
char const*const orwl_taskdep_oports[4][1] = {
	[0] = {
		[0] = "right",
	},
	[1] = {
		[0] = "left",
	},
	[2] = {
		[0] = "ping",
	},
	[3] = {
		[0] = "pong",
	},
};

// For each input port, provide the textual information about the link
char const*const orwl_taskdep_iports[4][2] = {
	[2] = {
		[0] = "pong",
		[1] = "left",
	},
	[3] = {
		[0] = "ping",
		[1] = "right",
	},
};

// The outdegree for each node
_Alignas(32) int64_t const orwl_taskdep_deg_out[4] = {
	[0] = 1,
	[1] = 1,
	[2] = 1,
	[3] = 1,
};

// The indegree for each node
_Alignas(32) int64_t const orwl_taskdep_deg_inp[4] = {
	[0] = 0,
	[1] = 0,
	[2] = 2,
	[3] = 2,
};

// The number of each output port, or -1 if not this number is not available
int64_t const*const orwl_taskdep_onum[4] = {
	[0] = (int64_t const[2]){
		[0] = -1,
		[1] = -1,
	},
	[1] = (int64_t const[2]){
		[0] = -1,
		[1] = -1,
	},
	[2] = (int64_t const[2]){
		[0] = -1,
		[1] = -1,
	},
	[3] = (int64_t const[2]){
		[0] = -1,
		[1] = -1,
	},
};

// The number of each input port, or -1 if not this number is not available
int64_t const*const orwl_taskdep_inum[4] = {
	[0] = (int64_t const[2]){
		[0] = -1,
		[1] = -1,
	},
	[1] = (int64_t const[2]){
		[0] = -1,
		[1] = -1,
	},
	[2] = (int64_t const[2]){
		[0] = -1,
		[1] = -1,
	},
	[3] = (int64_t const[2]){
		[0] = -1,
		[1] = -1,
	},
};

// For each input port, provide the source port
static int64_t const orwl_taskdep_tail[4][2] = {
	[0] = {
		[0] = -1,
		[1] = -1,
	},
	[1] = {
		[0] = -1,
		[1] = -1,
	},
	[2] = {
		[0] = 3,
		[1] = 1,
	},
	[3] = {
		[0] = 2,
		[1] = 0,
	},
};

void orwl_taskdep_usage(void) {
    printf("task number\tname\tfunction\tstartup\tshutdown\t(in ports)\t(out ports)\n");
    for (size_t i = 0; i < 4; ++i) {
        printf("%zu\t\t%s\t%s\t%s\t%s", i, orwl_taskdep_nnames[i], orwl_taskdep_fnames[i], orwl_taskdep_snames[i], orwl_taskdep_dnames[i]);
        if (orwl_taskdep_iports[i][0]) {
            printf("\t(%s", orwl_taskdep_iports[i][0]);
            for (size_t j = 1; j < 2; ++j) {
                if (orwl_taskdep_iports[i][j])
                   printf(", %s", orwl_taskdep_iports[i][j]);
            }
            printf(")");
        } else {
            printf("	()");
        }
        if (orwl_taskdep_oports[i][0]) {
            printf("\t(%s", orwl_taskdep_oports[i][0]);
            for (size_t j = 1; j < 1; ++j) {
                if (orwl_taskdep_oports[i][j])
                   printf(", %s", orwl_taskdep_oports[i][j]);
            }
            printf(")");
        } else {
            printf("	()");
        }
        printf("\n");
    }

}

P99_GETOPT_CALLBACK(orwl_taskdep_usage);

// *****************************************************
// Instantiate the ORWL structure.
ORWL_LOCATIONS_PER_TASK(
	orwl_task_write_if0,
	orwl_task_read_if0
);

ORWL_LOCATIONS_PER_TASK_INSTANTIATION();

ORWL_DECLARE_TASK(orwl_taskdep_type);

// *****************************************************
// Set up the ORWL structure that reflects dependencies.

// This graph is iterated.
// Using orwl_handle2.
// +∞	→ Pong	(right)
// 0	→ 3
// priorities: 0 1
// -∞	→ Ping	(left)
// 1	→ 2
// priorities: 0 1
// Ping	→ Pong	(ping)
// 2	→ 3
// priorities: 0 1
// Pong	→ Ping	(pong)
// 3	→ 2
// using reverse edge "ping (pong)"
// priorities: 1 0

// ****************************************************************
// Per task specific initialization functions. They should ease some
// const propagation concerning the task ID.
static void orwl_taskdep_init_0(orwl_handle2 hdl_in[static 2], orwl_handle2 hdl_out[static 1]) {
	report(orwl_taskdep_verbose, "0, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	// all ports are const, no handle insertion needed

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "0, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_1(orwl_handle2 hdl_in[static 2], orwl_handle2 hdl_out[static 1]) {
	report(orwl_taskdep_verbose, "1, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	// all ports are const, no handle insertion needed

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "1, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_2(orwl_handle2 hdl_in[static 2], orwl_handle2 hdl_out[static 1]) {
	report(orwl_taskdep_verbose, "2, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(2, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(2, orwl_task_write_if0), ORWL_LOCATION(2, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(3, orwl_task_read_if0), 0, seed, server);
	// const port at 1

	report(orwl_taskdep_verbose, "2, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "2, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_3(orwl_handle2 hdl_in[static 2], orwl_handle2 hdl_out[static 1]) {
	report(orwl_taskdep_verbose, "3, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(3, orwl_task_write_if0), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(3, orwl_task_write_if0), ORWL_LOCATION(3, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(2, orwl_task_read_if0), 1, seed, server);
	// const port at 1

	report(orwl_taskdep_verbose, "3, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "3, finished orwl_taskdep_init");

};
static orwl_taskdep_vector const p_dummy = { 0 };

// Task function that is executed by task 0 (+∞)
static void orwl_taskdep_run_0(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #0 (+∞)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(0));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 0,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[2] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[1] = {
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(start) orwl_taskdep_init_0(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #0 (+∞) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[0][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[0][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 1][port % 1]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 1][port % 1];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 1][port % 1]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(0), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #0 (+∞) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(0));

    report(orwl_taskdep_verbose, "task #0 (+∞) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #0 (+∞) disconnecting");

    report(orwl_taskdep_verbose, "task #0 (+∞) ends");
}


// Task function that is executed by task 1 (-∞)
static void orwl_taskdep_run_1(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #1 (-∞)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(1));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 0,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[2] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[1] = {
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(start) orwl_taskdep_init_1(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #1 (-∞) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[1][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[1][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 1][port % 1]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 1][port % 1];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 1][port % 1]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(1), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #1 (-∞) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(1));

    report(orwl_taskdep_verbose, "task #1 (-∞) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #1 (-∞) disconnecting");

    report(orwl_taskdep_verbose, "task #1 (-∞) ends");
}


// Task function that is executed by task 2 (Ping)
static void orwl_taskdep_run_2(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #2 (Ping)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(2));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 2,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[2] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[1] = {
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(2, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(2, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_2(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #2 (Ping) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[2][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[2][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 1][port % 1]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 1][port % 1];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 1][port % 1]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(2), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[2][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #2 (Ping) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(2));

    report(orwl_taskdep_verbose, "task #2 (Ping) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #2 (Ping) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #2 (Ping) ends");
}


// Task function that is executed by task 3 (Pong)
static void orwl_taskdep_run_3(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #3 (Pong)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(3));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 2,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[2] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[1] = {
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(3, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(3, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_3(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #3 (Pong) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[3][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[3][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 1][port % 1]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 1][port % 1];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 1][port % 1]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(3), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[3][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #3 (Pong) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(3));

    report(orwl_taskdep_verbose, "task #3 (Pong) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #3 (Pong) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #3 (Pong) ends");
}


static void (*const orwl_taskdep_runs[])(int argc, char* argv[argc+1]) = {
    orwl_taskdep_run_0,
    orwl_taskdep_run_1,
    orwl_taskdep_run_2,
    orwl_taskdep_run_3,
};

_Alignas(32) int64_t const orwl_taskdep_nodes   = 4;
_Alignas(32) int64_t const orwl_taskdep_max_inp = 2;
_Alignas(32) int64_t const orwl_taskdep_max_out = 1;

// Add symbols for Fortran compatibility.

_Alignas(32) int64_t const ORWL_TASKDEP_FORT(orwl_taskdep_vsize)   = orwl_taskdep_vsize;

ORWL_TASKDEP_ALIAS(orwl_taskdep_deg_out);
ORWL_TASKDEP_ALIAS(orwl_taskdep_deg_inp);
ORWL_TASKDEP_ALIAS(orwl_taskdep_nodes);
ORWL_TASKDEP_ALIAS(orwl_taskdep_max_inp);
ORWL_TASKDEP_ALIAS(orwl_taskdep_max_out);

// A wrapper as generic task function that just launches the specific function
ORWL_DEFINE_TASK(orwl_taskdep_type) {
    ORWL_THREAD_USE(orwl_taskdep_type, argc, argv);
    int64_t const tid = orwl_mytid;
    orwl_taskdep_runs[tid](argc, argv);
}

int main(int argc, char* argv[argc+1]) {
  p99_getopt_initialize(&argc, &argv);

  ORWL_TIMER() {
    orwl_init();

    if (orwl_nt != 4) {
        report(1, "We need exactly 4 task, to process this graph, current value is %zu, exiting.", orwl_nt);
        return EXIT_FAILURE;
    }

    //! [launch one thread per task]
    for (size_t i = 0; i < orwl_lt; i++) {
      orwl_taskdep_type* task = P99_NEW(orwl_taskdep_type, argc, argv);
      orwl_taskdep_type_create_task(task, orwl_tids[i]);
    }
    //! [launch one thread per task]
  }
  return EXIT_SUCCESS;
}

// End of program for graph four
// *********************************************************************************

