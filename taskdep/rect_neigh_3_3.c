// **********************************************
#include "orwl_taskdep.h"
#include "simple_functions.h"

// To simulate call by reference for Fortran we need a lot of
// temporaries.
#define FREF64(X) (int64_t const){ (X) }

P99_GETOPT_SYNOPSIS("A program derived from task graph 'rect_neigh_3_3' with 33 nodes and 48 dependencies.");
char const*const orwl_taskdep_nnames[33] = {
	[0] = "buf_1_1_b",
	[1] = "buf_1_1_r",
	[2] = "buf_1_2_b",
	[3] = "buf_1_2_l",
	[4] = "buf_1_2_r",
	[5] = "buf_1_3_b",
	[6] = "buf_1_3_l",
	[7] = "buf_2_1_b",
	[8] = "buf_2_1_r",
	[9] = "buf_2_1_t",
	[10] = "buf_2_2_b",
	[11] = "buf_2_2_l",
	[12] = "buf_2_2_r",
	[13] = "buf_2_2_t",
	[14] = "buf_2_3_b",
	[15] = "buf_2_3_l",
	[16] = "buf_2_3_t",
	[17] = "buf_3_1_r",
	[18] = "buf_3_1_t",
	[19] = "buf_3_2_l",
	[20] = "buf_3_2_r",
	[21] = "buf_3_2_t",
	[22] = "buf_3_3_l",
	[23] = "buf_3_3_t",
	[24] = "node_1_1",
	[25] = "node_1_2",
	[26] = "node_1_3",
	[27] = "node_2_1",
	[28] = "node_2_2",
	[29] = "node_2_3",
	[30] = "node_3_1",
	[31] = "node_3_2",
	[32] = "node_3_3",
};
// **********************************************


// Task graph "rect_neigh_3_3"


// **********************************************
// External information needed for the task graph

// The data type through which tasks communicate.

enum { orwl_taskdep_vsize = 3, };
typedef floating orwl_taskdep_data;

// ***************************************
// Types.

// The transfer type between tasks.
typedef orwl_taskdep_data orwl_taskdep_vector[orwl_taskdep_vsize];

// The task functions that are used.
//
typedef void orwl_taskdep_ftype(orwl_taskdep_blob state[static restrict 1], int64_t const tid[static 1], int64_t const phase[static 1],
                int64_t const in_num[static 1], orwl_taskdep_vector in_mat[restrict in_num[0]], _Bool skip_in[restrict in_num[0]],
                int64_t const out_num[static 1], orwl_taskdep_vector out_mat[restrict out_num[0]], _Bool skip_out[restrict out_num[0]]);
typedef void orwl_taskdep_stype(orwl_taskdep_blob state[static restrict 1], int64_t const num[static 1]);

// *****************************************************************************
// Static properties of the task graph

// we have 33 nodes/regions

extern void update(orwl_taskdep_blob state[static restrict 1], int64_t const tid[static 1], int64_t const phase[static 1],
    int64_t const in_num[static 1], orwl_taskdep_vector in_mat[restrict in_num[0]], _Bool skip_in[restrict in_num[0]],
    int64_t const out_num[static 1], orwl_taskdep_vector out_mat[restrict out_num[0]], _Bool skip_out[restrict out_num[0]]);
extern void orwl_taskdep_start(orwl_taskdep_blob state[static restrict 1], int64_t const num[static 1]);
extern void orwl_taskdep_shutdown(orwl_taskdep_blob state[static restrict 1], int64_t const num[static 1]);
extern void regular(orwl_taskdep_blob state[static restrict 1], int64_t const tid[static 1], int64_t const phase[static 1],
    int64_t const in_num[static 1], orwl_taskdep_vector in_mat[restrict in_num[0]], _Bool skip_in[restrict in_num[0]],
    int64_t const out_num[static 1], orwl_taskdep_vector out_mat[restrict out_num[0]], _Bool skip_out[restrict out_num[0]]);
char const*const orwl_taskdep_snames[33] = {
	[0] = "orwl_taskdep_start",
	[1] = "orwl_taskdep_start",
	[2] = "orwl_taskdep_start",
	[3] = "orwl_taskdep_start",
	[4] = "orwl_taskdep_start",
	[5] = "orwl_taskdep_start",
	[6] = "orwl_taskdep_start",
	[7] = "orwl_taskdep_start",
	[8] = "orwl_taskdep_start",
	[9] = "orwl_taskdep_start",
	[10] = "orwl_taskdep_start",
	[11] = "orwl_taskdep_start",
	[12] = "orwl_taskdep_start",
	[13] = "orwl_taskdep_start",
	[14] = "orwl_taskdep_start",
	[15] = "orwl_taskdep_start",
	[16] = "orwl_taskdep_start",
	[17] = "orwl_taskdep_start",
	[18] = "orwl_taskdep_start",
	[19] = "orwl_taskdep_start",
	[20] = "orwl_taskdep_start",
	[21] = "orwl_taskdep_start",
	[22] = "orwl_taskdep_start",
	[23] = "orwl_taskdep_start",
	[24] = "orwl_taskdep_start",
	[25] = "orwl_taskdep_start",
	[26] = "orwl_taskdep_start",
	[27] = "orwl_taskdep_start",
	[28] = "orwl_taskdep_start",
	[29] = "orwl_taskdep_start",
	[30] = "orwl_taskdep_start",
	[31] = "orwl_taskdep_start",
	[32] = "orwl_taskdep_start",
};
char const*const orwl_taskdep_fnames[33] = {
	[0] = "update",
	[1] = "update",
	[2] = "update",
	[3] = "update",
	[4] = "update",
	[5] = "update",
	[6] = "update",
	[7] = "update",
	[8] = "update",
	[9] = "update",
	[10] = "update",
	[11] = "update",
	[12] = "update",
	[13] = "update",
	[14] = "update",
	[15] = "update",
	[16] = "update",
	[17] = "update",
	[18] = "update",
	[19] = "update",
	[20] = "update",
	[21] = "update",
	[22] = "update",
	[23] = "update",
	[24] = "regular",
	[25] = "regular",
	[26] = "regular",
	[27] = "regular",
	[28] = "regular",
	[29] = "regular",
	[30] = "regular",
	[31] = "regular",
	[32] = "regular",
};
char const*const orwl_taskdep_dnames[33] = {
	[0] = "orwl_taskdep_shutdown",
	[1] = "orwl_taskdep_shutdown",
	[2] = "orwl_taskdep_shutdown",
	[3] = "orwl_taskdep_shutdown",
	[4] = "orwl_taskdep_shutdown",
	[5] = "orwl_taskdep_shutdown",
	[6] = "orwl_taskdep_shutdown",
	[7] = "orwl_taskdep_shutdown",
	[8] = "orwl_taskdep_shutdown",
	[9] = "orwl_taskdep_shutdown",
	[10] = "orwl_taskdep_shutdown",
	[11] = "orwl_taskdep_shutdown",
	[12] = "orwl_taskdep_shutdown",
	[13] = "orwl_taskdep_shutdown",
	[14] = "orwl_taskdep_shutdown",
	[15] = "orwl_taskdep_shutdown",
	[16] = "orwl_taskdep_shutdown",
	[17] = "orwl_taskdep_shutdown",
	[18] = "orwl_taskdep_shutdown",
	[19] = "orwl_taskdep_shutdown",
	[20] = "orwl_taskdep_shutdown",
	[21] = "orwl_taskdep_shutdown",
	[22] = "orwl_taskdep_shutdown",
	[23] = "orwl_taskdep_shutdown",
	[24] = "orwl_taskdep_shutdown",
	[25] = "orwl_taskdep_shutdown",
	[26] = "orwl_taskdep_shutdown",
	[27] = "orwl_taskdep_shutdown",
	[28] = "orwl_taskdep_shutdown",
	[29] = "orwl_taskdep_shutdown",
	[30] = "orwl_taskdep_shutdown",
	[31] = "orwl_taskdep_shutdown",
	[32] = "orwl_taskdep_shutdown",
};

// Initializers for ports. The table index is the index at the source, since only
// these are guaranteed to be unique. The values here are pointers to const
// qualified compound literals, or 0 if no "init" has been given for the port.
static orwl_taskdep_vector const*const orwl_taskdep_defaults[33][4] = {
	[0] = {
		[0] = 0, // no pre-init for dist_2_1_t
	},
	[1] = {
		[0] = 0, // no pre-init for dist_1_2_l
	},
	[2] = {
		[0] = 0, // no pre-init for dist_2_2_t
	},
	[3] = {
		[0] = &(orwl_taskdep_vector const){ +0 }, // dist_1_1_r
	},
	[4] = {
		[0] = 0, // no pre-init for dist_1_3_l
	},
	[5] = {
		[0] = 0, // no pre-init for dist_2_3_t
	},
	[6] = {
		[0] = &(orwl_taskdep_vector const){ +0 }, // dist_1_2_r
	},
	[7] = {
		[0] = 0, // no pre-init for dist_3_1_t
	},
	[8] = {
		[0] = 0, // no pre-init for dist_2_2_l
	},
	[9] = {
		[0] = &(orwl_taskdep_vector const){ +0 }, // dist_1_1_b
	},
	[10] = {
		[0] = 0, // no pre-init for dist_3_2_t
	},
	[11] = {
		[0] = &(orwl_taskdep_vector const){ +0 }, // dist_2_1_r
	},
	[12] = {
		[0] = 0, // no pre-init for dist_2_3_l
	},
	[13] = {
		[0] = &(orwl_taskdep_vector const){ +0 }, // dist_1_2_b
	},
	[14] = {
		[0] = 0, // no pre-init for dist_3_3_t
	},
	[15] = {
		[0] = &(orwl_taskdep_vector const){ +0 }, // dist_2_2_r
	},
	[16] = {
		[0] = &(orwl_taskdep_vector const){ +0 }, // dist_1_3_b
	},
	[17] = {
		[0] = 0, // no pre-init for dist_3_2_l
	},
	[18] = {
		[0] = &(orwl_taskdep_vector const){ +0 }, // dist_2_1_b
	},
	[19] = {
		[0] = &(orwl_taskdep_vector const){ +0 }, // dist_3_1_r
	},
	[20] = {
		[0] = 0, // no pre-init for dist_3_3_l
	},
	[21] = {
		[0] = &(orwl_taskdep_vector const){ +0 }, // dist_2_2_b
	},
	[22] = {
		[0] = &(orwl_taskdep_vector const){ +0 }, // dist_3_2_r
	},
	[23] = {
		[0] = &(orwl_taskdep_vector const){ +0 }, // dist_2_3_b
	},
	[24] = {
		[0] = 0, // no pre-init for out_1_1_b
		[1] = 0, // no pre-init for out_1_1_l
		[2] = 0, // no pre-init for out_1_1_t
		[3] = 0, // no pre-init for out_1_1_r
	},
	[25] = {
		[0] = 0, // no pre-init for out_1_2_b
		[1] = 0, // no pre-init for out_1_2_l
		[2] = 0, // no pre-init for out_1_2_t
		[3] = 0, // no pre-init for out_1_2_r
	},
	[26] = {
		[0] = 0, // no pre-init for out_1_3_b
		[1] = 0, // no pre-init for out_1_3_l
		[2] = 0, // no pre-init for out_1_3_t
		[3] = 0, // no pre-init for out_1_3_r
	},
	[27] = {
		[0] = 0, // no pre-init for out_2_1_b
		[1] = 0, // no pre-init for out_2_1_l
		[2] = 0, // no pre-init for out_2_1_t
		[3] = 0, // no pre-init for out_2_1_r
	},
	[28] = {
		[0] = 0, // no pre-init for out_2_2_b
		[1] = 0, // no pre-init for out_2_2_l
		[2] = 0, // no pre-init for out_2_2_t
		[3] = 0, // no pre-init for out_2_2_r
	},
	[29] = {
		[0] = 0, // no pre-init for out_2_3_b
		[1] = 0, // no pre-init for out_2_3_l
		[2] = 0, // no pre-init for out_2_3_t
		[3] = 0, // no pre-init for out_2_3_r
	},
	[30] = {
		[0] = 0, // no pre-init for out_3_1_b
		[1] = 0, // no pre-init for out_3_1_l
		[2] = 0, // no pre-init for out_3_1_t
		[3] = 0, // no pre-init for out_3_1_r
	},
	[31] = {
		[0] = 0, // no pre-init for out_3_2_b
		[1] = 0, // no pre-init for out_3_2_l
		[2] = 0, // no pre-init for out_3_2_t
		[3] = 0, // no pre-init for out_3_2_r
	},
	[32] = {
		[0] = 0, // no pre-init for out_3_3_b
		[1] = 0, // no pre-init for out_3_3_l
		[2] = 0, // no pre-init for out_3_3_t
		[3] = 0, // no pre-init for out_3_3_r
	},
};

// The const property for ports. The table index is the index at the source, since only
// these are guaranteed to be unique. .
static bool const orwl_taskdep_const[33][4] = {
	[0] = {
		[0] = 0, // dist_2_1_t
	},
	[1] = {
		[0] = 0, // dist_1_2_l
	},
	[2] = {
		[0] = 0, // dist_2_2_t
	},
	[3] = {
		[0] = 0, // dist_1_1_r
	},
	[4] = {
		[0] = 0, // dist_1_3_l
	},
	[5] = {
		[0] = 0, // dist_2_3_t
	},
	[6] = {
		[0] = 0, // dist_1_2_r
	},
	[7] = {
		[0] = 0, // dist_3_1_t
	},
	[8] = {
		[0] = 0, // dist_2_2_l
	},
	[9] = {
		[0] = 0, // dist_1_1_b
	},
	[10] = {
		[0] = 0, // dist_3_2_t
	},
	[11] = {
		[0] = 0, // dist_2_1_r
	},
	[12] = {
		[0] = 0, // dist_2_3_l
	},
	[13] = {
		[0] = 0, // dist_1_2_b
	},
	[14] = {
		[0] = 0, // dist_3_3_t
	},
	[15] = {
		[0] = 0, // dist_2_2_r
	},
	[16] = {
		[0] = 0, // dist_1_3_b
	},
	[17] = {
		[0] = 0, // dist_3_2_l
	},
	[18] = {
		[0] = 0, // dist_2_1_b
	},
	[19] = {
		[0] = 0, // dist_3_1_r
	},
	[20] = {
		[0] = 0, // dist_3_3_l
	},
	[21] = {
		[0] = 0, // dist_2_2_b
	},
	[22] = {
		[0] = 0, // dist_3_2_r
	},
	[23] = {
		[0] = 0, // dist_2_3_b
	},
	[24] = {
		[0] = 0, // out_1_1_b
		[1] = 0, // out_1_1_l
		[2] = 0, // out_1_1_t
		[3] = 0, // out_1_1_r
	},
	[25] = {
		[0] = 0, // out_1_2_b
		[1] = 0, // out_1_2_l
		[2] = 0, // out_1_2_t
		[3] = 0, // out_1_2_r
	},
	[26] = {
		[0] = 0, // out_1_3_b
		[1] = 0, // out_1_3_l
		[2] = 0, // out_1_3_t
		[3] = 0, // out_1_3_r
	},
	[27] = {
		[0] = 0, // out_2_1_b
		[1] = 0, // out_2_1_l
		[2] = 0, // out_2_1_t
		[3] = 0, // out_2_1_r
	},
	[28] = {
		[0] = 0, // out_2_2_b
		[1] = 0, // out_2_2_l
		[2] = 0, // out_2_2_t
		[3] = 0, // out_2_2_r
	},
	[29] = {
		[0] = 0, // out_2_3_b
		[1] = 0, // out_2_3_l
		[2] = 0, // out_2_3_t
		[3] = 0, // out_2_3_r
	},
	[30] = {
		[0] = 0, // out_3_1_b
		[1] = 0, // out_3_1_l
		[2] = 0, // out_3_1_t
		[3] = 0, // out_3_1_r
	},
	[31] = {
		[0] = 0, // out_3_2_b
		[1] = 0, // out_3_2_l
		[2] = 0, // out_3_2_t
		[3] = 0, // out_3_2_r
	},
	[32] = {
		[0] = 0, // out_3_3_b
		[1] = 0, // out_3_3_l
		[2] = 0, // out_3_3_t
		[3] = 0, // out_3_3_r
	},
};

// For each output port, provide the textual information about the link
char const*const orwl_taskdep_oports[33][4] = {
	[0] = {
		[0] = "dist_2_1_t",
	},
	[1] = {
		[0] = "dist_1_2_l",
	},
	[2] = {
		[0] = "dist_2_2_t",
	},
	[3] = {
		[0] = "dist_1_1_r",
	},
	[4] = {
		[0] = "dist_1_3_l",
	},
	[5] = {
		[0] = "dist_2_3_t",
	},
	[6] = {
		[0] = "dist_1_2_r",
	},
	[7] = {
		[0] = "dist_3_1_t",
	},
	[8] = {
		[0] = "dist_2_2_l",
	},
	[9] = {
		[0] = "dist_1_1_b",
	},
	[10] = {
		[0] = "dist_3_2_t",
	},
	[11] = {
		[0] = "dist_2_1_r",
	},
	[12] = {
		[0] = "dist_2_3_l",
	},
	[13] = {
		[0] = "dist_1_2_b",
	},
	[14] = {
		[0] = "dist_3_3_t",
	},
	[15] = {
		[0] = "dist_2_2_r",
	},
	[16] = {
		[0] = "dist_1_3_b",
	},
	[17] = {
		[0] = "dist_3_2_l",
	},
	[18] = {
		[0] = "dist_2_1_b",
	},
	[19] = {
		[0] = "dist_3_1_r",
	},
	[20] = {
		[0] = "dist_3_3_l",
	},
	[21] = {
		[0] = "dist_2_2_b",
	},
	[22] = {
		[0] = "dist_3_2_r",
	},
	[23] = {
		[0] = "dist_2_3_b",
	},
	[24] = {
		[0] = "out_1_1_b",
		[1] = "out_1_1_l",
		[2] = "out_1_1_t",
		[3] = "out_1_1_r",
	},
	[25] = {
		[0] = "out_1_2_b",
		[1] = "out_1_2_l",
		[2] = "out_1_2_t",
		[3] = "out_1_2_r",
	},
	[26] = {
		[0] = "out_1_3_b",
		[1] = "out_1_3_l",
		[2] = "out_1_3_t",
		[3] = "out_1_3_r",
	},
	[27] = {
		[0] = "out_2_1_b",
		[1] = "out_2_1_l",
		[2] = "out_2_1_t",
		[3] = "out_2_1_r",
	},
	[28] = {
		[0] = "out_2_2_b",
		[1] = "out_2_2_l",
		[2] = "out_2_2_t",
		[3] = "out_2_2_r",
	},
	[29] = {
		[0] = "out_2_3_b",
		[1] = "out_2_3_l",
		[2] = "out_2_3_t",
		[3] = "out_2_3_r",
	},
	[30] = {
		[0] = "out_3_1_b",
		[1] = "out_3_1_l",
		[2] = "out_3_1_t",
		[3] = "out_3_1_r",
	},
	[31] = {
		[0] = "out_3_2_b",
		[1] = "out_3_2_l",
		[2] = "out_3_2_t",
		[3] = "out_3_2_r",
	},
	[32] = {
		[0] = "out_3_3_b",
		[1] = "out_3_3_l",
		[2] = "out_3_3_t",
		[3] = "out_3_3_r",
	},
};

// For each input port, provide the textual information about the link
char const*const orwl_taskdep_iports[33][4] = {
	[0] = {
		[0] = "out_1_1_b",
	},
	[1] = {
		[0] = "out_1_1_r",
	},
	[2] = {
		[0] = "out_1_2_b",
	},
	[3] = {
		[0] = "out_1_2_l",
	},
	[4] = {
		[0] = "out_1_2_r",
	},
	[5] = {
		[0] = "out_1_3_b",
	},
	[6] = {
		[0] = "out_1_3_l",
	},
	[7] = {
		[0] = "out_2_1_b",
	},
	[8] = {
		[0] = "out_2_1_r",
	},
	[9] = {
		[0] = "out_2_1_t",
	},
	[10] = {
		[0] = "out_2_2_b",
	},
	[11] = {
		[0] = "out_2_2_l",
	},
	[12] = {
		[0] = "out_2_2_r",
	},
	[13] = {
		[0] = "out_2_2_t",
	},
	[14] = {
		[0] = "out_2_3_b",
	},
	[15] = {
		[0] = "out_2_3_l",
	},
	[16] = {
		[0] = "out_2_3_t",
	},
	[17] = {
		[0] = "out_3_1_r",
	},
	[18] = {
		[0] = "out_3_1_t",
	},
	[19] = {
		[0] = "out_3_2_l",
	},
	[20] = {
		[0] = "out_3_2_r",
	},
	[21] = {
		[0] = "out_3_2_t",
	},
	[22] = {
		[0] = "out_3_3_l",
	},
	[23] = {
		[0] = "out_3_3_t",
	},
	[24] = {
		[0] = "dist_1_1_b",
		[1] = "dist_1_1_l",
		[2] = "dist_1_1_t",
		[3] = "dist_1_1_r",
	},
	[25] = {
		[0] = "dist_1_2_b",
		[1] = "dist_1_2_l",
		[2] = "dist_1_2_t",
		[3] = "dist_1_2_r",
	},
	[26] = {
		[0] = "dist_1_3_b",
		[1] = "dist_1_3_l",
		[2] = "dist_1_3_t",
		[3] = "dist_1_3_r",
	},
	[27] = {
		[0] = "dist_2_1_b",
		[1] = "dist_2_1_l",
		[2] = "dist_2_1_t",
		[3] = "dist_2_1_r",
	},
	[28] = {
		[0] = "dist_2_2_b",
		[1] = "dist_2_2_l",
		[2] = "dist_2_2_t",
		[3] = "dist_2_2_r",
	},
	[29] = {
		[0] = "dist_2_3_b",
		[1] = "dist_2_3_l",
		[2] = "dist_2_3_t",
		[3] = "dist_2_3_r",
	},
	[30] = {
		[0] = "dist_3_1_b",
		[1] = "dist_3_1_l",
		[2] = "dist_3_1_t",
		[3] = "dist_3_1_r",
	},
	[31] = {
		[0] = "dist_3_2_b",
		[1] = "dist_3_2_l",
		[2] = "dist_3_2_t",
		[3] = "dist_3_2_r",
	},
	[32] = {
		[0] = "dist_3_3_b",
		[1] = "dist_3_3_l",
		[2] = "dist_3_3_t",
		[3] = "dist_3_3_r",
	},
};

// The outdegree for each node
_Alignas(32) int64_t const orwl_taskdep_deg_out[33] = {
	[0] = 1,
	[1] = 1,
	[2] = 1,
	[3] = 1,
	[4] = 1,
	[5] = 1,
	[6] = 1,
	[7] = 1,
	[8] = 1,
	[9] = 1,
	[10] = 1,
	[11] = 1,
	[12] = 1,
	[13] = 1,
	[14] = 1,
	[15] = 1,
	[16] = 1,
	[17] = 1,
	[18] = 1,
	[19] = 1,
	[20] = 1,
	[21] = 1,
	[22] = 1,
	[23] = 1,
	[24] = 4,
	[25] = 4,
	[26] = 4,
	[27] = 4,
	[28] = 4,
	[29] = 4,
	[30] = 4,
	[31] = 4,
	[32] = 4,
};

// The indegree for each node
_Alignas(32) int64_t const orwl_taskdep_deg_inp[33] = {
	[0] = 1,
	[1] = 1,
	[2] = 1,
	[3] = 1,
	[4] = 1,
	[5] = 1,
	[6] = 1,
	[7] = 1,
	[8] = 1,
	[9] = 1,
	[10] = 1,
	[11] = 1,
	[12] = 1,
	[13] = 1,
	[14] = 1,
	[15] = 1,
	[16] = 1,
	[17] = 1,
	[18] = 1,
	[19] = 1,
	[20] = 1,
	[21] = 1,
	[22] = 1,
	[23] = 1,
	[24] = 4,
	[25] = 4,
	[26] = 4,
	[27] = 4,
	[28] = 4,
	[29] = 4,
	[30] = 4,
	[31] = 4,
	[32] = 4,
};

// The number of each output port, or -1 if not this number is not available
int64_t const*const orwl_taskdep_onum[33] = {
	[0] = (int64_t const[4]){
		[0] = 2,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[1] = (int64_t const[4]){
		[0] = 1,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[2] = (int64_t const[4]){
		[0] = 2,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[3] = (int64_t const[4]){
		[0] = 1,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[4] = (int64_t const[4]){
		[0] = 1,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[5] = (int64_t const[4]){
		[0] = 2,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[6] = (int64_t const[4]){
		[0] = 1,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[7] = (int64_t const[4]){
		[0] = 3,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[8] = (int64_t const[4]){
		[0] = 2,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[9] = (int64_t const[4]){
		[0] = 1,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[10] = (int64_t const[4]){
		[0] = 3,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[11] = (int64_t const[4]){
		[0] = 2,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[12] = (int64_t const[4]){
		[0] = 2,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[13] = (int64_t const[4]){
		[0] = 1,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[14] = (int64_t const[4]){
		[0] = 3,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[15] = (int64_t const[4]){
		[0] = 2,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[16] = (int64_t const[4]){
		[0] = 1,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[17] = (int64_t const[4]){
		[0] = 3,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[18] = (int64_t const[4]){
		[0] = 2,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[19] = (int64_t const[4]){
		[0] = 3,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[20] = (int64_t const[4]){
		[0] = 3,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[21] = (int64_t const[4]){
		[0] = 2,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[22] = (int64_t const[4]){
		[0] = 3,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[23] = (int64_t const[4]){
		[0] = 2,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[24] = (int64_t const[4]){
		[0] = 1,
		[1] = 1,
		[2] = 1,
		[3] = 1,
	},
	[25] = (int64_t const[4]){
		[0] = 1,
		[1] = 1,
		[2] = 1,
		[3] = 1,
	},
	[26] = (int64_t const[4]){
		[0] = 1,
		[1] = 1,
		[2] = 1,
		[3] = 1,
	},
	[27] = (int64_t const[4]){
		[0] = 2,
		[1] = 2,
		[2] = 2,
		[3] = 2,
	},
	[28] = (int64_t const[4]){
		[0] = 2,
		[1] = 2,
		[2] = 2,
		[3] = 2,
	},
	[29] = (int64_t const[4]){
		[0] = 2,
		[1] = 2,
		[2] = 2,
		[3] = 2,
	},
	[30] = (int64_t const[4]){
		[0] = 3,
		[1] = 3,
		[2] = 3,
		[3] = 3,
	},
	[31] = (int64_t const[4]){
		[0] = 3,
		[1] = 3,
		[2] = 3,
		[3] = 3,
	},
	[32] = (int64_t const[4]){
		[0] = 3,
		[1] = 3,
		[2] = 3,
		[3] = 3,
	},
};

// The number of each input port, or -1 if not this number is not available
int64_t const*const orwl_taskdep_inum[33] = {
	[0] = (int64_t const[4]){
		[0] = 1,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[1] = (int64_t const[4]){
		[0] = 1,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[2] = (int64_t const[4]){
		[0] = 2,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[3] = (int64_t const[4]){
		[0] = 2,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[4] = (int64_t const[4]){
		[0] = 2,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[5] = (int64_t const[4]){
		[0] = 3,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[6] = (int64_t const[4]){
		[0] = 3,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[7] = (int64_t const[4]){
		[0] = 1,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[8] = (int64_t const[4]){
		[0] = 1,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[9] = (int64_t const[4]){
		[0] = 1,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[10] = (int64_t const[4]){
		[0] = 2,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[11] = (int64_t const[4]){
		[0] = 2,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[12] = (int64_t const[4]){
		[0] = 2,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[13] = (int64_t const[4]){
		[0] = 2,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[14] = (int64_t const[4]){
		[0] = 3,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[15] = (int64_t const[4]){
		[0] = 3,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[16] = (int64_t const[4]){
		[0] = 3,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[17] = (int64_t const[4]){
		[0] = 1,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[18] = (int64_t const[4]){
		[0] = 1,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[19] = (int64_t const[4]){
		[0] = 2,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[20] = (int64_t const[4]){
		[0] = 2,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[21] = (int64_t const[4]){
		[0] = 2,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[22] = (int64_t const[4]){
		[0] = 3,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[23] = (int64_t const[4]){
		[0] = 3,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[24] = (int64_t const[4]){
		[0] = 1,
		[1] = 1,
		[2] = 1,
		[3] = 1,
	},
	[25] = (int64_t const[4]){
		[0] = 2,
		[1] = 2,
		[2] = 2,
		[3] = 2,
	},
	[26] = (int64_t const[4]){
		[0] = 3,
		[1] = 3,
		[2] = 3,
		[3] = 3,
	},
	[27] = (int64_t const[4]){
		[0] = 1,
		[1] = 1,
		[2] = 1,
		[3] = 1,
	},
	[28] = (int64_t const[4]){
		[0] = 2,
		[1] = 2,
		[2] = 2,
		[3] = 2,
	},
	[29] = (int64_t const[4]){
		[0] = 3,
		[1] = 3,
		[2] = 3,
		[3] = 3,
	},
	[30] = (int64_t const[4]){
		[0] = 1,
		[1] = 1,
		[2] = 1,
		[3] = 1,
	},
	[31] = (int64_t const[4]){
		[0] = 2,
		[1] = 2,
		[2] = 2,
		[3] = 2,
	},
	[32] = (int64_t const[4]){
		[0] = 3,
		[1] = 3,
		[2] = 3,
		[3] = 3,
	},
};

// For each input port, provide the source port
static int64_t const orwl_taskdep_tail[33][4] = {
	[0] = {
		[0] = 96,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[1] = {
		[0] = 99,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[2] = {
		[0] = 100,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[3] = {
		[0] = 101,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[4] = {
		[0] = 103,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[5] = {
		[0] = 104,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[6] = {
		[0] = 105,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[7] = {
		[0] = 108,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[8] = {
		[0] = 111,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[9] = {
		[0] = 110,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[10] = {
		[0] = 112,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[11] = {
		[0] = 113,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[12] = {
		[0] = 115,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[13] = {
		[0] = 114,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[14] = {
		[0] = 116,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[15] = {
		[0] = 117,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[16] = {
		[0] = 118,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[17] = {
		[0] = 123,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[18] = {
		[0] = 122,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[19] = {
		[0] = 125,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[20] = {
		[0] = 127,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[21] = {
		[0] = 126,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[22] = {
		[0] = 129,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[23] = {
		[0] = 130,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[24] = {
		[0] = 36,
		[1] = -1,
		[2] = -1,
		[3] = 12,
	},
	[25] = {
		[0] = 52,
		[1] = 4,
		[2] = -1,
		[3] = 24,
	},
	[26] = {
		[0] = 64,
		[1] = 16,
		[2] = -1,
		[3] = -1,
	},
	[27] = {
		[0] = 72,
		[1] = -1,
		[2] = 0,
		[3] = 44,
	},
	[28] = {
		[0] = 84,
		[1] = 32,
		[2] = 8,
		[3] = 60,
	},
	[29] = {
		[0] = 92,
		[1] = 48,
		[2] = 20,
		[3] = -1,
	},
	[30] = {
		[0] = -1,
		[1] = -1,
		[2] = 28,
		[3] = 76,
	},
	[31] = {
		[0] = -1,
		[1] = 68,
		[2] = 40,
		[3] = 88,
	},
	[32] = {
		[0] = -1,
		[1] = 80,
		[2] = 56,
		[3] = -1,
	},
};

void orwl_taskdep_usage(void) {
    printf("task number\tname\tfunction\tstartup\tshutdown\t(in ports)\t(out ports)\n");
    for (size_t i = 0; i < 33; ++i) {
        printf("%zu\t\t%s\t%s\t%s\t%s", i, orwl_taskdep_nnames[i], orwl_taskdep_fnames[i], orwl_taskdep_snames[i], orwl_taskdep_dnames[i]);
        if (orwl_taskdep_iports[i][0]) {
            printf("\t(%s", orwl_taskdep_iports[i][0]);
            for (size_t j = 1; j < 4; ++j) {
                if (orwl_taskdep_iports[i][j])
                   printf(", %s", orwl_taskdep_iports[i][j]);
            }
            printf(")");
        } else {
            printf("	()");
        }
        if (orwl_taskdep_oports[i][0]) {
            printf("\t(%s", orwl_taskdep_oports[i][0]);
            for (size_t j = 1; j < 4; ++j) {
                if (orwl_taskdep_oports[i][j])
                   printf(", %s", orwl_taskdep_oports[i][j]);
            }
            printf(")");
        } else {
            printf("	()");
        }
        printf("\n");
    }

}

P99_GETOPT_CALLBACK(orwl_taskdep_usage);

// *****************************************************
// Instantiate the ORWL structure.
ORWL_LOCATIONS_PER_TASK(
	orwl_task_write_if0,
	orwl_task_read_if0,
	orwl_task_write_if1,
	orwl_task_read_if1,
	orwl_task_write_if2,
	orwl_task_read_if2,
	orwl_task_write_if3,
	orwl_task_read_if3
);

ORWL_LOCATIONS_PER_TASK_INSTANTIATION();

ORWL_DECLARE_TASK(orwl_taskdep_type);

// *****************************************************
// Set up the ORWL structure that reflects dependencies.

// This graph is iterated.
// Using orwl_handle2.
// buf_1_1_b	→ node_2_1	(dist_2_1_t)
// 0	→ 27
// priorities: 0 1
// buf_1_1_r	→ node_1_2	(dist_1_2_l)
// 1	→ 25
// priorities: 0 1
// buf_1_2_b	→ node_2_2	(dist_2_2_t)
// 2	→ 28
// priorities: 0 1
// buf_1_2_l	→ node_1_1	(dist_1_1_r)
// 3	→ 24
// priorities: 1 0
// buf_1_2_r	→ node_1_3	(dist_1_3_l)
// 4	→ 26
// priorities: 0 1
// buf_1_3_b	→ node_2_3	(dist_2_3_t)
// 5	→ 29
// priorities: 0 1
// buf_1_3_l	→ node_1_2	(dist_1_2_r)
// 6	→ 25
// priorities: 1 0
// buf_2_1_b	→ node_3_1	(dist_3_1_t)
// 7	→ 30
// priorities: 0 1
// buf_2_1_r	→ node_2_2	(dist_2_2_l)
// 8	→ 28
// priorities: 0 1
// buf_2_1_t	→ node_1_1	(dist_1_1_b)
// 9	→ 24
// priorities: 1 0
// buf_2_2_b	→ node_3_2	(dist_3_2_t)
// 10	→ 31
// priorities: 0 1
// buf_2_2_l	→ node_2_1	(dist_2_1_r)
// 11	→ 27
// priorities: 1 0
// buf_2_2_r	→ node_2_3	(dist_2_3_l)
// 12	→ 29
// priorities: 0 1
// buf_2_2_t	→ node_1_2	(dist_1_2_b)
// 13	→ 25
// priorities: 1 0
// buf_2_3_b	→ node_3_3	(dist_3_3_t)
// 14	→ 32
// priorities: 0 1
// buf_2_3_l	→ node_2_2	(dist_2_2_r)
// 15	→ 28
// priorities: 1 0
// buf_2_3_t	→ node_1_3	(dist_1_3_b)
// 16	→ 26
// priorities: 1 0
// buf_3_1_r	→ node_3_2	(dist_3_2_l)
// 17	→ 31
// priorities: 0 1
// buf_3_1_t	→ node_2_1	(dist_2_1_b)
// 18	→ 27
// priorities: 1 0
// buf_3_2_l	→ node_3_1	(dist_3_1_r)
// 19	→ 30
// priorities: 1 0
// buf_3_2_r	→ node_3_3	(dist_3_3_l)
// 20	→ 32
// priorities: 0 1
// buf_3_2_t	→ node_2_2	(dist_2_2_b)
// 21	→ 28
// priorities: 1 0
// buf_3_3_l	→ node_3_2	(dist_3_2_r)
// 22	→ 31
// priorities: 1 0
// buf_3_3_t	→ node_2_3	(dist_2_3_b)
// 23	→ 29
// priorities: 1 0
// node_1_1	→ buf_1_1_b	(out_1_1_b)
// 24	→ 0
// priorities: 0 1
// node_1_1	→ buf_1_1_r	(out_1_1_r)
// 24	→ 1
// priorities: 0 1
// node_1_2	→ buf_1_2_b	(out_1_2_b)
// 25	→ 2
// priorities: 0 1
// node_1_2	→ buf_1_2_l	(out_1_2_l)
// 25	→ 3
// priorities: 0 1
// node_1_2	→ buf_1_2_r	(out_1_2_r)
// 25	→ 4
// priorities: 0 1
// node_1_3	→ buf_1_3_b	(out_1_3_b)
// 26	→ 5
// priorities: 0 1
// node_1_3	→ buf_1_3_l	(out_1_3_l)
// 26	→ 6
// priorities: 0 1
// node_2_1	→ buf_2_1_b	(out_2_1_b)
// 27	→ 7
// priorities: 0 1
// node_2_1	→ buf_2_1_r	(out_2_1_r)
// 27	→ 8
// priorities: 0 1
// node_2_1	→ buf_2_1_t	(out_2_1_t)
// 27	→ 9
// priorities: 0 1
// node_2_2	→ buf_2_2_b	(out_2_2_b)
// 28	→ 10
// priorities: 0 1
// node_2_2	→ buf_2_2_l	(out_2_2_l)
// 28	→ 11
// priorities: 0 1
// node_2_2	→ buf_2_2_r	(out_2_2_r)
// 28	→ 12
// priorities: 0 1
// node_2_2	→ buf_2_2_t	(out_2_2_t)
// 28	→ 13
// priorities: 0 1
// node_2_3	→ buf_2_3_b	(out_2_3_b)
// 29	→ 14
// priorities: 0 1
// node_2_3	→ buf_2_3_l	(out_2_3_l)
// 29	→ 15
// priorities: 0 1
// node_2_3	→ buf_2_3_t	(out_2_3_t)
// 29	→ 16
// priorities: 0 1
// node_3_1	→ buf_3_1_r	(out_3_1_r)
// 30	→ 17
// priorities: 0 1
// node_3_1	→ buf_3_1_t	(out_3_1_t)
// 30	→ 18
// priorities: 0 1
// node_3_2	→ buf_3_2_l	(out_3_2_l)
// 31	→ 19
// priorities: 0 1
// node_3_2	→ buf_3_2_r	(out_3_2_r)
// 31	→ 20
// priorities: 0 1
// node_3_2	→ buf_3_2_t	(out_3_2_t)
// 31	→ 21
// priorities: 0 1
// node_3_3	→ buf_3_3_l	(out_3_3_l)
// 32	→ 22
// priorities: 0 1
// node_3_3	→ buf_3_3_t	(out_3_3_t)
// 32	→ 23
// priorities: 0 1

// ****************************************************************
// Per task specific initialization functions. They should ease some
// const propagation concerning the task ID.
static void orwl_taskdep_init_0(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "0, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(0, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(0, orwl_task_write_if0), ORWL_LOCATION(0, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(24, orwl_task_read_if0), 1, seed, server);

	report(orwl_taskdep_verbose, "0, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "0, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_1(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "1, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(1, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(1, orwl_task_write_if0), ORWL_LOCATION(1, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(24, orwl_task_read_if3), 1, seed, server);

	report(orwl_taskdep_verbose, "1, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "1, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_2(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "2, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(2, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(2, orwl_task_write_if0), ORWL_LOCATION(2, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(25, orwl_task_read_if0), 1, seed, server);

	report(orwl_taskdep_verbose, "2, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "2, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_3(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "3, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(3, orwl_task_write_if0), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(3, orwl_task_write_if0), ORWL_LOCATION(3, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(25, orwl_task_read_if1), 1, seed, server);

	report(orwl_taskdep_verbose, "3, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "3, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_4(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "4, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(4, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(4, orwl_task_write_if0), ORWL_LOCATION(4, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(25, orwl_task_read_if3), 1, seed, server);

	report(orwl_taskdep_verbose, "4, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "4, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_5(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "5, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(5, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(5, orwl_task_write_if0), ORWL_LOCATION(5, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(26, orwl_task_read_if0), 1, seed, server);

	report(orwl_taskdep_verbose, "5, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "5, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_6(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "6, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(6, orwl_task_write_if0), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(6, orwl_task_write_if0), ORWL_LOCATION(6, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(26, orwl_task_read_if1), 1, seed, server);

	report(orwl_taskdep_verbose, "6, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "6, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_7(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "7, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(7, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(7, orwl_task_write_if0), ORWL_LOCATION(7, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(27, orwl_task_read_if0), 1, seed, server);

	report(orwl_taskdep_verbose, "7, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "7, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_8(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "8, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(8, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(8, orwl_task_write_if0), ORWL_LOCATION(8, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(27, orwl_task_read_if3), 1, seed, server);

	report(orwl_taskdep_verbose, "8, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "8, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_9(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "9, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(9, orwl_task_write_if0), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(9, orwl_task_write_if0), ORWL_LOCATION(9, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(27, orwl_task_read_if2), 1, seed, server);

	report(orwl_taskdep_verbose, "9, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "9, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_10(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "10, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(10, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(10, orwl_task_write_if0), ORWL_LOCATION(10, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(28, orwl_task_read_if0), 1, seed, server);

	report(orwl_taskdep_verbose, "10, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "10, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_11(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "11, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(11, orwl_task_write_if0), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(11, orwl_task_write_if0), ORWL_LOCATION(11, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(28, orwl_task_read_if1), 1, seed, server);

	report(orwl_taskdep_verbose, "11, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "11, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_12(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "12, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(12, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(12, orwl_task_write_if0), ORWL_LOCATION(12, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(28, orwl_task_read_if3), 1, seed, server);

	report(orwl_taskdep_verbose, "12, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "12, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_13(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "13, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(13, orwl_task_write_if0), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(13, orwl_task_write_if0), ORWL_LOCATION(13, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(28, orwl_task_read_if2), 1, seed, server);

	report(orwl_taskdep_verbose, "13, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "13, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_14(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "14, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(14, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(14, orwl_task_write_if0), ORWL_LOCATION(14, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(29, orwl_task_read_if0), 1, seed, server);

	report(orwl_taskdep_verbose, "14, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "14, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_15(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "15, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(15, orwl_task_write_if0), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(15, orwl_task_write_if0), ORWL_LOCATION(15, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(29, orwl_task_read_if1), 1, seed, server);

	report(orwl_taskdep_verbose, "15, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "15, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_16(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "16, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(16, orwl_task_write_if0), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(16, orwl_task_write_if0), ORWL_LOCATION(16, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(29, orwl_task_read_if2), 1, seed, server);

	report(orwl_taskdep_verbose, "16, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "16, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_17(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "17, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(17, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(17, orwl_task_write_if0), ORWL_LOCATION(17, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(30, orwl_task_read_if3), 1, seed, server);

	report(orwl_taskdep_verbose, "17, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "17, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_18(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "18, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(18, orwl_task_write_if0), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(18, orwl_task_write_if0), ORWL_LOCATION(18, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(30, orwl_task_read_if2), 1, seed, server);

	report(orwl_taskdep_verbose, "18, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "18, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_19(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "19, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(19, orwl_task_write_if0), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(19, orwl_task_write_if0), ORWL_LOCATION(19, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(31, orwl_task_read_if1), 1, seed, server);

	report(orwl_taskdep_verbose, "19, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "19, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_20(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "20, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(20, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(20, orwl_task_write_if0), ORWL_LOCATION(20, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(31, orwl_task_read_if3), 1, seed, server);

	report(orwl_taskdep_verbose, "20, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "20, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_21(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "21, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(21, orwl_task_write_if0), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(21, orwl_task_write_if0), ORWL_LOCATION(21, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(31, orwl_task_read_if2), 1, seed, server);

	report(orwl_taskdep_verbose, "21, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "21, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_22(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "22, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(22, orwl_task_write_if0), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(22, orwl_task_write_if0), ORWL_LOCATION(22, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(32, orwl_task_read_if1), 1, seed, server);

	report(orwl_taskdep_verbose, "22, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "22, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_23(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "23, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(23, orwl_task_write_if0), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(23, orwl_task_write_if0), ORWL_LOCATION(23, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(32, orwl_task_read_if2), 1, seed, server);

	report(orwl_taskdep_verbose, "23, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "23, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_24(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "24, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(24, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(24, orwl_task_write_if0), ORWL_LOCATION(24, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(24, orwl_task_write_if1), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(24, orwl_task_write_if1), ORWL_LOCATION(24, orwl_task_read_if1), 10);
	orwl_write_insert(&hdl_out[2], ORWL_LOCATION(24, orwl_task_write_if2), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(24, orwl_task_write_if2), ORWL_LOCATION(24, orwl_task_read_if2), 10);
	orwl_write_insert(&hdl_out[3], ORWL_LOCATION(24, orwl_task_write_if3), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(24, orwl_task_write_if3), ORWL_LOCATION(24, orwl_task_read_if3), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(9, orwl_task_read_if0), 0, seed, server);
	// const port at 1
	// const port at 2
	orwl_read_insert(&hdl_in[3], ORWL_LOCATION(3, orwl_task_read_if0), 0, seed, server);

	report(orwl_taskdep_verbose, "24, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "24, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_25(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "25, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(25, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(25, orwl_task_write_if0), ORWL_LOCATION(25, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(25, orwl_task_write_if1), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(25, orwl_task_write_if1), ORWL_LOCATION(25, orwl_task_read_if1), 10);
	orwl_write_insert(&hdl_out[2], ORWL_LOCATION(25, orwl_task_write_if2), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(25, orwl_task_write_if2), ORWL_LOCATION(25, orwl_task_read_if2), 10);
	orwl_write_insert(&hdl_out[3], ORWL_LOCATION(25, orwl_task_write_if3), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(25, orwl_task_write_if3), ORWL_LOCATION(25, orwl_task_read_if3), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(13, orwl_task_read_if0), 0, seed, server);
	orwl_read_insert(&hdl_in[1], ORWL_LOCATION(1, orwl_task_read_if0), 1, seed, server);
	// const port at 2
	orwl_read_insert(&hdl_in[3], ORWL_LOCATION(6, orwl_task_read_if0), 0, seed, server);

	report(orwl_taskdep_verbose, "25, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "25, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_26(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "26, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(26, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(26, orwl_task_write_if0), ORWL_LOCATION(26, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(26, orwl_task_write_if1), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(26, orwl_task_write_if1), ORWL_LOCATION(26, orwl_task_read_if1), 10);
	orwl_write_insert(&hdl_out[2], ORWL_LOCATION(26, orwl_task_write_if2), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(26, orwl_task_write_if2), ORWL_LOCATION(26, orwl_task_read_if2), 10);
	orwl_write_insert(&hdl_out[3], ORWL_LOCATION(26, orwl_task_write_if3), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(26, orwl_task_write_if3), ORWL_LOCATION(26, orwl_task_read_if3), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(16, orwl_task_read_if0), 0, seed, server);
	orwl_read_insert(&hdl_in[1], ORWL_LOCATION(4, orwl_task_read_if0), 1, seed, server);
	// const port at 2
	// const port at 3

	report(orwl_taskdep_verbose, "26, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "26, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_27(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "27, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(27, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(27, orwl_task_write_if0), ORWL_LOCATION(27, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(27, orwl_task_write_if1), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(27, orwl_task_write_if1), ORWL_LOCATION(27, orwl_task_read_if1), 10);
	orwl_write_insert(&hdl_out[2], ORWL_LOCATION(27, orwl_task_write_if2), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(27, orwl_task_write_if2), ORWL_LOCATION(27, orwl_task_read_if2), 10);
	orwl_write_insert(&hdl_out[3], ORWL_LOCATION(27, orwl_task_write_if3), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(27, orwl_task_write_if3), ORWL_LOCATION(27, orwl_task_read_if3), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(18, orwl_task_read_if0), 0, seed, server);
	// const port at 1
	orwl_read_insert(&hdl_in[2], ORWL_LOCATION(0, orwl_task_read_if0), 1, seed, server);
	orwl_read_insert(&hdl_in[3], ORWL_LOCATION(11, orwl_task_read_if0), 0, seed, server);

	report(orwl_taskdep_verbose, "27, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "27, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_28(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "28, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(28, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(28, orwl_task_write_if0), ORWL_LOCATION(28, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(28, orwl_task_write_if1), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(28, orwl_task_write_if1), ORWL_LOCATION(28, orwl_task_read_if1), 10);
	orwl_write_insert(&hdl_out[2], ORWL_LOCATION(28, orwl_task_write_if2), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(28, orwl_task_write_if2), ORWL_LOCATION(28, orwl_task_read_if2), 10);
	orwl_write_insert(&hdl_out[3], ORWL_LOCATION(28, orwl_task_write_if3), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(28, orwl_task_write_if3), ORWL_LOCATION(28, orwl_task_read_if3), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(21, orwl_task_read_if0), 0, seed, server);
	orwl_read_insert(&hdl_in[1], ORWL_LOCATION(8, orwl_task_read_if0), 1, seed, server);
	orwl_read_insert(&hdl_in[2], ORWL_LOCATION(2, orwl_task_read_if0), 1, seed, server);
	orwl_read_insert(&hdl_in[3], ORWL_LOCATION(15, orwl_task_read_if0), 0, seed, server);

	report(orwl_taskdep_verbose, "28, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "28, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_29(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "29, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(29, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(29, orwl_task_write_if0), ORWL_LOCATION(29, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(29, orwl_task_write_if1), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(29, orwl_task_write_if1), ORWL_LOCATION(29, orwl_task_read_if1), 10);
	orwl_write_insert(&hdl_out[2], ORWL_LOCATION(29, orwl_task_write_if2), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(29, orwl_task_write_if2), ORWL_LOCATION(29, orwl_task_read_if2), 10);
	orwl_write_insert(&hdl_out[3], ORWL_LOCATION(29, orwl_task_write_if3), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(29, orwl_task_write_if3), ORWL_LOCATION(29, orwl_task_read_if3), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(23, orwl_task_read_if0), 0, seed, server);
	orwl_read_insert(&hdl_in[1], ORWL_LOCATION(12, orwl_task_read_if0), 1, seed, server);
	orwl_read_insert(&hdl_in[2], ORWL_LOCATION(5, orwl_task_read_if0), 1, seed, server);
	// const port at 3

	report(orwl_taskdep_verbose, "29, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "29, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_30(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "30, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(30, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(30, orwl_task_write_if0), ORWL_LOCATION(30, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(30, orwl_task_write_if1), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(30, orwl_task_write_if1), ORWL_LOCATION(30, orwl_task_read_if1), 10);
	orwl_write_insert(&hdl_out[2], ORWL_LOCATION(30, orwl_task_write_if2), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(30, orwl_task_write_if2), ORWL_LOCATION(30, orwl_task_read_if2), 10);
	orwl_write_insert(&hdl_out[3], ORWL_LOCATION(30, orwl_task_write_if3), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(30, orwl_task_write_if3), ORWL_LOCATION(30, orwl_task_read_if3), 10);
	// const port at 0
	// const port at 1
	orwl_read_insert(&hdl_in[2], ORWL_LOCATION(7, orwl_task_read_if0), 1, seed, server);
	orwl_read_insert(&hdl_in[3], ORWL_LOCATION(19, orwl_task_read_if0), 0, seed, server);

	report(orwl_taskdep_verbose, "30, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "30, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_31(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "31, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(31, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(31, orwl_task_write_if0), ORWL_LOCATION(31, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(31, orwl_task_write_if1), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(31, orwl_task_write_if1), ORWL_LOCATION(31, orwl_task_read_if1), 10);
	orwl_write_insert(&hdl_out[2], ORWL_LOCATION(31, orwl_task_write_if2), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(31, orwl_task_write_if2), ORWL_LOCATION(31, orwl_task_read_if2), 10);
	orwl_write_insert(&hdl_out[3], ORWL_LOCATION(31, orwl_task_write_if3), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(31, orwl_task_write_if3), ORWL_LOCATION(31, orwl_task_read_if3), 10);
	// const port at 0
	orwl_read_insert(&hdl_in[1], ORWL_LOCATION(17, orwl_task_read_if0), 1, seed, server);
	orwl_read_insert(&hdl_in[2], ORWL_LOCATION(10, orwl_task_read_if0), 1, seed, server);
	orwl_read_insert(&hdl_in[3], ORWL_LOCATION(22, orwl_task_read_if0), 0, seed, server);

	report(orwl_taskdep_verbose, "31, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "31, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_32(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "32, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(32, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(32, orwl_task_write_if0), ORWL_LOCATION(32, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(32, orwl_task_write_if1), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(32, orwl_task_write_if1), ORWL_LOCATION(32, orwl_task_read_if1), 10);
	orwl_write_insert(&hdl_out[2], ORWL_LOCATION(32, orwl_task_write_if2), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(32, orwl_task_write_if2), ORWL_LOCATION(32, orwl_task_read_if2), 10);
	orwl_write_insert(&hdl_out[3], ORWL_LOCATION(32, orwl_task_write_if3), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(32, orwl_task_write_if3), ORWL_LOCATION(32, orwl_task_read_if3), 10);
	// const port at 0
	orwl_read_insert(&hdl_in[1], ORWL_LOCATION(20, orwl_task_read_if0), 1, seed, server);
	orwl_read_insert(&hdl_in[2], ORWL_LOCATION(14, orwl_task_read_if0), 1, seed, server);
	// const port at 3

	report(orwl_taskdep_verbose, "32, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "32, finished orwl_taskdep_init");

};
static orwl_taskdep_vector const p_dummy = { 0 };

// Task function that is executed by task 0 (buf_1_1_b)
static void orwl_taskdep_run_0(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #0 (buf_1_1_b)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(0));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(0, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(0, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_0(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #0 (buf_1_1_b) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[0][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[0][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(0), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[0][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #0 (buf_1_1_b) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(0));

    report(orwl_taskdep_verbose, "task #0 (buf_1_1_b) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #0 (buf_1_1_b) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #0 (buf_1_1_b) ends");
}


// Task function that is executed by task 1 (buf_1_1_r)
static void orwl_taskdep_run_1(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #1 (buf_1_1_r)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(1));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(1, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(1, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_1(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #1 (buf_1_1_r) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[1][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[1][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(1), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[1][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #1 (buf_1_1_r) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(1));

    report(orwl_taskdep_verbose, "task #1 (buf_1_1_r) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #1 (buf_1_1_r) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #1 (buf_1_1_r) ends");
}


// Task function that is executed by task 2 (buf_1_2_b)
static void orwl_taskdep_run_2(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #2 (buf_1_2_b)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(2));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(2, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(2, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_2(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #2 (buf_1_2_b) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[2][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[2][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(2), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[2][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #2 (buf_1_2_b) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(2));

    report(orwl_taskdep_verbose, "task #2 (buf_1_2_b) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #2 (buf_1_2_b) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #2 (buf_1_2_b) ends");
}


// Task function that is executed by task 3 (buf_1_2_l)
static void orwl_taskdep_run_3(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #3 (buf_1_2_l)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(3));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(3, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(3, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_3(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #3 (buf_1_2_l) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[3][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[3][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(3), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[3][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #3 (buf_1_2_l) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(3));

    report(orwl_taskdep_verbose, "task #3 (buf_1_2_l) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #3 (buf_1_2_l) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #3 (buf_1_2_l) ends");
}


// Task function that is executed by task 4 (buf_1_2_r)
static void orwl_taskdep_run_4(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #4 (buf_1_2_r)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(4));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(4, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(4, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_4(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #4 (buf_1_2_r) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[4][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[4][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(4), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[4][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #4 (buf_1_2_r) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(4));

    report(orwl_taskdep_verbose, "task #4 (buf_1_2_r) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #4 (buf_1_2_r) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #4 (buf_1_2_r) ends");
}


// Task function that is executed by task 5 (buf_1_3_b)
static void orwl_taskdep_run_5(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #5 (buf_1_3_b)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(5));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(5, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(5, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_5(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #5 (buf_1_3_b) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[5][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[5][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(5), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[5][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #5 (buf_1_3_b) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(5));

    report(orwl_taskdep_verbose, "task #5 (buf_1_3_b) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #5 (buf_1_3_b) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #5 (buf_1_3_b) ends");
}


// Task function that is executed by task 6 (buf_1_3_l)
static void orwl_taskdep_run_6(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #6 (buf_1_3_l)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(6));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(6, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(6, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_6(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #6 (buf_1_3_l) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[6][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[6][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(6), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[6][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #6 (buf_1_3_l) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(6));

    report(orwl_taskdep_verbose, "task #6 (buf_1_3_l) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #6 (buf_1_3_l) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #6 (buf_1_3_l) ends");
}


// Task function that is executed by task 7 (buf_2_1_b)
static void orwl_taskdep_run_7(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #7 (buf_2_1_b)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(7));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(7, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(7, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_7(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #7 (buf_2_1_b) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[7][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[7][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(7), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[7][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #7 (buf_2_1_b) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(7));

    report(orwl_taskdep_verbose, "task #7 (buf_2_1_b) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #7 (buf_2_1_b) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #7 (buf_2_1_b) ends");
}


// Task function that is executed by task 8 (buf_2_1_r)
static void orwl_taskdep_run_8(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #8 (buf_2_1_r)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(8));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(8, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(8, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_8(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #8 (buf_2_1_r) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[8][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[8][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(8), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[8][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #8 (buf_2_1_r) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(8));

    report(orwl_taskdep_verbose, "task #8 (buf_2_1_r) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #8 (buf_2_1_r) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #8 (buf_2_1_r) ends");
}


// Task function that is executed by task 9 (buf_2_1_t)
static void orwl_taskdep_run_9(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #9 (buf_2_1_t)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(9));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(9, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(9, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_9(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #9 (buf_2_1_t) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[9][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[9][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(9), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[9][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #9 (buf_2_1_t) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(9));

    report(orwl_taskdep_verbose, "task #9 (buf_2_1_t) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #9 (buf_2_1_t) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #9 (buf_2_1_t) ends");
}


// Task function that is executed by task 10 (buf_2_2_b)
static void orwl_taskdep_run_10(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #10 (buf_2_2_b)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(10));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(10, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(10, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_10(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #10 (buf_2_2_b) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[10][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[10][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(10), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[10][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #10 (buf_2_2_b) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(10));

    report(orwl_taskdep_verbose, "task #10 (buf_2_2_b) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #10 (buf_2_2_b) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #10 (buf_2_2_b) ends");
}


// Task function that is executed by task 11 (buf_2_2_l)
static void orwl_taskdep_run_11(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #11 (buf_2_2_l)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(11));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(11, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(11, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_11(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #11 (buf_2_2_l) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[11][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[11][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(11), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[11][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #11 (buf_2_2_l) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(11));

    report(orwl_taskdep_verbose, "task #11 (buf_2_2_l) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #11 (buf_2_2_l) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #11 (buf_2_2_l) ends");
}


// Task function that is executed by task 12 (buf_2_2_r)
static void orwl_taskdep_run_12(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #12 (buf_2_2_r)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(12));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(12, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(12, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_12(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #12 (buf_2_2_r) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[12][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[12][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(12), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[12][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #12 (buf_2_2_r) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(12));

    report(orwl_taskdep_verbose, "task #12 (buf_2_2_r) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #12 (buf_2_2_r) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #12 (buf_2_2_r) ends");
}


// Task function that is executed by task 13 (buf_2_2_t)
static void orwl_taskdep_run_13(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #13 (buf_2_2_t)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(13));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(13, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(13, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_13(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #13 (buf_2_2_t) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[13][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[13][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(13), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[13][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #13 (buf_2_2_t) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(13));

    report(orwl_taskdep_verbose, "task #13 (buf_2_2_t) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #13 (buf_2_2_t) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #13 (buf_2_2_t) ends");
}


// Task function that is executed by task 14 (buf_2_3_b)
static void orwl_taskdep_run_14(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #14 (buf_2_3_b)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(14));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(14, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(14, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_14(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #14 (buf_2_3_b) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[14][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[14][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(14), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[14][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #14 (buf_2_3_b) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(14));

    report(orwl_taskdep_verbose, "task #14 (buf_2_3_b) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #14 (buf_2_3_b) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #14 (buf_2_3_b) ends");
}


// Task function that is executed by task 15 (buf_2_3_l)
static void orwl_taskdep_run_15(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #15 (buf_2_3_l)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(15));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(15, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(15, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_15(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #15 (buf_2_3_l) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[15][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[15][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(15), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[15][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #15 (buf_2_3_l) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(15));

    report(orwl_taskdep_verbose, "task #15 (buf_2_3_l) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #15 (buf_2_3_l) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #15 (buf_2_3_l) ends");
}


// Task function that is executed by task 16 (buf_2_3_t)
static void orwl_taskdep_run_16(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #16 (buf_2_3_t)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(16));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(16, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(16, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_16(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #16 (buf_2_3_t) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[16][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[16][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(16), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[16][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #16 (buf_2_3_t) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(16));

    report(orwl_taskdep_verbose, "task #16 (buf_2_3_t) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #16 (buf_2_3_t) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #16 (buf_2_3_t) ends");
}


// Task function that is executed by task 17 (buf_3_1_r)
static void orwl_taskdep_run_17(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #17 (buf_3_1_r)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(17));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(17, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(17, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_17(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #17 (buf_3_1_r) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[17][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[17][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(17), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[17][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #17 (buf_3_1_r) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(17));

    report(orwl_taskdep_verbose, "task #17 (buf_3_1_r) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #17 (buf_3_1_r) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #17 (buf_3_1_r) ends");
}


// Task function that is executed by task 18 (buf_3_1_t)
static void orwl_taskdep_run_18(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #18 (buf_3_1_t)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(18));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(18, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(18, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_18(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #18 (buf_3_1_t) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[18][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[18][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(18), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[18][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #18 (buf_3_1_t) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(18));

    report(orwl_taskdep_verbose, "task #18 (buf_3_1_t) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #18 (buf_3_1_t) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #18 (buf_3_1_t) ends");
}


// Task function that is executed by task 19 (buf_3_2_l)
static void orwl_taskdep_run_19(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #19 (buf_3_2_l)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(19));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(19, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(19, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_19(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #19 (buf_3_2_l) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[19][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[19][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(19), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[19][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #19 (buf_3_2_l) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(19));

    report(orwl_taskdep_verbose, "task #19 (buf_3_2_l) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #19 (buf_3_2_l) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #19 (buf_3_2_l) ends");
}


// Task function that is executed by task 20 (buf_3_2_r)
static void orwl_taskdep_run_20(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #20 (buf_3_2_r)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(20));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(20, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(20, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_20(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #20 (buf_3_2_r) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[20][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[20][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(20), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[20][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #20 (buf_3_2_r) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(20));

    report(orwl_taskdep_verbose, "task #20 (buf_3_2_r) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #20 (buf_3_2_r) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #20 (buf_3_2_r) ends");
}


// Task function that is executed by task 21 (buf_3_2_t)
static void orwl_taskdep_run_21(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #21 (buf_3_2_t)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(21));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(21, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(21, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_21(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #21 (buf_3_2_t) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[21][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[21][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(21), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[21][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #21 (buf_3_2_t) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(21));

    report(orwl_taskdep_verbose, "task #21 (buf_3_2_t) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #21 (buf_3_2_t) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #21 (buf_3_2_t) ends");
}


// Task function that is executed by task 22 (buf_3_3_l)
static void orwl_taskdep_run_22(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #22 (buf_3_3_l)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(22));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(22, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(22, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_22(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #22 (buf_3_3_l) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[22][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[22][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(22), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[22][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #22 (buf_3_3_l) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(22));

    report(orwl_taskdep_verbose, "task #22 (buf_3_3_l) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #22 (buf_3_3_l) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #22 (buf_3_3_l) ends");
}


// Task function that is executed by task 23 (buf_3_3_t)
static void orwl_taskdep_run_23(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #23 (buf_3_3_t)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(23));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(23, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(23, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_23(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #23 (buf_3_3_t) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[23][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[23][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(23), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[23][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #23 (buf_3_3_t) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(23));

    report(orwl_taskdep_verbose, "task #23 (buf_3_3_t) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #23 (buf_3_3_t) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #23 (buf_3_3_t) ends");
}


// Task function that is executed by task 24 (node_1_1)
static void orwl_taskdep_run_24(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #24 (node_1_1)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(24));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 4,
        odeg  = 4,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(24, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(24, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_24(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #24 (node_1_1) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[24][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[24][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(24), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[24][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #24 (node_1_1) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(24));

    report(orwl_taskdep_verbose, "task #24 (node_1_1) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #24 (node_1_1) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #24 (node_1_1) ends");
}


// Task function that is executed by task 25 (node_1_2)
static void orwl_taskdep_run_25(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #25 (node_1_2)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(25));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 4,
        odeg  = 4,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(25, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(25, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_25(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #25 (node_1_2) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[25][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[25][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(25), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[25][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #25 (node_1_2) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(25));

    report(orwl_taskdep_verbose, "task #25 (node_1_2) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #25 (node_1_2) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #25 (node_1_2) ends");
}


// Task function that is executed by task 26 (node_1_3)
static void orwl_taskdep_run_26(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #26 (node_1_3)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(26));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 4,
        odeg  = 4,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(26, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(26, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_26(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #26 (node_1_3) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[26][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[26][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(26), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[26][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #26 (node_1_3) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(26));

    report(orwl_taskdep_verbose, "task #26 (node_1_3) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #26 (node_1_3) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #26 (node_1_3) ends");
}


// Task function that is executed by task 27 (node_2_1)
static void orwl_taskdep_run_27(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #27 (node_2_1)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(27));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 4,
        odeg  = 4,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(27, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(27, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_27(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #27 (node_2_1) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[27][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[27][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(27), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[27][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #27 (node_2_1) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(27));

    report(orwl_taskdep_verbose, "task #27 (node_2_1) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #27 (node_2_1) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #27 (node_2_1) ends");
}


// Task function that is executed by task 28 (node_2_2)
static void orwl_taskdep_run_28(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #28 (node_2_2)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(28));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 4,
        odeg  = 4,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(28, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(28, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_28(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #28 (node_2_2) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[28][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[28][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(28), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[28][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #28 (node_2_2) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(28));

    report(orwl_taskdep_verbose, "task #28 (node_2_2) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #28 (node_2_2) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #28 (node_2_2) ends");
}


// Task function that is executed by task 29 (node_2_3)
static void orwl_taskdep_run_29(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #29 (node_2_3)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(29));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 4,
        odeg  = 4,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(29, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(29, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_29(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #29 (node_2_3) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[29][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[29][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(29), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[29][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #29 (node_2_3) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(29));

    report(orwl_taskdep_verbose, "task #29 (node_2_3) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #29 (node_2_3) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #29 (node_2_3) ends");
}


// Task function that is executed by task 30 (node_3_1)
static void orwl_taskdep_run_30(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #30 (node_3_1)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(30));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 4,
        odeg  = 4,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(30, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(30, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_30(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #30 (node_3_1) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[30][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[30][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(30), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[30][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #30 (node_3_1) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(30));

    report(orwl_taskdep_verbose, "task #30 (node_3_1) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #30 (node_3_1) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #30 (node_3_1) ends");
}


// Task function that is executed by task 31 (node_3_2)
static void orwl_taskdep_run_31(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #31 (node_3_2)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(31));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 4,
        odeg  = 4,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(31, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(31, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_31(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #31 (node_3_2) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[31][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[31][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(31), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[31][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #31 (node_3_2) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(31));

    report(orwl_taskdep_verbose, "task #31 (node_3_2) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #31 (node_3_2) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #31 (node_3_2) ends");
}


// Task function that is executed by task 32 (node_3_3)
static void orwl_taskdep_run_32(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #32 (node_3_3)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(32));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 4,
        odeg  = 4,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(32, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(32, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_32(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #32 (node_3_3) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[32][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[32][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(32), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[32][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #32 (node_3_3) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(32));

    report(orwl_taskdep_verbose, "task #32 (node_3_3) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #32 (node_3_3) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #32 (node_3_3) ends");
}


static void (*const orwl_taskdep_runs[])(int argc, char* argv[argc+1]) = {
    orwl_taskdep_run_0,
    orwl_taskdep_run_1,
    orwl_taskdep_run_2,
    orwl_taskdep_run_3,
    orwl_taskdep_run_4,
    orwl_taskdep_run_5,
    orwl_taskdep_run_6,
    orwl_taskdep_run_7,
    orwl_taskdep_run_8,
    orwl_taskdep_run_9,
    orwl_taskdep_run_10,
    orwl_taskdep_run_11,
    orwl_taskdep_run_12,
    orwl_taskdep_run_13,
    orwl_taskdep_run_14,
    orwl_taskdep_run_15,
    orwl_taskdep_run_16,
    orwl_taskdep_run_17,
    orwl_taskdep_run_18,
    orwl_taskdep_run_19,
    orwl_taskdep_run_20,
    orwl_taskdep_run_21,
    orwl_taskdep_run_22,
    orwl_taskdep_run_23,
    orwl_taskdep_run_24,
    orwl_taskdep_run_25,
    orwl_taskdep_run_26,
    orwl_taskdep_run_27,
    orwl_taskdep_run_28,
    orwl_taskdep_run_29,
    orwl_taskdep_run_30,
    orwl_taskdep_run_31,
    orwl_taskdep_run_32,
};

_Alignas(32) int64_t const orwl_taskdep_nodes   = 33;
_Alignas(32) int64_t const orwl_taskdep_max_inp = 4;
_Alignas(32) int64_t const orwl_taskdep_max_out = 4;

// Add symbols for Fortran compatibility.

_Alignas(32) int64_t const ORWL_TASKDEP_FORT(orwl_taskdep_vsize)   = orwl_taskdep_vsize;

ORWL_TASKDEP_ALIAS(orwl_taskdep_deg_out);
ORWL_TASKDEP_ALIAS(orwl_taskdep_deg_inp);
ORWL_TASKDEP_ALIAS(orwl_taskdep_nodes);
ORWL_TASKDEP_ALIAS(orwl_taskdep_max_inp);
ORWL_TASKDEP_ALIAS(orwl_taskdep_max_out);

// A wrapper as generic task function that just launches the specific function
ORWL_DEFINE_TASK(orwl_taskdep_type) {
    ORWL_THREAD_USE(orwl_taskdep_type, argc, argv);
    int64_t const tid = orwl_mytid;
    orwl_taskdep_runs[tid](argc, argv);
}

int main(int argc, char* argv[argc+1]) {
  p99_getopt_initialize(&argc, &argv);

  ORWL_TIMER() {
    orwl_init();

    if (orwl_nt != 33) {
        report(1, "We need exactly 33 task, to process this graph, current value is %zu, exiting.", orwl_nt);
        return EXIT_FAILURE;
    }

    //! [launch one thread per task]
    for (size_t i = 0; i < orwl_lt; i++) {
      orwl_taskdep_type* task = P99_NEW(orwl_taskdep_type, argc, argv);
      orwl_taskdep_type_create_task(task, orwl_tids[i]);
    }
    //! [launch one thread per task]
  }
  return EXIT_SUCCESS;
}

// End of program for graph rect_neigh_3_3
// *********************************************************************************

