// **********************************************
#include "orwl_taskdep.h"
#include "simple_functions.h"

// To simulate call by reference for Fortran we need a lot of
// temporaries.
#define FREF64(X) (int64_t const){ (X) }

P99_GETOPT_SYNOPSIS("A program derived from task graph 'line_12' with 12 nodes and 22 dependencies.");
char const*const orwl_taskdep_nnames[12] = {
	[0] = "node-1",
	[1] = "node-10",
	[2] = "node-11",
	[3] = "node-12",
	[4] = "node-2",
	[5] = "node-3",
	[6] = "node-4",
	[7] = "node-5",
	[8] = "node-6",
	[9] = "node-7",
	[10] = "node-8",
	[11] = "node-9",
};
// **********************************************


// Task graph "line_12"


// **********************************************
// External information needed for the task graph

// The data type through which tasks communicate.

enum { orwl_taskdep_vsize = 3, };
typedef floating orwl_taskdep_data;

// ***************************************
// Types.

// The transfer type between tasks.
typedef orwl_taskdep_data orwl_taskdep_vector[orwl_taskdep_vsize];

// The task functions that are used.
//
typedef void orwl_taskdep_ftype(orwl_taskdep_blob state[static restrict 1], int64_t const tid[static 1], int64_t const phase[static 1],
                int64_t const in_num[static 1], orwl_taskdep_vector in_mat[restrict in_num[0]], _Bool skip_in[restrict in_num[0]],
                int64_t const out_num[static 1], orwl_taskdep_vector out_mat[restrict out_num[0]], _Bool skip_out[restrict out_num[0]]);
typedef void orwl_taskdep_stype(orwl_taskdep_blob state[static restrict 1], int64_t const num[static 1]);

// *****************************************************************************
// Static properties of the task graph

// we have 12 nodes/regions

extern void regular(orwl_taskdep_blob state[static restrict 1], int64_t const tid[static 1], int64_t const phase[static 1],
    int64_t const in_num[static 1], orwl_taskdep_vector in_mat[restrict in_num[0]], _Bool skip_in[restrict in_num[0]],
    int64_t const out_num[static 1], orwl_taskdep_vector out_mat[restrict out_num[0]], _Bool skip_out[restrict out_num[0]]);
extern void orwl_taskdep_start(orwl_taskdep_blob state[static restrict 1], int64_t const num[static 1]);
extern void orwl_taskdep_shutdown(orwl_taskdep_blob state[static restrict 1], int64_t const num[static 1]);
char const*const orwl_taskdep_snames[12] = {
	[0] = "orwl_taskdep_start",
	[1] = "orwl_taskdep_start",
	[2] = "orwl_taskdep_start",
	[3] = "orwl_taskdep_start",
	[4] = "orwl_taskdep_start",
	[5] = "orwl_taskdep_start",
	[6] = "orwl_taskdep_start",
	[7] = "orwl_taskdep_start",
	[8] = "orwl_taskdep_start",
	[9] = "orwl_taskdep_start",
	[10] = "orwl_taskdep_start",
	[11] = "orwl_taskdep_start",
};
char const*const orwl_taskdep_fnames[12] = {
	[0] = "regular",
	[1] = "regular",
	[2] = "regular",
	[3] = "regular",
	[4] = "regular",
	[5] = "regular",
	[6] = "regular",
	[7] = "regular",
	[8] = "regular",
	[9] = "regular",
	[10] = "regular",
	[11] = "regular",
};
char const*const orwl_taskdep_dnames[12] = {
	[0] = "orwl_taskdep_shutdown",
	[1] = "orwl_taskdep_shutdown",
	[2] = "orwl_taskdep_shutdown",
	[3] = "orwl_taskdep_shutdown",
	[4] = "orwl_taskdep_shutdown",
	[5] = "orwl_taskdep_shutdown",
	[6] = "orwl_taskdep_shutdown",
	[7] = "orwl_taskdep_shutdown",
	[8] = "orwl_taskdep_shutdown",
	[9] = "orwl_taskdep_shutdown",
	[10] = "orwl_taskdep_shutdown",
	[11] = "orwl_taskdep_shutdown",
};

// Initializers for ports. The table index is the index at the source, since only
// these are guaranteed to be unique. The values here are pointers to const
// qualified compound literals, or 0 if no "init" has been given for the port.
static orwl_taskdep_vector const*const orwl_taskdep_defaults[12][2] = {
	[0] = {
		[0] = 0, // no pre-init for out-1-l
		[1] = 0, // no pre-init for out-1-r
	},
	[1] = {
		[0] = &(orwl_taskdep_vector const){ +0 }, // out-10-l
		[1] = 0, // no pre-init for out-10-r
	},
	[2] = {
		[0] = &(orwl_taskdep_vector const){ +0 }, // out-11-l
		[1] = 0, // no pre-init for out-11-r
	},
	[3] = {
		[0] = &(orwl_taskdep_vector const){ +0 }, // out-12-l
		[1] = 0, // no pre-init for out-12-r
	},
	[4] = {
		[0] = &(orwl_taskdep_vector const){ +0 }, // out-2-l
		[1] = 0, // no pre-init for out-2-r
	},
	[5] = {
		[0] = &(orwl_taskdep_vector const){ +0 }, // out-3-l
		[1] = 0, // no pre-init for out-3-r
	},
	[6] = {
		[0] = &(orwl_taskdep_vector const){ +0 }, // out-4-l
		[1] = 0, // no pre-init for out-4-r
	},
	[7] = {
		[0] = &(orwl_taskdep_vector const){ +0 }, // out-5-l
		[1] = 0, // no pre-init for out-5-r
	},
	[8] = {
		[0] = &(orwl_taskdep_vector const){ +0 }, // out-6-l
		[1] = 0, // no pre-init for out-6-r
	},
	[9] = {
		[0] = &(orwl_taskdep_vector const){ +0 }, // out-7-l
		[1] = 0, // no pre-init for out-7-r
	},
	[10] = {
		[0] = &(orwl_taskdep_vector const){ +0 }, // out-8-l
		[1] = 0, // no pre-init for out-8-r
	},
	[11] = {
		[0] = &(orwl_taskdep_vector const){ +0 }, // out-9-l
		[1] = 0, // no pre-init for out-9-r
	},
};

// The const property for ports. The table index is the index at the source, since only
// these are guaranteed to be unique. .
static bool const orwl_taskdep_const[12][2] = {
	[0] = {
		[0] = 0, // out-1-l
		[1] = 0, // out-1-r
	},
	[1] = {
		[0] = 0, // out-10-l
		[1] = 0, // out-10-r
	},
	[2] = {
		[0] = 0, // out-11-l
		[1] = 0, // out-11-r
	},
	[3] = {
		[0] = 0, // out-12-l
		[1] = 0, // out-12-r
	},
	[4] = {
		[0] = 0, // out-2-l
		[1] = 0, // out-2-r
	},
	[5] = {
		[0] = 0, // out-3-l
		[1] = 0, // out-3-r
	},
	[6] = {
		[0] = 0, // out-4-l
		[1] = 0, // out-4-r
	},
	[7] = {
		[0] = 0, // out-5-l
		[1] = 0, // out-5-r
	},
	[8] = {
		[0] = 0, // out-6-l
		[1] = 0, // out-6-r
	},
	[9] = {
		[0] = 0, // out-7-l
		[1] = 0, // out-7-r
	},
	[10] = {
		[0] = 0, // out-8-l
		[1] = 0, // out-8-r
	},
	[11] = {
		[0] = 0, // out-9-l
		[1] = 0, // out-9-r
	},
};

// For each output port, provide the textual information about the link
char const*const orwl_taskdep_oports[12][2] = {
	[0] = {
		[0] = "out-1-l",
		[1] = "out-1-r",
	},
	[1] = {
		[0] = "out-10-l",
		[1] = "out-10-r",
	},
	[2] = {
		[0] = "out-11-l",
		[1] = "out-11-r",
	},
	[3] = {
		[0] = "out-12-l",
		[1] = "out-12-r",
	},
	[4] = {
		[0] = "out-2-l",
		[1] = "out-2-r",
	},
	[5] = {
		[0] = "out-3-l",
		[1] = "out-3-r",
	},
	[6] = {
		[0] = "out-4-l",
		[1] = "out-4-r",
	},
	[7] = {
		[0] = "out-5-l",
		[1] = "out-5-r",
	},
	[8] = {
		[0] = "out-6-l",
		[1] = "out-6-r",
	},
	[9] = {
		[0] = "out-7-l",
		[1] = "out-7-r",
	},
	[10] = {
		[0] = "out-8-l",
		[1] = "out-8-r",
	},
	[11] = {
		[0] = "out-9-l",
		[1] = "out-9-r",
	},
};

// For each input port, provide the textual information about the link
char const*const orwl_taskdep_iports[12][2] = {
	[0] = {
		[0] = "out-2-l",
		[1] = "<out-0-r",
	},
	[1] = {
		[0] = "out-11-l",
		[1] = "<out-9-r",
	},
	[2] = {
		[0] = "out-12-l",
		[1] = "<out-10-r",
	},
	[3] = {
		[0] = "out-13-l",
		[1] = "<out-11-r",
	},
	[4] = {
		[0] = "out-3-l",
		[1] = "<out-1-r",
	},
	[5] = {
		[0] = "out-4-l",
		[1] = "<out-2-r",
	},
	[6] = {
		[0] = "out-5-l",
		[1] = "<out-3-r",
	},
	[7] = {
		[0] = "out-6-l",
		[1] = "<out-4-r",
	},
	[8] = {
		[0] = "out-7-l",
		[1] = "<out-5-r",
	},
	[9] = {
		[0] = "out-8-l",
		[1] = "<out-6-r",
	},
	[10] = {
		[0] = "out-9-l",
		[1] = "<out-7-r",
	},
	[11] = {
		[0] = "out-10-l",
		[1] = "<out-8-r",
	},
};

// The outdegree for each node
_Alignas(32) int64_t const orwl_taskdep_deg_out[12] = {
	[0] = 2,
	[1] = 2,
	[2] = 2,
	[3] = 2,
	[4] = 2,
	[5] = 2,
	[6] = 2,
	[7] = 2,
	[8] = 2,
	[9] = 2,
	[10] = 2,
	[11] = 2,
};

// The indegree for each node
_Alignas(32) int64_t const orwl_taskdep_deg_inp[12] = {
	[0] = 2,
	[1] = 2,
	[2] = 2,
	[3] = 2,
	[4] = 2,
	[5] = 2,
	[6] = 2,
	[7] = 2,
	[8] = 2,
	[9] = 2,
	[10] = 2,
	[11] = 2,
};

// The number of each output port, or -1 if not this number is not available
int64_t const*const orwl_taskdep_onum[12] = {
	[0] = (int64_t const[2]){
		[0] = -1,
		[1] = -1,
	},
	[1] = (int64_t const[2]){
		[0] = -1,
		[1] = -1,
	},
	[2] = (int64_t const[2]){
		[0] = -1,
		[1] = -1,
	},
	[3] = (int64_t const[2]){
		[0] = -1,
		[1] = -1,
	},
	[4] = (int64_t const[2]){
		[0] = -1,
		[1] = -1,
	},
	[5] = (int64_t const[2]){
		[0] = -1,
		[1] = -1,
	},
	[6] = (int64_t const[2]){
		[0] = -1,
		[1] = -1,
	},
	[7] = (int64_t const[2]){
		[0] = -1,
		[1] = -1,
	},
	[8] = (int64_t const[2]){
		[0] = -1,
		[1] = -1,
	},
	[9] = (int64_t const[2]){
		[0] = -1,
		[1] = -1,
	},
	[10] = (int64_t const[2]){
		[0] = -1,
		[1] = -1,
	},
	[11] = (int64_t const[2]){
		[0] = -1,
		[1] = -1,
	},
};

// The number of each input port, or -1 if not this number is not available
int64_t const*const orwl_taskdep_inum[12] = {
	[0] = (int64_t const[2]){
		[0] = -1,
		[1] = -1,
	},
	[1] = (int64_t const[2]){
		[0] = -1,
		[1] = -1,
	},
	[2] = (int64_t const[2]){
		[0] = -1,
		[1] = -1,
	},
	[3] = (int64_t const[2]){
		[0] = -1,
		[1] = -1,
	},
	[4] = (int64_t const[2]){
		[0] = -1,
		[1] = -1,
	},
	[5] = (int64_t const[2]){
		[0] = -1,
		[1] = -1,
	},
	[6] = (int64_t const[2]){
		[0] = -1,
		[1] = -1,
	},
	[7] = (int64_t const[2]){
		[0] = -1,
		[1] = -1,
	},
	[8] = (int64_t const[2]){
		[0] = -1,
		[1] = -1,
	},
	[9] = (int64_t const[2]){
		[0] = -1,
		[1] = -1,
	},
	[10] = (int64_t const[2]){
		[0] = -1,
		[1] = -1,
	},
	[11] = (int64_t const[2]){
		[0] = -1,
		[1] = -1,
	},
};

// For each input port, provide the source port
static int64_t const orwl_taskdep_tail[12][2] = {
	[0] = {
		[0] = 8,
		[1] = -1,
	},
	[1] = {
		[0] = 4,
		[1] = -1,
	},
	[2] = {
		[0] = 6,
		[1] = -1,
	},
	[3] = {
		[0] = -1,
		[1] = -1,
	},
	[4] = {
		[0] = 10,
		[1] = -1,
	},
	[5] = {
		[0] = 12,
		[1] = -1,
	},
	[6] = {
		[0] = 14,
		[1] = -1,
	},
	[7] = {
		[0] = 16,
		[1] = -1,
	},
	[8] = {
		[0] = 18,
		[1] = -1,
	},
	[9] = {
		[0] = 20,
		[1] = -1,
	},
	[10] = {
		[0] = 22,
		[1] = -1,
	},
	[11] = {
		[0] = 2,
		[1] = -1,
	},
};

void orwl_taskdep_usage(void) {
    printf("task number\tname\tfunction\tstartup\tshutdown\t(in ports)\t(out ports)\n");
    for (size_t i = 0; i < 12; ++i) {
        printf("%zu\t\t%s\t%s\t%s\t%s", i, orwl_taskdep_nnames[i], orwl_taskdep_fnames[i], orwl_taskdep_snames[i], orwl_taskdep_dnames[i]);
        if (orwl_taskdep_iports[i][0]) {
            printf("\t(%s", orwl_taskdep_iports[i][0]);
            for (size_t j = 1; j < 2; ++j) {
                if (orwl_taskdep_iports[i][j])
                   printf(", %s", orwl_taskdep_iports[i][j]);
            }
            printf(")");
        } else {
            printf("	()");
        }
        if (orwl_taskdep_oports[i][0]) {
            printf("\t(%s", orwl_taskdep_oports[i][0]);
            for (size_t j = 1; j < 2; ++j) {
                if (orwl_taskdep_oports[i][j])
                   printf(", %s", orwl_taskdep_oports[i][j]);
            }
            printf(")");
        } else {
            printf("	()");
        }
        printf("\n");
    }

}

P99_GETOPT_CALLBACK(orwl_taskdep_usage);

// *****************************************************
// Instantiate the ORWL structure.
ORWL_LOCATIONS_PER_TASK(
	orwl_task_write_if0,
	orwl_task_read_if0,
	orwl_task_write_if1,
	orwl_task_read_if1
);

ORWL_LOCATIONS_PER_TASK_INSTANTIATION();

ORWL_DECLARE_TASK(orwl_taskdep_type);

// *****************************************************
// Set up the ORWL structure that reflects dependencies.

// This graph is iterated.
// Using orwl_handle2.
// node-1	→ node-2	(out-1-r)
// 0	→ 4
// priorities: 0 1
// node-10	→ node-11	(out-10-r)
// 1	→ 2
// priorities: 0 1
// node-10	→ node-9	(out-10-l)
// 1	→ 11
// using reverse edge "out-9-r (out-10-l)"
// priorities: 1 0
// node-11	→ node-10	(out-11-l)
// 2	→ 1
// using reverse edge "out-10-r (out-11-l)"
// priorities: 1 0
// node-11	→ node-12	(out-11-r)
// 2	→ 3
// priorities: 0 1
// node-12	→ node-11	(out-12-l)
// 3	→ 2
// using reverse edge "out-11-r (out-12-l)"
// priorities: 1 0
// node-2	→ node-1	(out-2-l)
// 4	→ 0
// using reverse edge "out-1-r (out-2-l)"
// priorities: 1 0
// node-2	→ node-3	(out-2-r)
// 4	→ 5
// priorities: 0 1
// node-3	→ node-2	(out-3-l)
// 5	→ 4
// using reverse edge "out-2-r (out-3-l)"
// priorities: 1 0
// node-3	→ node-4	(out-3-r)
// 5	→ 6
// priorities: 0 1
// node-4	→ node-3	(out-4-l)
// 6	→ 5
// using reverse edge "out-3-r (out-4-l)"
// priorities: 1 0
// node-4	→ node-5	(out-4-r)
// 6	→ 7
// priorities: 0 1
// node-5	→ node-4	(out-5-l)
// 7	→ 6
// using reverse edge "out-4-r (out-5-l)"
// priorities: 1 0
// node-5	→ node-6	(out-5-r)
// 7	→ 8
// priorities: 0 1
// node-6	→ node-5	(out-6-l)
// 8	→ 7
// using reverse edge "out-5-r (out-6-l)"
// priorities: 1 0
// node-6	→ node-7	(out-6-r)
// 8	→ 9
// priorities: 0 1
// node-7	→ node-6	(out-7-l)
// 9	→ 8
// using reverse edge "out-6-r (out-7-l)"
// priorities: 1 0
// node-7	→ node-8	(out-7-r)
// 9	→ 10
// priorities: 0 1
// node-8	→ node-7	(out-8-l)
// 10	→ 9
// using reverse edge "out-7-r (out-8-l)"
// priorities: 1 0
// node-8	→ node-9	(out-8-r)
// 10	→ 11
// priorities: 0 1
// node-9	→ node-10	(out-9-r)
// 11	→ 1
// priorities: 0 1
// node-9	→ node-8	(out-9-l)
// 11	→ 10
// using reverse edge "out-8-r (out-9-l)"
// priorities: 1 0

// ****************************************************************
// Per task specific initialization functions. They should ease some
// const propagation concerning the task ID.
static void orwl_taskdep_init_0(orwl_handle2 hdl_in[static 2], orwl_handle2 hdl_out[static 2]) {
	report(orwl_taskdep_verbose, "0, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(0, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(0, orwl_task_write_if0), ORWL_LOCATION(0, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(0, orwl_task_write_if1), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(0, orwl_task_write_if1), ORWL_LOCATION(0, orwl_task_read_if1), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(4, orwl_task_read_if0), 0, seed, server);
	// const port at 1

	report(orwl_taskdep_verbose, "0, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "0, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_1(orwl_handle2 hdl_in[static 2], orwl_handle2 hdl_out[static 2]) {
	report(orwl_taskdep_verbose, "1, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(1, orwl_task_write_if0), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(1, orwl_task_write_if0), ORWL_LOCATION(1, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(1, orwl_task_write_if1), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(1, orwl_task_write_if1), ORWL_LOCATION(1, orwl_task_read_if1), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(2, orwl_task_read_if0), 0, seed, server);
	// const port at 1

	report(orwl_taskdep_verbose, "1, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "1, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_2(orwl_handle2 hdl_in[static 2], orwl_handle2 hdl_out[static 2]) {
	report(orwl_taskdep_verbose, "2, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(2, orwl_task_write_if0), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(2, orwl_task_write_if0), ORWL_LOCATION(2, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(2, orwl_task_write_if1), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(2, orwl_task_write_if1), ORWL_LOCATION(2, orwl_task_read_if1), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(3, orwl_task_read_if0), 0, seed, server);
	// const port at 1

	report(orwl_taskdep_verbose, "2, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "2, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_3(orwl_handle2 hdl_in[static 2], orwl_handle2 hdl_out[static 2]) {
	report(orwl_taskdep_verbose, "3, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(3, orwl_task_write_if0), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(3, orwl_task_write_if0), ORWL_LOCATION(3, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(3, orwl_task_write_if1), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(3, orwl_task_write_if1), ORWL_LOCATION(3, orwl_task_read_if1), 10);
	// const port at 0
	// const port at 1

	report(orwl_taskdep_verbose, "3, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "3, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_4(orwl_handle2 hdl_in[static 2], orwl_handle2 hdl_out[static 2]) {
	report(orwl_taskdep_verbose, "4, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(4, orwl_task_write_if0), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(4, orwl_task_write_if0), ORWL_LOCATION(4, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(4, orwl_task_write_if1), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(4, orwl_task_write_if1), ORWL_LOCATION(4, orwl_task_read_if1), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(5, orwl_task_read_if0), 0, seed, server);
	// const port at 1

	report(orwl_taskdep_verbose, "4, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "4, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_5(orwl_handle2 hdl_in[static 2], orwl_handle2 hdl_out[static 2]) {
	report(orwl_taskdep_verbose, "5, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(5, orwl_task_write_if0), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(5, orwl_task_write_if0), ORWL_LOCATION(5, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(5, orwl_task_write_if1), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(5, orwl_task_write_if1), ORWL_LOCATION(5, orwl_task_read_if1), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(6, orwl_task_read_if0), 0, seed, server);
	// const port at 1

	report(orwl_taskdep_verbose, "5, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "5, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_6(orwl_handle2 hdl_in[static 2], orwl_handle2 hdl_out[static 2]) {
	report(orwl_taskdep_verbose, "6, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(6, orwl_task_write_if0), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(6, orwl_task_write_if0), ORWL_LOCATION(6, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(6, orwl_task_write_if1), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(6, orwl_task_write_if1), ORWL_LOCATION(6, orwl_task_read_if1), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(7, orwl_task_read_if0), 0, seed, server);
	// const port at 1

	report(orwl_taskdep_verbose, "6, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "6, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_7(orwl_handle2 hdl_in[static 2], orwl_handle2 hdl_out[static 2]) {
	report(orwl_taskdep_verbose, "7, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(7, orwl_task_write_if0), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(7, orwl_task_write_if0), ORWL_LOCATION(7, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(7, orwl_task_write_if1), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(7, orwl_task_write_if1), ORWL_LOCATION(7, orwl_task_read_if1), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(8, orwl_task_read_if0), 0, seed, server);
	// const port at 1

	report(orwl_taskdep_verbose, "7, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "7, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_8(orwl_handle2 hdl_in[static 2], orwl_handle2 hdl_out[static 2]) {
	report(orwl_taskdep_verbose, "8, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(8, orwl_task_write_if0), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(8, orwl_task_write_if0), ORWL_LOCATION(8, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(8, orwl_task_write_if1), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(8, orwl_task_write_if1), ORWL_LOCATION(8, orwl_task_read_if1), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(9, orwl_task_read_if0), 0, seed, server);
	// const port at 1

	report(orwl_taskdep_verbose, "8, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "8, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_9(orwl_handle2 hdl_in[static 2], orwl_handle2 hdl_out[static 2]) {
	report(orwl_taskdep_verbose, "9, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(9, orwl_task_write_if0), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(9, orwl_task_write_if0), ORWL_LOCATION(9, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(9, orwl_task_write_if1), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(9, orwl_task_write_if1), ORWL_LOCATION(9, orwl_task_read_if1), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(10, orwl_task_read_if0), 0, seed, server);
	// const port at 1

	report(orwl_taskdep_verbose, "9, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "9, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_10(orwl_handle2 hdl_in[static 2], orwl_handle2 hdl_out[static 2]) {
	report(orwl_taskdep_verbose, "10, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(10, orwl_task_write_if0), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(10, orwl_task_write_if0), ORWL_LOCATION(10, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(10, orwl_task_write_if1), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(10, orwl_task_write_if1), ORWL_LOCATION(10, orwl_task_read_if1), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(11, orwl_task_read_if0), 0, seed, server);
	// const port at 1

	report(orwl_taskdep_verbose, "10, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "10, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_11(orwl_handle2 hdl_in[static 2], orwl_handle2 hdl_out[static 2]) {
	report(orwl_taskdep_verbose, "11, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(11, orwl_task_write_if0), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(11, orwl_task_write_if0), ORWL_LOCATION(11, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(11, orwl_task_write_if1), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(11, orwl_task_write_if1), ORWL_LOCATION(11, orwl_task_read_if1), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(1, orwl_task_read_if0), 0, seed, server);
	// const port at 1

	report(orwl_taskdep_verbose, "11, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "11, finished orwl_taskdep_init");

};
static orwl_taskdep_vector const p_dummy = { 0 };

// Task function that is executed by task 0 (node-1)
static void orwl_taskdep_run_0(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #0 (node-1)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(0));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 2,
        odeg  = 2,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[2] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[2] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(0, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(0, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_0(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #0 (node-1) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[0][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[0][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 2][port % 2]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 2][port % 2];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 2][port % 2]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(0), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[0][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #0 (node-1) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(0));

    report(orwl_taskdep_verbose, "task #0 (node-1) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #0 (node-1) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #0 (node-1) ends");
}


// Task function that is executed by task 1 (node-10)
static void orwl_taskdep_run_1(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #1 (node-10)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(1));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 2,
        odeg  = 2,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[2] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[2] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(1, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(1, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_1(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #1 (node-10) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[1][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[1][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 2][port % 2]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 2][port % 2];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 2][port % 2]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(1), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[1][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #1 (node-10) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(1));

    report(orwl_taskdep_verbose, "task #1 (node-10) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #1 (node-10) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #1 (node-10) ends");
}


// Task function that is executed by task 2 (node-11)
static void orwl_taskdep_run_2(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #2 (node-11)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(2));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 2,
        odeg  = 2,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[2] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[2] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(2, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(2, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_2(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #2 (node-11) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[2][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[2][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 2][port % 2]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 2][port % 2];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 2][port % 2]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(2), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[2][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #2 (node-11) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(2));

    report(orwl_taskdep_verbose, "task #2 (node-11) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #2 (node-11) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #2 (node-11) ends");
}


// Task function that is executed by task 3 (node-12)
static void orwl_taskdep_run_3(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #3 (node-12)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(3));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 2,
        odeg  = 2,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[2] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[2] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(3, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(3, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_3(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #3 (node-12) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[3][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[3][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 2][port % 2]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 2][port % 2];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 2][port % 2]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(3), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[3][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #3 (node-12) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(3));

    report(orwl_taskdep_verbose, "task #3 (node-12) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #3 (node-12) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #3 (node-12) ends");
}


// Task function that is executed by task 4 (node-2)
static void orwl_taskdep_run_4(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #4 (node-2)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(4));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 2,
        odeg  = 2,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[2] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[2] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(4, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(4, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_4(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #4 (node-2) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[4][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[4][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 2][port % 2]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 2][port % 2];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 2][port % 2]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(4), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[4][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #4 (node-2) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(4));

    report(orwl_taskdep_verbose, "task #4 (node-2) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #4 (node-2) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #4 (node-2) ends");
}


// Task function that is executed by task 5 (node-3)
static void orwl_taskdep_run_5(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #5 (node-3)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(5));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 2,
        odeg  = 2,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[2] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[2] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(5, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(5, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_5(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #5 (node-3) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[5][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[5][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 2][port % 2]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 2][port % 2];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 2][port % 2]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(5), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[5][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #5 (node-3) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(5));

    report(orwl_taskdep_verbose, "task #5 (node-3) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #5 (node-3) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #5 (node-3) ends");
}


// Task function that is executed by task 6 (node-4)
static void orwl_taskdep_run_6(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #6 (node-4)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(6));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 2,
        odeg  = 2,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[2] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[2] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(6, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(6, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_6(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #6 (node-4) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[6][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[6][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 2][port % 2]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 2][port % 2];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 2][port % 2]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(6), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[6][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #6 (node-4) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(6));

    report(orwl_taskdep_verbose, "task #6 (node-4) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #6 (node-4) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #6 (node-4) ends");
}


// Task function that is executed by task 7 (node-5)
static void orwl_taskdep_run_7(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #7 (node-5)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(7));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 2,
        odeg  = 2,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[2] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[2] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(7, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(7, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_7(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #7 (node-5) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[7][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[7][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 2][port % 2]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 2][port % 2];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 2][port % 2]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(7), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[7][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #7 (node-5) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(7));

    report(orwl_taskdep_verbose, "task #7 (node-5) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #7 (node-5) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #7 (node-5) ends");
}


// Task function that is executed by task 8 (node-6)
static void orwl_taskdep_run_8(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #8 (node-6)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(8));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 2,
        odeg  = 2,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[2] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[2] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(8, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(8, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_8(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #8 (node-6) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[8][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[8][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 2][port % 2]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 2][port % 2];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 2][port % 2]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(8), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[8][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #8 (node-6) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(8));

    report(orwl_taskdep_verbose, "task #8 (node-6) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #8 (node-6) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #8 (node-6) ends");
}


// Task function that is executed by task 9 (node-7)
static void orwl_taskdep_run_9(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #9 (node-7)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(9));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 2,
        odeg  = 2,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[2] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[2] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(9, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(9, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_9(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #9 (node-7) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[9][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[9][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 2][port % 2]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 2][port % 2];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 2][port % 2]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(9), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[9][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #9 (node-7) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(9));

    report(orwl_taskdep_verbose, "task #9 (node-7) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #9 (node-7) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #9 (node-7) ends");
}


// Task function that is executed by task 10 (node-8)
static void orwl_taskdep_run_10(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #10 (node-8)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(10));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 2,
        odeg  = 2,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[2] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[2] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(10, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(10, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_10(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #10 (node-8) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[10][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[10][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 2][port % 2]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 2][port % 2];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 2][port % 2]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(10), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[10][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #10 (node-8) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(10));

    report(orwl_taskdep_verbose, "task #10 (node-8) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #10 (node-8) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #10 (node-8) ends");
}


// Task function that is executed by task 11 (node-9)
static void orwl_taskdep_run_11(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #11 (node-9)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(11));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 2,
        odeg  = 2,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[2] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[2] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(11, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(11, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_11(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #11 (node-9) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[11][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[11][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 2][port % 2]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 2][port % 2];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 2][port % 2]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(11), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[11][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #11 (node-9) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(11));

    report(orwl_taskdep_verbose, "task #11 (node-9) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #11 (node-9) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #11 (node-9) ends");
}


static void (*const orwl_taskdep_runs[])(int argc, char* argv[argc+1]) = {
    orwl_taskdep_run_0,
    orwl_taskdep_run_1,
    orwl_taskdep_run_2,
    orwl_taskdep_run_3,
    orwl_taskdep_run_4,
    orwl_taskdep_run_5,
    orwl_taskdep_run_6,
    orwl_taskdep_run_7,
    orwl_taskdep_run_8,
    orwl_taskdep_run_9,
    orwl_taskdep_run_10,
    orwl_taskdep_run_11,
};

_Alignas(32) int64_t const orwl_taskdep_nodes   = 12;
_Alignas(32) int64_t const orwl_taskdep_max_inp = 2;
_Alignas(32) int64_t const orwl_taskdep_max_out = 2;

// Add symbols for Fortran compatibility.

_Alignas(32) int64_t const ORWL_TASKDEP_FORT(orwl_taskdep_vsize)   = orwl_taskdep_vsize;

ORWL_TASKDEP_ALIAS(orwl_taskdep_deg_out);
ORWL_TASKDEP_ALIAS(orwl_taskdep_deg_inp);
ORWL_TASKDEP_ALIAS(orwl_taskdep_nodes);
ORWL_TASKDEP_ALIAS(orwl_taskdep_max_inp);
ORWL_TASKDEP_ALIAS(orwl_taskdep_max_out);

// A wrapper as generic task function that just launches the specific function
ORWL_DEFINE_TASK(orwl_taskdep_type) {
    ORWL_THREAD_USE(orwl_taskdep_type, argc, argv);
    int64_t const tid = orwl_mytid;
    orwl_taskdep_runs[tid](argc, argv);
}

int main(int argc, char* argv[argc+1]) {
  p99_getopt_initialize(&argc, &argv);

  ORWL_TIMER() {
    orwl_init();

    if (orwl_nt != 12) {
        report(1, "We need exactly 12 task, to process this graph, current value is %zu, exiting.", orwl_nt);
        return EXIT_FAILURE;
    }

    //! [launch one thread per task]
    for (size_t i = 0; i < orwl_lt; i++) {
      orwl_taskdep_type* task = P99_NEW(orwl_taskdep_type, argc, argv);
      orwl_taskdep_type_create_task(task, orwl_tids[i]);
    }
    //! [launch one thread per task]
  }
  return EXIT_SUCCESS;
}

// End of program for graph line_12
// *********************************************************************************

