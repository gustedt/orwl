// **********************************************
#include "orwl_taskdep.h"
#include "simple_functions.h"

// To simulate call by reference for Fortran we need a lot of
// temporaries.
#define FREF64(X) (int64_t const){ (X) }

P99_GETOPT_SYNOPSIS("A program derived from task graph 'grid' with 9 nodes and 12 dependencies.");
char const*const orwl_taskdep_nnames[9] = {
	[0] = "00",
	[1] = "01",
	[2] = "02",
	[3] = "10",
	[4] = "11",
	[5] = "12",
	[6] = "20",
	[7] = "21",
	[8] = "22",
};
// **********************************************


// Task graph "grid"


// **********************************************
// External information needed for the task graph

// The data type through which tasks communicate.

enum { orwl_taskdep_vsize = 3, };
typedef floating orwl_taskdep_data;

// ***************************************
// Types.

// The transfer type between tasks.
typedef orwl_taskdep_data orwl_taskdep_vector[orwl_taskdep_vsize];

// The task functions that are used.
//
typedef void orwl_taskdep_ftype(orwl_taskdep_blob state[static restrict 1], int64_t const tid[static 1], int64_t const phase[static 1],
                int64_t const in_num[static 1], orwl_taskdep_vector in_mat[restrict in_num[0]], _Bool skip_in[restrict in_num[0]],
                int64_t const out_num[static 1], orwl_taskdep_vector out_mat[restrict out_num[0]], _Bool skip_out[restrict out_num[0]]);
typedef void orwl_taskdep_stype(orwl_taskdep_blob state[static restrict 1], int64_t const num[static 1]);

// *****************************************************************************
// Static properties of the task graph

// we have 9 nodes/regions

extern void regular(orwl_taskdep_blob state[static restrict 1], int64_t const tid[static 1], int64_t const phase[static 1],
    int64_t const in_num[static 1], orwl_taskdep_vector in_mat[restrict in_num[0]], _Bool skip_in[restrict in_num[0]],
    int64_t const out_num[static 1], orwl_taskdep_vector out_mat[restrict out_num[0]], _Bool skip_out[restrict out_num[0]]);
extern void orwl_taskdep_start(orwl_taskdep_blob state[static restrict 1], int64_t const num[static 1]);
extern void orwl_taskdep_shutdown(orwl_taskdep_blob state[static restrict 1], int64_t const num[static 1]);
char const*const orwl_taskdep_snames[9] = {
	[0] = "orwl_taskdep_start",
	[1] = "orwl_taskdep_start",
	[2] = "orwl_taskdep_start",
	[3] = "orwl_taskdep_start",
	[4] = "orwl_taskdep_start",
	[5] = "orwl_taskdep_start",
	[6] = "orwl_taskdep_start",
	[7] = "orwl_taskdep_start",
	[8] = "orwl_taskdep_start",
};
char const*const orwl_taskdep_fnames[9] = {
	[0] = "regular",
	[1] = "regular",
	[2] = "regular",
	[3] = "regular",
	[4] = "regular",
	[5] = "regular",
	[6] = "regular",
	[7] = "regular",
	[8] = "regular",
};
char const*const orwl_taskdep_dnames[9] = {
	[0] = "orwl_taskdep_shutdown",
	[1] = "orwl_taskdep_shutdown",
	[2] = "orwl_taskdep_shutdown",
	[3] = "orwl_taskdep_shutdown",
	[4] = "orwl_taskdep_shutdown",
	[5] = "orwl_taskdep_shutdown",
	[6] = "orwl_taskdep_shutdown",
	[7] = "orwl_taskdep_shutdown",
	[8] = "orwl_taskdep_shutdown",
};

// Initializers for ports. The table index is the index at the source, since only
// these are guaranteed to be unique. The values here are pointers to const
// qualified compound literals, or 0 if no "init" has been given for the port.
static orwl_taskdep_vector const*const orwl_taskdep_defaults[9][4] = {
	[0] = {
		[0] = 0, // no pre-init for 00E
		[1] = 0, // no pre-init for 00S
		[2] = 0, // no pre-init for 00W
		[3] = 0, // no pre-init for 00N
	},
	[1] = {
		[0] = 0, // no pre-init for 01E
		[1] = 0, // no pre-init for 01S
		[2] = &(orwl_taskdep_vector const){ +0 }, // 01W
		[3] = 0, // no pre-init for 01N
	},
	[2] = {
		[0] = 0, // no pre-init for 02E
		[1] = 0, // no pre-init for 02S
		[2] = &(orwl_taskdep_vector const){ +0 }, // 02W
		[3] = 0, // no pre-init for 02N
	},
	[3] = {
		[0] = 0, // no pre-init for 10E
		[1] = 0, // no pre-init for 10S
		[2] = 0, // no pre-init for 10W
		[3] = &(orwl_taskdep_vector const){ +0 }, // 10N
	},
	[4] = {
		[0] = 0, // no pre-init for 11E
		[1] = 0, // no pre-init for 11S
		[2] = &(orwl_taskdep_vector const){ +0 }, // 11W
		[3] = &(orwl_taskdep_vector const){ +0 }, // 11N
	},
	[5] = {
		[0] = 0, // no pre-init for 12E
		[1] = 0, // no pre-init for 12S
		[2] = &(orwl_taskdep_vector const){ +0 }, // 12W
		[3] = &(orwl_taskdep_vector const){ +0 }, // 12N
	},
	[6] = {
		[0] = 0, // no pre-init for 20E
		[1] = 0, // no pre-init for 20S
		[2] = 0, // no pre-init for 20W
		[3] = &(orwl_taskdep_vector const){ +0 }, // 20N
	},
	[7] = {
		[0] = 0, // no pre-init for 21E
		[1] = 0, // no pre-init for 21S
		[2] = &(orwl_taskdep_vector const){ +0 }, // 21W
		[3] = &(orwl_taskdep_vector const){ +0 }, // 21N
	},
	[8] = {
		[0] = 0, // no pre-init for 22E
		[1] = 0, // no pre-init for 22S
		[2] = &(orwl_taskdep_vector const){ +0 }, // 22W
		[3] = &(orwl_taskdep_vector const){ +0 }, // 22N
	},
};

// The const property for ports. The table index is the index at the source, since only
// these are guaranteed to be unique. .
static bool const orwl_taskdep_const[9][4] = {
	[0] = {
		[0] = 0, // 00E
		[1] = 0, // 00S
		[2] = 0, // 00W
		[3] = 0, // 00N
	},
	[1] = {
		[0] = 0, // 01E
		[1] = 0, // 01S
		[2] = 0, // 01W
		[3] = 0, // 01N
	},
	[2] = {
		[0] = 0, // 02E
		[1] = 0, // 02S
		[2] = 0, // 02W
		[3] = 0, // 02N
	},
	[3] = {
		[0] = 0, // 10E
		[1] = 0, // 10S
		[2] = 0, // 10W
		[3] = 0, // 10N
	},
	[4] = {
		[0] = 0, // 11E
		[1] = 0, // 11S
		[2] = 0, // 11W
		[3] = 0, // 11N
	},
	[5] = {
		[0] = 0, // 12E
		[1] = 0, // 12S
		[2] = 0, // 12W
		[3] = 0, // 12N
	},
	[6] = {
		[0] = 0, // 20E
		[1] = 0, // 20S
		[2] = 0, // 20W
		[3] = 0, // 20N
	},
	[7] = {
		[0] = 0, // 21E
		[1] = 0, // 21S
		[2] = 0, // 21W
		[3] = 0, // 21N
	},
	[8] = {
		[0] = 0, // 22E
		[1] = 0, // 22S
		[2] = 0, // 22W
		[3] = 0, // 22N
	},
};

// For each output port, provide the textual information about the link
char const*const orwl_taskdep_oports[9][4] = {
	[0] = {
		[0] = "00E",
		[1] = "00S",
		[2] = "00W",
		[3] = "00N",
	},
	[1] = {
		[0] = "01E",
		[1] = "01S",
		[2] = "01W",
		[3] = "01N",
	},
	[2] = {
		[0] = "02E",
		[1] = "02S",
		[2] = "02W",
		[3] = "02N",
	},
	[3] = {
		[0] = "10E",
		[1] = "10S",
		[2] = "10W",
		[3] = "10N",
	},
	[4] = {
		[0] = "11E",
		[1] = "11S",
		[2] = "11W",
		[3] = "11N",
	},
	[5] = {
		[0] = "12E",
		[1] = "12S",
		[2] = "12W",
		[3] = "12N",
	},
	[6] = {
		[0] = "20E",
		[1] = "20S",
		[2] = "20W",
		[3] = "20N",
	},
	[7] = {
		[0] = "21E",
		[1] = "21S",
		[2] = "21W",
		[3] = "21N",
	},
	[8] = {
		[0] = "22E",
		[1] = "22S",
		[2] = "22W",
		[3] = "22N",
	},
};

// For each input port, provide the textual information about the link
char const*const orwl_taskdep_iports[9][4] = {
	[0] = {
		[0] = "10N",
		[1] = "01W",
		[2] = "-10S",
		[3] = "0-1E",
	},
	[1] = {
		[0] = "11N",
		[1] = "02W",
		[2] = "-11S",
		[3] = "00E",
	},
	[2] = {
		[0] = "12N",
		[1] = "03W",
		[2] = "-12S",
		[3] = "01E",
	},
	[3] = {
		[0] = "20N",
		[1] = "11W",
		[2] = "00S",
		[3] = "1-1E",
	},
	[4] = {
		[0] = "21N",
		[1] = "12W",
		[2] = "01S",
		[3] = "10E",
	},
	[5] = {
		[0] = "22N",
		[1] = "13W",
		[2] = "02S",
		[3] = "11E",
	},
	[6] = {
		[0] = "30N",
		[1] = "21W",
		[2] = "10S",
		[3] = "2-1E",
	},
	[7] = {
		[0] = "31N",
		[1] = "22W",
		[2] = "11S",
		[3] = "20E",
	},
	[8] = {
		[0] = "32N",
		[1] = "23W",
		[2] = "12S",
		[3] = "21E",
	},
};

// The outdegree for each node
_Alignas(32) int64_t const orwl_taskdep_deg_out[9] = {
	[0] = 4,
	[1] = 4,
	[2] = 4,
	[3] = 4,
	[4] = 4,
	[5] = 4,
	[6] = 4,
	[7] = 4,
	[8] = 4,
};

// The indegree for each node
_Alignas(32) int64_t const orwl_taskdep_deg_inp[9] = {
	[0] = 4,
	[1] = 4,
	[2] = 4,
	[3] = 4,
	[4] = 4,
	[5] = 4,
	[6] = 4,
	[7] = 4,
	[8] = 4,
};

// The number of each output port, or -1 if not this number is not available
int64_t const*const orwl_taskdep_onum[9] = {
	[0] = (int64_t const[4]){
		[0] = -1,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[1] = (int64_t const[4]){
		[0] = -1,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[2] = (int64_t const[4]){
		[0] = -1,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[3] = (int64_t const[4]){
		[0] = -1,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[4] = (int64_t const[4]){
		[0] = -1,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[5] = (int64_t const[4]){
		[0] = -1,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[6] = (int64_t const[4]){
		[0] = -1,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[7] = (int64_t const[4]){
		[0] = -1,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[8] = (int64_t const[4]){
		[0] = -1,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
};

// The number of each input port, or -1 if not this number is not available
int64_t const*const orwl_taskdep_inum[9] = {
	[0] = (int64_t const[4]){
		[0] = -1,
		[1] = -1,
		[2] = -1,
		[3] = 1,
	},
	[1] = (int64_t const[4]){
		[0] = -1,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[2] = (int64_t const[4]){
		[0] = -1,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[3] = (int64_t const[4]){
		[0] = -1,
		[1] = -1,
		[2] = -1,
		[3] = 1,
	},
	[4] = (int64_t const[4]){
		[0] = -1,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[5] = (int64_t const[4]){
		[0] = -1,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[6] = (int64_t const[4]){
		[0] = -1,
		[1] = -1,
		[2] = -1,
		[3] = 1,
	},
	[7] = (int64_t const[4]){
		[0] = -1,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[8] = (int64_t const[4]){
		[0] = -1,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
};

// For each input port, provide the source port
static int64_t const orwl_taskdep_tail[9][4] = {
	[0] = {
		[0] = 15,
		[1] = 6,
		[2] = -1,
		[3] = -1,
	},
	[1] = {
		[0] = 19,
		[1] = 10,
		[2] = -1,
		[3] = 0,
	},
	[2] = {
		[0] = 23,
		[1] = -1,
		[2] = -1,
		[3] = 4,
	},
	[3] = {
		[0] = 27,
		[1] = 18,
		[2] = 1,
		[3] = -1,
	},
	[4] = {
		[0] = 31,
		[1] = 22,
		[2] = 5,
		[3] = 12,
	},
	[5] = {
		[0] = 35,
		[1] = -1,
		[2] = 9,
		[3] = 16,
	},
	[6] = {
		[0] = -1,
		[1] = 30,
		[2] = 13,
		[3] = -1,
	},
	[7] = {
		[0] = -1,
		[1] = 34,
		[2] = 17,
		[3] = 24,
	},
	[8] = {
		[0] = -1,
		[1] = -1,
		[2] = 21,
		[3] = 28,
	},
};

void orwl_taskdep_usage(void) {
    printf("task number\tname\tfunction\tstartup\tshutdown\t(in ports)\t(out ports)\n");
    for (size_t i = 0; i < 9; ++i) {
        printf("%zu\t\t%s\t%s\t%s\t%s", i, orwl_taskdep_nnames[i], orwl_taskdep_fnames[i], orwl_taskdep_snames[i], orwl_taskdep_dnames[i]);
        if (orwl_taskdep_iports[i][0]) {
            printf("\t(%s", orwl_taskdep_iports[i][0]);
            for (size_t j = 1; j < 4; ++j) {
                if (orwl_taskdep_iports[i][j])
                   printf(", %s", orwl_taskdep_iports[i][j]);
            }
            printf(")");
        } else {
            printf("	()");
        }
        if (orwl_taskdep_oports[i][0]) {
            printf("\t(%s", orwl_taskdep_oports[i][0]);
            for (size_t j = 1; j < 4; ++j) {
                if (orwl_taskdep_oports[i][j])
                   printf(", %s", orwl_taskdep_oports[i][j]);
            }
            printf(")");
        } else {
            printf("	()");
        }
        printf("\n");
    }

}

P99_GETOPT_CALLBACK(orwl_taskdep_usage);

// *****************************************************
// Instantiate the ORWL structure.
ORWL_LOCATIONS_PER_TASK(
	orwl_task_write_if0,
	orwl_task_read_if0,
	orwl_task_write_if1,
	orwl_task_read_if1,
	orwl_task_write_if2,
	orwl_task_read_if2,
	orwl_task_write_if3,
	orwl_task_read_if3
);

ORWL_LOCATIONS_PER_TASK_INSTANTIATION();

ORWL_DECLARE_TASK(orwl_taskdep_type);

// *****************************************************
// Set up the ORWL structure that reflects dependencies.

// This graph is iterated.
// Using orwl_handle2.
// 00	→ 01	(00E)
// 0	→ 1
// priorities: 0 1
// 00	→ 10	(00S)
// 0	→ 3
// priorities: 0 1
// 01	→ 00	(01W)
// 1	→ 0
// using reverse edge "00E (01W)"
// priorities: 1 0
// 01	→ 02	(01E)
// 1	→ 2
// priorities: 0 1
// 01	→ 11	(01S)
// 1	→ 4
// priorities: 0 1
// 02	→ 01	(02W)
// 2	→ 1
// using reverse edge "01E (02W)"
// priorities: 1 0
// 02	→ 12	(02S)
// 2	→ 5
// priorities: 0 1
// 10	→ 00	(10N)
// 3	→ 0
// using reverse edge "00S (10N)"
// priorities: 1 0
// 10	→ 11	(10E)
// 3	→ 4
// priorities: 0 1
// 10	→ 20	(10S)
// 3	→ 6
// priorities: 0 1
// 11	→ 01	(11N)
// 4	→ 1
// using reverse edge "01S (11N)"
// priorities: 1 0
// 11	→ 10	(11W)
// 4	→ 3
// using reverse edge "10E (11W)"
// priorities: 1 0
// 11	→ 12	(11E)
// 4	→ 5
// priorities: 0 1
// 11	→ 21	(11S)
// 4	→ 7
// priorities: 0 1
// 12	→ 02	(12N)
// 5	→ 2
// using reverse edge "02S (12N)"
// priorities: 1 0
// 12	→ 11	(12W)
// 5	→ 4
// using reverse edge "11E (12W)"
// priorities: 1 0
// 12	→ 22	(12S)
// 5	→ 8
// priorities: 0 1
// 20	→ 10	(20N)
// 6	→ 3
// using reverse edge "10S (20N)"
// priorities: 1 0
// 20	→ 21	(20E)
// 6	→ 7
// priorities: 0 1
// 21	→ 11	(21N)
// 7	→ 4
// using reverse edge "11S (21N)"
// priorities: 1 0
// 21	→ 20	(21W)
// 7	→ 6
// using reverse edge "20E (21W)"
// priorities: 1 0
// 21	→ 22	(21E)
// 7	→ 8
// priorities: 0 1
// 22	→ 12	(22N)
// 8	→ 5
// using reverse edge "12S (22N)"
// priorities: 1 0
// 22	→ 21	(22W)
// 8	→ 7
// using reverse edge "21E (22W)"
// priorities: 1 0

// ****************************************************************
// Per task specific initialization functions. They should ease some
// const propagation concerning the task ID.
static void orwl_taskdep_init_0(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "0, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(0, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(0, orwl_task_write_if0), ORWL_LOCATION(0, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(0, orwl_task_write_if1), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(0, orwl_task_write_if1), ORWL_LOCATION(0, orwl_task_read_if1), 10);
	orwl_write_insert(&hdl_out[2], ORWL_LOCATION(0, orwl_task_write_if2), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(0, orwl_task_write_if2), ORWL_LOCATION(0, orwl_task_read_if2), 10);
	orwl_write_insert(&hdl_out[3], ORWL_LOCATION(0, orwl_task_write_if3), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(0, orwl_task_write_if3), ORWL_LOCATION(0, orwl_task_read_if3), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(3, orwl_task_read_if3), 0, seed, server);
	orwl_read_insert(&hdl_in[1], ORWL_LOCATION(1, orwl_task_read_if2), 0, seed, server);
	// const port at 2
	// const port at 3

	report(orwl_taskdep_verbose, "0, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "0, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_1(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "1, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(1, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(1, orwl_task_write_if0), ORWL_LOCATION(1, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(1, orwl_task_write_if1), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(1, orwl_task_write_if1), ORWL_LOCATION(1, orwl_task_read_if1), 10);
	orwl_write_insert(&hdl_out[2], ORWL_LOCATION(1, orwl_task_write_if2), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(1, orwl_task_write_if2), ORWL_LOCATION(1, orwl_task_read_if2), 10);
	orwl_write_insert(&hdl_out[3], ORWL_LOCATION(1, orwl_task_write_if3), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(1, orwl_task_write_if3), ORWL_LOCATION(1, orwl_task_read_if3), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(4, orwl_task_read_if3), 0, seed, server);
	orwl_read_insert(&hdl_in[1], ORWL_LOCATION(2, orwl_task_read_if2), 0, seed, server);
	// const port at 2
	orwl_read_insert(&hdl_in[3], ORWL_LOCATION(0, orwl_task_read_if0), 1, seed, server);

	report(orwl_taskdep_verbose, "1, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "1, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_2(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "2, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(2, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(2, orwl_task_write_if0), ORWL_LOCATION(2, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(2, orwl_task_write_if1), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(2, orwl_task_write_if1), ORWL_LOCATION(2, orwl_task_read_if1), 10);
	orwl_write_insert(&hdl_out[2], ORWL_LOCATION(2, orwl_task_write_if2), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(2, orwl_task_write_if2), ORWL_LOCATION(2, orwl_task_read_if2), 10);
	orwl_write_insert(&hdl_out[3], ORWL_LOCATION(2, orwl_task_write_if3), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(2, orwl_task_write_if3), ORWL_LOCATION(2, orwl_task_read_if3), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(5, orwl_task_read_if3), 0, seed, server);
	// const port at 1
	// const port at 2
	orwl_read_insert(&hdl_in[3], ORWL_LOCATION(1, orwl_task_read_if0), 1, seed, server);

	report(orwl_taskdep_verbose, "2, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "2, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_3(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "3, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(3, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(3, orwl_task_write_if0), ORWL_LOCATION(3, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(3, orwl_task_write_if1), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(3, orwl_task_write_if1), ORWL_LOCATION(3, orwl_task_read_if1), 10);
	orwl_write_insert(&hdl_out[2], ORWL_LOCATION(3, orwl_task_write_if2), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(3, orwl_task_write_if2), ORWL_LOCATION(3, orwl_task_read_if2), 10);
	orwl_write_insert(&hdl_out[3], ORWL_LOCATION(3, orwl_task_write_if3), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(3, orwl_task_write_if3), ORWL_LOCATION(3, orwl_task_read_if3), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(6, orwl_task_read_if3), 0, seed, server);
	orwl_read_insert(&hdl_in[1], ORWL_LOCATION(4, orwl_task_read_if2), 0, seed, server);
	orwl_read_insert(&hdl_in[2], ORWL_LOCATION(0, orwl_task_read_if1), 1, seed, server);
	// const port at 3

	report(orwl_taskdep_verbose, "3, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "3, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_4(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "4, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(4, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(4, orwl_task_write_if0), ORWL_LOCATION(4, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(4, orwl_task_write_if1), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(4, orwl_task_write_if1), ORWL_LOCATION(4, orwl_task_read_if1), 10);
	orwl_write_insert(&hdl_out[2], ORWL_LOCATION(4, orwl_task_write_if2), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(4, orwl_task_write_if2), ORWL_LOCATION(4, orwl_task_read_if2), 10);
	orwl_write_insert(&hdl_out[3], ORWL_LOCATION(4, orwl_task_write_if3), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(4, orwl_task_write_if3), ORWL_LOCATION(4, orwl_task_read_if3), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(7, orwl_task_read_if3), 0, seed, server);
	orwl_read_insert(&hdl_in[1], ORWL_LOCATION(5, orwl_task_read_if2), 0, seed, server);
	orwl_read_insert(&hdl_in[2], ORWL_LOCATION(1, orwl_task_read_if1), 1, seed, server);
	orwl_read_insert(&hdl_in[3], ORWL_LOCATION(3, orwl_task_read_if0), 1, seed, server);

	report(orwl_taskdep_verbose, "4, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "4, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_5(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "5, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(5, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(5, orwl_task_write_if0), ORWL_LOCATION(5, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(5, orwl_task_write_if1), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(5, orwl_task_write_if1), ORWL_LOCATION(5, orwl_task_read_if1), 10);
	orwl_write_insert(&hdl_out[2], ORWL_LOCATION(5, orwl_task_write_if2), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(5, orwl_task_write_if2), ORWL_LOCATION(5, orwl_task_read_if2), 10);
	orwl_write_insert(&hdl_out[3], ORWL_LOCATION(5, orwl_task_write_if3), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(5, orwl_task_write_if3), ORWL_LOCATION(5, orwl_task_read_if3), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(8, orwl_task_read_if3), 0, seed, server);
	// const port at 1
	orwl_read_insert(&hdl_in[2], ORWL_LOCATION(2, orwl_task_read_if1), 1, seed, server);
	orwl_read_insert(&hdl_in[3], ORWL_LOCATION(4, orwl_task_read_if0), 1, seed, server);

	report(orwl_taskdep_verbose, "5, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "5, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_6(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "6, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(6, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(6, orwl_task_write_if0), ORWL_LOCATION(6, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(6, orwl_task_write_if1), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(6, orwl_task_write_if1), ORWL_LOCATION(6, orwl_task_read_if1), 10);
	orwl_write_insert(&hdl_out[2], ORWL_LOCATION(6, orwl_task_write_if2), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(6, orwl_task_write_if2), ORWL_LOCATION(6, orwl_task_read_if2), 10);
	orwl_write_insert(&hdl_out[3], ORWL_LOCATION(6, orwl_task_write_if3), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(6, orwl_task_write_if3), ORWL_LOCATION(6, orwl_task_read_if3), 10);
	// const port at 0
	orwl_read_insert(&hdl_in[1], ORWL_LOCATION(7, orwl_task_read_if2), 0, seed, server);
	orwl_read_insert(&hdl_in[2], ORWL_LOCATION(3, orwl_task_read_if1), 1, seed, server);
	// const port at 3

	report(orwl_taskdep_verbose, "6, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "6, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_7(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "7, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(7, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(7, orwl_task_write_if0), ORWL_LOCATION(7, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(7, orwl_task_write_if1), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(7, orwl_task_write_if1), ORWL_LOCATION(7, orwl_task_read_if1), 10);
	orwl_write_insert(&hdl_out[2], ORWL_LOCATION(7, orwl_task_write_if2), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(7, orwl_task_write_if2), ORWL_LOCATION(7, orwl_task_read_if2), 10);
	orwl_write_insert(&hdl_out[3], ORWL_LOCATION(7, orwl_task_write_if3), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(7, orwl_task_write_if3), ORWL_LOCATION(7, orwl_task_read_if3), 10);
	// const port at 0
	orwl_read_insert(&hdl_in[1], ORWL_LOCATION(8, orwl_task_read_if2), 0, seed, server);
	orwl_read_insert(&hdl_in[2], ORWL_LOCATION(4, orwl_task_read_if1), 1, seed, server);
	orwl_read_insert(&hdl_in[3], ORWL_LOCATION(6, orwl_task_read_if0), 1, seed, server);

	report(orwl_taskdep_verbose, "7, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "7, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_8(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "8, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(8, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(8, orwl_task_write_if0), ORWL_LOCATION(8, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(8, orwl_task_write_if1), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(8, orwl_task_write_if1), ORWL_LOCATION(8, orwl_task_read_if1), 10);
	orwl_write_insert(&hdl_out[2], ORWL_LOCATION(8, orwl_task_write_if2), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(8, orwl_task_write_if2), ORWL_LOCATION(8, orwl_task_read_if2), 10);
	orwl_write_insert(&hdl_out[3], ORWL_LOCATION(8, orwl_task_write_if3), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(8, orwl_task_write_if3), ORWL_LOCATION(8, orwl_task_read_if3), 10);
	// const port at 0
	// const port at 1
	orwl_read_insert(&hdl_in[2], ORWL_LOCATION(5, orwl_task_read_if1), 1, seed, server);
	orwl_read_insert(&hdl_in[3], ORWL_LOCATION(7, orwl_task_read_if0), 1, seed, server);

	report(orwl_taskdep_verbose, "8, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "8, finished orwl_taskdep_init");

};
static orwl_taskdep_vector const p_dummy = { 0 };

// Task function that is executed by task 0 (00)
static void orwl_taskdep_run_0(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #0 (00)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(0));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 4,
        odeg  = 4,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(0, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(0, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_0(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #0 (00) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[0][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[0][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(0), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[0][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #0 (00) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(0));

    report(orwl_taskdep_verbose, "task #0 (00) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #0 (00) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #0 (00) ends");
}


// Task function that is executed by task 1 (01)
static void orwl_taskdep_run_1(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #1 (01)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(1));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 4,
        odeg  = 4,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(1, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(1, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_1(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #1 (01) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[1][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[1][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(1), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[1][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #1 (01) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(1));

    report(orwl_taskdep_verbose, "task #1 (01) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #1 (01) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #1 (01) ends");
}


// Task function that is executed by task 2 (02)
static void orwl_taskdep_run_2(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #2 (02)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(2));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 4,
        odeg  = 4,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(2, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(2, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_2(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #2 (02) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[2][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[2][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(2), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[2][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #2 (02) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(2));

    report(orwl_taskdep_verbose, "task #2 (02) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #2 (02) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #2 (02) ends");
}


// Task function that is executed by task 3 (10)
static void orwl_taskdep_run_3(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #3 (10)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(3));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 4,
        odeg  = 4,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(3, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(3, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_3(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #3 (10) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[3][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[3][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(3), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[3][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #3 (10) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(3));

    report(orwl_taskdep_verbose, "task #3 (10) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #3 (10) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #3 (10) ends");
}


// Task function that is executed by task 4 (11)
static void orwl_taskdep_run_4(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #4 (11)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(4));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 4,
        odeg  = 4,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(4, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(4, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_4(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #4 (11) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[4][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[4][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(4), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[4][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #4 (11) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(4));

    report(orwl_taskdep_verbose, "task #4 (11) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #4 (11) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #4 (11) ends");
}


// Task function that is executed by task 5 (12)
static void orwl_taskdep_run_5(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #5 (12)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(5));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 4,
        odeg  = 4,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(5, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(5, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_5(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #5 (12) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[5][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[5][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(5), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[5][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #5 (12) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(5));

    report(orwl_taskdep_verbose, "task #5 (12) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #5 (12) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #5 (12) ends");
}


// Task function that is executed by task 6 (20)
static void orwl_taskdep_run_6(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #6 (20)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(6));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 4,
        odeg  = 4,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(6, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(6, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_6(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #6 (20) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[6][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[6][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(6), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[6][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #6 (20) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(6));

    report(orwl_taskdep_verbose, "task #6 (20) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #6 (20) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #6 (20) ends");
}


// Task function that is executed by task 7 (21)
static void orwl_taskdep_run_7(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #7 (21)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(7));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 4,
        odeg  = 4,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(7, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(7, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_7(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #7 (21) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[7][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[7][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(7), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[7][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #7 (21) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(7));

    report(orwl_taskdep_verbose, "task #7 (21) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #7 (21) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #7 (21) ends");
}


// Task function that is executed by task 8 (22)
static void orwl_taskdep_run_8(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #8 (22)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(8));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 4,
        odeg  = 4,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(8, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(8, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_8(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #8 (22) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[8][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[8][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(8), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[8][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #8 (22) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(8));

    report(orwl_taskdep_verbose, "task #8 (22) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #8 (22) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #8 (22) ends");
}


static void (*const orwl_taskdep_runs[])(int argc, char* argv[argc+1]) = {
    orwl_taskdep_run_0,
    orwl_taskdep_run_1,
    orwl_taskdep_run_2,
    orwl_taskdep_run_3,
    orwl_taskdep_run_4,
    orwl_taskdep_run_5,
    orwl_taskdep_run_6,
    orwl_taskdep_run_7,
    orwl_taskdep_run_8,
};

_Alignas(32) int64_t const orwl_taskdep_nodes   = 9;
_Alignas(32) int64_t const orwl_taskdep_max_inp = 4;
_Alignas(32) int64_t const orwl_taskdep_max_out = 4;

// Add symbols for Fortran compatibility.

_Alignas(32) int64_t const ORWL_TASKDEP_FORT(orwl_taskdep_vsize)   = orwl_taskdep_vsize;

ORWL_TASKDEP_ALIAS(orwl_taskdep_deg_out);
ORWL_TASKDEP_ALIAS(orwl_taskdep_deg_inp);
ORWL_TASKDEP_ALIAS(orwl_taskdep_nodes);
ORWL_TASKDEP_ALIAS(orwl_taskdep_max_inp);
ORWL_TASKDEP_ALIAS(orwl_taskdep_max_out);

// A wrapper as generic task function that just launches the specific function
ORWL_DEFINE_TASK(orwl_taskdep_type) {
    ORWL_THREAD_USE(orwl_taskdep_type, argc, argv);
    int64_t const tid = orwl_mytid;
    orwl_taskdep_runs[tid](argc, argv);
}

int main(int argc, char* argv[argc+1]) {
  p99_getopt_initialize(&argc, &argv);

  ORWL_TIMER() {
    orwl_init();

    if (orwl_nt != 9) {
        report(1, "We need exactly 9 task, to process this graph, current value is %zu, exiting.", orwl_nt);
        return EXIT_FAILURE;
    }

    //! [launch one thread per task]
    for (size_t i = 0; i < orwl_lt; i++) {
      orwl_taskdep_type* task = P99_NEW(orwl_taskdep_type, argc, argv);
      orwl_taskdep_type_create_task(task, orwl_tids[i]);
    }
    //! [launch one thread per task]
  }
  return EXIT_SUCCESS;
}

// End of program for graph grid
// *********************************************************************************

