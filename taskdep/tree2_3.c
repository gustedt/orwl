// **********************************************
#include "orwl_taskdep.h"
#include "simple_functions.h"

// To simulate call by reference for Fortran we need a lot of
// temporaries.
#define FREF64(X) (int64_t const){ (X) }

P99_GETOPT_SYNOPSIS("A program derived from task graph 'tree2_3' with 7 nodes and 12 dependencies.");
char const*const orwl_taskdep_nnames[7] = {
	[0] = "node_0",
	[1] = "node_1",
	[2] = "node_2",
	[3] = "node_3",
	[4] = "node_4",
	[5] = "node_5",
	[6] = "node_6",
};
// **********************************************


// Task graph "tree2_3"


// **********************************************
// External information needed for the task graph

// The data type through which tasks communicate.

enum { orwl_taskdep_vsize = 3, };
typedef floating orwl_taskdep_data;

// ***************************************
// Types.

// The transfer type between tasks.
typedef orwl_taskdep_data orwl_taskdep_vector[orwl_taskdep_vsize];

// The task functions that are used.
//
typedef void orwl_taskdep_ftype(orwl_taskdep_blob state[static restrict 1], int64_t const tid[static 1], int64_t const phase[static 1],
                int64_t const in_num[static 1], orwl_taskdep_vector in_mat[restrict in_num[0]], _Bool skip_in[restrict in_num[0]],
                int64_t const out_num[static 1], orwl_taskdep_vector out_mat[restrict out_num[0]], _Bool skip_out[restrict out_num[0]]);
typedef void orwl_taskdep_stype(orwl_taskdep_blob state[static restrict 1], int64_t const num[static 1]);

// *****************************************************************************
// Static properties of the task graph

// we have 7 nodes/regions

extern void regular(orwl_taskdep_blob state[static restrict 1], int64_t const tid[static 1], int64_t const phase[static 1],
    int64_t const in_num[static 1], orwl_taskdep_vector in_mat[restrict in_num[0]], _Bool skip_in[restrict in_num[0]],
    int64_t const out_num[static 1], orwl_taskdep_vector out_mat[restrict out_num[0]], _Bool skip_out[restrict out_num[0]]);
extern void orwl_taskdep_start(orwl_taskdep_blob state[static restrict 1], int64_t const num[static 1]);
extern void orwl_taskdep_shutdown(orwl_taskdep_blob state[static restrict 1], int64_t const num[static 1]);
char const*const orwl_taskdep_snames[7] = {
	[0] = "orwl_taskdep_start",
	[1] = "orwl_taskdep_start",
	[2] = "orwl_taskdep_start",
	[3] = "orwl_taskdep_start",
	[4] = "orwl_taskdep_start",
	[5] = "orwl_taskdep_start",
	[6] = "orwl_taskdep_start",
};
char const*const orwl_taskdep_fnames[7] = {
	[0] = "regular",
	[1] = "regular",
	[2] = "regular",
	[3] = "regular",
	[4] = "regular",
	[5] = "regular",
	[6] = "regular",
};
char const*const orwl_taskdep_dnames[7] = {
	[0] = "orwl_taskdep_shutdown",
	[1] = "orwl_taskdep_shutdown",
	[2] = "orwl_taskdep_shutdown",
	[3] = "orwl_taskdep_shutdown",
	[4] = "orwl_taskdep_shutdown",
	[5] = "orwl_taskdep_shutdown",
	[6] = "orwl_taskdep_shutdown",
};

// Initializers for ports. The table index is the index at the source, since only
// these are guaranteed to be unique. The values here are pointers to const
// qualified compound literals, or 0 if no "init" has been given for the port.
static orwl_taskdep_vector const*const orwl_taskdep_defaults[7][3] = {
	[0] = {
		[0] = 0, // no pre-init for down_1
		[1] = 0, // no pre-init for up_0
		[2] = 0, // no pre-init for down_2
	},
	[1] = {
		[0] = 0, // no pre-init for down_3
		[1] = &(orwl_taskdep_vector const){ +0 }, // up_1
		[2] = 0, // no pre-init for down_4
	},
	[2] = {
		[0] = 0, // no pre-init for down_5
		[1] = &(orwl_taskdep_vector const){ +0 }, // up_2
		[2] = 0, // no pre-init for down_6
	},
	[3] = {
		[0] = 0, // no pre-init for down_7
		[1] = &(orwl_taskdep_vector const){ +0 }, // up_3
		[2] = 0, // no pre-init for down_8
	},
	[4] = {
		[0] = 0, // no pre-init for down_9
		[1] = &(orwl_taskdep_vector const){ +0 }, // up_4
		[2] = 0, // no pre-init for down_10
	},
	[5] = {
		[0] = 0, // no pre-init for down_11
		[1] = &(orwl_taskdep_vector const){ +0 }, // up_5
		[2] = 0, // no pre-init for down_12
	},
	[6] = {
		[0] = 0, // no pre-init for down_13
		[1] = &(orwl_taskdep_vector const){ +0 }, // up_6
		[2] = 0, // no pre-init for down_14
	},
};

// The const property for ports. The table index is the index at the source, since only
// these are guaranteed to be unique. .
static bool const orwl_taskdep_const[7][3] = {
	[0] = {
		[0] = 0, // down_1
		[1] = 0, // up_0
		[2] = 0, // down_2
	},
	[1] = {
		[0] = 0, // down_3
		[1] = 0, // up_1
		[2] = 0, // down_4
	},
	[2] = {
		[0] = 0, // down_5
		[1] = 0, // up_2
		[2] = 0, // down_6
	},
	[3] = {
		[0] = 0, // down_7
		[1] = 0, // up_3
		[2] = 0, // down_8
	},
	[4] = {
		[0] = 0, // down_9
		[1] = 0, // up_4
		[2] = 0, // down_10
	},
	[5] = {
		[0] = 0, // down_11
		[1] = 0, // up_5
		[2] = 0, // down_12
	},
	[6] = {
		[0] = 0, // down_13
		[1] = 0, // up_6
		[2] = 0, // down_14
	},
};

// For each output port, provide the textual information about the link
char const*const orwl_taskdep_oports[7][3] = {
	[0] = {
		[0] = "down_1",
		[1] = "up_0",
		[2] = "down_2",
	},
	[1] = {
		[0] = "down_3",
		[1] = "up_1",
		[2] = "down_4",
	},
	[2] = {
		[0] = "down_5",
		[1] = "up_2",
		[2] = "down_6",
	},
	[3] = {
		[0] = "down_7",
		[1] = "up_3",
		[2] = "down_8",
	},
	[4] = {
		[0] = "down_9",
		[1] = "up_4",
		[2] = "down_10",
	},
	[5] = {
		[0] = "down_11",
		[1] = "up_5",
		[2] = "down_12",
	},
	[6] = {
		[0] = "down_13",
		[1] = "up_6",
		[2] = "down_14",
	},
};

// For each input port, provide the textual information about the link
char const*const orwl_taskdep_iports[7][3] = {
	[0] = {
		[0] = "up_1",
		[1] = "down_0",
		[2] = "up_2",
	},
	[1] = {
		[0] = "up_3",
		[1] = "down_1",
		[2] = "up_4",
	},
	[2] = {
		[0] = "up_5",
		[1] = "down_2",
		[2] = "up_6",
	},
	[3] = {
		[0] = "up_7",
		[1] = "down_3",
		[2] = "up_8",
	},
	[4] = {
		[0] = "up_9",
		[1] = "down_4",
		[2] = "up_10",
	},
	[5] = {
		[0] = "up_11",
		[1] = "down_5",
		[2] = "up_12",
	},
	[6] = {
		[0] = "up_13",
		[1] = "down_6",
		[2] = "up_14",
	},
};

// The outdegree for each node
_Alignas(32) int64_t const orwl_taskdep_deg_out[7] = {
	[0] = 3,
	[1] = 3,
	[2] = 3,
	[3] = 3,
	[4] = 3,
	[5] = 3,
	[6] = 3,
};

// The indegree for each node
_Alignas(32) int64_t const orwl_taskdep_deg_inp[7] = {
	[0] = 3,
	[1] = 3,
	[2] = 3,
	[3] = 3,
	[4] = 3,
	[5] = 3,
	[6] = 3,
};

// The number of each output port, or -1 if not this number is not available
int64_t const*const orwl_taskdep_onum[7] = {
	[0] = (int64_t const[3]){
		[0] = -1,
		[1] = -1,
		[2] = -1,
	},
	[1] = (int64_t const[3]){
		[0] = -1,
		[1] = -1,
		[2] = -1,
	},
	[2] = (int64_t const[3]){
		[0] = -1,
		[1] = -1,
		[2] = -1,
	},
	[3] = (int64_t const[3]){
		[0] = -1,
		[1] = -1,
		[2] = -1,
	},
	[4] = (int64_t const[3]){
		[0] = -1,
		[1] = -1,
		[2] = -1,
	},
	[5] = (int64_t const[3]){
		[0] = -1,
		[1] = -1,
		[2] = -1,
	},
	[6] = (int64_t const[3]){
		[0] = -1,
		[1] = -1,
		[2] = -1,
	},
};

// The number of each input port, or -1 if not this number is not available
int64_t const*const orwl_taskdep_inum[7] = {
	[0] = (int64_t const[3]){
		[0] = -1,
		[1] = -1,
		[2] = -1,
	},
	[1] = (int64_t const[3]){
		[0] = -1,
		[1] = -1,
		[2] = -1,
	},
	[2] = (int64_t const[3]){
		[0] = -1,
		[1] = -1,
		[2] = -1,
	},
	[3] = (int64_t const[3]){
		[0] = -1,
		[1] = -1,
		[2] = -1,
	},
	[4] = (int64_t const[3]){
		[0] = -1,
		[1] = -1,
		[2] = -1,
	},
	[5] = (int64_t const[3]){
		[0] = -1,
		[1] = -1,
		[2] = -1,
	},
	[6] = (int64_t const[3]){
		[0] = -1,
		[1] = -1,
		[2] = -1,
	},
};

// For each input port, provide the source port
static int64_t const orwl_taskdep_tail[7][3] = {
	[0] = {
		[0] = 4,
		[1] = -1,
		[2] = 7,
	},
	[1] = {
		[0] = 10,
		[1] = 0,
		[2] = 13,
	},
	[2] = {
		[0] = 16,
		[1] = 2,
		[2] = 19,
	},
	[3] = {
		[0] = -1,
		[1] = 3,
		[2] = -1,
	},
	[4] = {
		[0] = -1,
		[1] = 5,
		[2] = -1,
	},
	[5] = {
		[0] = -1,
		[1] = 6,
		[2] = -1,
	},
	[6] = {
		[0] = -1,
		[1] = 8,
		[2] = -1,
	},
};

void orwl_taskdep_usage(void) {
    printf("task number\tname\tfunction\tstartup\tshutdown\t(in ports)\t(out ports)\n");
    for (size_t i = 0; i < 7; ++i) {
        printf("%zu\t\t%s\t%s\t%s\t%s", i, orwl_taskdep_nnames[i], orwl_taskdep_fnames[i], orwl_taskdep_snames[i], orwl_taskdep_dnames[i]);
        if (orwl_taskdep_iports[i][0]) {
            printf("\t(%s", orwl_taskdep_iports[i][0]);
            for (size_t j = 1; j < 3; ++j) {
                if (orwl_taskdep_iports[i][j])
                   printf(", %s", orwl_taskdep_iports[i][j]);
            }
            printf(")");
        } else {
            printf("	()");
        }
        if (orwl_taskdep_oports[i][0]) {
            printf("\t(%s", orwl_taskdep_oports[i][0]);
            for (size_t j = 1; j < 3; ++j) {
                if (orwl_taskdep_oports[i][j])
                   printf(", %s", orwl_taskdep_oports[i][j]);
            }
            printf(")");
        } else {
            printf("	()");
        }
        printf("\n");
    }

}

P99_GETOPT_CALLBACK(orwl_taskdep_usage);

// *****************************************************
// Instantiate the ORWL structure.
ORWL_LOCATIONS_PER_TASK(
	orwl_task_write_if0,
	orwl_task_read_if0,
	orwl_task_write_if1,
	orwl_task_read_if1,
	orwl_task_write_if2,
	orwl_task_read_if2
);

ORWL_LOCATIONS_PER_TASK_INSTANTIATION();

ORWL_DECLARE_TASK(orwl_taskdep_type);

// *****************************************************
// Set up the ORWL structure that reflects dependencies.

// This graph is iterated.
// Using orwl_handle2.
// node_0	→ node_1	(down_1)
// 0	→ 1
// priorities: 0 1
// node_0	→ node_2	(down_2)
// 0	→ 2
// priorities: 0 1
// node_1	→ node_0	(up_1)
// 1	→ 0
// using reverse edge "down_1 (up_1)"
// priorities: 1 0
// node_1	→ node_3	(down_3)
// 1	→ 3
// priorities: 0 1
// node_1	→ node_4	(down_4)
// 1	→ 4
// priorities: 0 1
// node_2	→ node_0	(up_2)
// 2	→ 0
// using reverse edge "down_2 (up_2)"
// priorities: 1 0
// node_2	→ node_5	(down_5)
// 2	→ 5
// priorities: 0 1
// node_2	→ node_6	(down_6)
// 2	→ 6
// priorities: 0 1
// node_3	→ node_1	(up_3)
// 3	→ 1
// using reverse edge "down_3 (up_3)"
// priorities: 1 0
// node_4	→ node_1	(up_4)
// 4	→ 1
// using reverse edge "down_4 (up_4)"
// priorities: 1 0
// node_5	→ node_2	(up_5)
// 5	→ 2
// using reverse edge "down_5 (up_5)"
// priorities: 1 0
// node_6	→ node_2	(up_6)
// 6	→ 2
// using reverse edge "down_6 (up_6)"
// priorities: 1 0

// ****************************************************************
// Per task specific initialization functions. They should ease some
// const propagation concerning the task ID.
static void orwl_taskdep_init_0(orwl_handle2 hdl_in[static 3], orwl_handle2 hdl_out[static 3]) {
	report(orwl_taskdep_verbose, "0, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(0, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(0, orwl_task_write_if0), ORWL_LOCATION(0, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(0, orwl_task_write_if1), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(0, orwl_task_write_if1), ORWL_LOCATION(0, orwl_task_read_if1), 10);
	orwl_write_insert(&hdl_out[2], ORWL_LOCATION(0, orwl_task_write_if2), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(0, orwl_task_write_if2), ORWL_LOCATION(0, orwl_task_read_if2), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(1, orwl_task_read_if1), 0, seed, server);
	// const port at 1
	orwl_read_insert(&hdl_in[2], ORWL_LOCATION(2, orwl_task_read_if1), 0, seed, server);

	report(orwl_taskdep_verbose, "0, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "0, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_1(orwl_handle2 hdl_in[static 3], orwl_handle2 hdl_out[static 3]) {
	report(orwl_taskdep_verbose, "1, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(1, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(1, orwl_task_write_if0), ORWL_LOCATION(1, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(1, orwl_task_write_if1), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(1, orwl_task_write_if1), ORWL_LOCATION(1, orwl_task_read_if1), 10);
	orwl_write_insert(&hdl_out[2], ORWL_LOCATION(1, orwl_task_write_if2), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(1, orwl_task_write_if2), ORWL_LOCATION(1, orwl_task_read_if2), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(3, orwl_task_read_if1), 0, seed, server);
	orwl_read_insert(&hdl_in[1], ORWL_LOCATION(0, orwl_task_read_if0), 1, seed, server);
	orwl_read_insert(&hdl_in[2], ORWL_LOCATION(4, orwl_task_read_if1), 0, seed, server);

	report(orwl_taskdep_verbose, "1, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "1, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_2(orwl_handle2 hdl_in[static 3], orwl_handle2 hdl_out[static 3]) {
	report(orwl_taskdep_verbose, "2, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(2, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(2, orwl_task_write_if0), ORWL_LOCATION(2, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(2, orwl_task_write_if1), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(2, orwl_task_write_if1), ORWL_LOCATION(2, orwl_task_read_if1), 10);
	orwl_write_insert(&hdl_out[2], ORWL_LOCATION(2, orwl_task_write_if2), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(2, orwl_task_write_if2), ORWL_LOCATION(2, orwl_task_read_if2), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(5, orwl_task_read_if1), 0, seed, server);
	orwl_read_insert(&hdl_in[1], ORWL_LOCATION(0, orwl_task_read_if2), 1, seed, server);
	orwl_read_insert(&hdl_in[2], ORWL_LOCATION(6, orwl_task_read_if1), 0, seed, server);

	report(orwl_taskdep_verbose, "2, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "2, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_3(orwl_handle2 hdl_in[static 3], orwl_handle2 hdl_out[static 3]) {
	report(orwl_taskdep_verbose, "3, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(3, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(3, orwl_task_write_if0), ORWL_LOCATION(3, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(3, orwl_task_write_if1), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(3, orwl_task_write_if1), ORWL_LOCATION(3, orwl_task_read_if1), 10);
	orwl_write_insert(&hdl_out[2], ORWL_LOCATION(3, orwl_task_write_if2), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(3, orwl_task_write_if2), ORWL_LOCATION(3, orwl_task_read_if2), 10);
	// const port at 0
	orwl_read_insert(&hdl_in[1], ORWL_LOCATION(1, orwl_task_read_if0), 1, seed, server);
	// const port at 2

	report(orwl_taskdep_verbose, "3, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "3, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_4(orwl_handle2 hdl_in[static 3], orwl_handle2 hdl_out[static 3]) {
	report(orwl_taskdep_verbose, "4, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(4, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(4, orwl_task_write_if0), ORWL_LOCATION(4, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(4, orwl_task_write_if1), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(4, orwl_task_write_if1), ORWL_LOCATION(4, orwl_task_read_if1), 10);
	orwl_write_insert(&hdl_out[2], ORWL_LOCATION(4, orwl_task_write_if2), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(4, orwl_task_write_if2), ORWL_LOCATION(4, orwl_task_read_if2), 10);
	// const port at 0
	orwl_read_insert(&hdl_in[1], ORWL_LOCATION(1, orwl_task_read_if2), 1, seed, server);
	// const port at 2

	report(orwl_taskdep_verbose, "4, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "4, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_5(orwl_handle2 hdl_in[static 3], orwl_handle2 hdl_out[static 3]) {
	report(orwl_taskdep_verbose, "5, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(5, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(5, orwl_task_write_if0), ORWL_LOCATION(5, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(5, orwl_task_write_if1), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(5, orwl_task_write_if1), ORWL_LOCATION(5, orwl_task_read_if1), 10);
	orwl_write_insert(&hdl_out[2], ORWL_LOCATION(5, orwl_task_write_if2), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(5, orwl_task_write_if2), ORWL_LOCATION(5, orwl_task_read_if2), 10);
	// const port at 0
	orwl_read_insert(&hdl_in[1], ORWL_LOCATION(2, orwl_task_read_if0), 1, seed, server);
	// const port at 2

	report(orwl_taskdep_verbose, "5, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "5, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_6(orwl_handle2 hdl_in[static 3], orwl_handle2 hdl_out[static 3]) {
	report(orwl_taskdep_verbose, "6, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(6, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(6, orwl_task_write_if0), ORWL_LOCATION(6, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(6, orwl_task_write_if1), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(6, orwl_task_write_if1), ORWL_LOCATION(6, orwl_task_read_if1), 10);
	orwl_write_insert(&hdl_out[2], ORWL_LOCATION(6, orwl_task_write_if2), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(6, orwl_task_write_if2), ORWL_LOCATION(6, orwl_task_read_if2), 10);
	// const port at 0
	orwl_read_insert(&hdl_in[1], ORWL_LOCATION(2, orwl_task_read_if2), 1, seed, server);
	// const port at 2

	report(orwl_taskdep_verbose, "6, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "6, finished orwl_taskdep_init");

};
static orwl_taskdep_vector const p_dummy = { 0 };

// Task function that is executed by task 0 (node_0)
static void orwl_taskdep_run_0(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #0 (node_0)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(0));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 3,
        odeg  = 3,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[3] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[3] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(0, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(0, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_0(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #0 (node_0) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[0][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[0][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 3][port % 3]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 3][port % 3];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 3][port % 3]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(0), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[0][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #0 (node_0) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(0));

    report(orwl_taskdep_verbose, "task #0 (node_0) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #0 (node_0) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #0 (node_0) ends");
}


// Task function that is executed by task 1 (node_1)
static void orwl_taskdep_run_1(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #1 (node_1)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(1));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 3,
        odeg  = 3,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[3] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[3] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(1, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(1, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_1(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #1 (node_1) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[1][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[1][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 3][port % 3]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 3][port % 3];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 3][port % 3]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(1), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[1][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #1 (node_1) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(1));

    report(orwl_taskdep_verbose, "task #1 (node_1) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #1 (node_1) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #1 (node_1) ends");
}


// Task function that is executed by task 2 (node_2)
static void orwl_taskdep_run_2(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #2 (node_2)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(2));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 3,
        odeg  = 3,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[3] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[3] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(2, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(2, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_2(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #2 (node_2) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[2][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[2][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 3][port % 3]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 3][port % 3];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 3][port % 3]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(2), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[2][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #2 (node_2) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(2));

    report(orwl_taskdep_verbose, "task #2 (node_2) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #2 (node_2) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #2 (node_2) ends");
}


// Task function that is executed by task 3 (node_3)
static void orwl_taskdep_run_3(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #3 (node_3)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(3));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 3,
        odeg  = 3,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[3] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[3] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(3, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(3, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_3(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #3 (node_3) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[3][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[3][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 3][port % 3]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 3][port % 3];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 3][port % 3]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(3), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[3][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #3 (node_3) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(3));

    report(orwl_taskdep_verbose, "task #3 (node_3) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #3 (node_3) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #3 (node_3) ends");
}


// Task function that is executed by task 4 (node_4)
static void orwl_taskdep_run_4(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #4 (node_4)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(4));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 3,
        odeg  = 3,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[3] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[3] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(4, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(4, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_4(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #4 (node_4) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[4][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[4][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 3][port % 3]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 3][port % 3];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 3][port % 3]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(4), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[4][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #4 (node_4) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(4));

    report(orwl_taskdep_verbose, "task #4 (node_4) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #4 (node_4) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #4 (node_4) ends");
}


// Task function that is executed by task 5 (node_5)
static void orwl_taskdep_run_5(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #5 (node_5)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(5));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 3,
        odeg  = 3,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[3] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[3] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(5, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(5, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_5(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #5 (node_5) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[5][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[5][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 3][port % 3]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 3][port % 3];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 3][port % 3]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(5), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[5][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #5 (node_5) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(5));

    report(orwl_taskdep_verbose, "task #5 (node_5) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #5 (node_5) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #5 (node_5) ends");
}


// Task function that is executed by task 6 (node_6)
static void orwl_taskdep_run_6(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #6 (node_6)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(6));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 3,
        odeg  = 3,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[3] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[3] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(6, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(6, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_6(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #6 (node_6) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[6][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[6][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 3][port % 3]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 3][port % 3];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 3][port % 3]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(6), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[6][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #6 (node_6) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(6));

    report(orwl_taskdep_verbose, "task #6 (node_6) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #6 (node_6) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #6 (node_6) ends");
}


static void (*const orwl_taskdep_runs[])(int argc, char* argv[argc+1]) = {
    orwl_taskdep_run_0,
    orwl_taskdep_run_1,
    orwl_taskdep_run_2,
    orwl_taskdep_run_3,
    orwl_taskdep_run_4,
    orwl_taskdep_run_5,
    orwl_taskdep_run_6,
};

_Alignas(32) int64_t const orwl_taskdep_nodes   = 7;
_Alignas(32) int64_t const orwl_taskdep_max_inp = 3;
_Alignas(32) int64_t const orwl_taskdep_max_out = 3;

// Add symbols for Fortran compatibility.

_Alignas(32) int64_t const ORWL_TASKDEP_FORT(orwl_taskdep_vsize)   = orwl_taskdep_vsize;

ORWL_TASKDEP_ALIAS(orwl_taskdep_deg_out);
ORWL_TASKDEP_ALIAS(orwl_taskdep_deg_inp);
ORWL_TASKDEP_ALIAS(orwl_taskdep_nodes);
ORWL_TASKDEP_ALIAS(orwl_taskdep_max_inp);
ORWL_TASKDEP_ALIAS(orwl_taskdep_max_out);

// A wrapper as generic task function that just launches the specific function
ORWL_DEFINE_TASK(orwl_taskdep_type) {
    ORWL_THREAD_USE(orwl_taskdep_type, argc, argv);
    int64_t const tid = orwl_mytid;
    orwl_taskdep_runs[tid](argc, argv);
}

int main(int argc, char* argv[argc+1]) {
  p99_getopt_initialize(&argc, &argv);

  ORWL_TIMER() {
    orwl_init();

    if (orwl_nt != 7) {
        report(1, "We need exactly 7 task, to process this graph, current value is %zu, exiting.", orwl_nt);
        return EXIT_FAILURE;
    }

    //! [launch one thread per task]
    for (size_t i = 0; i < orwl_lt; i++) {
      orwl_taskdep_type* task = P99_NEW(orwl_taskdep_type, argc, argv);
      orwl_taskdep_type_create_task(task, orwl_tids[i]);
    }
    //! [launch one thread per task]
  }
  return EXIT_SUCCESS;
}

// End of program for graph tree2_3
// *********************************************************************************

