// **********************************************
#include "orwl_taskdep.h"
#include "simple_functions.h"

// To simulate call by reference for Fortran we need a lot of
// temporaries.
#define FREF64(X) (int64_t const){ (X) }

P99_GETOPT_SYNOPSIS("A program derived from task graph 'triangle_7' with 28 nodes and 84 dependencies.");
char const*const orwl_taskdep_nnames[28] = {
	[0] = "node_1_1",
	[1] = "node_2_1",
	[2] = "node_2_2",
	[3] = "node_3_1",
	[4] = "node_3_2",
	[5] = "node_3_3",
	[6] = "node_4_1",
	[7] = "node_4_2",
	[8] = "node_4_3",
	[9] = "node_4_4",
	[10] = "node_5_1",
	[11] = "node_5_2",
	[12] = "node_5_3",
	[13] = "node_5_4",
	[14] = "node_5_5",
	[15] = "node_6_1",
	[16] = "node_6_2",
	[17] = "node_6_3",
	[18] = "node_6_4",
	[19] = "node_6_5",
	[20] = "node_6_6",
	[21] = "node_7_1",
	[22] = "node_7_2",
	[23] = "node_7_3",
	[24] = "node_7_4",
	[25] = "node_7_5",
	[26] = "node_7_6",
	[27] = "node_7_7",
};
// **********************************************


// Task graph "triangle_7"


// **********************************************
// External information needed for the task graph

// The data type through which tasks communicate.

enum { orwl_taskdep_vsize = 3, };
typedef floating orwl_taskdep_data;

// ***************************************
// Types.

// The transfer type between tasks.
typedef orwl_taskdep_data orwl_taskdep_vector[orwl_taskdep_vsize];

// The task functions that are used.
//
typedef void orwl_taskdep_ftype(orwl_taskdep_blob state[static restrict 1], int64_t const tid[static 1], int64_t const phase[static 1],
                int64_t const in_num[static 1], orwl_taskdep_vector in_mat[restrict in_num[0]], _Bool skip_in[restrict in_num[0]],
                int64_t const out_num[static 1], orwl_taskdep_vector out_mat[restrict out_num[0]], _Bool skip_out[restrict out_num[0]]);
typedef void orwl_taskdep_stype(orwl_taskdep_blob state[static restrict 1], int64_t const num[static 1]);

// *****************************************************************************
// Static properties of the task graph

// we have 28 nodes/regions

extern void regular(orwl_taskdep_blob state[static restrict 1], int64_t const tid[static 1], int64_t const phase[static 1],
    int64_t const in_num[static 1], orwl_taskdep_vector in_mat[restrict in_num[0]], _Bool skip_in[restrict in_num[0]],
    int64_t const out_num[static 1], orwl_taskdep_vector out_mat[restrict out_num[0]], _Bool skip_out[restrict out_num[0]]);
extern void orwl_taskdep_start(orwl_taskdep_blob state[static restrict 1], int64_t const num[static 1]);
extern void orwl_taskdep_shutdown(orwl_taskdep_blob state[static restrict 1], int64_t const num[static 1]);
char const*const orwl_taskdep_snames[28] = {
	[0] = "orwl_taskdep_start",
	[1] = "orwl_taskdep_start",
	[2] = "orwl_taskdep_start",
	[3] = "orwl_taskdep_start",
	[4] = "orwl_taskdep_start",
	[5] = "orwl_taskdep_start",
	[6] = "orwl_taskdep_start",
	[7] = "orwl_taskdep_start",
	[8] = "orwl_taskdep_start",
	[9] = "orwl_taskdep_start",
	[10] = "orwl_taskdep_start",
	[11] = "orwl_taskdep_start",
	[12] = "orwl_taskdep_start",
	[13] = "orwl_taskdep_start",
	[14] = "orwl_taskdep_start",
	[15] = "orwl_taskdep_start",
	[16] = "orwl_taskdep_start",
	[17] = "orwl_taskdep_start",
	[18] = "orwl_taskdep_start",
	[19] = "orwl_taskdep_start",
	[20] = "orwl_taskdep_start",
	[21] = "orwl_taskdep_start",
	[22] = "orwl_taskdep_start",
	[23] = "orwl_taskdep_start",
	[24] = "orwl_taskdep_start",
	[25] = "orwl_taskdep_start",
	[26] = "orwl_taskdep_start",
	[27] = "orwl_taskdep_start",
};
char const*const orwl_taskdep_fnames[28] = {
	[0] = "regular",
	[1] = "regular",
	[2] = "regular",
	[3] = "regular",
	[4] = "regular",
	[5] = "regular",
	[6] = "regular",
	[7] = "regular",
	[8] = "regular",
	[9] = "regular",
	[10] = "regular",
	[11] = "regular",
	[12] = "regular",
	[13] = "regular",
	[14] = "regular",
	[15] = "regular",
	[16] = "regular",
	[17] = "regular",
	[18] = "regular",
	[19] = "regular",
	[20] = "regular",
	[21] = "regular",
	[22] = "regular",
	[23] = "regular",
	[24] = "regular",
	[25] = "regular",
	[26] = "regular",
	[27] = "regular",
};
char const*const orwl_taskdep_dnames[28] = {
	[0] = "orwl_taskdep_shutdown",
	[1] = "orwl_taskdep_shutdown",
	[2] = "orwl_taskdep_shutdown",
	[3] = "orwl_taskdep_shutdown",
	[4] = "orwl_taskdep_shutdown",
	[5] = "orwl_taskdep_shutdown",
	[6] = "orwl_taskdep_shutdown",
	[7] = "orwl_taskdep_shutdown",
	[8] = "orwl_taskdep_shutdown",
	[9] = "orwl_taskdep_shutdown",
	[10] = "orwl_taskdep_shutdown",
	[11] = "orwl_taskdep_shutdown",
	[12] = "orwl_taskdep_shutdown",
	[13] = "orwl_taskdep_shutdown",
	[14] = "orwl_taskdep_shutdown",
	[15] = "orwl_taskdep_shutdown",
	[16] = "orwl_taskdep_shutdown",
	[17] = "orwl_taskdep_shutdown",
	[18] = "orwl_taskdep_shutdown",
	[19] = "orwl_taskdep_shutdown",
	[20] = "orwl_taskdep_shutdown",
	[21] = "orwl_taskdep_shutdown",
	[22] = "orwl_taskdep_shutdown",
	[23] = "orwl_taskdep_shutdown",
	[24] = "orwl_taskdep_shutdown",
	[25] = "orwl_taskdep_shutdown",
	[26] = "orwl_taskdep_shutdown",
	[27] = "orwl_taskdep_shutdown",
};

// Initializers for ports. The table index is the index at the source, since only
// these are guaranteed to be unique. The values here are pointers to const
// qualified compound literals, or 0 if no "init" has been given for the port.
static orwl_taskdep_vector const*const orwl_taskdep_defaults[28][4] = {
	[0] = {
		[0] = 0, // no pre-init for out_1_1_b
		[1] = 0, // no pre-init for out_1_1_l
		[2] = 0, // no pre-init for out_1_1_t
		[3] = 0, // no pre-init for out_1_1_r
	},
	[1] = {
		[0] = 0, // no pre-init for out_2_1_b
		[1] = &(orwl_taskdep_vector const){ +0 }, // out_2_1_l
		[2] = 0, // no pre-init for out_2_1_t
		[3] = 0, // no pre-init for out_2_1_r
	},
	[2] = {
		[0] = 0, // no pre-init for out_2_2_b
		[1] = 0, // no pre-init for out_2_2_l
		[2] = &(orwl_taskdep_vector const){ +0 }, // out_2_2_t
		[3] = 0, // no pre-init for out_2_2_r
	},
	[3] = {
		[0] = 0, // no pre-init for out_3_1_b
		[1] = &(orwl_taskdep_vector const){ +0 }, // out_3_1_l
		[2] = 0, // no pre-init for out_3_1_t
		[3] = 0, // no pre-init for out_3_1_r
	},
	[4] = {
		[0] = 0, // no pre-init for out_3_2_b
		[1] = &(orwl_taskdep_vector const){ +0 }, // out_3_2_l
		[2] = &(orwl_taskdep_vector const){ +0 }, // out_3_2_t
		[3] = 0, // no pre-init for out_3_2_r
	},
	[5] = {
		[0] = 0, // no pre-init for out_3_3_b
		[1] = 0, // no pre-init for out_3_3_l
		[2] = &(orwl_taskdep_vector const){ +0 }, // out_3_3_t
		[3] = 0, // no pre-init for out_3_3_r
	},
	[6] = {
		[0] = 0, // no pre-init for out_4_1_b
		[1] = &(orwl_taskdep_vector const){ +0 }, // out_4_1_l
		[2] = 0, // no pre-init for out_4_1_t
		[3] = 0, // no pre-init for out_4_1_r
	},
	[7] = {
		[0] = 0, // no pre-init for out_4_2_b
		[1] = &(orwl_taskdep_vector const){ +0 }, // out_4_2_l
		[2] = &(orwl_taskdep_vector const){ +0 }, // out_4_2_t
		[3] = 0, // no pre-init for out_4_2_r
	},
	[8] = {
		[0] = 0, // no pre-init for out_4_3_b
		[1] = &(orwl_taskdep_vector const){ +0 }, // out_4_3_l
		[2] = &(orwl_taskdep_vector const){ +0 }, // out_4_3_t
		[3] = 0, // no pre-init for out_4_3_r
	},
	[9] = {
		[0] = 0, // no pre-init for out_4_4_b
		[1] = 0, // no pre-init for out_4_4_l
		[2] = &(orwl_taskdep_vector const){ +0 }, // out_4_4_t
		[3] = 0, // no pre-init for out_4_4_r
	},
	[10] = {
		[0] = 0, // no pre-init for out_5_1_b
		[1] = &(orwl_taskdep_vector const){ +0 }, // out_5_1_l
		[2] = 0, // no pre-init for out_5_1_t
		[3] = 0, // no pre-init for out_5_1_r
	},
	[11] = {
		[0] = 0, // no pre-init for out_5_2_b
		[1] = &(orwl_taskdep_vector const){ +0 }, // out_5_2_l
		[2] = &(orwl_taskdep_vector const){ +0 }, // out_5_2_t
		[3] = 0, // no pre-init for out_5_2_r
	},
	[12] = {
		[0] = 0, // no pre-init for out_5_3_b
		[1] = &(orwl_taskdep_vector const){ +0 }, // out_5_3_l
		[2] = &(orwl_taskdep_vector const){ +0 }, // out_5_3_t
		[3] = 0, // no pre-init for out_5_3_r
	},
	[13] = {
		[0] = 0, // no pre-init for out_5_4_b
		[1] = &(orwl_taskdep_vector const){ +0 }, // out_5_4_l
		[2] = &(orwl_taskdep_vector const){ +0 }, // out_5_4_t
		[3] = 0, // no pre-init for out_5_4_r
	},
	[14] = {
		[0] = 0, // no pre-init for out_5_5_b
		[1] = 0, // no pre-init for out_5_5_l
		[2] = &(orwl_taskdep_vector const){ +0 }, // out_5_5_t
		[3] = 0, // no pre-init for out_5_5_r
	},
	[15] = {
		[0] = 0, // no pre-init for out_6_1_b
		[1] = &(orwl_taskdep_vector const){ +0 }, // out_6_1_l
		[2] = 0, // no pre-init for out_6_1_t
		[3] = 0, // no pre-init for out_6_1_r
	},
	[16] = {
		[0] = 0, // no pre-init for out_6_2_b
		[1] = &(orwl_taskdep_vector const){ +0 }, // out_6_2_l
		[2] = &(orwl_taskdep_vector const){ +0 }, // out_6_2_t
		[3] = 0, // no pre-init for out_6_2_r
	},
	[17] = {
		[0] = 0, // no pre-init for out_6_3_b
		[1] = &(orwl_taskdep_vector const){ +0 }, // out_6_3_l
		[2] = &(orwl_taskdep_vector const){ +0 }, // out_6_3_t
		[3] = 0, // no pre-init for out_6_3_r
	},
	[18] = {
		[0] = 0, // no pre-init for out_6_4_b
		[1] = &(orwl_taskdep_vector const){ +0 }, // out_6_4_l
		[2] = &(orwl_taskdep_vector const){ +0 }, // out_6_4_t
		[3] = 0, // no pre-init for out_6_4_r
	},
	[19] = {
		[0] = 0, // no pre-init for out_6_5_b
		[1] = &(orwl_taskdep_vector const){ +0 }, // out_6_5_l
		[2] = &(orwl_taskdep_vector const){ +0 }, // out_6_5_t
		[3] = 0, // no pre-init for out_6_5_r
	},
	[20] = {
		[0] = 0, // no pre-init for out_6_6_b
		[1] = 0, // no pre-init for out_6_6_l
		[2] = &(orwl_taskdep_vector const){ +0 }, // out_6_6_t
		[3] = 0, // no pre-init for out_6_6_r
	},
	[21] = {
		[0] = 0, // no pre-init for out_7_1_b
		[1] = &(orwl_taskdep_vector const){ +0 }, // out_7_1_l
		[2] = 0, // no pre-init for out_7_1_t
		[3] = 0, // no pre-init for out_7_1_r
	},
	[22] = {
		[0] = 0, // no pre-init for out_7_2_b
		[1] = &(orwl_taskdep_vector const){ +0 }, // out_7_2_l
		[2] = &(orwl_taskdep_vector const){ +0 }, // out_7_2_t
		[3] = 0, // no pre-init for out_7_2_r
	},
	[23] = {
		[0] = 0, // no pre-init for out_7_3_b
		[1] = &(orwl_taskdep_vector const){ +0 }, // out_7_3_l
		[2] = &(orwl_taskdep_vector const){ +0 }, // out_7_3_t
		[3] = 0, // no pre-init for out_7_3_r
	},
	[24] = {
		[0] = 0, // no pre-init for out_7_4_b
		[1] = &(orwl_taskdep_vector const){ +0 }, // out_7_4_l
		[2] = &(orwl_taskdep_vector const){ +0 }, // out_7_4_t
		[3] = 0, // no pre-init for out_7_4_r
	},
	[25] = {
		[0] = 0, // no pre-init for out_7_5_b
		[1] = &(orwl_taskdep_vector const){ +0 }, // out_7_5_l
		[2] = &(orwl_taskdep_vector const){ +0 }, // out_7_5_t
		[3] = 0, // no pre-init for out_7_5_r
	},
	[26] = {
		[0] = 0, // no pre-init for out_7_6_b
		[1] = &(orwl_taskdep_vector const){ +0 }, // out_7_6_l
		[2] = &(orwl_taskdep_vector const){ +0 }, // out_7_6_t
		[3] = 0, // no pre-init for out_7_6_r
	},
	[27] = {
		[0] = 0, // no pre-init for out_7_7_b
		[1] = 0, // no pre-init for out_7_7_l
		[2] = &(orwl_taskdep_vector const){ +0 }, // out_7_7_t
		[3] = 0, // no pre-init for out_7_7_r
	},
};

// The const property for ports. The table index is the index at the source, since only
// these are guaranteed to be unique. .
static bool const orwl_taskdep_const[28][4] = {
	[0] = {
		[0] = 0, // out_1_1_b
		[1] = 0, // out_1_1_l
		[2] = 0, // out_1_1_t
		[3] = 0, // out_1_1_r
	},
	[1] = {
		[0] = 0, // out_2_1_b
		[1] = 0, // out_2_1_l
		[2] = 0, // out_2_1_t
		[3] = 0, // out_2_1_r
	},
	[2] = {
		[0] = 0, // out_2_2_b
		[1] = 0, // out_2_2_l
		[2] = 0, // out_2_2_t
		[3] = 0, // out_2_2_r
	},
	[3] = {
		[0] = 0, // out_3_1_b
		[1] = 0, // out_3_1_l
		[2] = 0, // out_3_1_t
		[3] = 0, // out_3_1_r
	},
	[4] = {
		[0] = 0, // out_3_2_b
		[1] = 0, // out_3_2_l
		[2] = 0, // out_3_2_t
		[3] = 0, // out_3_2_r
	},
	[5] = {
		[0] = 0, // out_3_3_b
		[1] = 0, // out_3_3_l
		[2] = 0, // out_3_3_t
		[3] = 0, // out_3_3_r
	},
	[6] = {
		[0] = 0, // out_4_1_b
		[1] = 0, // out_4_1_l
		[2] = 0, // out_4_1_t
		[3] = 0, // out_4_1_r
	},
	[7] = {
		[0] = 0, // out_4_2_b
		[1] = 0, // out_4_2_l
		[2] = 0, // out_4_2_t
		[3] = 0, // out_4_2_r
	},
	[8] = {
		[0] = 0, // out_4_3_b
		[1] = 0, // out_4_3_l
		[2] = 0, // out_4_3_t
		[3] = 0, // out_4_3_r
	},
	[9] = {
		[0] = 0, // out_4_4_b
		[1] = 0, // out_4_4_l
		[2] = 0, // out_4_4_t
		[3] = 0, // out_4_4_r
	},
	[10] = {
		[0] = 0, // out_5_1_b
		[1] = 0, // out_5_1_l
		[2] = 0, // out_5_1_t
		[3] = 0, // out_5_1_r
	},
	[11] = {
		[0] = 0, // out_5_2_b
		[1] = 0, // out_5_2_l
		[2] = 0, // out_5_2_t
		[3] = 0, // out_5_2_r
	},
	[12] = {
		[0] = 0, // out_5_3_b
		[1] = 0, // out_5_3_l
		[2] = 0, // out_5_3_t
		[3] = 0, // out_5_3_r
	},
	[13] = {
		[0] = 0, // out_5_4_b
		[1] = 0, // out_5_4_l
		[2] = 0, // out_5_4_t
		[3] = 0, // out_5_4_r
	},
	[14] = {
		[0] = 0, // out_5_5_b
		[1] = 0, // out_5_5_l
		[2] = 0, // out_5_5_t
		[3] = 0, // out_5_5_r
	},
	[15] = {
		[0] = 0, // out_6_1_b
		[1] = 0, // out_6_1_l
		[2] = 0, // out_6_1_t
		[3] = 0, // out_6_1_r
	},
	[16] = {
		[0] = 0, // out_6_2_b
		[1] = 0, // out_6_2_l
		[2] = 0, // out_6_2_t
		[3] = 0, // out_6_2_r
	},
	[17] = {
		[0] = 0, // out_6_3_b
		[1] = 0, // out_6_3_l
		[2] = 0, // out_6_3_t
		[3] = 0, // out_6_3_r
	},
	[18] = {
		[0] = 0, // out_6_4_b
		[1] = 0, // out_6_4_l
		[2] = 0, // out_6_4_t
		[3] = 0, // out_6_4_r
	},
	[19] = {
		[0] = 0, // out_6_5_b
		[1] = 0, // out_6_5_l
		[2] = 0, // out_6_5_t
		[3] = 0, // out_6_5_r
	},
	[20] = {
		[0] = 0, // out_6_6_b
		[1] = 0, // out_6_6_l
		[2] = 0, // out_6_6_t
		[3] = 0, // out_6_6_r
	},
	[21] = {
		[0] = 0, // out_7_1_b
		[1] = 0, // out_7_1_l
		[2] = 0, // out_7_1_t
		[3] = 0, // out_7_1_r
	},
	[22] = {
		[0] = 0, // out_7_2_b
		[1] = 0, // out_7_2_l
		[2] = 0, // out_7_2_t
		[3] = 0, // out_7_2_r
	},
	[23] = {
		[0] = 0, // out_7_3_b
		[1] = 0, // out_7_3_l
		[2] = 0, // out_7_3_t
		[3] = 0, // out_7_3_r
	},
	[24] = {
		[0] = 0, // out_7_4_b
		[1] = 0, // out_7_4_l
		[2] = 0, // out_7_4_t
		[3] = 0, // out_7_4_r
	},
	[25] = {
		[0] = 0, // out_7_5_b
		[1] = 0, // out_7_5_l
		[2] = 0, // out_7_5_t
		[3] = 0, // out_7_5_r
	},
	[26] = {
		[0] = 0, // out_7_6_b
		[1] = 0, // out_7_6_l
		[2] = 0, // out_7_6_t
		[3] = 0, // out_7_6_r
	},
	[27] = {
		[0] = 0, // out_7_7_b
		[1] = 0, // out_7_7_l
		[2] = 0, // out_7_7_t
		[3] = 0, // out_7_7_r
	},
};

// For each output port, provide the textual information about the link
char const*const orwl_taskdep_oports[28][4] = {
	[0] = {
		[0] = "out_1_1_b",
		[1] = "out_1_1_l",
		[2] = "out_1_1_t",
		[3] = "out_1_1_r",
	},
	[1] = {
		[0] = "out_2_1_b",
		[1] = "out_2_1_l",
		[2] = "out_2_1_t",
		[3] = "out_2_1_r",
	},
	[2] = {
		[0] = "out_2_2_b",
		[1] = "out_2_2_l",
		[2] = "out_2_2_t",
		[3] = "out_2_2_r",
	},
	[3] = {
		[0] = "out_3_1_b",
		[1] = "out_3_1_l",
		[2] = "out_3_1_t",
		[3] = "out_3_1_r",
	},
	[4] = {
		[0] = "out_3_2_b",
		[1] = "out_3_2_l",
		[2] = "out_3_2_t",
		[3] = "out_3_2_r",
	},
	[5] = {
		[0] = "out_3_3_b",
		[1] = "out_3_3_l",
		[2] = "out_3_3_t",
		[3] = "out_3_3_r",
	},
	[6] = {
		[0] = "out_4_1_b",
		[1] = "out_4_1_l",
		[2] = "out_4_1_t",
		[3] = "out_4_1_r",
	},
	[7] = {
		[0] = "out_4_2_b",
		[1] = "out_4_2_l",
		[2] = "out_4_2_t",
		[3] = "out_4_2_r",
	},
	[8] = {
		[0] = "out_4_3_b",
		[1] = "out_4_3_l",
		[2] = "out_4_3_t",
		[3] = "out_4_3_r",
	},
	[9] = {
		[0] = "out_4_4_b",
		[1] = "out_4_4_l",
		[2] = "out_4_4_t",
		[3] = "out_4_4_r",
	},
	[10] = {
		[0] = "out_5_1_b",
		[1] = "out_5_1_l",
		[2] = "out_5_1_t",
		[3] = "out_5_1_r",
	},
	[11] = {
		[0] = "out_5_2_b",
		[1] = "out_5_2_l",
		[2] = "out_5_2_t",
		[3] = "out_5_2_r",
	},
	[12] = {
		[0] = "out_5_3_b",
		[1] = "out_5_3_l",
		[2] = "out_5_3_t",
		[3] = "out_5_3_r",
	},
	[13] = {
		[0] = "out_5_4_b",
		[1] = "out_5_4_l",
		[2] = "out_5_4_t",
		[3] = "out_5_4_r",
	},
	[14] = {
		[0] = "out_5_5_b",
		[1] = "out_5_5_l",
		[2] = "out_5_5_t",
		[3] = "out_5_5_r",
	},
	[15] = {
		[0] = "out_6_1_b",
		[1] = "out_6_1_l",
		[2] = "out_6_1_t",
		[3] = "out_6_1_r",
	},
	[16] = {
		[0] = "out_6_2_b",
		[1] = "out_6_2_l",
		[2] = "out_6_2_t",
		[3] = "out_6_2_r",
	},
	[17] = {
		[0] = "out_6_3_b",
		[1] = "out_6_3_l",
		[2] = "out_6_3_t",
		[3] = "out_6_3_r",
	},
	[18] = {
		[0] = "out_6_4_b",
		[1] = "out_6_4_l",
		[2] = "out_6_4_t",
		[3] = "out_6_4_r",
	},
	[19] = {
		[0] = "out_6_5_b",
		[1] = "out_6_5_l",
		[2] = "out_6_5_t",
		[3] = "out_6_5_r",
	},
	[20] = {
		[0] = "out_6_6_b",
		[1] = "out_6_6_l",
		[2] = "out_6_6_t",
		[3] = "out_6_6_r",
	},
	[21] = {
		[0] = "out_7_1_b",
		[1] = "out_7_1_l",
		[2] = "out_7_1_t",
		[3] = "out_7_1_r",
	},
	[22] = {
		[0] = "out_7_2_b",
		[1] = "out_7_2_l",
		[2] = "out_7_2_t",
		[3] = "out_7_2_r",
	},
	[23] = {
		[0] = "out_7_3_b",
		[1] = "out_7_3_l",
		[2] = "out_7_3_t",
		[3] = "out_7_3_r",
	},
	[24] = {
		[0] = "out_7_4_b",
		[1] = "out_7_4_l",
		[2] = "out_7_4_t",
		[3] = "out_7_4_r",
	},
	[25] = {
		[0] = "out_7_5_b",
		[1] = "out_7_5_l",
		[2] = "out_7_5_t",
		[3] = "out_7_5_r",
	},
	[26] = {
		[0] = "out_7_6_b",
		[1] = "out_7_6_l",
		[2] = "out_7_6_t",
		[3] = "out_7_6_r",
	},
	[27] = {
		[0] = "out_7_7_b",
		[1] = "out_7_7_l",
		[2] = "out_7_7_t",
		[3] = "out_7_7_r",
	},
};

// For each input port, provide the textual information about the link
char const*const orwl_taskdep_iports[28][4] = {
	[0] = {
		[0] = "out_0_1_r",
		[1] = "out_1_2_t",
		[2] = "out_2_1_l",
		[3] = "out_1_0_b",
	},
	[1] = {
		[0] = "out_1_1_r",
		[1] = "out_2_2_t",
		[2] = "out_3_1_l",
		[3] = "out_2_0_b",
	},
	[2] = {
		[0] = "out_1_2_r",
		[1] = "out_2_3_t",
		[2] = "out_3_2_l",
		[3] = "out_2_1_b",
	},
	[3] = {
		[0] = "out_2_1_r",
		[1] = "out_3_2_t",
		[2] = "out_4_1_l",
		[3] = "out_3_0_b",
	},
	[4] = {
		[0] = "out_2_2_r",
		[1] = "out_3_3_t",
		[2] = "out_4_2_l",
		[3] = "out_3_1_b",
	},
	[5] = {
		[0] = "out_2_3_r",
		[1] = "out_3_4_t",
		[2] = "out_4_3_l",
		[3] = "out_3_2_b",
	},
	[6] = {
		[0] = "out_3_1_r",
		[1] = "out_4_2_t",
		[2] = "out_5_1_l",
		[3] = "out_4_0_b",
	},
	[7] = {
		[0] = "out_3_2_r",
		[1] = "out_4_3_t",
		[2] = "out_5_2_l",
		[3] = "out_4_1_b",
	},
	[8] = {
		[0] = "out_3_3_r",
		[1] = "out_4_4_t",
		[2] = "out_5_3_l",
		[3] = "out_4_2_b",
	},
	[9] = {
		[0] = "out_3_4_r",
		[1] = "out_4_5_t",
		[2] = "out_5_4_l",
		[3] = "out_4_3_b",
	},
	[10] = {
		[0] = "out_4_1_r",
		[1] = "out_5_2_t",
		[2] = "out_6_1_l",
		[3] = "out_5_0_b",
	},
	[11] = {
		[0] = "out_4_2_r",
		[1] = "out_5_3_t",
		[2] = "out_6_2_l",
		[3] = "out_5_1_b",
	},
	[12] = {
		[0] = "out_4_3_r",
		[1] = "out_5_4_t",
		[2] = "out_6_3_l",
		[3] = "out_5_2_b",
	},
	[13] = {
		[0] = "out_4_4_r",
		[1] = "out_5_5_t",
		[2] = "out_6_4_l",
		[3] = "out_5_3_b",
	},
	[14] = {
		[0] = "out_4_5_r",
		[1] = "out_5_6_t",
		[2] = "out_6_5_l",
		[3] = "out_5_4_b",
	},
	[15] = {
		[0] = "out_5_1_r",
		[1] = "out_6_2_t",
		[2] = "out_7_1_l",
		[3] = "out_6_0_b",
	},
	[16] = {
		[0] = "out_5_2_r",
		[1] = "out_6_3_t",
		[2] = "out_7_2_l",
		[3] = "out_6_1_b",
	},
	[17] = {
		[0] = "out_5_3_r",
		[1] = "out_6_4_t",
		[2] = "out_7_3_l",
		[3] = "out_6_2_b",
	},
	[18] = {
		[0] = "out_5_4_r",
		[1] = "out_6_5_t",
		[2] = "out_7_4_l",
		[3] = "out_6_3_b",
	},
	[19] = {
		[0] = "out_5_5_r",
		[1] = "out_6_6_t",
		[2] = "out_7_5_l",
		[3] = "out_6_4_b",
	},
	[20] = {
		[0] = "out_5_6_r",
		[1] = "out_6_7_t",
		[2] = "out_7_6_l",
		[3] = "out_6_5_b",
	},
	[21] = {
		[0] = "out_6_1_r",
		[1] = "out_7_2_t",
		[2] = "out_8_1_l",
		[3] = "out_7_0_b",
	},
	[22] = {
		[0] = "out_6_2_r",
		[1] = "out_7_3_t",
		[2] = "out_8_2_l",
		[3] = "out_7_1_b",
	},
	[23] = {
		[0] = "out_6_3_r",
		[1] = "out_7_4_t",
		[2] = "out_8_3_l",
		[3] = "out_7_2_b",
	},
	[24] = {
		[0] = "out_6_4_r",
		[1] = "out_7_5_t",
		[2] = "out_8_4_l",
		[3] = "out_7_3_b",
	},
	[25] = {
		[0] = "out_6_5_r",
		[1] = "out_7_6_t",
		[2] = "out_8_5_l",
		[3] = "out_7_4_b",
	},
	[26] = {
		[0] = "out_6_6_r",
		[1] = "out_7_7_t",
		[2] = "out_8_6_l",
		[3] = "out_7_5_b",
	},
	[27] = {
		[0] = "out_6_7_r",
		[1] = "out_7_8_t",
		[2] = "out_8_7_l",
		[3] = "out_7_6_b",
	},
};

// The outdegree for each node
_Alignas(32) int64_t const orwl_taskdep_deg_out[28] = {
	[0] = 4,
	[1] = 4,
	[2] = 4,
	[3] = 4,
	[4] = 4,
	[5] = 4,
	[6] = 4,
	[7] = 4,
	[8] = 4,
	[9] = 4,
	[10] = 4,
	[11] = 4,
	[12] = 4,
	[13] = 4,
	[14] = 4,
	[15] = 4,
	[16] = 4,
	[17] = 4,
	[18] = 4,
	[19] = 4,
	[20] = 4,
	[21] = 4,
	[22] = 4,
	[23] = 4,
	[24] = 4,
	[25] = 4,
	[26] = 4,
	[27] = 4,
};

// The indegree for each node
_Alignas(32) int64_t const orwl_taskdep_deg_inp[28] = {
	[0] = 4,
	[1] = 4,
	[2] = 4,
	[3] = 4,
	[4] = 4,
	[5] = 4,
	[6] = 4,
	[7] = 4,
	[8] = 4,
	[9] = 4,
	[10] = 4,
	[11] = 4,
	[12] = 4,
	[13] = 4,
	[14] = 4,
	[15] = 4,
	[16] = 4,
	[17] = 4,
	[18] = 4,
	[19] = 4,
	[20] = 4,
	[21] = 4,
	[22] = 4,
	[23] = 4,
	[24] = 4,
	[25] = 4,
	[26] = 4,
	[27] = 4,
};

// The number of each output port, or -1 if not this number is not available
int64_t const*const orwl_taskdep_onum[28] = {
	[0] = (int64_t const[4]){
		[0] = 1,
		[1] = 1,
		[2] = 1,
		[3] = 1,
	},
	[1] = (int64_t const[4]){
		[0] = 2,
		[1] = 2,
		[2] = 2,
		[3] = 2,
	},
	[2] = (int64_t const[4]){
		[0] = 2,
		[1] = 2,
		[2] = 2,
		[3] = 2,
	},
	[3] = (int64_t const[4]){
		[0] = 3,
		[1] = 3,
		[2] = 3,
		[3] = 3,
	},
	[4] = (int64_t const[4]){
		[0] = 3,
		[1] = 3,
		[2] = 3,
		[3] = 3,
	},
	[5] = (int64_t const[4]){
		[0] = 3,
		[1] = 3,
		[2] = 3,
		[3] = 3,
	},
	[6] = (int64_t const[4]){
		[0] = 4,
		[1] = 4,
		[2] = 4,
		[3] = 4,
	},
	[7] = (int64_t const[4]){
		[0] = 4,
		[1] = 4,
		[2] = 4,
		[3] = 4,
	},
	[8] = (int64_t const[4]){
		[0] = 4,
		[1] = 4,
		[2] = 4,
		[3] = 4,
	},
	[9] = (int64_t const[4]){
		[0] = 4,
		[1] = 4,
		[2] = 4,
		[3] = 4,
	},
	[10] = (int64_t const[4]){
		[0] = 5,
		[1] = 5,
		[2] = 5,
		[3] = 5,
	},
	[11] = (int64_t const[4]){
		[0] = 5,
		[1] = 5,
		[2] = 5,
		[3] = 5,
	},
	[12] = (int64_t const[4]){
		[0] = 5,
		[1] = 5,
		[2] = 5,
		[3] = 5,
	},
	[13] = (int64_t const[4]){
		[0] = 5,
		[1] = 5,
		[2] = 5,
		[3] = 5,
	},
	[14] = (int64_t const[4]){
		[0] = 5,
		[1] = 5,
		[2] = 5,
		[3] = 5,
	},
	[15] = (int64_t const[4]){
		[0] = 6,
		[1] = 6,
		[2] = 6,
		[3] = 6,
	},
	[16] = (int64_t const[4]){
		[0] = 6,
		[1] = 6,
		[2] = 6,
		[3] = 6,
	},
	[17] = (int64_t const[4]){
		[0] = 6,
		[1] = 6,
		[2] = 6,
		[3] = 6,
	},
	[18] = (int64_t const[4]){
		[0] = 6,
		[1] = 6,
		[2] = 6,
		[3] = 6,
	},
	[19] = (int64_t const[4]){
		[0] = 6,
		[1] = 6,
		[2] = 6,
		[3] = 6,
	},
	[20] = (int64_t const[4]){
		[0] = 6,
		[1] = 6,
		[2] = 6,
		[3] = 6,
	},
	[21] = (int64_t const[4]){
		[0] = 7,
		[1] = 7,
		[2] = 7,
		[3] = 7,
	},
	[22] = (int64_t const[4]){
		[0] = 7,
		[1] = 7,
		[2] = 7,
		[3] = 7,
	},
	[23] = (int64_t const[4]){
		[0] = 7,
		[1] = 7,
		[2] = 7,
		[3] = 7,
	},
	[24] = (int64_t const[4]){
		[0] = 7,
		[1] = 7,
		[2] = 7,
		[3] = 7,
	},
	[25] = (int64_t const[4]){
		[0] = 7,
		[1] = 7,
		[2] = 7,
		[3] = 7,
	},
	[26] = (int64_t const[4]){
		[0] = 7,
		[1] = 7,
		[2] = 7,
		[3] = 7,
	},
	[27] = (int64_t const[4]){
		[0] = 7,
		[1] = 7,
		[2] = 7,
		[3] = 7,
	},
};

// The number of each input port, or -1 if not this number is not available
int64_t const*const orwl_taskdep_inum[28] = {
	[0] = (int64_t const[4]){
		[0] = 1,
		[1] = 2,
		[2] = 1,
		[3] = 0,
	},
	[1] = (int64_t const[4]){
		[0] = 1,
		[1] = 2,
		[2] = 1,
		[3] = 0,
	},
	[2] = (int64_t const[4]){
		[0] = 2,
		[1] = 3,
		[2] = 2,
		[3] = 1,
	},
	[3] = (int64_t const[4]){
		[0] = 1,
		[1] = 2,
		[2] = 1,
		[3] = 0,
	},
	[4] = (int64_t const[4]){
		[0] = 2,
		[1] = 3,
		[2] = 2,
		[3] = 1,
	},
	[5] = (int64_t const[4]){
		[0] = 3,
		[1] = 4,
		[2] = 3,
		[3] = 2,
	},
	[6] = (int64_t const[4]){
		[0] = 1,
		[1] = 2,
		[2] = 1,
		[3] = 0,
	},
	[7] = (int64_t const[4]){
		[0] = 2,
		[1] = 3,
		[2] = 2,
		[3] = 1,
	},
	[8] = (int64_t const[4]){
		[0] = 3,
		[1] = 4,
		[2] = 3,
		[3] = 2,
	},
	[9] = (int64_t const[4]){
		[0] = 4,
		[1] = 5,
		[2] = 4,
		[3] = 3,
	},
	[10] = (int64_t const[4]){
		[0] = 1,
		[1] = 2,
		[2] = 1,
		[3] = 0,
	},
	[11] = (int64_t const[4]){
		[0] = 2,
		[1] = 3,
		[2] = 2,
		[3] = 1,
	},
	[12] = (int64_t const[4]){
		[0] = 3,
		[1] = 4,
		[2] = 3,
		[3] = 2,
	},
	[13] = (int64_t const[4]){
		[0] = 4,
		[1] = 5,
		[2] = 4,
		[3] = 3,
	},
	[14] = (int64_t const[4]){
		[0] = 5,
		[1] = 6,
		[2] = 5,
		[3] = 4,
	},
	[15] = (int64_t const[4]){
		[0] = 1,
		[1] = 2,
		[2] = 1,
		[3] = 0,
	},
	[16] = (int64_t const[4]){
		[0] = 2,
		[1] = 3,
		[2] = 2,
		[3] = 1,
	},
	[17] = (int64_t const[4]){
		[0] = 3,
		[1] = 4,
		[2] = 3,
		[3] = 2,
	},
	[18] = (int64_t const[4]){
		[0] = 4,
		[1] = 5,
		[2] = 4,
		[3] = 3,
	},
	[19] = (int64_t const[4]){
		[0] = 5,
		[1] = 6,
		[2] = 5,
		[3] = 4,
	},
	[20] = (int64_t const[4]){
		[0] = 6,
		[1] = 7,
		[2] = 6,
		[3] = 5,
	},
	[21] = (int64_t const[4]){
		[0] = 1,
		[1] = 2,
		[2] = 1,
		[3] = 0,
	},
	[22] = (int64_t const[4]){
		[0] = 2,
		[1] = 3,
		[2] = 2,
		[3] = 1,
	},
	[23] = (int64_t const[4]){
		[0] = 3,
		[1] = 4,
		[2] = 3,
		[3] = 2,
	},
	[24] = (int64_t const[4]){
		[0] = 4,
		[1] = 5,
		[2] = 4,
		[3] = 3,
	},
	[25] = (int64_t const[4]){
		[0] = 5,
		[1] = 6,
		[2] = 5,
		[3] = 4,
	},
	[26] = (int64_t const[4]){
		[0] = 6,
		[1] = 7,
		[2] = 6,
		[3] = 5,
	},
	[27] = (int64_t const[4]){
		[0] = 7,
		[1] = 8,
		[2] = 7,
		[3] = 6,
	},
};

// For each input port, provide the source port
static int64_t const orwl_taskdep_tail[28][4] = {
	[0] = {
		[0] = -1,
		[1] = -1,
		[2] = 5,
		[3] = -1,
	},
	[1] = {
		[0] = 3,
		[1] = 10,
		[2] = 13,
		[3] = -1,
	},
	[2] = {
		[0] = -1,
		[1] = -1,
		[2] = 17,
		[3] = 4,
	},
	[3] = {
		[0] = 7,
		[1] = 18,
		[2] = 25,
		[3] = -1,
	},
	[4] = {
		[0] = 11,
		[1] = 22,
		[2] = 29,
		[3] = 12,
	},
	[5] = {
		[0] = -1,
		[1] = -1,
		[2] = 33,
		[3] = 16,
	},
	[6] = {
		[0] = 15,
		[1] = 30,
		[2] = 41,
		[3] = -1,
	},
	[7] = {
		[0] = 19,
		[1] = 34,
		[2] = 45,
		[3] = 24,
	},
	[8] = {
		[0] = 23,
		[1] = 38,
		[2] = 49,
		[3] = 28,
	},
	[9] = {
		[0] = -1,
		[1] = -1,
		[2] = 53,
		[3] = 32,
	},
	[10] = {
		[0] = 27,
		[1] = 46,
		[2] = 61,
		[3] = -1,
	},
	[11] = {
		[0] = 31,
		[1] = 50,
		[2] = 65,
		[3] = 40,
	},
	[12] = {
		[0] = 35,
		[1] = 54,
		[2] = 69,
		[3] = 44,
	},
	[13] = {
		[0] = 39,
		[1] = 58,
		[2] = 73,
		[3] = 48,
	},
	[14] = {
		[0] = -1,
		[1] = -1,
		[2] = 77,
		[3] = 52,
	},
	[15] = {
		[0] = 43,
		[1] = 66,
		[2] = 85,
		[3] = -1,
	},
	[16] = {
		[0] = 47,
		[1] = 70,
		[2] = 89,
		[3] = 60,
	},
	[17] = {
		[0] = 51,
		[1] = 74,
		[2] = 93,
		[3] = 64,
	},
	[18] = {
		[0] = 55,
		[1] = 78,
		[2] = 97,
		[3] = 68,
	},
	[19] = {
		[0] = 59,
		[1] = 82,
		[2] = 101,
		[3] = 72,
	},
	[20] = {
		[0] = -1,
		[1] = -1,
		[2] = 105,
		[3] = 76,
	},
	[21] = {
		[0] = 63,
		[1] = 90,
		[2] = -1,
		[3] = -1,
	},
	[22] = {
		[0] = 67,
		[1] = 94,
		[2] = -1,
		[3] = 84,
	},
	[23] = {
		[0] = 71,
		[1] = 98,
		[2] = -1,
		[3] = 88,
	},
	[24] = {
		[0] = 75,
		[1] = 102,
		[2] = -1,
		[3] = 92,
	},
	[25] = {
		[0] = 79,
		[1] = 106,
		[2] = -1,
		[3] = 96,
	},
	[26] = {
		[0] = 83,
		[1] = 110,
		[2] = -1,
		[3] = 100,
	},
	[27] = {
		[0] = -1,
		[1] = -1,
		[2] = -1,
		[3] = 104,
	},
};

void orwl_taskdep_usage(void) {
    printf("task number\tname\tfunction\tstartup\tshutdown\t(in ports)\t(out ports)\n");
    for (size_t i = 0; i < 28; ++i) {
        printf("%zu\t\t%s\t%s\t%s\t%s", i, orwl_taskdep_nnames[i], orwl_taskdep_fnames[i], orwl_taskdep_snames[i], orwl_taskdep_dnames[i]);
        if (orwl_taskdep_iports[i][0]) {
            printf("\t(%s", orwl_taskdep_iports[i][0]);
            for (size_t j = 1; j < 4; ++j) {
                if (orwl_taskdep_iports[i][j])
                   printf(", %s", orwl_taskdep_iports[i][j]);
            }
            printf(")");
        } else {
            printf("	()");
        }
        if (orwl_taskdep_oports[i][0]) {
            printf("\t(%s", orwl_taskdep_oports[i][0]);
            for (size_t j = 1; j < 4; ++j) {
                if (orwl_taskdep_oports[i][j])
                   printf(", %s", orwl_taskdep_oports[i][j]);
            }
            printf(")");
        } else {
            printf("	()");
        }
        printf("\n");
    }

}

P99_GETOPT_CALLBACK(orwl_taskdep_usage);

// *****************************************************
// Instantiate the ORWL structure.
ORWL_LOCATIONS_PER_TASK(
	orwl_task_write_if0,
	orwl_task_read_if0,
	orwl_task_write_if1,
	orwl_task_read_if1,
	orwl_task_write_if2,
	orwl_task_read_if2,
	orwl_task_write_if3,
	orwl_task_read_if3
);

ORWL_LOCATIONS_PER_TASK_INSTANTIATION();

ORWL_DECLARE_TASK(orwl_taskdep_type);

// *****************************************************
// Set up the ORWL structure that reflects dependencies.

// This graph is iterated.
// Using orwl_handle2.
// node_1_1	→ node_2_1	(out_1_1_r)
// 0	→ 1
// priorities: 0 1
// node_2_1	→ node_1_1	(out_2_1_l)
// 1	→ 0
// using reverse edge "out_1_1_r (out_2_1_l)"
// priorities: 1 0
// node_2_1	→ node_2_2	(out_2_1_b)
// 1	→ 2
// priorities: 0 1
// node_2_1	→ node_3_1	(out_2_1_r)
// 1	→ 3
// priorities: 0 1
// node_2_2	→ node_2_1	(out_2_2_t)
// 2	→ 1
// using reverse edge "out_2_1_b (out_2_2_t)"
// priorities: 1 0
// node_2_2	→ node_3_2	(out_2_2_r)
// 2	→ 4
// priorities: 0 1
// node_3_1	→ node_2_1	(out_3_1_l)
// 3	→ 1
// using reverse edge "out_2_1_r (out_3_1_l)"
// priorities: 1 0
// node_3_1	→ node_3_2	(out_3_1_b)
// 3	→ 4
// priorities: 0 1
// node_3_1	→ node_4_1	(out_3_1_r)
// 3	→ 6
// priorities: 0 1
// node_3_2	→ node_2_2	(out_3_2_l)
// 4	→ 2
// using reverse edge "out_2_2_r (out_3_2_l)"
// priorities: 1 0
// node_3_2	→ node_3_1	(out_3_2_t)
// 4	→ 3
// using reverse edge "out_3_1_b (out_3_2_t)"
// priorities: 1 0
// node_3_2	→ node_3_3	(out_3_2_b)
// 4	→ 5
// priorities: 0 1
// node_3_2	→ node_4_2	(out_3_2_r)
// 4	→ 7
// priorities: 0 1
// node_3_3	→ node_3_2	(out_3_3_t)
// 5	→ 4
// using reverse edge "out_3_2_b (out_3_3_t)"
// priorities: 1 0
// node_3_3	→ node_4_3	(out_3_3_r)
// 5	→ 8
// priorities: 0 1
// node_4_1	→ node_3_1	(out_4_1_l)
// 6	→ 3
// using reverse edge "out_3_1_r (out_4_1_l)"
// priorities: 1 0
// node_4_1	→ node_4_2	(out_4_1_b)
// 6	→ 7
// priorities: 0 1
// node_4_1	→ node_5_1	(out_4_1_r)
// 6	→ 10
// priorities: 0 1
// node_4_2	→ node_3_2	(out_4_2_l)
// 7	→ 4
// using reverse edge "out_3_2_r (out_4_2_l)"
// priorities: 1 0
// node_4_2	→ node_4_1	(out_4_2_t)
// 7	→ 6
// using reverse edge "out_4_1_b (out_4_2_t)"
// priorities: 1 0
// node_4_2	→ node_4_3	(out_4_2_b)
// 7	→ 8
// priorities: 0 1
// node_4_2	→ node_5_2	(out_4_2_r)
// 7	→ 11
// priorities: 0 1
// node_4_3	→ node_3_3	(out_4_3_l)
// 8	→ 5
// using reverse edge "out_3_3_r (out_4_3_l)"
// priorities: 1 0
// node_4_3	→ node_4_2	(out_4_3_t)
// 8	→ 7
// using reverse edge "out_4_2_b (out_4_3_t)"
// priorities: 1 0
// node_4_3	→ node_4_4	(out_4_3_b)
// 8	→ 9
// priorities: 0 1
// node_4_3	→ node_5_3	(out_4_3_r)
// 8	→ 12
// priorities: 0 1
// node_4_4	→ node_4_3	(out_4_4_t)
// 9	→ 8
// using reverse edge "out_4_3_b (out_4_4_t)"
// priorities: 1 0
// node_4_4	→ node_5_4	(out_4_4_r)
// 9	→ 13
// priorities: 0 1
// node_5_1	→ node_4_1	(out_5_1_l)
// 10	→ 6
// using reverse edge "out_4_1_r (out_5_1_l)"
// priorities: 1 0
// node_5_1	→ node_5_2	(out_5_1_b)
// 10	→ 11
// priorities: 0 1
// node_5_1	→ node_6_1	(out_5_1_r)
// 10	→ 15
// priorities: 0 1
// node_5_2	→ node_4_2	(out_5_2_l)
// 11	→ 7
// using reverse edge "out_4_2_r (out_5_2_l)"
// priorities: 1 0
// node_5_2	→ node_5_1	(out_5_2_t)
// 11	→ 10
// using reverse edge "out_5_1_b (out_5_2_t)"
// priorities: 1 0
// node_5_2	→ node_5_3	(out_5_2_b)
// 11	→ 12
// priorities: 0 1
// node_5_2	→ node_6_2	(out_5_2_r)
// 11	→ 16
// priorities: 0 1
// node_5_3	→ node_4_3	(out_5_3_l)
// 12	→ 8
// using reverse edge "out_4_3_r (out_5_3_l)"
// priorities: 1 0
// node_5_3	→ node_5_2	(out_5_3_t)
// 12	→ 11
// using reverse edge "out_5_2_b (out_5_3_t)"
// priorities: 1 0
// node_5_3	→ node_5_4	(out_5_3_b)
// 12	→ 13
// priorities: 0 1
// node_5_3	→ node_6_3	(out_5_3_r)
// 12	→ 17
// priorities: 0 1
// node_5_4	→ node_4_4	(out_5_4_l)
// 13	→ 9
// using reverse edge "out_4_4_r (out_5_4_l)"
// priorities: 1 0
// node_5_4	→ node_5_3	(out_5_4_t)
// 13	→ 12
// using reverse edge "out_5_3_b (out_5_4_t)"
// priorities: 1 0
// node_5_4	→ node_5_5	(out_5_4_b)
// 13	→ 14
// priorities: 0 1
// node_5_4	→ node_6_4	(out_5_4_r)
// 13	→ 18
// priorities: 0 1
// node_5_5	→ node_5_4	(out_5_5_t)
// 14	→ 13
// using reverse edge "out_5_4_b (out_5_5_t)"
// priorities: 1 0
// node_5_5	→ node_6_5	(out_5_5_r)
// 14	→ 19
// priorities: 0 1
// node_6_1	→ node_5_1	(out_6_1_l)
// 15	→ 10
// using reverse edge "out_5_1_r (out_6_1_l)"
// priorities: 1 0
// node_6_1	→ node_6_2	(out_6_1_b)
// 15	→ 16
// priorities: 0 1
// node_6_1	→ node_7_1	(out_6_1_r)
// 15	→ 21
// priorities: 0 1
// node_6_2	→ node_5_2	(out_6_2_l)
// 16	→ 11
// using reverse edge "out_5_2_r (out_6_2_l)"
// priorities: 1 0
// node_6_2	→ node_6_1	(out_6_2_t)
// 16	→ 15
// using reverse edge "out_6_1_b (out_6_2_t)"
// priorities: 1 0
// node_6_2	→ node_6_3	(out_6_2_b)
// 16	→ 17
// priorities: 0 1
// node_6_2	→ node_7_2	(out_6_2_r)
// 16	→ 22
// priorities: 0 1
// node_6_3	→ node_5_3	(out_6_3_l)
// 17	→ 12
// using reverse edge "out_5_3_r (out_6_3_l)"
// priorities: 1 0
// node_6_3	→ node_6_2	(out_6_3_t)
// 17	→ 16
// using reverse edge "out_6_2_b (out_6_3_t)"
// priorities: 1 0
// node_6_3	→ node_6_4	(out_6_3_b)
// 17	→ 18
// priorities: 0 1
// node_6_3	→ node_7_3	(out_6_3_r)
// 17	→ 23
// priorities: 0 1
// node_6_4	→ node_5_4	(out_6_4_l)
// 18	→ 13
// using reverse edge "out_5_4_r (out_6_4_l)"
// priorities: 1 0
// node_6_4	→ node_6_3	(out_6_4_t)
// 18	→ 17
// using reverse edge "out_6_3_b (out_6_4_t)"
// priorities: 1 0
// node_6_4	→ node_6_5	(out_6_4_b)
// 18	→ 19
// priorities: 0 1
// node_6_4	→ node_7_4	(out_6_4_r)
// 18	→ 24
// priorities: 0 1
// node_6_5	→ node_5_5	(out_6_5_l)
// 19	→ 14
// using reverse edge "out_5_5_r (out_6_5_l)"
// priorities: 1 0
// node_6_5	→ node_6_4	(out_6_5_t)
// 19	→ 18
// using reverse edge "out_6_4_b (out_6_5_t)"
// priorities: 1 0
// node_6_5	→ node_6_6	(out_6_5_b)
// 19	→ 20
// priorities: 0 1
// node_6_5	→ node_7_5	(out_6_5_r)
// 19	→ 25
// priorities: 0 1
// node_6_6	→ node_6_5	(out_6_6_t)
// 20	→ 19
// using reverse edge "out_6_5_b (out_6_6_t)"
// priorities: 1 0
// node_6_6	→ node_7_6	(out_6_6_r)
// 20	→ 26
// priorities: 0 1
// node_7_1	→ node_6_1	(out_7_1_l)
// 21	→ 15
// using reverse edge "out_6_1_r (out_7_1_l)"
// priorities: 1 0
// node_7_1	→ node_7_2	(out_7_1_b)
// 21	→ 22
// priorities: 0 1
// node_7_2	→ node_6_2	(out_7_2_l)
// 22	→ 16
// using reverse edge "out_6_2_r (out_7_2_l)"
// priorities: 1 0
// node_7_2	→ node_7_1	(out_7_2_t)
// 22	→ 21
// using reverse edge "out_7_1_b (out_7_2_t)"
// priorities: 1 0
// node_7_2	→ node_7_3	(out_7_2_b)
// 22	→ 23
// priorities: 0 1
// node_7_3	→ node_6_3	(out_7_3_l)
// 23	→ 17
// using reverse edge "out_6_3_r (out_7_3_l)"
// priorities: 1 0
// node_7_3	→ node_7_2	(out_7_3_t)
// 23	→ 22
// using reverse edge "out_7_2_b (out_7_3_t)"
// priorities: 1 0
// node_7_3	→ node_7_4	(out_7_3_b)
// 23	→ 24
// priorities: 0 1
// node_7_4	→ node_6_4	(out_7_4_l)
// 24	→ 18
// using reverse edge "out_6_4_r (out_7_4_l)"
// priorities: 1 0
// node_7_4	→ node_7_3	(out_7_4_t)
// 24	→ 23
// using reverse edge "out_7_3_b (out_7_4_t)"
// priorities: 1 0
// node_7_4	→ node_7_5	(out_7_4_b)
// 24	→ 25
// priorities: 0 1
// node_7_5	→ node_6_5	(out_7_5_l)
// 25	→ 19
// using reverse edge "out_6_5_r (out_7_5_l)"
// priorities: 1 0
// node_7_5	→ node_7_4	(out_7_5_t)
// 25	→ 24
// using reverse edge "out_7_4_b (out_7_5_t)"
// priorities: 1 0
// node_7_5	→ node_7_6	(out_7_5_b)
// 25	→ 26
// priorities: 0 1
// node_7_6	→ node_6_6	(out_7_6_l)
// 26	→ 20
// using reverse edge "out_6_6_r (out_7_6_l)"
// priorities: 1 0
// node_7_6	→ node_7_5	(out_7_6_t)
// 26	→ 25
// using reverse edge "out_7_5_b (out_7_6_t)"
// priorities: 1 0
// node_7_6	→ node_7_7	(out_7_6_b)
// 26	→ 27
// priorities: 0 1
// node_7_7	→ node_7_6	(out_7_7_t)
// 27	→ 26
// using reverse edge "out_7_6_b (out_7_7_t)"
// priorities: 1 0

// ****************************************************************
// Per task specific initialization functions. They should ease some
// const propagation concerning the task ID.
static void orwl_taskdep_init_0(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "0, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(0, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(0, orwl_task_write_if0), ORWL_LOCATION(0, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(0, orwl_task_write_if1), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(0, orwl_task_write_if1), ORWL_LOCATION(0, orwl_task_read_if1), 10);
	orwl_write_insert(&hdl_out[2], ORWL_LOCATION(0, orwl_task_write_if2), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(0, orwl_task_write_if2), ORWL_LOCATION(0, orwl_task_read_if2), 10);
	orwl_write_insert(&hdl_out[3], ORWL_LOCATION(0, orwl_task_write_if3), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(0, orwl_task_write_if3), ORWL_LOCATION(0, orwl_task_read_if3), 10);
	// const port at 0
	// const port at 1
	orwl_read_insert(&hdl_in[2], ORWL_LOCATION(1, orwl_task_read_if1), 0, seed, server);
	// const port at 3

	report(orwl_taskdep_verbose, "0, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "0, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_1(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "1, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(1, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(1, orwl_task_write_if0), ORWL_LOCATION(1, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(1, orwl_task_write_if1), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(1, orwl_task_write_if1), ORWL_LOCATION(1, orwl_task_read_if1), 10);
	orwl_write_insert(&hdl_out[2], ORWL_LOCATION(1, orwl_task_write_if2), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(1, orwl_task_write_if2), ORWL_LOCATION(1, orwl_task_read_if2), 10);
	orwl_write_insert(&hdl_out[3], ORWL_LOCATION(1, orwl_task_write_if3), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(1, orwl_task_write_if3), ORWL_LOCATION(1, orwl_task_read_if3), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(0, orwl_task_read_if3), 1, seed, server);
	orwl_read_insert(&hdl_in[1], ORWL_LOCATION(2, orwl_task_read_if2), 0, seed, server);
	orwl_read_insert(&hdl_in[2], ORWL_LOCATION(3, orwl_task_read_if1), 0, seed, server);
	// const port at 3

	report(orwl_taskdep_verbose, "1, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "1, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_2(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "2, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(2, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(2, orwl_task_write_if0), ORWL_LOCATION(2, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(2, orwl_task_write_if1), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(2, orwl_task_write_if1), ORWL_LOCATION(2, orwl_task_read_if1), 10);
	orwl_write_insert(&hdl_out[2], ORWL_LOCATION(2, orwl_task_write_if2), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(2, orwl_task_write_if2), ORWL_LOCATION(2, orwl_task_read_if2), 10);
	orwl_write_insert(&hdl_out[3], ORWL_LOCATION(2, orwl_task_write_if3), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(2, orwl_task_write_if3), ORWL_LOCATION(2, orwl_task_read_if3), 10);
	// const port at 0
	// const port at 1
	orwl_read_insert(&hdl_in[2], ORWL_LOCATION(4, orwl_task_read_if1), 0, seed, server);
	orwl_read_insert(&hdl_in[3], ORWL_LOCATION(1, orwl_task_read_if0), 1, seed, server);

	report(orwl_taskdep_verbose, "2, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "2, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_3(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "3, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(3, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(3, orwl_task_write_if0), ORWL_LOCATION(3, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(3, orwl_task_write_if1), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(3, orwl_task_write_if1), ORWL_LOCATION(3, orwl_task_read_if1), 10);
	orwl_write_insert(&hdl_out[2], ORWL_LOCATION(3, orwl_task_write_if2), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(3, orwl_task_write_if2), ORWL_LOCATION(3, orwl_task_read_if2), 10);
	orwl_write_insert(&hdl_out[3], ORWL_LOCATION(3, orwl_task_write_if3), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(3, orwl_task_write_if3), ORWL_LOCATION(3, orwl_task_read_if3), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(1, orwl_task_read_if3), 1, seed, server);
	orwl_read_insert(&hdl_in[1], ORWL_LOCATION(4, orwl_task_read_if2), 0, seed, server);
	orwl_read_insert(&hdl_in[2], ORWL_LOCATION(6, orwl_task_read_if1), 0, seed, server);
	// const port at 3

	report(orwl_taskdep_verbose, "3, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "3, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_4(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "4, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(4, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(4, orwl_task_write_if0), ORWL_LOCATION(4, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(4, orwl_task_write_if1), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(4, orwl_task_write_if1), ORWL_LOCATION(4, orwl_task_read_if1), 10);
	orwl_write_insert(&hdl_out[2], ORWL_LOCATION(4, orwl_task_write_if2), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(4, orwl_task_write_if2), ORWL_LOCATION(4, orwl_task_read_if2), 10);
	orwl_write_insert(&hdl_out[3], ORWL_LOCATION(4, orwl_task_write_if3), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(4, orwl_task_write_if3), ORWL_LOCATION(4, orwl_task_read_if3), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(2, orwl_task_read_if3), 1, seed, server);
	orwl_read_insert(&hdl_in[1], ORWL_LOCATION(5, orwl_task_read_if2), 0, seed, server);
	orwl_read_insert(&hdl_in[2], ORWL_LOCATION(7, orwl_task_read_if1), 0, seed, server);
	orwl_read_insert(&hdl_in[3], ORWL_LOCATION(3, orwl_task_read_if0), 1, seed, server);

	report(orwl_taskdep_verbose, "4, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "4, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_5(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "5, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(5, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(5, orwl_task_write_if0), ORWL_LOCATION(5, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(5, orwl_task_write_if1), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(5, orwl_task_write_if1), ORWL_LOCATION(5, orwl_task_read_if1), 10);
	orwl_write_insert(&hdl_out[2], ORWL_LOCATION(5, orwl_task_write_if2), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(5, orwl_task_write_if2), ORWL_LOCATION(5, orwl_task_read_if2), 10);
	orwl_write_insert(&hdl_out[3], ORWL_LOCATION(5, orwl_task_write_if3), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(5, orwl_task_write_if3), ORWL_LOCATION(5, orwl_task_read_if3), 10);
	// const port at 0
	// const port at 1
	orwl_read_insert(&hdl_in[2], ORWL_LOCATION(8, orwl_task_read_if1), 0, seed, server);
	orwl_read_insert(&hdl_in[3], ORWL_LOCATION(4, orwl_task_read_if0), 1, seed, server);

	report(orwl_taskdep_verbose, "5, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "5, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_6(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "6, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(6, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(6, orwl_task_write_if0), ORWL_LOCATION(6, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(6, orwl_task_write_if1), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(6, orwl_task_write_if1), ORWL_LOCATION(6, orwl_task_read_if1), 10);
	orwl_write_insert(&hdl_out[2], ORWL_LOCATION(6, orwl_task_write_if2), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(6, orwl_task_write_if2), ORWL_LOCATION(6, orwl_task_read_if2), 10);
	orwl_write_insert(&hdl_out[3], ORWL_LOCATION(6, orwl_task_write_if3), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(6, orwl_task_write_if3), ORWL_LOCATION(6, orwl_task_read_if3), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(3, orwl_task_read_if3), 1, seed, server);
	orwl_read_insert(&hdl_in[1], ORWL_LOCATION(7, orwl_task_read_if2), 0, seed, server);
	orwl_read_insert(&hdl_in[2], ORWL_LOCATION(10, orwl_task_read_if1), 0, seed, server);
	// const port at 3

	report(orwl_taskdep_verbose, "6, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "6, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_7(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "7, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(7, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(7, orwl_task_write_if0), ORWL_LOCATION(7, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(7, orwl_task_write_if1), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(7, orwl_task_write_if1), ORWL_LOCATION(7, orwl_task_read_if1), 10);
	orwl_write_insert(&hdl_out[2], ORWL_LOCATION(7, orwl_task_write_if2), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(7, orwl_task_write_if2), ORWL_LOCATION(7, orwl_task_read_if2), 10);
	orwl_write_insert(&hdl_out[3], ORWL_LOCATION(7, orwl_task_write_if3), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(7, orwl_task_write_if3), ORWL_LOCATION(7, orwl_task_read_if3), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(4, orwl_task_read_if3), 1, seed, server);
	orwl_read_insert(&hdl_in[1], ORWL_LOCATION(8, orwl_task_read_if2), 0, seed, server);
	orwl_read_insert(&hdl_in[2], ORWL_LOCATION(11, orwl_task_read_if1), 0, seed, server);
	orwl_read_insert(&hdl_in[3], ORWL_LOCATION(6, orwl_task_read_if0), 1, seed, server);

	report(orwl_taskdep_verbose, "7, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "7, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_8(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "8, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(8, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(8, orwl_task_write_if0), ORWL_LOCATION(8, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(8, orwl_task_write_if1), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(8, orwl_task_write_if1), ORWL_LOCATION(8, orwl_task_read_if1), 10);
	orwl_write_insert(&hdl_out[2], ORWL_LOCATION(8, orwl_task_write_if2), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(8, orwl_task_write_if2), ORWL_LOCATION(8, orwl_task_read_if2), 10);
	orwl_write_insert(&hdl_out[3], ORWL_LOCATION(8, orwl_task_write_if3), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(8, orwl_task_write_if3), ORWL_LOCATION(8, orwl_task_read_if3), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(5, orwl_task_read_if3), 1, seed, server);
	orwl_read_insert(&hdl_in[1], ORWL_LOCATION(9, orwl_task_read_if2), 0, seed, server);
	orwl_read_insert(&hdl_in[2], ORWL_LOCATION(12, orwl_task_read_if1), 0, seed, server);
	orwl_read_insert(&hdl_in[3], ORWL_LOCATION(7, orwl_task_read_if0), 1, seed, server);

	report(orwl_taskdep_verbose, "8, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "8, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_9(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "9, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(9, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(9, orwl_task_write_if0), ORWL_LOCATION(9, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(9, orwl_task_write_if1), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(9, orwl_task_write_if1), ORWL_LOCATION(9, orwl_task_read_if1), 10);
	orwl_write_insert(&hdl_out[2], ORWL_LOCATION(9, orwl_task_write_if2), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(9, orwl_task_write_if2), ORWL_LOCATION(9, orwl_task_read_if2), 10);
	orwl_write_insert(&hdl_out[3], ORWL_LOCATION(9, orwl_task_write_if3), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(9, orwl_task_write_if3), ORWL_LOCATION(9, orwl_task_read_if3), 10);
	// const port at 0
	// const port at 1
	orwl_read_insert(&hdl_in[2], ORWL_LOCATION(13, orwl_task_read_if1), 0, seed, server);
	orwl_read_insert(&hdl_in[3], ORWL_LOCATION(8, orwl_task_read_if0), 1, seed, server);

	report(orwl_taskdep_verbose, "9, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "9, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_10(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "10, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(10, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(10, orwl_task_write_if0), ORWL_LOCATION(10, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(10, orwl_task_write_if1), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(10, orwl_task_write_if1), ORWL_LOCATION(10, orwl_task_read_if1), 10);
	orwl_write_insert(&hdl_out[2], ORWL_LOCATION(10, orwl_task_write_if2), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(10, orwl_task_write_if2), ORWL_LOCATION(10, orwl_task_read_if2), 10);
	orwl_write_insert(&hdl_out[3], ORWL_LOCATION(10, orwl_task_write_if3), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(10, orwl_task_write_if3), ORWL_LOCATION(10, orwl_task_read_if3), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(6, orwl_task_read_if3), 1, seed, server);
	orwl_read_insert(&hdl_in[1], ORWL_LOCATION(11, orwl_task_read_if2), 0, seed, server);
	orwl_read_insert(&hdl_in[2], ORWL_LOCATION(15, orwl_task_read_if1), 0, seed, server);
	// const port at 3

	report(orwl_taskdep_verbose, "10, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "10, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_11(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "11, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(11, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(11, orwl_task_write_if0), ORWL_LOCATION(11, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(11, orwl_task_write_if1), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(11, orwl_task_write_if1), ORWL_LOCATION(11, orwl_task_read_if1), 10);
	orwl_write_insert(&hdl_out[2], ORWL_LOCATION(11, orwl_task_write_if2), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(11, orwl_task_write_if2), ORWL_LOCATION(11, orwl_task_read_if2), 10);
	orwl_write_insert(&hdl_out[3], ORWL_LOCATION(11, orwl_task_write_if3), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(11, orwl_task_write_if3), ORWL_LOCATION(11, orwl_task_read_if3), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(7, orwl_task_read_if3), 1, seed, server);
	orwl_read_insert(&hdl_in[1], ORWL_LOCATION(12, orwl_task_read_if2), 0, seed, server);
	orwl_read_insert(&hdl_in[2], ORWL_LOCATION(16, orwl_task_read_if1), 0, seed, server);
	orwl_read_insert(&hdl_in[3], ORWL_LOCATION(10, orwl_task_read_if0), 1, seed, server);

	report(orwl_taskdep_verbose, "11, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "11, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_12(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "12, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(12, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(12, orwl_task_write_if0), ORWL_LOCATION(12, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(12, orwl_task_write_if1), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(12, orwl_task_write_if1), ORWL_LOCATION(12, orwl_task_read_if1), 10);
	orwl_write_insert(&hdl_out[2], ORWL_LOCATION(12, orwl_task_write_if2), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(12, orwl_task_write_if2), ORWL_LOCATION(12, orwl_task_read_if2), 10);
	orwl_write_insert(&hdl_out[3], ORWL_LOCATION(12, orwl_task_write_if3), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(12, orwl_task_write_if3), ORWL_LOCATION(12, orwl_task_read_if3), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(8, orwl_task_read_if3), 1, seed, server);
	orwl_read_insert(&hdl_in[1], ORWL_LOCATION(13, orwl_task_read_if2), 0, seed, server);
	orwl_read_insert(&hdl_in[2], ORWL_LOCATION(17, orwl_task_read_if1), 0, seed, server);
	orwl_read_insert(&hdl_in[3], ORWL_LOCATION(11, orwl_task_read_if0), 1, seed, server);

	report(orwl_taskdep_verbose, "12, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "12, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_13(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "13, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(13, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(13, orwl_task_write_if0), ORWL_LOCATION(13, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(13, orwl_task_write_if1), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(13, orwl_task_write_if1), ORWL_LOCATION(13, orwl_task_read_if1), 10);
	orwl_write_insert(&hdl_out[2], ORWL_LOCATION(13, orwl_task_write_if2), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(13, orwl_task_write_if2), ORWL_LOCATION(13, orwl_task_read_if2), 10);
	orwl_write_insert(&hdl_out[3], ORWL_LOCATION(13, orwl_task_write_if3), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(13, orwl_task_write_if3), ORWL_LOCATION(13, orwl_task_read_if3), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(9, orwl_task_read_if3), 1, seed, server);
	orwl_read_insert(&hdl_in[1], ORWL_LOCATION(14, orwl_task_read_if2), 0, seed, server);
	orwl_read_insert(&hdl_in[2], ORWL_LOCATION(18, orwl_task_read_if1), 0, seed, server);
	orwl_read_insert(&hdl_in[3], ORWL_LOCATION(12, orwl_task_read_if0), 1, seed, server);

	report(orwl_taskdep_verbose, "13, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "13, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_14(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "14, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(14, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(14, orwl_task_write_if0), ORWL_LOCATION(14, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(14, orwl_task_write_if1), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(14, orwl_task_write_if1), ORWL_LOCATION(14, orwl_task_read_if1), 10);
	orwl_write_insert(&hdl_out[2], ORWL_LOCATION(14, orwl_task_write_if2), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(14, orwl_task_write_if2), ORWL_LOCATION(14, orwl_task_read_if2), 10);
	orwl_write_insert(&hdl_out[3], ORWL_LOCATION(14, orwl_task_write_if3), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(14, orwl_task_write_if3), ORWL_LOCATION(14, orwl_task_read_if3), 10);
	// const port at 0
	// const port at 1
	orwl_read_insert(&hdl_in[2], ORWL_LOCATION(19, orwl_task_read_if1), 0, seed, server);
	orwl_read_insert(&hdl_in[3], ORWL_LOCATION(13, orwl_task_read_if0), 1, seed, server);

	report(orwl_taskdep_verbose, "14, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "14, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_15(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "15, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(15, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(15, orwl_task_write_if0), ORWL_LOCATION(15, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(15, orwl_task_write_if1), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(15, orwl_task_write_if1), ORWL_LOCATION(15, orwl_task_read_if1), 10);
	orwl_write_insert(&hdl_out[2], ORWL_LOCATION(15, orwl_task_write_if2), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(15, orwl_task_write_if2), ORWL_LOCATION(15, orwl_task_read_if2), 10);
	orwl_write_insert(&hdl_out[3], ORWL_LOCATION(15, orwl_task_write_if3), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(15, orwl_task_write_if3), ORWL_LOCATION(15, orwl_task_read_if3), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(10, orwl_task_read_if3), 1, seed, server);
	orwl_read_insert(&hdl_in[1], ORWL_LOCATION(16, orwl_task_read_if2), 0, seed, server);
	orwl_read_insert(&hdl_in[2], ORWL_LOCATION(21, orwl_task_read_if1), 0, seed, server);
	// const port at 3

	report(orwl_taskdep_verbose, "15, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "15, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_16(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "16, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(16, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(16, orwl_task_write_if0), ORWL_LOCATION(16, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(16, orwl_task_write_if1), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(16, orwl_task_write_if1), ORWL_LOCATION(16, orwl_task_read_if1), 10);
	orwl_write_insert(&hdl_out[2], ORWL_LOCATION(16, orwl_task_write_if2), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(16, orwl_task_write_if2), ORWL_LOCATION(16, orwl_task_read_if2), 10);
	orwl_write_insert(&hdl_out[3], ORWL_LOCATION(16, orwl_task_write_if3), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(16, orwl_task_write_if3), ORWL_LOCATION(16, orwl_task_read_if3), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(11, orwl_task_read_if3), 1, seed, server);
	orwl_read_insert(&hdl_in[1], ORWL_LOCATION(17, orwl_task_read_if2), 0, seed, server);
	orwl_read_insert(&hdl_in[2], ORWL_LOCATION(22, orwl_task_read_if1), 0, seed, server);
	orwl_read_insert(&hdl_in[3], ORWL_LOCATION(15, orwl_task_read_if0), 1, seed, server);

	report(orwl_taskdep_verbose, "16, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "16, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_17(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "17, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(17, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(17, orwl_task_write_if0), ORWL_LOCATION(17, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(17, orwl_task_write_if1), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(17, orwl_task_write_if1), ORWL_LOCATION(17, orwl_task_read_if1), 10);
	orwl_write_insert(&hdl_out[2], ORWL_LOCATION(17, orwl_task_write_if2), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(17, orwl_task_write_if2), ORWL_LOCATION(17, orwl_task_read_if2), 10);
	orwl_write_insert(&hdl_out[3], ORWL_LOCATION(17, orwl_task_write_if3), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(17, orwl_task_write_if3), ORWL_LOCATION(17, orwl_task_read_if3), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(12, orwl_task_read_if3), 1, seed, server);
	orwl_read_insert(&hdl_in[1], ORWL_LOCATION(18, orwl_task_read_if2), 0, seed, server);
	orwl_read_insert(&hdl_in[2], ORWL_LOCATION(23, orwl_task_read_if1), 0, seed, server);
	orwl_read_insert(&hdl_in[3], ORWL_LOCATION(16, orwl_task_read_if0), 1, seed, server);

	report(orwl_taskdep_verbose, "17, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "17, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_18(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "18, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(18, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(18, orwl_task_write_if0), ORWL_LOCATION(18, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(18, orwl_task_write_if1), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(18, orwl_task_write_if1), ORWL_LOCATION(18, orwl_task_read_if1), 10);
	orwl_write_insert(&hdl_out[2], ORWL_LOCATION(18, orwl_task_write_if2), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(18, orwl_task_write_if2), ORWL_LOCATION(18, orwl_task_read_if2), 10);
	orwl_write_insert(&hdl_out[3], ORWL_LOCATION(18, orwl_task_write_if3), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(18, orwl_task_write_if3), ORWL_LOCATION(18, orwl_task_read_if3), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(13, orwl_task_read_if3), 1, seed, server);
	orwl_read_insert(&hdl_in[1], ORWL_LOCATION(19, orwl_task_read_if2), 0, seed, server);
	orwl_read_insert(&hdl_in[2], ORWL_LOCATION(24, orwl_task_read_if1), 0, seed, server);
	orwl_read_insert(&hdl_in[3], ORWL_LOCATION(17, orwl_task_read_if0), 1, seed, server);

	report(orwl_taskdep_verbose, "18, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "18, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_19(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "19, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(19, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(19, orwl_task_write_if0), ORWL_LOCATION(19, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(19, orwl_task_write_if1), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(19, orwl_task_write_if1), ORWL_LOCATION(19, orwl_task_read_if1), 10);
	orwl_write_insert(&hdl_out[2], ORWL_LOCATION(19, orwl_task_write_if2), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(19, orwl_task_write_if2), ORWL_LOCATION(19, orwl_task_read_if2), 10);
	orwl_write_insert(&hdl_out[3], ORWL_LOCATION(19, orwl_task_write_if3), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(19, orwl_task_write_if3), ORWL_LOCATION(19, orwl_task_read_if3), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(14, orwl_task_read_if3), 1, seed, server);
	orwl_read_insert(&hdl_in[1], ORWL_LOCATION(20, orwl_task_read_if2), 0, seed, server);
	orwl_read_insert(&hdl_in[2], ORWL_LOCATION(25, orwl_task_read_if1), 0, seed, server);
	orwl_read_insert(&hdl_in[3], ORWL_LOCATION(18, orwl_task_read_if0), 1, seed, server);

	report(orwl_taskdep_verbose, "19, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "19, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_20(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "20, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(20, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(20, orwl_task_write_if0), ORWL_LOCATION(20, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(20, orwl_task_write_if1), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(20, orwl_task_write_if1), ORWL_LOCATION(20, orwl_task_read_if1), 10);
	orwl_write_insert(&hdl_out[2], ORWL_LOCATION(20, orwl_task_write_if2), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(20, orwl_task_write_if2), ORWL_LOCATION(20, orwl_task_read_if2), 10);
	orwl_write_insert(&hdl_out[3], ORWL_LOCATION(20, orwl_task_write_if3), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(20, orwl_task_write_if3), ORWL_LOCATION(20, orwl_task_read_if3), 10);
	// const port at 0
	// const port at 1
	orwl_read_insert(&hdl_in[2], ORWL_LOCATION(26, orwl_task_read_if1), 0, seed, server);
	orwl_read_insert(&hdl_in[3], ORWL_LOCATION(19, orwl_task_read_if0), 1, seed, server);

	report(orwl_taskdep_verbose, "20, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "20, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_21(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "21, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(21, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(21, orwl_task_write_if0), ORWL_LOCATION(21, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(21, orwl_task_write_if1), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(21, orwl_task_write_if1), ORWL_LOCATION(21, orwl_task_read_if1), 10);
	orwl_write_insert(&hdl_out[2], ORWL_LOCATION(21, orwl_task_write_if2), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(21, orwl_task_write_if2), ORWL_LOCATION(21, orwl_task_read_if2), 10);
	orwl_write_insert(&hdl_out[3], ORWL_LOCATION(21, orwl_task_write_if3), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(21, orwl_task_write_if3), ORWL_LOCATION(21, orwl_task_read_if3), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(15, orwl_task_read_if3), 1, seed, server);
	orwl_read_insert(&hdl_in[1], ORWL_LOCATION(22, orwl_task_read_if2), 0, seed, server);
	// const port at 2
	// const port at 3

	report(orwl_taskdep_verbose, "21, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "21, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_22(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "22, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(22, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(22, orwl_task_write_if0), ORWL_LOCATION(22, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(22, orwl_task_write_if1), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(22, orwl_task_write_if1), ORWL_LOCATION(22, orwl_task_read_if1), 10);
	orwl_write_insert(&hdl_out[2], ORWL_LOCATION(22, orwl_task_write_if2), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(22, orwl_task_write_if2), ORWL_LOCATION(22, orwl_task_read_if2), 10);
	orwl_write_insert(&hdl_out[3], ORWL_LOCATION(22, orwl_task_write_if3), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(22, orwl_task_write_if3), ORWL_LOCATION(22, orwl_task_read_if3), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(16, orwl_task_read_if3), 1, seed, server);
	orwl_read_insert(&hdl_in[1], ORWL_LOCATION(23, orwl_task_read_if2), 0, seed, server);
	// const port at 2
	orwl_read_insert(&hdl_in[3], ORWL_LOCATION(21, orwl_task_read_if0), 1, seed, server);

	report(orwl_taskdep_verbose, "22, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "22, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_23(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "23, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(23, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(23, orwl_task_write_if0), ORWL_LOCATION(23, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(23, orwl_task_write_if1), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(23, orwl_task_write_if1), ORWL_LOCATION(23, orwl_task_read_if1), 10);
	orwl_write_insert(&hdl_out[2], ORWL_LOCATION(23, orwl_task_write_if2), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(23, orwl_task_write_if2), ORWL_LOCATION(23, orwl_task_read_if2), 10);
	orwl_write_insert(&hdl_out[3], ORWL_LOCATION(23, orwl_task_write_if3), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(23, orwl_task_write_if3), ORWL_LOCATION(23, orwl_task_read_if3), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(17, orwl_task_read_if3), 1, seed, server);
	orwl_read_insert(&hdl_in[1], ORWL_LOCATION(24, orwl_task_read_if2), 0, seed, server);
	// const port at 2
	orwl_read_insert(&hdl_in[3], ORWL_LOCATION(22, orwl_task_read_if0), 1, seed, server);

	report(orwl_taskdep_verbose, "23, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "23, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_24(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "24, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(24, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(24, orwl_task_write_if0), ORWL_LOCATION(24, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(24, orwl_task_write_if1), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(24, orwl_task_write_if1), ORWL_LOCATION(24, orwl_task_read_if1), 10);
	orwl_write_insert(&hdl_out[2], ORWL_LOCATION(24, orwl_task_write_if2), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(24, orwl_task_write_if2), ORWL_LOCATION(24, orwl_task_read_if2), 10);
	orwl_write_insert(&hdl_out[3], ORWL_LOCATION(24, orwl_task_write_if3), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(24, orwl_task_write_if3), ORWL_LOCATION(24, orwl_task_read_if3), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(18, orwl_task_read_if3), 1, seed, server);
	orwl_read_insert(&hdl_in[1], ORWL_LOCATION(25, orwl_task_read_if2), 0, seed, server);
	// const port at 2
	orwl_read_insert(&hdl_in[3], ORWL_LOCATION(23, orwl_task_read_if0), 1, seed, server);

	report(orwl_taskdep_verbose, "24, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "24, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_25(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "25, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(25, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(25, orwl_task_write_if0), ORWL_LOCATION(25, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(25, orwl_task_write_if1), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(25, orwl_task_write_if1), ORWL_LOCATION(25, orwl_task_read_if1), 10);
	orwl_write_insert(&hdl_out[2], ORWL_LOCATION(25, orwl_task_write_if2), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(25, orwl_task_write_if2), ORWL_LOCATION(25, orwl_task_read_if2), 10);
	orwl_write_insert(&hdl_out[3], ORWL_LOCATION(25, orwl_task_write_if3), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(25, orwl_task_write_if3), ORWL_LOCATION(25, orwl_task_read_if3), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(19, orwl_task_read_if3), 1, seed, server);
	orwl_read_insert(&hdl_in[1], ORWL_LOCATION(26, orwl_task_read_if2), 0, seed, server);
	// const port at 2
	orwl_read_insert(&hdl_in[3], ORWL_LOCATION(24, orwl_task_read_if0), 1, seed, server);

	report(orwl_taskdep_verbose, "25, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "25, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_26(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "26, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(26, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(26, orwl_task_write_if0), ORWL_LOCATION(26, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(26, orwl_task_write_if1), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(26, orwl_task_write_if1), ORWL_LOCATION(26, orwl_task_read_if1), 10);
	orwl_write_insert(&hdl_out[2], ORWL_LOCATION(26, orwl_task_write_if2), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(26, orwl_task_write_if2), ORWL_LOCATION(26, orwl_task_read_if2), 10);
	orwl_write_insert(&hdl_out[3], ORWL_LOCATION(26, orwl_task_write_if3), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(26, orwl_task_write_if3), ORWL_LOCATION(26, orwl_task_read_if3), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(20, orwl_task_read_if3), 1, seed, server);
	orwl_read_insert(&hdl_in[1], ORWL_LOCATION(27, orwl_task_read_if2), 0, seed, server);
	// const port at 2
	orwl_read_insert(&hdl_in[3], ORWL_LOCATION(25, orwl_task_read_if0), 1, seed, server);

	report(orwl_taskdep_verbose, "26, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "26, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_27(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "27, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(27, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(27, orwl_task_write_if0), ORWL_LOCATION(27, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(27, orwl_task_write_if1), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(27, orwl_task_write_if1), ORWL_LOCATION(27, orwl_task_read_if1), 10);
	orwl_write_insert(&hdl_out[2], ORWL_LOCATION(27, orwl_task_write_if2), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(27, orwl_task_write_if2), ORWL_LOCATION(27, orwl_task_read_if2), 10);
	orwl_write_insert(&hdl_out[3], ORWL_LOCATION(27, orwl_task_write_if3), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(27, orwl_task_write_if3), ORWL_LOCATION(27, orwl_task_read_if3), 10);
	// const port at 0
	// const port at 1
	// const port at 2
	orwl_read_insert(&hdl_in[3], ORWL_LOCATION(26, orwl_task_read_if0), 1, seed, server);

	report(orwl_taskdep_verbose, "27, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "27, finished orwl_taskdep_init");

};
static orwl_taskdep_vector const p_dummy = { 0 };

// Task function that is executed by task 0 (node_1_1)
static void orwl_taskdep_run_0(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #0 (node_1_1)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(0));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 4,
        odeg  = 4,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(0, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(0, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_0(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #0 (node_1_1) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[0][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[0][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(0), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[0][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #0 (node_1_1) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(0));

    report(orwl_taskdep_verbose, "task #0 (node_1_1) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #0 (node_1_1) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #0 (node_1_1) ends");
}


// Task function that is executed by task 1 (node_2_1)
static void orwl_taskdep_run_1(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #1 (node_2_1)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(1));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 4,
        odeg  = 4,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(1, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(1, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_1(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #1 (node_2_1) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[1][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[1][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(1), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[1][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #1 (node_2_1) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(1));

    report(orwl_taskdep_verbose, "task #1 (node_2_1) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #1 (node_2_1) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #1 (node_2_1) ends");
}


// Task function that is executed by task 2 (node_2_2)
static void orwl_taskdep_run_2(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #2 (node_2_2)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(2));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 4,
        odeg  = 4,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(2, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(2, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_2(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #2 (node_2_2) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[2][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[2][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(2), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[2][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #2 (node_2_2) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(2));

    report(orwl_taskdep_verbose, "task #2 (node_2_2) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #2 (node_2_2) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #2 (node_2_2) ends");
}


// Task function that is executed by task 3 (node_3_1)
static void orwl_taskdep_run_3(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #3 (node_3_1)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(3));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 4,
        odeg  = 4,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(3, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(3, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_3(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #3 (node_3_1) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[3][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[3][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(3), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[3][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #3 (node_3_1) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(3));

    report(orwl_taskdep_verbose, "task #3 (node_3_1) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #3 (node_3_1) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #3 (node_3_1) ends");
}


// Task function that is executed by task 4 (node_3_2)
static void orwl_taskdep_run_4(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #4 (node_3_2)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(4));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 4,
        odeg  = 4,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(4, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(4, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_4(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #4 (node_3_2) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[4][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[4][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(4), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[4][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #4 (node_3_2) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(4));

    report(orwl_taskdep_verbose, "task #4 (node_3_2) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #4 (node_3_2) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #4 (node_3_2) ends");
}


// Task function that is executed by task 5 (node_3_3)
static void orwl_taskdep_run_5(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #5 (node_3_3)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(5));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 4,
        odeg  = 4,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(5, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(5, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_5(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #5 (node_3_3) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[5][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[5][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(5), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[5][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #5 (node_3_3) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(5));

    report(orwl_taskdep_verbose, "task #5 (node_3_3) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #5 (node_3_3) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #5 (node_3_3) ends");
}


// Task function that is executed by task 6 (node_4_1)
static void orwl_taskdep_run_6(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #6 (node_4_1)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(6));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 4,
        odeg  = 4,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(6, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(6, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_6(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #6 (node_4_1) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[6][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[6][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(6), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[6][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #6 (node_4_1) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(6));

    report(orwl_taskdep_verbose, "task #6 (node_4_1) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #6 (node_4_1) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #6 (node_4_1) ends");
}


// Task function that is executed by task 7 (node_4_2)
static void orwl_taskdep_run_7(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #7 (node_4_2)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(7));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 4,
        odeg  = 4,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(7, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(7, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_7(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #7 (node_4_2) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[7][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[7][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(7), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[7][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #7 (node_4_2) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(7));

    report(orwl_taskdep_verbose, "task #7 (node_4_2) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #7 (node_4_2) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #7 (node_4_2) ends");
}


// Task function that is executed by task 8 (node_4_3)
static void orwl_taskdep_run_8(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #8 (node_4_3)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(8));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 4,
        odeg  = 4,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(8, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(8, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_8(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #8 (node_4_3) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[8][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[8][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(8), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[8][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #8 (node_4_3) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(8));

    report(orwl_taskdep_verbose, "task #8 (node_4_3) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #8 (node_4_3) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #8 (node_4_3) ends");
}


// Task function that is executed by task 9 (node_4_4)
static void orwl_taskdep_run_9(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #9 (node_4_4)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(9));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 4,
        odeg  = 4,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(9, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(9, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_9(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #9 (node_4_4) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[9][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[9][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(9), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[9][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #9 (node_4_4) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(9));

    report(orwl_taskdep_verbose, "task #9 (node_4_4) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #9 (node_4_4) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #9 (node_4_4) ends");
}


// Task function that is executed by task 10 (node_5_1)
static void orwl_taskdep_run_10(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #10 (node_5_1)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(10));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 4,
        odeg  = 4,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(10, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(10, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_10(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #10 (node_5_1) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[10][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[10][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(10), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[10][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #10 (node_5_1) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(10));

    report(orwl_taskdep_verbose, "task #10 (node_5_1) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #10 (node_5_1) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #10 (node_5_1) ends");
}


// Task function that is executed by task 11 (node_5_2)
static void orwl_taskdep_run_11(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #11 (node_5_2)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(11));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 4,
        odeg  = 4,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(11, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(11, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_11(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #11 (node_5_2) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[11][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[11][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(11), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[11][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #11 (node_5_2) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(11));

    report(orwl_taskdep_verbose, "task #11 (node_5_2) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #11 (node_5_2) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #11 (node_5_2) ends");
}


// Task function that is executed by task 12 (node_5_3)
static void orwl_taskdep_run_12(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #12 (node_5_3)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(12));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 4,
        odeg  = 4,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(12, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(12, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_12(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #12 (node_5_3) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[12][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[12][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(12), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[12][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #12 (node_5_3) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(12));

    report(orwl_taskdep_verbose, "task #12 (node_5_3) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #12 (node_5_3) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #12 (node_5_3) ends");
}


// Task function that is executed by task 13 (node_5_4)
static void orwl_taskdep_run_13(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #13 (node_5_4)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(13));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 4,
        odeg  = 4,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(13, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(13, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_13(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #13 (node_5_4) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[13][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[13][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(13), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[13][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #13 (node_5_4) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(13));

    report(orwl_taskdep_verbose, "task #13 (node_5_4) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #13 (node_5_4) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #13 (node_5_4) ends");
}


// Task function that is executed by task 14 (node_5_5)
static void orwl_taskdep_run_14(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #14 (node_5_5)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(14));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 4,
        odeg  = 4,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(14, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(14, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_14(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #14 (node_5_5) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[14][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[14][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(14), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[14][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #14 (node_5_5) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(14));

    report(orwl_taskdep_verbose, "task #14 (node_5_5) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #14 (node_5_5) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #14 (node_5_5) ends");
}


// Task function that is executed by task 15 (node_6_1)
static void orwl_taskdep_run_15(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #15 (node_6_1)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(15));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 4,
        odeg  = 4,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(15, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(15, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_15(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #15 (node_6_1) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[15][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[15][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(15), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[15][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #15 (node_6_1) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(15));

    report(orwl_taskdep_verbose, "task #15 (node_6_1) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #15 (node_6_1) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #15 (node_6_1) ends");
}


// Task function that is executed by task 16 (node_6_2)
static void orwl_taskdep_run_16(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #16 (node_6_2)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(16));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 4,
        odeg  = 4,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(16, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(16, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_16(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #16 (node_6_2) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[16][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[16][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(16), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[16][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #16 (node_6_2) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(16));

    report(orwl_taskdep_verbose, "task #16 (node_6_2) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #16 (node_6_2) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #16 (node_6_2) ends");
}


// Task function that is executed by task 17 (node_6_3)
static void orwl_taskdep_run_17(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #17 (node_6_3)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(17));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 4,
        odeg  = 4,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(17, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(17, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_17(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #17 (node_6_3) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[17][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[17][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(17), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[17][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #17 (node_6_3) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(17));

    report(orwl_taskdep_verbose, "task #17 (node_6_3) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #17 (node_6_3) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #17 (node_6_3) ends");
}


// Task function that is executed by task 18 (node_6_4)
static void orwl_taskdep_run_18(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #18 (node_6_4)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(18));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 4,
        odeg  = 4,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(18, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(18, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_18(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #18 (node_6_4) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[18][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[18][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(18), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[18][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #18 (node_6_4) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(18));

    report(orwl_taskdep_verbose, "task #18 (node_6_4) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #18 (node_6_4) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #18 (node_6_4) ends");
}


// Task function that is executed by task 19 (node_6_5)
static void orwl_taskdep_run_19(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #19 (node_6_5)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(19));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 4,
        odeg  = 4,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(19, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(19, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_19(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #19 (node_6_5) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[19][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[19][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(19), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[19][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #19 (node_6_5) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(19));

    report(orwl_taskdep_verbose, "task #19 (node_6_5) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #19 (node_6_5) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #19 (node_6_5) ends");
}


// Task function that is executed by task 20 (node_6_6)
static void orwl_taskdep_run_20(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #20 (node_6_6)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(20));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 4,
        odeg  = 4,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(20, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(20, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_20(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #20 (node_6_6) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[20][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[20][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(20), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[20][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #20 (node_6_6) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(20));

    report(orwl_taskdep_verbose, "task #20 (node_6_6) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #20 (node_6_6) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #20 (node_6_6) ends");
}


// Task function that is executed by task 21 (node_7_1)
static void orwl_taskdep_run_21(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #21 (node_7_1)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(21));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 4,
        odeg  = 4,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(21, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(21, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_21(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #21 (node_7_1) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[21][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[21][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(21), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[21][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #21 (node_7_1) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(21));

    report(orwl_taskdep_verbose, "task #21 (node_7_1) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #21 (node_7_1) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #21 (node_7_1) ends");
}


// Task function that is executed by task 22 (node_7_2)
static void orwl_taskdep_run_22(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #22 (node_7_2)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(22));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 4,
        odeg  = 4,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(22, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(22, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_22(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #22 (node_7_2) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[22][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[22][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(22), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[22][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #22 (node_7_2) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(22));

    report(orwl_taskdep_verbose, "task #22 (node_7_2) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #22 (node_7_2) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #22 (node_7_2) ends");
}


// Task function that is executed by task 23 (node_7_3)
static void orwl_taskdep_run_23(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #23 (node_7_3)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(23));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 4,
        odeg  = 4,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(23, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(23, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_23(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #23 (node_7_3) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[23][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[23][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(23), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[23][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #23 (node_7_3) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(23));

    report(orwl_taskdep_verbose, "task #23 (node_7_3) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #23 (node_7_3) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #23 (node_7_3) ends");
}


// Task function that is executed by task 24 (node_7_4)
static void orwl_taskdep_run_24(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #24 (node_7_4)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(24));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 4,
        odeg  = 4,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(24, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(24, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_24(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #24 (node_7_4) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[24][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[24][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(24), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[24][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #24 (node_7_4) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(24));

    report(orwl_taskdep_verbose, "task #24 (node_7_4) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #24 (node_7_4) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #24 (node_7_4) ends");
}


// Task function that is executed by task 25 (node_7_5)
static void orwl_taskdep_run_25(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #25 (node_7_5)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(25));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 4,
        odeg  = 4,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(25, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(25, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_25(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #25 (node_7_5) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[25][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[25][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(25), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[25][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #25 (node_7_5) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(25));

    report(orwl_taskdep_verbose, "task #25 (node_7_5) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #25 (node_7_5) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #25 (node_7_5) ends");
}


// Task function that is executed by task 26 (node_7_6)
static void orwl_taskdep_run_26(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #26 (node_7_6)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(26));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 4,
        odeg  = 4,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(26, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(26, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_26(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #26 (node_7_6) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[26][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[26][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(26), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[26][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #26 (node_7_6) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(26));

    report(orwl_taskdep_verbose, "task #26 (node_7_6) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #26 (node_7_6) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #26 (node_7_6) ends");
}


// Task function that is executed by task 27 (node_7_7)
static void orwl_taskdep_run_27(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #27 (node_7_7)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(27));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 4,
        odeg  = 4,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(27, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(27, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_27(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #27 (node_7_7) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[27][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[27][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(27), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[27][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #27 (node_7_7) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(27));

    report(orwl_taskdep_verbose, "task #27 (node_7_7) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #27 (node_7_7) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #27 (node_7_7) ends");
}


static void (*const orwl_taskdep_runs[])(int argc, char* argv[argc+1]) = {
    orwl_taskdep_run_0,
    orwl_taskdep_run_1,
    orwl_taskdep_run_2,
    orwl_taskdep_run_3,
    orwl_taskdep_run_4,
    orwl_taskdep_run_5,
    orwl_taskdep_run_6,
    orwl_taskdep_run_7,
    orwl_taskdep_run_8,
    orwl_taskdep_run_9,
    orwl_taskdep_run_10,
    orwl_taskdep_run_11,
    orwl_taskdep_run_12,
    orwl_taskdep_run_13,
    orwl_taskdep_run_14,
    orwl_taskdep_run_15,
    orwl_taskdep_run_16,
    orwl_taskdep_run_17,
    orwl_taskdep_run_18,
    orwl_taskdep_run_19,
    orwl_taskdep_run_20,
    orwl_taskdep_run_21,
    orwl_taskdep_run_22,
    orwl_taskdep_run_23,
    orwl_taskdep_run_24,
    orwl_taskdep_run_25,
    orwl_taskdep_run_26,
    orwl_taskdep_run_27,
};

_Alignas(32) int64_t const orwl_taskdep_nodes   = 28;
_Alignas(32) int64_t const orwl_taskdep_max_inp = 4;
_Alignas(32) int64_t const orwl_taskdep_max_out = 4;

// Add symbols for Fortran compatibility.

_Alignas(32) int64_t const ORWL_TASKDEP_FORT(orwl_taskdep_vsize)   = orwl_taskdep_vsize;

ORWL_TASKDEP_ALIAS(orwl_taskdep_deg_out);
ORWL_TASKDEP_ALIAS(orwl_taskdep_deg_inp);
ORWL_TASKDEP_ALIAS(orwl_taskdep_nodes);
ORWL_TASKDEP_ALIAS(orwl_taskdep_max_inp);
ORWL_TASKDEP_ALIAS(orwl_taskdep_max_out);

// A wrapper as generic task function that just launches the specific function
ORWL_DEFINE_TASK(orwl_taskdep_type) {
    ORWL_THREAD_USE(orwl_taskdep_type, argc, argv);
    int64_t const tid = orwl_mytid;
    orwl_taskdep_runs[tid](argc, argv);
}

int main(int argc, char* argv[argc+1]) {
  p99_getopt_initialize(&argc, &argv);

  ORWL_TIMER() {
    orwl_init();

    if (orwl_nt != 28) {
        report(1, "We need exactly 28 task, to process this graph, current value is %zu, exiting.", orwl_nt);
        return EXIT_FAILURE;
    }

    //! [launch one thread per task]
    for (size_t i = 0; i < orwl_lt; i++) {
      orwl_taskdep_type* task = P99_NEW(orwl_taskdep_type, argc, argv);
      orwl_taskdep_type_create_task(task, orwl_tids[i]);
    }
    //! [launch one thread per task]
  }
  return EXIT_SUCCESS;
}

// End of program for graph triangle_7
// *********************************************************************************

