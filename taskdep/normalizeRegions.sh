#!/bin/sh

# collect the graph from the first argument or stdin
if [ $# = 0 ] ; then
    ifile="-"
    name="graph"
    ofile="graph.dot"
else
    ifile="$1"
    name="${1%.*}"
    ofile="${name}-clean.dot"
fi

# the second argument may provide names to the output interfaces
if [ $# = 2 ] ; then
    pfile="$2"

    setter=$(sed '
1,1 d
s!\([[:digit:]]\{1,\}\)[[:space:]]*\([[:digit:]]\{1,\}\)!port_\1="\2/" ;!
' "${pfile}")

    eval "${setter}"
fi

cat <<EOF  > "${ofile}"
digraph "${name}" {
   node [
      function = "iterationStep"
      startup = "startup"
      shutdown = "saveAll"
      shape = "record"
   ]
EOF

# scan for all ports
setter=$(sed '
/bassin versant/d
s![\$\`]!DANGEROUS!g
s!\([[:digit:]]\{1,\}\)[[:space:]]*->[[:space:]]*\([[:digit:]]\{1,\}\).*"[[:space:]]*\([[:digit:]]\{1,\}\)[[:space:]]*"[[:space:]]*\]!\
  # collect the nodes                            \
  nodes="${nodes}\n\1\n\2" ;                     \
  headports_\2="\${headports_\2} \${port_\1}\3" ;\
  tailports_\1="\${tailports_\1} \${port_\1}\3" ;\
  edges="${edges}\n\3" ;                         \
  # normalize the edge                           \
  out_\3="\\\"node_\1\\\"->\\\"node_\2\\\"[\\\"headport\\\"=\\\"\${port_\1}\3\\\"]" ;!
' "${ifile}")

eval "${setter}"

nodes=$(echo "${nodes}" | sort -u)

for n in ${nodes} ; do
    eval "tailp=\${tailports_$n}"
    eval "headp=\${headports_$n}"
    echo "node_$n ["                           >> "${ofile}"
    echo "  \"headports\" = \"${headp# }\""    >> "${ofile}"
    echo "  \"tailports\" = \"${tailp# }\""    >> "${ofile}"
    echo "]"                                   >> "${ofile}"
done

for e in ${edges} ; do
    eval "edge=\${out_$e}"
    echo "${edge}" >> "${ofile}"
done


echo "}"      >> "${ofile}"
