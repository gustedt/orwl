// **********************************************
#include "orwl_taskdep.h"
#include "simple_functions.h"

// To simulate call by reference for Fortran we need a lot of
// temporaries.
#define FREF64(X) (int64_t const){ (X) }

P99_GETOPT_SYNOPSIS("A program derived from task graph 'rect_neigh_4_5' with 82 nodes and 124 dependencies.");
char const*const orwl_taskdep_nnames[82] = {
	[0] = "buf_1_1_b",
	[1] = "buf_1_1_r",
	[2] = "buf_1_2_b",
	[3] = "buf_1_2_l",
	[4] = "buf_1_2_r",
	[5] = "buf_1_3_b",
	[6] = "buf_1_3_l",
	[7] = "buf_1_3_r",
	[8] = "buf_1_4_b",
	[9] = "buf_1_4_l",
	[10] = "buf_1_4_r",
	[11] = "buf_1_5_b",
	[12] = "buf_1_5_l",
	[13] = "buf_2_1_b",
	[14] = "buf_2_1_r",
	[15] = "buf_2_1_t",
	[16] = "buf_2_2_b",
	[17] = "buf_2_2_l",
	[18] = "buf_2_2_r",
	[19] = "buf_2_2_t",
	[20] = "buf_2_3_b",
	[21] = "buf_2_3_l",
	[22] = "buf_2_3_r",
	[23] = "buf_2_3_t",
	[24] = "buf_2_4_b",
	[25] = "buf_2_4_l",
	[26] = "buf_2_4_r",
	[27] = "buf_2_4_t",
	[28] = "buf_2_5_b",
	[29] = "buf_2_5_l",
	[30] = "buf_2_5_t",
	[31] = "buf_3_1_b",
	[32] = "buf_3_1_r",
	[33] = "buf_3_1_t",
	[34] = "buf_3_2_b",
	[35] = "buf_3_2_l",
	[36] = "buf_3_2_r",
	[37] = "buf_3_2_t",
	[38] = "buf_3_3_b",
	[39] = "buf_3_3_l",
	[40] = "buf_3_3_r",
	[41] = "buf_3_3_t",
	[42] = "buf_3_4_b",
	[43] = "buf_3_4_l",
	[44] = "buf_3_4_r",
	[45] = "buf_3_4_t",
	[46] = "buf_3_5_b",
	[47] = "buf_3_5_l",
	[48] = "buf_3_5_t",
	[49] = "buf_4_1_r",
	[50] = "buf_4_1_t",
	[51] = "buf_4_2_l",
	[52] = "buf_4_2_r",
	[53] = "buf_4_2_t",
	[54] = "buf_4_3_l",
	[55] = "buf_4_3_r",
	[56] = "buf_4_3_t",
	[57] = "buf_4_4_l",
	[58] = "buf_4_4_r",
	[59] = "buf_4_4_t",
	[60] = "buf_4_5_l",
	[61] = "buf_4_5_t",
	[62] = "node_1_1",
	[63] = "node_1_2",
	[64] = "node_1_3",
	[65] = "node_1_4",
	[66] = "node_1_5",
	[67] = "node_2_1",
	[68] = "node_2_2",
	[69] = "node_2_3",
	[70] = "node_2_4",
	[71] = "node_2_5",
	[72] = "node_3_1",
	[73] = "node_3_2",
	[74] = "node_3_3",
	[75] = "node_3_4",
	[76] = "node_3_5",
	[77] = "node_4_1",
	[78] = "node_4_2",
	[79] = "node_4_3",
	[80] = "node_4_4",
	[81] = "node_4_5",
};
// **********************************************


// Task graph "rect_neigh_4_5"


// **********************************************
// External information needed for the task graph

// The data type through which tasks communicate.

enum { orwl_taskdep_vsize = 3, };
typedef floating orwl_taskdep_data;

// ***************************************
// Types.

// The transfer type between tasks.
typedef orwl_taskdep_data orwl_taskdep_vector[orwl_taskdep_vsize];

// The task functions that are used.
//
typedef void orwl_taskdep_ftype(orwl_taskdep_blob state[static restrict 1], int64_t const tid[static 1], int64_t const phase[static 1],
                int64_t const in_num[static 1], orwl_taskdep_vector in_mat[restrict in_num[0]], _Bool skip_in[restrict in_num[0]],
                int64_t const out_num[static 1], orwl_taskdep_vector out_mat[restrict out_num[0]], _Bool skip_out[restrict out_num[0]]);
typedef void orwl_taskdep_stype(orwl_taskdep_blob state[static restrict 1], int64_t const num[static 1]);

// *****************************************************************************
// Static properties of the task graph

// we have 82 nodes/regions

extern void update(orwl_taskdep_blob state[static restrict 1], int64_t const tid[static 1], int64_t const phase[static 1],
    int64_t const in_num[static 1], orwl_taskdep_vector in_mat[restrict in_num[0]], _Bool skip_in[restrict in_num[0]],
    int64_t const out_num[static 1], orwl_taskdep_vector out_mat[restrict out_num[0]], _Bool skip_out[restrict out_num[0]]);
extern void orwl_taskdep_start(orwl_taskdep_blob state[static restrict 1], int64_t const num[static 1]);
extern void orwl_taskdep_shutdown(orwl_taskdep_blob state[static restrict 1], int64_t const num[static 1]);
extern void regular(orwl_taskdep_blob state[static restrict 1], int64_t const tid[static 1], int64_t const phase[static 1],
    int64_t const in_num[static 1], orwl_taskdep_vector in_mat[restrict in_num[0]], _Bool skip_in[restrict in_num[0]],
    int64_t const out_num[static 1], orwl_taskdep_vector out_mat[restrict out_num[0]], _Bool skip_out[restrict out_num[0]]);
char const*const orwl_taskdep_snames[82] = {
	[0] = "orwl_taskdep_start",
	[1] = "orwl_taskdep_start",
	[2] = "orwl_taskdep_start",
	[3] = "orwl_taskdep_start",
	[4] = "orwl_taskdep_start",
	[5] = "orwl_taskdep_start",
	[6] = "orwl_taskdep_start",
	[7] = "orwl_taskdep_start",
	[8] = "orwl_taskdep_start",
	[9] = "orwl_taskdep_start",
	[10] = "orwl_taskdep_start",
	[11] = "orwl_taskdep_start",
	[12] = "orwl_taskdep_start",
	[13] = "orwl_taskdep_start",
	[14] = "orwl_taskdep_start",
	[15] = "orwl_taskdep_start",
	[16] = "orwl_taskdep_start",
	[17] = "orwl_taskdep_start",
	[18] = "orwl_taskdep_start",
	[19] = "orwl_taskdep_start",
	[20] = "orwl_taskdep_start",
	[21] = "orwl_taskdep_start",
	[22] = "orwl_taskdep_start",
	[23] = "orwl_taskdep_start",
	[24] = "orwl_taskdep_start",
	[25] = "orwl_taskdep_start",
	[26] = "orwl_taskdep_start",
	[27] = "orwl_taskdep_start",
	[28] = "orwl_taskdep_start",
	[29] = "orwl_taskdep_start",
	[30] = "orwl_taskdep_start",
	[31] = "orwl_taskdep_start",
	[32] = "orwl_taskdep_start",
	[33] = "orwl_taskdep_start",
	[34] = "orwl_taskdep_start",
	[35] = "orwl_taskdep_start",
	[36] = "orwl_taskdep_start",
	[37] = "orwl_taskdep_start",
	[38] = "orwl_taskdep_start",
	[39] = "orwl_taskdep_start",
	[40] = "orwl_taskdep_start",
	[41] = "orwl_taskdep_start",
	[42] = "orwl_taskdep_start",
	[43] = "orwl_taskdep_start",
	[44] = "orwl_taskdep_start",
	[45] = "orwl_taskdep_start",
	[46] = "orwl_taskdep_start",
	[47] = "orwl_taskdep_start",
	[48] = "orwl_taskdep_start",
	[49] = "orwl_taskdep_start",
	[50] = "orwl_taskdep_start",
	[51] = "orwl_taskdep_start",
	[52] = "orwl_taskdep_start",
	[53] = "orwl_taskdep_start",
	[54] = "orwl_taskdep_start",
	[55] = "orwl_taskdep_start",
	[56] = "orwl_taskdep_start",
	[57] = "orwl_taskdep_start",
	[58] = "orwl_taskdep_start",
	[59] = "orwl_taskdep_start",
	[60] = "orwl_taskdep_start",
	[61] = "orwl_taskdep_start",
	[62] = "orwl_taskdep_start",
	[63] = "orwl_taskdep_start",
	[64] = "orwl_taskdep_start",
	[65] = "orwl_taskdep_start",
	[66] = "orwl_taskdep_start",
	[67] = "orwl_taskdep_start",
	[68] = "orwl_taskdep_start",
	[69] = "orwl_taskdep_start",
	[70] = "orwl_taskdep_start",
	[71] = "orwl_taskdep_start",
	[72] = "orwl_taskdep_start",
	[73] = "orwl_taskdep_start",
	[74] = "orwl_taskdep_start",
	[75] = "orwl_taskdep_start",
	[76] = "orwl_taskdep_start",
	[77] = "orwl_taskdep_start",
	[78] = "orwl_taskdep_start",
	[79] = "orwl_taskdep_start",
	[80] = "orwl_taskdep_start",
	[81] = "orwl_taskdep_start",
};
char const*const orwl_taskdep_fnames[82] = {
	[0] = "update",
	[1] = "update",
	[2] = "update",
	[3] = "update",
	[4] = "update",
	[5] = "update",
	[6] = "update",
	[7] = "update",
	[8] = "update",
	[9] = "update",
	[10] = "update",
	[11] = "update",
	[12] = "update",
	[13] = "update",
	[14] = "update",
	[15] = "update",
	[16] = "update",
	[17] = "update",
	[18] = "update",
	[19] = "update",
	[20] = "update",
	[21] = "update",
	[22] = "update",
	[23] = "update",
	[24] = "update",
	[25] = "update",
	[26] = "update",
	[27] = "update",
	[28] = "update",
	[29] = "update",
	[30] = "update",
	[31] = "update",
	[32] = "update",
	[33] = "update",
	[34] = "update",
	[35] = "update",
	[36] = "update",
	[37] = "update",
	[38] = "update",
	[39] = "update",
	[40] = "update",
	[41] = "update",
	[42] = "update",
	[43] = "update",
	[44] = "update",
	[45] = "update",
	[46] = "update",
	[47] = "update",
	[48] = "update",
	[49] = "update",
	[50] = "update",
	[51] = "update",
	[52] = "update",
	[53] = "update",
	[54] = "update",
	[55] = "update",
	[56] = "update",
	[57] = "update",
	[58] = "update",
	[59] = "update",
	[60] = "update",
	[61] = "update",
	[62] = "regular",
	[63] = "regular",
	[64] = "regular",
	[65] = "regular",
	[66] = "regular",
	[67] = "regular",
	[68] = "regular",
	[69] = "regular",
	[70] = "regular",
	[71] = "regular",
	[72] = "regular",
	[73] = "regular",
	[74] = "regular",
	[75] = "regular",
	[76] = "regular",
	[77] = "regular",
	[78] = "regular",
	[79] = "regular",
	[80] = "regular",
	[81] = "regular",
};
char const*const orwl_taskdep_dnames[82] = {
	[0] = "orwl_taskdep_shutdown",
	[1] = "orwl_taskdep_shutdown",
	[2] = "orwl_taskdep_shutdown",
	[3] = "orwl_taskdep_shutdown",
	[4] = "orwl_taskdep_shutdown",
	[5] = "orwl_taskdep_shutdown",
	[6] = "orwl_taskdep_shutdown",
	[7] = "orwl_taskdep_shutdown",
	[8] = "orwl_taskdep_shutdown",
	[9] = "orwl_taskdep_shutdown",
	[10] = "orwl_taskdep_shutdown",
	[11] = "orwl_taskdep_shutdown",
	[12] = "orwl_taskdep_shutdown",
	[13] = "orwl_taskdep_shutdown",
	[14] = "orwl_taskdep_shutdown",
	[15] = "orwl_taskdep_shutdown",
	[16] = "orwl_taskdep_shutdown",
	[17] = "orwl_taskdep_shutdown",
	[18] = "orwl_taskdep_shutdown",
	[19] = "orwl_taskdep_shutdown",
	[20] = "orwl_taskdep_shutdown",
	[21] = "orwl_taskdep_shutdown",
	[22] = "orwl_taskdep_shutdown",
	[23] = "orwl_taskdep_shutdown",
	[24] = "orwl_taskdep_shutdown",
	[25] = "orwl_taskdep_shutdown",
	[26] = "orwl_taskdep_shutdown",
	[27] = "orwl_taskdep_shutdown",
	[28] = "orwl_taskdep_shutdown",
	[29] = "orwl_taskdep_shutdown",
	[30] = "orwl_taskdep_shutdown",
	[31] = "orwl_taskdep_shutdown",
	[32] = "orwl_taskdep_shutdown",
	[33] = "orwl_taskdep_shutdown",
	[34] = "orwl_taskdep_shutdown",
	[35] = "orwl_taskdep_shutdown",
	[36] = "orwl_taskdep_shutdown",
	[37] = "orwl_taskdep_shutdown",
	[38] = "orwl_taskdep_shutdown",
	[39] = "orwl_taskdep_shutdown",
	[40] = "orwl_taskdep_shutdown",
	[41] = "orwl_taskdep_shutdown",
	[42] = "orwl_taskdep_shutdown",
	[43] = "orwl_taskdep_shutdown",
	[44] = "orwl_taskdep_shutdown",
	[45] = "orwl_taskdep_shutdown",
	[46] = "orwl_taskdep_shutdown",
	[47] = "orwl_taskdep_shutdown",
	[48] = "orwl_taskdep_shutdown",
	[49] = "orwl_taskdep_shutdown",
	[50] = "orwl_taskdep_shutdown",
	[51] = "orwl_taskdep_shutdown",
	[52] = "orwl_taskdep_shutdown",
	[53] = "orwl_taskdep_shutdown",
	[54] = "orwl_taskdep_shutdown",
	[55] = "orwl_taskdep_shutdown",
	[56] = "orwl_taskdep_shutdown",
	[57] = "orwl_taskdep_shutdown",
	[58] = "orwl_taskdep_shutdown",
	[59] = "orwl_taskdep_shutdown",
	[60] = "orwl_taskdep_shutdown",
	[61] = "orwl_taskdep_shutdown",
	[62] = "orwl_taskdep_shutdown",
	[63] = "orwl_taskdep_shutdown",
	[64] = "orwl_taskdep_shutdown",
	[65] = "orwl_taskdep_shutdown",
	[66] = "orwl_taskdep_shutdown",
	[67] = "orwl_taskdep_shutdown",
	[68] = "orwl_taskdep_shutdown",
	[69] = "orwl_taskdep_shutdown",
	[70] = "orwl_taskdep_shutdown",
	[71] = "orwl_taskdep_shutdown",
	[72] = "orwl_taskdep_shutdown",
	[73] = "orwl_taskdep_shutdown",
	[74] = "orwl_taskdep_shutdown",
	[75] = "orwl_taskdep_shutdown",
	[76] = "orwl_taskdep_shutdown",
	[77] = "orwl_taskdep_shutdown",
	[78] = "orwl_taskdep_shutdown",
	[79] = "orwl_taskdep_shutdown",
	[80] = "orwl_taskdep_shutdown",
	[81] = "orwl_taskdep_shutdown",
};

// Initializers for ports. The table index is the index at the source, since only
// these are guaranteed to be unique. The values here are pointers to const
// qualified compound literals, or 0 if no "init" has been given for the port.
static orwl_taskdep_vector const*const orwl_taskdep_defaults[82][4] = {
	[0] = {
		[0] = 0, // no pre-init for dist_2_1_t
	},
	[1] = {
		[0] = 0, // no pre-init for dist_1_2_l
	},
	[2] = {
		[0] = 0, // no pre-init for dist_2_2_t
	},
	[3] = {
		[0] = &(orwl_taskdep_vector const){ +0 }, // dist_1_1_r
	},
	[4] = {
		[0] = 0, // no pre-init for dist_1_3_l
	},
	[5] = {
		[0] = 0, // no pre-init for dist_2_3_t
	},
	[6] = {
		[0] = &(orwl_taskdep_vector const){ +0 }, // dist_1_2_r
	},
	[7] = {
		[0] = 0, // no pre-init for dist_1_4_l
	},
	[8] = {
		[0] = 0, // no pre-init for dist_2_4_t
	},
	[9] = {
		[0] = &(orwl_taskdep_vector const){ +0 }, // dist_1_3_r
	},
	[10] = {
		[0] = 0, // no pre-init for dist_1_5_l
	},
	[11] = {
		[0] = 0, // no pre-init for dist_2_5_t
	},
	[12] = {
		[0] = &(orwl_taskdep_vector const){ +0 }, // dist_1_4_r
	},
	[13] = {
		[0] = 0, // no pre-init for dist_3_1_t
	},
	[14] = {
		[0] = 0, // no pre-init for dist_2_2_l
	},
	[15] = {
		[0] = &(orwl_taskdep_vector const){ +0 }, // dist_1_1_b
	},
	[16] = {
		[0] = 0, // no pre-init for dist_3_2_t
	},
	[17] = {
		[0] = &(orwl_taskdep_vector const){ +0 }, // dist_2_1_r
	},
	[18] = {
		[0] = 0, // no pre-init for dist_2_3_l
	},
	[19] = {
		[0] = &(orwl_taskdep_vector const){ +0 }, // dist_1_2_b
	},
	[20] = {
		[0] = 0, // no pre-init for dist_3_3_t
	},
	[21] = {
		[0] = &(orwl_taskdep_vector const){ +0 }, // dist_2_2_r
	},
	[22] = {
		[0] = 0, // no pre-init for dist_2_4_l
	},
	[23] = {
		[0] = &(orwl_taskdep_vector const){ +0 }, // dist_1_3_b
	},
	[24] = {
		[0] = 0, // no pre-init for dist_3_4_t
	},
	[25] = {
		[0] = &(orwl_taskdep_vector const){ +0 }, // dist_2_3_r
	},
	[26] = {
		[0] = 0, // no pre-init for dist_2_5_l
	},
	[27] = {
		[0] = &(orwl_taskdep_vector const){ +0 }, // dist_1_4_b
	},
	[28] = {
		[0] = 0, // no pre-init for dist_3_5_t
	},
	[29] = {
		[0] = &(orwl_taskdep_vector const){ +0 }, // dist_2_4_r
	},
	[30] = {
		[0] = &(orwl_taskdep_vector const){ +0 }, // dist_1_5_b
	},
	[31] = {
		[0] = 0, // no pre-init for dist_4_1_t
	},
	[32] = {
		[0] = 0, // no pre-init for dist_3_2_l
	},
	[33] = {
		[0] = &(orwl_taskdep_vector const){ +0 }, // dist_2_1_b
	},
	[34] = {
		[0] = 0, // no pre-init for dist_4_2_t
	},
	[35] = {
		[0] = &(orwl_taskdep_vector const){ +0 }, // dist_3_1_r
	},
	[36] = {
		[0] = 0, // no pre-init for dist_3_3_l
	},
	[37] = {
		[0] = &(orwl_taskdep_vector const){ +0 }, // dist_2_2_b
	},
	[38] = {
		[0] = 0, // no pre-init for dist_4_3_t
	},
	[39] = {
		[0] = &(orwl_taskdep_vector const){ +0 }, // dist_3_2_r
	},
	[40] = {
		[0] = 0, // no pre-init for dist_3_4_l
	},
	[41] = {
		[0] = &(orwl_taskdep_vector const){ +0 }, // dist_2_3_b
	},
	[42] = {
		[0] = 0, // no pre-init for dist_4_4_t
	},
	[43] = {
		[0] = &(orwl_taskdep_vector const){ +0 }, // dist_3_3_r
	},
	[44] = {
		[0] = 0, // no pre-init for dist_3_5_l
	},
	[45] = {
		[0] = &(orwl_taskdep_vector const){ +0 }, // dist_2_4_b
	},
	[46] = {
		[0] = 0, // no pre-init for dist_4_5_t
	},
	[47] = {
		[0] = &(orwl_taskdep_vector const){ +0 }, // dist_3_4_r
	},
	[48] = {
		[0] = &(orwl_taskdep_vector const){ +0 }, // dist_2_5_b
	},
	[49] = {
		[0] = 0, // no pre-init for dist_4_2_l
	},
	[50] = {
		[0] = &(orwl_taskdep_vector const){ +0 }, // dist_3_1_b
	},
	[51] = {
		[0] = &(orwl_taskdep_vector const){ +0 }, // dist_4_1_r
	},
	[52] = {
		[0] = 0, // no pre-init for dist_4_3_l
	},
	[53] = {
		[0] = &(orwl_taskdep_vector const){ +0 }, // dist_3_2_b
	},
	[54] = {
		[0] = &(orwl_taskdep_vector const){ +0 }, // dist_4_2_r
	},
	[55] = {
		[0] = 0, // no pre-init for dist_4_4_l
	},
	[56] = {
		[0] = &(orwl_taskdep_vector const){ +0 }, // dist_3_3_b
	},
	[57] = {
		[0] = &(orwl_taskdep_vector const){ +0 }, // dist_4_3_r
	},
	[58] = {
		[0] = 0, // no pre-init for dist_4_5_l
	},
	[59] = {
		[0] = &(orwl_taskdep_vector const){ +0 }, // dist_3_4_b
	},
	[60] = {
		[0] = &(orwl_taskdep_vector const){ +0 }, // dist_4_4_r
	},
	[61] = {
		[0] = &(orwl_taskdep_vector const){ +0 }, // dist_3_5_b
	},
	[62] = {
		[0] = 0, // no pre-init for out_1_1_b
		[1] = 0, // no pre-init for out_1_1_l
		[2] = 0, // no pre-init for out_1_1_t
		[3] = 0, // no pre-init for out_1_1_r
	},
	[63] = {
		[0] = 0, // no pre-init for out_1_2_b
		[1] = 0, // no pre-init for out_1_2_l
		[2] = 0, // no pre-init for out_1_2_t
		[3] = 0, // no pre-init for out_1_2_r
	},
	[64] = {
		[0] = 0, // no pre-init for out_1_3_b
		[1] = 0, // no pre-init for out_1_3_l
		[2] = 0, // no pre-init for out_1_3_t
		[3] = 0, // no pre-init for out_1_3_r
	},
	[65] = {
		[0] = 0, // no pre-init for out_1_4_b
		[1] = 0, // no pre-init for out_1_4_l
		[2] = 0, // no pre-init for out_1_4_t
		[3] = 0, // no pre-init for out_1_4_r
	},
	[66] = {
		[0] = 0, // no pre-init for out_1_5_b
		[1] = 0, // no pre-init for out_1_5_l
		[2] = 0, // no pre-init for out_1_5_t
		[3] = 0, // no pre-init for out_1_5_r
	},
	[67] = {
		[0] = 0, // no pre-init for out_2_1_b
		[1] = 0, // no pre-init for out_2_1_l
		[2] = 0, // no pre-init for out_2_1_t
		[3] = 0, // no pre-init for out_2_1_r
	},
	[68] = {
		[0] = 0, // no pre-init for out_2_2_b
		[1] = 0, // no pre-init for out_2_2_l
		[2] = 0, // no pre-init for out_2_2_t
		[3] = 0, // no pre-init for out_2_2_r
	},
	[69] = {
		[0] = 0, // no pre-init for out_2_3_b
		[1] = 0, // no pre-init for out_2_3_l
		[2] = 0, // no pre-init for out_2_3_t
		[3] = 0, // no pre-init for out_2_3_r
	},
	[70] = {
		[0] = 0, // no pre-init for out_2_4_b
		[1] = 0, // no pre-init for out_2_4_l
		[2] = 0, // no pre-init for out_2_4_t
		[3] = 0, // no pre-init for out_2_4_r
	},
	[71] = {
		[0] = 0, // no pre-init for out_2_5_b
		[1] = 0, // no pre-init for out_2_5_l
		[2] = 0, // no pre-init for out_2_5_t
		[3] = 0, // no pre-init for out_2_5_r
	},
	[72] = {
		[0] = 0, // no pre-init for out_3_1_b
		[1] = 0, // no pre-init for out_3_1_l
		[2] = 0, // no pre-init for out_3_1_t
		[3] = 0, // no pre-init for out_3_1_r
	},
	[73] = {
		[0] = 0, // no pre-init for out_3_2_b
		[1] = 0, // no pre-init for out_3_2_l
		[2] = 0, // no pre-init for out_3_2_t
		[3] = 0, // no pre-init for out_3_2_r
	},
	[74] = {
		[0] = 0, // no pre-init for out_3_3_b
		[1] = 0, // no pre-init for out_3_3_l
		[2] = 0, // no pre-init for out_3_3_t
		[3] = 0, // no pre-init for out_3_3_r
	},
	[75] = {
		[0] = 0, // no pre-init for out_3_4_b
		[1] = 0, // no pre-init for out_3_4_l
		[2] = 0, // no pre-init for out_3_4_t
		[3] = 0, // no pre-init for out_3_4_r
	},
	[76] = {
		[0] = 0, // no pre-init for out_3_5_b
		[1] = 0, // no pre-init for out_3_5_l
		[2] = 0, // no pre-init for out_3_5_t
		[3] = 0, // no pre-init for out_3_5_r
	},
	[77] = {
		[0] = 0, // no pre-init for out_4_1_b
		[1] = 0, // no pre-init for out_4_1_l
		[2] = 0, // no pre-init for out_4_1_t
		[3] = 0, // no pre-init for out_4_1_r
	},
	[78] = {
		[0] = 0, // no pre-init for out_4_2_b
		[1] = 0, // no pre-init for out_4_2_l
		[2] = 0, // no pre-init for out_4_2_t
		[3] = 0, // no pre-init for out_4_2_r
	},
	[79] = {
		[0] = 0, // no pre-init for out_4_3_b
		[1] = 0, // no pre-init for out_4_3_l
		[2] = 0, // no pre-init for out_4_3_t
		[3] = 0, // no pre-init for out_4_3_r
	},
	[80] = {
		[0] = 0, // no pre-init for out_4_4_b
		[1] = 0, // no pre-init for out_4_4_l
		[2] = 0, // no pre-init for out_4_4_t
		[3] = 0, // no pre-init for out_4_4_r
	},
	[81] = {
		[0] = 0, // no pre-init for out_4_5_b
		[1] = 0, // no pre-init for out_4_5_l
		[2] = 0, // no pre-init for out_4_5_t
		[3] = 0, // no pre-init for out_4_5_r
	},
};

// The const property for ports. The table index is the index at the source, since only
// these are guaranteed to be unique. .
static bool const orwl_taskdep_const[82][4] = {
	[0] = {
		[0] = 0, // dist_2_1_t
	},
	[1] = {
		[0] = 0, // dist_1_2_l
	},
	[2] = {
		[0] = 0, // dist_2_2_t
	},
	[3] = {
		[0] = 0, // dist_1_1_r
	},
	[4] = {
		[0] = 0, // dist_1_3_l
	},
	[5] = {
		[0] = 0, // dist_2_3_t
	},
	[6] = {
		[0] = 0, // dist_1_2_r
	},
	[7] = {
		[0] = 0, // dist_1_4_l
	},
	[8] = {
		[0] = 0, // dist_2_4_t
	},
	[9] = {
		[0] = 0, // dist_1_3_r
	},
	[10] = {
		[0] = 0, // dist_1_5_l
	},
	[11] = {
		[0] = 0, // dist_2_5_t
	},
	[12] = {
		[0] = 0, // dist_1_4_r
	},
	[13] = {
		[0] = 0, // dist_3_1_t
	},
	[14] = {
		[0] = 0, // dist_2_2_l
	},
	[15] = {
		[0] = 0, // dist_1_1_b
	},
	[16] = {
		[0] = 0, // dist_3_2_t
	},
	[17] = {
		[0] = 0, // dist_2_1_r
	},
	[18] = {
		[0] = 0, // dist_2_3_l
	},
	[19] = {
		[0] = 0, // dist_1_2_b
	},
	[20] = {
		[0] = 0, // dist_3_3_t
	},
	[21] = {
		[0] = 0, // dist_2_2_r
	},
	[22] = {
		[0] = 0, // dist_2_4_l
	},
	[23] = {
		[0] = 0, // dist_1_3_b
	},
	[24] = {
		[0] = 0, // dist_3_4_t
	},
	[25] = {
		[0] = 0, // dist_2_3_r
	},
	[26] = {
		[0] = 0, // dist_2_5_l
	},
	[27] = {
		[0] = 0, // dist_1_4_b
	},
	[28] = {
		[0] = 0, // dist_3_5_t
	},
	[29] = {
		[0] = 0, // dist_2_4_r
	},
	[30] = {
		[0] = 0, // dist_1_5_b
	},
	[31] = {
		[0] = 0, // dist_4_1_t
	},
	[32] = {
		[0] = 0, // dist_3_2_l
	},
	[33] = {
		[0] = 0, // dist_2_1_b
	},
	[34] = {
		[0] = 0, // dist_4_2_t
	},
	[35] = {
		[0] = 0, // dist_3_1_r
	},
	[36] = {
		[0] = 0, // dist_3_3_l
	},
	[37] = {
		[0] = 0, // dist_2_2_b
	},
	[38] = {
		[0] = 0, // dist_4_3_t
	},
	[39] = {
		[0] = 0, // dist_3_2_r
	},
	[40] = {
		[0] = 0, // dist_3_4_l
	},
	[41] = {
		[0] = 0, // dist_2_3_b
	},
	[42] = {
		[0] = 0, // dist_4_4_t
	},
	[43] = {
		[0] = 0, // dist_3_3_r
	},
	[44] = {
		[0] = 0, // dist_3_5_l
	},
	[45] = {
		[0] = 0, // dist_2_4_b
	},
	[46] = {
		[0] = 0, // dist_4_5_t
	},
	[47] = {
		[0] = 0, // dist_3_4_r
	},
	[48] = {
		[0] = 0, // dist_2_5_b
	},
	[49] = {
		[0] = 0, // dist_4_2_l
	},
	[50] = {
		[0] = 0, // dist_3_1_b
	},
	[51] = {
		[0] = 0, // dist_4_1_r
	},
	[52] = {
		[0] = 0, // dist_4_3_l
	},
	[53] = {
		[0] = 0, // dist_3_2_b
	},
	[54] = {
		[0] = 0, // dist_4_2_r
	},
	[55] = {
		[0] = 0, // dist_4_4_l
	},
	[56] = {
		[0] = 0, // dist_3_3_b
	},
	[57] = {
		[0] = 0, // dist_4_3_r
	},
	[58] = {
		[0] = 0, // dist_4_5_l
	},
	[59] = {
		[0] = 0, // dist_3_4_b
	},
	[60] = {
		[0] = 0, // dist_4_4_r
	},
	[61] = {
		[0] = 0, // dist_3_5_b
	},
	[62] = {
		[0] = 0, // out_1_1_b
		[1] = 0, // out_1_1_l
		[2] = 0, // out_1_1_t
		[3] = 0, // out_1_1_r
	},
	[63] = {
		[0] = 0, // out_1_2_b
		[1] = 0, // out_1_2_l
		[2] = 0, // out_1_2_t
		[3] = 0, // out_1_2_r
	},
	[64] = {
		[0] = 0, // out_1_3_b
		[1] = 0, // out_1_3_l
		[2] = 0, // out_1_3_t
		[3] = 0, // out_1_3_r
	},
	[65] = {
		[0] = 0, // out_1_4_b
		[1] = 0, // out_1_4_l
		[2] = 0, // out_1_4_t
		[3] = 0, // out_1_4_r
	},
	[66] = {
		[0] = 0, // out_1_5_b
		[1] = 0, // out_1_5_l
		[2] = 0, // out_1_5_t
		[3] = 0, // out_1_5_r
	},
	[67] = {
		[0] = 0, // out_2_1_b
		[1] = 0, // out_2_1_l
		[2] = 0, // out_2_1_t
		[3] = 0, // out_2_1_r
	},
	[68] = {
		[0] = 0, // out_2_2_b
		[1] = 0, // out_2_2_l
		[2] = 0, // out_2_2_t
		[3] = 0, // out_2_2_r
	},
	[69] = {
		[0] = 0, // out_2_3_b
		[1] = 0, // out_2_3_l
		[2] = 0, // out_2_3_t
		[3] = 0, // out_2_3_r
	},
	[70] = {
		[0] = 0, // out_2_4_b
		[1] = 0, // out_2_4_l
		[2] = 0, // out_2_4_t
		[3] = 0, // out_2_4_r
	},
	[71] = {
		[0] = 0, // out_2_5_b
		[1] = 0, // out_2_5_l
		[2] = 0, // out_2_5_t
		[3] = 0, // out_2_5_r
	},
	[72] = {
		[0] = 0, // out_3_1_b
		[1] = 0, // out_3_1_l
		[2] = 0, // out_3_1_t
		[3] = 0, // out_3_1_r
	},
	[73] = {
		[0] = 0, // out_3_2_b
		[1] = 0, // out_3_2_l
		[2] = 0, // out_3_2_t
		[3] = 0, // out_3_2_r
	},
	[74] = {
		[0] = 0, // out_3_3_b
		[1] = 0, // out_3_3_l
		[2] = 0, // out_3_3_t
		[3] = 0, // out_3_3_r
	},
	[75] = {
		[0] = 0, // out_3_4_b
		[1] = 0, // out_3_4_l
		[2] = 0, // out_3_4_t
		[3] = 0, // out_3_4_r
	},
	[76] = {
		[0] = 0, // out_3_5_b
		[1] = 0, // out_3_5_l
		[2] = 0, // out_3_5_t
		[3] = 0, // out_3_5_r
	},
	[77] = {
		[0] = 0, // out_4_1_b
		[1] = 0, // out_4_1_l
		[2] = 0, // out_4_1_t
		[3] = 0, // out_4_1_r
	},
	[78] = {
		[0] = 0, // out_4_2_b
		[1] = 0, // out_4_2_l
		[2] = 0, // out_4_2_t
		[3] = 0, // out_4_2_r
	},
	[79] = {
		[0] = 0, // out_4_3_b
		[1] = 0, // out_4_3_l
		[2] = 0, // out_4_3_t
		[3] = 0, // out_4_3_r
	},
	[80] = {
		[0] = 0, // out_4_4_b
		[1] = 0, // out_4_4_l
		[2] = 0, // out_4_4_t
		[3] = 0, // out_4_4_r
	},
	[81] = {
		[0] = 0, // out_4_5_b
		[1] = 0, // out_4_5_l
		[2] = 0, // out_4_5_t
		[3] = 0, // out_4_5_r
	},
};

// For each output port, provide the textual information about the link
char const*const orwl_taskdep_oports[82][4] = {
	[0] = {
		[0] = "dist_2_1_t",
	},
	[1] = {
		[0] = "dist_1_2_l",
	},
	[2] = {
		[0] = "dist_2_2_t",
	},
	[3] = {
		[0] = "dist_1_1_r",
	},
	[4] = {
		[0] = "dist_1_3_l",
	},
	[5] = {
		[0] = "dist_2_3_t",
	},
	[6] = {
		[0] = "dist_1_2_r",
	},
	[7] = {
		[0] = "dist_1_4_l",
	},
	[8] = {
		[0] = "dist_2_4_t",
	},
	[9] = {
		[0] = "dist_1_3_r",
	},
	[10] = {
		[0] = "dist_1_5_l",
	},
	[11] = {
		[0] = "dist_2_5_t",
	},
	[12] = {
		[0] = "dist_1_4_r",
	},
	[13] = {
		[0] = "dist_3_1_t",
	},
	[14] = {
		[0] = "dist_2_2_l",
	},
	[15] = {
		[0] = "dist_1_1_b",
	},
	[16] = {
		[0] = "dist_3_2_t",
	},
	[17] = {
		[0] = "dist_2_1_r",
	},
	[18] = {
		[0] = "dist_2_3_l",
	},
	[19] = {
		[0] = "dist_1_2_b",
	},
	[20] = {
		[0] = "dist_3_3_t",
	},
	[21] = {
		[0] = "dist_2_2_r",
	},
	[22] = {
		[0] = "dist_2_4_l",
	},
	[23] = {
		[0] = "dist_1_3_b",
	},
	[24] = {
		[0] = "dist_3_4_t",
	},
	[25] = {
		[0] = "dist_2_3_r",
	},
	[26] = {
		[0] = "dist_2_5_l",
	},
	[27] = {
		[0] = "dist_1_4_b",
	},
	[28] = {
		[0] = "dist_3_5_t",
	},
	[29] = {
		[0] = "dist_2_4_r",
	},
	[30] = {
		[0] = "dist_1_5_b",
	},
	[31] = {
		[0] = "dist_4_1_t",
	},
	[32] = {
		[0] = "dist_3_2_l",
	},
	[33] = {
		[0] = "dist_2_1_b",
	},
	[34] = {
		[0] = "dist_4_2_t",
	},
	[35] = {
		[0] = "dist_3_1_r",
	},
	[36] = {
		[0] = "dist_3_3_l",
	},
	[37] = {
		[0] = "dist_2_2_b",
	},
	[38] = {
		[0] = "dist_4_3_t",
	},
	[39] = {
		[0] = "dist_3_2_r",
	},
	[40] = {
		[0] = "dist_3_4_l",
	},
	[41] = {
		[0] = "dist_2_3_b",
	},
	[42] = {
		[0] = "dist_4_4_t",
	},
	[43] = {
		[0] = "dist_3_3_r",
	},
	[44] = {
		[0] = "dist_3_5_l",
	},
	[45] = {
		[0] = "dist_2_4_b",
	},
	[46] = {
		[0] = "dist_4_5_t",
	},
	[47] = {
		[0] = "dist_3_4_r",
	},
	[48] = {
		[0] = "dist_2_5_b",
	},
	[49] = {
		[0] = "dist_4_2_l",
	},
	[50] = {
		[0] = "dist_3_1_b",
	},
	[51] = {
		[0] = "dist_4_1_r",
	},
	[52] = {
		[0] = "dist_4_3_l",
	},
	[53] = {
		[0] = "dist_3_2_b",
	},
	[54] = {
		[0] = "dist_4_2_r",
	},
	[55] = {
		[0] = "dist_4_4_l",
	},
	[56] = {
		[0] = "dist_3_3_b",
	},
	[57] = {
		[0] = "dist_4_3_r",
	},
	[58] = {
		[0] = "dist_4_5_l",
	},
	[59] = {
		[0] = "dist_3_4_b",
	},
	[60] = {
		[0] = "dist_4_4_r",
	},
	[61] = {
		[0] = "dist_3_5_b",
	},
	[62] = {
		[0] = "out_1_1_b",
		[1] = "out_1_1_l",
		[2] = "out_1_1_t",
		[3] = "out_1_1_r",
	},
	[63] = {
		[0] = "out_1_2_b",
		[1] = "out_1_2_l",
		[2] = "out_1_2_t",
		[3] = "out_1_2_r",
	},
	[64] = {
		[0] = "out_1_3_b",
		[1] = "out_1_3_l",
		[2] = "out_1_3_t",
		[3] = "out_1_3_r",
	},
	[65] = {
		[0] = "out_1_4_b",
		[1] = "out_1_4_l",
		[2] = "out_1_4_t",
		[3] = "out_1_4_r",
	},
	[66] = {
		[0] = "out_1_5_b",
		[1] = "out_1_5_l",
		[2] = "out_1_5_t",
		[3] = "out_1_5_r",
	},
	[67] = {
		[0] = "out_2_1_b",
		[1] = "out_2_1_l",
		[2] = "out_2_1_t",
		[3] = "out_2_1_r",
	},
	[68] = {
		[0] = "out_2_2_b",
		[1] = "out_2_2_l",
		[2] = "out_2_2_t",
		[3] = "out_2_2_r",
	},
	[69] = {
		[0] = "out_2_3_b",
		[1] = "out_2_3_l",
		[2] = "out_2_3_t",
		[3] = "out_2_3_r",
	},
	[70] = {
		[0] = "out_2_4_b",
		[1] = "out_2_4_l",
		[2] = "out_2_4_t",
		[3] = "out_2_4_r",
	},
	[71] = {
		[0] = "out_2_5_b",
		[1] = "out_2_5_l",
		[2] = "out_2_5_t",
		[3] = "out_2_5_r",
	},
	[72] = {
		[0] = "out_3_1_b",
		[1] = "out_3_1_l",
		[2] = "out_3_1_t",
		[3] = "out_3_1_r",
	},
	[73] = {
		[0] = "out_3_2_b",
		[1] = "out_3_2_l",
		[2] = "out_3_2_t",
		[3] = "out_3_2_r",
	},
	[74] = {
		[0] = "out_3_3_b",
		[1] = "out_3_3_l",
		[2] = "out_3_3_t",
		[3] = "out_3_3_r",
	},
	[75] = {
		[0] = "out_3_4_b",
		[1] = "out_3_4_l",
		[2] = "out_3_4_t",
		[3] = "out_3_4_r",
	},
	[76] = {
		[0] = "out_3_5_b",
		[1] = "out_3_5_l",
		[2] = "out_3_5_t",
		[3] = "out_3_5_r",
	},
	[77] = {
		[0] = "out_4_1_b",
		[1] = "out_4_1_l",
		[2] = "out_4_1_t",
		[3] = "out_4_1_r",
	},
	[78] = {
		[0] = "out_4_2_b",
		[1] = "out_4_2_l",
		[2] = "out_4_2_t",
		[3] = "out_4_2_r",
	},
	[79] = {
		[0] = "out_4_3_b",
		[1] = "out_4_3_l",
		[2] = "out_4_3_t",
		[3] = "out_4_3_r",
	},
	[80] = {
		[0] = "out_4_4_b",
		[1] = "out_4_4_l",
		[2] = "out_4_4_t",
		[3] = "out_4_4_r",
	},
	[81] = {
		[0] = "out_4_5_b",
		[1] = "out_4_5_l",
		[2] = "out_4_5_t",
		[3] = "out_4_5_r",
	},
};

// For each input port, provide the textual information about the link
char const*const orwl_taskdep_iports[82][4] = {
	[0] = {
		[0] = "out_1_1_b",
	},
	[1] = {
		[0] = "out_1_1_r",
	},
	[2] = {
		[0] = "out_1_2_b",
	},
	[3] = {
		[0] = "out_1_2_l",
	},
	[4] = {
		[0] = "out_1_2_r",
	},
	[5] = {
		[0] = "out_1_3_b",
	},
	[6] = {
		[0] = "out_1_3_l",
	},
	[7] = {
		[0] = "out_1_3_r",
	},
	[8] = {
		[0] = "out_1_4_b",
	},
	[9] = {
		[0] = "out_1_4_l",
	},
	[10] = {
		[0] = "out_1_4_r",
	},
	[11] = {
		[0] = "out_1_5_b",
	},
	[12] = {
		[0] = "out_1_5_l",
	},
	[13] = {
		[0] = "out_2_1_b",
	},
	[14] = {
		[0] = "out_2_1_r",
	},
	[15] = {
		[0] = "out_2_1_t",
	},
	[16] = {
		[0] = "out_2_2_b",
	},
	[17] = {
		[0] = "out_2_2_l",
	},
	[18] = {
		[0] = "out_2_2_r",
	},
	[19] = {
		[0] = "out_2_2_t",
	},
	[20] = {
		[0] = "out_2_3_b",
	},
	[21] = {
		[0] = "out_2_3_l",
	},
	[22] = {
		[0] = "out_2_3_r",
	},
	[23] = {
		[0] = "out_2_3_t",
	},
	[24] = {
		[0] = "out_2_4_b",
	},
	[25] = {
		[0] = "out_2_4_l",
	},
	[26] = {
		[0] = "out_2_4_r",
	},
	[27] = {
		[0] = "out_2_4_t",
	},
	[28] = {
		[0] = "out_2_5_b",
	},
	[29] = {
		[0] = "out_2_5_l",
	},
	[30] = {
		[0] = "out_2_5_t",
	},
	[31] = {
		[0] = "out_3_1_b",
	},
	[32] = {
		[0] = "out_3_1_r",
	},
	[33] = {
		[0] = "out_3_1_t",
	},
	[34] = {
		[0] = "out_3_2_b",
	},
	[35] = {
		[0] = "out_3_2_l",
	},
	[36] = {
		[0] = "out_3_2_r",
	},
	[37] = {
		[0] = "out_3_2_t",
	},
	[38] = {
		[0] = "out_3_3_b",
	},
	[39] = {
		[0] = "out_3_3_l",
	},
	[40] = {
		[0] = "out_3_3_r",
	},
	[41] = {
		[0] = "out_3_3_t",
	},
	[42] = {
		[0] = "out_3_4_b",
	},
	[43] = {
		[0] = "out_3_4_l",
	},
	[44] = {
		[0] = "out_3_4_r",
	},
	[45] = {
		[0] = "out_3_4_t",
	},
	[46] = {
		[0] = "out_3_5_b",
	},
	[47] = {
		[0] = "out_3_5_l",
	},
	[48] = {
		[0] = "out_3_5_t",
	},
	[49] = {
		[0] = "out_4_1_r",
	},
	[50] = {
		[0] = "out_4_1_t",
	},
	[51] = {
		[0] = "out_4_2_l",
	},
	[52] = {
		[0] = "out_4_2_r",
	},
	[53] = {
		[0] = "out_4_2_t",
	},
	[54] = {
		[0] = "out_4_3_l",
	},
	[55] = {
		[0] = "out_4_3_r",
	},
	[56] = {
		[0] = "out_4_3_t",
	},
	[57] = {
		[0] = "out_4_4_l",
	},
	[58] = {
		[0] = "out_4_4_r",
	},
	[59] = {
		[0] = "out_4_4_t",
	},
	[60] = {
		[0] = "out_4_5_l",
	},
	[61] = {
		[0] = "out_4_5_t",
	},
	[62] = {
		[0] = "dist_1_1_b",
		[1] = "dist_1_1_l",
		[2] = "dist_1_1_t",
		[3] = "dist_1_1_r",
	},
	[63] = {
		[0] = "dist_1_2_b",
		[1] = "dist_1_2_l",
		[2] = "dist_1_2_t",
		[3] = "dist_1_2_r",
	},
	[64] = {
		[0] = "dist_1_3_b",
		[1] = "dist_1_3_l",
		[2] = "dist_1_3_t",
		[3] = "dist_1_3_r",
	},
	[65] = {
		[0] = "dist_1_4_b",
		[1] = "dist_1_4_l",
		[2] = "dist_1_4_t",
		[3] = "dist_1_4_r",
	},
	[66] = {
		[0] = "dist_1_5_b",
		[1] = "dist_1_5_l",
		[2] = "dist_1_5_t",
		[3] = "dist_1_5_r",
	},
	[67] = {
		[0] = "dist_2_1_b",
		[1] = "dist_2_1_l",
		[2] = "dist_2_1_t",
		[3] = "dist_2_1_r",
	},
	[68] = {
		[0] = "dist_2_2_b",
		[1] = "dist_2_2_l",
		[2] = "dist_2_2_t",
		[3] = "dist_2_2_r",
	},
	[69] = {
		[0] = "dist_2_3_b",
		[1] = "dist_2_3_l",
		[2] = "dist_2_3_t",
		[3] = "dist_2_3_r",
	},
	[70] = {
		[0] = "dist_2_4_b",
		[1] = "dist_2_4_l",
		[2] = "dist_2_4_t",
		[3] = "dist_2_4_r",
	},
	[71] = {
		[0] = "dist_2_5_b",
		[1] = "dist_2_5_l",
		[2] = "dist_2_5_t",
		[3] = "dist_2_5_r",
	},
	[72] = {
		[0] = "dist_3_1_b",
		[1] = "dist_3_1_l",
		[2] = "dist_3_1_t",
		[3] = "dist_3_1_r",
	},
	[73] = {
		[0] = "dist_3_2_b",
		[1] = "dist_3_2_l",
		[2] = "dist_3_2_t",
		[3] = "dist_3_2_r",
	},
	[74] = {
		[0] = "dist_3_3_b",
		[1] = "dist_3_3_l",
		[2] = "dist_3_3_t",
		[3] = "dist_3_3_r",
	},
	[75] = {
		[0] = "dist_3_4_b",
		[1] = "dist_3_4_l",
		[2] = "dist_3_4_t",
		[3] = "dist_3_4_r",
	},
	[76] = {
		[0] = "dist_3_5_b",
		[1] = "dist_3_5_l",
		[2] = "dist_3_5_t",
		[3] = "dist_3_5_r",
	},
	[77] = {
		[0] = "dist_4_1_b",
		[1] = "dist_4_1_l",
		[2] = "dist_4_1_t",
		[3] = "dist_4_1_r",
	},
	[78] = {
		[0] = "dist_4_2_b",
		[1] = "dist_4_2_l",
		[2] = "dist_4_2_t",
		[3] = "dist_4_2_r",
	},
	[79] = {
		[0] = "dist_4_3_b",
		[1] = "dist_4_3_l",
		[2] = "dist_4_3_t",
		[3] = "dist_4_3_r",
	},
	[80] = {
		[0] = "dist_4_4_b",
		[1] = "dist_4_4_l",
		[2] = "dist_4_4_t",
		[3] = "dist_4_4_r",
	},
	[81] = {
		[0] = "dist_4_5_b",
		[1] = "dist_4_5_l",
		[2] = "dist_4_5_t",
		[3] = "dist_4_5_r",
	},
};

// The outdegree for each node
_Alignas(32) int64_t const orwl_taskdep_deg_out[82] = {
	[0] = 1,
	[1] = 1,
	[2] = 1,
	[3] = 1,
	[4] = 1,
	[5] = 1,
	[6] = 1,
	[7] = 1,
	[8] = 1,
	[9] = 1,
	[10] = 1,
	[11] = 1,
	[12] = 1,
	[13] = 1,
	[14] = 1,
	[15] = 1,
	[16] = 1,
	[17] = 1,
	[18] = 1,
	[19] = 1,
	[20] = 1,
	[21] = 1,
	[22] = 1,
	[23] = 1,
	[24] = 1,
	[25] = 1,
	[26] = 1,
	[27] = 1,
	[28] = 1,
	[29] = 1,
	[30] = 1,
	[31] = 1,
	[32] = 1,
	[33] = 1,
	[34] = 1,
	[35] = 1,
	[36] = 1,
	[37] = 1,
	[38] = 1,
	[39] = 1,
	[40] = 1,
	[41] = 1,
	[42] = 1,
	[43] = 1,
	[44] = 1,
	[45] = 1,
	[46] = 1,
	[47] = 1,
	[48] = 1,
	[49] = 1,
	[50] = 1,
	[51] = 1,
	[52] = 1,
	[53] = 1,
	[54] = 1,
	[55] = 1,
	[56] = 1,
	[57] = 1,
	[58] = 1,
	[59] = 1,
	[60] = 1,
	[61] = 1,
	[62] = 4,
	[63] = 4,
	[64] = 4,
	[65] = 4,
	[66] = 4,
	[67] = 4,
	[68] = 4,
	[69] = 4,
	[70] = 4,
	[71] = 4,
	[72] = 4,
	[73] = 4,
	[74] = 4,
	[75] = 4,
	[76] = 4,
	[77] = 4,
	[78] = 4,
	[79] = 4,
	[80] = 4,
	[81] = 4,
};

// The indegree for each node
_Alignas(32) int64_t const orwl_taskdep_deg_inp[82] = {
	[0] = 1,
	[1] = 1,
	[2] = 1,
	[3] = 1,
	[4] = 1,
	[5] = 1,
	[6] = 1,
	[7] = 1,
	[8] = 1,
	[9] = 1,
	[10] = 1,
	[11] = 1,
	[12] = 1,
	[13] = 1,
	[14] = 1,
	[15] = 1,
	[16] = 1,
	[17] = 1,
	[18] = 1,
	[19] = 1,
	[20] = 1,
	[21] = 1,
	[22] = 1,
	[23] = 1,
	[24] = 1,
	[25] = 1,
	[26] = 1,
	[27] = 1,
	[28] = 1,
	[29] = 1,
	[30] = 1,
	[31] = 1,
	[32] = 1,
	[33] = 1,
	[34] = 1,
	[35] = 1,
	[36] = 1,
	[37] = 1,
	[38] = 1,
	[39] = 1,
	[40] = 1,
	[41] = 1,
	[42] = 1,
	[43] = 1,
	[44] = 1,
	[45] = 1,
	[46] = 1,
	[47] = 1,
	[48] = 1,
	[49] = 1,
	[50] = 1,
	[51] = 1,
	[52] = 1,
	[53] = 1,
	[54] = 1,
	[55] = 1,
	[56] = 1,
	[57] = 1,
	[58] = 1,
	[59] = 1,
	[60] = 1,
	[61] = 1,
	[62] = 4,
	[63] = 4,
	[64] = 4,
	[65] = 4,
	[66] = 4,
	[67] = 4,
	[68] = 4,
	[69] = 4,
	[70] = 4,
	[71] = 4,
	[72] = 4,
	[73] = 4,
	[74] = 4,
	[75] = 4,
	[76] = 4,
	[77] = 4,
	[78] = 4,
	[79] = 4,
	[80] = 4,
	[81] = 4,
};

// The number of each output port, or -1 if not this number is not available
int64_t const*const orwl_taskdep_onum[82] = {
	[0] = (int64_t const[4]){
		[0] = 2,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[1] = (int64_t const[4]){
		[0] = 1,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[2] = (int64_t const[4]){
		[0] = 2,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[3] = (int64_t const[4]){
		[0] = 1,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[4] = (int64_t const[4]){
		[0] = 1,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[5] = (int64_t const[4]){
		[0] = 2,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[6] = (int64_t const[4]){
		[0] = 1,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[7] = (int64_t const[4]){
		[0] = 1,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[8] = (int64_t const[4]){
		[0] = 2,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[9] = (int64_t const[4]){
		[0] = 1,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[10] = (int64_t const[4]){
		[0] = 1,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[11] = (int64_t const[4]){
		[0] = 2,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[12] = (int64_t const[4]){
		[0] = 1,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[13] = (int64_t const[4]){
		[0] = 3,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[14] = (int64_t const[4]){
		[0] = 2,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[15] = (int64_t const[4]){
		[0] = 1,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[16] = (int64_t const[4]){
		[0] = 3,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[17] = (int64_t const[4]){
		[0] = 2,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[18] = (int64_t const[4]){
		[0] = 2,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[19] = (int64_t const[4]){
		[0] = 1,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[20] = (int64_t const[4]){
		[0] = 3,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[21] = (int64_t const[4]){
		[0] = 2,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[22] = (int64_t const[4]){
		[0] = 2,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[23] = (int64_t const[4]){
		[0] = 1,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[24] = (int64_t const[4]){
		[0] = 3,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[25] = (int64_t const[4]){
		[0] = 2,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[26] = (int64_t const[4]){
		[0] = 2,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[27] = (int64_t const[4]){
		[0] = 1,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[28] = (int64_t const[4]){
		[0] = 3,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[29] = (int64_t const[4]){
		[0] = 2,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[30] = (int64_t const[4]){
		[0] = 1,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[31] = (int64_t const[4]){
		[0] = 4,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[32] = (int64_t const[4]){
		[0] = 3,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[33] = (int64_t const[4]){
		[0] = 2,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[34] = (int64_t const[4]){
		[0] = 4,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[35] = (int64_t const[4]){
		[0] = 3,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[36] = (int64_t const[4]){
		[0] = 3,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[37] = (int64_t const[4]){
		[0] = 2,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[38] = (int64_t const[4]){
		[0] = 4,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[39] = (int64_t const[4]){
		[0] = 3,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[40] = (int64_t const[4]){
		[0] = 3,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[41] = (int64_t const[4]){
		[0] = 2,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[42] = (int64_t const[4]){
		[0] = 4,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[43] = (int64_t const[4]){
		[0] = 3,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[44] = (int64_t const[4]){
		[0] = 3,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[45] = (int64_t const[4]){
		[0] = 2,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[46] = (int64_t const[4]){
		[0] = 4,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[47] = (int64_t const[4]){
		[0] = 3,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[48] = (int64_t const[4]){
		[0] = 2,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[49] = (int64_t const[4]){
		[0] = 4,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[50] = (int64_t const[4]){
		[0] = 3,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[51] = (int64_t const[4]){
		[0] = 4,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[52] = (int64_t const[4]){
		[0] = 4,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[53] = (int64_t const[4]){
		[0] = 3,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[54] = (int64_t const[4]){
		[0] = 4,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[55] = (int64_t const[4]){
		[0] = 4,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[56] = (int64_t const[4]){
		[0] = 3,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[57] = (int64_t const[4]){
		[0] = 4,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[58] = (int64_t const[4]){
		[0] = 4,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[59] = (int64_t const[4]){
		[0] = 3,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[60] = (int64_t const[4]){
		[0] = 4,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[61] = (int64_t const[4]){
		[0] = 3,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[62] = (int64_t const[4]){
		[0] = 1,
		[1] = 1,
		[2] = 1,
		[3] = 1,
	},
	[63] = (int64_t const[4]){
		[0] = 1,
		[1] = 1,
		[2] = 1,
		[3] = 1,
	},
	[64] = (int64_t const[4]){
		[0] = 1,
		[1] = 1,
		[2] = 1,
		[3] = 1,
	},
	[65] = (int64_t const[4]){
		[0] = 1,
		[1] = 1,
		[2] = 1,
		[3] = 1,
	},
	[66] = (int64_t const[4]){
		[0] = 1,
		[1] = 1,
		[2] = 1,
		[3] = 1,
	},
	[67] = (int64_t const[4]){
		[0] = 2,
		[1] = 2,
		[2] = 2,
		[3] = 2,
	},
	[68] = (int64_t const[4]){
		[0] = 2,
		[1] = 2,
		[2] = 2,
		[3] = 2,
	},
	[69] = (int64_t const[4]){
		[0] = 2,
		[1] = 2,
		[2] = 2,
		[3] = 2,
	},
	[70] = (int64_t const[4]){
		[0] = 2,
		[1] = 2,
		[2] = 2,
		[3] = 2,
	},
	[71] = (int64_t const[4]){
		[0] = 2,
		[1] = 2,
		[2] = 2,
		[3] = 2,
	},
	[72] = (int64_t const[4]){
		[0] = 3,
		[1] = 3,
		[2] = 3,
		[3] = 3,
	},
	[73] = (int64_t const[4]){
		[0] = 3,
		[1] = 3,
		[2] = 3,
		[3] = 3,
	},
	[74] = (int64_t const[4]){
		[0] = 3,
		[1] = 3,
		[2] = 3,
		[3] = 3,
	},
	[75] = (int64_t const[4]){
		[0] = 3,
		[1] = 3,
		[2] = 3,
		[3] = 3,
	},
	[76] = (int64_t const[4]){
		[0] = 3,
		[1] = 3,
		[2] = 3,
		[3] = 3,
	},
	[77] = (int64_t const[4]){
		[0] = 4,
		[1] = 4,
		[2] = 4,
		[3] = 4,
	},
	[78] = (int64_t const[4]){
		[0] = 4,
		[1] = 4,
		[2] = 4,
		[3] = 4,
	},
	[79] = (int64_t const[4]){
		[0] = 4,
		[1] = 4,
		[2] = 4,
		[3] = 4,
	},
	[80] = (int64_t const[4]){
		[0] = 4,
		[1] = 4,
		[2] = 4,
		[3] = 4,
	},
	[81] = (int64_t const[4]){
		[0] = 4,
		[1] = 4,
		[2] = 4,
		[3] = 4,
	},
};

// The number of each input port, or -1 if not this number is not available
int64_t const*const orwl_taskdep_inum[82] = {
	[0] = (int64_t const[4]){
		[0] = 1,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[1] = (int64_t const[4]){
		[0] = 1,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[2] = (int64_t const[4]){
		[0] = 2,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[3] = (int64_t const[4]){
		[0] = 2,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[4] = (int64_t const[4]){
		[0] = 2,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[5] = (int64_t const[4]){
		[0] = 3,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[6] = (int64_t const[4]){
		[0] = 3,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[7] = (int64_t const[4]){
		[0] = 3,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[8] = (int64_t const[4]){
		[0] = 4,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[9] = (int64_t const[4]){
		[0] = 4,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[10] = (int64_t const[4]){
		[0] = 4,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[11] = (int64_t const[4]){
		[0] = 5,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[12] = (int64_t const[4]){
		[0] = 5,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[13] = (int64_t const[4]){
		[0] = 1,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[14] = (int64_t const[4]){
		[0] = 1,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[15] = (int64_t const[4]){
		[0] = 1,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[16] = (int64_t const[4]){
		[0] = 2,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[17] = (int64_t const[4]){
		[0] = 2,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[18] = (int64_t const[4]){
		[0] = 2,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[19] = (int64_t const[4]){
		[0] = 2,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[20] = (int64_t const[4]){
		[0] = 3,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[21] = (int64_t const[4]){
		[0] = 3,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[22] = (int64_t const[4]){
		[0] = 3,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[23] = (int64_t const[4]){
		[0] = 3,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[24] = (int64_t const[4]){
		[0] = 4,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[25] = (int64_t const[4]){
		[0] = 4,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[26] = (int64_t const[4]){
		[0] = 4,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[27] = (int64_t const[4]){
		[0] = 4,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[28] = (int64_t const[4]){
		[0] = 5,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[29] = (int64_t const[4]){
		[0] = 5,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[30] = (int64_t const[4]){
		[0] = 5,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[31] = (int64_t const[4]){
		[0] = 1,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[32] = (int64_t const[4]){
		[0] = 1,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[33] = (int64_t const[4]){
		[0] = 1,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[34] = (int64_t const[4]){
		[0] = 2,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[35] = (int64_t const[4]){
		[0] = 2,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[36] = (int64_t const[4]){
		[0] = 2,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[37] = (int64_t const[4]){
		[0] = 2,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[38] = (int64_t const[4]){
		[0] = 3,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[39] = (int64_t const[4]){
		[0] = 3,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[40] = (int64_t const[4]){
		[0] = 3,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[41] = (int64_t const[4]){
		[0] = 3,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[42] = (int64_t const[4]){
		[0] = 4,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[43] = (int64_t const[4]){
		[0] = 4,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[44] = (int64_t const[4]){
		[0] = 4,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[45] = (int64_t const[4]){
		[0] = 4,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[46] = (int64_t const[4]){
		[0] = 5,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[47] = (int64_t const[4]){
		[0] = 5,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[48] = (int64_t const[4]){
		[0] = 5,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[49] = (int64_t const[4]){
		[0] = 1,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[50] = (int64_t const[4]){
		[0] = 1,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[51] = (int64_t const[4]){
		[0] = 2,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[52] = (int64_t const[4]){
		[0] = 2,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[53] = (int64_t const[4]){
		[0] = 2,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[54] = (int64_t const[4]){
		[0] = 3,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[55] = (int64_t const[4]){
		[0] = 3,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[56] = (int64_t const[4]){
		[0] = 3,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[57] = (int64_t const[4]){
		[0] = 4,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[58] = (int64_t const[4]){
		[0] = 4,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[59] = (int64_t const[4]){
		[0] = 4,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[60] = (int64_t const[4]){
		[0] = 5,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[61] = (int64_t const[4]){
		[0] = 5,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[62] = (int64_t const[4]){
		[0] = 1,
		[1] = 1,
		[2] = 1,
		[3] = 1,
	},
	[63] = (int64_t const[4]){
		[0] = 2,
		[1] = 2,
		[2] = 2,
		[3] = 2,
	},
	[64] = (int64_t const[4]){
		[0] = 3,
		[1] = 3,
		[2] = 3,
		[3] = 3,
	},
	[65] = (int64_t const[4]){
		[0] = 4,
		[1] = 4,
		[2] = 4,
		[3] = 4,
	},
	[66] = (int64_t const[4]){
		[0] = 5,
		[1] = 5,
		[2] = 5,
		[3] = 5,
	},
	[67] = (int64_t const[4]){
		[0] = 1,
		[1] = 1,
		[2] = 1,
		[3] = 1,
	},
	[68] = (int64_t const[4]){
		[0] = 2,
		[1] = 2,
		[2] = 2,
		[3] = 2,
	},
	[69] = (int64_t const[4]){
		[0] = 3,
		[1] = 3,
		[2] = 3,
		[3] = 3,
	},
	[70] = (int64_t const[4]){
		[0] = 4,
		[1] = 4,
		[2] = 4,
		[3] = 4,
	},
	[71] = (int64_t const[4]){
		[0] = 5,
		[1] = 5,
		[2] = 5,
		[3] = 5,
	},
	[72] = (int64_t const[4]){
		[0] = 1,
		[1] = 1,
		[2] = 1,
		[3] = 1,
	},
	[73] = (int64_t const[4]){
		[0] = 2,
		[1] = 2,
		[2] = 2,
		[3] = 2,
	},
	[74] = (int64_t const[4]){
		[0] = 3,
		[1] = 3,
		[2] = 3,
		[3] = 3,
	},
	[75] = (int64_t const[4]){
		[0] = 4,
		[1] = 4,
		[2] = 4,
		[3] = 4,
	},
	[76] = (int64_t const[4]){
		[0] = 5,
		[1] = 5,
		[2] = 5,
		[3] = 5,
	},
	[77] = (int64_t const[4]){
		[0] = 1,
		[1] = 1,
		[2] = 1,
		[3] = 1,
	},
	[78] = (int64_t const[4]){
		[0] = 2,
		[1] = 2,
		[2] = 2,
		[3] = 2,
	},
	[79] = (int64_t const[4]){
		[0] = 3,
		[1] = 3,
		[2] = 3,
		[3] = 3,
	},
	[80] = (int64_t const[4]){
		[0] = 4,
		[1] = 4,
		[2] = 4,
		[3] = 4,
	},
	[81] = (int64_t const[4]){
		[0] = 5,
		[1] = 5,
		[2] = 5,
		[3] = 5,
	},
};

// For each input port, provide the source port
static int64_t const orwl_taskdep_tail[82][4] = {
	[0] = {
		[0] = 248,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[1] = {
		[0] = 251,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[2] = {
		[0] = 252,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[3] = {
		[0] = 253,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[4] = {
		[0] = 255,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[5] = {
		[0] = 256,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[6] = {
		[0] = 257,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[7] = {
		[0] = 259,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[8] = {
		[0] = 260,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[9] = {
		[0] = 261,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[10] = {
		[0] = 263,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[11] = {
		[0] = 264,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[12] = {
		[0] = 265,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[13] = {
		[0] = 268,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[14] = {
		[0] = 271,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[15] = {
		[0] = 270,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[16] = {
		[0] = 272,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[17] = {
		[0] = 273,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[18] = {
		[0] = 275,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[19] = {
		[0] = 274,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[20] = {
		[0] = 276,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[21] = {
		[0] = 277,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[22] = {
		[0] = 279,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[23] = {
		[0] = 278,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[24] = {
		[0] = 280,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[25] = {
		[0] = 281,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[26] = {
		[0] = 283,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[27] = {
		[0] = 282,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[28] = {
		[0] = 284,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[29] = {
		[0] = 285,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[30] = {
		[0] = 286,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[31] = {
		[0] = 288,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[32] = {
		[0] = 291,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[33] = {
		[0] = 290,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[34] = {
		[0] = 292,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[35] = {
		[0] = 293,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[36] = {
		[0] = 295,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[37] = {
		[0] = 294,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[38] = {
		[0] = 296,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[39] = {
		[0] = 297,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[40] = {
		[0] = 299,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[41] = {
		[0] = 298,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[42] = {
		[0] = 300,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[43] = {
		[0] = 301,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[44] = {
		[0] = 303,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[45] = {
		[0] = 302,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[46] = {
		[0] = 304,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[47] = {
		[0] = 305,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[48] = {
		[0] = 306,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[49] = {
		[0] = 311,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[50] = {
		[0] = 310,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[51] = {
		[0] = 313,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[52] = {
		[0] = 315,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[53] = {
		[0] = 314,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[54] = {
		[0] = 317,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[55] = {
		[0] = 319,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[56] = {
		[0] = 318,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[57] = {
		[0] = 321,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[58] = {
		[0] = 323,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[59] = {
		[0] = 322,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[60] = {
		[0] = 325,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[61] = {
		[0] = 326,
		[1] = -1,
		[2] = -1,
		[3] = -1,
	},
	[62] = {
		[0] = 60,
		[1] = -1,
		[2] = -1,
		[3] = 12,
	},
	[63] = {
		[0] = 76,
		[1] = 4,
		[2] = -1,
		[3] = 24,
	},
	[64] = {
		[0] = 92,
		[1] = 16,
		[2] = -1,
		[3] = 36,
	},
	[65] = {
		[0] = 108,
		[1] = 28,
		[2] = -1,
		[3] = 48,
	},
	[66] = {
		[0] = 120,
		[1] = 40,
		[2] = -1,
		[3] = -1,
	},
	[67] = {
		[0] = 132,
		[1] = -1,
		[2] = 0,
		[3] = 68,
	},
	[68] = {
		[0] = 148,
		[1] = 56,
		[2] = 8,
		[3] = 84,
	},
	[69] = {
		[0] = 164,
		[1] = 72,
		[2] = 20,
		[3] = 100,
	},
	[70] = {
		[0] = 180,
		[1] = 88,
		[2] = 32,
		[3] = 116,
	},
	[71] = {
		[0] = 192,
		[1] = 104,
		[2] = 44,
		[3] = -1,
	},
	[72] = {
		[0] = 200,
		[1] = -1,
		[2] = 52,
		[3] = 140,
	},
	[73] = {
		[0] = 212,
		[1] = 128,
		[2] = 64,
		[3] = 156,
	},
	[74] = {
		[0] = 224,
		[1] = 144,
		[2] = 80,
		[3] = 172,
	},
	[75] = {
		[0] = 236,
		[1] = 160,
		[2] = 96,
		[3] = 188,
	},
	[76] = {
		[0] = 244,
		[1] = 176,
		[2] = 112,
		[3] = -1,
	},
	[77] = {
		[0] = -1,
		[1] = -1,
		[2] = 124,
		[3] = 204,
	},
	[78] = {
		[0] = -1,
		[1] = 196,
		[2] = 136,
		[3] = 216,
	},
	[79] = {
		[0] = -1,
		[1] = 208,
		[2] = 152,
		[3] = 228,
	},
	[80] = {
		[0] = -1,
		[1] = 220,
		[2] = 168,
		[3] = 240,
	},
	[81] = {
		[0] = -1,
		[1] = 232,
		[2] = 184,
		[3] = -1,
	},
};

void orwl_taskdep_usage(void) {
    printf("task number\tname\tfunction\tstartup\tshutdown\t(in ports)\t(out ports)\n");
    for (size_t i = 0; i < 82; ++i) {
        printf("%zu\t\t%s\t%s\t%s\t%s", i, orwl_taskdep_nnames[i], orwl_taskdep_fnames[i], orwl_taskdep_snames[i], orwl_taskdep_dnames[i]);
        if (orwl_taskdep_iports[i][0]) {
            printf("\t(%s", orwl_taskdep_iports[i][0]);
            for (size_t j = 1; j < 4; ++j) {
                if (orwl_taskdep_iports[i][j])
                   printf(", %s", orwl_taskdep_iports[i][j]);
            }
            printf(")");
        } else {
            printf("	()");
        }
        if (orwl_taskdep_oports[i][0]) {
            printf("\t(%s", orwl_taskdep_oports[i][0]);
            for (size_t j = 1; j < 4; ++j) {
                if (orwl_taskdep_oports[i][j])
                   printf(", %s", orwl_taskdep_oports[i][j]);
            }
            printf(")");
        } else {
            printf("	()");
        }
        printf("\n");
    }

}

P99_GETOPT_CALLBACK(orwl_taskdep_usage);

// *****************************************************
// Instantiate the ORWL structure.
ORWL_LOCATIONS_PER_TASK(
	orwl_task_write_if0,
	orwl_task_read_if0,
	orwl_task_write_if1,
	orwl_task_read_if1,
	orwl_task_write_if2,
	orwl_task_read_if2,
	orwl_task_write_if3,
	orwl_task_read_if3
);

ORWL_LOCATIONS_PER_TASK_INSTANTIATION();

ORWL_DECLARE_TASK(orwl_taskdep_type);

// *****************************************************
// Set up the ORWL structure that reflects dependencies.

// This graph is iterated.
// Using orwl_handle2.
// buf_1_1_b	→ node_2_1	(dist_2_1_t)
// 0	→ 67
// priorities: 0 1
// buf_1_1_r	→ node_1_2	(dist_1_2_l)
// 1	→ 63
// priorities: 0 1
// buf_1_2_b	→ node_2_2	(dist_2_2_t)
// 2	→ 68
// priorities: 0 1
// buf_1_2_l	→ node_1_1	(dist_1_1_r)
// 3	→ 62
// priorities: 1 0
// buf_1_2_r	→ node_1_3	(dist_1_3_l)
// 4	→ 64
// priorities: 0 1
// buf_1_3_b	→ node_2_3	(dist_2_3_t)
// 5	→ 69
// priorities: 0 1
// buf_1_3_l	→ node_1_2	(dist_1_2_r)
// 6	→ 63
// priorities: 1 0
// buf_1_3_r	→ node_1_4	(dist_1_4_l)
// 7	→ 65
// priorities: 0 1
// buf_1_4_b	→ node_2_4	(dist_2_4_t)
// 8	→ 70
// priorities: 0 1
// buf_1_4_l	→ node_1_3	(dist_1_3_r)
// 9	→ 64
// priorities: 1 0
// buf_1_4_r	→ node_1_5	(dist_1_5_l)
// 10	→ 66
// priorities: 0 1
// buf_1_5_b	→ node_2_5	(dist_2_5_t)
// 11	→ 71
// priorities: 0 1
// buf_1_5_l	→ node_1_4	(dist_1_4_r)
// 12	→ 65
// priorities: 1 0
// buf_2_1_b	→ node_3_1	(dist_3_1_t)
// 13	→ 72
// priorities: 0 1
// buf_2_1_r	→ node_2_2	(dist_2_2_l)
// 14	→ 68
// priorities: 0 1
// buf_2_1_t	→ node_1_1	(dist_1_1_b)
// 15	→ 62
// priorities: 1 0
// buf_2_2_b	→ node_3_2	(dist_3_2_t)
// 16	→ 73
// priorities: 0 1
// buf_2_2_l	→ node_2_1	(dist_2_1_r)
// 17	→ 67
// priorities: 1 0
// buf_2_2_r	→ node_2_3	(dist_2_3_l)
// 18	→ 69
// priorities: 0 1
// buf_2_2_t	→ node_1_2	(dist_1_2_b)
// 19	→ 63
// priorities: 1 0
// buf_2_3_b	→ node_3_3	(dist_3_3_t)
// 20	→ 74
// priorities: 0 1
// buf_2_3_l	→ node_2_2	(dist_2_2_r)
// 21	→ 68
// priorities: 1 0
// buf_2_3_r	→ node_2_4	(dist_2_4_l)
// 22	→ 70
// priorities: 0 1
// buf_2_3_t	→ node_1_3	(dist_1_3_b)
// 23	→ 64
// priorities: 1 0
// buf_2_4_b	→ node_3_4	(dist_3_4_t)
// 24	→ 75
// priorities: 0 1
// buf_2_4_l	→ node_2_3	(dist_2_3_r)
// 25	→ 69
// priorities: 1 0
// buf_2_4_r	→ node_2_5	(dist_2_5_l)
// 26	→ 71
// priorities: 0 1
// buf_2_4_t	→ node_1_4	(dist_1_4_b)
// 27	→ 65
// priorities: 1 0
// buf_2_5_b	→ node_3_5	(dist_3_5_t)
// 28	→ 76
// priorities: 0 1
// buf_2_5_l	→ node_2_4	(dist_2_4_r)
// 29	→ 70
// priorities: 1 0
// buf_2_5_t	→ node_1_5	(dist_1_5_b)
// 30	→ 66
// priorities: 1 0
// buf_3_1_b	→ node_4_1	(dist_4_1_t)
// 31	→ 77
// priorities: 0 1
// buf_3_1_r	→ node_3_2	(dist_3_2_l)
// 32	→ 73
// priorities: 0 1
// buf_3_1_t	→ node_2_1	(dist_2_1_b)
// 33	→ 67
// priorities: 1 0
// buf_3_2_b	→ node_4_2	(dist_4_2_t)
// 34	→ 78
// priorities: 0 1
// buf_3_2_l	→ node_3_1	(dist_3_1_r)
// 35	→ 72
// priorities: 1 0
// buf_3_2_r	→ node_3_3	(dist_3_3_l)
// 36	→ 74
// priorities: 0 1
// buf_3_2_t	→ node_2_2	(dist_2_2_b)
// 37	→ 68
// priorities: 1 0
// buf_3_3_b	→ node_4_3	(dist_4_3_t)
// 38	→ 79
// priorities: 0 1
// buf_3_3_l	→ node_3_2	(dist_3_2_r)
// 39	→ 73
// priorities: 1 0
// buf_3_3_r	→ node_3_4	(dist_3_4_l)
// 40	→ 75
// priorities: 0 1
// buf_3_3_t	→ node_2_3	(dist_2_3_b)
// 41	→ 69
// priorities: 1 0
// buf_3_4_b	→ node_4_4	(dist_4_4_t)
// 42	→ 80
// priorities: 0 1
// buf_3_4_l	→ node_3_3	(dist_3_3_r)
// 43	→ 74
// priorities: 1 0
// buf_3_4_r	→ node_3_5	(dist_3_5_l)
// 44	→ 76
// priorities: 0 1
// buf_3_4_t	→ node_2_4	(dist_2_4_b)
// 45	→ 70
// priorities: 1 0
// buf_3_5_b	→ node_4_5	(dist_4_5_t)
// 46	→ 81
// priorities: 0 1
// buf_3_5_l	→ node_3_4	(dist_3_4_r)
// 47	→ 75
// priorities: 1 0
// buf_3_5_t	→ node_2_5	(dist_2_5_b)
// 48	→ 71
// priorities: 1 0
// buf_4_1_r	→ node_4_2	(dist_4_2_l)
// 49	→ 78
// priorities: 0 1
// buf_4_1_t	→ node_3_1	(dist_3_1_b)
// 50	→ 72
// priorities: 1 0
// buf_4_2_l	→ node_4_1	(dist_4_1_r)
// 51	→ 77
// priorities: 1 0
// buf_4_2_r	→ node_4_3	(dist_4_3_l)
// 52	→ 79
// priorities: 0 1
// buf_4_2_t	→ node_3_2	(dist_3_2_b)
// 53	→ 73
// priorities: 1 0
// buf_4_3_l	→ node_4_2	(dist_4_2_r)
// 54	→ 78
// priorities: 1 0
// buf_4_3_r	→ node_4_4	(dist_4_4_l)
// 55	→ 80
// priorities: 0 1
// buf_4_3_t	→ node_3_3	(dist_3_3_b)
// 56	→ 74
// priorities: 1 0
// buf_4_4_l	→ node_4_3	(dist_4_3_r)
// 57	→ 79
// priorities: 1 0
// buf_4_4_r	→ node_4_5	(dist_4_5_l)
// 58	→ 81
// priorities: 0 1
// buf_4_4_t	→ node_3_4	(dist_3_4_b)
// 59	→ 75
// priorities: 1 0
// buf_4_5_l	→ node_4_4	(dist_4_4_r)
// 60	→ 80
// priorities: 1 0
// buf_4_5_t	→ node_3_5	(dist_3_5_b)
// 61	→ 76
// priorities: 1 0
// node_1_1	→ buf_1_1_b	(out_1_1_b)
// 62	→ 0
// priorities: 0 1
// node_1_1	→ buf_1_1_r	(out_1_1_r)
// 62	→ 1
// priorities: 0 1
// node_1_2	→ buf_1_2_b	(out_1_2_b)
// 63	→ 2
// priorities: 0 1
// node_1_2	→ buf_1_2_l	(out_1_2_l)
// 63	→ 3
// priorities: 0 1
// node_1_2	→ buf_1_2_r	(out_1_2_r)
// 63	→ 4
// priorities: 0 1
// node_1_3	→ buf_1_3_b	(out_1_3_b)
// 64	→ 5
// priorities: 0 1
// node_1_3	→ buf_1_3_l	(out_1_3_l)
// 64	→ 6
// priorities: 0 1
// node_1_3	→ buf_1_3_r	(out_1_3_r)
// 64	→ 7
// priorities: 0 1
// node_1_4	→ buf_1_4_b	(out_1_4_b)
// 65	→ 8
// priorities: 0 1
// node_1_4	→ buf_1_4_l	(out_1_4_l)
// 65	→ 9
// priorities: 0 1
// node_1_4	→ buf_1_4_r	(out_1_4_r)
// 65	→ 10
// priorities: 0 1
// node_1_5	→ buf_1_5_b	(out_1_5_b)
// 66	→ 11
// priorities: 0 1
// node_1_5	→ buf_1_5_l	(out_1_5_l)
// 66	→ 12
// priorities: 0 1
// node_2_1	→ buf_2_1_b	(out_2_1_b)
// 67	→ 13
// priorities: 0 1
// node_2_1	→ buf_2_1_r	(out_2_1_r)
// 67	→ 14
// priorities: 0 1
// node_2_1	→ buf_2_1_t	(out_2_1_t)
// 67	→ 15
// priorities: 0 1
// node_2_2	→ buf_2_2_b	(out_2_2_b)
// 68	→ 16
// priorities: 0 1
// node_2_2	→ buf_2_2_l	(out_2_2_l)
// 68	→ 17
// priorities: 0 1
// node_2_2	→ buf_2_2_r	(out_2_2_r)
// 68	→ 18
// priorities: 0 1
// node_2_2	→ buf_2_2_t	(out_2_2_t)
// 68	→ 19
// priorities: 0 1
// node_2_3	→ buf_2_3_b	(out_2_3_b)
// 69	→ 20
// priorities: 0 1
// node_2_3	→ buf_2_3_l	(out_2_3_l)
// 69	→ 21
// priorities: 0 1
// node_2_3	→ buf_2_3_r	(out_2_3_r)
// 69	→ 22
// priorities: 0 1
// node_2_3	→ buf_2_3_t	(out_2_3_t)
// 69	→ 23
// priorities: 0 1
// node_2_4	→ buf_2_4_b	(out_2_4_b)
// 70	→ 24
// priorities: 0 1
// node_2_4	→ buf_2_4_l	(out_2_4_l)
// 70	→ 25
// priorities: 0 1
// node_2_4	→ buf_2_4_r	(out_2_4_r)
// 70	→ 26
// priorities: 0 1
// node_2_4	→ buf_2_4_t	(out_2_4_t)
// 70	→ 27
// priorities: 0 1
// node_2_5	→ buf_2_5_b	(out_2_5_b)
// 71	→ 28
// priorities: 0 1
// node_2_5	→ buf_2_5_l	(out_2_5_l)
// 71	→ 29
// priorities: 0 1
// node_2_5	→ buf_2_5_t	(out_2_5_t)
// 71	→ 30
// priorities: 0 1
// node_3_1	→ buf_3_1_b	(out_3_1_b)
// 72	→ 31
// priorities: 0 1
// node_3_1	→ buf_3_1_r	(out_3_1_r)
// 72	→ 32
// priorities: 0 1
// node_3_1	→ buf_3_1_t	(out_3_1_t)
// 72	→ 33
// priorities: 0 1
// node_3_2	→ buf_3_2_b	(out_3_2_b)
// 73	→ 34
// priorities: 0 1
// node_3_2	→ buf_3_2_l	(out_3_2_l)
// 73	→ 35
// priorities: 0 1
// node_3_2	→ buf_3_2_r	(out_3_2_r)
// 73	→ 36
// priorities: 0 1
// node_3_2	→ buf_3_2_t	(out_3_2_t)
// 73	→ 37
// priorities: 0 1
// node_3_3	→ buf_3_3_b	(out_3_3_b)
// 74	→ 38
// priorities: 0 1
// node_3_3	→ buf_3_3_l	(out_3_3_l)
// 74	→ 39
// priorities: 0 1
// node_3_3	→ buf_3_3_r	(out_3_3_r)
// 74	→ 40
// priorities: 0 1
// node_3_3	→ buf_3_3_t	(out_3_3_t)
// 74	→ 41
// priorities: 0 1
// node_3_4	→ buf_3_4_b	(out_3_4_b)
// 75	→ 42
// priorities: 0 1
// node_3_4	→ buf_3_4_l	(out_3_4_l)
// 75	→ 43
// priorities: 0 1
// node_3_4	→ buf_3_4_r	(out_3_4_r)
// 75	→ 44
// priorities: 0 1
// node_3_4	→ buf_3_4_t	(out_3_4_t)
// 75	→ 45
// priorities: 0 1
// node_3_5	→ buf_3_5_b	(out_3_5_b)
// 76	→ 46
// priorities: 0 1
// node_3_5	→ buf_3_5_l	(out_3_5_l)
// 76	→ 47
// priorities: 0 1
// node_3_5	→ buf_3_5_t	(out_3_5_t)
// 76	→ 48
// priorities: 0 1
// node_4_1	→ buf_4_1_r	(out_4_1_r)
// 77	→ 49
// priorities: 0 1
// node_4_1	→ buf_4_1_t	(out_4_1_t)
// 77	→ 50
// priorities: 0 1
// node_4_2	→ buf_4_2_l	(out_4_2_l)
// 78	→ 51
// priorities: 0 1
// node_4_2	→ buf_4_2_r	(out_4_2_r)
// 78	→ 52
// priorities: 0 1
// node_4_2	→ buf_4_2_t	(out_4_2_t)
// 78	→ 53
// priorities: 0 1
// node_4_3	→ buf_4_3_l	(out_4_3_l)
// 79	→ 54
// priorities: 0 1
// node_4_3	→ buf_4_3_r	(out_4_3_r)
// 79	→ 55
// priorities: 0 1
// node_4_3	→ buf_4_3_t	(out_4_3_t)
// 79	→ 56
// priorities: 0 1
// node_4_4	→ buf_4_4_l	(out_4_4_l)
// 80	→ 57
// priorities: 0 1
// node_4_4	→ buf_4_4_r	(out_4_4_r)
// 80	→ 58
// priorities: 0 1
// node_4_4	→ buf_4_4_t	(out_4_4_t)
// 80	→ 59
// priorities: 0 1
// node_4_5	→ buf_4_5_l	(out_4_5_l)
// 81	→ 60
// priorities: 0 1
// node_4_5	→ buf_4_5_t	(out_4_5_t)
// 81	→ 61
// priorities: 0 1

// ****************************************************************
// Per task specific initialization functions. They should ease some
// const propagation concerning the task ID.
static void orwl_taskdep_init_0(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "0, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(0, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(0, orwl_task_write_if0), ORWL_LOCATION(0, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(62, orwl_task_read_if0), 1, seed, server);

	report(orwl_taskdep_verbose, "0, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "0, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_1(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "1, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(1, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(1, orwl_task_write_if0), ORWL_LOCATION(1, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(62, orwl_task_read_if3), 1, seed, server);

	report(orwl_taskdep_verbose, "1, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "1, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_2(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "2, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(2, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(2, orwl_task_write_if0), ORWL_LOCATION(2, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(63, orwl_task_read_if0), 1, seed, server);

	report(orwl_taskdep_verbose, "2, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "2, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_3(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "3, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(3, orwl_task_write_if0), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(3, orwl_task_write_if0), ORWL_LOCATION(3, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(63, orwl_task_read_if1), 1, seed, server);

	report(orwl_taskdep_verbose, "3, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "3, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_4(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "4, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(4, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(4, orwl_task_write_if0), ORWL_LOCATION(4, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(63, orwl_task_read_if3), 1, seed, server);

	report(orwl_taskdep_verbose, "4, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "4, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_5(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "5, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(5, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(5, orwl_task_write_if0), ORWL_LOCATION(5, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(64, orwl_task_read_if0), 1, seed, server);

	report(orwl_taskdep_verbose, "5, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "5, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_6(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "6, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(6, orwl_task_write_if0), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(6, orwl_task_write_if0), ORWL_LOCATION(6, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(64, orwl_task_read_if1), 1, seed, server);

	report(orwl_taskdep_verbose, "6, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "6, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_7(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "7, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(7, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(7, orwl_task_write_if0), ORWL_LOCATION(7, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(64, orwl_task_read_if3), 1, seed, server);

	report(orwl_taskdep_verbose, "7, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "7, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_8(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "8, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(8, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(8, orwl_task_write_if0), ORWL_LOCATION(8, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(65, orwl_task_read_if0), 1, seed, server);

	report(orwl_taskdep_verbose, "8, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "8, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_9(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "9, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(9, orwl_task_write_if0), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(9, orwl_task_write_if0), ORWL_LOCATION(9, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(65, orwl_task_read_if1), 1, seed, server);

	report(orwl_taskdep_verbose, "9, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "9, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_10(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "10, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(10, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(10, orwl_task_write_if0), ORWL_LOCATION(10, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(65, orwl_task_read_if3), 1, seed, server);

	report(orwl_taskdep_verbose, "10, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "10, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_11(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "11, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(11, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(11, orwl_task_write_if0), ORWL_LOCATION(11, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(66, orwl_task_read_if0), 1, seed, server);

	report(orwl_taskdep_verbose, "11, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "11, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_12(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "12, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(12, orwl_task_write_if0), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(12, orwl_task_write_if0), ORWL_LOCATION(12, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(66, orwl_task_read_if1), 1, seed, server);

	report(orwl_taskdep_verbose, "12, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "12, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_13(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "13, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(13, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(13, orwl_task_write_if0), ORWL_LOCATION(13, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(67, orwl_task_read_if0), 1, seed, server);

	report(orwl_taskdep_verbose, "13, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "13, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_14(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "14, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(14, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(14, orwl_task_write_if0), ORWL_LOCATION(14, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(67, orwl_task_read_if3), 1, seed, server);

	report(orwl_taskdep_verbose, "14, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "14, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_15(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "15, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(15, orwl_task_write_if0), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(15, orwl_task_write_if0), ORWL_LOCATION(15, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(67, orwl_task_read_if2), 1, seed, server);

	report(orwl_taskdep_verbose, "15, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "15, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_16(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "16, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(16, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(16, orwl_task_write_if0), ORWL_LOCATION(16, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(68, orwl_task_read_if0), 1, seed, server);

	report(orwl_taskdep_verbose, "16, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "16, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_17(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "17, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(17, orwl_task_write_if0), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(17, orwl_task_write_if0), ORWL_LOCATION(17, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(68, orwl_task_read_if1), 1, seed, server);

	report(orwl_taskdep_verbose, "17, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "17, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_18(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "18, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(18, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(18, orwl_task_write_if0), ORWL_LOCATION(18, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(68, orwl_task_read_if3), 1, seed, server);

	report(orwl_taskdep_verbose, "18, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "18, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_19(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "19, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(19, orwl_task_write_if0), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(19, orwl_task_write_if0), ORWL_LOCATION(19, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(68, orwl_task_read_if2), 1, seed, server);

	report(orwl_taskdep_verbose, "19, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "19, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_20(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "20, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(20, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(20, orwl_task_write_if0), ORWL_LOCATION(20, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(69, orwl_task_read_if0), 1, seed, server);

	report(orwl_taskdep_verbose, "20, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "20, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_21(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "21, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(21, orwl_task_write_if0), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(21, orwl_task_write_if0), ORWL_LOCATION(21, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(69, orwl_task_read_if1), 1, seed, server);

	report(orwl_taskdep_verbose, "21, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "21, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_22(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "22, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(22, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(22, orwl_task_write_if0), ORWL_LOCATION(22, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(69, orwl_task_read_if3), 1, seed, server);

	report(orwl_taskdep_verbose, "22, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "22, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_23(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "23, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(23, orwl_task_write_if0), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(23, orwl_task_write_if0), ORWL_LOCATION(23, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(69, orwl_task_read_if2), 1, seed, server);

	report(orwl_taskdep_verbose, "23, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "23, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_24(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "24, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(24, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(24, orwl_task_write_if0), ORWL_LOCATION(24, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(70, orwl_task_read_if0), 1, seed, server);

	report(orwl_taskdep_verbose, "24, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "24, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_25(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "25, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(25, orwl_task_write_if0), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(25, orwl_task_write_if0), ORWL_LOCATION(25, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(70, orwl_task_read_if1), 1, seed, server);

	report(orwl_taskdep_verbose, "25, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "25, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_26(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "26, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(26, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(26, orwl_task_write_if0), ORWL_LOCATION(26, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(70, orwl_task_read_if3), 1, seed, server);

	report(orwl_taskdep_verbose, "26, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "26, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_27(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "27, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(27, orwl_task_write_if0), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(27, orwl_task_write_if0), ORWL_LOCATION(27, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(70, orwl_task_read_if2), 1, seed, server);

	report(orwl_taskdep_verbose, "27, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "27, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_28(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "28, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(28, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(28, orwl_task_write_if0), ORWL_LOCATION(28, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(71, orwl_task_read_if0), 1, seed, server);

	report(orwl_taskdep_verbose, "28, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "28, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_29(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "29, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(29, orwl_task_write_if0), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(29, orwl_task_write_if0), ORWL_LOCATION(29, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(71, orwl_task_read_if1), 1, seed, server);

	report(orwl_taskdep_verbose, "29, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "29, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_30(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "30, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(30, orwl_task_write_if0), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(30, orwl_task_write_if0), ORWL_LOCATION(30, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(71, orwl_task_read_if2), 1, seed, server);

	report(orwl_taskdep_verbose, "30, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "30, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_31(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "31, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(31, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(31, orwl_task_write_if0), ORWL_LOCATION(31, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(72, orwl_task_read_if0), 1, seed, server);

	report(orwl_taskdep_verbose, "31, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "31, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_32(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "32, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(32, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(32, orwl_task_write_if0), ORWL_LOCATION(32, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(72, orwl_task_read_if3), 1, seed, server);

	report(orwl_taskdep_verbose, "32, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "32, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_33(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "33, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(33, orwl_task_write_if0), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(33, orwl_task_write_if0), ORWL_LOCATION(33, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(72, orwl_task_read_if2), 1, seed, server);

	report(orwl_taskdep_verbose, "33, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "33, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_34(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "34, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(34, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(34, orwl_task_write_if0), ORWL_LOCATION(34, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(73, orwl_task_read_if0), 1, seed, server);

	report(orwl_taskdep_verbose, "34, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "34, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_35(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "35, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(35, orwl_task_write_if0), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(35, orwl_task_write_if0), ORWL_LOCATION(35, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(73, orwl_task_read_if1), 1, seed, server);

	report(orwl_taskdep_verbose, "35, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "35, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_36(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "36, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(36, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(36, orwl_task_write_if0), ORWL_LOCATION(36, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(73, orwl_task_read_if3), 1, seed, server);

	report(orwl_taskdep_verbose, "36, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "36, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_37(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "37, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(37, orwl_task_write_if0), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(37, orwl_task_write_if0), ORWL_LOCATION(37, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(73, orwl_task_read_if2), 1, seed, server);

	report(orwl_taskdep_verbose, "37, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "37, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_38(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "38, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(38, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(38, orwl_task_write_if0), ORWL_LOCATION(38, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(74, orwl_task_read_if0), 1, seed, server);

	report(orwl_taskdep_verbose, "38, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "38, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_39(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "39, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(39, orwl_task_write_if0), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(39, orwl_task_write_if0), ORWL_LOCATION(39, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(74, orwl_task_read_if1), 1, seed, server);

	report(orwl_taskdep_verbose, "39, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "39, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_40(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "40, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(40, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(40, orwl_task_write_if0), ORWL_LOCATION(40, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(74, orwl_task_read_if3), 1, seed, server);

	report(orwl_taskdep_verbose, "40, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "40, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_41(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "41, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(41, orwl_task_write_if0), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(41, orwl_task_write_if0), ORWL_LOCATION(41, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(74, orwl_task_read_if2), 1, seed, server);

	report(orwl_taskdep_verbose, "41, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "41, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_42(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "42, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(42, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(42, orwl_task_write_if0), ORWL_LOCATION(42, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(75, orwl_task_read_if0), 1, seed, server);

	report(orwl_taskdep_verbose, "42, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "42, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_43(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "43, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(43, orwl_task_write_if0), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(43, orwl_task_write_if0), ORWL_LOCATION(43, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(75, orwl_task_read_if1), 1, seed, server);

	report(orwl_taskdep_verbose, "43, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "43, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_44(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "44, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(44, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(44, orwl_task_write_if0), ORWL_LOCATION(44, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(75, orwl_task_read_if3), 1, seed, server);

	report(orwl_taskdep_verbose, "44, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "44, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_45(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "45, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(45, orwl_task_write_if0), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(45, orwl_task_write_if0), ORWL_LOCATION(45, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(75, orwl_task_read_if2), 1, seed, server);

	report(orwl_taskdep_verbose, "45, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "45, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_46(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "46, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(46, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(46, orwl_task_write_if0), ORWL_LOCATION(46, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(76, orwl_task_read_if0), 1, seed, server);

	report(orwl_taskdep_verbose, "46, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "46, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_47(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "47, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(47, orwl_task_write_if0), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(47, orwl_task_write_if0), ORWL_LOCATION(47, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(76, orwl_task_read_if1), 1, seed, server);

	report(orwl_taskdep_verbose, "47, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "47, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_48(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "48, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(48, orwl_task_write_if0), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(48, orwl_task_write_if0), ORWL_LOCATION(48, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(76, orwl_task_read_if2), 1, seed, server);

	report(orwl_taskdep_verbose, "48, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "48, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_49(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "49, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(49, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(49, orwl_task_write_if0), ORWL_LOCATION(49, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(77, orwl_task_read_if3), 1, seed, server);

	report(orwl_taskdep_verbose, "49, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "49, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_50(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "50, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(50, orwl_task_write_if0), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(50, orwl_task_write_if0), ORWL_LOCATION(50, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(77, orwl_task_read_if2), 1, seed, server);

	report(orwl_taskdep_verbose, "50, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "50, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_51(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "51, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(51, orwl_task_write_if0), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(51, orwl_task_write_if0), ORWL_LOCATION(51, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(78, orwl_task_read_if1), 1, seed, server);

	report(orwl_taskdep_verbose, "51, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "51, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_52(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "52, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(52, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(52, orwl_task_write_if0), ORWL_LOCATION(52, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(78, orwl_task_read_if3), 1, seed, server);

	report(orwl_taskdep_verbose, "52, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "52, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_53(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "53, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(53, orwl_task_write_if0), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(53, orwl_task_write_if0), ORWL_LOCATION(53, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(78, orwl_task_read_if2), 1, seed, server);

	report(orwl_taskdep_verbose, "53, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "53, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_54(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "54, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(54, orwl_task_write_if0), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(54, orwl_task_write_if0), ORWL_LOCATION(54, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(79, orwl_task_read_if1), 1, seed, server);

	report(orwl_taskdep_verbose, "54, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "54, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_55(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "55, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(55, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(55, orwl_task_write_if0), ORWL_LOCATION(55, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(79, orwl_task_read_if3), 1, seed, server);

	report(orwl_taskdep_verbose, "55, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "55, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_56(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "56, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(56, orwl_task_write_if0), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(56, orwl_task_write_if0), ORWL_LOCATION(56, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(79, orwl_task_read_if2), 1, seed, server);

	report(orwl_taskdep_verbose, "56, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "56, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_57(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "57, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(57, orwl_task_write_if0), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(57, orwl_task_write_if0), ORWL_LOCATION(57, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(80, orwl_task_read_if1), 1, seed, server);

	report(orwl_taskdep_verbose, "57, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "57, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_58(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "58, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(58, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(58, orwl_task_write_if0), ORWL_LOCATION(58, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(80, orwl_task_read_if3), 1, seed, server);

	report(orwl_taskdep_verbose, "58, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "58, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_59(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "59, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(59, orwl_task_write_if0), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(59, orwl_task_write_if0), ORWL_LOCATION(59, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(80, orwl_task_read_if2), 1, seed, server);

	report(orwl_taskdep_verbose, "59, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "59, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_60(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "60, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(60, orwl_task_write_if0), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(60, orwl_task_write_if0), ORWL_LOCATION(60, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(81, orwl_task_read_if1), 1, seed, server);

	report(orwl_taskdep_verbose, "60, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "60, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_61(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "61, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(61, orwl_task_write_if0), 1, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(61, orwl_task_write_if0), ORWL_LOCATION(61, orwl_task_read_if0), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(81, orwl_task_read_if2), 1, seed, server);

	report(orwl_taskdep_verbose, "61, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "61, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_62(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "62, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(62, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(62, orwl_task_write_if0), ORWL_LOCATION(62, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(62, orwl_task_write_if1), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(62, orwl_task_write_if1), ORWL_LOCATION(62, orwl_task_read_if1), 10);
	orwl_write_insert(&hdl_out[2], ORWL_LOCATION(62, orwl_task_write_if2), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(62, orwl_task_write_if2), ORWL_LOCATION(62, orwl_task_read_if2), 10);
	orwl_write_insert(&hdl_out[3], ORWL_LOCATION(62, orwl_task_write_if3), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(62, orwl_task_write_if3), ORWL_LOCATION(62, orwl_task_read_if3), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(15, orwl_task_read_if0), 0, seed, server);
	// const port at 1
	// const port at 2
	orwl_read_insert(&hdl_in[3], ORWL_LOCATION(3, orwl_task_read_if0), 0, seed, server);

	report(orwl_taskdep_verbose, "62, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "62, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_63(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "63, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(63, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(63, orwl_task_write_if0), ORWL_LOCATION(63, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(63, orwl_task_write_if1), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(63, orwl_task_write_if1), ORWL_LOCATION(63, orwl_task_read_if1), 10);
	orwl_write_insert(&hdl_out[2], ORWL_LOCATION(63, orwl_task_write_if2), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(63, orwl_task_write_if2), ORWL_LOCATION(63, orwl_task_read_if2), 10);
	orwl_write_insert(&hdl_out[3], ORWL_LOCATION(63, orwl_task_write_if3), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(63, orwl_task_write_if3), ORWL_LOCATION(63, orwl_task_read_if3), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(19, orwl_task_read_if0), 0, seed, server);
	orwl_read_insert(&hdl_in[1], ORWL_LOCATION(1, orwl_task_read_if0), 1, seed, server);
	// const port at 2
	orwl_read_insert(&hdl_in[3], ORWL_LOCATION(6, orwl_task_read_if0), 0, seed, server);

	report(orwl_taskdep_verbose, "63, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "63, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_64(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "64, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(64, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(64, orwl_task_write_if0), ORWL_LOCATION(64, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(64, orwl_task_write_if1), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(64, orwl_task_write_if1), ORWL_LOCATION(64, orwl_task_read_if1), 10);
	orwl_write_insert(&hdl_out[2], ORWL_LOCATION(64, orwl_task_write_if2), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(64, orwl_task_write_if2), ORWL_LOCATION(64, orwl_task_read_if2), 10);
	orwl_write_insert(&hdl_out[3], ORWL_LOCATION(64, orwl_task_write_if3), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(64, orwl_task_write_if3), ORWL_LOCATION(64, orwl_task_read_if3), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(23, orwl_task_read_if0), 0, seed, server);
	orwl_read_insert(&hdl_in[1], ORWL_LOCATION(4, orwl_task_read_if0), 1, seed, server);
	// const port at 2
	orwl_read_insert(&hdl_in[3], ORWL_LOCATION(9, orwl_task_read_if0), 0, seed, server);

	report(orwl_taskdep_verbose, "64, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "64, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_65(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "65, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(65, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(65, orwl_task_write_if0), ORWL_LOCATION(65, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(65, orwl_task_write_if1), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(65, orwl_task_write_if1), ORWL_LOCATION(65, orwl_task_read_if1), 10);
	orwl_write_insert(&hdl_out[2], ORWL_LOCATION(65, orwl_task_write_if2), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(65, orwl_task_write_if2), ORWL_LOCATION(65, orwl_task_read_if2), 10);
	orwl_write_insert(&hdl_out[3], ORWL_LOCATION(65, orwl_task_write_if3), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(65, orwl_task_write_if3), ORWL_LOCATION(65, orwl_task_read_if3), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(27, orwl_task_read_if0), 0, seed, server);
	orwl_read_insert(&hdl_in[1], ORWL_LOCATION(7, orwl_task_read_if0), 1, seed, server);
	// const port at 2
	orwl_read_insert(&hdl_in[3], ORWL_LOCATION(12, orwl_task_read_if0), 0, seed, server);

	report(orwl_taskdep_verbose, "65, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "65, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_66(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "66, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(66, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(66, orwl_task_write_if0), ORWL_LOCATION(66, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(66, orwl_task_write_if1), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(66, orwl_task_write_if1), ORWL_LOCATION(66, orwl_task_read_if1), 10);
	orwl_write_insert(&hdl_out[2], ORWL_LOCATION(66, orwl_task_write_if2), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(66, orwl_task_write_if2), ORWL_LOCATION(66, orwl_task_read_if2), 10);
	orwl_write_insert(&hdl_out[3], ORWL_LOCATION(66, orwl_task_write_if3), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(66, orwl_task_write_if3), ORWL_LOCATION(66, orwl_task_read_if3), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(30, orwl_task_read_if0), 0, seed, server);
	orwl_read_insert(&hdl_in[1], ORWL_LOCATION(10, orwl_task_read_if0), 1, seed, server);
	// const port at 2
	// const port at 3

	report(orwl_taskdep_verbose, "66, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "66, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_67(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "67, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(67, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(67, orwl_task_write_if0), ORWL_LOCATION(67, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(67, orwl_task_write_if1), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(67, orwl_task_write_if1), ORWL_LOCATION(67, orwl_task_read_if1), 10);
	orwl_write_insert(&hdl_out[2], ORWL_LOCATION(67, orwl_task_write_if2), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(67, orwl_task_write_if2), ORWL_LOCATION(67, orwl_task_read_if2), 10);
	orwl_write_insert(&hdl_out[3], ORWL_LOCATION(67, orwl_task_write_if3), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(67, orwl_task_write_if3), ORWL_LOCATION(67, orwl_task_read_if3), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(33, orwl_task_read_if0), 0, seed, server);
	// const port at 1
	orwl_read_insert(&hdl_in[2], ORWL_LOCATION(0, orwl_task_read_if0), 1, seed, server);
	orwl_read_insert(&hdl_in[3], ORWL_LOCATION(17, orwl_task_read_if0), 0, seed, server);

	report(orwl_taskdep_verbose, "67, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "67, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_68(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "68, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(68, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(68, orwl_task_write_if0), ORWL_LOCATION(68, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(68, orwl_task_write_if1), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(68, orwl_task_write_if1), ORWL_LOCATION(68, orwl_task_read_if1), 10);
	orwl_write_insert(&hdl_out[2], ORWL_LOCATION(68, orwl_task_write_if2), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(68, orwl_task_write_if2), ORWL_LOCATION(68, orwl_task_read_if2), 10);
	orwl_write_insert(&hdl_out[3], ORWL_LOCATION(68, orwl_task_write_if3), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(68, orwl_task_write_if3), ORWL_LOCATION(68, orwl_task_read_if3), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(37, orwl_task_read_if0), 0, seed, server);
	orwl_read_insert(&hdl_in[1], ORWL_LOCATION(14, orwl_task_read_if0), 1, seed, server);
	orwl_read_insert(&hdl_in[2], ORWL_LOCATION(2, orwl_task_read_if0), 1, seed, server);
	orwl_read_insert(&hdl_in[3], ORWL_LOCATION(21, orwl_task_read_if0), 0, seed, server);

	report(orwl_taskdep_verbose, "68, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "68, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_69(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "69, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(69, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(69, orwl_task_write_if0), ORWL_LOCATION(69, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(69, orwl_task_write_if1), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(69, orwl_task_write_if1), ORWL_LOCATION(69, orwl_task_read_if1), 10);
	orwl_write_insert(&hdl_out[2], ORWL_LOCATION(69, orwl_task_write_if2), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(69, orwl_task_write_if2), ORWL_LOCATION(69, orwl_task_read_if2), 10);
	orwl_write_insert(&hdl_out[3], ORWL_LOCATION(69, orwl_task_write_if3), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(69, orwl_task_write_if3), ORWL_LOCATION(69, orwl_task_read_if3), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(41, orwl_task_read_if0), 0, seed, server);
	orwl_read_insert(&hdl_in[1], ORWL_LOCATION(18, orwl_task_read_if0), 1, seed, server);
	orwl_read_insert(&hdl_in[2], ORWL_LOCATION(5, orwl_task_read_if0), 1, seed, server);
	orwl_read_insert(&hdl_in[3], ORWL_LOCATION(25, orwl_task_read_if0), 0, seed, server);

	report(orwl_taskdep_verbose, "69, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "69, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_70(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "70, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(70, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(70, orwl_task_write_if0), ORWL_LOCATION(70, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(70, orwl_task_write_if1), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(70, orwl_task_write_if1), ORWL_LOCATION(70, orwl_task_read_if1), 10);
	orwl_write_insert(&hdl_out[2], ORWL_LOCATION(70, orwl_task_write_if2), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(70, orwl_task_write_if2), ORWL_LOCATION(70, orwl_task_read_if2), 10);
	orwl_write_insert(&hdl_out[3], ORWL_LOCATION(70, orwl_task_write_if3), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(70, orwl_task_write_if3), ORWL_LOCATION(70, orwl_task_read_if3), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(45, orwl_task_read_if0), 0, seed, server);
	orwl_read_insert(&hdl_in[1], ORWL_LOCATION(22, orwl_task_read_if0), 1, seed, server);
	orwl_read_insert(&hdl_in[2], ORWL_LOCATION(8, orwl_task_read_if0), 1, seed, server);
	orwl_read_insert(&hdl_in[3], ORWL_LOCATION(29, orwl_task_read_if0), 0, seed, server);

	report(orwl_taskdep_verbose, "70, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "70, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_71(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "71, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(71, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(71, orwl_task_write_if0), ORWL_LOCATION(71, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(71, orwl_task_write_if1), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(71, orwl_task_write_if1), ORWL_LOCATION(71, orwl_task_read_if1), 10);
	orwl_write_insert(&hdl_out[2], ORWL_LOCATION(71, orwl_task_write_if2), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(71, orwl_task_write_if2), ORWL_LOCATION(71, orwl_task_read_if2), 10);
	orwl_write_insert(&hdl_out[3], ORWL_LOCATION(71, orwl_task_write_if3), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(71, orwl_task_write_if3), ORWL_LOCATION(71, orwl_task_read_if3), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(48, orwl_task_read_if0), 0, seed, server);
	orwl_read_insert(&hdl_in[1], ORWL_LOCATION(26, orwl_task_read_if0), 1, seed, server);
	orwl_read_insert(&hdl_in[2], ORWL_LOCATION(11, orwl_task_read_if0), 1, seed, server);
	// const port at 3

	report(orwl_taskdep_verbose, "71, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "71, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_72(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "72, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(72, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(72, orwl_task_write_if0), ORWL_LOCATION(72, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(72, orwl_task_write_if1), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(72, orwl_task_write_if1), ORWL_LOCATION(72, orwl_task_read_if1), 10);
	orwl_write_insert(&hdl_out[2], ORWL_LOCATION(72, orwl_task_write_if2), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(72, orwl_task_write_if2), ORWL_LOCATION(72, orwl_task_read_if2), 10);
	orwl_write_insert(&hdl_out[3], ORWL_LOCATION(72, orwl_task_write_if3), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(72, orwl_task_write_if3), ORWL_LOCATION(72, orwl_task_read_if3), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(50, orwl_task_read_if0), 0, seed, server);
	// const port at 1
	orwl_read_insert(&hdl_in[2], ORWL_LOCATION(13, orwl_task_read_if0), 1, seed, server);
	orwl_read_insert(&hdl_in[3], ORWL_LOCATION(35, orwl_task_read_if0), 0, seed, server);

	report(orwl_taskdep_verbose, "72, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "72, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_73(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "73, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(73, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(73, orwl_task_write_if0), ORWL_LOCATION(73, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(73, orwl_task_write_if1), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(73, orwl_task_write_if1), ORWL_LOCATION(73, orwl_task_read_if1), 10);
	orwl_write_insert(&hdl_out[2], ORWL_LOCATION(73, orwl_task_write_if2), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(73, orwl_task_write_if2), ORWL_LOCATION(73, orwl_task_read_if2), 10);
	orwl_write_insert(&hdl_out[3], ORWL_LOCATION(73, orwl_task_write_if3), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(73, orwl_task_write_if3), ORWL_LOCATION(73, orwl_task_read_if3), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(53, orwl_task_read_if0), 0, seed, server);
	orwl_read_insert(&hdl_in[1], ORWL_LOCATION(32, orwl_task_read_if0), 1, seed, server);
	orwl_read_insert(&hdl_in[2], ORWL_LOCATION(16, orwl_task_read_if0), 1, seed, server);
	orwl_read_insert(&hdl_in[3], ORWL_LOCATION(39, orwl_task_read_if0), 0, seed, server);

	report(orwl_taskdep_verbose, "73, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "73, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_74(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "74, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(74, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(74, orwl_task_write_if0), ORWL_LOCATION(74, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(74, orwl_task_write_if1), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(74, orwl_task_write_if1), ORWL_LOCATION(74, orwl_task_read_if1), 10);
	orwl_write_insert(&hdl_out[2], ORWL_LOCATION(74, orwl_task_write_if2), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(74, orwl_task_write_if2), ORWL_LOCATION(74, orwl_task_read_if2), 10);
	orwl_write_insert(&hdl_out[3], ORWL_LOCATION(74, orwl_task_write_if3), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(74, orwl_task_write_if3), ORWL_LOCATION(74, orwl_task_read_if3), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(56, orwl_task_read_if0), 0, seed, server);
	orwl_read_insert(&hdl_in[1], ORWL_LOCATION(36, orwl_task_read_if0), 1, seed, server);
	orwl_read_insert(&hdl_in[2], ORWL_LOCATION(20, orwl_task_read_if0), 1, seed, server);
	orwl_read_insert(&hdl_in[3], ORWL_LOCATION(43, orwl_task_read_if0), 0, seed, server);

	report(orwl_taskdep_verbose, "74, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "74, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_75(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "75, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(75, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(75, orwl_task_write_if0), ORWL_LOCATION(75, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(75, orwl_task_write_if1), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(75, orwl_task_write_if1), ORWL_LOCATION(75, orwl_task_read_if1), 10);
	orwl_write_insert(&hdl_out[2], ORWL_LOCATION(75, orwl_task_write_if2), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(75, orwl_task_write_if2), ORWL_LOCATION(75, orwl_task_read_if2), 10);
	orwl_write_insert(&hdl_out[3], ORWL_LOCATION(75, orwl_task_write_if3), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(75, orwl_task_write_if3), ORWL_LOCATION(75, orwl_task_read_if3), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(59, orwl_task_read_if0), 0, seed, server);
	orwl_read_insert(&hdl_in[1], ORWL_LOCATION(40, orwl_task_read_if0), 1, seed, server);
	orwl_read_insert(&hdl_in[2], ORWL_LOCATION(24, orwl_task_read_if0), 1, seed, server);
	orwl_read_insert(&hdl_in[3], ORWL_LOCATION(47, orwl_task_read_if0), 0, seed, server);

	report(orwl_taskdep_verbose, "75, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "75, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_76(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "76, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(76, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(76, orwl_task_write_if0), ORWL_LOCATION(76, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(76, orwl_task_write_if1), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(76, orwl_task_write_if1), ORWL_LOCATION(76, orwl_task_read_if1), 10);
	orwl_write_insert(&hdl_out[2], ORWL_LOCATION(76, orwl_task_write_if2), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(76, orwl_task_write_if2), ORWL_LOCATION(76, orwl_task_read_if2), 10);
	orwl_write_insert(&hdl_out[3], ORWL_LOCATION(76, orwl_task_write_if3), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(76, orwl_task_write_if3), ORWL_LOCATION(76, orwl_task_read_if3), 10);
	orwl_read_insert(&hdl_in[0], ORWL_LOCATION(61, orwl_task_read_if0), 0, seed, server);
	orwl_read_insert(&hdl_in[1], ORWL_LOCATION(44, orwl_task_read_if0), 1, seed, server);
	orwl_read_insert(&hdl_in[2], ORWL_LOCATION(28, orwl_task_read_if0), 1, seed, server);
	// const port at 3

	report(orwl_taskdep_verbose, "76, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "76, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_77(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "77, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(77, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(77, orwl_task_write_if0), ORWL_LOCATION(77, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(77, orwl_task_write_if1), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(77, orwl_task_write_if1), ORWL_LOCATION(77, orwl_task_read_if1), 10);
	orwl_write_insert(&hdl_out[2], ORWL_LOCATION(77, orwl_task_write_if2), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(77, orwl_task_write_if2), ORWL_LOCATION(77, orwl_task_read_if2), 10);
	orwl_write_insert(&hdl_out[3], ORWL_LOCATION(77, orwl_task_write_if3), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(77, orwl_task_write_if3), ORWL_LOCATION(77, orwl_task_read_if3), 10);
	// const port at 0
	// const port at 1
	orwl_read_insert(&hdl_in[2], ORWL_LOCATION(31, orwl_task_read_if0), 1, seed, server);
	orwl_read_insert(&hdl_in[3], ORWL_LOCATION(51, orwl_task_read_if0), 0, seed, server);

	report(orwl_taskdep_verbose, "77, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "77, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_78(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "78, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(78, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(78, orwl_task_write_if0), ORWL_LOCATION(78, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(78, orwl_task_write_if1), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(78, orwl_task_write_if1), ORWL_LOCATION(78, orwl_task_read_if1), 10);
	orwl_write_insert(&hdl_out[2], ORWL_LOCATION(78, orwl_task_write_if2), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(78, orwl_task_write_if2), ORWL_LOCATION(78, orwl_task_read_if2), 10);
	orwl_write_insert(&hdl_out[3], ORWL_LOCATION(78, orwl_task_write_if3), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(78, orwl_task_write_if3), ORWL_LOCATION(78, orwl_task_read_if3), 10);
	// const port at 0
	orwl_read_insert(&hdl_in[1], ORWL_LOCATION(49, orwl_task_read_if0), 1, seed, server);
	orwl_read_insert(&hdl_in[2], ORWL_LOCATION(34, orwl_task_read_if0), 1, seed, server);
	orwl_read_insert(&hdl_in[3], ORWL_LOCATION(54, orwl_task_read_if0), 0, seed, server);

	report(orwl_taskdep_verbose, "78, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "78, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_79(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "79, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(79, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(79, orwl_task_write_if0), ORWL_LOCATION(79, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(79, orwl_task_write_if1), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(79, orwl_task_write_if1), ORWL_LOCATION(79, orwl_task_read_if1), 10);
	orwl_write_insert(&hdl_out[2], ORWL_LOCATION(79, orwl_task_write_if2), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(79, orwl_task_write_if2), ORWL_LOCATION(79, orwl_task_read_if2), 10);
	orwl_write_insert(&hdl_out[3], ORWL_LOCATION(79, orwl_task_write_if3), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(79, orwl_task_write_if3), ORWL_LOCATION(79, orwl_task_read_if3), 10);
	// const port at 0
	orwl_read_insert(&hdl_in[1], ORWL_LOCATION(52, orwl_task_read_if0), 1, seed, server);
	orwl_read_insert(&hdl_in[2], ORWL_LOCATION(38, orwl_task_read_if0), 1, seed, server);
	orwl_read_insert(&hdl_in[3], ORWL_LOCATION(57, orwl_task_read_if0), 0, seed, server);

	report(orwl_taskdep_verbose, "79, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "79, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_80(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "80, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(80, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(80, orwl_task_write_if0), ORWL_LOCATION(80, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(80, orwl_task_write_if1), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(80, orwl_task_write_if1), ORWL_LOCATION(80, orwl_task_read_if1), 10);
	orwl_write_insert(&hdl_out[2], ORWL_LOCATION(80, orwl_task_write_if2), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(80, orwl_task_write_if2), ORWL_LOCATION(80, orwl_task_read_if2), 10);
	orwl_write_insert(&hdl_out[3], ORWL_LOCATION(80, orwl_task_write_if3), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(80, orwl_task_write_if3), ORWL_LOCATION(80, orwl_task_read_if3), 10);
	// const port at 0
	orwl_read_insert(&hdl_in[1], ORWL_LOCATION(55, orwl_task_read_if0), 1, seed, server);
	orwl_read_insert(&hdl_in[2], ORWL_LOCATION(42, orwl_task_read_if0), 1, seed, server);
	orwl_read_insert(&hdl_in[3], ORWL_LOCATION(60, orwl_task_read_if0), 0, seed, server);

	report(orwl_taskdep_verbose, "80, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "80, finished orwl_taskdep_init");

};
static void orwl_taskdep_init_81(orwl_handle2 hdl_in[static 4], orwl_handle2 hdl_out[static 4]) {
	report(orwl_taskdep_verbose, "81, starting orwl_taskdep_init, seeing %d locations", orwl_locations_amount);

	p99_seed*const seed = p99_seed_get();
	orwl_server*const server = orwl_server_get();
	orwl_write_insert(&hdl_out[0], ORWL_LOCATION(81, orwl_task_write_if0), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(81, orwl_task_write_if0), ORWL_LOCATION(81, orwl_task_read_if0), 10);
	orwl_write_insert(&hdl_out[1], ORWL_LOCATION(81, orwl_task_write_if1), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(81, orwl_task_write_if1), ORWL_LOCATION(81, orwl_task_read_if1), 10);
	orwl_write_insert(&hdl_out[2], ORWL_LOCATION(81, orwl_task_write_if2), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(81, orwl_task_write_if2), ORWL_LOCATION(81, orwl_task_read_if2), 10);
	orwl_write_insert(&hdl_out[3], ORWL_LOCATION(81, orwl_task_write_if3), 0, seed, server);
	orwl_alloc_queue(ORWL_LOCATION(81, orwl_task_write_if3), ORWL_LOCATION(81, orwl_task_read_if3), 10);
	// const port at 0
	orwl_read_insert(&hdl_in[1], ORWL_LOCATION(58, orwl_task_read_if0), 1, seed, server);
	orwl_read_insert(&hdl_in[2], ORWL_LOCATION(46, orwl_task_read_if0), 1, seed, server);
	// const port at 3

	report(orwl_taskdep_verbose, "81, orwl_taskdep_init all inserted");

	// Synchronize to have all requests inserted orderly at the other end.
	orwl_schedule();
	report(orwl_taskdep_verbose, "81, finished orwl_taskdep_init");

};
static orwl_taskdep_vector const p_dummy = { 0 };

// Task function that is executed by task 0 (buf_1_1_b)
static void orwl_taskdep_run_0(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #0 (buf_1_1_b)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(0));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(0, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(0, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_0(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #0 (buf_1_1_b) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[0][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[0][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(0), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[0][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #0 (buf_1_1_b) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(0));

    report(orwl_taskdep_verbose, "task #0 (buf_1_1_b) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #0 (buf_1_1_b) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #0 (buf_1_1_b) ends");
}


// Task function that is executed by task 1 (buf_1_1_r)
static void orwl_taskdep_run_1(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #1 (buf_1_1_r)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(1));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(1, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(1, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_1(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #1 (buf_1_1_r) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[1][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[1][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(1), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[1][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #1 (buf_1_1_r) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(1));

    report(orwl_taskdep_verbose, "task #1 (buf_1_1_r) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #1 (buf_1_1_r) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #1 (buf_1_1_r) ends");
}


// Task function that is executed by task 2 (buf_1_2_b)
static void orwl_taskdep_run_2(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #2 (buf_1_2_b)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(2));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(2, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(2, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_2(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #2 (buf_1_2_b) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[2][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[2][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(2), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[2][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #2 (buf_1_2_b) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(2));

    report(orwl_taskdep_verbose, "task #2 (buf_1_2_b) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #2 (buf_1_2_b) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #2 (buf_1_2_b) ends");
}


// Task function that is executed by task 3 (buf_1_2_l)
static void orwl_taskdep_run_3(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #3 (buf_1_2_l)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(3));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(3, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(3, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_3(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #3 (buf_1_2_l) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[3][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[3][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(3), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[3][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #3 (buf_1_2_l) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(3));

    report(orwl_taskdep_verbose, "task #3 (buf_1_2_l) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #3 (buf_1_2_l) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #3 (buf_1_2_l) ends");
}


// Task function that is executed by task 4 (buf_1_2_r)
static void orwl_taskdep_run_4(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #4 (buf_1_2_r)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(4));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(4, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(4, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_4(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #4 (buf_1_2_r) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[4][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[4][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(4), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[4][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #4 (buf_1_2_r) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(4));

    report(orwl_taskdep_verbose, "task #4 (buf_1_2_r) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #4 (buf_1_2_r) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #4 (buf_1_2_r) ends");
}


// Task function that is executed by task 5 (buf_1_3_b)
static void orwl_taskdep_run_5(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #5 (buf_1_3_b)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(5));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(5, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(5, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_5(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #5 (buf_1_3_b) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[5][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[5][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(5), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[5][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #5 (buf_1_3_b) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(5));

    report(orwl_taskdep_verbose, "task #5 (buf_1_3_b) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #5 (buf_1_3_b) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #5 (buf_1_3_b) ends");
}


// Task function that is executed by task 6 (buf_1_3_l)
static void orwl_taskdep_run_6(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #6 (buf_1_3_l)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(6));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(6, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(6, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_6(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #6 (buf_1_3_l) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[6][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[6][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(6), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[6][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #6 (buf_1_3_l) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(6));

    report(orwl_taskdep_verbose, "task #6 (buf_1_3_l) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #6 (buf_1_3_l) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #6 (buf_1_3_l) ends");
}


// Task function that is executed by task 7 (buf_1_3_r)
static void orwl_taskdep_run_7(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #7 (buf_1_3_r)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(7));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(7, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(7, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_7(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #7 (buf_1_3_r) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[7][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[7][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(7), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[7][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #7 (buf_1_3_r) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(7));

    report(orwl_taskdep_verbose, "task #7 (buf_1_3_r) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #7 (buf_1_3_r) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #7 (buf_1_3_r) ends");
}


// Task function that is executed by task 8 (buf_1_4_b)
static void orwl_taskdep_run_8(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #8 (buf_1_4_b)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(8));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(8, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(8, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_8(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #8 (buf_1_4_b) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[8][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[8][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(8), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[8][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #8 (buf_1_4_b) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(8));

    report(orwl_taskdep_verbose, "task #8 (buf_1_4_b) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #8 (buf_1_4_b) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #8 (buf_1_4_b) ends");
}


// Task function that is executed by task 9 (buf_1_4_l)
static void orwl_taskdep_run_9(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #9 (buf_1_4_l)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(9));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(9, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(9, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_9(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #9 (buf_1_4_l) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[9][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[9][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(9), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[9][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #9 (buf_1_4_l) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(9));

    report(orwl_taskdep_verbose, "task #9 (buf_1_4_l) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #9 (buf_1_4_l) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #9 (buf_1_4_l) ends");
}


// Task function that is executed by task 10 (buf_1_4_r)
static void orwl_taskdep_run_10(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #10 (buf_1_4_r)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(10));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(10, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(10, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_10(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #10 (buf_1_4_r) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[10][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[10][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(10), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[10][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #10 (buf_1_4_r) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(10));

    report(orwl_taskdep_verbose, "task #10 (buf_1_4_r) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #10 (buf_1_4_r) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #10 (buf_1_4_r) ends");
}


// Task function that is executed by task 11 (buf_1_5_b)
static void orwl_taskdep_run_11(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #11 (buf_1_5_b)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(11));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(11, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(11, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_11(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #11 (buf_1_5_b) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[11][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[11][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(11), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[11][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #11 (buf_1_5_b) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(11));

    report(orwl_taskdep_verbose, "task #11 (buf_1_5_b) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #11 (buf_1_5_b) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #11 (buf_1_5_b) ends");
}


// Task function that is executed by task 12 (buf_1_5_l)
static void orwl_taskdep_run_12(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #12 (buf_1_5_l)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(12));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(12, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(12, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_12(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #12 (buf_1_5_l) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[12][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[12][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(12), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[12][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #12 (buf_1_5_l) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(12));

    report(orwl_taskdep_verbose, "task #12 (buf_1_5_l) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #12 (buf_1_5_l) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #12 (buf_1_5_l) ends");
}


// Task function that is executed by task 13 (buf_2_1_b)
static void orwl_taskdep_run_13(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #13 (buf_2_1_b)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(13));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(13, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(13, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_13(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #13 (buf_2_1_b) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[13][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[13][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(13), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[13][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #13 (buf_2_1_b) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(13));

    report(orwl_taskdep_verbose, "task #13 (buf_2_1_b) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #13 (buf_2_1_b) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #13 (buf_2_1_b) ends");
}


// Task function that is executed by task 14 (buf_2_1_r)
static void orwl_taskdep_run_14(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #14 (buf_2_1_r)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(14));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(14, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(14, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_14(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #14 (buf_2_1_r) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[14][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[14][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(14), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[14][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #14 (buf_2_1_r) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(14));

    report(orwl_taskdep_verbose, "task #14 (buf_2_1_r) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #14 (buf_2_1_r) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #14 (buf_2_1_r) ends");
}


// Task function that is executed by task 15 (buf_2_1_t)
static void orwl_taskdep_run_15(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #15 (buf_2_1_t)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(15));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(15, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(15, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_15(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #15 (buf_2_1_t) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[15][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[15][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(15), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[15][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #15 (buf_2_1_t) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(15));

    report(orwl_taskdep_verbose, "task #15 (buf_2_1_t) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #15 (buf_2_1_t) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #15 (buf_2_1_t) ends");
}


// Task function that is executed by task 16 (buf_2_2_b)
static void orwl_taskdep_run_16(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #16 (buf_2_2_b)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(16));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(16, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(16, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_16(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #16 (buf_2_2_b) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[16][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[16][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(16), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[16][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #16 (buf_2_2_b) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(16));

    report(orwl_taskdep_verbose, "task #16 (buf_2_2_b) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #16 (buf_2_2_b) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #16 (buf_2_2_b) ends");
}


// Task function that is executed by task 17 (buf_2_2_l)
static void orwl_taskdep_run_17(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #17 (buf_2_2_l)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(17));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(17, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(17, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_17(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #17 (buf_2_2_l) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[17][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[17][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(17), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[17][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #17 (buf_2_2_l) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(17));

    report(orwl_taskdep_verbose, "task #17 (buf_2_2_l) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #17 (buf_2_2_l) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #17 (buf_2_2_l) ends");
}


// Task function that is executed by task 18 (buf_2_2_r)
static void orwl_taskdep_run_18(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #18 (buf_2_2_r)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(18));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(18, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(18, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_18(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #18 (buf_2_2_r) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[18][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[18][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(18), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[18][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #18 (buf_2_2_r) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(18));

    report(orwl_taskdep_verbose, "task #18 (buf_2_2_r) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #18 (buf_2_2_r) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #18 (buf_2_2_r) ends");
}


// Task function that is executed by task 19 (buf_2_2_t)
static void orwl_taskdep_run_19(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #19 (buf_2_2_t)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(19));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(19, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(19, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_19(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #19 (buf_2_2_t) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[19][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[19][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(19), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[19][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #19 (buf_2_2_t) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(19));

    report(orwl_taskdep_verbose, "task #19 (buf_2_2_t) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #19 (buf_2_2_t) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #19 (buf_2_2_t) ends");
}


// Task function that is executed by task 20 (buf_2_3_b)
static void orwl_taskdep_run_20(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #20 (buf_2_3_b)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(20));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(20, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(20, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_20(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #20 (buf_2_3_b) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[20][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[20][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(20), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[20][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #20 (buf_2_3_b) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(20));

    report(orwl_taskdep_verbose, "task #20 (buf_2_3_b) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #20 (buf_2_3_b) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #20 (buf_2_3_b) ends");
}


// Task function that is executed by task 21 (buf_2_3_l)
static void orwl_taskdep_run_21(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #21 (buf_2_3_l)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(21));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(21, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(21, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_21(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #21 (buf_2_3_l) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[21][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[21][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(21), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[21][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #21 (buf_2_3_l) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(21));

    report(orwl_taskdep_verbose, "task #21 (buf_2_3_l) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #21 (buf_2_3_l) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #21 (buf_2_3_l) ends");
}


// Task function that is executed by task 22 (buf_2_3_r)
static void orwl_taskdep_run_22(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #22 (buf_2_3_r)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(22));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(22, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(22, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_22(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #22 (buf_2_3_r) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[22][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[22][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(22), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[22][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #22 (buf_2_3_r) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(22));

    report(orwl_taskdep_verbose, "task #22 (buf_2_3_r) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #22 (buf_2_3_r) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #22 (buf_2_3_r) ends");
}


// Task function that is executed by task 23 (buf_2_3_t)
static void orwl_taskdep_run_23(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #23 (buf_2_3_t)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(23));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(23, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(23, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_23(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #23 (buf_2_3_t) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[23][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[23][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(23), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[23][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #23 (buf_2_3_t) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(23));

    report(orwl_taskdep_verbose, "task #23 (buf_2_3_t) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #23 (buf_2_3_t) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #23 (buf_2_3_t) ends");
}


// Task function that is executed by task 24 (buf_2_4_b)
static void orwl_taskdep_run_24(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #24 (buf_2_4_b)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(24));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(24, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(24, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_24(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #24 (buf_2_4_b) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[24][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[24][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(24), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[24][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #24 (buf_2_4_b) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(24));

    report(orwl_taskdep_verbose, "task #24 (buf_2_4_b) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #24 (buf_2_4_b) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #24 (buf_2_4_b) ends");
}


// Task function that is executed by task 25 (buf_2_4_l)
static void orwl_taskdep_run_25(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #25 (buf_2_4_l)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(25));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(25, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(25, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_25(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #25 (buf_2_4_l) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[25][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[25][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(25), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[25][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #25 (buf_2_4_l) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(25));

    report(orwl_taskdep_verbose, "task #25 (buf_2_4_l) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #25 (buf_2_4_l) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #25 (buf_2_4_l) ends");
}


// Task function that is executed by task 26 (buf_2_4_r)
static void orwl_taskdep_run_26(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #26 (buf_2_4_r)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(26));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(26, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(26, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_26(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #26 (buf_2_4_r) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[26][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[26][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(26), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[26][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #26 (buf_2_4_r) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(26));

    report(orwl_taskdep_verbose, "task #26 (buf_2_4_r) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #26 (buf_2_4_r) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #26 (buf_2_4_r) ends");
}


// Task function that is executed by task 27 (buf_2_4_t)
static void orwl_taskdep_run_27(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #27 (buf_2_4_t)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(27));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(27, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(27, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_27(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #27 (buf_2_4_t) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[27][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[27][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(27), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[27][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #27 (buf_2_4_t) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(27));

    report(orwl_taskdep_verbose, "task #27 (buf_2_4_t) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #27 (buf_2_4_t) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #27 (buf_2_4_t) ends");
}


// Task function that is executed by task 28 (buf_2_5_b)
static void orwl_taskdep_run_28(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #28 (buf_2_5_b)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(28));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(28, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(28, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_28(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #28 (buf_2_5_b) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[28][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[28][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(28), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[28][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #28 (buf_2_5_b) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(28));

    report(orwl_taskdep_verbose, "task #28 (buf_2_5_b) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #28 (buf_2_5_b) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #28 (buf_2_5_b) ends");
}


// Task function that is executed by task 29 (buf_2_5_l)
static void orwl_taskdep_run_29(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #29 (buf_2_5_l)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(29));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(29, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(29, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_29(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #29 (buf_2_5_l) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[29][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[29][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(29), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[29][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #29 (buf_2_5_l) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(29));

    report(orwl_taskdep_verbose, "task #29 (buf_2_5_l) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #29 (buf_2_5_l) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #29 (buf_2_5_l) ends");
}


// Task function that is executed by task 30 (buf_2_5_t)
static void orwl_taskdep_run_30(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #30 (buf_2_5_t)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(30));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(30, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(30, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_30(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #30 (buf_2_5_t) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[30][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[30][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(30), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[30][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #30 (buf_2_5_t) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(30));

    report(orwl_taskdep_verbose, "task #30 (buf_2_5_t) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #30 (buf_2_5_t) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #30 (buf_2_5_t) ends");
}


// Task function that is executed by task 31 (buf_3_1_b)
static void orwl_taskdep_run_31(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #31 (buf_3_1_b)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(31));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(31, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(31, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_31(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #31 (buf_3_1_b) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[31][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[31][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(31), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[31][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #31 (buf_3_1_b) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(31));

    report(orwl_taskdep_verbose, "task #31 (buf_3_1_b) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #31 (buf_3_1_b) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #31 (buf_3_1_b) ends");
}


// Task function that is executed by task 32 (buf_3_1_r)
static void orwl_taskdep_run_32(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #32 (buf_3_1_r)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(32));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(32, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(32, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_32(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #32 (buf_3_1_r) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[32][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[32][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(32), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[32][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #32 (buf_3_1_r) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(32));

    report(orwl_taskdep_verbose, "task #32 (buf_3_1_r) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #32 (buf_3_1_r) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #32 (buf_3_1_r) ends");
}


// Task function that is executed by task 33 (buf_3_1_t)
static void orwl_taskdep_run_33(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #33 (buf_3_1_t)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(33));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(33, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(33, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_33(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #33 (buf_3_1_t) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[33][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[33][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(33), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[33][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #33 (buf_3_1_t) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(33));

    report(orwl_taskdep_verbose, "task #33 (buf_3_1_t) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #33 (buf_3_1_t) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #33 (buf_3_1_t) ends");
}


// Task function that is executed by task 34 (buf_3_2_b)
static void orwl_taskdep_run_34(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #34 (buf_3_2_b)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(34));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(34, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(34, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_34(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #34 (buf_3_2_b) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[34][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[34][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(34), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[34][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #34 (buf_3_2_b) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(34));

    report(orwl_taskdep_verbose, "task #34 (buf_3_2_b) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #34 (buf_3_2_b) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #34 (buf_3_2_b) ends");
}


// Task function that is executed by task 35 (buf_3_2_l)
static void orwl_taskdep_run_35(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #35 (buf_3_2_l)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(35));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(35, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(35, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_35(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #35 (buf_3_2_l) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[35][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[35][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(35), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[35][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #35 (buf_3_2_l) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(35));

    report(orwl_taskdep_verbose, "task #35 (buf_3_2_l) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #35 (buf_3_2_l) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #35 (buf_3_2_l) ends");
}


// Task function that is executed by task 36 (buf_3_2_r)
static void orwl_taskdep_run_36(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #36 (buf_3_2_r)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(36));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(36, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(36, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_36(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #36 (buf_3_2_r) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[36][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[36][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(36), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[36][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #36 (buf_3_2_r) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(36));

    report(orwl_taskdep_verbose, "task #36 (buf_3_2_r) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #36 (buf_3_2_r) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #36 (buf_3_2_r) ends");
}


// Task function that is executed by task 37 (buf_3_2_t)
static void orwl_taskdep_run_37(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #37 (buf_3_2_t)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(37));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(37, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(37, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_37(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #37 (buf_3_2_t) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[37][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[37][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(37), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[37][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #37 (buf_3_2_t) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(37));

    report(orwl_taskdep_verbose, "task #37 (buf_3_2_t) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #37 (buf_3_2_t) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #37 (buf_3_2_t) ends");
}


// Task function that is executed by task 38 (buf_3_3_b)
static void orwl_taskdep_run_38(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #38 (buf_3_3_b)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(38));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(38, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(38, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_38(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #38 (buf_3_3_b) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[38][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[38][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(38), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[38][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #38 (buf_3_3_b) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(38));

    report(orwl_taskdep_verbose, "task #38 (buf_3_3_b) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #38 (buf_3_3_b) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #38 (buf_3_3_b) ends");
}


// Task function that is executed by task 39 (buf_3_3_l)
static void orwl_taskdep_run_39(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #39 (buf_3_3_l)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(39));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(39, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(39, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_39(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #39 (buf_3_3_l) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[39][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[39][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(39), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[39][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #39 (buf_3_3_l) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(39));

    report(orwl_taskdep_verbose, "task #39 (buf_3_3_l) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #39 (buf_3_3_l) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #39 (buf_3_3_l) ends");
}


// Task function that is executed by task 40 (buf_3_3_r)
static void orwl_taskdep_run_40(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #40 (buf_3_3_r)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(40));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(40, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(40, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_40(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #40 (buf_3_3_r) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[40][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[40][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(40), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[40][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #40 (buf_3_3_r) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(40));

    report(orwl_taskdep_verbose, "task #40 (buf_3_3_r) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #40 (buf_3_3_r) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #40 (buf_3_3_r) ends");
}


// Task function that is executed by task 41 (buf_3_3_t)
static void orwl_taskdep_run_41(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #41 (buf_3_3_t)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(41));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(41, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(41, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_41(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #41 (buf_3_3_t) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[41][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[41][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(41), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[41][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #41 (buf_3_3_t) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(41));

    report(orwl_taskdep_verbose, "task #41 (buf_3_3_t) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #41 (buf_3_3_t) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #41 (buf_3_3_t) ends");
}


// Task function that is executed by task 42 (buf_3_4_b)
static void orwl_taskdep_run_42(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #42 (buf_3_4_b)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(42));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(42, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(42, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_42(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #42 (buf_3_4_b) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[42][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[42][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(42), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[42][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #42 (buf_3_4_b) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(42));

    report(orwl_taskdep_verbose, "task #42 (buf_3_4_b) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #42 (buf_3_4_b) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #42 (buf_3_4_b) ends");
}


// Task function that is executed by task 43 (buf_3_4_l)
static void orwl_taskdep_run_43(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #43 (buf_3_4_l)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(43));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(43, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(43, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_43(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #43 (buf_3_4_l) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[43][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[43][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(43), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[43][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #43 (buf_3_4_l) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(43));

    report(orwl_taskdep_verbose, "task #43 (buf_3_4_l) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #43 (buf_3_4_l) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #43 (buf_3_4_l) ends");
}


// Task function that is executed by task 44 (buf_3_4_r)
static void orwl_taskdep_run_44(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #44 (buf_3_4_r)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(44));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(44, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(44, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_44(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #44 (buf_3_4_r) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[44][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[44][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(44), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[44][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #44 (buf_3_4_r) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(44));

    report(orwl_taskdep_verbose, "task #44 (buf_3_4_r) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #44 (buf_3_4_r) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #44 (buf_3_4_r) ends");
}


// Task function that is executed by task 45 (buf_3_4_t)
static void orwl_taskdep_run_45(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #45 (buf_3_4_t)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(45));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(45, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(45, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_45(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #45 (buf_3_4_t) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[45][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[45][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(45), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[45][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #45 (buf_3_4_t) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(45));

    report(orwl_taskdep_verbose, "task #45 (buf_3_4_t) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #45 (buf_3_4_t) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #45 (buf_3_4_t) ends");
}


// Task function that is executed by task 46 (buf_3_5_b)
static void orwl_taskdep_run_46(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #46 (buf_3_5_b)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(46));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(46, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(46, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_46(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #46 (buf_3_5_b) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[46][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[46][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(46), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[46][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #46 (buf_3_5_b) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(46));

    report(orwl_taskdep_verbose, "task #46 (buf_3_5_b) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #46 (buf_3_5_b) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #46 (buf_3_5_b) ends");
}


// Task function that is executed by task 47 (buf_3_5_l)
static void orwl_taskdep_run_47(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #47 (buf_3_5_l)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(47));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(47, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(47, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_47(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #47 (buf_3_5_l) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[47][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[47][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(47), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[47][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #47 (buf_3_5_l) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(47));

    report(orwl_taskdep_verbose, "task #47 (buf_3_5_l) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #47 (buf_3_5_l) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #47 (buf_3_5_l) ends");
}


// Task function that is executed by task 48 (buf_3_5_t)
static void orwl_taskdep_run_48(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #48 (buf_3_5_t)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(48));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(48, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(48, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_48(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #48 (buf_3_5_t) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[48][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[48][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(48), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[48][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #48 (buf_3_5_t) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(48));

    report(orwl_taskdep_verbose, "task #48 (buf_3_5_t) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #48 (buf_3_5_t) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #48 (buf_3_5_t) ends");
}


// Task function that is executed by task 49 (buf_4_1_r)
static void orwl_taskdep_run_49(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #49 (buf_4_1_r)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(49));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(49, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(49, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_49(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #49 (buf_4_1_r) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[49][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[49][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(49), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[49][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #49 (buf_4_1_r) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(49));

    report(orwl_taskdep_verbose, "task #49 (buf_4_1_r) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #49 (buf_4_1_r) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #49 (buf_4_1_r) ends");
}


// Task function that is executed by task 50 (buf_4_1_t)
static void orwl_taskdep_run_50(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #50 (buf_4_1_t)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(50));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(50, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(50, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_50(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #50 (buf_4_1_t) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[50][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[50][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(50), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[50][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #50 (buf_4_1_t) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(50));

    report(orwl_taskdep_verbose, "task #50 (buf_4_1_t) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #50 (buf_4_1_t) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #50 (buf_4_1_t) ends");
}


// Task function that is executed by task 51 (buf_4_2_l)
static void orwl_taskdep_run_51(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #51 (buf_4_2_l)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(51));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(51, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(51, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_51(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #51 (buf_4_2_l) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[51][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[51][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(51), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[51][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #51 (buf_4_2_l) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(51));

    report(orwl_taskdep_verbose, "task #51 (buf_4_2_l) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #51 (buf_4_2_l) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #51 (buf_4_2_l) ends");
}


// Task function that is executed by task 52 (buf_4_2_r)
static void orwl_taskdep_run_52(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #52 (buf_4_2_r)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(52));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(52, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(52, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_52(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #52 (buf_4_2_r) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[52][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[52][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(52), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[52][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #52 (buf_4_2_r) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(52));

    report(orwl_taskdep_verbose, "task #52 (buf_4_2_r) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #52 (buf_4_2_r) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #52 (buf_4_2_r) ends");
}


// Task function that is executed by task 53 (buf_4_2_t)
static void orwl_taskdep_run_53(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #53 (buf_4_2_t)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(53));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(53, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(53, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_53(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #53 (buf_4_2_t) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[53][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[53][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(53), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[53][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #53 (buf_4_2_t) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(53));

    report(orwl_taskdep_verbose, "task #53 (buf_4_2_t) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #53 (buf_4_2_t) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #53 (buf_4_2_t) ends");
}


// Task function that is executed by task 54 (buf_4_3_l)
static void orwl_taskdep_run_54(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #54 (buf_4_3_l)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(54));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(54, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(54, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_54(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #54 (buf_4_3_l) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[54][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[54][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(54), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[54][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #54 (buf_4_3_l) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(54));

    report(orwl_taskdep_verbose, "task #54 (buf_4_3_l) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #54 (buf_4_3_l) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #54 (buf_4_3_l) ends");
}


// Task function that is executed by task 55 (buf_4_3_r)
static void orwl_taskdep_run_55(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #55 (buf_4_3_r)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(55));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(55, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(55, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_55(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #55 (buf_4_3_r) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[55][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[55][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(55), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[55][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #55 (buf_4_3_r) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(55));

    report(orwl_taskdep_verbose, "task #55 (buf_4_3_r) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #55 (buf_4_3_r) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #55 (buf_4_3_r) ends");
}


// Task function that is executed by task 56 (buf_4_3_t)
static void orwl_taskdep_run_56(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #56 (buf_4_3_t)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(56));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(56, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(56, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_56(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #56 (buf_4_3_t) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[56][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[56][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(56), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[56][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #56 (buf_4_3_t) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(56));

    report(orwl_taskdep_verbose, "task #56 (buf_4_3_t) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #56 (buf_4_3_t) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #56 (buf_4_3_t) ends");
}


// Task function that is executed by task 57 (buf_4_4_l)
static void orwl_taskdep_run_57(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #57 (buf_4_4_l)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(57));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(57, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(57, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_57(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #57 (buf_4_4_l) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[57][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[57][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(57), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[57][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #57 (buf_4_4_l) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(57));

    report(orwl_taskdep_verbose, "task #57 (buf_4_4_l) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #57 (buf_4_4_l) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #57 (buf_4_4_l) ends");
}


// Task function that is executed by task 58 (buf_4_4_r)
static void orwl_taskdep_run_58(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #58 (buf_4_4_r)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(58));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(58, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(58, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_58(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #58 (buf_4_4_r) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[58][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[58][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(58), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[58][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #58 (buf_4_4_r) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(58));

    report(orwl_taskdep_verbose, "task #58 (buf_4_4_r) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #58 (buf_4_4_r) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #58 (buf_4_4_r) ends");
}


// Task function that is executed by task 59 (buf_4_4_t)
static void orwl_taskdep_run_59(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #59 (buf_4_4_t)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(59));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(59, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(59, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_59(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #59 (buf_4_4_t) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[59][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[59][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(59), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[59][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #59 (buf_4_4_t) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(59));

    report(orwl_taskdep_verbose, "task #59 (buf_4_4_t) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #59 (buf_4_4_t) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #59 (buf_4_4_t) ends");
}


// Task function that is executed by task 60 (buf_4_5_l)
static void orwl_taskdep_run_60(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #60 (buf_4_5_l)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(60));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(60, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(60, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_60(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #60 (buf_4_5_l) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[60][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[60][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(60), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[60][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #60 (buf_4_5_l) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(60));

    report(orwl_taskdep_verbose, "task #60 (buf_4_5_l) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #60 (buf_4_5_l) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #60 (buf_4_5_l) ends");
}


// Task function that is executed by task 61 (buf_4_5_t)
static void orwl_taskdep_run_61(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #61 (buf_4_5_t)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(61));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 1,
        odeg  = 1,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(61, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(61, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_61(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #61 (buf_4_5_t) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[61][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[61][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         update(&state, &FREF64(61), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[61][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #61 (buf_4_5_t) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(61));

    report(orwl_taskdep_verbose, "task #61 (buf_4_5_t) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #61 (buf_4_5_t) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #61 (buf_4_5_t) ends");
}


// Task function that is executed by task 62 (node_1_1)
static void orwl_taskdep_run_62(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #62 (node_1_1)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(62));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 4,
        odeg  = 4,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(62, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(62, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_62(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #62 (node_1_1) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[62][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[62][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(62), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[62][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #62 (node_1_1) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(62));

    report(orwl_taskdep_verbose, "task #62 (node_1_1) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #62 (node_1_1) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #62 (node_1_1) ends");
}


// Task function that is executed by task 63 (node_1_2)
static void orwl_taskdep_run_63(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #63 (node_1_2)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(63));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 4,
        odeg  = 4,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(63, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(63, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_63(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #63 (node_1_2) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[63][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[63][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(63), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[63][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #63 (node_1_2) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(63));

    report(orwl_taskdep_verbose, "task #63 (node_1_2) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #63 (node_1_2) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #63 (node_1_2) ends");
}


// Task function that is executed by task 64 (node_1_3)
static void orwl_taskdep_run_64(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #64 (node_1_3)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(64));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 4,
        odeg  = 4,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(64, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(64, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_64(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #64 (node_1_3) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[64][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[64][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(64), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[64][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #64 (node_1_3) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(64));

    report(orwl_taskdep_verbose, "task #64 (node_1_3) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #64 (node_1_3) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #64 (node_1_3) ends");
}


// Task function that is executed by task 65 (node_1_4)
static void orwl_taskdep_run_65(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #65 (node_1_4)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(65));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 4,
        odeg  = 4,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(65, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(65, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_65(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #65 (node_1_4) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[65][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[65][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(65), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[65][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #65 (node_1_4) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(65));

    report(orwl_taskdep_verbose, "task #65 (node_1_4) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #65 (node_1_4) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #65 (node_1_4) ends");
}


// Task function that is executed by task 66 (node_1_5)
static void orwl_taskdep_run_66(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #66 (node_1_5)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(66));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 4,
        odeg  = 4,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(66, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(66, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_66(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #66 (node_1_5) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[66][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[66][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(66), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[66][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #66 (node_1_5) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(66));

    report(orwl_taskdep_verbose, "task #66 (node_1_5) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #66 (node_1_5) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #66 (node_1_5) ends");
}


// Task function that is executed by task 67 (node_2_1)
static void orwl_taskdep_run_67(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #67 (node_2_1)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(67));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 4,
        odeg  = 4,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(67, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(67, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_67(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #67 (node_2_1) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[67][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[67][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(67), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[67][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #67 (node_2_1) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(67));

    report(orwl_taskdep_verbose, "task #67 (node_2_1) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #67 (node_2_1) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #67 (node_2_1) ends");
}


// Task function that is executed by task 68 (node_2_2)
static void orwl_taskdep_run_68(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #68 (node_2_2)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(68));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 4,
        odeg  = 4,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(68, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(68, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_68(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #68 (node_2_2) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[68][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[68][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(68), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[68][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #68 (node_2_2) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(68));

    report(orwl_taskdep_verbose, "task #68 (node_2_2) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #68 (node_2_2) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #68 (node_2_2) ends");
}


// Task function that is executed by task 69 (node_2_3)
static void orwl_taskdep_run_69(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #69 (node_2_3)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(69));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 4,
        odeg  = 4,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(69, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(69, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_69(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #69 (node_2_3) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[69][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[69][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(69), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[69][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #69 (node_2_3) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(69));

    report(orwl_taskdep_verbose, "task #69 (node_2_3) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #69 (node_2_3) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #69 (node_2_3) ends");
}


// Task function that is executed by task 70 (node_2_4)
static void orwl_taskdep_run_70(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #70 (node_2_4)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(70));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 4,
        odeg  = 4,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(70, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(70, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_70(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #70 (node_2_4) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[70][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[70][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(70), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[70][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #70 (node_2_4) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(70));

    report(orwl_taskdep_verbose, "task #70 (node_2_4) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #70 (node_2_4) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #70 (node_2_4) ends");
}


// Task function that is executed by task 71 (node_2_5)
static void orwl_taskdep_run_71(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #71 (node_2_5)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(71));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 4,
        odeg  = 4,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(71, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(71, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_71(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #71 (node_2_5) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[71][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[71][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(71), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[71][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #71 (node_2_5) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(71));

    report(orwl_taskdep_verbose, "task #71 (node_2_5) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #71 (node_2_5) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #71 (node_2_5) ends");
}


// Task function that is executed by task 72 (node_3_1)
static void orwl_taskdep_run_72(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #72 (node_3_1)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(72));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 4,
        odeg  = 4,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(72, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(72, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_72(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #72 (node_3_1) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[72][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[72][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(72), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[72][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #72 (node_3_1) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(72));

    report(orwl_taskdep_verbose, "task #72 (node_3_1) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #72 (node_3_1) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #72 (node_3_1) ends");
}


// Task function that is executed by task 73 (node_3_2)
static void orwl_taskdep_run_73(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #73 (node_3_2)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(73));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 4,
        odeg  = 4,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(73, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(73, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_73(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #73 (node_3_2) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[73][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[73][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(73), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[73][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #73 (node_3_2) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(73));

    report(orwl_taskdep_verbose, "task #73 (node_3_2) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #73 (node_3_2) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #73 (node_3_2) ends");
}


// Task function that is executed by task 74 (node_3_3)
static void orwl_taskdep_run_74(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #74 (node_3_3)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(74));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 4,
        odeg  = 4,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(74, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(74, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_74(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #74 (node_3_3) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[74][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[74][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(74), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[74][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #74 (node_3_3) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(74));

    report(orwl_taskdep_verbose, "task #74 (node_3_3) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #74 (node_3_3) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #74 (node_3_3) ends");
}


// Task function that is executed by task 75 (node_3_4)
static void orwl_taskdep_run_75(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #75 (node_3_4)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(75));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 4,
        odeg  = 4,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(75, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(75, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_75(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #75 (node_3_4) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[75][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[75][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(75), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[75][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #75 (node_3_4) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(75));

    report(orwl_taskdep_verbose, "task #75 (node_3_4) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #75 (node_3_4) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #75 (node_3_4) ends");
}


// Task function that is executed by task 76 (node_3_5)
static void orwl_taskdep_run_76(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #76 (node_3_5)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(76));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 4,
        odeg  = 4,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(76, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(76, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_76(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #76 (node_3_5) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[76][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[76][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(76), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[76][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #76 (node_3_5) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(76));

    report(orwl_taskdep_verbose, "task #76 (node_3_5) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #76 (node_3_5) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #76 (node_3_5) ends");
}


// Task function that is executed by task 77 (node_4_1)
static void orwl_taskdep_run_77(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #77 (node_4_1)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(77));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 4,
        odeg  = 4,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(77, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(77, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_77(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #77 (node_4_1) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[77][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[77][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(77), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[77][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #77 (node_4_1) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(77));

    report(orwl_taskdep_verbose, "task #77 (node_4_1) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #77 (node_4_1) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #77 (node_4_1) ends");
}


// Task function that is executed by task 78 (node_4_2)
static void orwl_taskdep_run_78(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #78 (node_4_2)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(78));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 4,
        odeg  = 4,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(78, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(78, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_78(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #78 (node_4_2) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[78][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[78][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(78), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[78][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #78 (node_4_2) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(78));

    report(orwl_taskdep_verbose, "task #78 (node_4_2) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #78 (node_4_2) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #78 (node_4_2) ends");
}


// Task function that is executed by task 79 (node_4_3)
static void orwl_taskdep_run_79(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #79 (node_4_3)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(79));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 4,
        odeg  = 4,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(79, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(79, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_79(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #79 (node_4_3) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[79][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[79][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(79), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[79][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #79 (node_4_3) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(79));

    report(orwl_taskdep_verbose, "task #79 (node_4_3) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #79 (node_4_3) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #79 (node_4_3) ends");
}


// Task function that is executed by task 80 (node_4_4)
static void orwl_taskdep_run_80(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #80 (node_4_4)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(80));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 4,
        odeg  = 4,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(80, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(80, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_80(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #80 (node_4_4) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[80][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[80][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(80), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[80][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #80 (node_4_4) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(80));

    report(orwl_taskdep_verbose, "task #80 (node_4_4) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #80 (node_4_4) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #80 (node_4_4) ends");
}


// Task function that is executed by task 81 (node_4_5)
static void orwl_taskdep_run_81(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #81 (node_4_5)");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        orwl_taskdep_start(&state, &FREF64(81));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = 4,
        odeg  = 4,
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

    // This graph is iterated.
    static orwl_handle2 hdl_in[4] = {
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
        ORWL_HANDLE2_INITIALIZER,
    };
    static orwl_handle2 hdl_out[4] = {
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
         ORWL_HANDLE2_INITIALIZER,
    };
	report(orwl_taskdep_verbose, "starting handle initialization");
    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(81, 2*i));
        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(81, 2*i+1));
    }
    ORWL_TIMER(start) orwl_taskdep_init_81(hdl_in, hdl_out);
    for (int64_t orwl_phase = 0; orwl_phase < 30; ++orwl_phase) {
     report(orwl_taskdep_verbose, "task #81 (node_4_5) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[81][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[81][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / 4][port % 4];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / 4][port % 4]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         regular(&state, &FREF64(81), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[81][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #81 (node_4_5) finished iterations");

    ORWL_TIMER(start)
        orwl_taskdep_shutdown(&state, &FREF64(81));

    report(orwl_taskdep_verbose, "task #81 (node_4_5) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #81 (node_4_5) disconnecting");
    orwl_disconnect(hdl_in, ideg, seed);
    orwl_disconnect(hdl_out, odeg, seed);

    report(orwl_taskdep_verbose, "task #81 (node_4_5) ends");
}


static void (*const orwl_taskdep_runs[])(int argc, char* argv[argc+1]) = {
    orwl_taskdep_run_0,
    orwl_taskdep_run_1,
    orwl_taskdep_run_2,
    orwl_taskdep_run_3,
    orwl_taskdep_run_4,
    orwl_taskdep_run_5,
    orwl_taskdep_run_6,
    orwl_taskdep_run_7,
    orwl_taskdep_run_8,
    orwl_taskdep_run_9,
    orwl_taskdep_run_10,
    orwl_taskdep_run_11,
    orwl_taskdep_run_12,
    orwl_taskdep_run_13,
    orwl_taskdep_run_14,
    orwl_taskdep_run_15,
    orwl_taskdep_run_16,
    orwl_taskdep_run_17,
    orwl_taskdep_run_18,
    orwl_taskdep_run_19,
    orwl_taskdep_run_20,
    orwl_taskdep_run_21,
    orwl_taskdep_run_22,
    orwl_taskdep_run_23,
    orwl_taskdep_run_24,
    orwl_taskdep_run_25,
    orwl_taskdep_run_26,
    orwl_taskdep_run_27,
    orwl_taskdep_run_28,
    orwl_taskdep_run_29,
    orwl_taskdep_run_30,
    orwl_taskdep_run_31,
    orwl_taskdep_run_32,
    orwl_taskdep_run_33,
    orwl_taskdep_run_34,
    orwl_taskdep_run_35,
    orwl_taskdep_run_36,
    orwl_taskdep_run_37,
    orwl_taskdep_run_38,
    orwl_taskdep_run_39,
    orwl_taskdep_run_40,
    orwl_taskdep_run_41,
    orwl_taskdep_run_42,
    orwl_taskdep_run_43,
    orwl_taskdep_run_44,
    orwl_taskdep_run_45,
    orwl_taskdep_run_46,
    orwl_taskdep_run_47,
    orwl_taskdep_run_48,
    orwl_taskdep_run_49,
    orwl_taskdep_run_50,
    orwl_taskdep_run_51,
    orwl_taskdep_run_52,
    orwl_taskdep_run_53,
    orwl_taskdep_run_54,
    orwl_taskdep_run_55,
    orwl_taskdep_run_56,
    orwl_taskdep_run_57,
    orwl_taskdep_run_58,
    orwl_taskdep_run_59,
    orwl_taskdep_run_60,
    orwl_taskdep_run_61,
    orwl_taskdep_run_62,
    orwl_taskdep_run_63,
    orwl_taskdep_run_64,
    orwl_taskdep_run_65,
    orwl_taskdep_run_66,
    orwl_taskdep_run_67,
    orwl_taskdep_run_68,
    orwl_taskdep_run_69,
    orwl_taskdep_run_70,
    orwl_taskdep_run_71,
    orwl_taskdep_run_72,
    orwl_taskdep_run_73,
    orwl_taskdep_run_74,
    orwl_taskdep_run_75,
    orwl_taskdep_run_76,
    orwl_taskdep_run_77,
    orwl_taskdep_run_78,
    orwl_taskdep_run_79,
    orwl_taskdep_run_80,
    orwl_taskdep_run_81,
};

_Alignas(32) int64_t const orwl_taskdep_nodes   = 82;
_Alignas(32) int64_t const orwl_taskdep_max_inp = 4;
_Alignas(32) int64_t const orwl_taskdep_max_out = 4;

// Add symbols for Fortran compatibility.

_Alignas(32) int64_t const ORWL_TASKDEP_FORT(orwl_taskdep_vsize)   = orwl_taskdep_vsize;

ORWL_TASKDEP_ALIAS(orwl_taskdep_deg_out);
ORWL_TASKDEP_ALIAS(orwl_taskdep_deg_inp);
ORWL_TASKDEP_ALIAS(orwl_taskdep_nodes);
ORWL_TASKDEP_ALIAS(orwl_taskdep_max_inp);
ORWL_TASKDEP_ALIAS(orwl_taskdep_max_out);

// A wrapper as generic task function that just launches the specific function
ORWL_DEFINE_TASK(orwl_taskdep_type) {
    ORWL_THREAD_USE(orwl_taskdep_type, argc, argv);
    int64_t const tid = orwl_mytid;
    orwl_taskdep_runs[tid](argc, argv);
}

int main(int argc, char* argv[argc+1]) {
  p99_getopt_initialize(&argc, &argv);

  ORWL_TIMER() {
    orwl_init();

    if (orwl_nt != 82) {
        report(1, "We need exactly 82 task, to process this graph, current value is %zu, exiting.", orwl_nt);
        return EXIT_FAILURE;
    }

    //! [launch one thread per task]
    for (size_t i = 0; i < orwl_lt; i++) {
      orwl_taskdep_type* task = P99_NEW(orwl_taskdep_type, argc, argv);
      orwl_taskdep_type_create_task(task, orwl_tids[i]);
    }
    //! [launch one thread per task]
  }
  return EXIT_SUCCESS;
}

// End of program for graph rect_neigh_4_5
// *********************************************************************************

