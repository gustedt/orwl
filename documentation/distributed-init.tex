%  -*- mode: LaTeX;  ispell-local-dictionary: "american" -*-
\documentclass[10pt]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{listings}

\title{Distributed initialization of ORWL applications}
\author{Jens Gustedt \and Emmanuel Jeanvoine}

\begin{document}
\maketitle

\section{Problem statements}
To launch a distributed ORWL application, two points must be
addressed. The first one is related to the connection between the
distributed ORWL tasks and the second one is related to the
initialization of the locks.

\subsection{Connection}
The distributed applications considered here are the applications
composed of several tasks that have data dependencies. With ORWL, the
remote data access is performed through \textit{orwl\_mirror} objects
that represent an object locally and that must be connected either to a local or to a distant
location that is accessible by others. To allow the distant connections, the tasks must know the
physical location of other tasks. If the application is launched with
a static scheme (aka the port number of the servers is specified at
the startup), the solution is trivial. However it is not really
flexible to proceed like that and it is more convenient to let the
servers find a free port. The problem in this case is to know for a
task the port number of the server of a distant task.

\subsection{Data Dependency}
To keep the liveness and equity properties in ORWL applications, it is
crucial to ensure that the insertion of the lock handles into the FIFO
queues of the locations is
performed correctly. The typical workflow is as follows. The tasks
manage local data that are accessed though write locks and these tasks
also access to distant data through read locks.

To associate a lock handle to a physical location, a read or write
request (depending on the access kind) must be posted. In several
communication patterns, the request post order is important and must
be respected. For instance, a write request from a local task must be
posted before read requests from the distant tasks.

\section{Concept of the solution provided}
For both initialization tasks, connection and lock insertion, we offer
different levels of granularity that can be used by an
application. The principal concept to identify different application
tasks and their locations is a so-called \emph{address book}.

Used in its easiest form, each task and its corresponding location can
be identified with an entry in such an address book. This provides an
access that is similar to a global communicator as for MPI.

Lock and data dependency information can be provided directly by the
application. This can e.g be done through index calculations, e.g a
task representing a matrix block with indices $(x,y)$ would compute
the index of a ``neighboring'' task by adding or subtracting $1$ in
any of the coordinates and insert a read request on the location of
that task.

For other application with more irregular dependency patterns a
connection graph can be used.

\section{Address book}
\label{sec:ab}
The address book contains the information required to establish a
communication between any task (eventually several tasks can be
launched in the same \texttt{orwl\_server}). Basically it is a pair
composed of a global task rank and an \texttt{orwl\_endpoint}.

At the startup, each server generates a file containing the
information for every task it hosts. This information is called
\textit{local address book}. Then, the servers are blocked until an
external process gathers all the information and broadcasts the
\textit{global address book}.

For the easiest case, the remote locations between tasks may now be
used to place requests between different tasks:

\lstset{language=C,%
  basicstyle=\scriptsize,%
  %frameround=fttt,%
  frame=single,%
  showstringspaces=true}
\begin{lstlisting}
  orwl_mynum = IDofThisTask;
  ...
  orwl_handle2 here = ORWL_HANDLE2_INITIALIZER;
  orwl_handle2 there = ORWL_HANDLE2_INITIALIZER;
  /* Take the local lock in write mode */
  orwl_write_request2_grouped(&here);
  /* Take a lock on the main task in read mode */
  orwl_read_request2_grouped(&there, distantId);
  orwl_schedule_grouped();
\end{lstlisting}

First this fixes the ID of the current task by assigning
\texttt{orwl\_mynum} the appropriate value. This assignment should
always be done at the beginning of the execution of the task.

Then we create two handles, \texttt{here} and \texttt{there}, and
insert them into queues of two different locations. So the first
request (which is for iterative, exclusive reading and writing)
inserts the handle in the queue of its ``own'' location.

The second inserts an iterative request for reading in the queue of
another task, identified by the value \texttt{distantId}. Programmed
like that, there is no need to know on which host a task is executed
and inside which process it runs: the address book is used to resolve
all this behind the scenes.

The call of \texttt{orwl\_schedule\_grouped} then synchronizes the
request insertion with all other tasks. Using a sequence as given here
for all tasks guarantees that the lock insertion is done properly such
that a deadlock-free and fair iterative processing can be performed
between all processes.

The reference manual for the grouped request shows you how additional
arguments to these functions can be used to ascertain that tasks are
inserted according to a given ordering, and how this calling sequence
can be a bit optimized.

\section{Connection graph}\label{subsec:connection-graph}
The connection graph is a directed graph where each vertex stands for
a task and each edge stands for a data access. Thanks to this data
structure, each task can know on which distant tasks the data accesses
must be performed.

Furthermore, to ensure the liveness property in ORWL, the graph is
also used for the initialization of the applications. Actually, the
graph is also colored with the following properties: two vertices that
are connected cannot have the same color. The number of color in the
graph is kept low, such that good parallel execution can be
expected.

With that information, an initialization process can ensure that a
given task can perform some operations before its neighbors. This is
required when the read/write requests are posted to ensure a
serialization of some requests in sub group of connected tasks.

\subsection{How to create a graph file?}
The graph files required to run the distributed applications are based
on the DOT language.

Here is an example of a fully connected graph with 4 vertices:
\begin{verbatim}
digraph G {
0 -> 1
0 -> 2
0 -> 3
1 -> 0
1 -> 2
1 -> 3
2 -> 0
2 -> 1
2 -> 3
3 -> 0
3 -> 1
3 -> 2
0 [color="0", label="0-0"]
1 [color="1", label="1-1"]
2 [color="2", label="2-2"]
3 [color="3", label="3-3"]
}
\end{verbatim}
The graph file actually contains three informations:
\begin{itemize}
\item the connections between the vertices ;
\item a color for each vertex ;
\item a label for each vertex.
\end{itemize}

The label field does not necessarily contains significant information,
but it can be used for some kind of applications.
% For instance, the
% Livermoore Kernel 23 application uses this field to specify on which
% main task belong a sub task and which is its position with respect to
% the main task.

Writing a graph quickly becomes tedious as the number of tasks
increased. Thus it is recommended to create a script to achieve that
or to use a feature of the grouped requests as mentioned above to also
produce the dependency graph as a side effect. If the environment
variable \texttt{ORWL\_OUTGRAPH} is set to a file name without
extension, say \texttt{group\_graph}, the initialization sequence as
presented in Section~\ref{sec:ab} would produce files named
\texttt{group\_graph\_XXX.dot}. These then can be used
\begin{verbatim}
$> scripts/scripts/dep_graph.pl group_graph_* > result-graph.dot
$> scripts/layout-graph.sh result-graph.dot
\end{verbatim}
to produce a PDF file \texttt{result-graph.dot} to inspect the
dependency pattern of your application and to feed back some priority
information for the ordering of the requests into your application.

For irregular dependencies between tasks a program designer would have
to produce the dependency directly. Either inside the program through
calls to the grouped requests as above or by writing a script that
generates the dependency graph.

Two scripts are provided as examples for generating the graph
associated to the ORWL benchmarks:\\
\texttt{gen\_graph\_bench\_synthetic.rb} and
\texttt{gen\_graph\_bench\_kernel23.pl}. These scripts generate
uncolored graph that must be colored. To achieve this coloring scripts
are provided in the ORWL distribution (\texttt{script/coloreo.pl} and
\texttt{script/parallel\_coloreo.pl}).

For instance, to create a graph for the
\texttt{orwl\_benchmark\_kernel23} application, and for a $4\times 4$ problem,
one can generate it like that:
\begin{verbatim}
$> scripts/gen_graph_bench_kernel23.pl 4 4 > uncolored_graph.tmp
$> scripts/coloreo.pl -n uncolored_graph.tmp > graph_4_4.dot
$> rm uncolored_graph.tmp
\end{verbatim}

\section{Helpers library}
A set of functions provides the developers with some facilities to
run distributed ORWL applications.

\subsection{Address book and graph data structures initialization}
In order to create a global address book and to load the graph into
memory, users can use the following function:
\begin{lstlisting}
bool
orwl_wait_and_load_init_files(size_t nb_local_tasks,
                              size_t list_local_tasks[nb_id],
                              const char *local_ab_file,
                              const char *global_ab_file,
                              const char *graph_filename,
                              orwl_server *serv);
\end{lstlisting}
\begin{itemize}
\item \texttt{nb\_local\_tasks} is the number of tasks running on the
  server that is indicated by the parameter \texttt{serv}.
\item \texttt{list\_local\_tasks} is an array containing the task rank
  of the local tasks. By this mechanism the application can regroup
  the tasks arbitrarily between different servers and is not forced to
  allocate them continuously.
\item \texttt{global\_ab\_file} is the expected path to the global
  address book. The call will block until this file is produced. The
  global number of tasks will be determined from the contents of this
  file. If this is omitted, the contents of the environment variable
  \texttt{ORWL\_GLOBAL\_AG} is used.
\item \texttt{local\_ab\_file} is the path to the partial address book
  that will be produced by this call. If this is omitted, the contents
  of the environment variable \texttt{ORWL\_LOCAL\_AG} is used.
\item \texttt{graph\_file} is the path to the dot file containing the
  graph description, if any. This parameter can be \texttt{0} or
  omitted and no graph file is used.
\item \texttt{serv} identifies the server thread for which all this
  mechanism is applied. If omitted (currently probably most of the use
  cases) it defaults to the default server thread for this ORWL run.
\end{itemize}

This function should be placed in the main program, before launching
the task threads. It blocks the application until a kick-off is
given. Once called, this function produces a partial address book
containing the information of the tasks launched on the current server
and writes it in the \texttt{local\_ab\_file} file. The idea is to
collect the \texttt{local\_ab\_file} files on all the nodes from an
external application that concatenates it to a global address
book. Then the external application has to send the global address
book on the nodes (the expected file name is
\texttt{global\_ab\_file}). Finally, once the global address book is
copied on all the nodes, the kick-off can be performed by the external
application by deleting \texttt{local\_ab\_file} on all the nodes.

If provided, this function also expects that the \texttt{graph\_file} file has been
copied on all the nodes by the external application once the kick-off
is given.

After the kick-off, the function loads the address book and the graph
into memory and returns \texttt{true} if everything went well.

\paragraph{Warning:} \texttt{local\_ab\_file} must not point to a
shared space amongs all the nodes involved in the
application. Typically it should points to a file in the \texttt{/tmp}
directory.

\subsection{Locks initialization}
\emph{The method of initialization described here is now
  deprecated. Please use the mechanism described in
  Section~\ref{sec:ab}.}

To ensure that some locks initializations are performed in the
correct order (based on the colored graph,
cf. section~\ref{subsec:connection-graph}), two functions are
provided. The two following functions must be called from the task
threads and the server must be launched in blocked mode beforehand.

\begin{verbatim}
bool
orwl_wait_to_initialize_locks(size_t task)
\end{verbatim}
\begin{itemize}
\item \texttt{task} is the rank of the task thread
\end{itemize}

This function blocks a task thread until the corresponding task is
able to initialize its locks. This \textit{ability} is based on graph
coloring and on the policy to block a task until all its neighbors in
the undirected graph with a lower color are initialized.

\begin{verbatim}
bool
orwl_wait_to_start(size_t task,
                   size_t nb_local_tasks)
\end{verbatim}
\begin{itemize}
\item \texttt{task} is the rank of the task thread
\item \texttt{nb\_local\_tasks} is the number of tasks running on the
  local server
\end{itemize}

This function blocks a task thread until the neighbors (in the
undirected graph) of the corresponding task have initialized their
locks. Furthermore, once all the local task of a server have passed
this function, the server is unblocked and starts to serve the remote
requests.

The typical workflow is as
follows. \texttt{orwl\_wait\_to\_initialize\_locks()} is called and
blocks until the neighbors with a lower color are initialized
(aka have called \texttt{orwl\_wait\_to\_start()}). Then the task
thread performs the locks initialization. Then it calls
\texttt{orwl\_wait\_to\_start()}. This method allows to ensure that
any task cannot start before all its neighbors are initialized.

\paragraph{Warning:}this kind of initialization works only when the
graph does not contain cycle of length 2.

\subsection{Global barrier}
The initialization functions use a global barrier to achieve the
initialization. This global barrier uses all the tasks that are listed
in the corresponding address book, see Section~\ref{sec:ab}. Calling
it with less tasks than that will deadlock the application.

\begin{verbatim}
void
orwl_global_barrier_init(size_t task)
\end{verbatim}
\begin{itemize}
\item \texttt{task} is the task number
\end{itemize}
This function re-initializes a global barrier, normally this should
not be used by an application. If used, this must be performed on all
the tasks simultaneously.

\begin{verbatim}
int
orwl_global_barrier_wait(size_t task,
                         orwl_server *srv,
                         rand48_t * seed);
\end{verbatim}
\begin{itemize}
\item \texttt{task} is the number of the invoking task. It defaults to
  the correct task ID if that has been assigned to
  \texttt{orwl\_mynum} as indicated in Section~\ref{sec:ab}.
\item \texttt{srv} and \texttt{seed} are for more sophisticated use
  cases and default to reasonable values if omitted.
\end{itemize}
This function waits until all the tasks enter in the barrier. This
must be performed on all the tasks. Also, it returns 0 on all the
tasks but one. That designated task then may perform some global
cleanup operation, if necessary.

\section{Distributed run}
A distributed ORWL application has its tasks distributed over several
nodes. To ease the multi-nodes execution, a script is provided in the
ORWL distribution that aims at:
\begin{itemize}
\item spawning all the processes (eventually composed of several
  tasks) on the nodes
\item cooperating with the helpers function to :
\begin{itemize}
  \item broadcast the graph file
  \item gather the local address books and broadcast the global one
  \item remove the local address books to kick-off the application
\end{itemize}
\item gathering the outputs of all the processes
\end{itemize}

This script can be found in \texttt{script/deploy\_orwl.rb}. It relies
heavily on the parallel launcher
Taktuk\footnote{http://taktuk.gforge.inria.fr/}. So Taktuk must be
installed at least on the frontend of the cluster. Furthermore, it is
supposed to work with the OAR batch scheduler and thus, it uses
\texttt{oarsh} instead of the classical connector. If it supposed to
work with another batch scheduler, the definition of the constant
\texttt{CONNECTOR} must be adapted in the script with respect to the
batch scheduler used.

To launch an application, \texttt{script/deploy\_orwl.rb} must be used
as follows:
\begin{verbatim}
ruby $ORWL_HOME/script/deploy_orwl.rb $MACHINE_FILE \
                                      $OUTPUT_PATH \
                                      $SCRIPT \
                                      $SCRIPT_ARGS
\end{verbatim}
\begin{itemize}
\item \texttt{\$ORWL\_HOME} is the path to the ORWL distribution
\item \texttt{\$MACHINE\_FILE} is the path to node file, containing
  one node per line
\item \texttt{\$OUTPUT\_PATH} is the path to the directory that will
  be used to store the output of each ORWL process
\item \texttt{\$SCRIPT} is the path to an application specific script
  that really spawn an  ORWL process. This script is supposed to take
  4 parameters that are, in this order: 
  \begin{enumerate}
  \item the global address book file
  \item the local address book file
  \item the rank of the process
  \item the output path
  \end{enumerate}
\item \texttt{\$SCRIPT\_ARGS} eventually contains additional
  parameters that are directly passed to \texttt{\$SCRIPT}, just after
  the 4 parameters above
\end{itemize}

Some application specific scripts are provided in the distribution:
\begin{itemize}
\item \texttt{scripts/launchers/run\_benchmark\_synthetic.rb}: it can
  be used to launch the \texttt{orwl\_benchmark\_synthetic}
  application (provided in the \texttt{tests/} directory).
\item \texttt{scripts/launchers/run\_benchmark\_kernel23.rb}: it can be
  used to launch the \texttt{orwl\_benchmark\_kernel23} application
  (provided in the \texttt{tests/} directory).
\item \texttt{scripts/launchers/run\_gpupricer.rb}: it can be used to
  launch the full ORWL version of the GPU pricer (not privided in the
  ORWL ditribution).
\end{itemize}

\end{document}