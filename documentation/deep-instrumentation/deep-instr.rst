
----------------------------
Deep instrumentation on ORWL
----------------------------

Introduction
============

Deep instrumentation is a way to instrument the library to get the CPU time used
inside the library when running a program. Please keep in mind this is CPU time
and not wall clock time or any other time. You can think of it as running your
program with the "time" command and add the user + system time, but for the
library instead of the whole program. It will print the total CPU time used by
the program, the total CPU time used by the library and then give you the
percentage used by the library.

It is called "deep instrumentation" because it is kind of heavy instrumentation.
When you compile the library with instrumentation, besides we try our best to
keep the overhead low, 2 system calls are added. And, like it or not, that is
some overhead.

An important goal is to avoid any overhead when compiling without deep
instrumentation. And that was achieved pretty well, it adds ZERO overhead when
compiling without deep instrumentation. All calls are expanded to P99_NOP that
are then optimized out by the compiler.

The main difference with using regular profilers is that they usually take
samples. With deep instrumentation we use an "exact" approach (although not sure
it's a big win over regular profilers) and should be possible to extend to know
the CPU time spent on different subsystems by just saving the elapsed time on
different (per subsystem) variables.


How is it done ?
================

There are different parts in the library that we need to focus: inline functions,
not inline functions and library internal threads.

For inline functions we add some code at the begining and at the end of every
function, and we are set with it (more details in the specific section). For not
inline functions we create a small shared library we use with "LD_PRELOAD" and
add our wrapper there. And for library internal threads, as a thread pool
implementation is used, we add some instrumentation after/before running the
routine for that thread.

Our best friend is the "clock_gettime()" syscall that we use for instrumenting
all 3 parts of the library. The main simplified idea is that we use it to get
the CPU time (user + system CPU time) used by the thread before calling the
library, we save that, and then just before exiting the library we get the time
again, and get the difference. That should be (with some overhead for the
"clock_gettime()" call) the time used by the library.

We also handle correctly the case where an instrumented library call ends up
calling (directly or inderectly) an other library functions that we need to
instrument too. To handle this, we just keep a counter of how many "levels"
inside the library we are and only the outermost does the accounting.


"clock_gettime()" meassurement of itself
----------------------------------------

The "clock_gettime()" syscall counts a fraction of it's usage when returning the
current used CPU time. Let :math:`x` be the part of "clock_gettime()" that is
accounted into the returned usage and let :math:`y` the part that is not. Then,
the overhead for a "clock_gettime()" call is :math:`x+y`.

We will use that "clock_gettime()" meassures a part of itself when returning in
the next sections (and always the same part, it doesn't matter how much as long
as it is always the same).


Instrumenting inline functions
------------------------------

To instrument an inline function we just add some code at the beginning and at
the end of it. In fact, we add a function call "_start()" at the beginning and
an "_stop()" at the end. So you can think of an instrumented inline function
looks something like::

        _start()
        ... routine ...
        _stop()

Let :math:`x` be the part till the first call to "clock_gettime()" (inside
"_start()") includes itself and let :math:`y` be the part after that until the
library routine starts.

If we consider "_start()" and "_stop()" to have the same overhead, then we can
also split "_stop()" as :math:`x+y`. And keeping in mind that we calculate the
time inside the library as the difference between the time we got when calling
"clock_gettime()" in "_start()" and the time we called "clock_gettime()" at
"_stop()", then the time inside the library for this call is
:math:`y + T_{routine} + x`

If we look at the user time, it is :math:`T_{user} + x + y` where the :math:`x`
comes from the "_start()" call and the :math:`y` comes from "_stop()".

If we generalize this and assume :math:`N` calls to inline function while
running a program, then the library time we calculate is:

.. math:: \sum_{i=1}^{N} T_{i} + x + y = T_{L1} + N (x+y) = T_{L1} + \delta_{1}

where :math:`T_{L1} = \sum_{i=1}^{N} T_{i}` and :math:`\delta_{1} = N (x+y)`


Instrumenting not inline functions
----------------------------------

To instrument not inline functions we create a small shared library and use it
with LD_PRELOAD to overload library functions. We use this because it's easier
compared to adding code to **every** function we want to overload (as we do with
inline functions) and is much more easy to maintain. So, as long as we can, we
do prefer to do it this way.

The reasoning for the library time spent on a call to a non-inline instrumented
functions is pretty similar to an inline, with just some small differences. For
example, instead of having the routing code inline, we need to make a call to
the actual function. So, it looks like::

        _start()
        .. call to real function ..
        _stop()

Assuming the overhead of a function call is negible (it was not an inline
function the one we are wrapping) and using the same reasoning we end up with a
very similar result as with inline functions:

.. math:: \sum_{i=1}^{M} T_{i} + x + y = T_{L2} + M (x+y) = T_{L2} + \delta_{2}

where :math:`M` is the number of calls to instrumented non inline functions,
:math:`T_{L2} = \sum_{i=1}^{M} T_{i}` and :math:`\delta_{2} = M (x+y)`

Nothing suprising in that aspect.

But to be able to have a pointer to the original function and that stuff, we
needed to overload "orwl_init()" and look for all the function pointers there.
So we added some overhead to "orwl_init()" and we need to know how much it is.
As this is only run once in the whole program, we just instrumented our
"orwl_init()" wrapper with calls to "clock_gettime()" to get the overhead. And
the two calls to "clock_gettime()" are (by far) less than 1us (this should be
revisited on each system, but seems pretty safe)

Then, the library also gets the overhead for the "orwl_init()" and we call it
:math:`\gamma` from now on.


Instrumenting library internal threads
--------------------------------------

The library has a thread pool implementation that is used for library internal
threads and for user tasks. So a given thread blocks until there is something to
do, do it, and blocks again. We just added the "_start()" and "_stop()"
surrounding the routine it actually executes to get the stats. It looks like
something like this::

        _thread_start()
        .. call to routing ..
        _thread_stop()

The only difference with the "_start()" and "_stop()" functions used before is
that this checks if the thread is an internal thread or not (it might be a user
thread, used for a user task). If it's not a internal thread, then this calls
just do nothing.

So, using the same reasoning as before, we arrive to a very similar formula for
the library time as we arrive for inline functions:

.. math:: \sum_{i=1}^{T} T_{i} + x + y = T_{L3} + T (x+y) = T_{L3} + \delta_{3}

where :math:`T` is the number of library internal threads, 
:math:`T_{L3} = \sum_{i=1}^{T} T_{i}` and :math:`\delta_{3} = T (x+y)`


Getting rid of the overhead
---------------------------

*NOTE*: Trying to estimate the overhead to get more accurate results is not
always possible. For example, when using spinlocks (as ORWL does), the CPU time
can be really affected because if a thread is interrupted (for the syscall) or
if some threads take more time to be unscheduled and the thread that needs to
release the lock takes more time to be scheduled (and there is an other thread
spinning for that lock), the CPU time is affected. For these reasons is that we
are not using the estimation of the overhead in ORWL now. The total CPU time
with instrumentation and without instrumentation is pretty much the same, so the
overhead should be really low (in CPU time). Also, we checked with perf that we
don't add branch mispredictions. So it really seems it adds really low overhead.

If we get the total CPU time for a program running without instrumentation we
get the sum of the time in the library plus the time in the user code. That is,
we get:

.. math:: T_{L} + T_{U}

If we mix the results we get from the differents ways of instrumenting and let
:math:`\delta = \delta_{1} + \delta{2} + \delta{3}` and
:math:`T_{L} = T_{L1} + T_{L2} + T_{L3}` then the time inside the library we calculate is:

.. math:: T_{L} + \delta + \gamma

And the time in user code we calculate is:

.. math:: T_{U} + \delta


Therefore, the total CPU time for a program running with instrumentation is:

.. math:: T_{U} + T_{L} + 2 \delta + \gamma

As :math:`\gamma` is estimated by the library, we then know how much

.. math:: T_{U} + T_{L} + 2 \delta

is, and subtracting the time the program without instrumentation took we get:

.. math:: 2 \delta

And though, :math:`\delta`

Then, the library time we calculated as:

.. math:: T_{L} + \delta + \gamma

if we substract :math:`\gamma` and :math:`\delta` and we get the real library
cpu time. Then also we know the value of :math:`T_{U}` and can calculate a
percentage of the CPU time spent in the library.


A note to extending the meassurements
-------------------------------------

As you can see in the past section, getting rid of the overhead is quite easy.
That is because we always surround the code we want to instrument with
"_start()" and "_stop()" functions and that forces that we always will have a
:math:`2 \delta` on the total time.

If you need to add meassurements on other parts of the code, keep in mind that
doing this kind of thing is easy and should only add a :math:`\delta_{4}` to the
formula. That will be transformed in a :math:`2 \delta` and the reasoning should
be the same. If you can avoid having a :math:`\gamma` as we do now have, things
will be way easier.

.. Some implementations details
.. ============================
..
.. thread local vars, atomic globals
.. maybe add easily to do "per subsystem"
.. if nothing relevant inside inline, should we use regular profiling tools to
.. profile ?
..
..
.. Some limitations of the current approach
.. ========================================
..
..
.. .. maybe put the same limitations are already on the code (in a big comment) ?
..

Requirements from the OS and libc
=================================

This technique uses clock_gettime() with CLOCK_THREAD_CPUTIME_ID to get the CPU
usage of the current thread. If this syscall on some platforms gives bogus
results on some conditions, then deep instrumentation will give bogus results on
those conditions too.

For example, older versions of glibc (< 2.4) implemented the
CLOCK_THREAD_CPUTIME_ID clock on many platforms using timer registers from the
CPUs (TSC on i386, AR.ITC on Itanium).  These registers may differ between CPUs
and as a consequence this clock may return bogus results if a process/thread is
migrated to another CPU.

Since  glibc 2.4, the wrapper functions for the system call avoid the
abovementioned problems by employing the kernel implementation of
CLOCK_THREAD_CPUTIME_ID, on systems that provide such an implementation
(i.e.,  Linux 2.6.12 and later).

So, if you are using Linux and glibc, you need Linux >= 2.6.12 and glibc >= 2.4.
If you are using something else, you should check that the clock_gettime()
syscall works as expected on that platform.

Don't panic if your manpage for clock_gettime() on Linux says the note on SMP
systems still applies, as the note is outdated. We have filled a bug:
https://bugzilla.kernel.org/show_bug.cgi?id=60602 and submitted a patch to fix
it (after some investigation to know that it was indeed outdated). The fix is
already commited upstream as commit 78638aa, but there is still no release that
includes this patch.


..
.. What have we tried before
.. =========================
..
.. TODO

.. basically all I put in my presentation to Jens might be useful ? Or at least
.. the final research abouth clock_gettime() and why is it fine to use on SMP
.. systems ?


.. .. math:: n_{\mathrm{offset}} = \sum_{k=0}^{N-1} s_k n_k
