#!/bin/sh -f
#  This may look like complete nonsense, but it really is -*- mode: perl; coding: utf-8 -*-
eval 'exec perl -wS -x $0 ${1+"$@"}'
if 0;               #### for this magic, see findSvnAuthors ####
#!perl
#
#
# Except for parts copied from previous work and as explicitly stated below,
# the author and copyright holder for this work is
# all rights reserved,  2012 Jens Gustedt, INRIA, France
#
# This file is part of the ORWL project. You received this file as as
# part of a confidential agreement and you may generally not
# redistribute it and/or modify it, unless under the terms as given in
# the file LICENSE.  It is distributed without any warranty; without
# even the implied warranty of merchantability or fitness for a
# particular purpose.
#

## Analyze the output of the timing routines
##
## without option this just collects entries in different files that
## have the same name and recomputes the statistics
##
## with option --dot the output is a tree in dot format
##
## with option --tm the output is a csv file that is suitable for an
## interpretation by the R module treemap. If you have stored the csv
## in "dep.txt" you can use it inside R with something like
##
## > library(treemap)
## > data <- read.csv("dep.txt", header=TRUE)
## > tmPlot(data, index=c("id0", "id1", "id2", "id3", "id4", "id5"), vSize="n", vColor="t", type="linked", algorithm="squarified")
## > tmPlot(data[data$id0=="orwl_send_remote",], index=c("id0", "id1", "id2", "id3", "id4", "id5"), vSize="n", vColor="t", type="linked", algorithm="squarified")
## > tmPlot(data[data$id0=="orwl_send_remote",], index=c("id0", "id1", "id2", "id3", "id4", "id5"), vSize="t", type="linked", algorithm="squarified")
## > tmPlot(data[data$id0=="orwl_send_remote",], index=c("id0", "id1", "id2", "id3", "id4", "id5"), vSize="t", type="linked", algorithm="squarified")
## > tmPlot(data[data$id0=="orwl_server_start",], index=c("id0", "id1", "id2", "id3", "id4", "id5"), vSize="t", type="linked", algorithm="squarified")
##
## Here the list of "idX" may be longer if you have a deeper nesting
## of calling contexts.



use English;
use Getopt::Long;
use strict;

sub compute($);
sub printLine($);
sub help();

my %table;
my %edge;
my %node;

my %factor = (
    "y" => 1.E-24,
    "z" => 1.E-21,
    "a" => 1.E-18,
    "f" => 1.E-15,
    "p" => 1.E-12,
    "n" => 1.E-9,
    "u" => 1.E-6,
    "m" => 1.E-3,
    "k" => 1.E3,
    "M" => 1.E6,
    "G" => 1.E9,
    "T" => 1.E12,
    "P" => 1.E15,
    "E" => 1.E18,
    "Z" => 1.E21,
    "Y" => 1.E24,
    "Ki" => (1 << 10),
    "Mi" => (1 << 20),
    "Gi" => (1 << 30),
    "Ti" => (1 << 40),
    "Pi" => (1 << 50),
    "Ei" => (1 << 60),
    # Don't assume integers with more than 6
    "Zi" => (1 << 60) * 1024E0,
    "Yi" => (1 << 60) * 1024E0 * 1024E0,
    );

my $factor = join("|", keys %factor);
my $factorRE = qr/$factor/;

my $numberRE = qr/[-+[:digit:].E]+/;

my @tag;
my $help;
my $dot;
my $tm;
my $subs;
my $rel;

# allow for multiple tags to be entered either by repeating the option
# or by providing comma or colon separated values

my $result = GetOptions(
    "tag=s" => \@tag,
    "help!" => \$help,
    "dot!" => \$dot,
    "tm!" => \$tm,
    "subs!" => \$subs,
    "rel!" => \$rel,
    );

help() if (defined($help));

@tag = split(/[:,]/,join(',',@tag));

my $events = 0;
my $total = 0;

foreach my $line (<>) {
    if ($line =~ m/^TIMING:[^#]+$/) {
        my ($name, $vals) = $line =~ m/^TIMING:\s*(\S+)\s+(.+)/o;
        $name =~ s/^([a-z0-1_]+)>$/$1/;
        $name =~ s/^([a-z0-1_]+)>>/$1>/;

        # split into fields, each a number optionally followed by a
        # factor indicator
        my @vals = ($vals =~ m|($numberRE *(?:$factorRE)?)[^[:digit:]]*|go);

        # merge number and factor
        foreach my $val (@vals) {
            my ($base, $fact) = $val =~ m/($numberRE) *($factorRE)?/;
            $base *= $factor{$fact} if (defined($fact) && length($fact));
            $val = $base;
        }

        # interpret the fields
        my ($n, $t, $tn, $dev, $c) = @vals;

        # account for the total number of measurements
        $events += $n;

        # account for the top level time
        if ($name =~ m/^[^>]*$/) {
            $total += $t;
        }

        # reconstruct the square of the time $t
        my $t2 = ($dev * $dev + $t * $t) / $n;
        @vals = ($n, $t, $t2);
        push(@vals, $c) if (defined($c));

        # store or add up in the corresponding hash table entry
        if (!defined($table{$name})) {
            $table{$name} = \@vals;
        } else {
            for (my $i = 0; $i < @{$table{$name}}; ++$i) {
                @{$table{$name}}[$i] += $vals[$i];
            }
        }
    }
}

foreach my $key (keys %table) {
    my @val = $key =~ m/^orwl_calibrate>([a-zA-Z_0-9>]+)$/;
    if (@val) {
        $table{"_calibrate>".$val[0]} = $table{$key};
        delete($table{$key});
    }
}

sub deduceTimer($$$) {
    my ($kind, $f, $X) = @_;
    my $fact = $factor{$f};
    my $n = @{$table{"_calibrate>$kind>$f$X"}}[0];
    my $t1 = @{$table{"_calibrate>$kind>$f$X"}}[1];
    my $t2 = @{$table{"_calibrate>$kind>2$f$X"}}[1];
    my $t = $t2;
    $t -= $t1;
    my @val = ($n, 2.0*$t1 - $t2, 0, 0, 0);
    $table{"_calibrate>$kind>$f$X.timer"} = \@val;
    $n *= $fact;
    $table{"_calibrate>$kind>$X"} = $table{"_calibrate>$kind>$f$X"};
    @{$table{"_calibrate>$kind>$X"}}[0] = $n;
    @{$table{"_calibrate>$kind>$X"}}[1] = $t;
    @{$table{"_calibrate>$kind>$X"}}[2] = 0;
    delete($table{"_calibrate>$kind>$f$X"});
    delete($table{"_calibrate>$kind>2$f$X"});
}

my %primitive = (
    "Iinc" => 1,
    "Finc" => 1,
    );

my %dependant = (
    "sqrt" => "Fadd",
    );

sub deduceOps($$) {
    my ($kind, $X) = @_;
    my $n = @{$table{"_calibrate>$kind>$X"}}[0];
    my $t = @{$table{"_calibrate>$kind>$X"}}[1];
    if (!defined($primitive{$X})) {
        my $dep =
            (defined($dependant{$X}))
            ? "$dependant{$X}"
            : "Iinc";
        $t -= @{$table{"_calibrate>instruction>$dep"}}[1];
    }
    $t /= $n;
    my @val = ( int(1.0 / $t), 1, 0, 0, 0);
    $table{"_calibrate>$kind>$X.ops"} = \@val;
}

if (defined($table{"_calibrate>timer1000"})) {
    my $n = @{$table{"_calibrate>timer1000>timer0>timer1"}}[0];
    my $t1 = @{$table{"_calibrate>timer1000>timer0>timer1"}}[1];
    my $t2 = @{$table{"_calibrate>timer1000>timer0>timer1>timer2"}}[1];
    my $t = (($t1 - $t2) / $n) * $events;
    my @val = ($events, $t, 0, 0, 0);
    $table{"_calibrate>timer"} = \@val;
    delete($table{"_calibrate>timer1000"});
    delete($table{"_calibrate>timer1000>timer0"});
    delete($table{"_calibrate>timer1000>timer0>timer1"});
    delete($table{"_calibrate>timer1000>timer0>timer1>timer2"});
    delete($table{"_calibrate>timer1000>timer0>timer1>timer2>timer3"});
    #
    my @timers;
    foreach my $key (keys %table) {
        my @val = $key =~ m/^_calibrate>([a-zA-Z_0-9]+)>2([a-zA-Z])([a-zA-Z_0-9]+)$/;
        if (@val) {
            push(@timers, \@val);
            #deduceTimer($kind, $f, $X);
        }
    }
    foreach my $timer (@timers) {
        my ($kind, $f, $X) = @{$timer};
        deduceTimer($kind, $f, $X);
    }
    foreach my $timer (@timers) {
        my ($kind, $f, $X) = @{$timer};
        deduceOps($kind, $X);
    }
}


if (defined($subs)) {
    my @keys = sort { length($a) > length($b) } keys %table;
    foreach my $key (@keys) {
        my @parent = split(">", $key);
        pop(@parent);
        if (@parent) {
            my $parent = join(">", @parent);
            my ($n, $t, $mu, $dev, $c) = compute($key);
            if (defined($table{$parent})) {
                @{$table{$parent}}[1] -= $t;
            }
        }
    }
}

if (defined($rel)) {
    while (1) {
        my %fill;
        my @keys = sort { length($a) > length($b) } keys %table;
        foreach my $key (@keys) {
            my @parent = split(">", $key);
            pop(@parent);
            if (@parent) {
                my $parent = join(">", @parent);
                my $val = @{$table{$key}}[1];
                if (!defined($table{$parent})) {
                    if (!defined($fill{$parent})) {
                        my @val = (0, $val, 0, 0, 0);
                        $fill{$parent} = \@val;
                    } else {
                        @{$fill{$parent}}[1] += $val;
                    }
                }
            }
        }
        last if (! %fill);
        foreach my $key (keys %fill) {
            $table{$key} = $fill{$key};
            # account for the top level time
            if ($key =~ m/^[^>]*$/) {
                $total += @{$table{$key}}[1];
            }
        }
    }
}

my $maxdepth = 10;

if (defined($dot)) {
    print "digraph \"hoi\" {\n";
} elsif (defined($tm)) {
    print join(",", (map { "id".$_ } (0..$maxdepth-1)), "n", "t", "tn", "def", "m\n");
} else {
    print "id\tn\tt\tt/n\tdev\tc\tc/n\tcn/t\n";
}

if (@tag) {
    foreach my $key (@tag) {
        printLine($key);
    }
} else {
    foreach my $key (sort keys %table) {
        printLine($key);
    }
}

if (defined($dot)) {
    foreach my $node (keys %node) {
        print "\"$node\" [ rank=\"$node{$node}\"]\n"
    }
    print "}\n";
}

exit 0;

sub printLine($) {
    my $key = shift;
    my ($n, $t, $mu, $dev, $c) = compute($key);
    my @key = split('>', $key);
    if (defined($dot)) {
        foreach my $k (@key) {
            $k = "+" if ($k eq "");
        }
        $key = join('>', @key);
        my $last = pop @key;
        my $source;
        if ($last eq $key) {
            $source = "";
        } else {
            $source = join(">", @key);
        }
        my $width = 10.0 * $n / $events;
        my $edge = "$source|$key";
        $edge{$edge} = 1;
        print "\"$source\" -> \"$key\" [ weight=\"$width\" ]";
        print "\n";
        $source = "";
        my $target;
        my $rank = 1;
        foreach my $k (@key) {
            $node{$source} = $rank;
            if (defined($target)) {
                $target .= ">$k";
            } else {
                $target = $k;
            }
            my $edge = "$source|$target";
            if (!defined($edge{$edge})) {
                $edge{$edge} = 1;
                print "\"$source\" -> \"$target\" [ weight=\"$width\" ]";
                print "\n";
            }
            $source = $target;
            ++$rank;
            $node{$target} = $rank;
        }
    } elsif(defined($tm)) {
        push(@key, "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
        print join(",", @key[0...$maxdepth-1], $n, $t, $mu, $dev);
        print ",$c"
            if (defined($c));
        print "\n";
    } else {
        print "$key\t$n\t$t\t$mu\t$dev";
        if (defined($c)) {
            print "\t$c";
            print "\t".($c/$n);
            print "\t".($c/$mu);
        }
        print "\n";
    }
}

sub compute($) {
    my ($key) = @_;
    # interpret the table entries
    my ($n, $t, $t2, $c) = @{$table{$key}};

    if (defined($rel)) {
        my ($parent) = $key =~ m/(.+)>[a-zA-Z0-9_]+$/;
        if (defined($parent) && defined($table{$parent})) {
            my $pt = @{$table{$parent}}[1];
            # print "we have: $parent $pt, $key $t, \n";
            $t /= $pt;
            # print " $t\n";
        } else {
            # print " $t total is $total\n";
            $t /= $total;
        }
        return ($n, $t, 0, 0, 0);
    } else {
        # compute the statistics
        my $mu = $t/$n;
        my $val = ($t2 - $t * $mu);
        my $dev = (($n > 0) && ($val > 1E-15)) ? sqrt( $val / $n) : 0;
        return ($n, $t, $mu, $dev, $c);
    }
}

sub help() {
    print STDERR <<"HELP";
$0 [--tag=TAG0,TAG1,...] file0 file1 ...

Cumulate the timing results of one or several ORWL runs.  If run
without the --tag flag, all timings are written sorted by their key
value, one per line.

With the --tag flag, only the timings that are named are written in
one single line as tab separated values. For each timing four
(sometimes five) values are written: the number of samples, the total
time, the average time and the standard deviation. The optional fifth
value is the resource counter that may have been associated to the
timing.

Other options:

--dot  prints out a dot graph instead of a table
--tm   prints a table suitable for treemap
--subs subtracts the time value of the children from the parent
--rel  outputs the relative time of children compared to their parents
          parents that don't exist are filled with the sum of their children

This script also interprets the calibration loop of the timing
routines, if that was done during this run.

HELP

    exit 0;
}
