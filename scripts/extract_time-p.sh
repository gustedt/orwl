#!/bin/sh -f

if [ -z "${ORWL_EXECDIR}" ]; then
    ORWL_EXECDIR="`dirname $0`"
else
  if [ ! -z "${ORWL_TRANSLATED_HOME}" ]; then
      ORWL_EXECDIR=`echo -n ${ORWL_EXECDIR} | sed "s|^${ORWL_TRANSLATED_HOME}|${HOME}|"`
  fi
fi

sed -n "
    /^\(real\|user\|sys\) /{
    s!\([a-z]*\) \(.*\)!TIMING:	\1	1	\2	\2	0	0	0!
    p
   }
" $* | ${ORWL_EXECDIR}/analyse_timing.pl
