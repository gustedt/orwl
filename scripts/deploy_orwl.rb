#!/usr/bin/ruby
#
# Usage: deploy_orwl.rb output_path remote_script #nb_nodes #nb_procs [script arguments]
#
# Notes:
# This script aims at deploying an application based on ORWL. In particular it manages
# the broadcast of the address book to every nodes and the kick-off of the application.
# It launches the application exec on all the nodes of the machine_file and fetches all
# the outputs in output_path. The script transmits several parameter to the exec
# application (in the following order):
# - path to the expected global address book,
# - path to the local address book,
# - rank,
# - path to the local output,
#
# followed by all the other arguments provided in exec_arguments.
#
# Warning:
# A passwordless connection must be available between all the nodes for the Taktuk
# execution.


require "tempfile"

name = $0

unless ARGV.length >= 4
  puts "Usage:\n"
  puts "       #{name} output_path remote_script #nb_nodes #nb_procs [script arguments]"
  puts "the remote script is called as"
  puts "       remote_script global_address_book local_address_book rank local_output [script arguments]"
  puts "for all processes where"
  puts "  - local_address_book is a filename in /tmp that is unique per process"
  puts "  - global_address_book is a filename in /tmp where the processes will later find the address book"
  puts "  - rank is a number 1 ... #nb_procs, including"
  puts "  - local_output is a filename in /tmp that is unique per process and should receive the output of the process"
  puts ""
  puts "[script arguments] is then any number of additional arguments that are passed to the application"
  puts ""
  puts "The environment variable ORWL_NODEFILE (or OAR_NODEFILE if run under oar) must point to a file that"
  puts "lists all nodes on which this job is to be run. The #nb_nodes first nodes of this list are chosen."
  puts "If #nb_nodes is less than #nb_procs, #nb_procs/#nb_nodes processes are started on each node."
  puts ""
  puts "The environment variable OAR_JOB_ID is used to decide if this is an oar job. If so, oarsh is used"
  puts "to launch the remote processes. Otherwise ssh is used."
  exit
end


if ENV['OAR_JOB_ID'] then
  SSH = "oarsh"
  if ENV['OAR_NODEFILE'] then
    ENV['ORWL_NODEFILE'] = ENV['OAR_NODEFILE']
  else
    puts "We need the name of the oar node file in the environment, OAR_NODEFILE is missing\n"
    exit
  end
else
  SSH = "ssh"
end

CONNECTOR = "#{SSH} -q -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -o PreferredAuthentications=publickey -o BatchMode=yes"

unless ENV['ORWL_NODEFILE']
  puts "We need the name of the node file with the hosts list in the environment, ORWL_NODEFILE is missing\n"
  exit
end

output_path = ARGV.shift
script = ARGV.shift
nbnodes = ARGV.shift
nbproc = ARGV.shift

# Configuration stuffs
GLOBAL_ADDRESS_BOOK = Tempfile.open("orwl_global_ab").path
LOCAL_ADDRESS_BOOK = Tempfile.open("orwl_local_ab").path
LOCAL_OUTPUT = Tempfile.open("orwl_output").path
machine_file = Tempfile.open("orwl_nodefile").path

ENV['ORWL_SECRET'] = "0x" + rand(16**16).to_s(16)

outline = '\'"$rank/$count:$type: $line\n"\''

$taktuk_header = "taktuk -s -o status=#{outline} -o default=#{outline} -c \"#{CONNECTOR}\""

def taktuk(nodes, p)
  system($taktuk_header + nodes + p)
end

system("sort -u $ORWL_NODEFILE --output=#{machine_file}")

nodes = Array.new
IO.readlines(machine_file).each { |line|
  if not (/^$/ =~ line) then
    nodes.push(line.chomp)
  end
}

nodeShort = Array.new
nodeShort = nodes.slice(0,nbnodes.to_i)

folding = nbproc.to_i / nbnodes.to_i
folding = folding < 1 ? 1 : folding

node_list0 = String.new
node_list2 = String.new
nodeShort.each { |m|
  node_list0 += " -m #{m} "
  node_list2 += " -m #{m} " * folding
}

# cleanup for the case that there are left overs from previous runs
system("rm -f #{GLOBAL_ADDRESS_BOOK} #{LOCAL_ADDRESS_BOOK}.*")

# Remove the global and local address books and output files on exit
# on the remote nodes. This is attempted even when killed by a signal.
trap("EXIT") {
  taktuk node_list0, "broadcast exec [ 'rm #{GLOBAL_ADDRESS_BOOK} #{LOCAL_ADDRESS_BOOK}.$TAKTUK_RANK #{LOCAL_OUTPUT}.$TAKTUK_RANK 2> /dev/null || true' ]"
}

renv = String.new
ENV.each { |name,val|
  if (name =~ /^ORWL/ ) then
    renv += "#{name}='#{val}' "
  end
}

# Launch the application in background on all the nodes
taktuk_thread = Thread.new {
  taktuk node_list2, "broadcast exec [ '#{renv} #{script} #{GLOBAL_ADDRESS_BOOK} #{LOCAL_ADDRESS_BOOK}.$TAKTUK_RANK $TAKTUK_RANK #{LOCAL_OUTPUT}.$TAKTUK_RANK #{nbproc} #{ARGV.join(" ")}' ]"
}

# Wait for the production of the local address books
taktuk node_list2, "broadcast exec [ 'while [ ! -r #{LOCAL_ADDRESS_BOOK}.$TAKTUK_RANK ]; do sleep 0.1 ; done ; ' ]"

# Fetch the local address books and build the global address book
taktuk node_list2, "broadcast get [ '#{LOCAL_ADDRESS_BOOK}.$TAKTUK_RANK' ] [ '#{LOCAL_ADDRESS_BOOK}.$rank' ]"

tmp = Tempfile.open("orwl_tmp")
system("cat #{LOCAL_ADDRESS_BOOK}.* > #{tmp.path}")

# Broadcast the global address book
taktuk node_list0, "broadcast put [ #{tmp.path} ] [ #{GLOBAL_ADDRESS_BOOK} ]"

tmp.close

# Remove the local address books to kick-off the application
taktuk node_list2, "broadcast exec [ 'rm #{LOCAL_ADDRESS_BOOK}.$TAKTUK_RANK' ]"

system("rm -f #{tmp.path}")

# Wait for the end of the execution
taktuk_thread.join

# Fetch the outputs
taktuk node_list2, "broadcast get [ '#{LOCAL_OUTPUT}.$TAKTUK_RANK' ] [ '#{output_path}/#{File.basename(LOCAL_OUTPUT)}.$rank' ]"
