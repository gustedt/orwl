#!/bin/sh -f
#  This may look like complete nonsense, but it really is -*- mode: perl; coding: utf-8 -*-
eval 'exec perl -wS -x $0 ${1+"$@"}'
if 0;               #### for this magic, see findSvnAuthors ####
#!perl
#
#
# Except for parts copied from previous work and as explicitly stated below,
# the author and copyright holder for this work is
# all rights reserved,  2012 Jens Gustedt, INRIA, France
#
# This file is part of the ORWL project. You received this file as as
# part of a confidential agreement and you may generally not
# redistribute it and/or modify it, unless under the terms as given in
# the file LICENSE.  It is distributed without any warranty; without
# even the implied warranty of merchantability or fitness for a
# particular purpose.
#

use English;
use Getopt::Long;
use strict;

sub compute($);
sub normalize($);
sub help();

my @tag;
my $help;

# allow for multiple tags to be entered either by repeating the option
# or by providing comma or colon separated values

my $result = GetOptions(
    "tag=s" => \@tag,
    "help!" => \$help,
    );

help() if (defined($help));

@tag = split(/[:,]/,join(',',@tag));

my %tag = map { normalize($_) => 1 } @tag;

#print "#";
$OFS = "\t";
foreach my $tag (@tag) {
    my @head = map { join(":", $tag, $_); } ("n", "t", "tn", "def", "m");
    print "", @head;
}
print "\n";

my @lines;

foreach my $file (@ARGV) {
    my %lines;
    my @line;
    if (open(my $hand, "<$file")) {
        foreach my $line (<$hand>) {
            chomp $line;
            my ($tag, @fields) = split(/[ \t]/, $line);
            normalize($tag);
            if (defined($tag{$tag})) {
                $lines{$tag} = \@fields;
            }
        }
        foreach my $tag (@tag) {
            my $fields = $lines{$tag};
            my @fields;
            if ($fields) {
                @fields = @{$fields};
            } else {
                @fields = (0, 0, 0, 0, 0);
            }
            push(@line, @fields);
        }
    }
    push(@lines, \@line);
}

@lines = (sort {
    my @A = @{$a};
    my @B = @{$b};
    my $ret = ($A[0] <=> $B[0]);
    $ret = ($A[5] <=> $B[5]) if ($ret == 0);
    $ret;
} @lines);

print @{$_}, "\n" foreach @lines;

sub help() {
    print STDERR <<"HELP";
$0 [--tag=TAG0,TAG1,...] file0 file1 ...

Cumulate the timing results of one or several ORWL runs.


Only the timings that are named with the --tag flag are written in one
single line as tab separated values. For each timing five values are
written: the number of samples, the total time, the average time and
the standard deviation. The fifth value is the resource counter that
may have been associated to the timing.

HELP

    exit 0;
}

sub normalize($) {
    my @tags = split(/>/, $_[0]);
    $_[0] = join(">", @tags);
}
