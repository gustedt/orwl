#!/usr/bin/env sh
#
# $1: directory to put the logs.
# $2: number of threads to run
# $3..N: ORWL binary and it's parameters
#
# This is a simple shell script to easily run a multi-threaded version of ORWL.
# The main idea of this script is to remain simple and portable. This has been
# tested on bash, dash and busybox. Busybox is an important target because the
# Intel Xeon Phi has a busybox.

read_fifo_out() {
	# Read until the fifo returns an empty line (usually when closed)
	while true; do
		read -r line <&3

		if [ -z "$line" ]; then
			break;
		fi

		# Add the "\n" separator "read" trims, so $stdout looks exactly
		# the same
		line="$line\n"
		stdout="$stdout$line"
	done
}

if [ $# -lt 3 ]; then
	echo "Usage: $0 <output dir> <number of tasks> <ORWL binary>" >&2
	exit 1
fi

OUTPUT=$1
mkdir -p $OUTPUT
# Tasks enumeration starts from zero to ORWL_TASKS, so substract one
export ORWL_TASKS=$(($2 - 1))
shift 2

# ORWL_SECRET shouldn't be empty, the program will abort if it is. So we put
# something and use the shell $RANDOM. If the shell doesn't support this,
# $RANDOM just will be empty and the secret won't be a good one. But this
# shouldn't be an issue when running in controlled environments
export ORWL_SECRET="0x112$RANDOM"
export ORWL_NTASKS=$ORWL_TASKS
export ORWL_VERBOSE=1
export ORWL_TRANSLATED_HOME='<<ORWL_TRANSLATED_HOME>>'
export ORWL_STDOUT="${OUTPUT}/1"
export ORWL_STDERR="${OUTPUT}/2"
export ORWL_TIDS="$(seq 0 $ORWL_TASKS)"

# Any ORWL binary works like this: prints something to stdout and waits to read
# from stdin. What it prints and reads is the address book (it prints its
# address and reads ALL the address book, his address and the address for other
# nodes). But as this script will launch only one process with several threads,
# his address are all the addresses. So we just give the same on stdin.  But to
# read stdout while the program is running is not easy (redirecting to a file or
# using tee didn't work), so we use a fifo that we can read just fine. And to
# write to stdin, we use a fifo too. FIFOs for the win :)
#
# It seems there is also another way to run an ORWL binary, giving the address
# book in a file, but this will require to use the FIFOs to read the stdout. And
# that is the tricky part. So using that mode doesn't simplify anything.
#
# In the future we should try to see a way to simplify this simple but really
# useful case of running one process with threads onl, probably with some
# patches on the ORWL lib.

tmpdir=$(mktemp -d)
fifo_out="$tmpdir/stdout"
fifo_in="$tmpdir/stdin"
mkfifo $fifo_out $fifo_in

# It is VERY important the order here. The fds must be opened in the same order
# we are giving them to the process. Order matters in shell redirections!
"$@" > $fifo_out < $fifo_in & exec 3<$fifo_out 4>$fifo_in
pid=$!

# Feels the "stdout" var with the content of $fifo_out
stdout=""
read_fifo_out

# Use the same output as address book. We add an empty newline at the end to
# specify that that's all the address book, if not it always tries to read more
printf "$stdout" >&4
printf "\n" >&4

wait $pid
ret=$?
rm -fr $tmpdir

echo "Exiting with return code (see man wait): $ret"
exit $ret
