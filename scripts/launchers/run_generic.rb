#!/usr/bin/ruby

## A generic run skript to be used for a remote ORWL launch.
##
## It uses 5 arguments on the command line for its own purpose
## and investigates the 6th, EXE.
##
## run_generic.rb GAB LAB RANK OUTPUT NPROC EXE ARG1 ARG2 ...
##
## That 6th argument EXE should be the full path name of the
## executable that is to be started. The working directory for that
## executable will be directory where it is located.
##
## All other arguments ARG1 ARG2 ... are passed untouched to that
## executable EXE.
##
## The 3rd argument RANK is the taktuk rank of this *process*. From
## that rank we deduce the tasks that are to be performed by this
## process. If ENV['ORWL_NTASKS'] is unset, 0 or 1 one task ID is
## assigned to this process. If it is greater, that amount of task IDs
## are assigned to that same process. In that case EXE must be able to
## run several tasks.


ENV['ORWL_GLOBAL_AB'] = ARGV.shift
ENV['ORWL_LOCAL_AB'] = ARGV.shift
PID = ARGV.shift.to_i - 1
OUTPUT = ARGV.shift
# the total number of processes is not used by this skript
nbproc = ARGV.shift.to_i

ENV['ORWL_STDOUT'] = "#{OUTPUT}-1"
ENV['ORWL_STDERR'] = "#{OUTPUT}-2"

ORWL_PATH = File.dirname(ARGV[0])

# compute the number of tasks per process
NTASKS = (!defined?(ENV['ORWL_NTASKS']) || (ENV['ORWL_NTASKS'].to_i == 0)) ? 1 : ENV['ORWL_NTASKS'].to_i;

# define the range of task IDs that will be handled by this process
PSTART = PID * NTASKS
PEND = PSTART + NTASKS
ENV['ORWL_TIDS'] = (PSTART...PEND).to_a.join(' ');

orwl_cmd = "cd #{ORWL_PATH} ; " + ARGV.join(' ')
puts("executing: #{orwl_cmd}")
system(orwl_cmd)
