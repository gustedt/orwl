#!/usr/bin/ruby
ORWL_PATH = "~/build/orwl/tests"
ENV['ORWL_GLOBAL_AB'] = ARGV.shift
ENV['ORWL_LOCAL_AB'] = ARGV.shift
tmp_tids = ARGV.shift.to_i - 1
ENV['ORWL_TIDS'] = "#{tmp_tids}"
OUTPUT = ARGV.shift
nbproc = ARGV.shift.to_i
#nb_tasks_per_node = ARGV[0].to_i
#submatrix_size = ARGV[1].to_i
iterations = ARGV[0].to_i
nb_rows = ARGV[1].to_i
nb_cols = ARGV[2].to_i

ENV['ORWL_STDOUT'] = "#{OUTPUT}"
ENV['ORWL_STDERR'] = "#{OUTPUT}-2"
#ENV['ORWL_VERBOSE'] = "1"

orwl_cmd = "cd #{ORWL_PATH} ;  ./orwl_benchmark_kernel23 #{iterations} #{nb_rows} #{nb_cols}"
#orwl_cmd = "cd #{ORWL_PATH} ;  ./orwl_benchmark_kernel23 #{iterations} #{nb_rows} #{nb_cols}"
puts("(#{tmp_tids}) executing: #{orwl_cmd}")
system(orwl_cmd)
