#!/bin/bash

# layout the dependeny graph
#
# This goes in three phases:
# - First color the graph by means of the script coloreo
# - Run neato on it to obtain a good set of initial coordinates
# - Run fdp to fine tune the length of the arcs
#
# The result of the last run depends much on the choice of the number
# of iterations for fdp. You may fine tune it by providing an
# additional parameter on the command line. Its default value is
# 1000.

if [ $# -lt 1 ]; then
    echo "Usage: $0 ifile.dot [maxiter]"
    exit 1
elif [ $# -gt 1 ]; then
    maxiter=$2
else
    maxiter=1000
fi

coloreo=`dirname $0`/coloreo.pl

ifile=$1
ofile=${ifile/[.]*/.pdf}

${coloreo} ${ifile} | neato -Tdot | fdp -Gmaxiter=${maxiter} -GK=1 -Tpdf -o ${ofile}
