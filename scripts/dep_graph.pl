#!/bin/sh -f
#  This may look like complete nonsense, but it really is -*- mode: perl; coding: utf-8 -*-
eval 'exec perl -wS -x $0 ${1+"$@"}'
if 0;               #### for this magic, see findSvnAuthors ####
#!perl
#
#
# Except for parts copied from previous work and as explicitly stated below,
# the author and copyright holder for this work is
# all rights reserved,  2012 Jens Gustedt, INRIA, France
#
# This file is part of the ORWL project. You received this file as as
# part of a confidential agreement and you may generally not
# redistribute it and/or modify it, unless under the terms as given in
# the file LICENSE.  It is distributed without any warranty; without
# even the implied warranty of merchantability or fitness for a
# particular purpose.
#


# This file follows a simple strategy to identify the dependency graph
# of an ORWL run. A task is identified with the location to which it
# reclaims write access. Such write accesses don't produce arcs of the
# graph. Arcs correspond to read accesses. Arcs that are local to an
# ORWL server thread have a high weight and will then be grouped
# closer together by the graphviz tools.
#
# The script coloreo.pl can then be used to color the resulting graph.

my %identities;
my @arcs;
my %name;
my %server;

# This script reads files that are given as arguments or from
# stdin. It dumps its output to stdout.

# Input lines are just search for three strings that are placed inside
# "..". The first two correspond to the source and target vertex of an
# arc. The encoding of the first is just the taskid, a number. The
# second is given as an orwl_endpoint.

# The third field is supposed to contain the strings false or true to
# indicate if this is a write or read access. If it is "false" the
# information is used to map a taskid to an orwl_endpoint.

# The output consists of the arcs that represent a read access, where
# the target part (that was given as orwl_endpoint) is replaced by the
# taskid. An additional arc weight is placed to mark if an edge is
# local to the same process or corresponds to a remote access.
foreach my $line (<>) {
    my ($source, $target, $label)
        = $line =~ m/"([^"]+)".*"([^"]+)".*"([^"]+)"/;
    if ($label =~ m/false/) {
        $identities{$target} = $source;
    } else {
        my @arc = ($source, $target);
        push(@arcs, \@arc);
    }
}

print "digraph G {\n";

sub id($) {
    $id = shift;
    $identities{$id} // $id;
}

sub group(_) {
    my $arg = shift;
    my ($group) = $arg =~ m|orwl://([^/]+)/|;
    $group;
}

foreach my $key (sort { id($a) <=> id($b) } keys %identities) {
    my $target = $identities{$key};
    my $group = group($key);
    $server{$target} = $group;
}

my $cluster = 0;

sub comp($$) {
    my ($arc0, $arc1) = @_;
    my @arc0 = (@{$arc0});
    my @arc1 = (@{$arc1});
    $arc0[0] <=> $arc1[0];
}

foreach my $arc (sort comp @arcs) {
    my @arc = (@{$arc});
    my $target = $identities{$arc[1]};
    print "$arc[0] -> $target";
    if ($server{$arc[0]} =~ $server{$target}) {
        print " [ weight = 1.0 ]";
    } else {
        print " [ weight = 0.1 ]";
    }
    print "\n";
}

print "}\n";
