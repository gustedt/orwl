# -*- makefile-gmake -*-

## @file
##
## @brief detect if CC is icc and setup correct linking depending on
## the target architecture
##
## This can be controled by ORWL_ICC_TARGET or ORWL_TARGET, which
## define if this is a host compilation or a crosscompilation. Known
## targets are
## - intel64
## - mic

ifeq (${ORWL_BARE_CC},icc)
ORWL_ICC_DIR := $(shell dirname ${ORWL_FULL_CC})
ORWL_ICC_HOST := $(shell basename ${ORWL_ICC_DIR})
ifeq (${INTEL_LICENSE_FILE},)
$(warning detected icc without initialization of the environment)
$(warning consider running ${ORWL_ICC_DIR}/compilervars_arch.sh)
endif
# first check if the generic target option is set
ifeq (${ORWL_ICC_TARGET},)
ifneq (${ORWL_TARGET},)
export ORWL_ICC_TARGET := ${ORWL_TARGET}
endif
endif
# if it was not set, chose host architecture
ifeq (${ORWL_ICC_TARGET},)
export ORWL_ICC_TARGET := ${ORWL_ICC_HOST}
$(warning setting icc target architecture to host architecture ${ORWL_ICC_TARGET})
endif
ifneq (${ORWL_ICC_TARGET},${ORWL_ICC_HOST})
$(warning icc is crosscompiling to ${ORWL_ICC_TARGET})
export ORWL_CROSS_COMPILING := 1
endif
ifeq (${ORWL_ICC_TARGET},mic)
CFLAGS += -mmic
LDSHARED += -mmic
LDFLAGS += -mmic
OPT ?= -O3
else
CFLAGS += -fPIC -global-hoist -fzero-initialized-in-bss -static-intel
LDFLAGS += -global-hoist -fzero-initialized-in-bss -static-intel
OPT ?= -O3 -march=native
endif
ORWL_ICC_LDIR := $(subst ${ORWL_ICC_HOST},${ORWL_ICC_TARGET},$(subst bin,compiler/lib,${ORWL_ICC_DIR}))
ORWL_ICC_MDIR := $(subst compiler,mkl,${ORWL_ICC_LDIR})
LDSHARED += -Wl,-rpath,${ORWL_ICC_LDIR}
LDFLAGS += -Wl,-rpath,${ORWL_ICC_LDIR} -L ${ORWL_ICC_LDIR}
# icc comes with a native BLAS implementation
ifeq (${HAVE_CBLAS},)
HAVE_CBLAS = 1
HAVE_ATLAS = 0
CBLASLIB = $(subst compiler,mkl,${ORWL_ICC_LDIR})
CBLASINC = $(MKLROOT)/include
CBLASLIBS = -Wl,-rpath,$(MKLROOT)/lib/${ORWL_ICC_TARGET} -L$(MKLROOT)/lib/${ORWL_ICC_TARGET} -Wl,--start-group -lmkl_intel_lp64 -lmkl_core -lmkl_sequential -Wl,--end-group -lpthread -lm
endif
endif

export LORWL := -lorwl_${ORWL_ICC_TARGET}
export LIBORWL := liborwl_${ORWL_ICC_TARGET}
