#!/usr/bin/env python

import sys
import os
import subprocess
import shutil
import math
import multiprocessing

def average(l):
    return sum(l) * 1.0 / len(l)

def stats(l):
    avg = average(l)
    variance = [ (x - avg)**2 for x in l ]
    stdev = math.sqrt(average(variance))

    return avg, stdev

def run_cmd(cmd):
    output = subprocess.check_output(cmd, shell=True)
    return output

def run_orwlcmd(orwlrun_cmd, env_vars_dict = None):
    # Verbose is needed to actually show the output
    d = dict(os.environ)
    d["ORWL_VERBOSE"] = "1"

    # Add (and overwrite if repeated with the defaults) specific vars
    if env_vars_dict is not None:
        d.update(env_vars_dict)

    # We don't care about shell injection (possible with shell=True)
    proc = subprocess.Popen(orwlrun_cmd, shell=True, stdout=subprocess.PIPE,
                            stderr=subprocess.STDOUT, env=d)

    output = proc.communicate()[0]

    # On error return exception, as other subprocess functions does
    if proc.returncode != 0:
        print "ERROR!"
        print output
        raise subprocess.CalledProcessError(returncode=proc.returncode,
                                            cmd=orwlrun_cmd, output=output)
    return output

def get_cmd_time_output(output):
    # The line we are looking for is something like:
    # .*CMD_TIME_SCRIPT|<wallclock time> <user time> <sys time>
    #
    # So we want to get that line and return all numbers after the
    # last pipe in that line
    look_for = "CMD_TIME_SCRIPT|"

    # Ignore the prefix, we are only intrested on what is after this
    start = output.index(look_for)
    start += len(look_for)

    end = output.index('\n', start)

    wallclock, user, system = output[start:end].split()
    return float(wallclock), float(user), float(system)


class CPUStats (object):

    def __init__(self, root_dir, logs_path, cmd, orwl_compile,
                 orwl_compile_instr):

        self.matrix_size = None
        self.cores = multiprocessing.cpu_count()
        self.root_dir = root_dir
        self.logs_path = logs_path
        self.cmd = cmd

        # Command to compile your binary WITHOUT deep instrumentation
        self.orwl_compile = orwl_compile

        # Command to compile your binary WITH deep instrumentation
        self.orwl_compile_instr = orwl_compile_instr

    def compile_liborwl(self, flags):
        lib_dir = self.root_dir + "/lib/"
        cmd = "cd %s && " % lib_dir
        cmd += "make clean > /dev/null && "
        cmd += "make -j %s %s liborwl.so > /dev/null " % (self.cores, flags)

        subprocess.check_call(cmd, shell=True)

    def compile_preload(self, flags):
        preload_dir = self.root_dir + "/deep-instr-preload/"
        cmd = "cd %s && " % preload_dir
        cmd += "make -j %s %s > /dev/null " % (self.cores, flags)

        subprocess.check_call(cmd, shell=True)
        return preload_dir + "liborwl-preload.so"

    def find_orwl_logs(self):
        """ In self.logs_path should be the output path specified to orwlrun
        As orwlrun creates a new dir in logs_dir, we try to guess which one it is by
        finding the most recent one"""
        cmd = "cd %s && " % self.logs_path
        cmd += "ls -td1 orwl_output* | head -n 1"
        output = subprocess.check_output(cmd, shell=True)
        return self.logs_path + "/" + output.strip()

    def uncompress_logs(self, log_dir):
        """log_dir as returned by find_orwl_logs(). i.e, the directory created by
        orwlrun
        Returns the path to the folder where uncompressed logs are"""
        cmd = "cd %s/logs && " % log_dir
        cmd += "tar xjf *.tar.bz2 && "
        cmd += "pwd"

        uncompress_logs_dir = subprocess.check_output(cmd, shell=True)

        # Remove newline and possible trailing spaces added by the shell
        uncompress_logs_dir = uncompress_logs_dir.strip()
        return uncompress_logs_dir

    def get_tl_delta_gamma(self, logs):
        """logs should be the "logs" directory as created by orwlrun
        This returns tl + delta and gamma calculated by the library and printed to
        stdout"""
        uncompress_logs_dir = self.uncompress_logs(logs)

        cmd_base = "cd %s && " % uncompress_logs_dir

        cmd_delta = cmd_base
        cmd_delta += "grep 'lib total time' *-1 | cut -d ':' -f2 "
        tl_delta = subprocess.check_output(cmd_delta, shell=True)
        tl_delta = float(tl_delta)

        cmd_gamma = cmd_base
        cmd_gamma += "grep 'lib init time' *-1 | cut -d ':' -f2 "
        gamma = subprocess.check_output(cmd_gamma, shell=True)
        gamma = float(gamma)

        return tl_delta, gamma


    def run_without_instr(self, iters = 10):
        self.compile_liborwl("RELEASE=1")
        run_cmd(self.orwl_compile)

        times = []
        wallclocks = []

        for i in range(iters):
            output = run_orwlcmd(self.cmd, {"ORWL_CMD_TIME_SCRIPT": "1"})

            wallclock, user, system = get_cmd_time_output(output)

            # Save the time in nanoseconds
            wallclocks.append(wallclock * 1e9)
            times.append((user + system) * 1e9)

            # Delete the log dir created by orwlrun
            orwl_logs = self.find_orwl_logs()
            shutil.rmtree(orwl_logs)

        return {"ttime": stats(times), "wallclock": stats(wallclocks)}

    def run_with_instr(self, iters = 10):
        wallclocks = []
        ttimes = []
        deltas = []
        gammas = []

        self.compile_liborwl("DEEP_INSTRUMENTATION=1 RELEASE=1")
        preload_file = self.compile_preload("RELEASE=1")
        run_cmd(self.orwl_compile_instr)

        # Run the command with LD_PRELOAD
        orwlrun_cmd = "LD_PRELOAD=" + preload_file + " " + self.cmd

        for i in range(iters):
            output = run_orwlcmd(orwlrun_cmd, {"ORWL_CMD_TIME_SCRIPT": "1"})
            wallclock, user, system = get_cmd_time_output(output)
            orwl_logs = self.find_orwl_logs()
            tl_delta, gamma = self.get_tl_delta_gamma(orwl_logs)

            # The wallcloc, user and syste are seconds instead of nanoseconds, so
            # convert them to nanoseconds
            wallclocks.append(wallclock * 1e9)
            ttimes.append((user + system) * 1e9)
            deltas.append(tl_delta)
            gammas.append(gamma)

            # Delete the log dir created by orwlrun
            shutil.rmtree(orwl_logs)

        #print "ttimes", ttimes
        #print "deltas:", deltas
        #print "gammas:", gammas
        d = {"ttime": stats(ttimes), "tl_delta": stats(deltas),
             "gamma": stats(gammas), "wallclock": stats(wallclocks)}
        return d

    def run_with_time(self, iters = 5):
        self.compile_liborwl("RELEASE=1")
        run_cmd(self.orwl_compile)

        wallclock_l = []
        user_l = []
        system_l = []

        for i in range(iters):
            output = run_orwlcmd(self.cmd, {"ORWL_CMD_TIME_SCRIPT": "1"})
            wallclock, user, system = get_cmd_time_output(output)

            # Save the time in nanoseconds
            wallclock_l.append(wallclock * 1e9)
            user_l.append(user * 1e9)
            system_l.append(system * 1e9)

            # Delete the log dir created by orwlrun
            orwl_logs = self.find_orwl_logs()
            shutil.rmtree(orwl_logs)

        return {"wallclock": stats(wallclock_l), "user": stats(user_l),
                "sys": stats(system_l) }

    def cpu_inside_lib(self, tu_tl, tu_tl_2delta_gamma, tl_delta, gamma):
        percentage_1 = 100.0 * tl_delta / tu_tl
        percentage_2 = 100.0 * tl_delta / tu_tl_2delta_gamma

        return {"percentage": [percentage_1, percentage_2]}

        tu_tl_2delta = tu_tl_2delta_gamma - gamma
        two_delta = tu_tl_2delta - tu_tl
        delta = two_delta / 2.0

        tl = tl_delta - delta
        tu = tu_tl - tl

        print "CPU time (ns) inside the library is", tl
        print "CPU time (ns) in user code is", tu

        print "two_delta", two_delta
        print "tl _tl_2delta - tu_tl", tu_tl_2delta_gamma - tu_tl
        print "CPU time (ns) inside the lib ignoring gamma is", tl_delta - ((tu_tl_2delta_gamma - tu_tl) / 2.0)
        print "tu_tl_2delta_gamma - tu_tl", tu_tl_2delta_gamma - tu_tl
        assert tu_tl_2delta_gamma - tu_tl > 0
        assert two_delta > 0
        assert tl > 0
        assert tu > 0

        percentage = 100.0 * tl / (tl + tu)
        print "Percentage used by the lib:", percentage, "%"

    def get_cpu_inside_lib(self, iters=10):
        ret = {}

        d = self.run_without_instr(iters)
        ret["wo_instr"] = d
        tu_tl = d["ttime"][0]

        d = self.run_with_instr(iters)
        ret["w_instr"] = d

        d = self.cpu_inside_lib(tu_tl = tu_tl, tu_tl_2delta_gamma = d["ttime"][0],
                                gamma = d["gamma"][0], tl_delta = d["tl_delta"][0])
        ret.update(d)
        return ret

    def cpu_used(self, wallclock_time, cpu_time):
        cores = multiprocessing.cpu_count()
        ideal = wallclock_time * cores
        used = 100.0 * cpu_time / ideal
        ret = { "ideal": ideal, "cores": cores, "percentage_used": used }

        if self.matrix_size is not None:
            ops = 2.0 * self.matrix_size ** 3
            wallclock_secs = wallclock_time / 1e9
            flops = ops / wallclock_secs
            ret["flops"] = flops

        return ret

    def get_cpu_used(self, iters=5):
        ret = {}
        ret = self.run_with_time(iters)
        w_avg, _ = ret["wallclock"]
        u_avg, _ = ret["user"]
        s_avg, _ = ret["sys"]

        d = self.cpu_used(wallclock_time = w_avg, cpu_time = u_avg + s_avg)
        ret.update(d)
        return ret

    def get_lib_stats(self):
        cpu_used = self.get_cpu_used()
        w_avg, _ = cpu_used["wallclock"]
        u_avg, _ = cpu_used["user"]
        s_avg, _ = cpu_used["sys"]

        print "CPU time:", u_avg + s_avg, "Wallclock:", w_avg
        print "Ideal time with", cpu_used["cores"], "cores is:", cpu_used["ideal"]
        print "Percentage of ideal time used:", cpu_used["percentage_used"], "%"
        if "flops" in cpu_used:
            print "Giga flops:", cpu_used["flops"] / 1e9
        print
        print "WARNING: Futex and other stuff used inside the library consume CPU"
        print "So please take into account the CPU time used inside the library"

        cpu_inside = self.get_cpu_inside_lib()

        d = cpu_inside["wo_instr"]
        print
        print "Stats without instrumentation"
        print "Average CPU time:", d["ttime"][0], "stdev:", d["ttime"][1]
        print "Average Wallclock time:", d["wallclock"][0], "stdev:", d["wallclock"][1]

        d = cpu_inside["w_instr"]
        print
        print "Stats with instrumentation"
        print "Total CPU time average:", d["ttime"][0], "stdev:", d["ttime"][1]
        print "Tl + delta average:", d["tl_delta"][0], "stdev:", d["tl_delta"][1]
        print "Gamma average:", d["gamma"][0], "stdev:", d["gamma"][1]
        print "Average Wallclock time:", d["wallclock"][0], "stdev:", d["wallclock"][1]
        print "Percentage used inside the lib:", cpu_inside["percentage"]


HELP = '''
Usage: <logs_path> <orwlrun cmd> <compile cmd wo/deep instr> <compile cmd w/deep instr>

Keep in mind the first argument MUST be the same as the one used as output_path
in orwlrun. You can also define the env variable ORWL_CORES to the number of
cores you want to use while compiling.

The second argument should be the orwlrun command that runs the binary you want,
in other word, the command you want to get cpu stats from.

The third one should be the line needed (with "cd" and all that is needed) to
compile the binary you are running with orwlrun. And the fourth should be the
line needed to compile it with deep instrumentation. That is, whatever is
neccesary to compile your binary with "-DDEEP_INSTRUMENTATION" defined in the
CFLAGS
'''

if __name__ == '__main__':

    if len(sys.argv) < 5:
        print HELP
        sys.exit(1)
    logs_path = sys.argv[1]
    orwlrun_cmd = sys.argv[2]
    orwl_compile = sys.argv[3]
    orwl_compile_instr = sys.argv[4]

    root_dir = os.path.abspath(os.path.dirname(__file__) + "/../../")
    orwl_cores = int(os.environ.get("ORWL_CORES", multiprocessing.cpu_count()))

    # Optionally defined by the user to have some reports about flops and that stuff
    # TODO: use a proper command line argument for this
    matrix_size = os.environ.get("ORWL_MATRIX_SIZE")

    cpu_stats = CPUStats(root_dir, logs_path, orwlrun_cmd, orwl_compile,
                         orwl_compile_instr)
    if matrix_size is not None:
        cpu_stats.matrix_size = int(matrix_size)
    cpu_stats.cores = orwl_cores
    cpu_stats.get_lib_stats()
