# -*- makefile-gmake -*-

## @file
## @brief detect HWLOC include and lib directory

ifeq (${ORWL_NOHWLOC},)

## @brief A list of possible include paths for hwloc
##
## If you have other typical install candidates please let us know
HWLOC_INCLUDE_PATHS =				\
        ${HWLOCINC}                             \
	/usr/include				\
	/usr/local/hwloc/include		\
	/opt/hwloc/include			\
	/opt/hwloc-base/include			\
	/usr/include/hwloc			\
	/usr/include/hwloc-base			\
	/usr/local/include/hwloc		\
	/usr/local/include/hwloc-base		\
	/usr/local/include                      \
        /home/farouk/hwloc-master/include

## @brief A list of possible library paths for hwloc
##
## If you have other typical install candidates please let us know
HWLOC_LIB_PATHS =				\
        ${HWLOCLIB}                             \
	/usr/lib				\
	/usr/local/hwloc/lib			\
	/opt/hwloc/lib				\
	/opt/hwloc-base/lib			\
	/opt/hwloc/lib64			\
	/opt/hwloc-base/lib64			\
	/opt/hwloc/lib32			\
	/opt/hwloc-base/lib32			\
	/usr/lib64/hwloc			\
	/usr/lib/hwloc-base			\
	/usr/lib64				\
	/usr/lib32				\
	/usr/local/lib64/hwloc			\
	/usr/local/lib/hwloc-base		\
	/usr/local/lib				\
	/usr/local/lib64			\
	/usr/local/lib32			\
	/usr/shared/lib64/hwloc			\
	/usr/shared/lib/hwloc-base		\
	/usr/shared/lib				\
	/usr/shared/lib64			\
	/usr/shared/lib32                       \
        /home/farouk/hwloc-master/lib

_HWLOC_PROG := '%:include <hwloc.h>|int (*toto)(hwloc_topology_t*) = hwloc_topology_init ;|int main(void)<% return 0; %>'

define FIND_HWLOC_INCLUDE
ifeq ($${_RET},NO)
ifneq ($$(wildcard $(1)),)
$$(call COMPILE_CHECK, ${CC}, ${CFLAGS}, ${_HWLOC_PROG}, -I$(1), -c)
ifeq ($${_RET},NO)
#$$(warning no hwloc found with $(1))
else
ifneq ($${_RET},YES)
override _RET := YES
override HWLOCINC := $(1)
$$(info hwloc include found in $(1))
endif
endif
endif
endif
endef

define FIND_HWLOC_LIB
ifeq ($${_RET},NO)
ifneq ($$(wildcard $(1)),)
$$(call COMPILE_CHECK, ${CC}, ${CFLAGS} -Werror, ${_HWLOC_PROG}, -I$(HWLOCINC), -L$(1) -lhwloc)
ifeq ($${_RET},NO)
#$$(warning no hwloc found with $(HWLOCINC) $(1))
else
ifneq ($${_RET},YES)
override _RET := YES
override HWLOCLIB := $(1)
override HWLOCLIBS := -lhwloc
override HAVE_HWLOC := 1
$$(info hwloc library found in $(1))
endif
endif
endif
endif
endef


ifeq (${HWLOCINC},)
override _RET := NO
${foreach PA, ${HWLOC_INCLUDE_PATHS}, ${eval ${call FIND_HWLOC_INCLUDE, ${PA}}}}
endif

ifeq (${HWLOCLIB},)
override _RET := NO
${foreach LI, ${HWLOC_LIB_PATHS}, ${eval ${call FIND_HWLOC_LIB, ${LI}}}}
endif

ifeq (${HAVE_HWLOC},1)
CFLAGS += -DHAVE_HWLOC=1
HWLOCLIBS = -lhwloc
endif

endif
