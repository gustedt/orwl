#!/bin/sh -f
#  This may look like complete nonsense, but it really is -*- mode: perl; coding: utf-8 -*-
eval 'exec perl -wS -x $0 ${1+"$@"}'
if 0;               #### for this magic, see findSvnAuthors ####
#!perl
#
#
# Except for parts copied from previous work and as explicitly stated below,
# the author and copyright holder for this work is
# all rights reserved,  2018 Jens Gustedt, INRIA, France
#
# This file is part of the ORWL project. You received this file as as
# part of a confidential agreement and you may generally not
# redistribute it and/or modify it, unless under the terms as given in
# the file LICENSE.  It is distributed without any warranty; without
# even the implied warranty of merchantability or fitness for a
# particular purpose.
#

use Getopt::Long;
use strict;
use English;

my $nfile;
my $afile;
my $help;

my $result = GetOptions(
    "normalized=s" => \$nfile,
    "acyclic=s" => \$afile,
    "help!" => \$help,
    );

#help() if (defined($help));

use Graph::Reader::Dot;
use Graph;

# The Graph::Writer module is not appropriate for us because it
# doesn't write out non-standard properties.
#
# First a function that writes a list of "tag = value" pairs that are
# provide through a reference to hash.
sub write_attributes ($$$) {
    my ($out, $name, $attri) = @_;
    if (defined($attri)) {
        print $out "  ${name} [\n";
        my %attri = %{$attri};
        foreach my $key (sort keys %attri) {
            print $out "    \"$key\" = \"$attri{$key}\"\n";
        }
        print $out "  ]\n";
    }
}

sub write_graph ($$) {
    my ($graph, $file) = @_;
    open my $out, ">$file";

    # The name attribute is special. We don't want to write it out as
    # the other attributes, but in the header of the file. Therefore
    # we copy the hash and delete the "name" entry.
    my $name = ${graph}->get_graph_attribute("name");
    print $out "digraph \"$name\" {\n";

    my %attri = %{${graph}->get_graph_attributes()};
    delete $attri{"name"};
    write_attributes($out, "graph", \%attri);

    print $out "\n  // vertex attributes\n";
    foreach my $v (sort ${graph}->vertices()) {
        write_attributes($out, "\"$v\"", ${graph}->get_vertex_attributes($v));
    }

    my @edges = sort {
        my $r = @{$a}[0] cmp @{$b}[0];
        if (!$r) {
            $r = @{$a}[1] cmp @{$b}[1];
        }
        $r;
    } ${graph}->edges;
    print $out "\n  // edge attributes\n";
    foreach my $e (@edges) {
        write_attributes($out, "\"@{$e}[0]\" -> \"@{$e}[1]\"", ${graph}->get_edge_attributes(@{$e}));
    }
    print $out "}\n";
}

my $reader = Graph::Reader::Dot->new();
$Graph::Reader::Dot::UseNodeAttr = 1;

my $graph = ${reader}->read_graph($ARGV[0]);

# This auxiliary graph will be used to test if the input graph (with
# eventually some reverted edges) is acyclic or not.
my $acyclic = Graph->new();

####################################################################################
# Start printing the program to stdout
#
# Header information

print "// **********************************************\n";
print "#include \"orwl_taskdep.h\"\n";
my $include = ${graph}->get_graph_attribute("include");
if (defined(${include})) {
    print "#include \"${include}\"\n";
}
print <<PRE;

// To simulate call by reference for Fortran we need a lot of
// temporaries.
#define FREF64(X) (int64_t const){ (X) }

PRE
my $name = ${graph}->get_graph_attribute("name");

# alphabetic list of vertices
my @vertex = sort(${graph}->vertices);

print "P99_GETOPT_SYNOPSIS(\"A program derived from task graph '${name}' with "
    .int(@vertex)
    ." nodes and "
    .int(${graph}->edges)
    ." dependencies.\");\n";

# inverse hash for that numbering
my %vertex;

# total number of vertices
my $n = @vertex;

# max in- and outdegree
my $out_max = 1;
my $in_max = 1;

# For each node, a hash with an array of input ports
my %iports;
# For each node, a hash with an array of output ports
my %oports;
# For each port, a hash with the order number at the source
my %interface;
# For each dependency the number of the corresponding head and tail.
my %tail;
my %head;

# Scan a graphviz port description and return a list of ports.  The
# syntax is a port name inside <>, followed by some arbitrary text.
sub ports ($) {
    map {
        $_ =~ /<([^<>]+)>.*/;
    } split('\|', $_[0]);
}

sub makeports {
    join("|", map {
        "<$_>$_";
         } @_
        );
}

# Number the vertices consistently and collect their input and output
# port orders.
foreach my $vn (0 .. $#vertex) {
    my $v = $vertex[$vn];
    $vertex{$v} = $vn;
    my $attri = ${graph}->get_vertex_attributes($v);
    my @oports;
    my @iports;
    if (defined($attri->{"headports"}) || defined($attri->{"headports"})) {
        $attri->{"headports"} //= "";
        $attri->{"tailports"} //= "";
        @iports = split(/[,[:space:]]+/, $attri->{"headports"});
        @oports = split(/[,[:space:]]+/, $attri->{"tailports"});
        my $func = $attri->{"function"};
        $attri->{"label"} =
            "\\N\\l${func}|{{"
            .makeports(@iports)
            ."}|{"
            .makeports(@oports)
            ."}}";
    } elsif (defined($attri->{"label"})) {
        my $label = $attri->{"label"};
        # The regular expression to parse the label is so ugly because
        # {, } and | all have special meaning in perl's regular
        # expressions. The syntax we parse is a quite restricted node
        # structure of graphviz' record labels, that give rise to a
        # layout such as
        #
        # +------------------------+
        # |      | In0 | In1 | In3 |
        # + Name +-----------------+
        # |      | Out0   | Out1   |
        # +------------------------+
        #
        # LABEL :=  [ Name "|" ] "{" LIST "|" LIST "}"
        # LIST  :=  "{" ELIST "}"
        # ELIST :=  ELEM [ "|" ELIST ]
        # ELEM  :=  <empty>
        #           | "<" ID ">" ID
        #
        my ($name, $iports, $oports) = ${label} =~ /([^{}|]*)[|]?[{][[:space:]]*[{]([^{}]*)[}][|][{]([^{}]*)[}][[:space:]]*[}]/;
        die "node $v syntax error on label ${label}, aborting" if (!defined($iports) || !defined($oports));
        @oports = ports($oports);
        @iports = ports($iports);
    }

    if (@oports || @iports) {
        $oports{$v} = \@oports;
        foreach my $i (0 .. $#oports) {
            $interface{${oports}[$i]} = $i;
            $tail{${oports}[$i]} = $vn;
        }

        # also collect the input port order
        $iports{$v} = \@iports;
        foreach my $i (0 .. $#iports) {
            # this is just one of the possible heads
            $head{${iports}[$i]} = $vn;
        }
        if (@{oports} > ${out_max}) {
            ${out_max} = @{oports};
        }
        if (@{iports} > ${in_max}) {
            ${in_max} = @{iports};
        }

    }
    $oports{$v} //= [];
    $iports{$v} //= [];
}

my @nname;

print "char const*const orwl_taskdep_nnames[$n] = {\n";
foreach my $v (@vertex) {
    #my $indeg = ${graph}->in_degree($v);
    $nname[$vertex{$v}] = $v;
    print "\t[$vertex{$v}] = \"${v}\",\n";
}
print "};\n";

my $data_type = ${graph}->get_graph_attribute("data_type")
    // "double";

my $data_size = ${graph}->get_graph_attribute("data_size")
    // 1;

my $buffer_length = ${graph}->get_graph_attribute("buffer_length")
    // 10;

# Look for pairs of input and output ports that don't have edges, yet.
{
    my %has_edge;
    foreach my $e (${graph}->edges) {
        my ($source, $target) = @{$e};
        my $attri = ${graph}->get_edge_attributes(${source}, ${target});
        my $interface = $attri->{"headport"} =~ s/:[a-z]+$//r;
        if (defined($interface)) {
            $has_edge{"${interface}\"\"$vertex{$source}\"\"$vertex{$target}"} = 1;
            $attri->{"headport"} = "${interface}:n"
        }
        $attri->{"const"} //= 0;
        $attri->{"initial"} //= 0;
        $attri->{"init"} //= "{ +0 }"
            if ($attri->{"const"} || $attri->{"initial"});
    }

    foreach my $v (@vertex) {
        my $vn = $vertex{$v};
        my @iports = @{$iports{$v} // []};
        for my $port (@iports) {
            if (defined($tail{$port})) {
                my $id = "$port\"\"$tail{$port}\"\"$vn";
                if (!defined($has_edge{$id})) {
                    print STDERR "adding edge for port \"$port\": $nname[$tail{$port}] -> $v\n";
                    ${graph}->set_edge_attribute($nname[$tail{$port}], $v, "headport", "${port}:n");
                    $has_edge{$id} = 1;
                }
            } else {
                print STDERR "port \"${port}\" is a boundary sink at node $v\n";
            }
        }
    }

    foreach my $v (@vertex) {
        my @oports = @{$oports{$v} // []};
        for my $port (@oports) {
            $tail{$port} // print STDERR "port \"${port}\" is a boundary source at node $v\n";
        }
    }
}

# Now add the type information

print <<"TYPES";
// **********************************************


// Task graph \"$name\"


// **********************************************
// External information needed for the task graph

// The data type through which tasks communicate.

enum { orwl_taskdep_vsize = ${data_size}, };
typedef ${data_type} orwl_taskdep_data;

// ***************************************
// Types.

// The transfer type between tasks.
typedef orwl_taskdep_data orwl_taskdep_vector[orwl_taskdep_vsize];

// The task functions that are used.
//
typedef void orwl_taskdep_ftype(orwl_taskdep_blob state[static restrict 1], int64_t const tid[static 1], int64_t const phase[static 1],
                int64_t const in_num[static 1], orwl_taskdep_vector in_mat[restrict in_num[0]], _Bool skip_in[restrict in_num[0]],
                int64_t const out_num[static 1], orwl_taskdep_vector out_mat[restrict out_num[0]], _Bool skip_out[restrict out_num[0]]);
typedef void orwl_taskdep_stype(orwl_taskdep_blob state[static restrict 1], int64_t const num[static 1]);

// *****************************************************************************
// Static properties of the task graph

// we have $n nodes/regions

TYPES

# find the functions associated to each vertex
my @vfunction;
my %vfunction;
my @vstartup;
my %vstartup;
my @vshutup;
my %vshutup;
foreach my $v (@vertex) {
    my $outdeg = ${graph}->out_degree($v);
    my $function = ${graph}->get_vertex_attribute(${v}, "function");
    if (!defined($vfunction{${function}})) {
        print "extern void ${function}(orwl_taskdep_blob state[static restrict 1], int64_t const tid[static 1], int64_t const phase[static 1],\n",
            "    int64_t const in_num[static 1], orwl_taskdep_vector in_mat[restrict in_num[0]], _Bool skip_in[restrict in_num[0]],\n",
            "    int64_t const out_num[static 1], orwl_taskdep_vector out_mat[restrict out_num[0]], _Bool skip_out[restrict out_num[0]]);\n";
        $vfunction{${function}} = 1;
    }
    @vfunction[$vertex{$v}] = ${function};
    my $startup = ${graph}->get_vertex_attribute(${v}, "startup")
        // "orwl_taskdep_start";
    if (!defined($vstartup{${startup}})) {
        print "extern void ${startup}(orwl_taskdep_blob state[static restrict 1], int64_t const num[static 1]);\n";
        $vstartup{${startup}} = 1;
    }
    @vstartup[$vertex{$v}] = ${startup};
    my $shutup = ${graph}->get_vertex_attribute(${v}, "shutdown")
        // "orwl_taskdep_shutdown";
    if (!defined($vshutup{${shutup}})) {
        print "extern void ${shutup}(orwl_taskdep_blob state[static restrict 1], int64_t const num[static 1]);\n";
        $vshutup{${shutup}} = 1;
    }
    @vshutup[$vertex{$v}] = ${shutup};
}

print "char const*const orwl_taskdep_snames[$n] = {\n";
foreach my $v (@vertex) {
    print "\t[$vertex{$v}] = \"@vstartup[$vertex{$v}]\",\n";
}
print "};\n";

print "char const*const orwl_taskdep_fnames[$n] = {\n";
foreach my $v (@vertex) {
    print "\t[$vertex{$v}] = \"@vfunction[$vertex{$v}]\",\n";
}
print "};\n";

print "char const*const orwl_taskdep_dnames[$n] = {\n";
foreach my $v (@vertex) {
    print "\t[$vertex{$v}] = \"@vshutup[$vertex{$v}]\",\n";
}
print "};\n";

# collect initializers
my %init;
# If a port has this property, the value that readers see in the first
# phase is the initializer.
my %initial;
# If a port has this property, readers always see the same value, the
# initializer.
my %const;
my @odeg;
foreach my $v (@vertex) {
    my @edges = ${graph}->edges_from($v);
    my @oports = @{$oports{$v}};
    my $outdeg = @oports;
    foreach my $e (@edges) {
        my $attri = ${graph}->get_edge_attributes(@{$e}[0], @{$e}[1]);
        my $interface = $attri->{"headport"} =~ s/:[a-z]+$//r;
        if (defined($interface)) {
            $attri->{"headport"} = "${interface}:n";
            $attri->{"tailport"} //= "${interface}:s";
            $attri->{"label"}  //= "${interface}";
            if (!defined($interface{${interface}})) {
                print STDERR "warning: dependency \"${interface}\" is not listed as output of @{$e}[0]\n";
                $interface{${interface}} = "${outdeg}";
                ++${outdeg};
            }
            $init{${interface}} = $attri->{"init"};
            $initial{${interface}} = $attri->{"initial"};
            $const{${interface}} = $attri->{"const"};
            print STDERR "port ${interface} has init $init{${interface}}\n" if (defined $init{${interface}});
        } else {
            print STDERR "warning: anonymous dependency @{$e}[0] -> @{$e}[1]";
            $interface = "annonymous_interface_${outdeg}";
            $interface{${interface}} = "$vertex{@{$e}[1]}";
            ++${outdeg};
        }
        $attri->{"const"} //= 1;
    }
    if (${outdeg} > ${out_max}) {
        ${out_max} = ${outdeg};
    }
    $odeg[$vertex{$v}] = ${outdeg};
}

print "\n";
print "// Initializers for ports. The table index is the index at the source, since only\n";
print "// these are guaranteed to be unique. The values here are pointers to const\n";
print "// qualified compound literals, or 0 if no \"init\" has been given for the port.\n";
print "static orwl_taskdep_vector const*const orwl_taskdep_defaults[$n][${out_max}] = {\n";
foreach my $v (@vertex) {
    my @oports = @{$oports{$v} // []};
    if (@oports) {
        print "\t[$vertex{$v}] = {\n";
        for my $i (0 .. $#oports) {
            if (defined($init{$oports[$i]})) {
                print "\t\t[$i] = &(orwl_taskdep_vector const)$init{$oports[$i]}, // $oports[$i]\n";
            } else {
                print "\t\t[$i] = 0, // no pre-init for $oports[$i]\n";
            }
        }
        print "\t},\n";
    }
}
print "};\n";

print "\n";
print "// The const property for ports. The table index is the index at the source, since only\n";
print "// these are guaranteed to be unique. .\n";
print "static bool const orwl_taskdep_const[$n][${out_max}] = {\n";
foreach my $v (@vertex) {
    my @oports = @{$oports{$v} // []};
    if (@oports) {
        print "\t[$vertex{$v}] = {\n";
        for my $i (0 .. $#oports) {
            $const{$oports[$i]} //= 0;
            print "\t\t[$i] = $const{$oports[$i]}, // $oports[$i]\n";
        }
        print "\t},\n";
    }
}
print "};\n";

print "\n";
print "// For each output port, provide the textual information about the link\n";
print "char const*const orwl_taskdep_oports[$n][${out_max}] = {\n";
foreach my $v (@vertex) {
    #if (defined($oports{$v})) {
    my @oports = @{$oports{$v} // []};
    if (@oports) {
        print "\t[$vertex{$v}] = {\n";
        for my $i (0 .. $#oports) {
            print "\t\t[$i] = \"$oports[$i]\",\n";
        }
        print "\t},\n";
    }
    #}
}
print "};\n";

print "\n";
print "// For each input port, provide the textual information about the link\n";
print "char const*const orwl_taskdep_iports[$n][${in_max}] = {\n";
foreach my $v (@vertex) {
    my @iports = @{$iports{$v} // []};
    if (@iports) {
        print "\t[$vertex{$v}] = {\n";
        for my $i (0 .. $#iports) {
            print "\t\t[$i] = \"$iports[$i]\",\n";
        }
        print "\t},\n";
    }
}
print "};\n";

print "\n";
print "// The outdegree for each node\n";
print "_Alignas(64) int64_t const orwl_taskdep_deg_out[$n] = {\n";
foreach my $v (@vertex) {
    my @oports = @{$oports{$v} // []};
    print "\t[$vertex{$v}] = ".(1+$#oports).",\n";
}
print "};\n";

print "\n";
print "// The indegree for each node\n";
print "_Alignas(64) int64_t const orwl_taskdep_deg_inp[$n] = {\n";
foreach my $v (@vertex) {
    my @iports = @{$iports{$v} // []};
    print "\t[$vertex{$v}] = ".(1+$#iports).",\n";
}
print "};\n";

print "\n";
print "// The number of each output port, or -1 if not this number is not available\n";
print "int64_t const*const orwl_taskdep_onum[$n] = {\n";
foreach my $v (@vertex) {
    my @oports = @{$oports{$v} // []};
    print "\t[$vertex{$v}] = (int64_t const[${in_max}]){\n";
    for my $i (0 .. (${in_max}-1)) {
        my $port = -1;
        if (defined($oports[$i])) {
            my ($tail, $head) = $oports[$i] =~ /([[:digit:]]+)[^[:digit:]]+([[:digit:]]+)/;
            if (defined(${tail})) {
                $port = "${tail}";
            }
        }
        print "\t\t[$i] = ${port},\n";
    }
    print "\t},\n";
}
print "};\n";

print "\n";
print "// The number of each input port, or -1 if not this number is not available\n";
print "int64_t const*const orwl_taskdep_inum[$n] = {\n";
foreach my $v (@vertex) {
    my @iports = @{$iports{$v} // []};
    print "\t[$vertex{$v}] = (int64_t const[${in_max}]){\n";
    for my $i (0 .. (${in_max}-1)) {
        my $port = -1;
        if (defined($iports[$i])) {
            my ($tail, $head) = $iports[$i] =~ /([[:digit:]]+)[^[:digit:]]+([[:digit:]]+)/;
            if (defined(${head})) {
                $port = "${head}";
            }
        }
        print "\t\t[$i] = ${port},\n";
    }
    print "\t},\n";
}
print "};\n";

my @ideg;

print "\n";
print "// For each input port, provide the source port\n";
print "static int64_t const orwl_taskdep_tail[$n][${in_max}] = {\n";
foreach my $v (@vertex) {
    my $vn = $vertex{$v};
    my @iports = @{$iports{$v} // []};
    $ideg[$vn] = scalar @iports;
    print "\t[${vn}] = {\n";
    foreach my $i (0 .. $#iports) {
        # This is a source port index
        my $sport = defined($interface{$iports[$i]})
            # an output port exists
            ? ($tail{$iports[$i]}*${out_max})+$interface{$iports[$i]}
            # no corresponding output exists
            : -1;
        # This is a target port index
        # my $tport = ($vn*${in_max}) + $i;
        print "\t\t[${i}] = ${sport},\n";
    }
    if (@iports < $in_max) {
        for my $i (@iports .. (${in_max}-1)) {
            my $tport = ($vn*${in_max}) + $i;
            print "\t\t[${i}] = -1,\n";
        }
    }
    print "\t},\n";
}
print "};\n";


print <<"USAGE";

void orwl_taskdep_usage(void) {
    printf("task number\\tname\\tfunction\\tstartup\\tshutdown\\t(in ports)\\t(out ports)\\n");
    for (size_t i = 0; i < ${n}; ++i) {
        printf("%zu\\t\\t%s\\t%s\\t%s\\t%s", i, orwl_taskdep_nnames[i], orwl_taskdep_fnames[i], orwl_taskdep_snames[i], orwl_taskdep_dnames[i]);
        if (orwl_taskdep_iports[i][0]) {
            printf("\\t(%s", orwl_taskdep_iports[i][0]);
            for (size_t j = 1; j < ${in_max}; ++j) {
                if (orwl_taskdep_iports[i][j])
                   printf(", %s", orwl_taskdep_iports[i][j]);
            }
            printf(")");
        } else {
            printf("\t()");
        }
        if (orwl_taskdep_oports[i][0]) {
            printf("\\t(%s", orwl_taskdep_oports[i][0]);
            for (size_t j = 1; j < ${out_max}; ++j) {
                if (orwl_taskdep_oports[i][j])
                   printf(", %s", orwl_taskdep_oports[i][j]);
            }
            printf(")");
        } else {
            printf("\t()");
        }
        printf("\\n");
    }

}

P99_GETOPT_CALLBACK(orwl_taskdep_usage);
USAGE

print "\n";
print "// *****************************************************\n";
print "// Instantiate the ORWL structure.\n";

print "ORWL_LOCATIONS_PER_TASK(\n\torwl_task_write_if0,\n\torwl_task_read_if0";
foreach my $k (2..${out_max}) {
    print ",\n\torwl_task_write_if". (${k}-1);
    print ",\n\torwl_task_read_if". (${k}-1);
}
print "\n);\n";

print "\nORWL_LOCATIONS_PER_TASK_INSTANTIATION();\n\n";

print "ORWL_DECLARE_TASK(orwl_taskdep_type);\n";

print "\n";


my $iterate = ${graph}->get_graph_attribute("iterate");

$iterate //= 1;

if ("${iterate}" =~ /^[[:alpha:]_][[:alnum:]_]*$/) {
    print "extern bool (${iterate})(orwl_taskdep_blob state[static restrict 1], int64_t const tid[static 1], int64_t const phase[static 1]);\n"
}

print "// *****************************************************\n";
print "// Set up the ORWL structure that reflects dependencies.\n";

print "\n";

my $htype;
if (${iterate} =~ /^[0-9]+$/ && "${iterate}" == "1") {
    print "// This graph is not iterated.\n";
    $htype = "orwl_handle";

} else {
    print "// This graph is iterated.\n";
    $htype = "orwl_handle2";
}
print "// Using ${htype}.\n";

my @edge = sort {
    my $r = $vertex{@{$a}[0]} <=> $vertex{@{$b}[0]};
    if (!$r) {
        $r = $vertex{@{$a}[1]} <=> $vertex{@{$b}[1]};
    }
    $r;
} ${graph}->edges;

foreach my $e (@edge) {
    my ($source, $target) = @{$e};
    my $interface = ${graph}->get_edge_attribute(${source}, ${target}, "headport") =~ s/:[a-z]+$//r ;
    if (defined(${interface})) {
        print "// ${source}\t→ ${target}\t(${interface})\n";
    } else {
        print "// ${source}\t→ ${target}\t(<anonymous>)\n";
    }
    ($source, $target) = map { $vertex{$_}; } @{$e};
    print "// ${source}\t→ ${target}\n";
    my $wri_pri = $initial{$interface} ? 1 : 0;
    my $rea_pri = ${wri_pri} ? 0 : 1;
    my %attri = %{${graph}->get_edge_attributes(@{$e})};
    if (${rea_pri}) {
        if (${acyclic}->has_edge(@{$e})) {
            # we have already seen the reverse edge of this one
            my $dir = ${acyclic}->get_edge_attribute(@{$e}, "dir");
            my $label = ${acyclic}->get_edge_attribute(@{$e}, "label");
            if (defined($dir) && "$dir" eq "both") {
                $attri{"label"} = "$label";
                $attri{"color"} = "black:red";
                $attri{"dir"} = "both";
            } else {
                print STDERR "dependency ${interface} as backedge ${label} without \"init\" attribute\n";
            }
        } else {
            ${acyclic}->add_edges(@{$e});
        }
        ${acyclic}->set_edge_attributes(@{$e}[0], @{$e}[1], \%{attri});
    } elsif (${graph}->has_edge(@{$e}[1], @{$e}[0])) {
        ${acyclic}->set_edge_attribute(@{$e}[1], @{$e}[0], "dir", "both");
        ${acyclic}->set_edge_attribute(@{$e}[1], @{$e}[0], "color", "black:red");
        my $label = ${graph}->get_edge_attribute(@{$e}[1], @{$e}[0], "label");
        if (defined($label)) {
            $label .= " ($interface)";
        } else {
            $label = "$interface";
        }
        print "// using reverse edge \"${label}\"\n";
        ${acyclic}->set_edge_attribute(@{$e}[1], @{$e}[0], "label", "${label}");
    } else {
        ${acyclic}->set_edge_attributes(@{$e}[1], @{$e}[0], \%{attri});
        ${acyclic}->set_edge_attribute(@{$e}[1], @{$e}[0], "dir", "back");
        ${acyclic}->set_edge_attribute(@{$e}[1], @{$e}[0], "color", "red");
        ${acyclic}->set_edge_attribute(@{$e}[1], @{$e}[0], "headport", "${interface}:s");
        ${acyclic}->set_edge_attribute(@{$e}[1], @{$e}[0], "tailport", "${interface}:n");
    }
    print "// priorities: ${wri_pri} ${rea_pri}\n";
}

print "\n";
print "// ****************************************************************\n";
print "// Per task specific initialization functions. They should ease some\n";
print "// const propagation concerning the task ID.\n";

my @oactive;
my @iactive;

foreach my $v (@vertex) {
    my $tid = $vertex{$v};

    print "static void orwl_taskdep_init_${tid}(${htype} hdl_in[static ${in_max}], ${htype} hdl_out[static ${out_max}]) {\n";

    print "\treport(orwl_taskdep_verbose, \"${tid}, starting orwl_taskdep_init, seeing %d locations\", orwl_locations_amount);\n";
    print "\n";

    my @oports = @{$oports{$v} // []};
    my @iports = @{$iports{$v} // []};

    $oactive[$tid] = 0;
    foreach my $port (@oports) {
        ++${oactive}[$tid] if (!$const{$port});
    }
    $iactive[$tid] = 0;
    foreach my $port (@iports) {
        ++${iactive}[$tid] if (!$const{$port});
    }
    if (${oactive}[$tid]+${iactive}[$tid] == 0) {
        print "\t// all ports are const, no handle insertion needed\n";
    } else {
        print "\tp99_seed*const seed = p99_seed_get();\n";
        print "\torwl_server*const server = orwl_server_get();\n";
        if (${oactive}[$tid] == 0) {
            print "\t// all output ports are const, no output insertion needed\n";
        } else {
            for my $i (0 .. $#oports) {
                my $wri_pri = $initial{$oports[$i]} ? 1 : 0;
                print "\torwl_write_insert(&hdl_out\[$i\], ORWL_LOCATION(${tid}, orwl_task_write_if$i), ${wri_pri}, seed, server);\n";
                print "\torwl_alloc_queue(ORWL_LOCATION(${tid}, orwl_task_write_if$i), ORWL_LOCATION(${tid}, orwl_task_read_if$i), ${buffer_length});\n";
            }
        }
        for my $i (0 .. $#iports) {
            if (defined($interface{$iports[$i]}) && !$const{$iports[$i]}) {
                my $read_pri = $initial{$iports[$i]} ? 0 : 1;
                print "\torwl_read_insert(&hdl_in\[$i\], ORWL_LOCATION($tail{$iports[$i]}, orwl_task_read_if$interface{$iports[$i]}), ${read_pri}, seed, server);\n";
            } else {
                print "\t// const port at $i\n";
            }
        }
        print "\n";
        print "\treport(orwl_taskdep_verbose, \"${tid}, orwl_taskdep_init all inserted\");\n";
    }

    print "\n";
    print "\t// Synchronize to have all requests inserted orderly at the other end.\n";
    print "\torwl_schedule();\n";
    print "\treport(orwl_taskdep_verbose, \"${tid}, finished orwl_taskdep_init\");\n";
    print "\n";
    print "};\n";

}

print "static orwl_taskdep_vector const p_dummy = { 0 };\n";

foreach my $tid (0 .. $#vertex) {

print <<"TASKSTART";

// Task function that is executed by task ${tid} ($vertex[${tid}])
static void orwl_taskdep_run_${tid}(int argc, char* argv[argc+1]) {
    report(orwl_taskdep_verbose, "starting task #${tid} ($vertex[${tid}])");

    orwl_taskdep_blob state;
    ORWL_TIMER(start)
        $vstartup[${tid}](&state, &FREF64(${tid}));

    p99_seed*const seed = p99_seed_get();
    enum {
        ideg  = $ideg[${tid}],
        odeg  = $odeg[${tid}],
        isize = ideg ? ideg : 1,
        osize = odeg ? odeg : 1,
    };

    static orwl_taskdep_vector mat_in[isize] = { 0 };
    static orwl_taskdep_vector mat_out[osize]= { 0 };
    orwl_taskdep_vector* min = ideg ? &mat_in[0] : 0;
    static _Bool skip_in[isize] = { 0 };
    static _Bool skip_out[osize]= { 0 };

TASKSTART

if (${iterate} =~ /^[0-9]+$/ && "${iterate}" == "1") {
    print "    // This graph is not iterated.\n";
    print "    static orwl_handle hdl_in[${in_max}] = {\n";
    print "         ORWL_HANDLE_INITIALIZER," foreach (1..${in_max});
    print "    };\n";
    print "    static orwl_handle hdl_out[${out_max}] = {\n";
    print "         ORWL_HANDLE_INITIALIZER,\n" foreach (1..${out_max});
    print "    };\n";
} else {
    print "    // This graph is iterated.\n";
    print "    static orwl_handle2 hdl_in[${in_max}] = {\n";
    print "        ORWL_HANDLE2_INITIALIZER,\n" foreach (1..${in_max});
    print "    };\n";
    print "    static orwl_handle2 hdl_out[${out_max}] = {\n";
    print "         ORWL_HANDLE2_INITIALIZER,\n" foreach (1..${out_max});
    print "    };\n";
}
print "\treport(orwl_taskdep_verbose, \"starting handle initialization\");\n";
if ($oactive[$tid]) {
    print "    ORWL_TIMER(scale) for (size_t i = 0; i < odeg; ++i) {\n";
    print "        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(${tid}, 2*i));\n";
    print "        orwl_scale(sizeof(orwl_taskdep_vector), ORWL_LOCATION(${tid}, 2*i+1));\n";
    print "    }\n";
}
print "    ORWL_TIMER(start) orwl_taskdep_init_${tid}(hdl_in, hdl_out);\n";

if (${iterate} =~ /^[0-9]+$/ && "${iterate}" == "1") {
    print "    {\n";
    print "    int64_t const orwl_phase = 0;\n";
} else {
    if ("${iterate}" =~ /^[0-9]+$/ && "${iterate}" > "1") {
        print "    for (int64_t orwl_phase = 0; orwl_phase < ${iterate}; ++orwl_phase) {\n";
    } else {
        print "    for (int64_t orwl_phase = 0; ${iterate}(&state, &FREF64(${tid}), &orwl_phase); ++orwl_phase) {\n";
    }
}

print <<"TASKMID0";
     report(orwl_taskdep_verbose, "task #${tid} ($vertex[${tid}]) new phase");
     for (size_t i = 0; i < ideg; ++i) {
         if (orwl_taskdep_tail[${tid}][i] != -1
             && !skip_in[i]) {
            size_t port = orwl_taskdep_tail[${tid}][i];
            orwl_taskdep_vector const* p_in = 0;
            // only acquire if the port is not const
            if (!orwl_taskdep_const[port / ${out_max}][port % ${out_max}]) {
                orwl_acquire(&hdl_in[i], 1, seed);
                p_in = orwl_read_map(&hdl_in[i], 0, seed);
            }
            if (!p_in) {
               p_in = orwl_taskdep_defaults[port / ${out_max}][port % ${out_max}];
            }
            memcpy(&min[i], p_in, sizeof *p_in);
            // only release if the port is not const
            if (!orwl_taskdep_const[port / ${out_max}][port % ${out_max}]) {
                orwl_release_or_next(&hdl_in[i], 1, seed);
            }
         } else {
            memcpy(&min[i], &p_dummy, sizeof p_dummy);
         }
     }

TASKMID0

print <<"TASKMID2";
     // Run one iteration of the function for the node.
     ORWL_TIMER(func)
         $vfunction[${tid}](&state, &FREF64(${tid}), &orwl_phase,
                            &FREF64(ideg), min, skip_in,
                            &FREF64(odeg), mat_out, skip_out);

TASKMID2

if ($oactive[$tid]) {
print <<"TASKMID1";
     {
               // Copy the result into the target locations.
               orwl_taskdep_vector* p_out[osize] = { 0 };
               if (odeg > 1) {
                   for (size_t i = 0; i < odeg; ++i)
                     if (!skip_out[i]) {
                       orwl_acquire(&hdl_out[i], 1, seed);
                       // If this port is const, just ignore the value
                       if (!orwl_taskdep_const[${tid}][i]) {
                           p_out[i] = orwl_write_map(&hdl_out[i], 0, seed);
                           memcpy(p_out[i], &mat_out[i], sizeof *(p_out[i]));
                       }
                       orwl_release_or_next(&hdl_out[i], 1, seed);
                    }
              } else {
                   // This port can't be  const.
                   orwl_acquire(&hdl_out[0], 1, seed);
                   p_out[0] = orwl_write_map(&hdl_out[0], 0, seed);
                   memcpy(p_out[0], &mat_out[0], sizeof *(p_out[0]));
                   orwl_release_or_next(&hdl_out[0], 1, seed);
               }
     }
TASKMID1
}

print <<"TASKMID4";
     if (orwl_taskdep_status_) {
         TRACE(1, "task ending with status %s", orwl_taskdep_status_);
         break;
     }
    } // end of iteration or block

    report(orwl_taskdep_verbose, "task #${tid} ($vertex[${tid}]) finished iterations");

    ORWL_TIMER(start)
        $vshutup[${tid}](&state, &FREF64(${tid}));

    report(orwl_taskdep_verbose, "task #${tid} ($vertex[${tid}]) waiting");
    orwl_global_barrier_wait();
    report(orwl_taskdep_verbose, "task #${tid} ($vertex[${tid}]) disconnecting");
TASKMID4

if (${iterate} !~ /^[0-9]+$/ || ${iterate} > 1) {
    print "    orwl_disconnect(hdl_in, ideg, seed);\n" if ($iactive[$tid]);
    print "    orwl_disconnect(hdl_out, odeg, seed);\n" if ($oactive[$tid]);
}

print <<"TASKEND";

    report(orwl_taskdep_verbose, "task #${tid} ($vertex[${tid}]) ends");
}

TASKEND
}

print "\n";
print "static void (*const orwl_taskdep_runs[])(int argc, char* argv[argc+1]) = {\n";
foreach my $tid (0 .. $#vertex) {
    print <<"IRUN"
    orwl_taskdep_run_${tid},
IRUN
}
print "};\n";

print <<"FORTRAN";

_Alignas(64) int64_t const orwl_taskdep_nodes   = ${n};
_Alignas(64) int64_t const orwl_taskdep_max_inp = ${in_max};
_Alignas(64) int64_t const orwl_taskdep_max_out = ${out_max};

// Add symbols for Fortran compatibility.

_Alignas(64) int64_t const ORWL_TASKDEP_FORT(orwl_taskdep_vsize)   = orwl_taskdep_vsize;

FORTRAN

foreach my $symb ("orwl_taskdep_deg_out",
                  "orwl_taskdep_deg_inp",
                  "orwl_taskdep_nodes",
                  "orwl_taskdep_max_inp",
                  "orwl_taskdep_max_out") {
    print "ORWL_TASKDEP_ALIAS(${symb});\n"
}

print <<"MAIN";

// A wrapper as generic task function that just launches the specific function
ORWL_DEFINE_TASK(orwl_taskdep_type) {
    ORWL_THREAD_USE(orwl_taskdep_type, argc, argv);
    int64_t const tid = orwl_mytid;
    orwl_taskdep_runs[tid](argc, argv);
}

// Scan the command line arguments. This is defined as a weak symbol such that 
// user code can overwrite this and provide their own.
P99_WEAK(orwl_taskdep_getopt)
void orwl_taskdep_getopt(int* argcp, char***argvp) {
  p99_getopt_initialize(argcp, argvp);
}

int main(int argc, char* argv[argc+1]) {
  orwl_taskdep_getopt(&argc, &argv);

  ORWL_TIMER() {
    orwl_init();

    if (orwl_nt != $n) {
        report(1, "We need exactly $n task, to process this graph, current value is %zu, exiting.", orwl_nt);
        return EXIT_FAILURE;
    }

    //! [launch one thread per task]
    for (size_t i = 0; i < orwl_lt; i++) {
      orwl_taskdep_type* task = P99_NEW(orwl_taskdep_type, argc, argv);
      orwl_taskdep_type_create_task(task, orwl_tids[i]);
    }
    //! [launch one thread per task]
  }
  return EXIT_SUCCESS;
}

// End of program for graph ${name}
// *********************************************************************************

MAIN

# print some output graphs, if requested

if (defined($afile) || defined($nfile)) {
    my %attri = %{${graph}->get_graph_attributes()};
    if (defined($afile)) {
        ${acyclic}->set_graph_attributes(\%{attri});
        ${acyclic}->set_graph_attribute("name", "acyclic");
        foreach my $v (@vertex) {
            my %attri = %{${graph}->get_vertex_attributes($v)};
            ${acyclic}->set_vertex_attributes($v, \%attri);
        }
        write_graph($acyclic, ${afile});
    }
    write_graph($graph, ${nfile}) if (defined($nfile));
}

# task graphs must essentially be acyclic, check it
{
    my @cycle = ${acyclic}->find_a_cycle();
    @cycle && die "task graph with inverted inits has cycle [@cycle], would deadlock, aborting";
}
