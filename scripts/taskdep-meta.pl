#!/bin/sh -f
#  This may look like complete nonsense, but it really is -*- mode: perl; coding: utf-8 -*-
eval 'exec perl -wS -x $0 ${1+"$@"}'
if 0;               #### for this magic, see findSvnAuthors ####
#!perl
#
#
# Except for parts copied from previous work and as explicitly stated below,
# the author and copyright holder for this work is
# all rights reserved,  2018 Jens Gustedt, INRIA, France
#
# This file is part of the ORWL project. You received this file as as
# part of a confidential agreement and you may generally not
# redistribute it and/or modify it, unless under the terms as given in
# the file LICENSE.  It is distributed without any warranty; without
# even the implied warranty of merchantability or fitness for a
# particular purpose.
#

# @file
# @brief Expand pattern (meta-) graphs according to specific parameters
#
# A meta graph is a graph that usually consists of a handful vertices
# and edges. All names (of the graph or of vertices) and attributes of
# that graph can be parameterized with expressions. This script
# generates a new graph from that, that has several copies of the
# original graph where the parameters of the attributes etc are
# instantiated with some values.
#
# A parameterized attribute is a string that contains expressions that
# are surrounded by () parenthesis, such as
#
# "node_($i)_($j)"
#
# or
#
# "($i % 2 ? \"odd\" : \"even\")"
#
# The "$i" and "$j" in here are variables that can be instantiate
# through two different mechanisms:
#
# - If the name of the variable is listed in the graph property
#   "environment", the value is initialized from an environment
#   variable of the same name.
#
# - If the name of the variable, say "i", is listed in the graph
#   property "ranges" it varies according to a graph property
#   "ranges_i". Usually such a property will contain a whole range of
#   values. For possible specifications of such ranges see below.
#
# Set set of value combinations for which copies of the input graph
# are produced is the combination of all possible values. If e.g
# ranges is "i, j", "ranges_i" is "1, 2, 3" and "ranges_j" is "5, 6",
# all tuples ($i, $j) that are considered are
#
# <code>
# "(1, 5)" "(1, 6)"
# "(2, 5)" "(2, 6)"
# "(3, 5)" "(3, 6)"
# </code>
#
# and the node names as above would expand to
#
# <code>
# node_1_5
# node_1_6
# node_2_5
# node_2_6
# node_3_5
# node_3_6
# </code>
#
#
# This is as if we had two nested `foreach` loops to produce the nodes
#
# <code>
# foreach my $i (1, 2, 3) {
#   foreach my $j (5, 6) {
#     ...
#   }
# }
# </code>
#
# The ranges themselves are also handled as expressions. If e.g we
# have "environment" as "maxi", "ranges_i" is "1 .. $maxi" and
# "ranges_j" is "1 .. $i", the resulting graph is as if it were
# produced by
#
# <code>
# foreach my $i (1 .. $maxi) {
#   foreach my $j (1 .. $i) {
#     ...
#   }
# }
# </code>
#
# and the value tuples are of the form
#
# <code>
# "(1, 1)"
# "(2, 1)" "(2, 2)"
# "(3, 1)" "(3, 2)" "(3, 2)"
# "(4, 1)" "(4, 2)" "(4, 2)" "(4, 4)"
# ...
# "($maxi, 1)" ....................... "($maxi, $maxi)"
# </code>
#
# ** Expressions
#
# Valid expressions are all perl expressions that use
#
# - variables from the ranges and environment properties preceded by a
#   dollar, optionally enclosed in braces, e.g "$i", "${maxi}"
#
# - arrays that are formed from ranges, e.g "@i" or "@{j}". If
#   evaluated in an arithmetic expression this results in the size of
#   the corresponding range.
#
# - the last position in a range, e.g "$#i" or "$#{j}". This is the
#   size of the corresponding range minus 1.
#
# - array elements from ranges, e.g "$i[0]" for the first element in
#   the "i" range or "$j[$#j]" for the last element in the "j"
#   range.
#
# Be careful, these are perl expressions so they might not be exactly
# what you'd expect when you are used to a different setting.
#
# ** Ranges
#
# Ranges can be formed by all perl expressions that may give rise to a
# list of values. Most prominently among these are explicit lists such
# as "1, 2, 3" and ranges such as "1 .. 3".
#
# ** Graph generation
#
# The name of the generated graph is the expanded name of the input
# graph. E.g if the the input graph has name "graph_($maxi)" and
# "maxi" is instantiated with the value 3, the output graph has the
# name "graph_3" and is written into the file "graph_3.dot" in the
# current directory.
#
# We expand a copy of the input graph for each range tuple.
#
# (1) First, we generate copies for all vertices that are explicitly
#   specified in the input graph, but see below. The output graph will
#   have no other vertex than that, no vertices will be inserted
#   implicitly through edges hereafter.
#
# (2) Then, edges are generated. For each candidate edge, we check if
#   both endpoints have been generated previously. If not, the edge is
#   omitted and otherwise it is added to result.
#
# For (1) it is not so easy to decide if a vertex had been specified
# explicitly in the input graph, or if it is just generated as the
# endpoint of an edge. We suppose that it is genuinely defined if has
# one of the attributes "headports", "tailports", or "label".
#
# In addition, the attribute "invalid" can be used to control if a
# expanded vertex is valid for the current value tuple or not. If set
# to something else than "" or "0" the vertex is invalid and not
# inserted into the output graph.

use strict;
use English;

# A regular expression that recursively captures balanced strings that
# are delimited by "(" ")".
my $re = qr{
            (                   # paren group 1
              \(                 # a "(" character
                 (?:              # group 2 without backref, may be empty
                   (?> [^()]+ )     # Non-parens without backtracking
                  |                 # or
                   (?1)             # Recurse to start of paren group 1
                 )*               # end of group 2
              \)                 # a ")" character
            )                   # end of group 1
}x;

use Graph::Reader::Dot;
use Graph;

# The Graph::Writer module is not appropriate for us because it
# doesn't write out non-standard properties.
#
# First a function that writes a list of "tag = value" pairs that are
# provide through a reference to hash.
sub write_attributes ($$$) {
    my ($out, $name, $attri) = @_;
    if (defined($attri)) {
        print $out "  ${name} [\n";
        my %attri = %{$attri};
        foreach my $key (sort keys %attri) {
            print $out "    \"$key\" = \"$attri{$key}\"\n";
        }
        print $out "  ]\n";
    }
}

sub write_graph ($$) {
    my ($graph, $file) = @_;
    open my $out, ">$file";

    # The name attribute is special. We don't want to write it out as
    # the other attributes, but in the header of the file. Therefore
    # we copy the hash and delete the "name" entry.
    my $name = ${graph}->get_graph_attribute("name");
    print $out "digraph $name {\n";

    my %attri = %{${graph}->get_graph_attributes()};
    delete $attri{"name"};
    write_attributes($out, "graph", \%attri);

    print $out "\n  // vertex attributes\n";
    foreach my $v (sort ${graph}->vertices()) {
        write_attributes($out, "\"$v\"", ${graph}->get_vertex_attributes($v));
    }

    print $out "\n  // edge attributes\n";
    foreach my $e (${graph}->edges()) {
        write_attributes($out, "\"@{$e}[0]\" -> \"@{$e}[1]\"", ${graph}->get_edge_attributes(@{$e}));
    }
    print $out "}\n";
}

my $reader = Graph::Reader::Dot->new();
$Graph::Reader::Dot::UseNodeAttr = 1;

my %range;
my @range;
my @name;
my @var;
my @environment;

sub repl ($) {
    my ($str) = (@_);
    foreach my $k (0 .. $#name) {
        $str =~ s/\@$name[$k]/\@\{\$range[$k]\}/g;
        $str =~ s/\@\{$name[$k]\}/\@\{\$range[$k]\}/g;

        $str =~ s/\$$name[$k]\[/\@\{\$range[$k]\}[/g;
        $str =~ s/\$\{$name[$k]\}\[/\@\{\$range[$k]\}[/g;

        $str =~ s/\$$name[$k]/\@\{\$range[$k]\}[\$var[$k]]/g;
        $str =~ s/\$\{$name[$k]\}/\@\{\$range[$k]\}[\$var[$k]]/g;

        $str =~ s/\$#$name[$k]/\$#\{\$range[$k]\}/g;
        $str =~ s/\$#\{$name[$k]\}/\$#\{\$range[$k]\}/g;
    }
    foreach my $env (@environment) {
        $str =~ s/\$$env/\$ENV\{$env\}/g;
        $str =~ s/\$\{$env\}/\$ENV\{$env\}/g;
    }
    $str =~ s!${re}!$1!gee;
    $str;
}

sub advance(\@\@\%) {
    my ($index, $ranges, $range) = (@_);
    if (!@{$index}) {
        #@{$index} = map { 0; } @{name};
        foreach my $dim (0 .. $#name) {
            my @arr;
            my $key = "range_$name[$dim]";
            #print STDERR "I: recomputing $key\n";
            my $r = repl($range->{${key}});
            eval "\@arr = ($r)";
            #print STDERR "I: recomputing: @arr\n";
            $ranges->[$dim] = \@arr;
            $index->[$dim] = 0;
        }
        print STDERR "now @{$index}\n";
        return 1;
    }
    my $dim = $#{$ranges};
    ++$index->[$dim];
    if ($index->[$dim] > $#{$ranges->[$dim]}) {
        while ($index->[$dim] >= $#{$ranges->[$dim]}) {
            --$dim;
            if ($dim < 0) {
                @{$index} = ();
                return 0;
            }
        }
        ++$index->[$dim];
        ++$dim;
        while ($dim < @{$ranges}) {
            my @arr;
            my $key = "range_$name[$dim]";
            #print STDERR "recomputing $key\n";
            my $r = repl($range->{${key}});
            eval "\@arr = ($r)";
            #print STDERR "recomputing: @arr\n";
            $ranges->[$dim] = \@arr;
            $index->[$dim] = 0;
            ++$dim;
        }
    }
    print STDERR "now @{$index}\n";
    return 1;
}

my $meta = ${reader}->read_graph($ARGV[0]);

my $generated = Graph->new();

my %attri = %{${meta}->get_graph_attributes()};

delete $attri{"name"};

${generated}->set_graph_attributes(\%attri);

@name = split(', ', $attri{'ranges'});
delete $attri{'ranges'};

foreach my $name (@name) {
    my $key = "range_${name}";
    die "missing graph attribute \"${key}\" for range ${name}"
        if (!defined($attri{$key}));
    $range{$key} = $attri{$key};
    delete $attri{$key};
}

@environment = split(', ', $attri{'environment'});
foreach my $env (@environment) {
    die "missing environment variable ${env}"
        if (!defined($ENV{$env}));
}
delete $attri{'environment'};

# Now it should be safe to call "repl" for the first time.
my $graphname = repl(${meta}->get_graph_attribute("name"));
${generated}->set_graph_attribute("name", ${graphname});

print STDERR "graphname is $graphname\n";

@var = ();
my %vertex;

while (advance(@var, @range, %range)) {
    foreach my $v (${meta}->vertices()) {
        my $nv = repl("$v");
        my %attr = %{${meta}->get_vertex_attributes($v)};
        $attr{"invalid"} //=0;
        if (defined($attr{"headports"}) || defined($attr{"tailports"}) || defined($attr{"label"})) {
            foreach my $key (keys %attr) {
                $attr{$key} = repl($attr{$key});
            }
            my @attr = %attr;
            if (!$attr{"invalid"}) {
                delete $attr{"invalid"};
                ${generated}->set_vertex_attributes($nv, \%attr);
                $vertex{$nv} = 1;
            }
        }
    }
}

while (advance(@var, @range, %range)) {
    foreach my $v (${meta}->vertices()) {
        my $nv = repl("$v");
        foreach my $e (${meta}->edges($v)) {
            my @ne = map { repl($_); } @{$e};
            if (defined($vertex{$ne[0]}) && defined($vertex{$ne[1]})) {
                my %attr = %{${meta}->get_edge_attributes(@{$e})};
                foreach my $key (keys %attr) {
                    $attr{$key} = repl($attr{$key});
                }
                my @attr = %attr;
                #print "edge @ne: @attr\n";
                ${generated}->set_edge_attributes(@ne, \%attr);
            }
        }
    }
}

write_graph($generated, "${graphname}.dot");
