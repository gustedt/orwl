/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the author and copyright holder for this work is                           */
/* all rights reserved,  2013 Jens Gustedt, INRIA, France                     */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#ifndef ORWL_MATMULT_CBLAS_H_
#define ORWL_MATMULT_CBLAS_H_

#if HAVE_CBLAS > 0

# include <cblas.h>
# include "orwl.h"
# include "orwl_matmult_float_type.h"
# include "orwl_matmult.h"


#define p00_gen_cblas_gemmd cblas_dgemm
#define p00_gen_cblas_gemmf cblas_sgemm
#define p00_gen_cblas_gemmdc cblas_zgemm
#define p00_gen_cblas_gemmfc cblas_cgemm

#define cblas_gemm(Order, TransA, TransB, M, N, K, alpha, A, lda,  B, ldb, beta, C, ldc)                                             \
P99_GEN_EXPR(cblas_gemm, ((A)[0]+(B)[0]+(C)[0]), d, f, dc, fc)(Order, TransA, TransB, M, N, K, alpha, A, lda,  B, ldb, beta, C, ldc)



/**
 ** @brief Multply matrix blocks @a curA and @a curB and store the
 ** result as @a iB's submatrix of @a curC.
 **
 ** @remark This particular function supposes that the matrices are
 ** already split into contiguous subblocks.
 **
 ** @remark The subblocks in matrix @a curB are supposed to be already
 ** transposed.
 **
 ** @remark @a transBuffer is not used by this function.
 **
 ** @remark This uses @c cblas_dgemm under the hood.
 **
 ** @see rowBlockMultBStd for the meaning of the parameters.
 **/
ALGOTEST_DECLARE(rowBlockGemmBTransposed);

/**
 ** @brief Multply matrix blocks @a curA and @a curB and store the
 ** result as @a iB's submatrix of @a curC.
 **
 ** @remark This particular function supposes that the matrices are
 ** already split into contiguous subblocks.
 **
 ** @remark The subblocks in matrix @a curB are not yet transposed.
 **
 ** @remark This uses @c cblas_gemm under the hood.
 **
 ** @remark @a transBuffer is not used by this function.
 **
 ** @see rowBlockMultBStd for the meaning of the parameters.
 **/
ALGOTEST_DECLARE(rowBlockGemmBStd);

/**
 ** @brief Multply matrix blocks @a curA and @a curB and store the
 ** result as @a iB's submatrix of @a curC.
 **
 ** @remark This particular function supposes that the matrices are
 ** are represented in rowmajor with long rows.
 **
 ** @remark Matrix @a curB is supposed to be already transposed and
 ** thus @a transBuffer is unused.
 **
 ** @see rowBlockMultBStd for the meaning of the parameters.
 **/
ALGOTEST_DECLARE(rowFlatGemmBTransposed);

/**
 ** @brief Multply matrix blocks @a curA and @a curB and store the
 ** result as @a iB's submatrix of @a curC.
 **
 ** @remark This particular function supposes that the matrices are
 ** are represented in rowmajor with long rows.
 **
 ** @remark Matrix @a curB is not supposed to be transposed.
 **
 ** @remark The transposition is done implicitly by @c gemm, thus @a
 ** transBuffer is unused.
 **
 ** @see rowBlockMultBStd for the meaning of the parameters.
 **/
ALGOTEST_DECLARE(rowFlatGemmBStd);
#endif

#endif
