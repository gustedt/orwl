/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the authors and copyright holders for this work are as follows:            */
/* all rights reserved,  2013 Jens Gustedt, INRIA, France                     */
/* all rights reserved,  2013 Stephane Vialle, SUPELEC, France                */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#ifndef ORWL_MATMULT_CPU_H_
#define ORWL_MATMULT_CPU_H_
#include "orwl.h"
#include "orwl_matmult_float_type.h"
#include "orwl_matmult.h"

/**
 ** @brief Dummy function for coprocessor context creation
 **
 ** @a id could be any sort of ID to address the unit that we want
 **
 ** Parameters @a mA, @a nB and @a kAB are the matrix dimensions that
 ** are to be handled; @c A is a <code>mA x kAB</code> matrix, @c B a
 ** <code>kAB x nB</code> matrix and @c C is a <code>mA x nB</code>
 ** matrix.
 **
 ** The result is a pointer to an opaque object, its structure is
 ** depending on the coprocessor library.
 **/
void* contextCreateDefault(int id, size_t n0, size_t n1, size_t n2, size_t nt);

/**
 ** @brief Dummy function for coprocessor context destruction
 **
 **/
void contextDestroyDefault(void* context);

/**
 ** @brief Dummy function to send matrix A to a coprocessor
 **
 ** @a A is a <code>n0 x (nt*n1)</code> matrix
 **/
void transferInputDefault(size_t n0, size_t n1,
                          size_t nt,
                          float_type const A[n0*(nt*n1)],
                          void* context);

/**
 ** @brief Dummy function to receive matrix C from a coprocessor
 **
 ** @a C is a <code>n0 x (nt*n1)</code> matrix
 **/
void transferOutputDefault(size_t n0, size_t n1,
                           size_t nt,
                           float_type C[n0*(nt*n1)],
                           void* context);

/**
 ** @brief Multply matrix blocks @a curA and @a curB and store the
 ** result as @a iB's submatrix of @a curC.
 **
 ** @param n0 matrix dimension
 ** @param n1 matrix dimension
 ** @param n2 matrix dimension
 **
 ** @param nt the number of stripes in which the overall matrices are
 ** cut. @a curA, @a curB and @a curC correspond to one stripe of such
 ** matrices.
 **
 ** @param iB is stripe number that corresponds to @a curB in the
 ** global matrix @c B. The result matrix that will be computed
 ** corresponds to the @a iB's submatrix in @a curC.
 **
 ** @param curA corresponds to a matrix row block of @a n0 rows, each
 ** of length @a nt X @a n1.
 **
 ** @param curB corresponds to a matrix column block of @a n2 columns,
 ** each of length @a nt X @a n1.
 **
 ** @param curC, the target, corresponds to a matrix row block of @a
 ** n0 rows, each of length @a nt X @a n2.
 **
 ** @param transBuffer is an auxiliary buffer that may be used to hold
 ** a transposed matrix.
 **
 ** @remark This particular function supposes that the matrices are
 ** already split into contiguous subblocks.
 **
 ** @remark The subblocks in matrix @a curB are supposed to be already
 ** transposed and thus @a transBuffer is unused.
 **
 **/
ALGOTEST_DECLARE(rowBlockMultBTransposed);

/**
 ** @brief Multply matrix blocks @a curA and @a curB and store the
 ** result as @a iB's submatrix of @a curC.
 **
 ** @remark This particular function supposes that the matrices are
 ** already split into contiguous subblocks.
 **
 ** @remark The subblocks in matrix @a curB are not yet transposed.
 **
 ** @remark @a transBuffer will be used to hold the transpose of @a
 ** curB during the processing.
 **
 ** @see rowBlockMultBStd for the meaning of the parameters.
 **/
ALGOTEST_DECLARE(rowBlockMultBStd);

/**
 ** @brief Multply matrix blocks @a curA and @a curB and store the
 ** result as @a iB's submatrix of @a curC.
 **
 ** @remark This particular function supposes that the matrices are
 ** are represented in rowmajor with long rows.
 **
 ** @remark Matrix @a curB is supposed to be already transposed and
 ** thus @a transBuffer is unused.
 **
 ** @see rowBlockMultBStd for the meaning of the parameters.
 **/
ALGOTEST_DECLARE(rowFlatMultBTransposed);

/**
 ** @brief Multply matrix blocks @a curA and @a curB and store the
 ** result as @a iB's submatrix of @a curC.
 **
 ** @remark This particular function supposes that the matrices are
 ** are represented in rowmajor with long rows.
 **
 ** @remark Matrix @a curB is not supposed to be transposed and
 ** thus @a transBuffer is used.
 **
 ** @see rowBlockMultBStd for the meaning of the parameters.
 **/
ALGOTEST_DECLARE(rowFlatMultBStd);

/**
 ** @brief Transpose @a A and store the value in @a B
 **/
void transpose(size_t n0, size_t n1,
               float_type B[n1][n0],
               float_type const A[n0][n1]);



#endif
