// This may look like C code, but it really is -*- mode: c++; coding: utf-8 -*-
//
// Except for parts copied from previous work and as explicitly stated below,
// the authors and copyright holders for this work are as follows:
// all rights reserved,  2013 Jens Gustedt, INRIA, France
// all rights reserved,  2013 Stephane Vialle, SUPELEC, France
//
// This file is part of the ORWL project. You received this file as as
// part of a confidential agreement and you may generally not
// redistribute it and/or modify it, unless under the terms as given in
// the file LICENSE.  It is distributed without any warranty; without
// even the implied warranty of merchantability or fitness for a
// particular purpose.
//

#ifdef HAVE_CUDAGPU

#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>
#include <cuda_runtime.h>
#include <cublas_v2.h>

#include "orwl_matmult_cublas.h"
#include "orwl_cublas.h"

//----------------------------------------------------------------------------
// Init the GPU device
//----------------------------------------------------------------------------
void GPU_device_init(void) {
// Init of the GPU device
  cuInit(0);
}

//----------------------------------------------------------------------------
// Init and finalization on the GPU
//----------------------------------------------------------------------------
void orwl_cublasInit(cublasHandle_t ** pt_adr_handle) {
  *pt_adr_handle = (cublasHandle_t*)malloc(sizeof **pt_adr_handle);
  CHECK_CUBLAS_SUCCESS(cublasCreate(*pt_adr_handle), "Init of the CUBLAS lib handle");
}


void orwl_cublasDestroy(cublasHandle_t ** pt_adr_handle) {
  // Remove the cublas handle only after we have free all buffers.
  CHECK_CUBLAS_SUCCESS(cublasDestroy(**pt_adr_handle),
                       "Destroy the CUBLAS lib handle");
  *pt_adr_handle = 0;
}


//----------------------------------------------------------------------------
// Matrix product on GPU
//----------------------------------------------------------------------------

#ifndef CUBLAS_TRANSPOSE_SHARED
# define CUBLAS_TRANSPOSE_SHARED 1
#endif

// Optimized version using the shared memory --------------------
__global__ void TransposeKernel_shared(float_type const*buff_in, float_type *buff_out,
                                       size_t n0, size_t n1, size_t n2, size_t iB, size_t nt) {
  // Small matrix in the shared memory
  __shared__ float_type smat[BLOCK_SIZE_XY_TK0][BLOCK_SIZE_XY_TK0];

  // Compute thread coordinates in the n2xn0 AxB matrix in ColumnMajor mode
  size_t l_in = blockIdx.y*BLOCK_SIZE_XY_TK0 + threadIdx.y;
  size_t c_in = blockIdx.x*BLOCK_SIZE_XY_TK0 + threadIdx.x;
  // Compute thread coordinates in the n0xn2 sub-part of C matrix in RowMajor mode
  // of the [threadIdx.y][threadIdx.x] data of the transposed smat matrix
  size_t L_out = blockIdx.x*BLOCK_SIZE_XY_TK0 + threadIdx.y;
  size_t C_out = blockIdx.y*BLOCK_SIZE_XY_TK0 + threadIdx.x;

  // If the thread aims to process a "real" pixel :
  if (l_in < n2 && c_in < n0) {
    size_t idx_in = c_in + l_in*n0;
    // Write in sh mem with transposition ([.x][.y]), read global mem with coalescence
    smat[threadIdx.x][threadIdx.y] = buff_in[idx_in];
  }
  // Wait for all data in the sh mem
  __syncthreads();
  if (L_out < n0 && C_out < n2) {
    // Compute idx in the n0*(n2*nt) C matrix
    size_t Idx_out = L_out*(n2*nt) + C_out + iB*n2;
    // Write in the global mem with coalescence
    buff_out[Idx_out] = smat[threadIdx.y][threadIdx.x];
  }
}

// Basic version for debugging -------------------------------------
__global__ void TransposeKernel_direct(float_type const*buff_in, float_type *buff_out,
                                       size_t n0, size_t n1, size_t n2, size_t iB, size_t nt) {
  // Compute thread coordinates in the n2xn0 AxB matrix in ColumnMajor mode
  size_t l_in = blockIdx.y*BLOCK_SIZE_XY_TK0 + threadIdx.y;
  size_t c_in = blockIdx.x*BLOCK_SIZE_XY_TK0 + threadIdx.x;
  // If the thread aims to process a "real" pixel :
  if (l_in < n2 && c_in < n0) {
    size_t idx_in = c_in + l_in*n0;
    // compute thread coordinates in the n0x(n2xnt) C matrix in RowMajor mode
    size_t L_out = c_in;
    size_t C_out = l_in + iB*n2;
    size_t Idx_out = C_out + L_out*(n2*nt);
    // copy AxB elt into C box
    buff_out[Idx_out] = buff_in[idx_in];
  }
}

template< class T >
void
cublasXgemm (cublasHandle_t handle, cublasOperation_t transa, cublasOperation_t transb, int m, int n,
             int k, const T* alpha, const T *A, int lda,
             const T *B, int ldb, const T* beta,
             T *C, int ldc);

template<>
void
cublasXgemm<float> (cublasHandle_t handle, cublasOperation_t transa, cublasOperation_t transb, int m, int n,
                    int k, const float* alpha, const float *A, int lda,
                    const float *B, int ldb, const float* beta,
                    float *C, int ldc) {
  cublasSgemm(handle, transa, transb, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc);
}


template<>
void
cublasXgemm<double> (cublasHandle_t handle, cublasOperation_t transa, cublasOperation_t transb, int m, int n,
                     int k, const double* alpha, const double *A, int lda,
                     const double *B, int ldb, const double* beta,
                     double *C, int ldc) {
  cublasDgemm(handle, transa, transb, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc);
}


template<>
void
cublasXgemm<cuComplex> (cublasHandle_t handle, cublasOperation_t transa, cublasOperation_t transb, int m, int n,
                        int k, const cuComplex* alpha, const cuComplex *A, int lda,
                        const cuComplex *B, int ldb, const cuComplex* beta,
                        cuComplex *C, int ldc) {
  cublasCgemm(handle, transa, transb, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc);
}


template<>
void
cublasXgemm<cuDoubleComplex> (cublasHandle_t handle, cublasOperation_t transa, cublasOperation_t transb, int m, int n,
                              int k, const cuDoubleComplex* alpha, const cuDoubleComplex *A, int lda,
                              const cuDoubleComplex *B, int ldb, const cuDoubleComplex* beta,
                              cuDoubleComplex *C, int ldc) {
  cublasZgemm(handle, transa, transb, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc);
}


void orwl_cublasProduct(cublasHandle_t * pt_handle,
                        size_t n0, size_t n1, size_t n2, size_t iB, size_t nt,
                        orwl_cudaObject*addr_A,
                        orwl_cudaObject*addr_B,
                        orwl_cudaObject* addr_AxB,
                        orwl_cudaObject* addr_C) {
  float_type alpha = 1.0;
  float_type beta  = 0.0;
  dim3 DbT, DgT;

  float_type const* gpuA = static_cast<float_type const*>(addr_A->addrGPU);
  float_type const* gpuB = static_cast<float_type const*>(addr_B->addrGPU);
  float_type * gpuAxB = static_cast<float_type*>(addr_AxB->addrGPU);
  float_type * gpuC = static_cast<float_type*>(addr_C->addrGPU);

  // In mid-air transposed version
  cublasXgemm(*pt_handle, CUBLAS_OP_T, CUBLAS_OP_T,
              n0, n2, n1*nt,
              &alpha, gpuA, n1*nt,
              gpuB, n2,
              &beta, gpuAxB, n0);

  // Transpose Row Major result into a part of C matrix
  DbT.x = BLOCK_SIZE_XY_TK0;
  DbT.y = BLOCK_SIZE_XY_TK0;
  DbT.z = 1;
  DgT.x = n0/BLOCK_SIZE_XY_TK0 + (n0%BLOCK_SIZE_XY_TK0 ? 1 : 0);
  DgT.y = n2/BLOCK_SIZE_XY_TK0 + (n2%BLOCK_SIZE_XY_TK0 ? 1 : 0);
  DgT.z = 1;
  if (CUBLAS_TRANSPOSE_SHARED) {
    TransposeKernel_shared<<<DgT,DbT>>>(gpuAxB, gpuC, n0, n1, n2, iB, nt);
  } else {
    TransposeKernel_direct<<<DgT,DbT>>>(gpuAxB, gpuC, n0, n1, n2, iB, nt);
  }

  // Don't wait for the end of the preceding GPU operations. This
  // helps to have overlap between computation and communication. But
  // this also means that runtime of this function itself can not be
  // used for performance measurements. You have to take the overall
  // function that measures computation and data transfers.
}


#endif
