/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the author and copyright holder for this work is                           */
/* all rights reserved,  2013 Jens Gustedt, INRIA, France                     */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#ifndef ORWL_CUDAOBJECT_H_
#define ORWL_CUDAOBJECT_H_


// Macro definitions
#define CHECK_CUDA_SUCCESS(EXP, MSG)                                    \
do {                                                                    \
  cudaError err = (EXP);                                                \
  if ((err) != cudaSuccess) {                                           \
    fprintf(stderr,"Error %d on CUDA operation: %s\n", err, (MSG));     \
    exit(EXIT_FAILURE);}                                                \
} while(0)

#ifdef __cplusplus
extern "C" {
  // C++ resp. CUDA are quite peculiar with their pointer model on the
  // GPU device. We introduce a dummy type cudaDeviceObject that
  // protects us from misinterpretation of these device pointers on
  // the C side.
  typedef void cudaDeviceObject;
#else
/* On the C side this maps to an opaque struct. The advantage of
   such a struct is that a pointer to such a struct is not allowed
   to alias any other pointer, so better optimizations to functions
   that receive mixed arguments could be possible. void*, unsigned
   char* or derivatives would be allowed to alias, so these
   opportunities for optimization could get lost. */
typedef struct cudaDeviceObject cudaDeviceObject;

/* We don't want to include any CUDA headers. */
typedef struct cublasHandle_t cublasHandle_t;
#endif

  /**
   ** @brief encapsulate an object that can either be located on CPU
   ** or GPU
   **
   ** A simple object to handle objects on the GPU.
   **
   ** The flag @c onGPU controls if the actual object is considered to
   ** be on the GPU or not. To transfer the object from or to GPU use
   ** the functions ::cudaObject_CPU2GPU or ::cudaObject_GPU2CPU,
   ** respectively.
   **
   ** Both these functions can also be used to force-update an object
   ** to a new contents: if an object is already "onGPU", the CPU side
   ** of the object is modified and then ::cudaObject_CPU2GPU is
   ** called, the new contents is copied to the GPU.
   **/
  typedef struct orwl_cudaObject orwl_cudaObject;
  struct orwl_cudaObject {
    cudaDeviceObject* addrGPU;  //!< address of the incarnation on GPU
    void* addrCPU;              //!< address of the incarnation on CPU, if any
    size_t size;                //!< its size
    bool onGPU;                 //!< is the object currently considered to be on GPU
    bool oursCPU;               //!< has the CPU part been allocated by the library
  };

  orwl_cudaObject* orwl_cudaObject_init(orwl_cudaObject* A, size_t s);
  void orwl_cudaObject_destroy(orwl_cudaObject* A);
  void orwl_cudaObject_replace(orwl_cudaObject* A, void* Arep);
  void orwl_cudaObject_copy(orwl_cudaObject* target, orwl_cudaObject const* source, size_t s);
  void orwl_cudaObject_set(orwl_cudaObject* target, unsigned char val);
  void orwl_cudaObject_CPU2GPU(orwl_cudaObject*A);
  void orwl_cudaObject_GPU2CPU(orwl_cudaObject*A);

  inline
  void* orwl_cudaObject_write_map(orwl_cudaObject*A) {
    if (A->onGPU) orwl_cudaObject_GPU2CPU(A);
    return A->addrCPU;
  }

  void const* orwl_cudaObject_read_map(orwl_cudaObject*A);
  void orwl_cudaObject_unmap(orwl_cudaObject*A);

#ifdef __cplusplus
}
#endif


#endif
