/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the authors and copyright holders for this work are as follows:            */
/* all rights reserved,  2013 Jens Gustedt, INRIA, France                     */
/* all rights reserved,  2013 Stephane Vialle, SUPELEC, France                */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#include "orwl.h"
#include "orwl_matmult_cublas_interface.h"
#include "orwl_matmult.h"
#include "orwl_task_obj.h"

P99_DECLARE_STRUCT(GPUctx);

struct GPUctx {
  orwl_cudaObject adrA;
  orwl_cudaObject adrB;
  orwl_cudaObject adrAxB;
  orwl_cudaObject adrC;
  cublasHandle_t* handle;

};

GPUctx* GPUctx_init(GPUctx* ctx,
                    size_t n0, size_t n1, size_t n2, size_t nt) {
  if (ctx) {
    // Init of the CUBLAS lib handle
    orwl_cublasInit(&ctx->handle);

    orwl_cudaObject_init(&ctx->adrA, n0*(n1*nt)*sizeof(float_type));
    orwl_cudaObject_init(&ctx->adrB, (n1*nt)*n2*sizeof(float_type));
    orwl_cudaObject_init(&ctx->adrAxB, n0*n2*sizeof(float_type));
    orwl_cudaObject_init(&ctx->adrC, n0*(n2*nt)*sizeof(float_type));

    // Reset GPU_C symbol (to debug)
    orwl_cudaObject_set(&ctx->adrAxB, 0);

    // Reset GPU_C symbol (to debug)
    orwl_cudaObject_set(&ctx->adrC, 0);
  }
  return ctx;
}

void GPUctx_destroy(GPUctx* ctx) {
  // Free the GPU matrices
  orwl_cudaObject_destroy(&ctx->adrA);
  orwl_cudaObject_destroy(&ctx->adrB);
  orwl_cudaObject_destroy(&ctx->adrAxB);
  orwl_cudaObject_destroy(&ctx->adrC);
  orwl_cublasDestroy(&ctx->handle);
}

P99_DECLARE_DELETE(GPUctx);
P99_DEFINE_DELETE(GPUctx);

void* contextCreateCUDAGPU(int id, size_t n0, size_t n1, size_t n2, size_t nt) {
  GPUctx * context = P99_NEW(GPUctx, n0, n1, n2, nt);
  return context;
}

void contextDestroyCUDAGPU(void* cxt) {
  GPUctx * context = cxt;
  GPUctx_delete(context);
}

void transferInputCUDAGPU(size_t n0, size_t n1,
                          size_t nt,
                          float_type const A[n0*(nt*n1)],
                          void* ctx) {
  GPUctx * context = ctx;
  orwl_cudaObject_replace(&context->adrA, (void*)A);
  orwl_cudaObject_CPU2GPU(&context->adrA);
}

void transferOutputCUDAGPU(size_t n0, size_t n2,
                           size_t nt,
                           float_type C[n0*(nt*n2)],
                           void* ctx) {
  GPUctx * context = ctx;
  orwl_cudaObject_replace(&context->adrC, C);
  orwl_cudaObject_GPU2CPU(&context->adrC);
}

/**
 ** @brief Multply matrix blocks @a curA and @a curB and store the
 ** result as @a iB's submatrix of @a curC.
 **
 ** @remark This particular function supposes that the matrices are
 ** are represented in rowmajor with long rows.
 **
 ** @remark Matrix @a curB is supposed to be already transposed and
 ** thus @a transBuffer is unused.
 **
 ** @see rowBlockMultBStd for the meaning of the parameters.
 **/
void
ALGOTEST_FUNC(rowFlatCUDAGPUBStd)(size_t n0, size_t n1, size_t n2,
                                  size_t iB, size_t nt,
                                  float_type curC[n0*(nt*n2)], float_type const curA[n0*(nt*n1)], float_type const curB[(nt*n1)*n2],
                                  void*transBuffer, void* ctx) {
  ORWL_TIMER(, nt*n0*n1*n2) {
    GPUctx * context = ctx;
    orwl_cudaObject_replace(&context->adrB, (void*)curB);
    orwl_cudaObject_CPU2GPU(&context->adrB);
    orwl_cublasProduct(context->handle,
                       n0, n1, n2, iB, nt,
                       &context->adrA, &context->adrB, &context->adrAxB, &context->adrC);
  }
}
