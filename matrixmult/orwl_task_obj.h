/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the authors and copyright holders for this work are as follows:            */
/* all rights reserved,  2011 Emmanuel Jeanvoine, INRIA, France               */
/* all rights reserved,  2012-2014 Jens Gustedt, INRIA, France                */
/* all rights reserved,  2013 Stephane Vialle, SUPELEC, France                */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#ifndef ORWL_TUTORIAL_TASK_OBJ_H_
#define ORWL_TUTORIAL_TASK_OBJ_H_

//! [compile time parametrization]
#include "orwl.h"
ORWL_LOCATIONS_PER_TASK(locationB);
//! [compile time parametrization]

//! [run time parametrization]
#include "p99_getopt.h"

P99_GETOPT_SYNOPSIS("an ORWL test program for different dense matrix multiplication algorithms");
P99_GETOPT_DECLARE(n, size_t, opt_n, 0, "n", "control the nb of columns in a block of B");
P99_GETOPT_DECLARE(m, size_t, opt_m, 0, "m", "control the nb of rows in a block of A");
P99_GETOPT_DECLARE(k, size_t, opt_k, 0, "k", "control the nb of columns in a block of A (and nb of rows in a block of B) ");
P99_GETOPT_DECLARE(N, size_t, opt_N, 0, "N-colB", "control the total nb of columns of B");
P99_GETOPT_DECLARE(M, size_t, opt_M, 0, "M-ligA", "control the total nb of rows of A");
P99_GETOPT_DECLARE(K, size_t, opt_K, 0, "K-colA-ligB", "control the total nb of columns of A (and rows of B)");
P99_GETOPT_DECLARE(i, size_t, opt_i,   5, "iterations", "the number of test iterations");
P99_GETOPT_DECLARE(a, char const*, opt_afile, 0, "afile", "an input file name");
P99_GETOPT_DECLARE(b, char const*, opt_bfile, 0, "bfile", "a second input file name");
P99_GETOPT_DECLARE(o, char const*, opt_outfile, 0, "ofile", "an output file name");
P99_GETOPT_DECLARE(T, char const*, opt_tests, "rowBlockMultBStd", "tests", "name the test(s) that are to be performed, can be overwritten by orwl_tgetenv");
P99_GETOPT_DECLARE(W, char const*, opt_weights, 0, "weights", "weights of the test(s) that are to be performed, can be overwritten by orwl_tgetenv");
P99_GETOPT_DECLARE(P, unsigned, opt_P, UINT_MAX, "cpus", "control the total nb of CPUS to be used by CPU intensive functions");
P99_GETOPT_DECLARE(1, unsigned, opt_L1, UINT_MAX, "L1", "control the total nb of L1 caches to be used by CPU intensive functions");
P99_GETOPT_DECLARE(2, unsigned, opt_L2, UINT_MAX, "L2", "control the total nb of L2 caches to be used by CPU intensive functions");
P99_GETOPT_DECLARE(3, unsigned, opt_L3, UINT_MAX, "L1", "control the total nb of L3 caches to be used by CPU intensive functions");
//! [run time parametrization]

extern void usage(void);
P99_GETOPT_CALLBACK(usage);

//! [declare task specific state]
P99_DECLARE_STRUCT(task_obj);
P99_DEFINE_STRUCT(task_obj,
                  unsigned dummy_val
                 );

task_obj* task_obj_init(task_obj* task);
void task_obj_destroy(task_obj *task);

P99_DECLARE_DELETE(task_obj);
ORWL_DECLARE_TASK(task_obj);
//! [declare task specific state]


//! [initialize the GPU, if any]
extern void GPU_device_init(void);
//! [initialize the GPU, if any]

#endif
