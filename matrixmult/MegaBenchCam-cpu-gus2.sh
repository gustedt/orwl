export ORWL_NODEFILE=/usr/users/info/vialle/Proj-ORWL/orwl/matrixmult/mach_cam16.txt
#
../scripts/orwlrun output-cpu-double-gus2 default 1 1:$1 orwl_matmult -M 5760 -N 5760 -K 5760 -i 10 -T rowFlatGemmBStd -2 $2
sleep 5s
#
../scripts/orwlrun output-cpu-double-gus2 default 2 2:$1 orwl_matmult -M 8146 -N 8172 -K 8172 -i 10 -T rowFlatGemmBStd -2 $2
sleep 5s
#
../scripts/orwlrun output-cpu-double-gus2 default 3 3:$1 orwl_matmult -M 9978 -N 9990 -K 9990 -i 10 -T rowFlatGemmBStd -2 $2
sleep 5s
#
../scripts/orwlrun output-cpu-double-gus2 default 4 4:$1 orwl_matmult -M 11520 -N 11520 -K 11520 -i 10 -T rowFlatGemmBStd -2 $2
sleep 5s
#
../scripts/orwlrun output-cpu-double-gus2 default 5 5:$1 orwl_matmult -M 12880 -N 12960 -K 12960 -i 10 -T rowFlatGemmBStd -2 $2
sleep 5s
#
../scripts/orwlrun output-cpu-double-gus2 default 6 6:$1 orwl_matmult -M 14112 -N 14688 -K 14688 -i 10 -T rowFlatGemmBStd -2 $2
sleep 5s
#
../scripts/orwlrun output-cpu-double-gus2 default 8 8:$1 orwl_matmult -M 16288 -N 16416 -K 16416 -i 10 -T rowFlatGemmBStd -2 $2
sleep 5s
#
../scripts/orwlrun output-cpu-double-gus2 default 10 10:$1 orwl_matmult -M 18210 -N 18360 -K 18360 -i 10 -T rowFlatGemmBStd -2 $2
sleep 5s
#
../scripts/orwlrun output-cpu-double-gus2 default 12 12:$1 orwl_matmult -M 19956 -N 20088 -K 20088 -i 10 -T rowFlatGemmBStd -2 $2
sleep 5s
#
../scripts/orwlrun output-cpu-double-gus2 default 15 15:$1 orwl_matmult -M 22305 -N 22410 -K 22410 -i 10 -T rowFlatGemmBStd -2 $2
sleep 5s
#
../scripts/orwlrun output-cpu-double-gus2 default 16 16:$1 orwl_matmult -M 23040 -N 23040 -K 23040 -i 10 -T rowFlatGemmBStd -2 $2
sleep 5s
#
