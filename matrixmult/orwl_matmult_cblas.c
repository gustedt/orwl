/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the authors and copyright holders for this work are as follows:            */
/* all rights reserved,  2013 Jens Gustedt, INRIA, France                     */
/* all rights reserved,  2013 Stephane Vialle, SUPELEC, France                */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#include "orwl_matmult_cblas.h"
#include "orwl_qresource.h"

void
ALGOTEST_FUNC(rowBlockGemmBTransposed)(size_t n0, size_t n1, size_t n2,
                                       size_t iB, size_t nt,
                                       float_type curC[n0*(nt*n2)], float_type const curA[n0*(nt*n1)], float_type const curB[(nt*n1)*n2],
                                       void*transBuffer, void* context) {
  ORWL_TIMER(, nt*n0*n1*n2) {
    /* Declare the lengths in all dimensions. */
    float_type const (*restrict curRowA)[nt][n0][n1] = (void*)curA;
    /* B is already transposed. */
    float_type const (*restrict curRowB)[nt][n2][n1] = (void*)curB;
    float_type (*restrict curRowC)[nt][n0][n2] = (void*)curC;

    ORWL_QSECTION(L2)
    P99_DO(size_t, k, 0, nt) {
      cblas_gemm(CblasRowMajor, CblasNoTrans, CblasTrans,
                 n0, n2, n1,
                 1.0, &(*curRowA)[k][0][0], n1,
                 &(*curRowB)[k][0][0], n1,
                 1.0, &(*curRowC)[iB][0][0], n2);
    }
  }
}

void
ALGOTEST_FUNC(rowBlockGemmBStd)(size_t n0, size_t n1, size_t n2,
                                size_t iB, size_t nt,
                                float_type curC[n0*(nt*n2)], float_type const curA[n0*(nt*n1)], float_type const curB[(nt*n1)*n2],
                                void*transBuffer, void* context) {
  ORWL_TIMER(, nt*n0*n1*n2) {
    /* Declare the lengths in all dimensions. */
    float_type const (*restrict curRowA)[nt][n0][n1] = (void*)curA;
    float_type const (*restrict curColumnB)[nt][n1][n2] = (void*)curB;
    float_type (*restrict curRowC)[nt][n0][n2] = (void*)curC;

    ORWL_QSECTION(L2)
    P99_DO(size_t, k, 0, nt) {
      cblas_gemm(CblasRowMajor, CblasNoTrans, CblasNoTrans,
                 n0, n2, n1,
                 1.0, &(*curRowA)[k][0][0], n1,
                 &(*curColumnB)[k][0][0], n2,
                 1.0,&(*curRowC)[iB][0][0], n2);
    }
  }
}

void
ALGOTEST_FUNC(rowFlatGemmBTransposed)(size_t n0, size_t n1, size_t n2,
                                      size_t iB, size_t nt,
                                      float_type curC[n0*(nt*n2)], float_type const curA[n0*(nt*n1)], float_type const curB[(nt*n1)*n2],
                                      void*transBuffer, void* context) {
  if (!P99_IS_MAX(INT_MAX, n0, n1, n2, nt*n1, nt*n2))
    trace(1, "dimension(s) exceed the possibility of cblas_gemm: %zX %zX %zX %zX %zX",
          n0, n1, n2, nt*n1, nt*n2);
  else
    ORWL_TIMER(, nt*n0*n1*n2) {
    /* Declare the lengths in all dimensions. */
    float_type const (*restrict curRowA)[n0][nt*n1] = (void*)curA;
    /* B is already transposed. */
    float_type const (*restrict curRowB)[n2][nt*n1] = (void*)curB;
    float_type (*restrict curRowC)[n0][nt*n2] = (void*)curC;

    ORWL_QSECTION(L2)
    cblas_gemm(CblasRowMajor, CblasNoTrans, CblasTrans,
               n0, n2, nt*n1,
               1.0, &(*curRowA)[0][0], nt*n1,
               &(*curRowB)[0][0], nt*n1,
               0.0, &(*curRowC)[0][iB*n2], nt*n2);
  }
}

void
ALGOTEST_FUNC(rowFlatGemmBStd)(size_t n0, size_t n1, size_t n2,
                               size_t iB, size_t nt,
                               float_type curC[n0*(nt*n2)], float_type const curA[n0*(nt*n1)], float_type const curB[(nt*n1)*n2],
                               void*transBuffer, void* context) {
  if (!P99_IS_MAX(INT_MAX, n0, n1, n2, nt*n1, nt*n2))
    trace(1, "dimension(s) exceed the possibility of cblas_gemm: %zX %zX %zX %zX %zX",
          n0, n1, n2, nt*n1, nt*n2);
  else
    ORWL_TIMER(, nt*n0*n1*n2) {
    /* Declare the lengths in all dimensions. */
    float_type const (*restrict curRowA)[n0][nt*n1] = (void*)curA;
    /* B is already transposed. */
    float_type const (*restrict curRowB)[nt*n1][n2] = (void*)curB;
    float_type (*restrict curRowC)[n0][nt*n2] = (void*)curC;

    ORWL_QSECTION(L2)
    cblas_gemm(CblasRowMajor, CblasNoTrans, CblasNoTrans,
               n0, n2, nt*n1,
               1.0, &(*curRowA)[0][0], nt*n1,
               &(*curRowB)[0][0], n2,
               0.0, &(*curRowC)[0][iB*n2], nt*n2);
  }
}

