/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the authors and copyright holders for this work are as follows:            */
/* all rights reserved,  2013-2014 Jens Gustedt, INRIA, France                */
/* all rights reserved,  2013 Stephane Vialle, SUPELEC, France                */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#include "orwl_matmult.h"
#include "orwl_matmult_cpu.h"
#if HAVE_CBLAS > 0
# include "orwl_matmult_cblas.h"
#endif
#if HAVE_CUDAGPU > 0
# include "orwl_matmult_cublas.h"
# include "orwl_matmult_cublas_interface.h"
#endif

/**
 ** @file
 ** @brief A test and benchmark program for matrix multiplication
 **/

P99_DEFINE_ENUM(algoMM);

/**
 ** @brief Initialization of a matrix block of size @a nt X @a n0 X @a
 ** n1 elements.
 **
 ** Here this matrix block is interpreted as @a n0 rows and @a nt X @a
 ** n1 columns at line offset @a n0 X @a mytid. The matrix block is
 ** structured into @a nt submatrices of the form @a n0 X @a n1.
 **/
void* dummy_init_line_blocks(size_t mytid, size_t n0, size_t n1, size_t n2, size_t nt, float_type Aflat[n0*(n1*nt)]) {
  float_type (*A)[n0][n1] = (void*)Aflat;
  size_t len_l = nt * n1;
  size_t off_l = mytid * n0;
  P99_PARALLEL_DO(size_t, it, 0, nt) {
    float_type (*Ab)[n1] = A[it];
    size_t off_cb = it * n1;
    P99_DO(size_t, l, 0, n0) {
      P99_DO(size_t, c, 0, n1) {
        Ab[l][c] = (off_cb + c) + (off_l + l)*len_l;
      }
    }
  }
  return A;
}

/**
 ** @brief Initialization of a chunk of columns of a matrix interpreted
 ** as a vector of @a nt blocks of size @a n1 X @a n2 elements.
 **/
void* dummy_init_col_blocks(size_t mytid, size_t n0, size_t n1, size_t n2, size_t nt, float_type Bflat[(n1*nt)*n2]) {
  float_type (*B)[n1][n2] = (void*)Bflat;
  size_t off_c = mytid * n2;
  size_t len_l = nt * n2;
  P99_PARALLEL_DO(size_t, it, 0, nt) {
    float_type (*Bb)[n2] = B[it];
    size_t off_lb = it * n1;
    P99_DO(size_t, l, 0, n1) {
      P99_DO(size_t, c, 0, n2) {
        Bb[l][c] = (off_c + c) + (off_lb + l)*len_l;
      }
    }
  }
  return B;
}


/**
 ** @brief Initialization of a chunk of columns of a matrix interpreted
 ** as a vector of @a nt transposed blocks of size @a n2 X @a n1 elements.
 **/
void* dummy_init_col_blocks_transposed(size_t mytid, size_t n0, size_t n1, size_t n2, size_t nt, float_type Bflat[n2*(n1*nt)]) {
  float_type (*B)[n2][n1] = (void*)Bflat;
  size_t off_c = mytid * n2;
  size_t len_l = nt * n2;
  P99_PARALLEL_DO(size_t, it, 0, nt) {
    float_type (*Bb)[n1] = B[it];
    size_t off_lb = it * n1;
    P99_DO(size_t, c, 0, n2) {
      P99_DO(size_t, l, 0, n1) {
        Bb[c][l] = (off_c + c) + (off_lb + l)*len_l;
      }
    }
  }
  return B;
}


/**
 ** @brief Initialization of a flat matrix of size @a n0 X (@a nt X
 ** @a n1) elements.
 **/
void* dummy_init_line_flat(size_t mytid, size_t n0, size_t n1, size_t n2, size_t nt, float_type Aflat[n0*(n1*nt)]) {
  size_t len1 = nt * n1;
  size_t off_elt = (mytid * n0) * len1;
  P99_PARALLEL_DO(size_t, i, 0, n0*(n1*nt)) {
    Aflat[i] = off_elt + i;
  }
  return Aflat;
}

/**
 ** @brief Initialization of a flat transposed matrix of size @a n2 X
 ** (@a n1 X @a nt) elements.
 **/
void* dummy_init_col_flat_transposed(size_t mytid, size_t n0, size_t n1, size_t n2, size_t nt, float_type Bflat[n2*(n1*nt)]) {
  float_type (*B)[n1*nt] = (void*)Bflat;
  size_t off_c = mytid * n2;
  size_t len_l = n2 * nt;
  P99_PARALLEL_DO(size_t, c, 0, n2) {
    P99_DO(size_t, l, 0, n1*nt) {
      B[c][l] = (off_c + c) + l*len_l;
    }
  }
  return B;
}


/**
 ** @brief Initialization of a flat matrix of size (@a n1 X @a nt) X
 ** @a n2 elements.
 **/
void* dummy_init_col_flat(size_t mytid, size_t n0, size_t n1, size_t n2, size_t nt, float_type Bflat[(n1*nt)*n2]) {
  float_type (*B)[n2] = (void*)Bflat;
  size_t off_c = mytid * n2;
  size_t len_l = n2 * nt;
  P99_PARALLEL_DO(size_t, l, 0, n1*nt) {
    P99_DO(size_t, c, 0, n2) {
      B[l][c] = (off_c + c) + l*len_l;
    }
  }
  return B;
}


/**
 ** @brief print value (@a lg,@a cg) in a flat matrix that is supposed to
 ** be split into a chunk of rows or of columns depending on the value of
 ** @a chunkOfLines.
 **/
void printValueStdFlat(char const* str, size_t mytid, size_t nlth,
                       size_t ncth, size_t nt, bool chunkOfLines,
                       float_type const Mloc[nlth*ncth/nt],
                       size_t lg, size_t cg) {
  // Compute the [l,c] range managed by the task
  size_t lineLow = (chunkOfLines ? nlth/nt*mytid : 0);
  size_t lineUpEx = (chunkOfLines ? nlth/nt*(mytid+1) : nlth);
  size_t columnLow = (chunkOfLines ? 0 : ncth/nt*mytid);
  size_t columnUpEx = (chunkOfLines ? ncth : ncth/nt*(mytid+1));
  // Size of the local matrix lines
  size_t nbColLoc = (chunkOfLines ? ncth : ncth/nt);
  // If the (lg,cg) elt to print is in the ranges of the task: print it
  if (lineLow <= lg && lg < lineUpEx &&
      columnLow <= cg && cg < columnUpEx) {
    float_type (*M)[nbColLoc] = (void *) Mloc;
    printf("%s[%zu][%zu]: is %a\n", str, lg, cg, M[lg-lineLow][cg-columnLow]);
  }
}


/**
 ** @brief print value (@a lg,@a cg) in a flat transposed matrix that is
 ** supposed to be split into a chunk of rows or of columns depending on
 ** the value of @a chunkOfLines.
 **/
void printValueTransFlat(char const* str, size_t mytid, size_t nlth,
                         size_t ncth, size_t nt, bool chunkOfLines,
                         float_type const Mloc_trans[nlth*ncth/nt],
                         size_t lg, size_t cg) {
  // Compute the [l,c] range managed by the task
  size_t lineLow = (chunkOfLines ? nlth/nt*mytid : 0);
  size_t lineUpEx = (chunkOfLines ? nlth/nt*(mytid+1) : nlth);
  size_t columnLow = (chunkOfLines ? 0 : ncth/nt*mytid);
  size_t columnUpEx = (chunkOfLines ? ncth : ncth/nt*(mytid+1));
  // Size of the local matrix lines
  size_t nbColLocTrans = (chunkOfLines ? nlth/nt : nlth);
  // If the (lg,cg) elt to print is in the ranges of the task: print it
  if (lineLow <= lg && lg < lineUpEx &&
      columnLow <= cg && cg < columnUpEx) {
    float_type (*M_trans)[nbColLocTrans] = (void *) Mloc_trans;
    printf("%s[%zu][%zu]: is %a\n",
           str, lg, cg, M_trans[cg-columnLow][lg-lineLow]);
  }
}


/**
 ** @brief print value (@a lg,@a cg) in a matrix of blocks that is supposed to
 ** be split into a chunk of rows or of columns depending on the value of
 ** @a chunkOfLines.
 **/
void printValueStdBlock(char const* str, size_t mytid, size_t nlth,
                        size_t ncth, size_t nt, bool chunkOfLines,
                        float_type const Mloc[nlth*ncth/nt],
                        size_t lg, size_t cg) {
  // Compute the [l,c] range managed by the task
  size_t lineLow = (chunkOfLines ? nlth/nt*mytid : 0);
  size_t lineUpEx = (chunkOfLines ? nlth/nt*(mytid+1) : nlth);
  size_t columnLow = (chunkOfLines ? 0 : ncth/nt*mytid);
  size_t columnUpEx = (chunkOfLines ? ncth : ncth/nt*(mytid+1));
  // Size of a local block-matrix lines and columns
  size_t nbColBlock = ncth/nt;
  size_t nbLineBlock = nlth/nt;
  // If the (lg,cg) elt to print is in the ranges of the task: print it
  if (lineLow <= lg && lg < lineUpEx &&
      columnLow <= cg && cg < columnUpEx) {
    size_t iB, cl, ll;
    float_type (*Mb)[nbLineBlock][nbColBlock] = (void *) Mloc;
    if (chunkOfLines) {
      iB = cg/nbColBlock;
      cl = cg%nbColBlock;
      ll = lg-lineLow;
    } else {
      iB = lg/nbLineBlock;
      cl = cg-columnLow;
      ll = lg%nbLineBlock;
    }
    printf("%s[%zu][%zu]: is %a\n",
           str, lg, cg, Mb[iB][ll][cl]);
  }
}


/**
 ** @brief print value (@a lg,@a cg) in a matrix of transposed blocks that
 ** is supposed to be split into a chunk of rows or of columns depending on
 ** the value of @a chunkOfLines.
 **/
void printValueTransBlock(char const* str, size_t mytid, size_t nlth,
                          size_t ncth, size_t nt, bool chunkOfLines,
                          float_type const Mloc_trans[nlth*ncth/nt],
                          size_t lg, size_t cg) {
  // Compute the [l,c] range managed by the task
  size_t lineLow = (chunkOfLines ? nlth/nt*mytid : 0);
  size_t lineUpEx = (chunkOfLines ? nlth/nt*(mytid+1) : nlth);
  size_t columnLow = (chunkOfLines ? 0 : ncth/nt*mytid);
  size_t columnUpEx = (chunkOfLines ? ncth : ncth/nt*(mytid+1));
  // Size of a local block-matrix lines and columns
  size_t nbColBlock = ncth/nt;
  size_t nbLineBlock = nlth/nt;
  // If the (lg,cg) elt to print is in the ranges of the task: print it
  if (lineLow <= lg && lg < lineUpEx &&
      columnLow <= cg && cg < columnUpEx) {
    size_t iB, cl, ll;
    float_type (*Mb_trans)[nbColBlock][nbLineBlock] = (void *) Mloc_trans;
    if (chunkOfLines) {
      iB = cg/nbColBlock;
      cl = cg%nbColBlock;
      ll = lg-lineLow;
    } else {
      iB = lg/nbLineBlock;
      cl = cg-columnLow;
      ll = lg%nbLineBlock;
    }
    printf("%s[%zu][%zu]: is %a\n",
           str, lg, cg, Mb_trans[iB][cl][ll]);
  }
}


static algoTest const funcTab[algoMM_amount] = {
  ALGOTEST_INITIALIZER(rowFlatMultBStd,
  dummy_init_line_flat,
  dummy_init_col_flat,
  printValueStdFlat,
  printValueStdFlat,
  contextCreateDefault,
  contextDestroyDefault,
  transferInputDefault,
  transferOutputDefault
                      ),
  ALGOTEST_INITIALIZER(rowFlatMultBTransposed,
  dummy_init_line_flat,
  dummy_init_col_flat_transposed,
  printValueStdFlat,
  printValueTransFlat,
  contextCreateDefault,
  contextDestroyDefault,
  transferInputDefault,
  transferOutputDefault
                      ),
  ALGOTEST_INITIALIZER(rowBlockMultBStd,
  dummy_init_line_blocks,
  dummy_init_col_blocks,
  printValueStdBlock,
  printValueStdBlock,
  contextCreateDefault,
  contextDestroyDefault,
  transferInputDefault,
  transferOutputDefault
                      ),
  ALGOTEST_INITIALIZER(rowBlockMultBTransposed,
  dummy_init_line_blocks,
  dummy_init_col_blocks_transposed,
  printValueStdBlock,
  printValueTransBlock,
  contextCreateDefault,
  contextDestroyDefault,
  transferInputDefault,
  transferOutputDefault
                      ),
#if HAVE_CBLAS > 0
  ALGOTEST_INITIALIZER(rowFlatGemmBStd,
  dummy_init_line_flat,
  dummy_init_col_flat,
  printValueStdFlat,
  printValueStdFlat,
  contextCreateDefault,
  contextDestroyDefault,
  transferInputDefault,
  transferOutputDefault
                      ),
  ALGOTEST_INITIALIZER(rowFlatGemmBTransposed,
  dummy_init_line_flat,
  dummy_init_col_flat_transposed,
  printValueStdFlat,
  printValueTransFlat,
  contextCreateDefault,
  contextDestroyDefault,
  transferInputDefault,
  transferOutputDefault
                      ),
  ALGOTEST_INITIALIZER(rowBlockGemmBStd,
  dummy_init_line_blocks,
  dummy_init_col_blocks,
  printValueStdBlock,
  printValueStdBlock,
  contextCreateDefault,
  contextDestroyDefault,
  transferInputDefault,
  transferOutputDefault
                      ),
  ALGOTEST_INITIALIZER(rowBlockGemmBTransposed,
  dummy_init_line_blocks,
  dummy_init_col_blocks_transposed,
  printValueStdBlock,
  printValueTransBlock,
  contextCreateDefault,
  contextDestroyDefault,
  transferInputDefault,
  transferOutputDefault
                      ),
#endif
#if HAVE_CUDAGPU > 0
  ALGOTEST_INITIALIZER(rowFlatCUDAGPUBStd,
  dummy_init_line_flat,
  dummy_init_col_flat,
  printValueStdFlat,
  printValueStdFlat,
  contextCreateCUDAGPU,
  contextDestroyCUDAGPU,
  transferInputCUDAGPU,
  transferOutputCUDAGPU
                      ),
#endif
};

/* Cyclic multiplication of matrix blocks. The matrix B is already
   transposed, and provided through ORWL handles. One to write the
   modified matrix row at each phase, and one to read the row in the
   current phase and one read the remote row for the next phase. */
void cyclicMult(size_t mytid, size_t n0, size_t n1, size_t n2, size_t nt,
                float_type C[nt*n0*n2],
                float_type const A[nt*n0*n1],
                orwl_alloc_ref* allocB,
                orwl_circulate * circ,
                void* transBuffer,
                algoTest const* func,
                void* context) {
  /* The blocks of C will accumulate the result. Initialize them
     to all 0.0 */
  P99_MEMZERO(float_type, C, nt*n0*n2);
  func->input(n0, n1, nt, A, context);
  for (size_t orwl_phase = 0; orwl_phase < nt; ++orwl_phase) ORWL_TIMER(phase) {
    /* Replace the data in B by the row that we receive in
       the previous phase. */
    ORWL_CIRCULATE_SECTION(circ, allocB) ORWL_TIMER(circulate) {
      /* Obtain a pointer to the buffer in our virtual address space. */
      void const*restrict curB = orwl_circulate_map(circ);

      /* keep track of the current row of B that we handle in this
         phase. We start with mytid and then at each phase add one
         around the circle. */
      size_t iB = (mytid + orwl_phase) % nt;
      func->mult(n0, n1, n2, iB, nt, C, A, curB, transBuffer, context);
    }
  }
  func->output(n0, n2, nt, C, context);
}

orwl_alloc* alloc_file(char const* name, size_t size) {
  char buff[strlen(name) + 50];
  sprintf(buff, "%s-%03zX.dat", name, orwl_mytid);
  orwl_alloc* ret = P99_NEW(orwl_alloc, buff, orwl_alloc_realloc_file);
  if (!ret->size || size || ret->max_size) {
    if (!size) size = ret->max_size;
    orwl_alloc_realloc(ret, size);
  }
  return ret;
}

static
size_t divRem(size_t n, size_t mytid, size_t nt) {
  size_t div = n / nt;
  size_t rem = n % nt;
  return div + (mytid < rem);
}


static
size_t computeShare(size_t n, size_t mytid, size_t nt) {
  /* all tasks perform the same calculations to know how the charge is
     distributed */
  double total = 0;
  for (size_t i = 0; i < nt; ++i)
    total += algoWeight[i];
  report(1, "share in computation is %g", algoWeight[mytid]/total);
  size_t weight[nt];
  double ntotal = 0;
  for (size_t i = 0; i < nt; ++i) {
    weight[i] = (n * algoWeight[i])/total;
    ntotal += weight[i];
  }
  while (ntotal < n) {
    for (size_t i = 0; i < nt && ntotal < n; ++i) {
      ++weight[i];
      ++ntotal;
    }
  }
  return weight[mytid];
}

ORWL_DEFINE_TASK(task_obj) {
  size_t const nt = orwl_nt;
  size_t const mytid = orwl_mytid;
  /* This will solve a matrix multiplication problem of nt x nt blocks
     of size n x n. Each task will hold a row of the matrix A, and the
     columns of the matrix B will circulate. The result is a row of
     matrix C.*/
  /* In the general case all matrix blocks will have different sizes,
     but still it arises quite often that they are all the same. */
  if (!opt_M) {
    if (opt_N) opt_M = opt_N;
    else if (opt_K) opt_M = opt_K;
  }
  if (!opt_N) {
    if (opt_M) opt_N = opt_M;
    else if (opt_K) opt_N = opt_K;
  }
  if (!opt_K) {
    if (opt_N) opt_K = opt_N;
    else if (opt_M) opt_K = opt_M;
  }
  size_t const n0 = (opt_m ? opt_m : computeShare(opt_M, mytid, nt));
  size_t const n1 = (opt_k ? opt_k : divRem(opt_K, mytid, nt));
  size_t const n2 = (opt_n ? opt_n : divRem(opt_N, mytid, nt));

  /* The first dimension, number of lines in A, can be split
     differently among the tasks. */
  /* The total size of the second and third dimension must still be
     divisible by nt. */
  assert(opt_k || (opt_K && n1*nt == opt_K));
  assert(opt_n || (opt_N && n2*nt == opt_N));

  /* */
  typedef float_type const blockA[n0][n1];
  typedef blockA rowA[nt];

  /* B is already transposed. */
  typedef float_type const blockB[n2][n1];
  typedef blockB rowB[nt];

  /* C must be writable. */
  typedef float_type blockC[n0][n2];
  typedef blockC rowC[nt];

  orwl_alloc_ref allocA = ORWL_ALLOC_REF_INITIALIZER(opt_afile ? alloc_file(opt_afile, 0) : P99_NEW(orwl_alloc, 0, 0, sizeof(rowA)));
  orwl_alloc_ref allocB = ORWL_ALLOC_REF_INITIALIZER(opt_bfile ? alloc_file(opt_bfile, 0) : P99_NEW(orwl_alloc, 0, 0, sizeof(rowB)));
  orwl_alloc_ref allocBt = ORWL_ALLOC_REF_INITIALIZER(P99_NEW(orwl_alloc, 0, 0, sizeof(rowB)));
  orwl_alloc_ref allocC = ORWL_ALLOC_REF_INITIALIZER(opt_outfile ? alloc_file(opt_outfile, sizeof(rowC)) : P99_NEW(orwl_alloc, 0, 0, sizeof(rowC)));

  bool mustInitA = !opt_afile || !orwl_alloc_ref_get(&allocA)->max_size;
  bool mustInitB = !opt_bfile || !orwl_alloc_ref_get(&allocB)->max_size;


  REPORT(1, "starting task with parameters %s %s %s", n0, n1, n2);
  report(1, "FLOAT_TYPE is %s", P99_STRINGIFY(FLOAT_TYPE));
  report(!mytid && opt_afile, "A is taken from file %s", opt_afile);
  report(!mytid && opt_bfile, "B is taken from file %s", opt_bfile);
  report(!mytid && opt_outfile, "C is written to file %s", opt_outfile);
  REPORT(!mytid, "total matrix size: A %s, B %s, C %s",
         nt*sizeof(rowA), nt*sizeof(rowB), nt*sizeof(rowC));
  REPORT(!mytid, "total operation: mult and add %s",
         nt*n0*n1*n2);

  /* A chunk of matrix B will circulate among the tasks. */
  orwl_circulate circB = ORWL_CIRCULATE_INITIALIZER;

  ORWL_TIMER(, opt_i*nt*nt*(2*n0+1)*n1*n2) {
    ORWL_TIMER(schedule) {

      /* The initial scheduling is simple, here. We only use one
         location per task, through which the matrix B will
         circulate. */
      orwl_circulate_insert(&circB, locationB);

      /* Now synchronize to have all requests inserted orderly at the
         other end. */
      orwl_schedule();
    }


    /* The row of A can just be hold during the whole of the algorithm. */
    ORWL_TIMER(run, opt_i*nt*nt*(2*n0+1)*n1*n2) {
      size_t f = algoID[mytid];
      report(1, "using algo %s", algoMM_getname(f));
      if (f > algoMM_max) p00_constraint_call(EINVAL, __FILE__, __func__, "invalid algo specification");
      if (!funcTab[f].name) p00_constraint_call(ENOSYS, __FILE__, __func__, "algo not supported, here");
      ORWL_ALLOC_SECTION(aB, &allocB, sizeof(rowB)) {
        if (mustInitB) funcTab[f].initB(mytid, n0, n1, n2, nt, aB);
        funcTab[f].printInColumnMatrix("B", mytid, n1*nt, n2*nt, nt,
                                       false, aB, 0, n2*nt - 1);
        funcTab[f].printInColumnMatrix("B", mytid, n1*nt, n2*nt, nt,
                                       false, aB, n1*nt/2, n2*nt/2);
        funcTab[f].printInColumnMatrix("B", mytid, n1*nt, n2*nt, nt,
                                       false, aB,n1*nt - 1, 0);
      }
      fflush(0);
      ORWL_ALLOC_SECTION(curA, &allocA, (mustInitA ? sizeof(rowA) : ORWL_ALLOC_FETCH)) {
        if (mustInitA) funcTab[f].initA(mytid, n0, n1, n2, nt, curA);
        funcTab[f].printInLineMatrix("A", mytid, n0*nt, n1*nt, nt,
                                     true, curA, 0, n1*nt - 1);
        funcTab[f].printInLineMatrix("A", mytid, n0*nt, n1*nt, nt,
                                     true, curA, n0*nt/2, n1*nt/2);
        funcTab[f].printInLineMatrix("A", mytid, n0*nt, n1*nt, nt,
                                     true, curA, n0*nt - 1, 0);
        /* a dummy object to hold the transposed B. */
        ORWL_ALLOC_SECTION(traB, &allocBt, ORWL_ALLOC_MAP) {
          ORWL_ALLOC_SECTION(curC, &allocC, ORWL_ALLOC_SNATCH) {
            void* context = funcTab[f].create(0, n0, n1, n2, nt);
            for (size_t i = 0; i < opt_i; ++i) ORWL_TIMER(cyclicMult, nt*nt*(2*n0+1)*n1*n2) {
              cyclicMult(mytid, n0, n1, n2, nt,
                         curC, curA,
                         &allocB, &circB,
                         traB,
                         &funcTab[f],
                         context);
              funcTab[f].printInLineMatrix("C", mytid, n0*nt, n2*nt, nt,
                                           true, curC, 0, n2*nt - 1);
              funcTab[f].printInLineMatrix("C", mytid, n0*nt, n2*nt, nt,
                                           true, curC, n0*nt/2, n2*nt/2);
              funcTab[f].printInLineMatrix("C", mytid, n0*nt, n2*nt, nt,
                                           true, curC, n0*nt - 1, 0);
            }
            funcTab[f].destroy(context);
          }
        }
      }
    }
    orwl_alloc_ref_destroy(&allocC);
    orwl_alloc_ref_destroy(&allocBt);
    orwl_alloc_ref_destroy(&allocB);
    orwl_alloc_ref_destroy(&allocA);
    orwl_circulate_disconnect(&circB);
    orwl_stop_task();
  }
}

#define LOG(STR) fputs(STR "\n", stderr);

void usage(void) {
  LOG("Execute a series of matrix multiplications that are composed");
  LOG("of one or several basic multiplication algorithms.");
  LOG("The different algorithms that can be triggered through ``-R'':");
  for (algoMM i = algoMM_min; i < algoMM_amount; ++i) {
    fprintf(stderr, "\t%s\n", algoMM_getname(i));
  }
  LOG("");
  LOG("These algorithms are used round robin by the tasks.");
  LOG("");
  LOG("The first dimension of the matrix A (number of lines)");
  LOG("can be split unevenly by using weights according to");
  LOG(" the option ``-W''. Eg.");
  LOG("");
  LOG("-T rowFlatGemmBStd,rowFlatGemmBTransposed -W 1.5,2");
  LOG("");
  LOG("would use the untransposed version of @c dgemm with a weight");
  LOG("of @c 1.5 and the transposed version with a weight of @c 2.");
  LOG("So the first would only be requested to perform 75% of the flops");
  LOG("than the second");
}

