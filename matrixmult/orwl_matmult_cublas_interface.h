/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the author and copyright holder for this work is                           */
/* all rights reserved,  2010-2011, 2013 Jens Gustedt, INRIA, France          */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#ifndef ORWL_MATMULT_CUBLAS_INTERFACE_H_
#define ORWL_MATMULT_CUBLAS_INTERFACE_H_

#include "orwl_matmult_float_type.h"
#include "orwl_matmult.h"
#include "orwl_cudaObject.h"
#include "orwl_matmult_cublas.h"

extern void* contextCreateCUDAGPU(int id, size_t n0, size_t n1, size_t n2, size_t nt);

extern void contextDestroyCUDAGPU(void* cxt);

extern void transferInputCUDAGPU(size_t n0, size_t n1,
                                 size_t nt,
                                 float_type const A[n0*(nt*n1)],
                                 void* ctx);

extern void transferOutputCUDAGPU(size_t n0, size_t n1,
                                  size_t nt,
                                  float_type C[n0*(nt*n1)],
                                  void* ctx);

ALGOTEST_DECLARE(rowFlatCUDAGPUBStd);

#endif
