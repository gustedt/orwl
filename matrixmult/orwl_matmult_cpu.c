/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the authors and copyright holders for this work are as follows:            */
/* all rights reserved,  2013 Jens Gustedt, INRIA, France                     */
/* all rights reserved,  2013 Stephane Vialle, SUPELEC, France                */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#include "orwl_matmult_cpu.h"
#include "orwl_dotproduct.h"
#include "orwl_task_obj.h"
#include "orwl_qresource.h"

void* contextCreateDefault(int id, size_t n0, size_t n1, size_t n2, size_t nt) {
  return 0;
}

void contextDestroyDefault(void* context) {
  // empty
}

void transferInputDefault(size_t n0, size_t n1,
                          size_t nt,
                          float_type const A[n0*(nt*n1)],
                          void* context) {
  // empty
}

void transferOutputDefault(size_t n0, size_t n1,
                           size_t nt,
                           float_type C[n0*(nt*n1)],
                           void* context) {
  // empty
}


/**
 ** @brief Multiplication of contiguous matrix blocks. The matrix B is
 ** already transposed.
 **/
static
void mult(size_t n0, size_t n1, size_t n2,
          float_type C[restrict n0][n2],
          float_type const A[n0][n1],
          float_type const B[n2][n1]) {
  P99_PARALLEL_DO(size_t, i0, 0, n0) {
    float_type const*restrict Arow = A[i0];
    P99_DO(size_t, i2, 0, n2) {
      C[i0][i2] += orwl_dotproduct(n1, Arow, B[i2]);
    }
  }
}

/**
 ** @brief Multiplication of contiguous matrix blocks. The matrix B is
 ** already transposed.
 **
 ** This function supposes that the target matrix is a row matrix with
 ** row length @a nc starting at column offset @a offc.
 **/
static
void multStride(size_t n0, size_t n1, size_t n2,
                size_t offc, size_t nc,
                float_type C[restrict n0][nc],
                float_type const A[n0][n1],
                float_type const B[n2][n1]) {
  P99_PARALLEL_DO(size_t, i0, 0, n0) {
    float_type const*restrict Arow = A[i0];
    P99_DO(size_t, i2, 0, n2) {
      C[i0][offc+i2] += orwl_dotproduct(n1, Arow, B[i2]);
    }
  }
}

void transpose(size_t n0, size_t n1, float_type B[n1][n0], float_type const A[n0][n1]) {
  P99_PARALLEL_DO(size_t, i0, 0, n0) {
    float_type const*restrict Arow = A[i0];
    P99_DO(size_t, i1, 0, n1) {
      B[i1][i0] = Arow[i1];
    }
  }
}

/**
 ** @brief Multiplication of contiguous matrix blocks. The matrix B is
 ** first transposed into auxiliary buffer @a buf.
 **/
static
void mult_and_transpose(size_t n0, size_t n1, size_t n2,
                        float_type C[restrict n0][n2],
                        float_type const A[n0][n1],
                        float_type const B[n1][n2],
                        float_type buf[n2][n1]) {
  transpose(n1, n2, buf, B);
  mult(n0, n1, n2, C, A, (void*)buf);
}

void
ALGOTEST_FUNC(rowBlockMultBTransposed)(size_t n0, size_t n1, size_t n2,
                                       size_t iB, size_t nt,
                                       float_type curC[n0*(nt*n2)],
                                       float_type const curA[n0*(nt*n1)],
                                       float_type const curB[(nt*n1)*n2],
                                       void*transBuffer,
                                       void* context) {
  ORWL_TIMER(, nt*n0*n1*n2) {
    /* Declare the lengths in all dimensions. */
    float_type const (*restrict curRowA)[nt][n0][n1] = (void*)curA;
    /* B is already transposed. */
    float_type const (*restrict curRowB)[nt][n2][n1] = (void*)curB;
    float_type (*restrict curRowC)[nt][n0][n2] = (void*)curC;

    ORWL_QSECTION(L2)
    P99_DO(size_t, k, 0, nt) ORWL_TIMER(mult, 2*(n0+1)*n1*n2) {
      mult(n0, n1, n2, (*curRowC)[iB], (*curRowA)[k], (*curRowB)[k]);
    }
  }
}

void
ALGOTEST_FUNC(rowBlockMultBStd)(size_t n0, size_t n1, size_t n2,
                                size_t iB, size_t nt,
                                float_type curC[n0*(nt*n2)],
                                float_type const curA[n0*(nt*n1)],
                                float_type const curB[(nt*n1)*n2],
                                void*transBuffer,
                                void* context) {
  ORWL_TIMER(, nt*n0*n1*n2) {
    /* Declare the lengths in all dimensions. */
    float_type const (*restrict curRowA)[nt][n0][n1] = (void*)curA;
    float_type const (*restrict curColumnB)[nt][n1][n2] = (void*)curB;
    float_type (*restrict curRowC)[nt][n0][n2] = (void*)curC;

    ORWL_QSECTION(L2)
    P99_DO(size_t, k, 0, nt) ORWL_TIMER(mult_and_transpose, 2*(n0+1)*n1*n2) {
      mult_and_transpose(n0, n1, n2, (*curRowC)[iB], (*curRowA)[k], (*curColumnB)[k], transBuffer);
    }
  }
}

void
ALGOTEST_FUNC(rowFlatMultBTransposed)(size_t n0, size_t n1, size_t n2,
                                      size_t iB, size_t nt,
                                      float_type curC[n0*(nt*n2)],
                                      float_type const curA[n0*(nt*n1)],
                                      float_type const curB[(nt*n1)*n2],
                                      void*transBuffer,
                                      void* context) {
  ORWL_TIMER(, nt*n0*n1*n2) {
    /* Declare the lengths in all dimensions. */
    float_type const (*restrict curRowA)[n0][nt*n1] = (void*)curA;
    /* B is already transposed. */
    float_type const (*restrict curRowB)[n2][nt*n1] = (void*)curB;
    float_type (*restrict curRowC)[n0][nt*n2] = (void*)curC;

    ORWL_QSECTION(L2)
    multStride(n0, nt*n1, n2, iB*n2, nt*n2, (*curRowC), (*curRowA), (*curRowB));
  }
}

void
ALGOTEST_FUNC(rowFlatMultBStd)(size_t n0, size_t n1, size_t n2,
                               size_t iB, size_t nt,
                               float_type curC[n0*(nt*n2)],
                               float_type const curA[n0*(nt*n1)],
                               float_type const curB[(nt*n1)*n2],
                               void*transBuffer,
                               void* context) {
  ORWL_TIMER(, nt*n0*n1*n2) {
    /* B is not transposed. */
    float_type const (*restrict curColB)[nt*n1][n2] = (void*)curB;
    float_type (*restrict curRowBtrans)[n2][nt*n1] = (void*)transBuffer;
    /* only scope this L2 section to transpose,
       rowFlatMultBTransposed already has one */
    ORWL_QSECTION(L2)
    transpose(nt*n1, n2, *curRowBtrans, *curColB);
    ALGOTEST_FUNC(rowFlatMultBTransposed)(n0, n1, n2, iB, nt, curC, curA, &(*curRowBtrans)[0][0], 0, context);
  }
}

