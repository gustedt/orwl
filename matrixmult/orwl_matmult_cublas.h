/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the author and copyright holder for this work is                           */
/* all rights reserved,  2013 Jens Gustedt, INRIA, France                     */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#ifndef ORWL_MATMULT_CUBLAS_H_
#define ORWL_MATMULT_CUBLAS_H_

#include "orwl_cudaObject.h"
#include "orwl_matmult_float_type.h"

#ifdef __cplusplus
extern "C" {
#endif

  extern void GPU_device_init(void);

  extern void orwl_cublasInit(cublasHandle_t ** pt_adr_handle);
  extern void orwl_cublasDestroy(cublasHandle_t ** pt_adr_handle);

  extern void orwl_cublasProduct(cublasHandle_t * pt_handle,
                                 size_t n0, size_t n1, size_t n2, size_t iB, size_t nt,
                                 orwl_cudaObject* addr_A,
                                 orwl_cudaObject* addr_B,
                                 orwl_cudaObject* addr_AxB,
                                 orwl_cudaObject* addr_C);


#ifdef __cplusplus
}
#endif


#endif
