export ORWL_NODEFILE=/usr/users/info/vialle/Proj-ORWL/orwl/matrixmult/mach_cam16.txt
#
../scripts/orwlrun output-cpu-double-gus2gm default 1 1:$1 orwl_matmult -M 6894 -i 10 -T rowFlatGemmBStd -2 $2
sleep 5s
#
../scripts/orwlrun output-cpu-double-gus2gm default 2 2:$1 orwl_matmult -M 10422 -i 10 -T rowFlatGemmBStd -2 $2
sleep 5s
#
../scripts/orwlrun output-cpu-double-gus2gm default 4 4:$1 orwl_matmult -M 15300 -i 10 -T rowFlatGemmBStd -2 $2
sleep 5s
#
../scripts/orwlrun output-cpu-double-gus2gm default 6 6:$1 orwl_matmult -M 18900 -i 10 -T rowFlatGemmBStd -2 $2
sleep 5s
#
../scripts/orwlrun output-cpu-double-gus2gm default 8 8:$1 orwl_matmult -M 21888 -i 10 -T rowFlatGemmBStd -2 $2
sleep 5s
#
../scripts/orwlrun output-cpu-double-gus2gm default 10 10:$1 orwl_matmult -M 24840 -i 10 -T rowFlatGemmBStd -2 $2
sleep 5s
#
../scripts/orwlrun output-cpu-double-gus2gm default 12 12:$1 orwl_matmult -M 27216 -i 10 -T rowFlatGemmBStd -2 $2
sleep 5s
#
../scripts/orwlrun output-cpu-double-gus2gm default 14 14:$1 orwl_matmult -M 29484 -i 10 -T rowFlatGemmBStd -2 $2
sleep 5s
#
../scripts/orwlrun output-cpu-double-gus2gm default 16 16:$1 orwl_matmult -M 30240 -i 10 -T rowFlatGemmBStd -2 $2
sleep 5s
#
