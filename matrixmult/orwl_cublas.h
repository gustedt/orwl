// This may look like C code, but it really is -*- mode: c++; coding: utf-8 -*-
//
// Except for parts copied from previous work and as explicitly stated below,
// the authors and copyright holders for this work are as follows:
// all rights reserved,  2013 Jens Gustedt, INRIA, France
// all rights reserved,  2013 Stephane Vialle, SUPELEC, France
//
// This file is part of the ORWL project. You received this file as as
// part of a confidential agreement and you may generally not
// redistribute it and/or modify it, unless under the terms as given in
// the file LICENSE.  It is distributed without any warranty; without
// even the implied warranty of merchantability or fitness for a
// particular purpose.
//
#ifndef ORWL_CUBLAS_H_
#define ORWL_CUBLAS_H_

#ifdef HAVE_CUDAGPU

#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>
#include <cublas_v2.h>


// Size def of Blocks of threads
#define BLOCK_SIZE_XY_TK0 32

inline
char const* cublasStatusString(cublasStatus_t status) {
  switch (status) {
  case CUBLAS_STATUS_NOT_INITIALIZED: return "CUBLAS library not initialized";
  case CUBLAS_STATUS_ALLOC_FAILED:    return "resource allocation failed";
  case CUBLAS_STATUS_INVALID_VALUE:   return "unsupported numerical value was passed to function";
  case CUBLAS_STATUS_ARCH_MISMATCH:   return "function requires an architectural feature absent from the architecture of the device";
  case CUBLAS_STATUS_MAPPING_ERROR:   return "access to GPU memory space failed CUBLAS_STATUS_EXECUTION_FAILED GPU program failed to execute";
  case CUBLAS_STATUS_INTERNAL_ERROR:  return "an internal CUBLAS operation failed";
  default:                            return "an unknown CUBLAS error occured";
  }
}

#define CHECK_CUBLAS_SUCCESS(EXP, MSG)                                                                  \
do {                                                                                                    \
  cublasStatus_t status = (EXP);                                                                        \
  if (status != CUBLAS_STATUS_SUCCESS) {                                                                \
    fprintf(stderr,"Error %d on CUBLAS operation, %s: %s\n", status, (MSG), cublasStatusString(status));\
    exit(EXIT_FAILURE);                                                                                 \
  }                                                                                                     \
} while(0)

#endif

#endif /* ORWL_MATMULT_CUBLAS_H_ */
