/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the authors and copyright holders for this work are as follows:            */
/* all rights reserved,  2013-2014 Jens Gustedt, INRIA, France                */
/* all rights reserved,  2013 Stephane Vialle, SUPELEC, France                */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#ifndef ORWL_MATMULT_H_
#define ORWL_MATMULT_H_
#include "orwl_circulate.h"
#include "orwl_task_obj.h"
#include "orwl_dotproduct.h"
#include "orwl_matmult_float_type.h"

/**
 ** @brief A prototype for initialization of a matrix
 **/
typedef void* initFunc(size_t, size_t, size_t, size_t, size_t, float_type[]);

/**
 ** @brief A prototype to print some values of a matrix
 **/
typedef void printFunc(char const*, size_t, size_t, size_t, size_t, bool, float_type const[], size_t, size_t);
/**
 ** @brief A prototype to multiply matrices
 **/
typedef void multFunc(size_t, size_t, size_t, size_t, size_t,
                      float_type[], float_type const[], float_type const[],
                      void*, void*);

/**
 ** @brief A prototype to create a context on a coprocessor
 **/
typedef void* contextCreateFunc(int, size_t, size_t, size_t, size_t);
/**
 ** @brief A prototype to destroy a context on a coprocessor
 **/
typedef void contextDestroyFunc(void*);

/**
 ** @brief A prototype to transfer matrix A to a coprocessor
 **/
typedef void transferInputFunc(size_t, size_t, size_t, float_type const[], void*);

/**
 ** @brief A prototype to transfer matrix C from a coprocessor
 **/
typedef void transferOutputFunc(size_t, size_t, size_t, float_type [], void*);


P99_DECLARE_ENUM(algoMM,
                 rowBlockMultBTransposed,
                 rowBlockMultBStd,
                 rowFlatMultBTransposed,
                 rowFlatMultBStd,
                 rowBlockGemmBTransposed,
                 rowBlockGemmBStd,
                 rowFlatGemmBTransposed,
                 rowFlatGemmBStd,
                 rowFlatCUDAGPUBStd
                );

P99_DECLARE_STRUCT(algoTest);

/**
 ** @brief A structure that holds the different components needed for
 ** a matrix benchmark.
 **/
struct algoTest {
  char const* name;
  multFunc* mult;
  initFunc* initA;
  initFunc* initB;
  printFunc* printInLineMatrix;
  printFunc* printInColumnMatrix;
  contextCreateFunc* create;
  contextDestroyFunc* destroy;
  transferInputFunc* input;
  transferOutputFunc* output;
};

#define ALGOTEST_FUNC(NAME) algo_ ## NAME ## _func

#define ALGOTEST_DECLARE(NAME)                                 \
  void                                                         \
  ALGOTEST_FUNC(NAME)(size_t n0, size_t n1, size_t n2,         \
                      size_t iB, size_t nt,                    \
                      float_type curC[n0*(nt*n2)],             \
                      float_type const curA[n0*(nt*n1)],       \
                      float_type const curB[(nt*n1)*n2],       \
                      void*transBuffer,                        \
                      void* context)


#define ALGOTEST_INITIALIZER(MULT, INITA, INITB, PRINTLINE, PRINTCOLUMN, CREATE, DESTR, INPUT, OUTPUT) \
[MULT] = {                                                                                             \
  .name = P99_STRINGIFY(MULT),                                                                         \
    .mult = ALGOTEST_FUNC(MULT),                                                                       \
    .initB = INITB,                                                                                    \
    .initA = INITA,                                                                                    \
    .printInLineMatrix = PRINTLINE,                                                                    \
    .printInColumnMatrix = PRINTCOLUMN,                                                                \
    .create = CREATE,                                                                                  \
    .destroy = DESTR,                                                                                  \
    .input = INPUT,                                                                                    \
    .output = OUTPUT,                                                                                  \
    }

extern size_t const* algoID;
extern double const* algoWeight;

#endif
