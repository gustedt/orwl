#!/usr/bin/gnuplot -persist
#
#    
#    	G N U P L O T
#    	Version 4.4 patchlevel 3
#    	last modified March 2011
#    	System: Linux 3.8.2
#    
#    	Copyright (C) 1986-1993, 1998, 2004, 2007-2010
#    	Thomas Williams, Colin Kelley and many others
#    
#    	gnuplot home:     http://www.gnuplot.info
#    	faq, bugs, etc:   type "help seeking-assistance"
#    	immediate help:   type "help"
#    	plot window:      hit 'h'
file = "timing.csv"
nodes = 4
blocksize = 128
set output "timing.eps"
# set terminal wxt 0
# set output
set terminal postscript color
unset clip points
set clip one
unset clip two
set bar 1.000000 front
set border 31 front linetype -1 linewidth 1.000
set xdata
set ydata
set zdata
set x2data
set y2data
set timefmt x "%d/%m/%y,%H:%M"
set timefmt y "%d/%m/%y,%H:%M"
set timefmt z "%d/%m/%y,%H:%M"
set timefmt x2 "%d/%m/%y,%H:%M"
set timefmt y2 "%d/%m/%y,%H:%M"
set timefmt cb "%d/%m/%y,%H:%M"
set boxwidth
set style fill  empty border
set style rectangle back fc lt -3 fillstyle   solid 1.00 border lt -1
#set style circle radius graph 0.02, first 0, 0 
set dummy x,y
set format x "% g"
set format y "% g"
set format x2 "% g"
set format y2 "% g"
set format z "% g"
set format cb "% g"
set angles radians
unset grid
set key title ""
set key inside right bottom vertical Right noreverse enhanced autotitles columnhead nobox
set key noinvert samplen 4 spacing 1 width 0 height 0 
#set key maxcolumns 0 maxrows 0
unset label
unset arrow
set style increment default
unset style line
unset style arrow
set style histogram clustered gap 2 title  offset character 0, 0, 0
set logscale xy
set offsets 0, 0, 0, 0
set pointsize 1
#set pointintervalbox 1
set encoding default
unset polar
unset parametric
unset decimalsign
set view 60, 30, 1, 1
set samples 100, 100
set isosamples 10, 10
set surface
unset contour
set clabel '%8.3g'
set mapping cartesian
set datafile separator whitespace
unset hidden3d
set cntrparam order 4
set cntrparam linear
set cntrparam levels auto 5
set cntrparam points 5
set size ratio 0 1,1
set origin 0,0
set style data linespoints
set style function lines
set xzeroaxis linetype -2 linewidth 1.000
set yzeroaxis linetype -2 linewidth 1.000
set zzeroaxis linetype -2 linewidth 1.000
set x2zeroaxis linetype -2 linewidth 1.000
set y2zeroaxis linetype -2 linewidth 1.000
set ticslevel 0.5
set mxtics default
set mytics default
set mztics default
set mx2tics default
set my2tics default
set mcbtics default
set xtics border in scale 1,0.5 mirror norotate  offset character 0, 0, 0
set xtics autofreq  norangelimit
set ytics border in scale 1,0.5 mirror norotate  offset character 0, 0, 0
set ytics autofreq  norangelimit
set ztics border in scale 1,0.5 nomirror norotate  offset character 0, 0, 0
set ztics autofreq  norangelimit
set nox2tics
set noy2tics
set cbtics border in scale 1,0.5 mirror norotate  offset character 0, 0, 0
set cbtics autofreq  norangelimit
set title  offset character 0, 0, 0 font "" norotate
set timestamp bottom 
set timestamp "" 
set timestamp  offset character 0, 0, 0 font "" norotate
set rrange [ * : * ] noreverse nowriteback  # (currently [8.98847e+307:-8.98847e+307] )
set trange [ * : * ] noreverse nowriteback  # (currently [-5.00000:5.00000] )
set urange [ * : * ] noreverse nowriteback  # (currently [-10.0000:10.0000] )
set vrange [ * : * ] noreverse nowriteback  # (currently [-10.0000:10.0000] )
set xlabel "ORWL tasks" 
set xlabel  offset character 0, 0, 0 font "" textcolor lt -1 norotate
set x2label "" 
set x2label  offset character 0, 0, 0 font "" textcolor lt -1 norotate
set xrange [ * : * ] noreverse nowriteback  # (currently [1.00000:4.00000] )
set x2range [ * : * ] noreverse nowriteback  # (currently [1.00000:4.00000] )
set ylabel "flop/s" 
set ylabel  offset character 0, 0, 0 font "" textcolor lt -1 rotate by -270
set y2label "" 
set y2label  offset character 0, 0, 0 font "" textcolor lt -1 rotate by -270
set yrange [ * : * ] noreverse nowriteback  # (currently [0.00000:1.80000] )
set y2range [ * : * ] noreverse nowriteback  # (currently [0.0805182:1.64124] )
set zlabel "" 
set zlabel  offset character 0, 0, 0 font "" textcolor lt -1 norotate
set zrange [ * : * ] noreverse nowriteback  # (currently [-10.0000:10.0000] )
set cblabel "" 
set cblabel  offset character 0, 0, 0 font "" textcolor lt -1 rotate by -270
set cbrange [ * : * ] noreverse nowriteback  # (currently [8.98847e+307:-8.98847e+307] )
set zero 1e-08
set lmargin  -1
set bmargin  -1
set rmargin  -1
set tmargin  -1
set pm3d explicit at s
set pm3d scansautomatic
set pm3d interpolate 1,1 flush begin noftriangles nohidden3d corners2color mean
set palette positive nops_allcF maxcolors 0 gamma 1.5 color model RGB 
set palette rgbformulae 7, 5, 15
set colorbox default
set colorbox vertical origin screen 0.9, 0.2, 0 size screen 0.05, 0.6, 0 front bdefault
set loadpath 
set fontpath 
set fit noerrorvariables
GNUTERM = "wxt"
# list the different points of messure that we take on each run
main = 0
task_obj_start = 1
rowFlatGemmBStd = 2
rowFlatGemmBTransposed = 3
rowBlockGemmBStd = 4
rowBlockGemmBTransposed = 5
rowFlatMultBStd = 6
rowFlatMultBTransposed = 7
rowBlockMultBStd = 8
rowBlockMultBTransposed = 9
# each point of measure has 5 different values
numberc(x) = 5*x + 1
timec(x)   = 5*x + 2
timenc(x)  = 5*x + 3
devc(x)    = 5*x + 4
countc(x)  = 5*x + 5
# The resource counter gives the number of multiplications per call
# Multiply by two to have all flops.
flop(x) = 2*column(countc(rowFlatGemmBStd))
# The number of processes is given by number of calls to main
processes(x) = column(numberc(main))
# The number of tasks is given by number of calls to the task starting
# routine
tasks(x) = column(numberc(task_obj_start))
rtime(x) = x/tasks(0)
flops(x) = flop(0)/rtime(column(timec(x)))
hasproc(p) = (processes(0) == p ? tasks(0) : NaN)
ptitle(p) = sprintf("%d proc, ", p)
set title sprintf("matrix multiplication block size %dx%d, %d graphene nodes, with OpenMp", blocksize, blocksize, nodes)
plot                                                                                  \
	 file using (hasproc(4)):(flops(rowFlatGemmBTransposed)) title ptitle(4)."dgemm",     \
	 file using (hasproc(4)):(flops(rowBlockMultBTransposed)) title ptitle(4)."mult", \
	 file using (hasproc(8)):(flops(rowFlatGemmBTransposed)) title ptitle(8)."dgemm",     \
	 file using (hasproc(8)):(flops(rowBlockMultBTransposed)) title ptitle(8)."mult"
#    EOF
