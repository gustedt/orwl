export ORWL_NODEFILE=/usr/users/info/vialle/Proj-ORWL/orwl/matrixmult/mach_cam16.txt
#
../scripts/orwlrun output-cpu-double-gus default 1 1:$1 orwl_matmult -M 5760 -N 5760 -K 5760 -i 10 -T rowFlatGemmBStd -2 $2
sleep 5s
#
../scripts/orwlrun output-cpu-double-gus default 2 2:$1 orwl_matmult -M 7256 -N 7248 -K 7248 -i 10 -T rowFlatGemmBStd -2 $2
sleep 5s
#
../scripts/orwlrun output-cpu-double-gus default 3 3:$1 orwl_matmult -M 8307 -N 8316 -K 8316 -i 10 -T rowFlatGemmBStd -2 $2
sleep 5s
#
../scripts/orwlrun output-cpu-double-gus default 4 4:$1 orwl_matmult -M 9144 -N 9144 -K 9144 -i 10 -T rowFlatGemmBStd -2 $2
sleep 5s
#
../scripts/orwlrun output-cpu-double-gus default 5 5:$1 orwl_matmult -M 9850 -N 9840 -K 9840 -i 10 -T rowFlatGemmBStd -2 $2
sleep 5s
#
../scripts/orwlrun output-cpu-double-gus default 6 6:$1 orwl_matmult -M 10464 -N 10476 -K 10476 -i 10 -T rowFlatGemmBStd -2 $2
sleep 5s
#
../scripts/orwlrun output-cpu-double-gus default 8 8:$1 orwl_matmult -M 11520 -N 11520 -K 11520 -i 10 -T rowFlatGemmBStd -2 $2
sleep 5s
#
../scripts/orwlrun output-cpu-double-gus default 10 10:$1 orwl_matmult -M 12410 -N 12420 -K 12420 -i 10 -T rowFlatGemmBStd -2 $2
sleep 5s
#
../scripts/orwlrun output-cpu-double-gus default 12 12:$1 orwl_matmult -M 13188 -N 13176 -K 13176 -i 10 -T rowFlatGemmBStd -2 $2
sleep 5s
#
../scripts/orwlrun output-cpu-double-gus default 15 15:$1 orwl_matmult -M 14205 -N 14220 -K 14220 -i 10 -T rowFlatGemmBStd -2 $2
sleep 5s
#
../scripts/orwlrun output-cpu-double-gus default 16 16:$1 orwl_matmult -M 14512 -N 14592 -K 14592 -i 10 -T rowFlatGemmBStd -2 $2
sleep 5s
#
