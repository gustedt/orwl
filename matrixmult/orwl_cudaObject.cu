// This may look like C code, but it really is -*- mode: c++; coding: utf-8 -*-
//
// Except for parts copied from previous work and as explicitly stated below,
// the author and copyright holder for this work is
// all rights reserved,  2013 Jens Gustedt, INRIA, France
//
// This file is part of the ORWL project. You received this file as as
// part of a confidential agreement and you may generally not
// redistribute it and/or modify it, unless under the terms as given in
// the file LICENSE.  It is distributed without any warranty; without
// even the implied warranty of merchantability or fitness for a
// particular purpose.
//

#ifdef HAVE_CUDAGPU

#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>
#include <cuda_runtime.h>
#include <cublas_v2.h>

#include "orwl_cudaObject.h"

orwl_cudaObject* orwl_cudaObject_init(orwl_cudaObject* A, size_t s) {
  if (A) {
    A->onGPU = false;
    A->oursCPU = false;
    A->size = s;
    A->addrCPU = 0;
    CHECK_CUDA_SUCCESS(cudaMalloc(&A->addrGPU, s), "Allocation on the GPU");
  }
  return A;
}

void orwl_cudaObject_destroy(orwl_cudaObject* A) {
  CHECK_CUDA_SUCCESS(cudaFree(A->addrGPU),"Free on the GPU");
  if (A->oursCPU) free(A->addrCPU);
  A->addrGPU = 0;
  A->addrCPU = 0;
}

void orwl_cudaObject_replace(orwl_cudaObject* A, void* Arep) {
  if (A->oursCPU) free(A->addrCPU);
  A->addrCPU = Arep;
  A->oursCPU = false;
}

/* ensure that the CPU is allocated if not yet so */
static
void o_rwl_cudaObject_allocCPU(orwl_cudaObject* target) {
  if (!target->addrCPU) {
    target->addrCPU = malloc(target->size);
    target->oursCPU = true;
  }
}

void orwl_cudaObject_copy(orwl_cudaObject* target, orwl_cudaObject const* source) {
  size_t size = target->size < source->size ? target->size : source->size;
  if (target->onGPU) {
    if (source->onGPU) {
      CHECK_CUDA_SUCCESS(cudaMemcpy(target->addrGPU, source->addrGPU, size, cudaMemcpyDeviceToDevice),
                         "Problem transfer CPU --> GPU");
    } else {
      CHECK_CUDA_SUCCESS(cudaMemcpy(target->addrGPU, source->addrCPU, size, cudaMemcpyHostToDevice),
                         "Problem transfer CPU --> GPU");
    }
  } else {
    o_rwl_cudaObject_allocCPU(target);
    if (source->onGPU) {
      CHECK_CUDA_SUCCESS(cudaMemcpy(target->addrCPU, source->addrGPU, size, cudaMemcpyDeviceToHost),
                         "Problem transfer GPU --> CPU");
    } else {
      memcpy(target->addrCPU, source->addrCPU, size);
    }
  }
}

void orwl_cudaObject_set(orwl_cudaObject* target, unsigned char val) {
  if (target->onGPU) {
    CHECK_CUDA_SUCCESS(cudaMemset(target->addrGPU, val, target->size), "set to val on GPU");
  } else {
    o_rwl_cudaObject_allocCPU(target);
    memset(target->addrCPU, val, target->size);
  }
}

void orwl_cudaObject_CPU2GPU(orwl_cudaObject*A) {
  CHECK_CUDA_SUCCESS(cudaMemcpy(A->addrGPU, A->addrCPU, A->size, cudaMemcpyHostToDevice),
                     "Problem transfer CPU --> GPU");
  A->onGPU = true;
}

void orwl_cudaObject_GPU2CPU(orwl_cudaObject*A) {
  o_rwl_cudaObject_allocCPU(A);
  CHECK_CUDA_SUCCESS(cudaMemcpy(A->addrCPU, A->addrGPU, A->size, cudaMemcpyDeviceToHost),
                     "Problem transfer GPU --> CPU");
  A->onGPU = false;
}

void* orwl_cudaObject_write_map(orwl_cudaObject*A);

void const* orwl_cudaObject_read_map(orwl_cudaObject*A) {
  o_rwl_cudaObject_allocCPU(A);
  CHECK_CUDA_SUCCESS(cudaMemcpy(A->addrCPU, A->addrGPU, A->size, cudaMemcpyDeviceToHost),
                     "Problem transfer GPU --> CPU");
}

void orwl_cudaObject_unmap(orwl_cudaObject*A) {
  if (!A->onGPU)
    CHECK_CUDA_SUCCESS(cudaMemcpy(A->addrCPU, A->addrGPU, A->size, cudaMemcpyDeviceToHost),
                       "Problem transfer GPU --> CPU");
}


#endif
