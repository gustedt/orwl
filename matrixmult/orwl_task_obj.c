/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the authors and copyright holders for this work are as follows:            */
/* all rights reserved,  2011 Emmanuel Jeanvoine, INRIA, France               */
/* all rights reserved,  2012-2014 Jens Gustedt, INRIA, France                */
/* all rights reserved,  2013 Stephane Vialle, SUPELEC, France                */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#include "orwl_task_obj.h"
#include "orwl_qresource.h"
#include "orwl_matmult.h"

ORWL_LOCATIONS_PER_TASK_INSTANTIATION();

P99_GETOPT_DEFINE(n, size_t, opt_n, 0, "n", "control the nb of columns in a block of B");
P99_GETOPT_DEFINE(m, size_t, opt_m, 0, "m", "control the nb of rows in a block of A");
P99_GETOPT_DEFINE(k, size_t, opt_k, 0, "k", "control the nb of columns in a block of A (and nb of rows in a block of B) ");
P99_GETOPT_DEFINE(N, size_t, opt_N, 0, "N-colB", "control the total nb of columns of B");
P99_GETOPT_DEFINE(M, size_t, opt_M, 0, "M-ligA", "control the total nb of rows of A");
P99_GETOPT_DEFINE(K, size_t, opt_K, 0, "K-colA-ligB", "control the total nb of columns of A (and rows of B)");
P99_GETOPT_DEFINE(i, size_t, opt_i,   5, "i", "the number of test iterations");
P99_GETOPT_DEFINE(a, char const*, opt_afile, 0, "afile", "an input file name");
P99_GETOPT_DEFINE(b, char const*, opt_bfile, 0, "bfile", "a second input file name");
P99_GETOPT_DEFINE(o, char const*, opt_outfile, 0, "ofile", "an output file name");
P99_GETOPT_DEFINE(T, char const*, opt_tests, "rowBlockMultBStd", "tests", "name the test(s) that are to be performed");
P99_GETOPT_DEFINE(W, char const*, opt_weights, 0, "weights", "weights of the test(s) that are to be performed");
P99_GETOPT_DEFINE(P, unsigned, opt_P, UINT_MAX, "cpus", "control the total nb of CPUS to be used by CPU intensive functions");
P99_GETOPT_DEFINE(1, unsigned, opt_L1, UINT_MAX, "L1", "control the total nb of L1 caches to be used by CPU intensive functions");
P99_GETOPT_DEFINE(2, unsigned, opt_L2, UINT_MAX, "L2", "control the total nb of L2 caches to be used by CPU intensive functions");
P99_GETOPT_DEFINE(3, unsigned, opt_L3, UINT_MAX, "L1", "control the total nb of L3 caches to be used by CPU intensive functions");



task_obj* task_obj_init(task_obj* task) {
  if (task) {
    *task = (task_obj)P99_INIT;;
  }
  return task;
}

void task_obj_destroy(task_obj *task) {
  /* empty */
}

P99_DEFINE_DELETE(task_obj);

char** split(char const *str, char const needle[]) {
  if (!str) return P99_CALLOC(char*);
  /* skip initial garbage */
  str += strspn(str, needle);
  size_t len = strlen(str) + 1;
  char* cpy = memcpy(calloc(len+15, 1), str, len);
  char** ret = malloc(sizeof(char const*[len]));
  size_t i = 0;
  while (cpy[0]) {
    ret[i] = &cpy[0];
    ++i;
    cpy += strcspn(cpy, needle);
    if (!cpy[0]) break;
    cpy[0] = 0;
    ++cpy;
    if (!cpy[0]) break;
    cpy += strspn(cpy, needle);
  }
  ret[i] = 0;
  return ret;
}

size_t const* algoID = 0;
double const* algoWeight = 0;

int main(int argc, char **argv) {
  p99_getopt_initialize(&argc, &argv);
  ORWL_TIMER() {
    //! [condition ORWL]
    orwl_init();
    //! [condition ORWL]

    GPU_device_init();
    ORWL_QRESOURCE_INIT(CPU, opt_P);
    ORWL_QRESOURCE_INIT(L1, opt_L1);
    ORWL_QRESOURCE_INIT(L2, opt_L2);
    ORWL_QRESOURCE_INIT(L3, opt_L3);
    trace(1, "CPU usage restricted to %u (%u, %u, %u) ",
          ORWL_QRESOURCE(CPU),
          ORWL_QRESOURCE(L1),
          ORWL_QRESOURCE(L2),
          ORWL_QRESOURCE(L3)
         );
    size_t algoUsed = 0;
    if (opt_tests) {
      char** func = split(opt_tests, " :,;");
      for (char**f = func; *f; ++f) {
        ++algoUsed;
      }
      size_t * algoID_ = ORWL_SMALLOC(size_t[orwl_nt]);
      for (size_t i = 0; i < algoUsed; ++i) {
        char const* funcI = orwl_tgetenv("T", i);
        if (!funcI) funcI = func[i];
        algoID_[i] = algoMM_parse(funcI);
        report(1, "tests %zu to be performed is %s, %zu", i, funcI, algoID_[i]);
      }
      for (size_t i = algoUsed; i < orwl_nt; ++i) {
        char const* funcI = orwl_tgetenv("T", i);
        if (!funcI) funcI = func[i % algoUsed];
        algoID_[i] = algoMM_parse(funcI);
        report(1, "tests %zu to be performed is %s, %zu", i, funcI, algoID_[i]);
      }
      free(func[0]);
      free(func);
      algoID = algoID_;
    }

    {
      char** weights = split(opt_weights, " :,;");
      weights = P99_REALLOC(weights, char*[orwl_nt]);
      for (size_t i = 0; i < algoUsed; ++i) {
        if (!weights[i]) {
          for (; i < algoUsed; ++i)
            weights[i] = 0;
        }
      }
      double * algoWeight_ = ORWL_SMALLOC(double[orwl_nt]);
      for (size_t i = 0; i < algoUsed; ++i) {
        char const* weightsI = orwl_tgetenv("W", i);
        if (!weightsI) weightsI = weights[i];
        algoWeight_[i] = weightsI ? strtod(weightsI) : 1.0/orwl_nt;
        report(1, "weights %zu to be used is %s, %g", i, weightsI, algoWeight_[i]);
      }
      for (size_t i = algoUsed; i < orwl_nt; ++i) {
        char const* weightsI = orwl_tgetenv("W", i);
        if (!weightsI && algoUsed) weightsI = weights[i % algoUsed];
        algoWeight_[i] = weightsI ? strtod(weightsI) : 1.0/orwl_nt;
        report(1, "weights %zu to be used is %s, %g", i, weightsI, algoWeight_[i]);
      }
      free(weights[0]);
      free(weights);
      algoWeight = algoWeight_;
    }

    //! [launch one thread per task]
    for (size_t i = 0; i < orwl_lt; i++) {
      task_obj* task = P99_NEW(task_obj);
      task_obj_create_task(task, orwl_tids[i]);
    }
    //! [launch one thread per task]
  }
  orwl_stop();
  return EXIT_SUCCESS;
}

P99_WEAK(GPU_device_init)
void GPU_device_init(void) {
  /* empty */
}

