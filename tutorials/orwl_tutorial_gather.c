/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the author and copyright holder for this work is                           */
/* all rights reserved,  2012 Jens Gustedt, INRIA, France                     */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#include "orwl_tutorial_task_obj.h"

ORWL_ALLGATHER_LOCATION(sec_loc);

ORWL_DEFINE_TASK(task_obj) {
  ORWL_THREAD_USE(task_obj, init_val);
  size_t const mytid = orwl_mytid;

  ORWL_ALLGATHER_DECLARE(double, sec_loc, (orwl_nt - 1)*orwl_locations_amount + sec_loc);

  sec_loc[mytid] = init_val;

  /* Now synchronize again to have all requests inserted orderly at
     the other end. */
  orwl_schedule();

  for (size_t orwl_phase = 0; orwl_phase < 10; ++orwl_phase) {
    /* Store some new value of your liking */
    sec_loc[mytid] = init_val + orwl_phase + mytid;
    ORWL_ALLGATHER(sec_loc);
    if (mytid != (orwl_nt - 1)) PROGRESS(1, mytid, "found %s", sec_loc[0]);
    else {
      fprintf(stderr, ORWL_TERM_CLEAR "found the result [" ORWL_TERM_ALERT);
      for (size_t i = 0; i < orwl_nt; ++i)
        fprintf(stderr, " % 6.4f", sec_loc[i]);
      fprintf(stderr, ORWL_TERM_OFF "]\n");
    }
  }
  orwl_stop_task();
}
