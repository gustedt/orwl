/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the authors and copyright holders for this work are as follows:            */
/* all rights reserved,  2011 Emmanuel Jeanvoine, INRIA, France               */
/* all rights reserved,  2012-2014 Jens Gustedt, INRIA, France                */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#include "orwl_tutorial_task_obj.h"

int main(int argc, char **argv) {
  p99_getopt_initialize(&argc, &argv);
  GPU_device_init();

  double init_val = (argc < 2) ? (opt_d != 0.0 ? opt_d : p99_drand()) : strtod(argv[1]);

  ORWL_TIMER() {
    //! [condition ORWL]
    orwl_init();
    //! [condition ORWL]

    //! [launch one thread per task]
    for (size_t i = 0; i < orwl_lt; i++) {
      task_obj* task = P99_NEW(task_obj, init_val);
      task_obj_create_task(task, orwl_tids[i]);
    }
    //! [launch one thread per task]
  }
  return EXIT_SUCCESS;
}
