/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the authors and copyright holders for this work are as follows:            */
/* all rights reserved,  2012-2013, 2016 Jens Gustedt, INRIA, France          */
/* all rights reserved,  2013 Rodrigo Campos, INRIA, France                   */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#include "orwl_tutorial_task_obj.h"
#include "orwl_alloc_queue.h"

//! [start the task specific function definition]
ORWL_DEFINE_TASK(task_obj) {
  ORWL_THREAD_USE(task_obj, init_val);
  p99_seed* seed = p99_seed_get();
  //! [start the task specific function definition]

  //! [condition this task]
  /* Scale our own location(s) to the appropriate buffer size. */
  orwl_scale(sizeof(double));
  //! [condition this task]

  /* Sleep a bit to give the user the occasion to admire the following
     message. */
  sleepfor(0.5 * p99_drand(seed));
  /* Now ORWL knows the total amount of tasks that will be
     performed. Avoid being too verbose so let only task 0 tell it. */
  PROGRESS(1, orwl_myloc,
           "<-------- the start of the report line now should contain location ID (%s) and total number (%s)",
           orwl_myloc, orwl_nl);

  //! [connect the resources]
  /* Create handles for the locations that we are interested in. We
     will create a chain of dependencies from task 0 to task 1 etc. */
  orwl_handle2 here = ORWL_HANDLE2_INITIALIZER;
  orwl_handle2 there = ORWL_HANDLE2_INITIALIZER;

  /* Have our own location writable. */
  orwl_write_insert(&here, ORWL_LOCATION(orwl_mytid, main_loc), 0);

  /* link the "there" handle where appropriate */
  if (orwl_mytid)
    orwl_read_insert(&there, ORWL_LOCATION(orwl_mytid - 1, sec_loc), -1);

  /* Add a queue that buffers data that is produced */
  if (orwl_mytid < orwl_nt-1)
    orwl_alloc_queue(ORWL_LOCATION(orwl_mytid, main_loc), ORWL_LOCATION(orwl_mytid, sec_loc), (orwl_mytid%7));

  /* Now synchronize to have all requests inserted orderly at the
     other end. */
  orwl_schedule();
  //! [connect the resources]

  report(!orwl_mytid);
  for (size_t orwl_phase = 0; orwl_phase < opt_i; ++orwl_phase) {
    sleepfor(p99_drand(seed)*1E-3);
    //! [perform the task]
    /* All tasks create a critical section that guarantees exclusive
       access to their location. */
    ORWL_SECTION(&here) {
      /* Obtain a pointer to the buffer in our virtual address space. */
      double * wval = orwl_write_map(&here);
      if (!orwl_phase) *wval = init_val+orwl_mytid;
      /* All other tasks read the value of their predecessor. */
      if (orwl_mytid) {
        /* this will block until the data is available */
        ORWL_SECTION(&there) {
          double const* rval = orwl_read_map(&there);
          /* Do some dummy computation. */
          *wval = (*rval + *wval) * 0.5;
          PROGRESS(1, orwl_mytid, "we stored the average: %s", *wval);
        }
      }
    }
    //! [perform the task]
  }
  report(!orwl_mytid);

  //! [say good bye]
  orwl_disconnect(&here);
  orwl_disconnect(&there);
  orwl_stop_task();
  //! [say good bye]
}
