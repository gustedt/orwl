##
## Makefile
##

include ../scripts/Makefile-orwl

EXSRC	=							\
		orwl_tutorial_allreduce.c			\
		orwl_tutorial_allreduce_op.c                	\
		orwl_tutorial_broadcast.c			\
		orwl_tutorial_gather.c				\
		orwl_tutorial_gather_det_op.c			\
		orwl_tutorial_gather_op.c             		\
		orwl_tutorial_main_chain.c			\
		orwl_tutorial_main_push.c			\
		orwl_tutorial_matrix2.c				\
		orwl_tutorial_reduce.c				\
		orwl_tutorial_threadsOMP_matrix2.c		\
		orwl_tutorial_threads_allgather.c		\
		orwl_tutorial_threads_broadcast.c		\
		orwl_tutorial_threads_chain.c			\
		orwl_tutorial_threads_chain2.c			\
		orwl_tutorial_threads_circle.c			\
		orwl_tutorial_threads_circle2.c			\
		orwl_tutorial_threads_circle2hard.c		\
		orwl_tutorial_threads_circulate.c		\
		orwl_tutorial_threads_gather.c			\
		orwl_tutorial_threads_matrix.c			\
		orwl_tutorial_threads_matrix2.c			\
		orwl_tutorial_threads_producer_consumer.c	\
		orwl_tutorial_threads_reduce.c			\
		orwl_tutorial_threads_scatter.c			\
		orwl_tutorial_threads_split.c			\
		warmup.c					\
		half-active.c					\
		grid.c

CUDA_SOURCES = Calcul_CudaGpu.cu
CUDA_HEADERS = Calcul_CudaGpu.h


SRC	= ${EXSRC} orwl_tutorial_task_obj.c orwl_tutorial_task_main.c

MODULAR	=							\
		orwl_tutorial_allreduce.c			\
		orwl_tutorial_broadcast.c			\
		orwl_tutorial_gather.c				\
		orwl_tutorial_gather_det_op.c 			\
		orwl_tutorial_matrix2.c				\
		orwl_tutorial_reduce.c				\
		orwl_tutorial_threadsOMP_matrix2.c		\
		orwl_tutorial_threads_allgather.c		\
		orwl_tutorial_threads_broadcast.c		\
		orwl_tutorial_threads_chain2.c			\
		orwl_tutorial_threads_circle.c			\
		orwl_tutorial_threads_circle2.c			\
		orwl_tutorial_threads_circle2hard.c		\
		orwl_tutorial_threads_circulate.c		\
		orwl_tutorial_threads_gather.c			\
		orwl_tutorial_threads_matrix.c			\
		orwl_tutorial_threads_matrix2.c			\
		orwl_tutorial_threads_matrix2alloc.c		\
		orwl_tutorial_threads_producer_consumer.c	\
		orwl_tutorial_threads_reduce.c			\
		orwl_tutorial_threads_scatter.c                 \
		orwl_tutorial_threads_split.c

MOD_EXAMPLES = ${MODULAR:.c=}

OPERATION =					\
		orwl_tutorial_allreduce_op.c 	\
		orwl_tutorial_gather_op.c

OP_EXAMPLES = ${OPERATION:.c=}

INCL  	=		# List of *.h
LIBDIR	:= $(join $(dir ${CURDIR}),lib)
################
# Optional add #
################
OPT	?= -O3
IPATH   += -I.
OBJOPT  += -Wall ${OPT}
EXEOPT  += ${OPT}
LPATH   += -L.
COPY = "all rights reserved, "
LICENSE = SHORTLICENCE-closed.txt
SCRIPTS = ${ORWLMAKEDIR}
AUTH = ${SCRIPTS}/AUTHORS.txt
TIDYC = ../p99-source/scripts/findSvnAuthors --scm git --type c --auth ${AUTH} --copy ${COPY} --lice ${LICENSE} --ofile
TIDYCUDA = ../p99-source/scripts/findSvnAuthors --scm git --type c++ --auth ${AUTH} --copy ${COPY} --lice ${LICENSE} --ofile

ifdef DEEP_INSTRUMENTATION
CFLAGS += -DDEEP_INSTRUMENTATION
endif

ifdef DEBUG
CFLAGS += -g -fstack-protector-all
else
#LDFLAGS += -s
endif

ifneq ($(V), 1)
       NICE_CC = @echo "  CC  $@"; $(CC)
else
       NICE_CC = $(CC)
endif

#####################
# Macro Definitions #
#####################
MAKE 	= make
SHELL	= /bin/sh
OBJS 	= $(SRC:.c=.o)
NAME    = ${EXSRC:.c=}
CUDA_OBJECTS = $(CUDA_SOURCES:.cu=.o)
RM 	= /bin/rm -f
COMP	= gzip -9v
UNCOMP	= gzip -df
STRIP	= strip

ifeq (${HAVE_CUDAGPU},1)
OBJS += ${CUDA_OBJECTS}
CFLAGS += -DHAVE_CUDAGPU=1
LDFLAGS +=  -Wl,-rpath,${CUDALIB} -L${CUDALIB} -lcudart -lcuda -lcublas
endif

CFLAGS  += $(OBJOPT) $(IPATH)
CPPFLAGS  += $(IPATH)
LDFLAGS += $(EXEOPT) $(LPATH)
LDLIBS += $(LPATH)

CUDAFLAGS += -arch=sm_20

.SUFFIXES: .h.Z .c.Z .h.gz .c.gz .c.z .h.z

ifeq (${ORWL_TIMING},)
else
CPPFLAGS += -DORWL_USE_TIMING=1
endif

##############################
# Basic Compile Instructions #
##############################

all:	$(NAME)

% : %.o
	$(NICE_CC) $(LDFLAGS) $^ $(LDLIBS) -o $@

% : %.c
	$(NICE_CC) $(CFLAGS) $(CPPFLAGS) $^ $(LDFLAGS) $(LDLIBS) -o $@

%.o : %.c
	$(NICE_CC) $(CFLAGS) $(CPPFLAGS) -c $^

%.o : %.cu
	$(CUDAC) $(CUDAFLAGS) $(CPPFLAGS) -DHAVE_CUDAGPU -c $^

ifeq (${HAVE_CUDAGPU},1)
# The cuda module should not end up in the "normal" prerequistes of a target
# but only be an "order-only" prerequiste.
${MOD_EXAMPLES} : Calcul_CudaGpu.o
endif


depend Makefile.inc:
	${NICE_CC} $(CFLAGS) $(CPPFLAGS) $(IPATH) -MM $(SRC) > Makefile.inc
clean:
	-$(RM) $(NAME) $(OBJS) *~
distclean: clean
	-$(RM) Makefile.inc
fclean:
	-$(RM) $(NAME)
comp: clean
	$(COMP) $(INCL) $(SRC)
ucomp:
	$(UNCOMP) $(SRC) $(INCL)

.c.Z.c .h.Z.h .c.gz.c .h.gz.h .c.z.c .h.z.h :
	 -$(UNCOMP) $<

%.o : %.c
	$(NICE_CC) $(CFLAGS) $(CPPFLAGS) -c $<

${MOD_EXAMPLES} : orwl_tutorial_task_obj.o orwl_tutorial_task_main.o

${OP_EXAMPLES} : orwl_tutorial_task_obj.o orwl_tutorial_op_main.o

orwl_tutorial_task_obj.o : orwl_tutorial_task_obj.c

tidy :
	for f in ${SRC} ; do  ${TIDYC} $$f ; done
	for f in ${CUDA_SOURCES} ${CUDA_HEADERS} ; do  ${TIDYCUDA} $$f ; done

html : ${HTML}

TAGS : ${SRC}
	etags *.c *.h

################
# Dependencies #
################

include Makefile.inc
