#!/bin/sh -f
#  This may look like complete nonsense, but it really is -*- mode: perl; coding: utf-8 -*-
eval 'exec perl -wS -s -x $0 ${1+"$@"}'
if 0;               #### for this magic, see findSvnAuthors ####
#!perl
#
#
# Except of parts copied from previous work and as explicitly stated below,
# the authors and copyright holders for this work are as follows:
# all rights reserved,  2012 Jens Gustedt, INRIA, France
#
# This file is part of the ORWL project. You received this file as as
# part of a confidential agreement and you may generally not
# redistribute it and/or modify it, unless under the terms as given in
# the file LICENSE.  It is distributed without any warranty; without
# even the implied warranty of merchantability or fitness for a
# particular purpose.

# Arguments: [Options] executable nb_tasks [stride]

# Options
#  -term=N  with N=1 for gdb, N=2 for valgrind
#
#  -break=function to establish a first break point for debugging at
#   "function", default is "main"

use English;
use POSIX;
use File::Temp qw/ tempfile /;
use Time::HiRes qw( clock_gettime CLOCK_REALTIME );

# Set up a simple cleanup handler to remove temporary files and a
# function to start tasks in the background.
sub cleanup(@);
sub background(@);
sub waitall(@);
sub chose($$);
sub pump($$);

my @sig = qw(ABRT BUS FPE HUP ILL INT KILL PIPE POLL PWR QUIT SEGV
    STKFLT TERM TSTP TTIN TTOU URG USR1 USR2 VTALRM XCPU XFSZ );

foreach my $sig (@sig) {
    $SIG{$sig}  = \&cleanup;
}

END {
    cleanup();
}

my $term = defined($term) ? $term : 0;

## set up this individual run
my ($bin, $nb, $step) = ();
die "not enough command line arguments" if (!@ARGV);
$bin = shift @ARGV;
die "not enough command line arguments" if (!@ARGV);
$nb = shift @ARGV;
if (@ARGV && ("$ARGV[0]" !~ "--")) {
    $step = shift @ARGV;
} else{
    $step = 1;
}

shift @ARGV if (@ARGV && ($ARGV[0] =~ "--"));

my $run = int(rand(0xFFFFFFFF));

## environment variables needed for ORWL
$ENV{ORWL_SECRET} = int(rand(0xFFFFFFFF));
$ENV{ORWL_GLOBAL_AB} = "${run}_global.txt";

## Arrays needed for job control

my @pid;
my @locfiles;
my @garb = ($ENV{ORWL_GLOBAL_AB}, "$ENV{ORWL_GLOBAL_AB}_0");

for (my $i = 0; $i < $nb; $i += ${step}) {
    push(@locfiles, "${run}_${i}.txt");
}

my $collectorID = fork();
if ($collectorID) {
    ## Launch the tasks.
    push(@pid, $collectorID);
    for (my $i = 0; $i < $nb; $i += ${step}) {
        # group some main tasks into the same process
        my @mtask = ();
        for (my $j = $i; $j < $nb && $j < $i + $step; ++$j) {
            push(@mtask, $j);
        }
        $ENV{ORWL_LOCAL_AB} = "${run}_${i}.txt";
        $ENV{ORWL_TIDS} = "@mtask";
        my @cmd = ($bin, @ARGV ? @ARGV : (rand(100)));
        push(@pid, background($ENV{ORWL_LOCAL_AB}, chose($term, $i), @cmd));
    }
} else {
    # The collector process poll for the existence of the partial
    # address books that are produced by the application processes.
    # It then dumps all these parts into one single file.
    if (defined($ENV{ORWL_STDOUT})) {
        system("touch $ENV{ORWL_GLOBAL_AB}");
    }
    foreach my $file (@locfiles) {
        while (! -e $file) {
            select(undef, undef, undef, 0.01);
        }
    }

    if (defined($ENV{ORWL_STDOUT})) {
        # In this case we will do the polling for each application
        # process in a process of its own.
        system("cat @locfiles >> $ENV{ORWL_GLOBAL_AB}");
        system("echo >> $ENV{ORWL_GLOBAL_AB}");
    } else {
        system("cat @locfiles > $ENV{ORWL_GLOBAL_AB}_0");
        system("echo >> $ENV{ORWL_GLOBAL_AB}_0");
        ## Only move the file to the expected name, once it is completely written.
        rename "$ENV{ORWL_GLOBAL_AB}_0", $ENV{ORWL_GLOBAL_AB};
    }
    unlink(@locfiles);
    @locfiles = ();
    @garb = ();
    exit 0;
}

## Wait for all processes to finish
waitall(@pid);

print "\n$bin have all finished\n";

############ End of processing, only function definitions beyond that
############ point.
exit 0;

sub chose($$) {
    my ($what, $i) = @_;
    my @what;
    if ($what) {
        if ($what == 1) {
            my $tmprun = "${run}.cmd";
            if (defined(${break})) {
                $break = "main" if (length(${break}) == 0);
                system("echo break ${break} > $tmprun");
            } else {
                system("echo > $tmprun");
            }
            system("echo run >> $tmprun");
            push(@garb, $tmprun);
            @what = ("gdb", "-x", "$tmprun", "--args");
        } elsif ($what == 2) {
            @what = ("valgrind", "--log-file=${bin}-valgrind-${i}.log", "--verbose", "--leak-check=full", "--show-reachable=yes", "--track-origins=yes");
        }
        unshift(@what, "xterm", "-l", "+wf", "-geometry", "120x25", "-hold", "-fn", '-*-courier-*-r-*17*', "-T", "Main task $i", "-e");
    }
    return @what;
}

sub cleanup(@) {
    my ($sig) = @_;
    unlink(@locfiles, @garb);
    if (defined($sig)) {
        print STDERR "caught SIG$sig\n";
        die;
    }
}

sub background (@) {
    my $abfile = shift;
    my $fifo = tempfile();
    my $tmp = "${fifo}.txt";
    my $job = int(rand(0xFFFFFFFF));
    if (defined($ENV{ORWL_ERRDIR})) {
        my $dir = sprintf("$ENV{ORWL_ERRDIR}/err-%08X", ${run});
        mkdir "$dir" || 0;
        my $err = sprintf("${dir}/err-%08X.txt", $job);
        $ENV{"ORWL_STDERR"} = $err;
    }
    push(@garb, $tmp, $fifo);
    my $ppid = getpid();
    my $pid = fork();
    my $start = clock_gettime(CLOCK_REALTIME);
    if (!$pid) {
        # exit neatly
        @locfiles = ();
        @garb = ();
        if (defined($ENV{ORWL_STDOUT})) {
            # Each application process will receive the complete
            # address book on stdin. We provide this through a
            # separate process that dumps the AB file as long as the
            # collector process exists.
            my $tail = open(STDIN, "-|", "tail -n +0 --pid=$collectorID -q -f $ENV{ORWL_GLOBAL_AB} 2>/dev/null");
            # Each application process will output its part of the
            # address book on stdout. We create a specific process
            # that collects this data and pumps it into $abfile.
            mkfifo($fifo, 0700) or die "mkfifo $fifo failed: $!";
            if (!fork()) {
                pump($fifo, $tmp);
                rename $tmp, $abfile;
                waitpid($tail, 0);
                my $stop = clock_gettime(CLOCK_REALTIME);
                $stop -= $start;
                #print STDERR "setup completed: $stop seconds\n";
                exit 0;
            } else {
                open(STDOUT, ">>", $fifo);
            }
        }
        print STDERR "executing: @_\n";
        { exec @_; }
        { kill TERM, $ppid; }
        { die; }
    }
    return $pid;
}

sub waitall(@) {
    foreach my $pid (@_) {
        waitpid($pid, 0);
    }
}

sub pump($$) {
    my ($inf, $ouf) = @_;
    open(my $in, "<", $inf);
    my $content = "";
    foreach my $line (<$in>) {
        $content .= $line;
    }
    open(my $out, ">", $ouf);
    print $out $content;
    close($out);
}
