/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the author and copyright holder for this work is                           */
/* all rights reserved,  2012 Jens Gustedt, INRIA, France                     */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#include "orwl_tutorial_task_obj.h"

static
size_t parent(size_t source, size_t tid, size_t nt) {
  size_t pos = (tid + nt - source) % nt - 1;
  size_t ppos = pos >> 2;
  return (ppos + source) % nt;
}

ORWL_DEFINE_TASK(task_obj) {
  ORWL_THREAD_USE(task_obj, init_val);
  size_t const nt = orwl_nt;
  size_t const mytid = orwl_mytid;
  /* Scale our own location to the appropriate buffer size. */
  orwl_scale(sizeof(double));


  orwl_global_barrier_wait();
  /* Create handles for the locations that we are interested in. We
     will create a chain of dependencies from task 0 to task 1 etc. */
  orwl_handle2 here = ORWL_HANDLE2_INITIALIZER;
  orwl_handle2 herer = ORWL_HANDLE2_INITIALIZER;
  orwl_handle2 there = ORWL_HANDLE2_INITIALIZER;

  /* Have our own task writable. */
  orwl_write_insert(&here, ORWL_LOCATION(mytid), 0);

  /* all but task 0 link the "there" handle to their predecessor */
  if (mytid) {
    orwl_read_insert(&there, ORWL_LOCATION(parent(0, mytid, nt)), 1);
    orwl_read_insert(&herer, ORWL_LOCATION(mytid), 2);
  }

  /* Now synchronize again to have all requests inserted orderly at
     the other end. */
  orwl_schedule();

  for (size_t orwl_phase = 0; orwl_phase < 10; ++orwl_phase) {
    /* All tasks create a critical section that guarantees exclusive
       access to their location. */
    ORWL_SECTION(&here) {
      /* Obtain a pointer to the buffer in our virtual address space. */
      double * hval = orwl_write_map(&here);

      /* Task 0 is the starting point of the chain and initializes its location. */
      if (!mytid) {
        *hval = init_val + orwl_phase;
        REPORT(1, ORWL_TERM_CLEAR "we initialized our value to " ORWL_TERM_REMARK "%s" ORWL_TERM_OFF, *hval);
      }
      /* All other tasks read the value of their predecessor. */
      else {
        /* this will block until the data is available */
        ORWL_SECTION(&there) {
          /* This one is read only. */
          double const* tval = orwl_read_map(&there);
          /* Do some dummy computation. */
          *hval = *tval;
        }
      }
    }
    if (mytid) {
      ORWL_SECTION(&herer) {
        /* This one is read only. */
        double const* hval = orwl_read_map(&herer);
        if (mytid != (nt - 1)) PROGRESS(1, mytid, "found %s", *hval);
        else REPORT(1, ORWL_TERM_CLEAR "found the result   " ORWL_TERM_ALERT "%s" ORWL_TERM_OFF, *hval);
      }
    }
  }
  orwl_disconnect(&here);
  orwl_disconnect(&there);
  orwl_disconnect(&herer);
  orwl_stop_task();
}
