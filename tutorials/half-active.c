/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the author and copyright holder for this work is                           */
/* all rights reserved,  2013 Rodrigo Campos, INRIA, France                   */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#include "orwl.h"
ORWL_LOCATIONS_PER_TASK(main_loc);
ORWL_LOCATIONS_PER_TASK_INSTANTIATION();

P99_DECLARE_STRUCT(task_obj);
P99_DEFINE_STRUCT(task_obj, double init_val);

task_obj* task_obj_init(task_obj* task, double val) {

  if (!task)
    return NULL;

  *task = (task_obj) { .init_val = val, };
  return task;
}

void task_obj_destroy(task_obj *task) {
  /* empty */
}

P99_DECLARE_DELETE(task_obj);
P99_DEFINE_DELETE(task_obj);
ORWL_DECLARE_TASK(task_obj);



/* "x" must be a number between -total_tasks and total_tasks */
size_t get_neigh(int x) {
  /*
   * We should not use a negative number with the % operator since it is not
   * defined on C for negative numbers. So, as "x" is a number from -total_tasks
   * to total_tasks and all other numbers are positive (size_t is unsigned) it's
   * equivalent to do:
   *
   * (mytid + x) % total_tasks
   *
   * or
   *
   * (mytid + x + total_tasks) % total_tasks
   *
   * with the difference that the latter assures the first operand to the %
   * operator is always positive, and though always gets the right result in C.
   *
   * XXX: we are not using this "feature" now, because we call this function
   * with "x" as a positive number (becase right now only task with odd id read
   * the previous, and the first task is 0). We just leave this "more
   * complicated" implementation just in case we need it in the future, and
   * because it's not really complicated :-)
   */
  assert(orwl_mytid + x + orwl_nt >= 0);
  size_t neigh = orwl_mytid + x + orwl_nt;
  neigh = neigh % orwl_nt;

  return neigh;
}

int main(int argc, char **argv) {
  if (argc < 2) {
    REPORT(1, "only " ORWL_TERM_ALERT "%s commandline argument(s)" ORWL_TERM_OFF ", this ain't enough", argc);
    return EXIT_FAILURE;
  }
  double init_val = strtod(argv[1]);

  orwl_init();

  // XXX: Needed so that the simple code to initialize works okay (without
  // deadlocks). See next comment to understand why we need orwl_nt to be an
  // even number
  assert(orwl_nt % 2 == 0);

  /*
   * Simple chain where even tasks id wait and odd tasks id read from the
   * previous task. So, it is something like this:
   *
   *
   * | task 0 |task 1 | task 2| task3 |
   *
   * First each task writes to it's own location a number. Then, even tasks
   * numbers read from the previous one and do some dummy computation. For
   * example, in this case task 1 reads from task 0 and task 3 from task 2.
   *
   * This way, we have half of the locations active reading/writing.
   *
   * But, of course, this only makes sense if orwl_nt is an even number. If not,
   * there is one location that is unused
   */

  for (size_t i = 0; i < orwl_lt; i++) {
    task_obj* task = P99_NEW(task_obj, init_val);
    task_obj_create_task(task, orwl_tids[i]);
  }

  return EXIT_SUCCESS;
}

ORWL_DEFINE_TASK(task_obj) {
  ORWL_THREAD_USE(task_obj, init_val);

  /* Scale our own location(s) to the appropriate buffer size. */
  orwl_scale(sizeof(double));

  /* Sleep a bit to give the user the occasion to admire the following
     message. */
  sleepfor(0.5 * p99_drand());

  /* Now ORWL knows the total amount of tasks that will be
     performed. Avoid being too verbose so let only task 0 tell it. */
  PROGRESS(1, orwl_myloc,
           "<-------- the start of the report line now should contain location ID (%s) and total number (%s)",
           orwl_myloc, orwl_nl);

  /* Create handles for the locations that we are interested in */
  orwl_handle here_1 = ORWL_HANDLE_INITIALIZER;
  orwl_handle2 here_2 = ORWL_HANDLE2_INITIALIZER;
  orwl_handle2 left = ORWL_HANDLE2_INITIALIZER;

  /* We want a previous task, but % is not defined for negative numbers on C.
   * So, as we know that orwl_mytid is between 0 and orwl_nt, to make sure its
   * never negative when substractic a number between 0 and orwl_nt, we just add
   * orwl_nt. This, of course, doesn't change the result of the "%" calculated
   */
  size_t left_loc_id = get_neigh(-1);

  // Everyone writes something to his task
  orwl_write_insert(&here_1, ORWL_LOCATION(orwl_mytid, main_loc), 0);

  /* read the left neigh and write the average here only on even tasks */
  if (orwl_mytid % 2 != 0) {
    orwl_handle2_read_insert(&left, ORWL_LOCATION(left_loc_id, main_loc), 1);
    orwl_handle2_write_insert(&here_2, ORWL_LOCATION(orwl_mytid, main_loc), 1);
  }

  orwl_schedule();

  double *here_val;
  ORWL_SECTION(&here_1) {
    here_val = orwl_write_map(&here_1);
    *here_val = init_val;
  }

  /* even tasks id just write once and wait for the others to read */
  if (orwl_mytid % 2 == 0) {
    goto out;
  }


  for (int i = 0; i < 1000; i++) {

    ORWL_SECTION(&here_2) {
      here_val = orwl_write_map(&here_2);

      REPORT(1, "first value is " ORWL_TERM_REMARK "%s" ORWL_TERM_OFF, *here_val);

      ORWL_SECTION(&left) {
        double const* rval = orwl_read_map(&left);

        /* Do some dummy computation. */
        REPORT(1, "the average between %s (here) and %s (there) is stored", *here_val, *rval);
        *here_val = (*rval + *here_val) * 0.5;
        REPORT(1, "the average is %s", *here_val);
      }
    }
  }

  orwl_handle2_disconnect(&here_2);
  orwl_handle2_disconnect(&left);
out:
  orwl_stop_task();
}
