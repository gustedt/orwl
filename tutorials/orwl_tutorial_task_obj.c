/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the authors and copyright holders for this work are as follows:            */
/* all rights reserved,  2011 Emmanuel Jeanvoine, INRIA, France               */
/* all rights reserved,  2012-2014 Jens Gustedt, INRIA, France                */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#include "orwl_tutorial_task_obj.h"

ORWL_LOCATIONS_PER_TASK_INSTANTIATION();

P99_GETOPT_DEFINE(n, size_t, opt_n, 512, "n", "control the n dimension of some matrix");
P99_GETOPT_DEFINE(m, size_t, opt_m, 512, "m", "control the m dimension of some matrix");
P99_GETOPT_DEFINE(k, size_t, opt_k, 512, "k", "control the k dimension of some matrix");
P99_GETOPT_DEFINE(i, size_t, opt_i,   5, "i", "the number of test iterations");
P99_GETOPT_DEFINE(d, double, opt_d, 0.0, "d", "an initialization parameter");
P99_GETOPT_DEFINE(a, char const*, opt_afile, 0, "afile", "an input file name");
P99_GETOPT_DEFINE(b, char const*, opt_bfile, 0, "bfile", "a second input file name");
P99_GETOPT_DEFINE(o, char const*, opt_outfile, 0, "ofile", "an output file name");



task_obj* task_obj_init(task_obj* task, double val) {
  if (task) {
    *task = (task_obj) {
      .init_val = val,
    };
  }
  return task;
}

void task_obj_destroy(task_obj *task) {
  /* empty */
}

P99_DEFINE_DELETE(task_obj);

P99_WEAK(GPU_device_init)
void GPU_device_init(void) {
  /* empty */
}

