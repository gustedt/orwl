/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the author and copyright holder for this work is                           */
/* all rights reserved,  2012 Jens Gustedt, INRIA, France                     */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#include "orwl_tutorial_task_obj.h"
#include "orwl_reduce.h"
ORWL_ALLREDUCE_LOCATION(sec_loc);

typedef struct data data;
struct data {
  double x;
  double y;
};

ORWL_DEFINE_TASK(task_obj) {
  ORWL_THREAD_USE(task_obj, init_val);
  size_t const mytid = orwl_mytid;

  ORWL_ALLREDUCE_DECLARE(data, sec_loc) = { .x = init_val, .y = 2*init_val, };

  /* Now synchronize again to have all requests inserted orderly at
     the other end. */
  orwl_schedule();

  for (size_t orwl_phase = 0; orwl_phase < 10; ++orwl_phase) {
    /* Store some new value of your liking */
    sec_loc = (data){
      .x = init_val + orwl_phase + mytid,
      .y = 2*init_val + orwl_phase + mytid,
    };
    ORWL_ALLREDUCE(sec_loc, other) {
      /* Accumulate the value */
      sec_loc.x += other.x;
      sec_loc.y += other.y;
    }
    report(1, "found (%g, %g)", sec_loc.x, sec_loc.y);
  }
  orwl_stop_task();
}
