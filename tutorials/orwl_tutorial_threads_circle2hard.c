/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the authors and copyright holders for this work are as follows:            */
/* all rights reserved,  2012-2013 Jens Gustedt, INRIA, France                */
/* all rights reserved,  2013 Rodrigo Campos, INRIA, France                   */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#include "orwl_tutorial_task_obj.h"

//! [compute a color]
enum { chi = 2 };

static
size_t color(size_t mytid, size_t nt) {
  /* Every circle is three-colorable and "almost" two colorable */
  return (mytid
          ? mytid % chi
          /* watch that the cycle doesn't link the wrong colors, that
             is if the other neighbor of thread 0 choses a 0, we
             choose another one. */
          : ((nt % chi == 1)
             /* Take care of the case that chi is 2 and nt is odd. */
             ? ((chi == 2) ? 2 : (chi - 1))
             : 0));
}
//! [compute a color]

//! [start the task specific function definition]
ORWL_DEFINE_TASK(task_obj) {
  ORWL_THREAD_USE(task_obj, init_val);
  //! [start the task specific function definition]

  //! [condition this task]
  /* Scale our own location(s) to the appropriate buffer size. */
  double dummi[2] = { 0 };
  static double dummies[10] = { 0 };
  double* dummy =
    (ORWL_LOCATION(orwl_mytid, main_loc) < 10)
    ? &dummies[ORWL_LOCATION(orwl_mytid, main_loc)]
    : &dummi[0];
  orwl_allocate_hard(0, &dummy[0], sizeof dummy[0], ORWL_LOCATION(orwl_mytid, main_loc));
  orwl_allocate_hard(0, &dummy[1], sizeof dummy[1], ORWL_LOCATION((orwl_mytid + orwl_nt - 1) % orwl_nt, main_loc)),
                     //! [condition this task]

                     /* Sleep a bit to give the user the occasion to admire the following
                        message. */
                     sleepfor(0.5 * p99_drand());
  /* Now ORWL knows the total amount of tasks that will be
     performed. Avoid being too verbose so let only task 0 tell it. */
  PROGRESS(1, orwl_myloc,
           "<-------- the start of the report line now should contain location ID (%s) and total number (%s)",
           orwl_myloc, orwl_nl);

  //! [connect the resources]
  /* Create handles for the locations that we are interested in. We
     will create a chain of dependencies from task 0 to task 1 etc. */
  orwl_handle2 here = ORWL_HANDLE2_INITIALIZER;
  orwl_handle2 there = ORWL_HANDLE2_INITIALIZER;

  /* Have our own location writable. */
  orwl_write_insert(&here, ORWL_LOCATION(orwl_mytid, main_loc), color(orwl_mytid, orwl_nt));

  /* link the "there" handle where appropriate */
  orwl_read_insert(&there, ORWL_LOCATION((orwl_mytid + orwl_nt - 1) % orwl_nt, main_loc), color(orwl_mytid, orwl_nt));

  /* Now synchronize to have all requests inserted orderly at the
     other end. */
  orwl_schedule();
  //! [connect the resources]

  //! [initialization phase]
  ORWL_SECTION(&here) {
    /* Just to be sure to keep everybody in sync. */
    ORWL_SECTION(&there);
    double * wval = orwl_write_map(&here);
    *wval = init_val;
    REPORT(1, "first value is " ORWL_TERM_REMARK "%s" ORWL_TERM_OFF, *wval);
  }
  //! [initialization phase]

  //! [start an iteration loop]
  for (size_t orwl_phase = 0; orwl_phase < opt_i; ++orwl_phase)
    //! [start an iteration loop]
    //! [perform the task]
    /* All tasks create a critical section that guarantees exclusive
       access to their location. */
    ORWL_SECTION(&here) {
    /* Obtain a pointer to the buffer in our virtual address space. */
    double * wval = orwl_write_map(&here);
    /* this will block until the data is available */
    ORWL_SECTION(&there) {
      double const* rval = orwl_read_map(&there);
      /* Do some dummy computation. */
      *wval = (*rval + *wval) * 0.5;
      REPORT(1, "we stored the average: %s", *wval);
    }
  }
  //! [perform the task]

  //! [say good bye]
  orwl_disconnect(&here);
  orwl_disconnect(&there);
  orwl_stop_task();
  //! [say good bye]
}
