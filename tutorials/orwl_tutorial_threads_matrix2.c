/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the author and copyright holder for this work is                           */
/* all rights reserved,  2012-2013 Jens Gustedt, INRIA, France                */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#include "orwl_tutorial_task_obj.h"

//! [compute a color]
enum { chi = 2 };

static
size_t color(size_t mytid, size_t nt) {
  /* Every circle is three-colorable and "almost" two colorable */
  return (mytid
          ? mytid % chi
          /* watch that the cycle doesn't link the wrong colors, that
             is if the other neighbor of thread 0 choses a 0, we
             choose another one. */
          : ((nt % chi == 1)
             /* Take care of the case that chi is 2 and nt is odd. */
             ? ((chi == 2) ? 2 : (chi - 1))
             : 0));
}
//! [compute a color]

//! [helpers]
#define AVERAGE(...) (P99_SUMS(__VA_ARGS__)*(1.0/P99_NARG(__VA_ARGS__)))
//! [helpers]

//! [start the task specific function definition]
ORWL_DEFINE_TASK(task_obj) {
  ORWL_THREAD_USE(task_obj, init_val);
  //! [start the task specific function definition]

  //! [condition this task]
  enum { n = 512, n2 = n*n, n3 = n2*n };
  /* Scale our own location(s) to the appropriate buffer size. */
  orwl_scale(sizeof(double[n][n]));
  //! [condition this task]

  //! [connect the resources]
  /* Create handles for the locations that we are interested in. We
     will create a chain of dependencies from task 0 to task 1 etc. */
  orwl_handle2 here = ORWL_HANDLE2_INITIALIZER;
  orwl_handle2 there[2] = { ORWL_HANDLE2_INITIALIZER, ORWL_HANDLE2_INITIALIZER};

  /* Have our own location writable. */
  orwl_write_insert(&here, ORWL_LOCATION(orwl_mytid, main_loc), color(orwl_mytid, orwl_nt));

  /* link the "there" handle where appropriate */
  orwl_read_insert(&there[0], ORWL_LOCATION((orwl_mytid + orwl_nt - 1) % orwl_nt, main_loc), color(orwl_mytid, orwl_nt));
  orwl_read_insert(&there[1], ORWL_LOCATION((orwl_mytid + orwl_nt + 1) % orwl_nt, main_loc), color(orwl_mytid, orwl_nt));

  /* Now synchronize to have all requests inserted orderly at the
     other end. */
  orwl_schedule();
  //! [connect the resources]

  //! [initialization phase]
  ORWL_SECTION(&here) {
    /* Just to be sure to keep everybody in sync. */
    ORWL_SECTION(there);

    /* Obtain a pointer to the buffer in our virtual address space. */
    double (*hval)[n] = orwl_write_map(&here);

    /* Everybody initializes its location. */
    for (size_t i = 0; i < n; ++i)
      for (size_t j = 0; j < n; ++j)
        hval[i][j] = init_val + i + j;
    PROGRESS(1, orwl_mytid, "we initialized our value [0][0] to " ORWL_TERM_REMARK "%s" ORWL_TERM_OFF, hval[0][0]);

    /* Sleep a bit to let the user read the message*/
    sleepfor(0.05);
  }
  //! [initialization phase]

  //! [start an iteration loop]
  for (size_t orwl_phase = 0; orwl_phase < opt_i; ++orwl_phase)
    //! [start an iteration loop]
    //! [perform the task]
    /* All tasks create a critical section that guarantees exclusive
       access to their location. */
    ORWL_SECTION(&here) {
    /* Obtain a pointer to the buffer in our virtual address space. */
    double (*hval)[n] = orwl_write_map(&here);
    /* this will block until the data is available */
    ORWL_SECTION(there) {
      /* This one is read only. */
      double const (*tval[2])[n] = { orwl_read_map(&there[0]), orwl_read_map(&there[1]) };
      /* Do some dummy computation. */
      for (size_t i = 0; i < n; ++i)
        for (size_t j = 0; j < n; ++j) {
          double tmp = AVERAGE(tval[0][i][j], tval[1][i][j]);
          hval[i][j] = AVERAGE(tmp, hval[i][j]);
        }
      if (orwl_mytid) PROGRESS(1, orwl_mytid, "found the result for [0][0]  " ORWL_TERM_ALERT "%s" ORWL_TERM_OFF, hval[0][0]);
      else REPORT(1, ORWL_TERM_CLEAR "found the result for [0][0]   " ORWL_TERM_ALERT "%s" ORWL_TERM_OFF, hval[0][0]);
    }
  }

  //! [perform the task]

  //! [say good bye]
  orwl_disconnect(&here);
  orwl_disconnect(there);
  orwl_stop_task();
  //! [say good bye]
}
