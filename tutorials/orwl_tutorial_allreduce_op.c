/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the author and copyright holder for this work is                           */
/* all rights reserved,  2012 Jens Gustedt, INRIA, France                     */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#include "orwl_tutorial_task_obj.h"
#include "orwl_reduce.h"

static
void main_op(task_obj* Arg, orwl_server* srv, size_t myloc, orwl_locations task1) {
  size_t const mytid = orwl_mytid;
  fprintf(stderr, "main op: %zu on location %zu\n", mytid, myloc);

  /* Use our own location to communicate with the allreduce
     operation. */
  orwl_handle here = ORWL_HANDLE_INITIALIZER;
  orwl_write_insert(&here, ORWL_LOCATION(mytid, main_loc), 1);
  orwl_scale(sizeof(double));

  orwl_schedule(myloc, 1, srv);

  /* This will only be acquired when the reduce operation is finished. */
  ORWL_SECTION(&here) {
    double * wval = orwl_write_map(&here);
    fprintf(stderr, "main op: %zu on location %zu, finishing with %g\n", mytid, myloc, *wval);
  }
}

ORWL_ALLREDUCE_LOCATION(sec_loc);

typedef struct data data;
struct data {
  double x;
  double y;
};

static
void reduce_op(task_obj* Arg, orwl_server* srv, size_t myloc, orwl_locations task1) {
  ORWL_THREAD_USE(task_obj, init_val);
  size_t const mytid = orwl_mytid;
  fprintf(stderr, "reduce op: %zu on location %zu\n", mytid, myloc);

  /* Use main's location to communicate with it. */
  orwl_handle there = ORWL_HANDLE_INITIALIZER;
  orwl_write_insert(&there, ORWL_LOCATION(mytid, main_loc), 0);

  ORWL_ALLREDUCE_DECLARE(data, sec_loc) = { .x = init_val, .y = 2*init_val, };

  /* Now synchronize again to have all requests inserted orderly at
     the other end. */
  orwl_schedule(myloc, 1, srv);

  for (size_t orwl_phase = 0; orwl_phase < 10; ++orwl_phase) {
    /* Store some new value of your liking */
    sec_loc = (data){
      .x = init_val + orwl_phase + mytid,
      .y = 2*init_val + orwl_phase + mytid,
    };
    ORWL_ALLREDUCE(sec_loc, other) {
      /* Accumulate the value */
      sec_loc.x += other.x;
      sec_loc.y += other.y;
    }
    report(1, "found (%g, %g)", sec_loc.x, sec_loc.y);
  }
  /* This will block the main operation until we are done, here. */
  ORWL_SECTION(&there) {
    double * wval = orwl_write_map(&there);
    *wval = myloc;
  }
  ORWL_ALLREDUCE_DISCONNECT(sec_loc);
}

static
void null_op(task_obj* Arg, orwl_server* srv, size_t myloc, orwl_locations task1) {
  size_t const mytid = orwl_mytid;
  fprintf(stderr, "null op: %zu on location %zu\n", mytid, myloc);
  orwl_schedule(myloc, 1, srv);
}

ORWL_DEFINE_TASK(task_obj) {
  size_t myloc = orwl_myloc;
  orwl_server *const srv = orwl_server_get();
  orwl_locations task1 = ORWL_LOCAL(myloc);
  switch (task1) {
  case main_loc: main_op(Arg, srv, myloc, task1); break;
  case sec_loc : reduce_op(Arg, srv, myloc, task1); break;
  default: null_op(Arg, srv, myloc, task1); break;
  }
}
