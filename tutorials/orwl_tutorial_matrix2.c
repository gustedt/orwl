/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the author and copyright holder for this work is                           */
/* all rights reserved,  2012 Jens Gustedt, INRIA, France                     */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#include "orwl_tutorial_task_obj.h"
#include "orwl_reduce.h"
ORWL_ALLREDUCE_LOCATION(sec_loc);

size_t const chi = 3;

static
size_t color(size_t mytid, size_t nt) {
  /* Every circle is three-colorable */
  return (mytid
          ? mytid % chi
          /* watch that the cycle doesn't link the wrong colors, that
             is if the other neighbor of thread 0 choses a 0, we
             choose another one. */
          : ((nt % chi == 1)
             ? (chi - 1)
             : 0));
}

#define AVERAGE(...) (P99_SUMS(__VA_ARGS__)*(1.0/P99_NARG(__VA_ARGS__)))

ORWL_DEFINE_TASK(task_obj) {
  ORWL_THREAD_USE(task_obj, init_val);
  size_t const nt = orwl_nt;
  /* Tell orwl which location this process will occupy */
  size_t mytid = orwl_mytid;
  /* Scale our own location to the appropriate buffer size. */
  orwl_scale(sizeof(double[nt][nt]));
  /* Sleep a bit to give the user the occasion to admire the following
     message. */
  sleepfor(2.0 * p99_drand());
  /* Now ORWL knows the total amount of tasks that will be
     performed. Avoid being too verbose so let only task 0 tell it. */
  PROGRESS(1, mytid,
           "<-------- the start of the report line now should contain location ID (%s) and total number (%s)",
           orwl_myloc, orwl_nl);

  /* Create handles for the locations that we are interested in. We
     will create a chain of dependencies from task 0 to task 1 etc. */
  orwl_handle2 here = ORWL_HANDLE2_INITIALIZER;
  orwl_handle2 there[2] = { ORWL_HANDLE2_INITIALIZER, ORWL_HANDLE2_INITIALIZER};

  /* Have our own task writable. */
  orwl_write_insert(&here, ORWL_LOCATION(mytid, main_loc), color(mytid, nt));

  /* all the "there" handles to their predecessor and successors */
  orwl_read_insert(&there[0], ORWL_LOCATION((mytid + nt - 1) % nt, main_loc), color(mytid, nt));
  orwl_read_insert(&there[1], ORWL_LOCATION((mytid + nt + 1) % nt, main_loc), color(mytid, nt));

  /* A reduce variable can be initialized if the base type (here
     size_t) can be initialized. */
  ORWL_ALLREDUCE_DECLARE(size_t, sec_loc)
    = (p99_rand() % 2 ? 1 : mytid);

  /* Now synchronize again to have all requests inserted orderly at
     the other end. */
  orwl_schedule();

  /* All tasks create a critical section that guarantees exclusive
     access to their location. */
  ORWL_SECTION(&here) {
    /* Just to be sure to keep everybody in sync. */
    ORWL_SECTION(there);

    /* Obtain a pointer to the buffer in our virtual address space. */
    double (*hval)[nt] = orwl_write_map(&here);

    /* Everybody initializes its location. */
    for (size_t i = 0; i < nt; ++i)
      for (size_t j = 0; j < nt; ++j)
        hval[i][j] = init_val + i + j;
    PROGRESS(1, mytid, "we initialized our value [0][0] to " ORWL_TERM_REMARK "%s" ORWL_TERM_OFF, hval[0][0]);

    /* Sleep a bit to let the user read the message*/
    sleepfor(0.05);
  }

  if (!mytid) report(1, ORWL_TERM_CLEAR "all locations are initialized");


  // perform a maximum operation on all the mytids
  ORWL_ALLREDUCE(sec_loc, other) {
    if (other > sec_loc) sec_loc = other;
  }
  size_t maxid = sec_loc;
  if (mytid == nt-1) REPORT(1, ORWL_TERM_CLEAR "maximum id is %s", maxid);

  {
    /* redeclare the reduce location to contain a whole line of the
       matrix. */
    typedef double line[nt];
    /* A reduce variable can't be initialized if the base type (here a
       VLA) can't. */
    ORWL_ALLREDUCE_DECLARE(line, sec_loc);

    for (size_t orwl_phase = 0; orwl_phase <= maxid; ++orwl_phase) {
      ORWL_SECTION(&here) {
        /* All tasks read the value of their predecessor. */
        /* this will block until the data is available */
        ORWL_SECTION(there) {
          line * hval = orwl_write_map(&here);
          /* This one is read only. */
          double const (*tval[2])[nt] = { orwl_read_map(&there[0]), orwl_read_map(&there[1]) };
          /* Do some dummy computation. */
          for (size_t i = 0; i < nt; ++i)
            for (size_t j = 0; j < nt; ++j) {
              double tmp = AVERAGE(tval[0][i][j], tval[1][i][j]);
              hval[i][j] = AVERAGE(tmp, hval[i][j]);
            }
          memcpy(sec_loc, hval[0], sizeof *sec_loc);
        }
      }

      // perform a minimum operation on all the upper left corners
      ORWL_ALLREDUCE(sec_loc, other) {
        for (size_t i = 0; i < nt; ++i) {
          if (other[i] < sec_loc[i]) sec_loc[i] = other[i];
        }
      }

      if (mytid && mytid < (nt - 1)) PROGRESS(1, mytid, "found the intermediate result " ORWL_TERM_ALERT "%s" ORWL_TERM_OFF, sec_loc[0]);
      else REPORT(1, ORWL_TERM_CLEAR "found the minimum of the upper left corners " ORWL_TERM_ALERT "%s" ORWL_TERM_OFF, sec_loc[0]);
    }
  }
  orwl_disconnect(&here);
  orwl_disconnect(there);
  orwl_stop_task();
}
