/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the author and copyright holder for this work is                           */
/* all rights reserved,  2012-2013 Jens Gustedt, INRIA, France                */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#include "orwl_tutorial_task_obj.h"

//! [start the task specific function definition]
ORWL_DEFINE_TASK(task_obj) {
  ORWL_THREAD_USE(task_obj);
  //! [start the task specific function definition]

  //! [condition this task]
  /* Scale our own location(s) to the appropriate buffer size. */
  orwl_scale(sizeof(double));
  //! [condition this task]

  /* Sleep a bit to give the user the occasion to admire the following
     message. */
  sleepfor(0.5 * p99_drand());
  /* Now ORWL knows the total amount of tasks that will be
     performed. Avoid being too verbose so let only task 0 tell it. */
  PROGRESS(1, orwl_myloc,
           "<-------- the start of the report line now should contain location ID (%s) and total number (%s)",
           orwl_myloc, orwl_nl);

  //! [connect the resources]
  /* Create handles for the locations that we are interested in. We
     will create a chain of dependencies from task 0 to task 1 etc. */
  orwl_handle2 here = ORWL_HANDLE2_INITIALIZER;
  orwl_handle2 there = ORWL_HANDLE2_INITIALIZER;

  /* Have our own location writable. */
  orwl_write_insert(&here, ORWL_LOCATION(orwl_mytid, main_loc), 0);

  /* link the "there" handle where appropriate */
  orwl_read_insert(&there, ORWL_LOCATION((orwl_mytid + orwl_nt - 1) % orwl_nt, main_loc), 1);

  /* Now synchronize to have all requests inserted orderly at the
     other end. */
  orwl_schedule();
  //! [connect the resources]

  //! [initialization phase]
  ORWL_SECTION(&here) {
    /* Just to be sure to keep everybody in sync. */
    double * wval = orwl_write_map(&here);
    *wval = orwl_mytid;
    REPORT(1, "first value is " ORWL_TERM_REMARK "%s" ORWL_TERM_OFF, *wval);
  }
  //! [initialization phase]

  //! [start an iteration loop]
  orwl_alloc_ref alloc = ORWL_ALLOC_REF_INITIALIZER(0);
  for (size_t orwl_phase = 0; orwl_phase < opt_i; ++orwl_phase) {
    //! [start an iteration loop]
    //! [perform the task]
    ORWL_SECTION(&there) {
      orwl_hold(&there, &alloc);
    }
    /* All tasks create a critical section that guarantees exclusive
       access to their location. */
    ORWL_SECTION(&here) {
      orwl_replace(&here, &alloc);
      /* Obtain a pointer to the buffer in our virtual address space. */
      double * wval = orwl_write_map(&here);
      /* this will block until the data is available */
      REPORT(!orwl_mytid, "we replaced the data by the remote and find %s", *wval);
    }
    //! [perform the task]
  }
  orwl_alloc_ref_destroy(&alloc);
  //! [say good bye]
  orwl_disconnect(&there);
  orwl_disconnect(&here);
  orwl_stop_task();
  //! [say good bye]
}
