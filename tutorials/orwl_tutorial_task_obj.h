/* This may look like nonsense, but it really is -*- mode: C -*-             */
/*                                                                           */
/* Except of parts copied from previous work and as explicitly stated below, */
/* the author and copyright holder for this work is                          */
/* all rights reserved,  2012 Jens Gustedt, INRIA, France                    */
/*                                                                           */
/* This file is part of the ORWL project. You received this file as as       */
/* part of a confidential agreement and you may generally not                */
/* redistribute it and/or modify it, unless under the terms as given in      */
/* the file LICENSE.  It is distributed without any warranty; without        */
/* even the implied warranty of merchantability or fitness for a             */
/* particular purpose.                                                       */
/*                                                                           */

#ifndef ORWL_TUTORIAL_TASK_OBJ_H
#define ORWL_TUTORIAL_TASK_OBJ_H

//! [compile time parametrization]
#include "orwl.h"
ORWL_LOCATIONS_PER_TASK(main_loc, sec_loc, thrd_loc);
//! [compile time parametrization]

//! [run time parametrization]
#include "p99_getopt.h"
P99_GETOPT_DECLARE(n, size_t, opt_n, 512, "n", "control the n dimension of some matrix");
P99_GETOPT_DECLARE(m, size_t, opt_m, 512, "m", "control the m dimension of some matrix");
P99_GETOPT_DECLARE(k, size_t, opt_k, 512, "k", "control the k dimension of some matrix");
P99_GETOPT_DECLARE(i, size_t, opt_i,   5, "iterations", "the number of test iterations");
P99_GETOPT_DECLARE(d, double, opt_d, 0.0, "d", "an initialization parameter");
P99_GETOPT_DECLARE(a, char const*, opt_afile, 0, "afile", "an input file name");
P99_GETOPT_DECLARE(b, char const*, opt_bfile, 0, "bfile", "a second input file name");
P99_GETOPT_DECLARE(o, char const*, opt_outfile, 0, "ofile", "an output file name");
//! [run time parametrization]

//! [declare task specific state]
P99_DECLARE_STRUCT(task_obj);
P99_DEFINE_STRUCT(task_obj,
                  double init_val
                 );

task_obj* task_obj_init(task_obj* task, double val);
void task_obj_destroy(task_obj *task);

P99_DECLARE_DELETE(task_obj);
ORWL_DECLARE_TASK(task_obj);
//! [declare task specific state]


//! [initialize the GPU, if any]
extern void GPU_device_init(void);
//! [initialize the GPU, if any]

#endif
