/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the author and copyright holder for this work is                           */
/* all rights reserved,  2012 Jens Gustedt, INRIA, France                     */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#include "orwl_tutorial_task_obj.h"

static
size_t chunkSizeRec(size_t tid, size_t nt) {
  if (!tid) return nt;
  else {
    size_t nt1 = nt / 2;
    size_t nt2 = nt - (nt / 2) - 1;
    return (tid < nt1 + 1)
           ? chunkSizeRec(tid - 1, nt1)
           : chunkSizeRec(tid - (nt1 + 1), nt2);
  }
}

static
size_t chunkSize(size_t source, size_t tid, size_t nt) {
  size_t pos = (tid + nt - source) % nt;
  return chunkSizeRec(pos, nt);
}

ORWL_DEFINE_TASK(task_obj) {
  ORWL_THREAD_USE(task_obj, init_val);
  size_t const nt = orwl_nt;
  size_t const mytid = orwl_mytid;
  /* Scale our own location to the appropriate buffer size. */
  size_t size = chunkSize(0, mytid, nt);
  size_t const sizes[2] = { size / 2, size -  (size / 2) - 1, };
  size_t const children[2] = { mytid + 1, mytid + 1 + sizes[0], };
  size_t const numb = (sizes[0] ? (sizes[1] ? 2 : 1) : 0);

  orwl_scale(sizeof(double[size]));

  /* Create handles for the locations that we are interested in. We
     will create a chain of dependencies from task 0 to task 1 etc. */
  orwl_handle2 here = ORWL_HANDLE2_INITIALIZER;
  orwl_handle2 there[2] = { ORWL_HANDLE2_INITIALIZER, ORWL_HANDLE2_INITIALIZER, };

  /* Have our own task writable. */
  orwl_write_insert(&here, ORWL_LOCATION(mytid), 0);

  /* all but task 0 link the "there" handle to their predecessor */
  for (unsigned i = 0; i < numb; ++i)
    orwl_read_insert(&there[i], ORWL_LOCATION(children[i]), 1);

  /* Now synchronize again to have all requests inserted orderly at
     the other end. */
  orwl_schedule();

  for (size_t orwl_phase = 0; orwl_phase < 10; ++orwl_phase) {
    /* All tasks create a critical section that guarantees exclusive
       access to their location. */
    ORWL_SECTION(&here) {
      /* Obtain a pointer to the buffer in our virtual address space. */
      double * hval = orwl_write_map(&here);
      *hval = init_val + orwl_phase + mytid;
      size_t pos = 1;

      PROGRESS(1, orwl_phase, "we initialized our value to " ORWL_TERM_REMARK "%s" ORWL_TERM_OFF, *hval);

      for (unsigned i = 0; i < numb; ++i) {
        /* this will block until the data is available */
        ORWL_SECTION(&there[i]) {
          /* This one is read only. */
          double const* tval = orwl_read_map(&there[i]);
          /* Accumulate the values */
          memcpy(&hval[pos], tval, sizeof(double[sizes[i]]));
          pos += sizes[i];
        }
      }
      if (mytid) PROGRESS(1, mytid, "found %s", *hval);
      else {
        fprintf(stderr, ORWL_TERM_CLEAR "found the result" ORWL_TERM_ALERT);
        for (size_t i = 0; i < size; ++i) fprintf(stderr, "\t% 6.4f", hval[i]);
        fprintf(stderr, ORWL_TERM_OFF "\n");
      }
    }
  }
  orwl_disconnect(&here);
  orwl_disconnect(there);
  orwl_stop_task();
}
