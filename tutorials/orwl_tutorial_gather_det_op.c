/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the author and copyright holder for this work is                           */
/* all rights reserved,  2012 Jens Gustedt, INRIA, France                     */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#include "orwl_tutorial_task_obj.h"

#define CONV_LIMIT 100
//
// TASK THREAD
//
P99_DECLARE_STRUCT(task_thread);
P99_DEFINE_STRUCT(task_thread,
         );

task_thread* task_thread_init(task_thread* task) {
  if (task) {
    *task = (task_thread) {
    };
  }
  return task;
}

void task_thread_destroy(task_thread *task) {
}

DECLARE_NEW_DELETE(task_thread);
DEFINE_NEW_DELETE(task_thread);
DECLARE_THREAD(task_thread);

ORWL_ALLGATHER_LOCATION(thrd_loc);
//
// END TASK THREAD
//
// Define a structure to store gathered data
P99_DECLARE_STRUCT(gather_str);
P99_DEFINE_STRUCT(gather_str,
                  size_t points,
                  size_t changes,
                  bool   convergence
                );

gather_str* gather_str_init(gather_str* el, size_t points, size_t changes, bool convergence) {
  if (el) {
    *el = (gather_str) {
      .points = points,
      .changes = changes,
      .convergence = convergence,
    };
  }
  return el;
}

DEFINE_THREAD(task_thread) {
  // ORWL_THREAD_USE(task_obj);
  trace(1, "detached thread orwl_myloc %zu from task %zu", orwl_myloc, orwl_mytid);
  orwl_server *srv = orwl_server_get();
  double tot_points, tot_changes;
  ORWL_ALLGATHER_DECLARE(gather_str, thrd_loc, (orwl_nt - 1) * orwl_locations_amount + thrd_loc);

  // Handles declaration
  orwl_handle2 thrw_hdl = ORWL_HANDLE2_INITIALIZER;
  orwl_handle2 tskr_hdl = ORWL_HANDLE2_INITIALIZER;
  
  orwl_scale(sizeof(gather_str), ORWL_LOCATION(orwl_mytid, sec_loc));
  orwl_write_insert(&thrw_hdl, ORWL_LOCATION(orwl_mytid, sec_loc), 0);
  orwl_read_insert(&tskr_hdl, ORWL_LOCATION(orwl_mytid, main_loc), 1);

  gather_str *thrw_hdl_ptr = P99_NEW(gather_str, 0, 0, false);

  orwl_schedule(orwl_myloc, 1, srv);

  for (size_t t = 0; !thrw_hdl_ptr->convergence; t++) {
    ORWL_SECTION(&tskr_hdl){
      gather_str *tskr_hdl_ptr = orwl_read_map(&tskr_hdl);
      thrd_loc[orwl_mytid].points = tskr_hdl_ptr->points;
      thrd_loc[orwl_mytid].changes = tskr_hdl_ptr->changes;
    }
    
    ORWL_ALLGATHER(thrd_loc);

    for (size_t i = 0; i < orwl_nt; i++) {
      tot_points += thrd_loc[i].points;
      tot_changes += thrd_loc[i].changes;
    }

    ORWL_SECTION(&thrw_hdl){
        thrw_hdl_ptr = orwl_write_map(&thrw_hdl);
        if (tot_points > CONV_LIMIT) {
          thrw_hdl_ptr->convergence = true;
        }
        thrw_hdl_ptr->points = tot_points;
        thrw_hdl_ptr->changes = tot_changes;
    }
  }

  ORWL_ALLGATHER_DISCONNECT(thrd_loc);
  orwl_disconnect(&thrw_hdl);
  orwl_disconnect(&tskr_hdl);
}

ORWL_DEFINE_TASK(task_obj) {
  trace(1, "enter task %zu", orwl_mytid);
  // Handle to communicate with the detached thread
  orwl_handle2 thrr_hdl = ORWL_HANDLE2_INITIALIZER;
  orwl_handle2 tskw_hdl = ORWL_HANDLE2_INITIALIZER;

  orwl_scale(sizeof(gather_str), ORWL_LOCATION(orwl_mytid, main_loc));
  orwl_read_insert(&thrr_hdl, ORWL_LOCATION(orwl_mytid, sec_loc), 1);
  orwl_write_insert(&tskw_hdl, ORWL_LOCATION(orwl_mytid, main_loc), 0);

  size_t myloc = orwl_myloc;
  orwl_server *const srv = orwl_server_get();
  orwl_locations task1 = ORWL_LOCAL(myloc);
  p99_seed *seed = p99_seed_get();

  task_thread* thr = P99_NEW(task_thread);
  *thr = (task_thread) {
  };

  gather_str *thrr_hdl_ptr = P99_NEW(gather_str, 0, 0, false);

  task_thread_operation(thr, orwl_myloc + thrd_loc);
  orwl_schedule(orwl_myloc, 2, srv);

  for (size_t t = 0; !thrr_hdl_ptr->convergence; t++) {
    ORWL_SECTION(&tskw_hdl) {
      gather_str *tskw_hdl_ptr = orwl_write_map(&tskw_hdl);
      tskw_hdl_ptr->points = p99_rand(seed)%10;
      tskw_hdl_ptr->changes = p99_rand(seed)%10;
    }

    ORWL_SECTION(&thrr_hdl) {
      thrr_hdl_ptr = orwl_read_map(&thrr_hdl);
      trace(1, "it %zu task %zu points %zu changes %zu", t, orwl_mytid, thrr_hdl_ptr->points, thrr_hdl_ptr->changes);
    }
  }

  orwl_disconnect(&thrr_hdl);
  orwl_disconnect(&tskw_hdl);
}
