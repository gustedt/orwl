/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the authors and copyright holders for this work are as follows:            */
/* all rights reserved,  2012-2013 Jens Gustedt, INRIA, France                */
/* all rights reserved,  2013 Rodrigo Campos, INRIA, France                   */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */

#include <opencv/highgui.h>

//! [compile time parametrization]
#include "orwl.h"
ORWL_LOCATIONS_PER_TASK(main_loc);
ORWL_LOCATIONS_PER_TASK_INSTANTIATION();
//! [compile time parametrization]

//! [declare task specific state]
P99_DECLARE_STRUCT(task_obj);
P99_DEFINE_STRUCT(task_obj,
                  double loop,
                  int size
                 );

task_obj* task_obj_init(task_obj* task, double my_loop, int my_size) {
  if (task) {
    *task = (task_obj) {
      .loop = my_loop,
      .size = my_size,
    };
  }
  return task;
}

void task_obj_destroy(task_obj *task) {
  /* empty */
}

P99_DECLARE_DELETE(task_obj);
P99_DEFINE_DELETE(task_obj);
ORWL_DECLARE_TASK(task_obj);
//! [declare task specific state]

void synthetic ( double * vector , int size , double val, int time )
{
  
  for (int k=0; k<time ; ++k)
  {
	for (int s=0; s<size ; ++s)
  {
	  vector[s] = vector[s]+val ;
  }  
  }
	
}

void producer (double * input, int loop)
{
	IplImage* img = NULL;
	 char s [350];
     sprintf(s,"images-numbers/%d.jpg",loop+1);
     printf(s);
	//img = cvLoadImage("lena.jpg", -1);
	img = cvLoadImage(s, -1);

  if (img == NULL)
  {
    fprintf (stderr, "couldn't open image file: %s\n", "lena.jpg");
    return EXIT_FAILURE;
  }
	
	int size = 512*512; //img->imageSize;
	
	/*cvNamedWindow("window", CV_WINDOW_AUTOSIZE);
    cvShowImage("window", img);
    cvWaitKey(0);
    cvReleaseImage(&img);
    cvDestroyWindow("window");*/
    		 
	    for (int j = 0; j <size ; ++j)
	{ 
		input[j] = (double) img->imageData[j];
		
	printf(" \n A - value of pixel %d is %f ", j , img->imageData[j]);
	printf(" \n A - value of pixel %d is %f ", j , input[j]);
	}
	
	
	//printf(" \n value of pixel %d is %f ", 23 , img->imageData[23]);
	//printf(" \n value of pixel %d is %f ", 23 , input[23]);
		 
  }
  
void consummer (double * w2val, int size)
{
	IplImage *img1 = cvCreateImage(cvSize(512, 512), IPL_DEPTH_8U, 1);	
	
	    for (int j = 0; j <size ; ++j)
	{ 
		img1->imageData[j] = w2val[j] ;
	
     }
	cvNamedWindow("window", CV_WINDOW_AUTOSIZE);
    cvShowImage("window", img1);
    cvWaitKey(30);
    cvReleaseImage(&img1);
    cvDestroyWindow("window");
	
}  



int main(int argc, char **argv) {
  if (argc < 2) {
    REPORT(1, "only " ORWL_TERM_ALERT "%s commandline argument(s)" ORWL_TERM_OFF ", this ain't enough", argc);
    return EXIT_FAILURE;
  }
  double init_val = strtod(argv[1]);
  double loop = strtod(argv[2]);
  int size = strtod(argv[3]);
  
  
  //! [condition ORWL]
  orwl_init();
  //! [condition ORWL]

  //! [launch one thread per task]
  for (size_t i = 0; i < orwl_lt; i++) {
    task_obj* task = P99_NEW(task_obj, loop, size);
    task_obj_create_task(task, orwl_tids[i]);
  }
  
  
 
  //! [launch one thread per task]
  return EXIT_SUCCESS;
}

//! [start the task specific function definition]
ORWL_DEFINE_TASK(task_obj) {
  ORWL_THREAD_USE(task_obj, loop, size);
  //! [start the task specific function definition]
  struct timeval tv1,tv2;
  gettimeofday(&tv1, NULL); 
  
  //! [condition this task]
  /* Scale our own location(s) to the appropriate buffer size. */
  orwl_scale(sizeof(double[size]));
  //! [condition this task]

  /* Sleep a bit to give the user the occasion to admire the following
     message. */
  sleepfor(0.5 * p99_drand());
  /* Now ORWL knows the total amount of tasks that will be
     performed. Avoid being too verbose so let only task 0 tell it. */
  PROGRESS(1, orwl_myloc,
           "<-------- the start of the report line now should contain location ID (%s) and total number (%s)",
           orwl_myloc, orwl_nl);

  //! [connect the resources]
  /* Create handles for the locations that we are interested in. We
     will create a chain of dependencies from task 0 to task 1 etc. */
  orwl_handle2 here = ORWL_HANDLE2_INITIALIZER;
  orwl_handle2 there = ORWL_HANDLE2_INITIALIZER;

  /* Have our own location writable. */
   if (!orwl_mytid)
  orwl_write_insert(&here, ORWL_LOCATION(orwl_mytid, main_loc), orwl_mytid);
  
  if (orwl_mytid)
  orwl_write_insert(&here, ORWL_LOCATION(orwl_mytid, main_loc), orwl_mytid);
  /* link the "there" handle where appropriate */
  if (orwl_mytid)
    orwl_read_insert(&there, ORWL_LOCATION(orwl_mytid - 1, main_loc), orwl_mytid);
    
  

  /* Now synchronize to have all requests inserted orderly at the
     other end. */
  orwl_schedule();
  //! [connect the resources]

  //! [perform the task]
  /* All tasks create a critical section that guarantees exclusive
     access to their location. */
     
  for (size_t i = 0; i <loop ; ++i)
	{  
      
	 if (!orwl_mytid) 
     {
	   	double *input =  malloc ( size * sizeof(double));  /// study the overhead of iterative allocation
		 producer(input, i);
		 
		 
	  	 printf(" value of pixel %d is %f ", 23 , input[23]);
		 
   ORWL_SECTION(&here) {
    /* Obtain a pointer to the buffer in our virtual address space. */
    double * wval = orwl_write_map(&here);
    //*wval = init_val*(i+1);
    
    /// producer computation ///
       for (int j = 0; j <size ; ++j)
	{ 
		wval[j] = input[j] ;
	}
    
    REPORT(!orwl_mytid, "first value is " ORWL_TERM_REMARK "%s" ORWL_TERM_OFF, wval[0]);
    // printf(" \n I'm a task N° %d | value init = %f \n", orwl_mytid, wval[0]);
      }
      
      free(input);  
  
     }          
    /* All other tasks read the value of their predecessor. */
   
   else {
      /* this will block until the data is available */
      double temp[size];
      
      ORWL_SECTION(&there) {
        double const* rval = orwl_read_map(&there);
        /* Do some dummy computation. */
       // *wval = (*rval + *wval) * 0.5;
     //   REPORT(1, "we stored the average: %s", *wval);
        for (int j = 0; j <size ; ++j)
	{ 
	temp[j]= rval[j];
	}
     
       }  
       
   //// switch task ////
   
   switch (orwl_mytid) {
   
   case 1: 
    synthetic(temp,size, 0.0001, 100);
	break;
       
    case 2: 
     synthetic(temp,size, 0.0010, 100);
     break;
     
     
     case 3:  
      synthetic(temp,size, 0.01, 100);
     break;
     
     case 4: 
       synthetic(temp,size, 0.10, 100);
	 break;
	
      }
	
     ORWL_SECTION(&here) {
        double * w2val = orwl_write_map(&here);
	     
	      for (int j = 0; j <size ; ++j)
	{ 
        w2val[j] =	temp[j] ;
	}
     
       
	     if (orwl_mytid==9)  { 
			 printf(" \n I'm a task N° %d | value 1 = %f \n", orwl_mytid, w2val[23]);
	
             consummer(w2val,size);
    
          }
      
      // printf(" \n I'm a task N° %d | value 2 = %f \n", orwl_mytid, *rval);
     }
     
 }  
   
}
   
  
  //! [perform the task]


  orwl_disconnect(&here);
  orwl_disconnect(&there);
  //! [say good bye]
  orwl_stop_task();
  //! [say good bye]
  
 gettimeofday(&tv2, NULL); 
 long temp;
 if (tv2.tv_usec<tv1.tv_usec) temp=1000000+ tv2.tv_usec-tv1.tv_usec;
 else temp = tv2.tv_usec-tv1.tv_usec;
 printf("%ld s and %ld u_sec \n", tv2.tv_sec-tv1.tv_sec, temp);

}
