/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the author and copyright holder for this work is                           */
/* all rights reserved,  2012 Jens Gustedt, INRIA, France                     */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#include "orwl_helpers.h"
#include "orwl_tutorial_task_obj.h"

ORWL_BROADCAST_LOCATION(sec_loc);

ORWL_DEFINE_TASK(task_obj) {
  ORWL_THREAD_USE(task_obj, init_val);
  size_t const mytid = orwl_mytid;
  ORWL_BROADCAST_DECLARE(size_t, sec_loc, (orwl_nl - orwl_locations_amount) + sec_loc);

  /* Now synchronize again to have all requests inserted orderly at
     the other end. */
  orwl_schedule();

  sec_loc = 1 + (p99_rand() % orwl_nt);
  /* First communicate the value of the desired vector size to all
     tasks. */
  ORWL_BROADCAST(sec_loc);
  size_t size = sec_loc;
  if (!mytid || mytid == (orwl_nt - 1)) REPORT(1, "vector size chosen is %s", size);
  {
    typedef double aVector[size];
    ORWL_BROADCAST_DECLARE(aVector, sec_loc);
    for (size_t orwl_phase = 0; orwl_phase < 10; ++orwl_phase) {
      /* Store some new value of your liking */
      sec_loc[0] = orwl_phase + init_val + mytid;
      sec_loc[size - 1] = orwl_phase + mytid;
      ORWL_BROADCAST(sec_loc);
      if (!mytid || mytid == (orwl_nt - 1))
        report(1, ORWL_TERM_CLEAR "found the result   " ORWL_TERM_ALERT "% 6.4f % 6.4f" ORWL_TERM_OFF, sec_loc[0], sec_loc[size - 1]);
    }
  }
  orwl_stop_task();
}
