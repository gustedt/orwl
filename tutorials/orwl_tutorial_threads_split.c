/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the author and copyright holder for this work is                           */
/* all rights reserved,  2016 Jens Gustedt, INRIA, France                     */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#include "orwl_tutorial_task_obj.h"
#include "orwl_split.h"

//! [function that will be executed by the provider task]
void provider(void) {

  //! [condition provider]
  /* Scale our own main location to the appropriate buffer size. */
  orwl_scale(sizeof(double[opt_n]));
  //! [condition provider]

  //! [connect the resources of the provider]
  /* Create handles for the locations that we are interested in. */
  orwl_handle2 providerOut = ORWL_HANDLE2_INITIALIZER;

  /* Provider is a writer, so it comes before the split. */
  orwl_write_insert(&providerOut, ORWL_LOCATION(orwl_mytid, main_loc), 0);

  size_t nW = orwl_nt-1;
  orwl_split_location wrks[nW];
  wrks[0] = (orwl_split_location) { ORWL_LOCATION(orwl_mytid, main_loc) };
  for (size_t i = 2; i < orwl_nt; ++i) {
    wrks[i-1] = (orwl_split_location) { ORWL_LOCATION(i, sec_loc) };
  }
  orwl_split(true, nW, wrks);

  /* Now synchronize to have all requests inserted orderly at the
     other end. */
  orwl_schedule();

  //! [connect the resources of the provider]

  //! [start the provider iteration loop]
  for (size_t orwl_phase = 0; orwl_phase < opt_i; ++orwl_phase) {
    //! [start the provider iteration loop]
    //! [perform the provider task]
    //! [Condition the data for the workers]
    ORWL_SECTION(&providerOut) {
      /* Obtain a pointer to the buffer in our virtual address space. */
      size_t sIn;
      double* wIn = orwl_write_map(&providerOut, &sIn);
      /* Do some dummy computation. */
      for (size_t j = 0; j < opt_n; ++j)
        wIn[j] = orwl_phase+313100;
    }
    //! [Condition the data for the workers]

    /* providerOutSplit kicks in here and does the split */
    /* workers do their work */
  }
  //! [perform the provider task]
  orwl_phase = UINT32_MAX;

  //! [provider says good bye]
  orwl_disconnect(&providerOut);
  orwl_stop_task();
  //! [provider says good bye]
}
//! [function that will be executed by the provider task]

//! [function that will be executed by the consumer task]
void consumer(void) {

  //! [condition consumer]
  /* The consumer is conditioned implicitly from the sum of the sizes
     of his workers. */
  //! [condition consumer]

  //! [connect the resources for the consumer]
  /* Create handles for the locations that we are interested in. */
  orwl_handle2 consumerIn = ORWL_HANDLE2_INITIALIZER;

  /* Consumer is a reader, so it comes after the split. */
  orwl_read_insert(&consumerIn, ORWL_LOCATION(orwl_mytid, main_loc), 1);

  size_t nW = orwl_nt-1;
  orwl_split_location wrks[nW];
  wrks[0] = (orwl_split_location) { ORWL_LOCATION(orwl_mytid, main_loc) };
  for (size_t i = 2; i < orwl_nt; ++i) {
    wrks[i-1] = (orwl_split_location) { ORWL_LOCATION(i, main_loc) };
  }
  orwl_split(false, nW, wrks);

  /* Now synchronize to have all requests inserted orderly at the
     other end. */
  orwl_schedule();

  //! [start the consumer iteration loop]
  for (size_t orwl_phase = 0; orwl_phase < opt_i; ++orwl_phase) {
    /* consumerInSplit kicks in here and does the split */
    /* the workers do their work, and then we go. */
    //! [start the consumer iteration loop]
    //! [perform the consumer task]
    ORWL_SECTION(&consumerIn) {
      sleepfor(0.005 * p99_drand());
      /* Obtain a pointer to the buffer in our virtual address space. */
      size_t sIn;
      double const* wIn = orwl_read_map(&consumerIn, &sIn);
      size_t workers = orwl_nt-2;
      size_t share = sIn/sizeof *wIn/workers;
      // in every phase show us the value of a different worker
      size_t worker = orwl_phase % workers;
      progress(1, worker, "we received a new value from worker %zX: wIn[%.8zX], %.12g", worker, worker*share, wIn[worker*share]);
    }
    //! [perform the consumer task]
  }
  orwl_phase = UINT32_MAX;

  //! [consumer says good bye]
  orwl_disconnect(&consumerIn);
  orwl_stop_task();
  //! [consumer says good bye]
}
//! [function that will be executed by the consumer task]


//! [function that will be executed by the workers]
void worker(void) {

  //! [condition a worker]
  /* Scale our own main location to the appropriate buffer size. This
     could be a different size for each worker. */
  size_t workers = orwl_nt-2;
  orwl_scale(sizeof(double[opt_n/workers]));
  /* The secondary location is taken as the input to this worker. It
     is scaled appropriately by the split. */
  //! [condition a worker]

  //! [connect the resources for the worker]
  /* Create handles for the locations that we are interested in. */
  orwl_handle2 workerIn = ORWL_HANDLE2_INITIALIZER;
  orwl_handle2 workerOut = ORWL_HANDLE2_INITIALIZER;

  /* Have our own locations writable. */
  /* These must always have priority greater than 0, to give the split
     the chance to kick in first. */
  orwl_write_insert(&workerOut, ORWL_LOCATION(orwl_mytid, main_loc), 0);
  orwl_read_insert(&workerIn, ORWL_LOCATION(orwl_mytid, sec_loc), 1);

  /* Now synchronize to have all requests inserted orderly at the
     other end. */
  orwl_schedule();

  //! [connect the resources for the worker]

  //! [start a worker iteration loop]
  for (size_t orwl_phase = 0; orwl_phase < opt_i; ++orwl_phase) {
    //! [start a worker iteration loop]
    //! [perform the worker task]
    //! [Connect to input and output for the master]
    ORWL_SECTION(&workerIn) {
      ORWL_SECTION(&workerOut) {
        size_t nIn;
        double const* wIn = orwl_read_map(&workerIn, &nIn);
        nIn /= sizeof *wIn;
        /* Obtain a pointer to the buffer in our virtual address space. */
        size_t nOut;
        double* wOut = orwl_write_map(&workerOut, &nOut);
        nOut /= sizeof *wOut;
        P99_THROW_ASSERT(EINVAL, nIn == nOut);
        //! [Connect to input and output for the master]
        //! [Do the worker computation]
        for (size_t j = 0; j < nIn; ++j)
          wOut[j] = wIn[j] + orwl_phase*orwl_mytid;
        //! [Do the worker computation]
      }
    }

  }
  //! [perform the task]
  orwl_phase = UINT32_MAX;

  //! [say good bye]
  orwl_disconnect(&workerIn);
  orwl_disconnect(&workerOut);
  orwl_stop_task();
  //! [say good bye]
}
//! [function that will be executed by the workers]



//! [the task specific function is a dispatcher]
ORWL_DEFINE_TASK(task_obj) {
  if (orwl_nt < 3) {
    report(!orwl_mytid, ORWL_TERM_ALERT "this program needs at least 3 tasks to run" ORWL_TERM_OFF);
    return;
  }
  switch (orwl_mytid) {
  case 0: provider(); break;
  case 1: consumer(); break;
  default: worker();  break;
  }
}
//! [the task specific function is a dispatcher]
