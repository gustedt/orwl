/* This may look like nonsense, but it really is -*- mode: C -*-              */
/*                                                                            */
/* Except for parts copied from previous work and as explicitly stated below, */
/* the authors and copyright holders for this work are as follows:            */
/* all rights reserved,  2013 Jens Gustedt, INRIA, France                     */
/* all rights reserved,  2013 Rodrigo Campos, INRIA, France                   */
/*                                                                            */
/* This file is part of the ORWL project. You received this file as as        */
/* part of a confidential agreement and you may generally not                 */
/* redistribute it and/or modify it, unless under the terms as given in       */
/* the file LICENSE.  It is distributed without any warranty; without         */
/* even the implied warranty of merchantability or fitness for a              */
/* particular purpose.                                                        */
/*                                                                            */
#include "orwl.h"
#include <stdbool.h>



ORWL_LOCATIONS_PER_TASK(main_loc);
ORWL_LOCATIONS_PER_TASK_INSTANTIATION();

P99_DECLARE_STRUCT(task_obj);
P99_DEFINE_STRUCT(task_obj, double init_val);

task_obj* task_obj_init(task_obj* task, double val) {

  if (!task)
    return NULL;

  *task = (task_obj) { .init_val = val, };
  return task;
}

void task_obj_destroy(task_obj *task) {
  /* empty */
}

P99_DECLARE_DELETE(task_obj);
P99_DEFINE_DELETE(task_obj);
ORWL_DECLARE_TASK(task_obj);


/*
 * We organize the locations as follow:
 * locations[0] = first neigh to the left (undifined if it does not exist)
 * locations[1] = first neigh to the right (undifined if it does not exist)
 * locations[2] = first neigh up (undifined if it does not exist)
 * locations[3] = first neigh down (undifined if it does not exist)
 * locations[4] = second neigh to the left (undifined if it does not exist)
 * locations[5] = second neigh to the right ((undifined if it does not exist)
 * ...
 *
 * And then we mat them with the task id of the first neigh to the left and
 * so on (always if they exist). That's why we define the orwl_handle* as this
 * on this macro
 *
 * "i" is the task id we want to declare the neighbors to. So "i" should be a
 * number between 0 and orwl_nt - 1
 */
#define DEFINE_NEIGHBORS(i)                                    \
      size_t left_loc_id = get_neigh(1 * -1 * (i + 1));        \
      size_t right_loc_id = get_neigh(1 * (i + 1));            \
      size_t up_loc_id = get_neigh(row_size * -1 * (i + 1));   \
      size_t down_loc_id = get_neigh(row_size * (i + 1));      \
      orwl_handle2 *left = locations + i * 4 + 0;              \
      orwl_handle2 *right = locations + i * 4 + 1;             \
      orwl_handle2 *up = locations + i * 4 + 2;                \
      orwl_handle2 *down = locations + i * 4 + 3;

void read_neigh(orwl_handle2 *neigh, double *write_here) {
  ORWL_SECTION(neigh) {
    double const* rval = orwl_read_map(neigh);
    *write_here = (*rval + *write_here) * 0.5;
  }
}

/* Get the neighbor task id that is that "x" positions after if "x" is positive,
 * or the neighbor that is "x" positions before if "x" is negative. If "x" is
 * zero, it just returns itself.
 * It returns orwl_nt (an invalid task number) if such a neighbour does not
 * exist.
 * For example, the first cell does not have any previous cell (-1) under this
 * definition of neighbor */
size_t get_neigh(int x) {

  size_t neigh = orwl_mytid + x;
  if (neigh >= orwl_nt)
    return orwl_nt;

  return neigh;
}

// Checks if we can read from this neighbor. For us to can read from it there
// must be a neigh obtained with get_neigh()
// So we check that is a valid neigh (!= orwl_nt) and that is not ourselfs, because
// we are already adding a write on our location, so we cannot read too.
bool is_neigh_readable(size_t neigh) {
  return neigh != orwl_nt && neigh != orwl_mytid;
}



int main(int argc, char **argv) {
  if (argc < 2) {
    REPORT(1, "only " ORWL_TERM_ALERT "%s commandline argument(s)" ORWL_TERM_OFF ", this ain't enough", argc);
    return EXIT_FAILURE;
  }
  double init_val = strtod(argv[1]);

  /*
   * With orwl_nt and row_size we have a matrix. We have orwl_nt cells and each
   * row is row_size long. On this grid each task will write on his own location
   * and read from the neighbors.
   *
   * For example, if we have 9 tasks and the row_size is 3, it should look like
   * this:
   *
   * +------------------------+
   * |task 1 | task 2 | task 3|
   * |------------------------|
   * |task 4 | task 5 | task 6|
   * |------------------------|
   * |task 7 | task 8 | task 9|
   * +------------------------+
   *
   * Then we can configure how many neighbors each task will read. For example
   * with "1" task 5 will read the neighbors at distance one, on the same row or
   * column. That is tasks: 4, 2, 6 and 8. If you use "2", you will read 4 and 3
   * as left neighbors (left neighbors are our id -1 and our id -2), 6 and 7 as
   * right neighbors and only 2 and 8 as up/down neighbors, as the definition of
   * neighbor does not cycle (so it does NOT have task 2 as a down neighbor too)
   *
   * TODO: Make sure the library throws an error if the user wants to do some of
   * this "strange" things on the the library doesn't handle them correctly.
   * IOW, make sure it throws an error when it should.
   *
   */

  orwl_init();

  for (size_t i = 0; i < orwl_lt; i++) {
    task_obj* task = P99_NEW(task_obj, init_val);
    task_obj_create_task(task, orwl_tids[i]);
  }

  return EXIT_SUCCESS;
}

ORWL_DEFINE_TASK(task_obj) {
  ORWL_THREAD_USE(task_obj, init_val);


  // TODO: take these variable from command line instead
  int row_size = 2;
  int neigh = 2;
  assert(neigh > 0);
  if (orwl_nt % row_size != 0) {
    REPORT(1, "Total number of tasks %s is not multiple of %s", orwl_nt, row_size);
    return;
  }

  if (row_size > orwl_nt) {
    REPORT(1, "Total row_size %s could not be bigger than the amounts of tasks: %s", row_size, orwl_nt);
    return;
  }


  /* Scale our own location(s) to the appropriate buffer size. */
  orwl_scale(sizeof(double));

  /* Create handles for the locations that we are interested in */
  orwl_handle here_1 = ORWL_HANDLE_INITIALIZER;
  orwl_handle2 here_2 = ORWL_HANDLE2_INITIALIZER;

  // If neigh is 2, for example, we will read the neighbor up, down, left and
  // right, and then the next up, next down, next left and next right. So, we
  // need 4 (up, down, left, right) mutiplied by the amounts of "neigh"
  // locations
  orwl_handle2 locations[4 * neigh];

  // XXX: Why we need to use orwl_handle_init and does not compile if we use
  // something like:
  // locations[i] = ORWL_HANDLE_INITIALIZER
  // ?
  for (int i = 0; i < 4 * neigh; i++)
    orwl_handle2_init(locations + i);

  // Everyone writes something to his task: first to write something the
  // others will read
  orwl_write_insert(&here_1, ORWL_LOCATION(orwl_mytid, main_loc), 0);

  // Slice everything one place so the previous write has the lowest priority.
  // Then, after that write, each task gets the priority in order using their
  // id
  size_t prio = orwl_mytid + 1;

  // And the second write is to write "here" whatever we read (if anything at
  // all) from our neighbors. And we issue this write with the same priority as
  // the read from the neighbors
  orwl_handle2_write_insert(&here_2, ORWL_LOCATION(orwl_mytid, main_loc), prio);

  for (int i = 0; i < neigh; i++) {

    DEFINE_NEIGHBORS(i);

    if (is_neigh_readable(left_loc_id))
      orwl_handle2_read_insert(left, ORWL_LOCATION(left_loc_id, main_loc), prio);

    if (is_neigh_readable(right_loc_id))
      orwl_handle2_read_insert(right, ORWL_LOCATION(right_loc_id, main_loc), prio);

    if (is_neigh_readable(up_loc_id))
      orwl_handle2_read_insert(up, ORWL_LOCATION(up_loc_id, main_loc), prio);

    if (is_neigh_readable(down_loc_id))
      orwl_handle2_read_insert(down, ORWL_LOCATION(down_loc_id, main_loc), prio);

  }


  orwl_schedule();

  double *here_val;
  ORWL_SECTION(&here_1) {
    here_val = orwl_write_map(&here_1);
    *here_val = init_val;
  }

  for (int i = 0; i < 1000; i++) {
    ORWL_SECTION(&here_2) {
      here_val = orwl_write_map(&here_2);
      *here_val = init_val;

      for (int i = 0; i < neigh; i++) {

        DEFINE_NEIGHBORS(i);

        if (is_neigh_readable(left_loc_id))
          read_neigh(left, here_val);

        if (is_neigh_readable(right_loc_id))
          read_neigh(right, here_val);

        if (is_neigh_readable(up_loc_id))
          read_neigh(up, here_val);

        if (is_neigh_readable(down_loc_id))
          read_neigh(down, here_val);
      }
    }
  }


  REPORT(1, "checkpoint charlie");
  orwl_handle2_disconnect(&here_2);
  for (int i = 0; i < 4 * neigh; i++)
    orwl_handle2_disconnect(locations + i);

  orwl_stop_task();
}
