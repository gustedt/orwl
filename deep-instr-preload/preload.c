
#include "orwl_deep_instr.h"  // orwl_deep_instr_*()
#include "wrappers.h"         // o_rwl_sym_init()
#include "debug.h"            // dprintf(), printerr()
#include <stdint.h>           // uint64_t and friends
#include <stdlib.h>           // abort()
#include <dlfcn.h>            // dlsym(), dlopen()
#include <time.h>             // clock_gettime()

#define ORWL_SONAME "liborwl.so"

static void *orwl_lib = NULL;

// Forward declare this to avoid including more ".h"s that make compiling slower
typedef struct orwl_server orwl_server;


void *o_rwl_symbol(const char *symbol) {

  /* DO NOT make a print to stdout here -printf, dprintf, anything- see note
   * inside orwl_init() */

  /* Clear any existing error */
  dlerror();
  char *error;

  void *f_ptr = dlsym(orwl_lib, symbol);
  if ((error = dlerror()) != NULL)  {
      printerr("Error on dlsym: %s\n", error);
      abort();
  }

  /* If the function pointer is NULL, it is an error (for our use case) too */
  if (f_ptr == NULL) {
    printerr("Error getting the symbol for %s\n", symbol);
    abort();
  }

  return f_ptr;
}


/*
 * ALWAYS OVERRIDE orwl_init so we can init our stuff (like orwl_lib pointer)
 *
 * There is no problem on multi-threaded programs because orwl_init() must be
 * called only once, no matter how many threads there are.
 */
void orwl_init(orwl_server *srv, size_t locations_amount) {

  /* Estimate the overhead we add to orwl_init() */
  struct timespec start;
  int err = clock_gettime(CLOCK_THREAD_CPUTIME_ID, &start);
  if (err)
    abort();

  // Load global pointer and resolve all the symbols now (RTLD_NOW) so future
  // calls to dlsym() are faster
  orwl_lib = dlopen(ORWL_SONAME, RTLD_NOW);
  if (orwl_lib == NULL) {
    printerr("Error loading %s: %s\n", ORWL_SONAME, dlerror());
    abort();
  }

  /* Init the pointers for all other functions we will wrap */
  o_rwl_sym_init();

  /* Get the pointer to the real orwl_init() */
  void (*real_f)(orwl_server*, size_t);
  real_f = o_rwl_symbol("orwl_init");

  /* Get the overhead doing all, just except calling orwl_init() */
  struct timespec end;
  err = clock_gettime(CLOCK_THREAD_CPUTIME_ID, &end);
  if (err)
    abort();

  uint64_t overhead = end.tv_sec * 1000000000UL + end.tv_nsec -
                      (start.tv_sec * 1000000000UL + start.tv_nsec);

  orwl_deep_instr_set_init_overhead(overhead);


  /* Do the real orwl_init()
   * XXX: YOU CAN NOT DO ANY PRINT TO STDOUT BEFORE CALLING the real orwl_init()
   * During init some things are printed to stdout to create an address book and
   * if we print something before the initialization is done, it doesn't have
   * the expected format and it crashes
   * TODO: document this limitation!
   * XXX: The orwl_deep_instr_init() is done in the real orwl_init(), so don't
   * need to call it again! (Note: to use LD_PRELOAD, the lib must be compiled
   * with DEEP_INSTRUMENTATION anyways, so the call to orwl_deep_instr_init()
   * will always be there)
   */
  real_f(srv, locations_amount);

  dprintf("%s\n", "Overridden init finished OK!");
}
