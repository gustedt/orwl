#ifndef PRELOAD_H
#define PRELOAD_H


// Returns a pointer to the symbol in liborwl.so, so you can call the real
// function in orwl
void *o_rwl_symbol(const char *symbol);



#endif // PRELOAD_H
