#!/usr/bin/env bash

# Just a basic test running LD_PRELOAD with a very simple tutorial
# Make sure this is called when the library is compiled with
# DEEP_INSTRUMENTATION=1

set -e

# Some basic env vars to orwlrun
export ORWL_VERBOSE=0
export ORWL_NO_COMPRESS=1

# Use an absolute path to the proj_root so we can use it even we "cd" to some
# other dir
CWD_ABS_PATH=$(cd $(dirname $0); pwd)
PROJ_ROOT="${CWD_ABS_PATH}/../../"

DEEP_INSTR_DIR="${PROJ_ROOT}/deep-instr-preload/"
DEEP_INSTR_TMP="${DEEP_INSTR_DIR}/tests/tests-tmp"

# Remove output from previous runs
rm -fr $DEEP_INSTR_TMP
mkdir -p "$DEEP_INSTR_TMP"

# Make sure the preload library is built
cd $DEEP_INSTR_DIR
make DEBUG=1


# Run a tutorial
cd ${PROJ_ROOT}/tutorials
make DEBUG=1 warmup

# This uses the ORWL vars exported at the beginning
LD_PRELOAD="${DEEP_INSTR_DIR}/liborwl-preload.so" \
${PROJ_ROOT}/scripts/orwlrun ${DEEP_INSTR_TMP} default local 1:2 ./warmup 100


# To verify this, the test is quite simple and basic. We just read the logs for
# stdout (file name matches mattern "*-1", -1 for stdout) and check some prints
# are there
cd $DEEP_INSTR_TMP
cd */logs

# The output in a file called "something"-1
lib_output=$(grep -c "lib time" *-1)
lib_init_output=$(grep -c "lib init time" *-1)
lib_debug_output=$(grep -c "liborwl-preload.so" *-1)
lib_wrapping_output=$(grep -c "liborwl-preload.so.*Wrapping" *-1)

# There should always be one and only one print with the final stats
if [ ! $lib_output -eq 1 ]; then
	echo "Error while running the tests. Library output not detected"
	exit 1
fi

# There should always be one and only one print with the init overhead
if [ ! $lib_init_output -eq 1 ]; then
	echo "Error while running the tests. Library output not detected"
	exit 1
fi


if [ ! $lib_debug_output -gt 0 ]; then
	echo "Error while running the tests. liborwl-preload.so output not detected"
	exit 1
fi

if [ ! $lib_wrapping_output -gt 0 ]; then
	echo "Error while running the tests. No function wrapping detected!"
	exit 1
fi

# If tests run ok, remove the tmp directories too. If not, keep them to properly
# understand what went wrong
rm -r $DEEP_INSTR_TMP
echo "Tests OK"
