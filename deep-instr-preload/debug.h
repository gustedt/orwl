
#ifndef DEBUG_H
#define DEBUG_H

#include <stdio.h>
#include "p99_compiler.h"   // P99_UNLIKELY()

#define DBG_PREFIX "liborwl-preload.so:%s:%d: "

#ifdef DEBUG
#define dprintf(fmt, ...) fprintf(stdout, DBG_PREFIX fmt, __FILE__, __LINE__, \
                                  __VA_ARGS__); \
                          fflush(stdout);
#define DPRINT_ONCE(...) PRINT_ONCE(__VA_ARGS__)
#else
#define dprintf(fmt, ...)
#define DPRINT_ONCE( ...)
#endif

#define printerr(fmt, ...) fprintf(stderr, DBG_PREFIX fmt, __FILE__, __LINE__, \
                                  __VA_ARGS__); \
                           fflush(stderr);

#define PRINT_ONCE(fmt, ...) do {                                         \
    static int printed = 0;                                               \
    if (P99_UNLIKELY(!printed)) {                                         \
      fprintf(stdout, DBG_PREFIX fmt, __FILE__, __LINE__,  __VA_ARGS__);  \
      printed = 1;                                                        \
    }                                                                     \
  } while (0)


#endif // DEBUG_H
